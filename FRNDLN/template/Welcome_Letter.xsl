<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    <xsl:output
        indent="yes"
        method="xml" />
    <xsl:template match="WELCOME_LETTER">
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format"
            xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml"
            font-family="Calibri">
            <fo:layout-master-set xmlns:aml="http://schemas.microsoft.com/aml/2001/core"
                xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"
                xmlns:o="urn:schemas-microsoft-com:office:office"
                xmlns:rx="http://www.renderx.com/XSL/Extensions"
                xmlns:v="urn:schemas-microsoft-com:vml"
                xmlns:w10="urn:schemas-microsoft-com:office:word"
                xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint">
                <fo:simple-page-master
                    margin-bottom="0pt"
                    margin-left="72pt"
                    margin-right="72pt"
                    margin-top="0pt"
                    master-name="section1-first-page"
                    page-height="11in"
                    page-width="8.5in">
                    <fo:region-body
                        margin-bottom="72pt"
                        margin-top="72pt"></fo:region-body>
                    <fo:region-before
                        extent="11in"
                        region-name="first-page-header"></fo:region-before>
                    <fo:region-after
                        display-align="after"
                        extent="11in"
                        region-name="first-page-footer"></fo:region-after>
                </fo:simple-page-master>
                <fo:simple-page-master
                    margin-bottom="0pt"
                    margin-left="72pt"
                    margin-right="72pt"
                    margin-top="0pt"
                    master-name="section1-odd-page"
                    page-height="11in"
                    page-width="8.5in">
                    <fo:region-body
                        margin-bottom="72pt"
                        margin-top="72pt"></fo:region-body>
                    <fo:region-before
                        extent="11in"
                        region-name="odd-page-header"></fo:region-before>
                    <fo:region-after
                        display-align="after"
                        extent="11in"
                        region-name="odd-page-footer"></fo:region-after>
                </fo:simple-page-master>
                <fo:simple-page-master
                    margin-bottom="0pt"
                    margin-left="72pt"
                    margin-right="72pt"
                    margin-top="0pt"
                    master-name="section1-even-page"
                    page-height="11in"
                    page-width="8.5in">
                    <fo:region-body
                        margin-bottom="72pt"
                        margin-top="72pt"></fo:region-body>
                    <fo:region-before
                        extent="11in"
                        region-name="even-page-header"></fo:region-before>
                    <fo:region-after
                        display-align="after"
                        extent="11in"
                        region-name="even-page-footer"></fo:region-after>
                </fo:simple-page-master>
                <fo:page-sequence-master master-name="section1-page-sequence-master">
                    <fo:repeatable-page-master-alternatives>
                        <fo:conditional-page-master-reference
                            master-reference="section1-odd-page"
                            odd-or-even="odd" />
                        <fo:conditional-page-master-reference
                            master-reference="section1-even-page"
                            odd-or-even="even" />
                    </fo:repeatable-page-master-alternatives>
                </fo:page-sequence-master>
            </fo:layout-master-set>
            <fo:page-sequence xmlns:aml="http://schemas.microsoft.com/aml/2001/core"
                xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"
                xmlns:o="urn:schemas-microsoft-com:office:office"
                xmlns:rx="http://www.renderx.com/XSL/Extensions"
                xmlns:v="urn:schemas-microsoft-com:vml"
                xmlns:w10="urn:schemas-microsoft-com:office:word"
                xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint"
                format="1"
                id="IDKS1XFE0B51NKH3MQHGAXS2AOL5NRGLLIUQTRCBBAVE22WTSTYZ"
                master-reference="section1-page-sequence-master">
                <fo:static-content flow-name="first-page-header">
                    <fo:retrieve-marker
                        retrieve-boundary="page"
                        retrieve-class-name="first-page-header"
                        retrieve-position="first-including-carryover" />
                </fo:static-content>
                <fo:static-content flow-name="first-page-footer">
                    <fo:retrieve-marker
                        retrieve-boundary="page"
                        retrieve-class-name="first-page-footer"
                        retrieve-position="first-including-carryover" />
                </fo:static-content>
                <fo:static-content flow-name="odd-page-header">
                    <fo:retrieve-marker
                        retrieve-boundary="page"
                        retrieve-class-name="odd-page-header"
                        retrieve-position="first-including-carryover" />
                </fo:static-content>
                <fo:static-content flow-name="odd-page-footer">
                    <fo:retrieve-marker
                        retrieve-boundary="page"
                        retrieve-class-name="odd-page-footer"
                        retrieve-position="first-including-carryover" />
                </fo:static-content>
                <fo:static-content flow-name="even-page-header">
                    <fo:retrieve-marker
                        retrieve-boundary="page"
                        retrieve-class-name="odd-page-header"
                        retrieve-position="first-including-carryover" />
                </fo:static-content>
                <fo:static-content flow-name="even-page-footer">
                    <fo:retrieve-marker
                        retrieve-boundary="page"
                        retrieve-class-name="odd-page-footer"
                        retrieve-position="first-including-carryover" />
                </fo:static-content>
                <fo:static-content flow-name="xsl-footnote-separator">
                    <fo:block>
                        <fo:leader
                            color="gray"
                            leader-length="144pt"
                            leader-pattern="rule"
                            rule-style="solid"
                            rule-thickness="0.5pt" />
                    </fo:block>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body">
                    <fo:block
                        font-size="10pt"
                        line-height="1.147"
                        orphans="2"
                        white-space-collapse="false"
                        widows="2">
                        <fo:marker xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            marker-class-name="first-page-header" />
                        <fo:marker xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            marker-class-name="first-page-footer" />
                        <fo:marker xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            marker-class-name="odd-page-header" />
                        <fo:marker xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            marker-class-name="odd-page-footer" />
                        <fo:marker xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            marker-class-name="even-page-header" />
                        <fo:marker xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            marker-class-name="even-page-footer" />
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            id="IDBM4ESJ4WW43GE2X32ENNGSLYDDGFDAIYHWPN2RGTNIIZ2C5XHFK1" />
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="0pt"
                            space-after.conditionality="retain">
                            <fo:leader />
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="0pt"
                            space-after.conditionality="retain">
                            <fo:inline>
                                <fo:leader
                                    leader-length="35.25pt"
                                    leader-pattern="use-content"> 
                                </fo:leader>
                            </fo:inline>
                            <fo:inline>
                                <fo:leader
                                    leader-length="35.25pt"
                                    leader-pattern="use-content"> 
                                </fo:leader>
                            </fo:inline>
                            <fo:inline>
                                <fo:leader
                                    leader-length="35.25pt"
                                    leader-pattern="use-content"> 
                                </fo:leader>
                            </fo:inline>
                            <fo:inline>
                                <fo:leader
                                    leader-length="35.25pt"
                                    leader-pattern="use-content"> 
                                </fo:leader>
                            </fo:inline>
                            <fo:inline>
                                <fo:leader
                                    leader-length="35.25pt"
                                    leader-pattern="use-content"> 
                                </fo:leader>
                                <fo:leader leader-length="0pt" />WELCOME LETTER
                            </fo:inline>
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="0pt"
                            space-after.conditionality="retain">
                            <fo:leader />
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="0pt"
                            space-after.conditionality="retain">
                            <fo:leader />
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="0pt"
                            space-after.conditionality="retain">
                            <fo:inline><fo:leader leader-length="0pt" />Date :
                                <xsl:value-of select="LETTER_DATE" />
                            </fo:inline>
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="0pt"
                            space-after.conditionality="retain">
                            <fo:inline>
                                <fo:leader leader-length="0pt" />
                                <xsl:value-of select="NAME" />
                            </fo:inline>
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="0pt"
                            space-after.conditionality="retain">
                            <fo:inline>
                                <fo:leader leader-length="0pt" />
                                <xsl:value-of select="ADDRESS" />
                            </fo:inline>
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="0pt"
                            space-after.conditionality="retain">
                            <fo:inline>
                                <fo:leader leader-length="0pt" />
                                <xsl:value-of select="DISTRICT" />
                            </fo:inline>
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="0pt"
                            space-after.conditionality="retain">
                            <fo:inline>
                                <fo:leader leader-length="0pt" />
                                <xsl:value-of select="STATE" />
                                –
                                <xsl:value-of select="PINCODE" />
                            </fo:inline>
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="0pt"
                            space-after.conditionality="retain">
                            <fo:inline><fo:leader leader-length="0pt" />Mobile No
                            </fo:inline>
                            <fo:inline>
                                <fo:leader
                                    leader-length="35.25pt"
                                    leader-pattern="use-content"> 
                                </fo:leader>
                                <fo:leader leader-length="0pt" />
                                :
                                <xsl:value-of select="MOBILE_NO" />
                            </fo:inline>
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="0pt"
                            space-after.conditionality="retain">
                            <fo:inline><fo:leader leader-length="0pt" />Reference No :
                                <xsl:value-of select="APPNO" />
                            </fo:inline>
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="10pt"
                            space-after.conditionality="retain">
                            <fo:leader />
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="10pt"
                            space-after.conditionality="retain">
                            <fo:inline><fo:leader leader-length="0pt" />Dear Customer,
                            </fo:inline>
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="10pt"
                            space-after.conditionality="retain">
                            <fo:inline><fo:leader leader-length="0pt" />We are delighted to welcome
                                you and are privileged to have you as our customer. Your Two Wheeler
                                Loan has been disbursed on<xsl:value-of select="DISBURSED_DATE" />.
                            </fo:inline>
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="10pt"
                            space-after.conditionality="retain">
                            <fo:inline><fo:leader leader-length="0pt" />The important terms of your
                                loan are given
                            </fo:inline>
                            <fo:inline><fo:leader leader-length="0pt" />below :
                            </fo:inline>
                            <fo:inline><fo:leader leader-length="0pt" />-
                            </fo:inline>
                        </fo:block>
                        <fo:table xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            language="EN-IN"
                            start-indent="0pt">
                            <fo:table-column
                                column-number="1"
                                column-width="306pt" />
                            <fo:table-column
                                column-number="2"
                                column-width="162pt" />
                            <fo:table-body
                                end-indent="0pt"
                                start-indent="0pt">
                                <fo:table-row>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="0pt"
                                                space-after.conditionality="retain">
                                                <fo:inline><fo:leader leader-length="0pt" />Loan
                                                    Account No
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-right-color="#000000"
                                        border-right-style="solid"
                                        border-right-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="0pt"
                                                space-after.conditionality="retain">
                                                <fo:inline>
                                                    <fo:leader leader-length="0pt" />
                                                    <xsl:value-of select="APPNO" />
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                </fo:table-row>
                                <fo:table-row>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="0pt"
                                                space-after.conditionality="retain">
                                                <fo:inline><fo:leader leader-length="0pt" />Loan
                                                    Sanction Amount
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-right-color="#000000"
                                        border-right-style="solid"
                                        border-right-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="10pt"
                                                space-after.conditionality="retain">
                                                <fo:inline>
                                                    <fo:leader leader-length="0pt" />
                                                </fo:inline>
                                                <fo:inline>
                                                    <fo:leader leader-length="0pt" />
                                                    <xsl:value-of select="SANCTION_AMT" />
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                </fo:table-row>
                                <fo:table-row>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="0pt"
                                                space-after.conditionality="retain">
                                                <fo:inline><fo:leader leader-length="0pt" />Loan
                                                    Amount Disbursed
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-right-color="#000000"
                                        border-right-style="solid"
                                        border-right-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="10pt"
                                                space-after.conditionality="retain">
                                                <fo:inline>
                                                    <fo:leader leader-length="0pt" />
                                                    <xsl:value-of select="DISBURSED_AMT" />
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                </fo:table-row>
                                <fo:table-row>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="0pt"
                                                space-after.conditionality="retain">
                                                <fo:inline><fo:leader leader-length="0pt" />Name of
                                                    the Authorized Dealer
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-right-color="#000000"
                                        border-right-style="solid"
                                        border-right-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="10pt"
                                                space-after.conditionality="retain">
                                                <fo:inline>
                                                    <fo:leader leader-length="0pt" />
                                                    <xsl:value-of select="DEALER_NAME" />
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                </fo:table-row>
                                <fo:table-row>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="0pt"
                                                space-after.conditionality="retain">
                                                <fo:inline><fo:leader leader-length="0pt" />The Type
                                                    Of Vehicle Hypothecated As Security In Our
                                                    Records
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-right-color="#000000"
                                        border-right-style="solid"
                                        border-right-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="10pt"
                                                space-after.conditionality="retain">
                                                <fo:inline>
                                                    <fo:leader leader-length="0pt" />
                                                    <xsl:value-of select="MAKE_MODEL" />
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                </fo:table-row>
                                <fo:table-row>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="0pt"
                                                space-after.conditionality="retain">
                                                <fo:inline><fo:leader leader-length="0pt" />Type of
                                                    Interest
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-right-color="#000000"
                                        border-right-style="solid"
                                        border-right-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="10pt"
                                                space-after.conditionality="retain">
                                                <fo:inline>
                                                    <fo:leader leader-length="0pt" />
                                                    <xsl:value-of select="INTEREST_TYPE" />
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                </fo:table-row>
                                <fo:table-row>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="0pt"
                                                space-after.conditionality="retain">
                                                <fo:inline><fo:leader leader-length="0pt" />Interest
                                                    Rate Applicable (p.a.)
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-right-color="#000000"
                                        border-right-style="solid"
                                        border-right-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="10pt"
                                                space-after.conditionality="retain">
                                                <fo:inline>
                                                    <fo:leader leader-length="0pt" />
                                                </fo:inline>
                                                <fo:inline>
                                                    <fo:leader leader-length="0pt" />
                                                    <xsl:value-of select="INTEREST_RATE" />
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                </fo:table-row>
                                <fo:table-row>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="0pt"
                                                space-after.conditionality="retain">
                                                <fo:inline><fo:leader leader-length="0pt" />Loan
                                                    Tenure(Months)
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-right-color="#000000"
                                        border-right-style="solid"
                                        border-right-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="10pt"
                                                space-after.conditionality="retain">
                                                <fo:inline>
                                                    <fo:leader leader-length="0pt" />
                                                    <xsl:value-of select="TENURE" />
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                </fo:table-row>
                                <fo:table-row>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="0pt"
                                                space-after.conditionality="retain">
                                                <fo:inline><fo:leader leader-length="0pt" />Equated
                                                    Monthly
                                                </fo:inline>
                                                <fo:inline>
                                                    <fo:leader leader-length="0pt" />
                                                    Instalment
                                                </fo:inline>
                                                <fo:inline>
                                                    <fo:leader leader-length="0pt" />
                                                    Amount (EMI)
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-right-color="#000000"
                                        border-right-style="solid"
                                        border-right-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="10pt"
                                                space-after.conditionality="retain">
                                                <fo:inline>
                                                    <fo:leader leader-length="0pt" />
                                                    <xsl:value-of select="EMI" />
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                </fo:table-row>
                                <fo:table-row>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="0pt"
                                                space-after.conditionality="retain">
                                                <fo:inline><fo:leader leader-length="0pt" />Date of
                                                    Commencement of EMI
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-right-color="#000000"
                                        border-right-style="solid"
                                        border-right-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="10pt"
                                                space-after.conditionality="retain">
                                                <fo:inline>
                                                    <fo:leader leader-length="0pt" />
                                                    <xsl:value-of select="DATE_COMMENCE" />
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                </fo:table-row>
                                <fo:table-row>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="0pt"
                                                space-after.conditionality="retain">
                                                <fo:inline>
                                                    <fo:leader leader-length="0pt" />
                                                    Repayment Cycle
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-right-color="#000000"
                                        border-right-style="solid"
                                        border-right-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="10pt"
                                                space-after.conditionality="retain">
                                                <fo:inline>
                                                    <fo:leader leader-length="0pt" />
                                                    <xsl:value-of select="REPAYMENT_CYCLE" />
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                </fo:table-row>
                                <fo:table-row>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="0pt"
                                                space-after.conditionality="retain">
                                                <fo:inline><fo:leader leader-length="0pt" />Number
                                                    of Advance EMI
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-right-color="#000000"
                                        border-right-style="solid"
                                        border-right-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="10pt"
                                                space-after.conditionality="retain">
                                                <fo:inline>
                                                    <fo:leader leader-length="0pt" />
                                                    <xsl:value-of select="NO_ADV_EMI" />
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                </fo:table-row>
                                <fo:table-row>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="0pt"
                                                space-after.conditionality="retain">
                                                <fo:inline><fo:leader leader-length="0pt" />Amount
                                                    of Advance EMI
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-right-color="#000000"
                                        border-right-style="solid"
                                        border-right-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="10pt"
                                                space-after.conditionality="retain">
                                                <fo:inline>
                                                    <fo:leader leader-length="0pt" />
                                                    <xsl:value-of select="AMT_ADV_EMI" />
                                                </fo:inline>
                                                <fo:inline>
                                                    <fo:leader leader-length="0pt" />
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                </fo:table-row>
                                <fo:table-row>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="0pt"
                                                space-after.conditionality="retain">
                                                <fo:inline>
                                                    <fo:leader leader-length="0pt" />
                                                    Processing Fee Deducted From Disbursal Amount
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-right-color="#000000"
                                        border-right-style="solid"
                                        border-right-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="10pt"
                                                space-after.conditionality="retain">
                                                <fo:inline>
                                                    <fo:leader leader-length="0pt" />
                                                    <xsl:value-of select="PROC_FEE" />
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                </fo:table-row>
                                <fo:table-row>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="0pt"
                                                space-after.conditionality="retain">
                                                <fo:inline><fo:leader leader-length="0pt" />Total
                                                    Deduction From Disbursal Amount
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-right-color="#000000"
                                        border-right-style="solid"
                                        border-right-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="10pt"
                                                space-after.conditionality="retain">
                                                <fo:inline>
                                                    <fo:leader leader-length="0pt" />
                                                    <xsl:value-of select="DEDU_FROM_DIS_AMT" />
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                </fo:table-row>
                                <fo:table-row>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="0pt"
                                                space-after.conditionality="retain">
                                                <fo:inline><fo:leader leader-length="0pt" />Net Loan
                                                    Amount Disbursed to the Dealer
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                    <fo:table-cell
                                        background-color="white"
                                        border-bottom-color="#000000"
                                        border-bottom-style="solid"
                                        border-bottom-width="0.25pt"
                                        border-left-color="#000000"
                                        border-left-style="solid"
                                        border-left-width="0.25pt"
                                        border-right-color="#000000"
                                        border-right-style="solid"
                                        border-right-width="0.25pt"
                                        border-top-color="#000000"
                                        border-top-style="solid"
                                        border-top-width="0.25pt"
                                        color="black"
                                        display-align="center"
                                        padding-bottom="0pt"
                                        padding-left="0pt"
                                        padding-right="0pt"
                                        padding-top="0pt">
                                        <fo:block-container>
                                            <fo:block
                                                font-family="Calibri"
                                                font-size="11pt"
                                                language="EN-US"
                                                line-height="1.3190500000000002"
                                                space-after="10pt"
                                                space-after.conditionality="retain">
                                                <fo:inline>
                                                    <fo:leader leader-length="0pt" />
                                                    <xsl:value-of select="NET_DISBURSED_AMT" />
                                                </fo:inline>
                                            </fo:block>
                                        </fo:block-container>
                                    </fo:table-cell>
                                </fo:table-row>
                            </fo:table-body>
                        </fo:table>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="10pt"
                            space-after.conditionality="retain">
                            <fo:inline><fo:leader leader-length="0pt" />Please ensure that the EMI
                                repayments are regular and made by the du
                            </fo:inline>
                            <fo:inline><fo:leader leader-length="0pt" />e date to avoid any late
                                payment charges.
                            </fo:inline>
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="10pt"
                            space-after.conditionality="retain">
                            <fo:inline><fo:leader leader-length="0pt" />In case you find any of the
                                above mentioned details to be incorrect, kindly contact us within 7
                                days of receipt of this letter. Please make a note of your Loan
                                Account Number and quote the same in any future corres
                            </fo:inline>
                            <fo:inline><fo:leader leader-length="0pt" />pondence with the Company.
                            </fo:inline>
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="10pt"
                            space-after.conditionality="retain">
                            <fo:inline><fo:leader leader-length="0pt" />To know more about the
                                product features and the list of services offered to you, please
                                visit company website.
                            </fo:inline>
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="10pt"
                            space-after.conditionality="retain">
                            <fo:leader />
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="10pt"
                            space-after.conditionality="retain">
                            <fo:inline><fo:leader leader-length="0pt" />Thank you for choosing
                                Orange Retail Finance India Private Limited as your company partner
                                and we look forward to
                            </fo:inline>
                            <fo:inline><fo:leader leader-length="0pt" />your continued patronage.
                            </fo:inline>
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="10pt"
                            space-after.conditionality="retain">
                            <fo:inline><fo:leader leader-length="0pt" />Sincerely,
                            </fo:inline>
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="10pt"
                            space-after.conditionality="retain">
                            <fo:leader />
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="10pt"
                            space-after.conditionality="retain">
                            <fo:inline><fo:leader leader-length="0pt" />Business Head – Retail
                                Assets and Small Business Lending
                            </fo:inline>
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="10pt"
                            space-after.conditionality="retain">
                            <fo:leader />
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            id="IDBM4ESJ4WW43GE2X32ENNGSLYDDGFDAIYHWPN2RGTNIIZ2C5XHFK" />
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            border-after-color="#00000A"
                            border-after-style="solid"
                            border-after-width="1.5pt"
                            border-after-width.conditionality="retain"
                            padding-after="1pt"
                            padding-after.conditionality="retain"
                            space-after="10pt"
                            space-after.conditionality="retain">
                            <fo:block
                                font-family="Calibri"
                                font-size="11pt"
                                language="EN-US"
                                line-height="1.3190500000000002"
                                space-after="0pt"
                                space-after.conditionality="retain"
                                space-before="0pt">
                                <fo:leader />
                            </fo:block>
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="10pt"
                            space-after.conditionality="retain">
                            <fo:leader />
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="0pt"
                            space-after.conditionality="retain"
                            text-align="center">
                            <fo:inline><fo:leader leader-length="0pt" />ORANGE RETAIL FINANCE INDIA
                                PRIVATE LIMITED
                            </fo:inline>
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="0pt"
                            space-after.conditionality="retain"
                            text-align="center">
                            <fo:inline><fo:leader leader-length="0pt" />NO 5, 5
                            </fo:inline>
                            <fo:inline
                                baseline-shift="super"
                                font-size="66.7%"><fo:leader leader-length="0pt" />TH
                            </fo:inline>
                            <fo:inline>
                                <fo:leader leader-length="0pt" />
                                MAIN ROAD, KASTHURIBAI NAGAR, ADYAR, CHENNAI – 600020.
                            </fo:inline>
                        </fo:block>
                        <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg"
                            font-family="Calibri"
                            font-size="11pt"
                            language="EN-US"
                            line-height="1.3190500000000002"
                            space-after="0pt"
                            space-after.conditionality="retain"
                            text-align="center">
                            <fo:inline><fo:leader leader-length="0pt" />Website :
                            </fo:inline>
                            <fo:inline>
                                <fo:leader leader-length="0pt" />
                                www.Orangeretailfinance.com
                            </fo:inline>
                        </fo:block>
                    </fo:block>
                    <fo:block id="IDEXRGR1CZWG1FF3OVBQS23HIXCI30SO04OAT0HLIM22GAU3CBKSAB" />
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
</xsl:stylesheet>
