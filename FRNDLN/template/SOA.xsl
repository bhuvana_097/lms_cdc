<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.1"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:barcode="org.krysalis.barcode4j.xalan.BarcodeExt" xmlns:common="http://exslt.org/common"
	xmlns:xalan="http://xml.apache.org" exclude-result-prefixes="barcode common xalan">
	<xsl:template match="SOA">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
	                <fo:layout-master-set>
                                <fo:simple-page-master master-name="simple"
        				page-width="8.268055555555555in" 
        				page-height="11.693055555555556in" 
        				margin-top="35.4pt" margin-bottom="35.4pt" 
        				margin-right="72pt" margin-left="72pt">
				<fo:region-body margin-top="36.6pt" margin-bottom="36.6pt" />
                                </fo:simple-page-master>
	                </fo:layout-master-set>
	                <fo:page-sequence master-reference="simple"  font-size="10pt">
                                <fo:flow flow-name="xsl-region-body">
                                	<fo:block text-align="center"  font-family="Calibri"  font-size="10pt"  font-weight="bold">
										Statement Of Accounts
									</fo:block>
									<fo:block><fo:leader/></fo:block>
                                        <fo:block font-family="Calibri" font-size="10pt" font-weight="normal">
                                                <fo:table>
		                                        <fo:table-column column-number="1" column-width="3cm" />
		                                        <fo:table-column column-number="2" column-width="0.5cm" />
		                                        <fo:table-column column-number="3" column-width="12cm" />
		                                        <fo:table-body height="10cm">
		                                        	<fo:table-row>
		                                        		<fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        Name
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        <xsl:value-of select="NAME" />
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                </fo:table-row> 
		                                        	<fo:table-row>
		                                        		<fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        Address
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        <xsl:value-of select="ADDRESS" />
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                </fo:table-row> 
		                                        	<fo:table-row>
		                                        		<fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        City
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        <xsl:value-of select="CITY" />
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                </fo:table-row> 
		                                        	<fo:table-row>
		                                        		<fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        State
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        <xsl:value-of select="STATE" />
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                </fo:table-row> 
		                                        	<fo:table-row>
		                                        		<fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        Pincode
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        <xsl:value-of select="PINCODE" />
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                </fo:table-row> 
		                                        	<fo:table-row>
		                                        		<fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        Mobile
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        <xsl:value-of select="MOBILE_NO" />
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                </fo:table-row> 
		                                        	<fo:table-row>
		                                        		<fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        Statement issue Dt
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        <xsl:value-of select="STMT_ISSUE_DATE" />
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                </fo:table-row> 
		                                        	<fo:table-row>
		                                        		<fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        Statement Period
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        <xsl:value-of select="STMT_PERIOD_FROM" /> to <xsl:value-of select="STMT_PERIOD_TO" />
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                </fo:table-row> 
		                                        	<fo:table-row>
		                                        		<fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        Loan Account No
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        <xsl:value-of select="LOANNO" />
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                </fo:table-row> 
		                                        	<fo:table-row>
		                                        		<fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        Name of Financier
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        Orange Retail Finance India Private Limited
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                </fo:table-row> 
		                                        	<fo:table-row>
		                                        		<fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        UCIC
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        <xsl:value-of select="UCIC" />
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                </fo:table-row> 
		                                        </fo:table-body>
		                        	</fo:table>
						<fo:block><fo:leader/></fo:block>
						<fo:block>
							<fo:block border-bottom-width="0.4pt" border-bottom-style="solid" margin-top="5mm"></fo:block>
		                                </fo:block>
		                                <fo:block><fo:leader/></fo:block>
						<fo:table>
		                                        <fo:table-column column-number="1" column-width="3.2cm" />
		                                        <fo:table-column column-number="2" column-width="0.2cm" />
		                                        <fo:table-column column-number="3" column-width="4.0cm" />
		                                        <fo:table-column column-number="4" column-width="1.2cm" />
		                                        <fo:table-column column-number="5" column-width="3cm" />
		                                        <fo:table-column column-number="6" column-width="0.2cm" />
		                                        <fo:table-column column-number="7" column-width="4.2cm" />
		                                        <fo:table-body height="10cm">
		                                        	<fo:table-row>
		                                        		<fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        Branch
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        <xsl:value-of select="BRANCH" />
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                        <fo:block>
		                                                               
		                                                        </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        Amount Financed
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        <xsl:value-of select="FINANCE_AMT" />
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                </fo:table-row> 
		                                                <fo:table-row>
		                                        		<fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        Product
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        <xsl:value-of select="PRODUCT" />
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                              
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        EMI Amount (Rs)
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                       <xsl:value-of select="EMI_AMT" />
		                                                                </fo:block>
		                                                        </fo:table-cell>

		                                                </fo:table-row> 
		                                                <fo:table-row>
		                                        		<fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        Model
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        <xsl:value-of select="MODEL" />
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                       
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        ROI
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                   	<xsl:value-of select="ROI" />    
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                </fo:table-row> 
		                                                <fo:table-row>
		                                        		<fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        Disbursal Date
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        <xsl:value-of select="DISB_DATE" />
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                       
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                      Total Tenure
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                       <xsl:value-of select="TENURE" />
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                </fo:table-row>
		                                                <fo:table-row>
		                                        		<fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        Installment start Date
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        <xsl:value-of select="INST_START_DATE" />
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                       
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                      Installment End Date
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                       <xsl:value-of select="INST_END_DATE" />
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                </fo:table-row>
		                                        </fo:table-body>
		                        	</fo:table>
						<fo:block><fo:leader/></fo:block>
						<fo:block>
							<fo:block border-bottom-width="0.4pt" border-bottom-style="solid" margin-top="5mm"></fo:block>
		                                </fo:block>
		                                <fo:block><fo:leader/></fo:block>
		                                <fo:table>
		                                        <fo:table-column column-number="1" column-width="2cm" />
		                                        <fo:table-column column-number="2" column-width="3.5cm" />
		                                        <fo:table-column column-number="3" column-width="2cm" />
		                                        <fo:table-column column-number="4" column-width="2cm" />
		                                        <fo:table-column column-number="5" column-width="2cm" />
		                                        <fo:table-column column-number="6" column-width="2cm" />
		                                        <fo:table-column column-number="7" column-width="2cm" />
		                                        <fo:table-body  height="10cm">
		                                        	<fo:table-row border="solid 0.1mm black" font-weight="bold">
		                                                       <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                               <fo:block>
		                                                                       Value Date
		                                                               </fo:block>
		                                                       </fo:table-cell>
		                                                       <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                               <fo:block>
		                                                                       Particulars
		                                                               </fo:block>
		                                                       </fo:table-cell>
		                                                       <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                               <fo:block>
		                                                                       Debit
		                                                               </fo:block>
		                                                       </fo:table-cell>
		                                                       <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                               <fo:block>
		                                                                       Credit
		                                                               </fo:block>
		                                                       </fo:table-cell>
		                                                       <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                               <fo:block>
		                                                                       Balance
		                                                               </fo:block>
		                                                       </fo:table-cell>
		                                                       <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                               <fo:block>
		                                                                       Book No
		                                                               </fo:block>
		                                                       </fo:table-cell>
		                                                       <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                               <fo:block>
		                                                                       Receipt No
		                                                               </fo:block>
		                                                       </fo:table-cell>
		                                               </fo:table-row>
		                                               <xsl:for-each select="./creditDebitScheduleList/SOACreditDebitScheduleModel">
		                                                       <fo:table-row border="solid 0.1mm black"  font-size="8pt">
		                                                               <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                                       <fo:block>
		                                                                               <xsl:value-of select="valueDate" />
		                                                                       </fo:block>
		                                                               </fo:table-cell>
		                                                               <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                                       <fo:block>
		                                                                               <xsl:value-of select="particulars" />
		                                                                       </fo:block>
		                                                               </fo:table-cell>
		                                                               <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                                       <fo:block>
		                                                                               <xsl:value-of select="debit" />
		                                                                       </fo:block>
		                                                               </fo:table-cell>
		                                                               <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                                       <fo:block>
		                                                                               <xsl:value-of select="credit" />
		                                                                       </fo:block>
		                                                               </fo:table-cell>
		                                                               <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                                       <fo:block>
		                                                                               <xsl:value-of select="balance" />
		                                                                       </fo:block>
		                                                               </fo:table-cell>
		                                                               <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                                       <fo:block>
		                                                                               <xsl:value-of select="bookNo" />
		                                                                       </fo:block>
		                                                               </fo:table-cell>
		                                                               <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                                       <fo:block>
		                                                                               <xsl:value-of select="receiptNo" />
		                                                                       </fo:block>
		                                                               </fo:table-cell>
		                                                       </fo:table-row>
		                                               </xsl:for-each>
		                                        </fo:table-body>
		                                </fo:table>
		                                
		                                <fo:block><fo:leader/></fo:block>
						<fo:footnote><fo:inline color="white">1</fo:inline><!--footnote number is not visible-->
							<fo:footnote-body>
							    <fo:block  text-align="center">
								This is computer generated statement hence signature is not required.
								<fo:block>
									<fo:block border-bottom-width="0.4pt" border-bottom-style="solid" margin-top="0.5mm"></fo:block>
						                </fo:block>
						                <fo:block><fo:leader/></fo:block>
							    </fo:block>
							    <fo:block  text-align="center"  font-family="Calibri" font-size="8pt">
								ORANGE RETAIL FINANCE INDIA PRIVATE LIMITED
							    </fo:block>
							    <fo:block  text-align="center"  font-family="Calibri" font-size="8pt">
								Please quote your TWO WHEELER LOAN Account Number whenever you contact us.
							    </fo:block>
							    <fo:block  text-align="center"  font-family="Calibri" font-size="8pt">
								At:NO 5, 5th Main Road, Kasthuribai Nagar, Adyar Chennai - 600020.
							    </fo:block>
							    <fo:block  text-align="center"  font-family="Calibri" font-size="8pt">
									Website : 
										<fo:basic-link color="#0000ff">
									           <xsl:attribute name="external-destination">
									             www.OrangeRetailFinance.com
									           </xsl:attribute>
									           www.OrangeRetailFinance.com
									      </fo:basic-link>
									 ,  Email ID: info@orangeretailfinance.com
							    </fo:block>
							    <fo:block  text-align="center"  font-family="Calibri" font-size="8pt">
								Registered Address: Orange Retail Finance India Private Ltd.No.5,5th Main Road, Kasthuribai Nagar, Adyar, Chennai - 600 020, Tamil Nadu, India. Phone : 044 - 4077 4077
							    </fo:block>
							</fo:footnote-body>
						</fo:footnote>
                                        </fo:block>
                                </fo:flow>
	                </fo:page-sequence>
		</fo:root>
	</xsl:template>
</xsl:stylesheet>
