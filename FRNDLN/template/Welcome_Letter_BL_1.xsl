<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="WELCOME_LETTER_BL">
        <fo:root font-family="Calibri"
            xmlns:fo="http://www.w3.org/1999/XSL/Format"
            xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml">
            <fo:layout-master-set
                xmlns:rx="http://www.renderx.com/XSL/Extensions"
                xmlns:o="urn:schemas-microsoft-com:office:office"
                xmlns:v="urn:schemas-microsoft-com:vml"
                xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint"
                xmlns:aml="http://schemas.microsoft.com/aml/2001/core"
                xmlns:w10="urn:schemas-microsoft-com:office:word"
                xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882">
                <fo:simple-page-master master-name="section1-first-page" page-width="8.5in" page-height="11in" margin-top="0pt" margin-bottom="0pt" margin-right="72pt" margin-left="72pt">
                    <fo:region-body margin-top="72pt" margin-bottom="72pt"></fo:region-body>
                    <fo:region-before region-name="first-page-header" extent="11in"></fo:region-before>
                    <fo:region-after region-name="first-page-footer" extent="11in" display-align="after"></fo:region-after>
                </fo:simple-page-master>
                <fo:simple-page-master master-name="section1-odd-page" page-width="8.5in" page-height="11in" margin-top="0pt" margin-bottom="0pt" margin-right="72pt" margin-left="72pt">
                    <fo:region-body margin-top="72pt" margin-bottom="72pt"></fo:region-body>
                    <fo:region-before region-name="odd-page-header" extent="11in"></fo:region-before>
                    <fo:region-after region-name="odd-page-footer" extent="11in" display-align="after"></fo:region-after>
                </fo:simple-page-master>
                <fo:simple-page-master master-name="section1-even-page" page-width="8.5in" page-height="11in" margin-top="0pt" margin-bottom="0pt" margin-right="72pt" margin-left="72pt">
                    <fo:region-body margin-top="72pt" margin-bottom="72pt"></fo:region-body>
                    <fo:region-before region-name="even-page-header" extent="11in"></fo:region-before>
                    <fo:region-after region-name="even-page-footer" extent="11in" display-align="after"></fo:region-after>
                </fo:simple-page-master>
                <fo:page-sequence-master master-name="section1-page-sequence-master">
                    <fo:repeatable-page-master-alternatives>
                        <fo:conditional-page-master-reference odd-or-even="odd" master-reference="section1-odd-page" />
                        <fo:conditional-page-master-reference odd-or-even="even" master-reference="section1-even-page" />
                    </fo:repeatable-page-master-alternatives>
                </fo:page-sequence-master>
            </fo:layout-master-set>
            <fo:page-sequence master-reference="section1-page-sequence-master" id="IDKS1XFE0B51NKH3MQHGAXS2AOL5NRGLLIUQTRCBBAVE22WTSTYZ" format="1"
                xmlns:rx="http://www.renderx.com/XSL/Extensions"
                xmlns:o="urn:schemas-microsoft-com:office:office"
                xmlns:v="urn:schemas-microsoft-com:vml"
                xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint"
                xmlns:aml="http://schemas.microsoft.com/aml/2001/core"
                xmlns:w10="urn:schemas-microsoft-com:office:word"
                xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882">
                <fo:static-content flow-name="first-page-header">
                    <fo:retrieve-marker retrieve-class-name="first-page-header" retrieve-position="first-including-carryover" retrieve-boundary="page" />
                </fo:static-content>
                <fo:static-content flow-name="first-page-footer">
                    <fo:retrieve-marker retrieve-class-name="first-page-footer" retrieve-position="first-including-carryover" retrieve-boundary="page" />
                </fo:static-content>
                <fo:static-content flow-name="odd-page-header">
                    <fo:retrieve-marker retrieve-class-name="odd-page-header" retrieve-position="first-including-carryover" retrieve-boundary="page" />
                </fo:static-content>
                <fo:static-content flow-name="odd-page-footer">
                    <fo:retrieve-marker retrieve-class-name="odd-page-footer" retrieve-position="first-including-carryover" retrieve-boundary="page" />
                </fo:static-content>
                <fo:static-content flow-name="even-page-header">
                    <fo:retrieve-marker retrieve-class-name="odd-page-header" retrieve-position="first-including-carryover" retrieve-boundary="page" />
                </fo:static-content>
                <fo:static-content flow-name="even-page-footer">
                    <fo:retrieve-marker retrieve-class-name="odd-page-footer" retrieve-position="first-including-carryover" retrieve-boundary="page" />
                </fo:static-content>
                <fo:static-content flow-name="xsl-footnote-separator">
                    <fo:block>
                        <fo:leader leader-pattern="rule" leader-length="144pt" rule-thickness="0.5pt" rule-style="solid" color="gray" />
                    </fo:block>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body">
                    <fo:block widows="2" orphans="2" font-size="10pt" line-height="1.147" white-space-collapse="false">
                        <fo:marker marker-class-name="first-page-header"
                            xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                            xmlns:svg="http://www.w3.org/2000/svg" />
                            <fo:marker marker-class-name="first-page-footer"
                                xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                xmlns:svg="http://www.w3.org/2000/svg" />
                                <fo:marker marker-class-name="odd-page-header"
                                    xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                    xmlns:svg="http://www.w3.org/2000/svg" />
                                    <fo:marker marker-class-name="odd-page-footer"
                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                        xmlns:svg="http://www.w3.org/2000/svg" />
                                        <fo:marker marker-class-name="even-page-header"
                                            xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                            xmlns:svg="http://www.w3.org/2000/svg" />
                                            <fo:marker marker-class-name="even-page-footer"
                                                xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                xmlns:svg="http://www.w3.org/2000/svg" />
                                                <fo:block id="IDBM4ESJ4WW43GE2X32ENNGSLYDDGFDAIYHWPN2RGTNIIZ2C5XHFK1"
                                                    xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                    xmlns:svg="http://www.w3.org/2000/svg" />
                                                    <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg">
                                                        <fo:leader />
                                                    </fo:block>
                                                    <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg">
                                                        <fo:inline>
                                                            <fo:leader leader-pattern="use-content" leader-length="35.25pt"></fo:leader>
                                                        </fo:inline>
                                                        <fo:inline>
                                                            <fo:leader leader-pattern="use-content" leader-length="35.25pt"></fo:leader>
                                                        </fo:inline>
                                                        <fo:inline>
                                                            <fo:leader leader-pattern="use-content" leader-length="35.25pt"></fo:leader>
                                                        </fo:inline>
                                                        <fo:inline>
                                                            <fo:leader leader-pattern="use-content" leader-length="35.25pt"></fo:leader>
                                                        </fo:inline>
                                                        <fo:inline>
                                                            <fo:leader leader-pattern="use-content" leader-length="35.25pt"></fo:leader>
                                                            <fo:leader leader-length="0pt" />WELCOME LETTER
                                                        </fo:inline>
                                                    </fo:block>
                                                    <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg">
                                                        <fo:leader />
                                                    </fo:block>
                                                    <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg">
                                                        <fo:leader />
                                                    </fo:block>
                                                    <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg">
                                                        <fo:inline>
                                                            <fo:leader leader-length="0pt" />Date : 
                                                            <xsl:value-of select="LETTER_DATE" />
                                                        </fo:inline>
                                                    </fo:block>
                                                    <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg">
                                                        <fo:inline>
                                                            <fo:leader leader-length="0pt" />
                                                            <xsl:value-of select="NAME" />
                                                        </fo:inline>
                                                    </fo:block>
                                                    <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg">
                                                        <fo:inline>
                                                            <fo:leader leader-length="0pt" />
                                                            <xsl:value-of select="ADDRESS" />
                                                        </fo:inline>
                                                    </fo:block>
                                                    <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg">
                                                        <fo:inline>
                                                            <fo:leader leader-length="0pt" />
                                                            <xsl:value-of select="DISTRICT" />
                                                        </fo:inline>
                                                    </fo:block>
                                                    <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg">
                                                        <fo:inline>
                                                            <fo:leader leader-length="0pt" />
                                                            <xsl:value-of select="STATE" /> – 
                                                            <xsl:value-of select="PINCODE" />
                                                        </fo:inline>
                                                    </fo:block>
                                                    <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg">
                                                        <fo:inline>
                                                            <fo:leader leader-length="0pt" />Mobile No 
                                                        </fo:inline>
                                                        <fo:inline>
                                                            <fo:leader leader-pattern="use-content" leader-length="35.25pt"></fo:leader>
                                                            <fo:leader leader-length="0pt" />  : 
                                                            <xsl:value-of select="MOBILE" />
                                                        </fo:inline>
                                                    </fo:block>
                                                    <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg">
                                                        <fo:inline>
                                                            <fo:leader leader-length="0pt" />Application No : 
                                                            <xsl:value-of select="APPNO" />
                                                        </fo:inline>
                                                    </fo:block>
                                                    <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg">
                                                        <fo:leader />
                                                    </fo:block>
                                                    <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg">
                                                        <fo:inline>
                                                            <fo:leader leader-length="0pt" />Dear Customer, 
                                                        </fo:inline>
                                                    </fo:block>
                                                    <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg">
                                                        <fo:inline>
                                                            <fo:leader leader-length="0pt" />We are delighted to welcome you and are privileged to have you as our customer. Your Two Wheeler Loan has been disbursed on 
                                                            <xsl:value-of select="DISBURSED_DATE" />.
                                                        </fo:inline>
                                                    </fo:block>
                                                    <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg">
                                                        <fo:inline>
                                                            <fo:leader leader-length="0pt" />The important terms of your loan are given 
                                                        </fo:inline>
                                                        <fo:inline>
                                                            <fo:leader leader-length="0pt" />below :
                                                        </fo:inline>
                                                        <fo:inline>
                                                            <fo:leader leader-length="0pt" />-
                                                        </fo:inline>
                                                    </fo:block>
                                                    <fo:table font-family="Calibri" language="EN-IN" start-indent="0pt"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg">
                                                        <fo:table-column column-number="1" column-width="306pt" />
                                                        <fo:table-column column-number="2" column-width="162pt" />
                                                        <fo:table-body start-indent="0pt" end-indent="0pt">
                                                            <fo:table-row>
                                                                <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" background-color="white" color="black" display-align="center">
                                                                    <fo:block-container>
                                                                        <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />Loan Account No
                                                                            </fo:inline>
                                                                        </fo:block>
                                                                    </fo:block-container>
                                                                </fo:table-cell>
                                                                <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-right-style="solid" border-right-color="#000000" border-right-width="0.25pt" background-color="white" color="black" display-align="center">
                                                                    <fo:block-container>
                                                                        <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />
                                                                                <xsl:value-of select="LOAN_ACCOUNT_NUMBER" />
                                                                            </fo:inline>
                                                                        </fo:block>
                                                                    </fo:block-container>
                                                                </fo:table-cell>
                                                            </fo:table-row>
                                                            <fo:table-row>
                                                                <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" background-color="white" color="black" display-align="center">
                                                                    <fo:block-container>
                                                                        <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />Loan Sanction Amount
                                                                            </fo:inline>
                                                                        </fo:block>
                                                                    </fo:block-container>
                                                                </fo:table-cell>
                                                                <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-right-style="solid" border-right-color="#000000" border-right-width="0.25pt" background-color="white" color="black" display-align="center">
                                                                    <fo:block-container>
                                                                        <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />
                                                                            </fo:inline>
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />
                                                                                <xsl:value-of select="LOAN_AMOUNT" />
                                                                            </fo:inline>
                                                                        </fo:block>
                                                                    </fo:block-container>
                                                                </fo:table-cell>
                                                            </fo:table-row>
                                                            <fo:table-row>
                                                                <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" background-color="white" color="black" display-align="center">
                                                                    <fo:block-container>
                                                                        <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />Loan Amount Disbursed
                                                                            </fo:inline>
                                                                        </fo:block>
                                                                    </fo:block-container>
                                                                </fo:table-cell>
                                                                <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-right-style="solid" border-right-color="#000000" border-right-width="0.25pt" background-color="white" color="black" display-align="center">
                                                                    <fo:block-container>
                                                                        <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />
                                                                                <xsl:value-of select="DISBURSAL_AMOUNT" />
                                                                            </fo:inline>
                                                                        </fo:block>
                                                                    </fo:block-container>
                                                                </fo:table-cell>
                                                            </fo:table-row>
                                                             <fo:table-row>
                                                                <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" background-color="white" color="black" display-align="center">
                                                                    <fo:block-container>
                                                                        <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />The Type Of Vehicle Hypothecated As Security In Our Records
                                                                            </fo:inline>
                                                                        </fo:block>
                                                                    </fo:block-container>
                                                                </fo:table-cell>
                                                              </fo:table-row>
                                                            <fo:table-row>
                                                                <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" background-color="white" color="black" display-align="center">
                                                                    <fo:block-container>
                                                                        <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />Rate of Interest
                                                                            </fo:inline>
                                                                        </fo:block>
                                                                    </fo:block-container>
                                                                </fo:table-cell>
                                                                <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-right-style="solid" border-right-color="#000000" border-right-width="0.25pt" background-color="white" color="black" display-align="center">
                                                                    <fo:block-container>
                                                                        <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />
                                                                                <xsl:value-of select="EMI_AMOUNT" />
                                                                            </fo:inline>
                                                                        </fo:block>
                                                                    </fo:block-container>
                                                                </fo:table-cell>
                                                            </fo:table-row>
                                                            <fo:table-row>
                                                                <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" background-color="white" color="black" display-align="center">
                                                                    <fo:block-container>
                                                                        <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />EMI AMOUNT
                                                                            </fo:inline>
                                                                        </fo:block>
                                                                    </fo:block-container>
                                                                </fo:table-cell>
                                                                <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-right-style="solid" border-right-color="#000000" border-right-width="0.25pt" background-color="white" color="black" display-align="center">
                                                                    <fo:block-container>
                                                                        <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />
                                                                            </fo:inline>
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />
                                                                                <xsl:value-of select="EMI_AMOUNT" />
                                                                            </fo:inline>
                                                                        </fo:block>
                                                                    </fo:block-container>
                                                                </fo:table-cell>
                                                            </fo:table-row>
															    <fo:table-row>
                             <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" background-color="white" color="black" display-align="center">
                                 <fo:block-container>
                                     <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                         <fo:inline>
                                             <fo:leader leader-length="0pt" />EMI START DATE
                                         </fo:inline>
                                    </fo:block>
                                </fo:block-container>
                            </fo:table-cell>
                            <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-right-style="solid" border-right-color="#000000" border-right-width="0.25pt" background-color="white" color="black" display-align="center">
                                <fo:block-container>
                                    <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                        <fo:inline>
                                            <fo:leader leader-length="0pt" />
                                        </fo:inline>
                                        <fo:inline>
                                            <fo:leader leader-length="0pt" />
                                            <xsl:value-of select="EMI_Start_Date" />
                                      </fo:inline>
                                                                     </fo:block>
                                                                 </fo:block-container>
                                                             </fo:table-cell>
                                                          </fo:table-row>
  <fo:table-row>
           <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" background-color="white" color="black" display-align="center">
                 <fo:block-container>
                     <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                         <fo:inline>
                             <fo:leader leader-length="0pt" />Maturity DATE
                         </fo:inline>
                     </fo:block>
                 </fo:block-container>
             </fo:table-cell>
             <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-right-style="solid" border-right-color="#000000" border-right-width="0.25pt" background-color="white" color="black" display-align="center">
                 <fo:block-container>
                     <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                         <fo:inline>
                             <fo:leader leader-length="0pt" />
                         </fo:inline>
                         <fo:inline>
                             <fo:leader leader-length="0pt" />
                             <xsl:value-of select="EMI_End_Date" />
                         </fo:inline>
                     </fo:block>
                 </fo:block-container>
             </fo:table-cell>
         </fo:table-row>
                                                            <fo:table-row>
                                                                <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" background-color="white" color="black" display-align="center">
                                                                    <fo:block-container>
                                                                        <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />Loan Tenure(Months) 
                                                                            </fo:inline>
                                                                        </fo:block>
                                                                    </fo:block-container>
                                                                </fo:table-cell>
                                                                <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-right-style="solid" border-right-color="#000000" border-right-width="0.25pt" background-color="white" color="black" display-align="center">
                                                                    <fo:block-container>
                                                                        <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />
                                                                                <xsl:value-of select="TENURE" />
                                                                            </fo:inline>
                                                                        </fo:block>
                                                                    </fo:block-container>
                                                                </fo:table-cell>
                                                            </fo:table-row>
                                                      <fo:table-row>
                                                                <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" background-color="white" color="black" display-align="center">
                                                                    <fo:block-container>
                                                                        <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />Date of Commencement of EMI
                                                                            </fo:inline>
                                                                        </fo:block>
                                                                    </fo:block-container>
                                                                </fo:table-cell>
                                                                <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-right-style="solid" border-right-color="#000000" border-right-width="0.25pt" background-color="white" color="black" display-align="center">
                                                                    <fo:block-container>
                                                                        <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />
                                                                                <xsl:value-of select="EMI_Start_Date" />
                                                                            </fo:inline>
                                                                        </fo:block>
                                                                    </fo:block-container>
                                                                </fo:table-cell>
                                                            </fo:table-row>
                                                            <fo:table-row>
                                                                <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" background-color="white" color="black" display-align="center">
                                                                    <fo:block-container>
                                                                        <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />Repayment Cycle
                                                                            </fo:inline>
                                                                        </fo:block>
                                                                    </fo:block-container>
                                                                </fo:table-cell>
                                                                <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-right-style="solid" border-right-color="#000000" border-right-width="0.25pt" background-color="white" color="black" display-align="center">
                                                                    <fo:block-container>
                                                                        <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />
                                                                                <xsl:value-of select="REPAYMENT_CYCLE" />
                                                                            </fo:inline>
                                                                        </fo:block>
                                                                    </fo:block-container>
                                                                </fo:table-cell>
                                                            </fo:table-row>
                                                            <fo:table-row>
                                                                <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" background-color="white" color="black" display-align="center">
                                                                    <fo:block-container>
                                                                        <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />LOAN DOCUMENT CHARGES
                                                                            </fo:inline>
                                                                        </fo:block>
                                                                    </fo:block-container>
                                                                </fo:table-cell>
                                                                <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-right-style="solid" border-right-color="#000000" border-right-width="0.25pt" background-color="white" color="black" display-align="center">
                                                                    <fo:block-container>
                                                                        <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />
                                                                                <xsl:value-of select="LOAN_DOCUMENT_CHARGES" />
                                                                            </fo:inline>
                                                                        </fo:block>
                                                                    </fo:block-container>
                                                                </fo:table-cell>
                                                            </fo:table-row>
                                                            <fo:table-row>
                                                                <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" background-color="white" color="black" display-align="center">
                                                                    <fo:block-container>
                                                                        <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />LOAN MANAGEMENT CHARGES
                                                                            </fo:inline>
                                                                        </fo:block>
                                                                    </fo:block-container>
                                                                </fo:table-cell>
                                                                <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-right-style="solid" border-right-color="#000000" border-right-width="0.25pt" background-color="white" color="black" display-align="center">
                                                                    <fo:block-container>
                                                                        <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />
                                                                                <xsl:value-of select="LOAN_MANAGEMENT_CHARGES" />
                                                                            </fo:inline>
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />
                                                                            </fo:inline>
                                                                        </fo:block>
                                                                    </fo:block-container>
                                                                </fo:table-cell>
                                                            </fo:table-row>
                                                            <fo:table-row>
                                                                <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" background-color="white" color="black" display-align="center">
                                                                    <fo:block-container>
                                                                        <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />Processing Fee Deducted From Disbursal Amount
                                                                            </fo:inline>
                                                                        </fo:block>
                                                                    </fo:block-container>
                                                                </fo:table-cell>
                                                                <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-right-style="solid" border-right-color="#000000" border-right-width="0.25pt" background-color="white" color="black" display-align="center">
                                                                    <fo:block-container>
                                                                        <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />
                                                                                <xsl:value-of select="PROCESSING_FEE" />
                                                                            </fo:inline>
                                                                        </fo:block>
                                                                    </fo:block-container>
                                                                </fo:table-cell>
                                                            </fo:table-row>
                                                            <fo:table-row>
                                                                <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" background-color="white" color="black" display-align="center">
                                                                    <fo:block-container>
                                                                        <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />INSURANCE
                                                                            </fo:inline>
                                                                        </fo:block>
                                                                    </fo:block-container>
                                                                </fo:table-cell>
                                                                <fo:table-cell padding-top="0pt" padding-left="0pt" padding-bottom="0pt" padding-right="0pt" border-bottom-style="solid" border-bottom-color="#000000" border-bottom-width="0.25pt" border-top-style="solid" border-top-color="#000000" border-top-width="0.25pt" border-left-style="solid" border-left-color="#000000" border-left-width="0.25pt" border-right-style="solid" border-right-color="#000000" border-right-width="0.25pt" background-color="white" color="black" display-align="center">
                                                                    <fo:block-container>
                                                                        <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US">
                                                                            <fo:inline>
                                                                                <fo:leader leader-length="0pt" />
                                                                                <xsl:value-of select="INSURANCE" />
                                                                            </fo:inline>
                                                                        </fo:block>
                                                                    </fo:block-container>
                                                                </fo:table-cell>
                                                            </fo:table-row>
                                                     
                                                        </fo:table-body>
                                                    </fo:table>
                                                    <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg">
                                                        <fo:inline>
                                                            <fo:leader leader-length="0pt" />Please ensure that the EMI repayments are regular and made by the du
                                                        </fo:inline>
                                                        <fo:inline>
                                                            <fo:leader leader-length="0pt" />e date to avoid any late payment charges.
                                                        </fo:inline>
                                                    </fo:block>
                                                    <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg">
                                                        <fo:inline>
                                                            <fo:leader leader-length="0pt" />In case you find any of the above mentioned details to be incorrect, kindly contact us within 7 days of receipt of this letter. Please make a note of your Loan Account Number and quote the same in any future corres
                                                        </fo:inline>
                                                        <fo:inline>
                                                            <fo:leader leader-length="0pt" />pondence with the Company.
                                                        </fo:inline>
                                                    </fo:block>
                                                    <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg">
                                                        <fo:inline>
                                                            <fo:leader leader-length="0pt" />To know more about the product features and the list of services offered to you, please visit company website.
                                                        </fo:inline>
                                                    </fo:block>
                                                    <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg">
                                                        <fo:leader />
                                                    </fo:block>
                                                    <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg">
                                                        <fo:inline>
                                                            <fo:leader leader-length="0pt" />Thank you for choosing Orange Retail Finance India Private Limited as your company partner and we look forward to 
                                                        </fo:inline>
                                                        <fo:inline>
                                                            <fo:leader leader-length="0pt" />your continued patronage.
                                                        </fo:inline>
                                                    </fo:block>
                                                    <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg">
                                                        <fo:inline>
                                                            <fo:leader leader-length="0pt" />Sincerely,
                                                        </fo:inline>
                                                    </fo:block>
                                                    <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg">
                                                        <fo:leader />
                                                    </fo:block>
                                                    <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg">
                                                        <fo:inline>
                                                            <fo:leader leader-length="0pt" />Business Head – Retail Assets and Small Business Lending 
                                                        </fo:inline>
                                                    </fo:block>
                                                    <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg">
                                                        <fo:leader />
                                                    </fo:block>
                                                    <fo:block id="IDBM4ESJ4WW43GE2X32ENNGSLYDDGFDAIYHWPN2RGTNIIZ2C5XHFK"
                                                        xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                        xmlns:svg="http://www.w3.org/2000/svg" />
                                                        <fo:block border-after-style="solid" border-after-color="#00000A" border-after-width="1.5pt" border-after-width.conditionality="retain" padding-after="1pt" padding-after.conditionality="retain" space-after="10pt" space-after.conditionality="retain"
                                                            xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                            xmlns:svg="http://www.w3.org/2000/svg">
                                                            <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US" space-before="0pt">
                                                                <fo:leader />
                                                            </fo:block>
                                                        </fo:block>
                                                        <fo:block space-after="10pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US"
                                                            xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                            xmlns:svg="http://www.w3.org/2000/svg">
                                                            <fo:leader />
                                                        </fo:block>
                                                        <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US" text-align="center"
                                                            xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                            xmlns:svg="http://www.w3.org/2000/svg">
                                                            <fo:inline>
                                                                <fo:leader leader-length="0pt" />ORANGE RETAIL FINANCE INDIA PRIVATE LIMITED
                                                            </fo:inline>
                                                        </fo:block>
                                                        <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US" text-align="center"
                                                            xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                            xmlns:svg="http://www.w3.org/2000/svg">
                                                            <fo:inline>
                                                                <fo:leader leader-length="0pt" />NO 5, 5
                                                            </fo:inline>
                                                            <fo:inline baseline-shift="super" font-size="66.7%">
                                                                <fo:leader leader-length="0pt" />TH
                                                            </fo:inline>
                                                            <fo:inline>
                                                                <fo:leader leader-length="0pt" /> MAIN ROAD, KASTHURIBAI NAGAR, ADYAR, CHENNAI – 600020.
                                                            </fo:inline>
                                                        </fo:block>
                                                        <fo:block space-after="0pt" space-after.conditionality="retain" line-height="1.3190500000000002" font-family="Calibri" font-size="11pt" language="EN-US" text-align="center"
                                                            xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
                                                            xmlns:svg="http://www.w3.org/2000/svg">
                                                            <fo:inline>
                                                                <fo:leader leader-length="0pt" />Website :
                                                            </fo:inline>
                                                            <fo:inline>
                                                                <fo:leader leader-length="0pt" /> www.Orangeretailfinance.com
                                                            </fo:inline>
                                                        </fo:block>
                                                    </fo:block>
                                                    <fo:block id="IDEXRGR1CZWG1FF3OVBQS23HIXCI30SO04OAT0HLIM22GAU3CBKSAB" />
                                                </fo:flow>
                                            </fo:page-sequence>
                                        </fo:root>
                                    </xsl:template>
                                </xsl:stylesheet>