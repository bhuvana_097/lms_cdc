<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.1"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:barcode="org.krysalis.barcode4j.xalan.BarcodeExt" xmlns:common="http://exslt.org/common"
	xmlns:xalan="http://xml.apache.org" exclude-result-prefixes="barcode common xalan">
	<xsl:template match="REPAYMENT_SCHEDULE">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
	                <fo:layout-master-set>
                                <fo:simple-page-master master-name="simple"
        				page-width="8.268055555555555in" 
        				page-height="11.693055555555556in" 
        				margin-top="35.4pt" margin-bottom="35.4pt" 
        				margin-right="72pt" margin-left="72pt">
				<fo:region-body margin-top="36.6pt" margin-bottom="36.6pt" />
                                </fo:simple-page-master>
	                </fo:layout-master-set>
	                <fo:page-sequence master-reference="simple"  font-size="10pt">
                                <fo:flow flow-name="xsl-region-body">
					<fo:block text-align="center"  font-family="Calibri"  font-size="10pt"  font-weight="bold">
						Repayment schedule 
					</fo:block>
					<fo:block><fo:leader/></fo:block>
                                        <fo:block font-family="Calibri" font-size="10pt" font-weight="normal">
                                                <fo:block text-align="right">
							Date :<xsl:value-of select="DATE" />
						</fo:block>
						<fo:block><fo:leader/></fo:block>
						
						<fo:table>
		                                        <fo:table-column column-number="1" column-width="3cm" />
		                                        <fo:table-column column-number="2" column-width="0.2cm" />
		                                        <fo:table-column column-number="3" column-width="4.2cm" />
		                                        <fo:table-column column-number="4" column-width="1.2cm" />
		                                        <fo:table-column column-number="5" column-width="3cm" />
		                                        <fo:table-column column-number="6" column-width="0.2cm" />
		                                        <fo:table-column column-number="7" column-width="4.2cm" />
		                                        <fo:table-body height="10cm">
		                                        	<fo:table-row>
		                                        		<fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        Agreement No
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        <xsl:value-of select="AGGRE_NO" />
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                        <fo:block>
		                                                               
		                                                        </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        UCCI
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        <xsl:value-of select="UCIC" />
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                </fo:table-row> 
		                                                <fo:table-row>
		                                        		<fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        Customer
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        <xsl:value-of select="NAME" />
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                               
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                       
		                                                                </fo:block>
		                                                        </fo:table-cell>

		                                                </fo:table-row> 
		                                                <fo:table-row>
		                                        		<fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        Tenure
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        <xsl:value-of select="TENURE" />
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                       
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        Loan Type
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                   	<xsl:value-of select="LOAN_TYPE" />    
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                </fo:table-row> 
		                                                <fo:table-row>
		                                        		<fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        Total Instl
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        <xsl:value-of select="TOTAL_INSTL" />
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                       
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                      Amount Financed  
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                        :
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                        <fo:table-cell text-align="left">
		                                                                <fo:block>
		                                                                       <xsl:value-of select="FINANCE_AMT" />
		                                                                </fo:block>
		                                                        </fo:table-cell>
		                                                </fo:table-row> 
		                                        </fo:table-body>
		                        	</fo:table>
		                                		
		                        	<fo:block>
							<fo:block border-bottom-width="0.4pt" border-bottom-style="solid" margin-top="5mm"></fo:block>
		                                </fo:block>
		                                <fo:block><fo:leader/></fo:block>
		                                <fo:table>
		                                        <fo:table-column column-number="1" column-width="1.3cm" />
		                                        <fo:table-column column-number="2" column-width="3cm" />
		                                        <fo:table-column column-number="3" column-width="2.6cm" />
		                                        <fo:table-column column-number="4" column-width="3cm" />
		                                        <fo:table-column column-number="5" column-width="3cm" />
		                                        <fo:table-column column-number="6" column-width="3cm" />
		                                        <fo:table-body  height="10cm">
		                                        	<fo:table-row border="solid 0.1mm black" font-weight="bold">
		                                                       <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                               <fo:block>
		                                                                       Due No
		                                                               </fo:block>
		                                                       </fo:table-cell>
		                                                       <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                               <fo:block>
		                                                                       Due Date
		                                                               </fo:block>
		                                                       </fo:table-cell>
		                                                       <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                               <fo:block>
		                                                                       Due AMT
		                                                               </fo:block>
		                                                       </fo:table-cell>
		                                                       <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                               <fo:block>
		                                                                       Principal
		                                                               </fo:block>
		                                                       </fo:table-cell>
		                                                       <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                               <fo:block>
		                                                                       Interest
		                                                               </fo:block>
		                                                       </fo:table-cell>
		                                                       <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                               <fo:block>
		                                                                       o/s Principal
		                                                               </fo:block>
		                                                       </fo:table-cell>
														   <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                               <fo:block>
		                                                                       PostMorat-Principal
		                                                               </fo:block>
		                                                       </fo:table-cell>
		                                               </fo:table-row>
		                                               <xsl:for-each select="./repayScheduleList/RepaymentScheduleModel">
		                                                       <fo:table-row border="solid 0.1mm black"  font-size="8pt">
		                                                               <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                                       <fo:block>
		                                                                               <xsl:value-of select="dueNo" />
		                                                                       </fo:block>
		                                                               </fo:table-cell>
		                                                               <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                                       <fo:block>
		                                                                               <xsl:value-of select="dueDate" />
		                                                                       </fo:block>
		                                                               </fo:table-cell>
		                                                               <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                                       <fo:block>
		                                                                               <xsl:value-of select="dueAmt" />
		                                                                       </fo:block>
		                                                               </fo:table-cell>
		                                                               <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                                       <fo:block>
		                                                                               <xsl:value-of select="principal" />
		                                                                       </fo:block>
		                                                               </fo:table-cell>
		                                                               <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                                       <fo:block>
		                                                                               <xsl:value-of select="interest" />
		                                                                       </fo:block>
		                                                               </fo:table-cell>
		                                                               <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                                       <fo:block>
		                                                                               <xsl:value-of select="osPrincipal" />
		                                                                       </fo:block>
		                                                               </fo:table-cell>
		                                                       </fo:table-row>
		                                               </xsl:for-each>
		                                               <fo:table-row border="solid 0.1mm black" font-weight="bold">
		                                                       <fo:table-cell text-align="center">
		                                                               <fo:block>
		                                                                       
		                                                               </fo:block>
		                                                       </fo:table-cell>
		                                                       <fo:table-cell text-align="left">
		                                                               <fo:block>
		                                                                       TOTAL
		                                                               </fo:block>
		                                                       </fo:table-cell>
		                                                       <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                               <fo:block>
		                                                                       <xsl:value-of select="TOT_DUE" />
		                                                               </fo:block>
		                                                       </fo:table-cell>
		                                                       <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                               <fo:block>
		                                                                       <xsl:value-of select="TOT_PRINCIPAL" />
		                                                               </fo:block>
		                                                       </fo:table-cell>
		                                                       <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                               <fo:block>
		                                                                       
		                                                               </fo:block>
		                                                       </fo:table-cell>
		                                                       <fo:table-cell border="solid 0.1mm black" text-align="center">
		                                                               <fo:block>
		                                                                       <xsl:value-of select="TOT_OS_PRINCIPAL" />
		                                                               </fo:block>
		                                                       </fo:table-cell>
		                                               </fo:table-row>
		                                        </fo:table-body>
		                                </fo:table>
                                        </fo:block>
                                </fo:flow>
	                </fo:page-sequence>
		</fo:root>
	</xsl:template>
</xsl:stylesheet>