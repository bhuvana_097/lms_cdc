
package com.vg.lms.incred.stat;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "TRANSACTION_ID",
    "APPLICATION_ID",
    "STATUS",
    "EPOCH"
})
public class Response {

    @JsonProperty("TRANSACTION_ID")
    private String tRANSACTIONID;
    @JsonProperty("APPLICATION_ID")
    private String aPPLICATIONID;
    @JsonProperty("STATUS")
    private String sTATUS;
    @JsonProperty("EPOCH")
    private Integer ePOCH;

    @JsonProperty("TRANSACTION_ID")
    public String getTRANSACTIONID() {
        return tRANSACTIONID;
    }

    @JsonProperty("TRANSACTION_ID")
    public void setTRANSACTIONID(String tRANSACTIONID) {
        this.tRANSACTIONID = tRANSACTIONID;
    }

    @JsonProperty("APPLICATION_ID")
    public String getAPPLICATIONID() {
        return aPPLICATIONID;
    }

    @JsonProperty("APPLICATION_ID")
    public void setAPPLICATIONID(String aPPLICATIONID) {
        this.aPPLICATIONID = aPPLICATIONID;
    }

    @JsonProperty("STATUS")
    public String getSTATUS() {
        return sTATUS;
    }

    @JsonProperty("STATUS")
    public void setSTATUS(String sTATUS) {
        this.sTATUS = sTATUS;
    }

    @JsonProperty("EPOCH")
    public Integer getEPOCH() {
        return ePOCH;
    }

    @JsonProperty("EPOCH")
    public void setEPOCH(Integer ePOCH) {
        this.ePOCH = ePOCH;
    }

}
