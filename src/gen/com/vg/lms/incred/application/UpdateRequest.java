
package com.vg.lms.incred.application;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "APPLICATION_ID",
    "LOAN_AMOUNT",
    "LOAN_TENURE",
    "LOAN_SCHEME",
    "EXISTING_EMI",
    "REFERENCE"
})
public class UpdateRequest {

    @JsonProperty("APPLICATION_ID")
    private String aPPLICATIONID;
    @JsonProperty("LOAN_AMOUNT")
    private Integer lOANAMOUNT;
    @JsonProperty("LOAN_TENURE")
    private Integer lOANTENURE;
    @JsonProperty("LOAN_SCHEME")
    private String lOANSCHEME;
    @JsonProperty("EXISTING_EMI")
    private Integer eXISTINGEMI;
    @JsonProperty("REFERENCE")
    private List<REFERENCE> rEFERENCE = null;

    @JsonProperty("APPLICATION_ID")
    public String getAPPLICATIONID() {
        return aPPLICATIONID;
    }

    @JsonProperty("APPLICATION_ID")
    public void setAPPLICATIONID(String aPPLICATIONID) {
        this.aPPLICATIONID = aPPLICATIONID;
    }

    @JsonProperty("LOAN_AMOUNT")
    public Integer getLOANAMOUNT() {
        return lOANAMOUNT;
    }

    @JsonProperty("LOAN_AMOUNT")
    public void setLOANAMOUNT(Integer lOANAMOUNT) {
        this.lOANAMOUNT = lOANAMOUNT;
    }

    @JsonProperty("LOAN_TENURE")
    public Integer getLOANTENURE() {
        return lOANTENURE;
    }

    @JsonProperty("LOAN_TENURE")
    public void setLOANTENURE(Integer lOANTENURE) {
        this.lOANTENURE = lOANTENURE;
    }

    @JsonProperty("LOAN_SCHEME")
    public String getLOANSCHEME() {
        return lOANSCHEME;
    }

    @JsonProperty("LOAN_SCHEME")
    public void setLOANSCHEME(String lOANSCHEME) {
        this.lOANSCHEME = lOANSCHEME;
    }

    @JsonProperty("EXISTING_EMI")
    public Integer getEXISTINGEMI() {
        return eXISTINGEMI;
    }

    @JsonProperty("EXISTING_EMI")
    public void setEXISTINGEMI(Integer eXISTINGEMI) {
        this.eXISTINGEMI = eXISTINGEMI;
    }

    @JsonProperty("REFERENCE")
    public List<REFERENCE> getREFERENCE() {
        return rEFERENCE;
    }

    @JsonProperty("REFERENCE")
    public void setREFERENCE(List<REFERENCE> rEFERENCE) {
        this.rEFERENCE = rEFERENCE;
    }

}
