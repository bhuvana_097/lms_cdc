
package com.vg.lms.incred.application;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "NAME",
    "PHONE",
    "RELATION_TYPE"
})
public class REFERENCE {

    @JsonProperty("NAME")
    private String nAME;
    @JsonProperty("PHONE")
    private String pHONE;
    @JsonProperty("RELATION_TYPE")
    private String rELATIONTYPE;

    @JsonProperty("NAME")
    public String getNAME() {
        return nAME;
    }

    @JsonProperty("NAME")
    public void setNAME(String nAME) {
        this.nAME = nAME;
    }

    @JsonProperty("PHONE")
    public String getPHONE() {
        return pHONE;
    }

    @JsonProperty("PHONE")
    public void setPHONE(String pHONE) {
        this.pHONE = pHONE;
    }

    @JsonProperty("RELATION_TYPE")
    public String getRELATIONTYPE() {
        return rELATIONTYPE;
    }

    @JsonProperty("RELATION_TYPE")
    public void setRELATIONTYPE(String rELATIONTYPE) {
        this.rELATIONTYPE = rELATIONTYPE;
    }

}
