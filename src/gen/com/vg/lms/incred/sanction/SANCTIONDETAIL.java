
package com.vg.lms.incred.sanction;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "LOAN_AMOUNT",
    "LOAN_RATE",
    "LOAN_TENURE",
    "DUE_DATE",
    "PROCESSING_FEE",
    "ADVANCE_EMI"
})
public class SANCTIONDETAIL {

    @JsonProperty("LOAN_AMOUNT")
    private Integer lOANAMOUNT;
    @JsonProperty("LOAN_RATE")
    private float lOANRATE;
    @JsonProperty("LOAN_TENURE")
    private Integer lOANTENURE;
    @JsonProperty("DUE_DATE")
    private String dUEDATE;
    @JsonProperty("LOAN_SCHEME")
    private String loanScheme;
    @JsonProperty("PROCESSING_FEE")
    private Integer pROCESSINGFEE;
    @JsonProperty("ADVANCE_EMI")
    private Integer aDVANCEEMI;


    @JsonProperty("LOAN_AMOUNT")
    public Integer getLOANAMOUNT() {
        return lOANAMOUNT;
    }

    @JsonProperty("LOAN_AMOUNT")
    public void setLOANAMOUNT(Integer lOANAMOUNT) {
        this.lOANAMOUNT = lOANAMOUNT;
    }

    @JsonProperty("LOAN_RATE")
    public float getLOANRATE() {
        return lOANRATE;
    }

    @JsonProperty("LOAN_RATE")
    public void setLOANRATE(float lOANRATE) {
        this.lOANRATE = lOANRATE;
    }

    @JsonProperty("LOAN_TENURE")
    public Integer getLOANTENURE() {
        return lOANTENURE;
    }

    @JsonProperty("LOAN_TENURE")
    public void setLOANTENURE(Integer lOANTENURE) {
        this.lOANTENURE = lOANTENURE;
    }

    @JsonProperty("DUE_DATE")
    public String getDUEDATE() {
        return dUEDATE;
    }

    @JsonProperty("DUE_DATE")
    public void setDUEDATE(String dUEDATE) {
        this.dUEDATE = dUEDATE;
    }

    @JsonProperty("PROCESSING_FEE")
    public Integer getPROCESSINGFEE() {
        return pROCESSINGFEE;
    }

    @JsonProperty("PROCESSING_FEE")
    public void setPROCESSINGFEE(Integer pROCESSINGFEE) {
        this.pROCESSINGFEE = pROCESSINGFEE;
    }
    
    @JsonProperty("ADVANCE_EMI")
    public Integer getADVANCEEMI() {
        return aDVANCEEMI;
    }

    @JsonProperty("ADVANCE_EMI")
    public void setADVANCEEMI(Integer aDVANCEEMI) {
        this.aDVANCEEMI = aDVANCEEMI;
    }
    
    @JsonProperty("LOAN_SCHEME")
    public void setLoanScheme(String loanScheme) {
        this.loanScheme = loanScheme;
    }


}
