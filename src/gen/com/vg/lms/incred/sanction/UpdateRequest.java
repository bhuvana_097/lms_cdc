
package com.vg.lms.incred.sanction;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "APPLICATION_ID",
    "SANCTION_DETAIL"
})
public class UpdateRequest {

    @JsonProperty("APPLICATION_ID")
    private String aPPLICATIONID;
    @JsonProperty("SANCTION_DETAIL")
    private SANCTIONDETAIL sANCTIONDETAIL;

    @JsonProperty("APPLICATION_ID")
    public String getAPPLICATIONID() {
        return aPPLICATIONID;
    }

    @JsonProperty("APPLICATION_ID")
    public void setAPPLICATIONID(String aPPLICATIONID) {
        this.aPPLICATIONID = aPPLICATIONID;
    }

    @JsonProperty("SANCTION_DETAIL")
    public SANCTIONDETAIL getSANCTIONDETAIL() {
        return sANCTIONDETAIL;
    }

    @JsonProperty("SANCTION_DETAIL")
    public void setSANCTIONDETAIL(SANCTIONDETAIL sANCTIONDETAIL) {
        this.sANCTIONDETAIL = sANCTIONDETAIL;
    }

}
