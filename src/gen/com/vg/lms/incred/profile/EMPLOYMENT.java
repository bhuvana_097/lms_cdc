
package com.vg.lms.incred.profile;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ADDRESS",
    "LOCALITY",
    "PINCODE",
    "STATE",
    "SALARY"
})
public class EMPLOYMENT {

    @JsonProperty("ADDRESS")
    private String aDDRESS;
    @JsonProperty("LOCALITY")
    private String lOCALITY;
    @JsonProperty("PINCODE")
    private String pINCODE;
    @JsonProperty("STATE")
    private Integer sTATE;
    @JsonProperty("SALARY")
    private SALARY sALARY;

    @JsonProperty("ADDRESS")
    public String getADDRESS() {
        return aDDRESS;
    }

    @JsonProperty("ADDRESS")
    public void setADDRESS(String aDDRESS) {
        this.aDDRESS = aDDRESS;
    }

    @JsonProperty("LOCALITY")
    public String getLOCALITY() {
        return lOCALITY;
    }

    @JsonProperty("LOCALITY")
    public void setLOCALITY(String lOCALITY) {
        this.lOCALITY = lOCALITY;
    }

    @JsonProperty("PINCODE")
    public String getPINCODE() {
        return pINCODE;
    }

    @JsonProperty("PINCODE")
    public void setPINCODE(String pINCODE) {
        this.pINCODE = pINCODE;
    }

    @JsonProperty("STATE")
    public Integer getSTATE() {
        return sTATE;
    }

    @JsonProperty("STATE")
    public void setSTATE(Integer sTATE) {
        this.sTATE = sTATE;
    }

    @JsonProperty("SALARY")
    public SALARY getSALARY() {
        return sALARY;
    }

    @JsonProperty("SALARY")
    public void setSALARY(SALARY sALARY) {
        this.sALARY = sALARY;
    }

}
