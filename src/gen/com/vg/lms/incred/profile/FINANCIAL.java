
package com.vg.lms.incred.profile;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "BANK"
})
public class FINANCIAL {

    @JsonProperty("BANK")
    private BANK bANK;

    @JsonProperty("BANK")
    public BANK getBANK() {
        return bANK;
    }

    @JsonProperty("BANK")
    public void setBANK(BANK bANK) {
        this.bANK = bANK;
    }

}
