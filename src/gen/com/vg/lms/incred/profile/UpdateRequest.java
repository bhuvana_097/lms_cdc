
package com.vg.lms.incred.profile;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "CUSTOMER_ID",
    "FNAME",
    "LNAME",
    "EMPLOYMENT",
    "ADDRESS",
    "FINANCIAL"
})
public class UpdateRequest {

    @JsonProperty("CUSTOMER_ID")
    private String cUSTOMERID;
    @JsonProperty("FNAME")
    private String fNAME;
    @JsonProperty("LNAME")
    private String lNAME;
    @JsonProperty("EMPLOYMENT")
    private List<EMPLOYMENT> eMPLOYMENT = null;
    @JsonProperty("ADDRESS")
    private List<ADDRESS> aDDRESS = null;
    @JsonProperty("FINANCIAL")
    private FINANCIAL fINANCIAL;

    @JsonProperty("CUSTOMER_ID")
    public String getCUSTOMERID() {
        return cUSTOMERID;
    }

    @JsonProperty("CUSTOMER_ID")
    public void setCUSTOMERID(String cUSTOMERID) {
        this.cUSTOMERID = cUSTOMERID;
    }

    @JsonProperty("FNAME")
    public String getFNAME() {
        return fNAME;
    }

    @JsonProperty("FNAME")
    public void setFNAME(String fNAME) {
        this.fNAME = fNAME;
    }

    @JsonProperty("LNAME")
    public String getLNAME() {
        return lNAME;
    }

    @JsonProperty("LNAME")
    public void setLNAME(String lNAME) {
        this.lNAME = lNAME;
    }

    @JsonProperty("EMPLOYMENT")
    public List<EMPLOYMENT> getEMPLOYMENT() {
        return eMPLOYMENT;
    }

    @JsonProperty("EMPLOYMENT")
    public void setEMPLOYMENT(List<EMPLOYMENT> eMPLOYMENT) {
        this.eMPLOYMENT = eMPLOYMENT;
    }

    @JsonProperty("ADDRESS")
    public List<ADDRESS> getADDRESS() {
        return aDDRESS;
    }

    @JsonProperty("ADDRESS")
    public void setADDRESS(List<ADDRESS> aDDRESS) {
        this.aDDRESS = aDDRESS;
    }

    @JsonProperty("FINANCIAL")
    public FINANCIAL getFINANCIAL() {
        return fINANCIAL;
    }

    @JsonProperty("FINANCIAL")
    public void setFINANCIAL(FINANCIAL fINANCIAL) {
        this.fINANCIAL = fINANCIAL;
    }

}
