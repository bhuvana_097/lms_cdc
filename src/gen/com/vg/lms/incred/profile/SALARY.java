
package com.vg.lms.incred.profile;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "MONTHLY",
    "NET_MONTHLY",
    "TYPE"
})
public class SALARY {

    @JsonProperty("MONTHLY")
    private Integer mONTHLY;
    @JsonProperty("NET_MONTHLY")
    private Integer nETMONTHLY;
    @JsonProperty("TYPE")
    private String tYPE;

    @JsonProperty("MONTHLY")
    public Integer getMONTHLY() {
        return mONTHLY;
    }

    @JsonProperty("MONTHLY")
    public void setMONTHLY(Integer mONTHLY) {
        this.mONTHLY = mONTHLY;
    }

    @JsonProperty("NET_MONTHLY")
    public Integer getNETMONTHLY() {
        return nETMONTHLY;
    }

    @JsonProperty("NET_MONTHLY")
    public void setNETMONTHLY(Integer nETMONTHLY) {
        this.nETMONTHLY = nETMONTHLY;
    }

    @JsonProperty("TYPE")
    public String getTYPE() {
        return tYPE;
    }

    @JsonProperty("TYPE")
    public void setTYPE(String tYPE) {
        this.tYPE = tYPE;
    }

}
