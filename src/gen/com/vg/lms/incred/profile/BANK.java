
package com.vg.lms.incred.profile;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ACCOUNT"
})
public class BANK {

    @JsonProperty("ACCOUNT")
    private List<ACCOUNT> aCCOUNT = null;

    @JsonProperty("ACCOUNT")
    public List<ACCOUNT> getACCOUNT() {
        return aCCOUNT;
    }

    @JsonProperty("ACCOUNT")
    public void setACCOUNT(List<ACCOUNT> aCCOUNT) {
        this.aCCOUNT = aCCOUNT;
    }

}
