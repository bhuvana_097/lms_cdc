
package com.vg.lms.incred.profile;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ADDRESS",
    "PINCODE"
})
public class ADDRESS {

    @JsonProperty("ADDRESS")
    private String aDDRESS;
    @JsonProperty("PINCODE")
    private String pINCODE;

    @JsonProperty("ADDRESS")
    public String getADDRESS() {
        return aDDRESS;
    }

    @JsonProperty("ADDRESS")
    public void setADDRESS(String aDDRESS) {
        this.aDDRESS = aDDRESS;
    }

    @JsonProperty("PINCODE")
    public String getPINCODE() {
        return pINCODE;
    }

    @JsonProperty("PINCODE")
    public void setPINCODE(String pINCODE) {
        this.pINCODE = pINCODE;
    }

}
