
package com.vg.lms.incred.profile;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ACCOUNT_HOLDER_NAME",
    "ACCOUNT_NUM",
    "ACCOUNT_TYPE",
    "BANK_CODE",
    "IFSC_CODE"
})
public class ACCOUNT {

    @JsonProperty("ACCOUNT_HOLDER_NAME")
    private String aCCOUNTHOLDERNAME;
    @JsonProperty("ACCOUNT_NUM")
    private String aCCOUNTNUM;
    @JsonProperty("ACCOUNT_TYPE")
    private String aCCOUNTTYPE;
    @JsonProperty("BANK_CODE")
    private String bANKNAME;
    @JsonProperty("IFSC_CODE")
    private String iFSCCODE;

    @JsonProperty("ACCOUNT_HOLDER_NAME")
    public String getACCOUNTHOLDERNAME() {
        return aCCOUNTHOLDERNAME;
    }

    @JsonProperty("ACCOUNT_HOLDER_NAME")
    public void setACCOUNTHOLDERNAME(String aCCOUNTHOLDERNAME) {
        this.aCCOUNTHOLDERNAME = aCCOUNTHOLDERNAME;
    }

    @JsonProperty("ACCOUNT_NUM")
    public String getACCOUNTNUM() {
        return aCCOUNTNUM;
    }

    @JsonProperty("ACCOUNT_NUM")
    public void setACCOUNTNUM(String aCCOUNTNUM) {
        this.aCCOUNTNUM = aCCOUNTNUM;
    }

    @JsonProperty("ACCOUNT_TYPE")
    public String getACCOUNTTYPE() {
        return aCCOUNTTYPE;
    }

    @JsonProperty("ACCOUNT_TYPE")
    public void setACCOUNTTYPE(String aCCOUNTTYPE) {
        this.aCCOUNTTYPE = aCCOUNTTYPE;
    }

    @JsonProperty("BANK_CODE")
    public String getBANKNAME() {
        return bANKNAME;
    }

    @JsonProperty("BANK_CODE")
    public void setBANKNAME(String bANKNAME) {
        this.bANKNAME = bANKNAME;
    }

    @JsonProperty("IFSC_CODE")
    public String getIFSCCODE() {
        return iFSCCODE;
    }

    @JsonProperty("IFSC_CODE")
    public void setIFSCCODE(String iFSCCODE) {
        this.iFSCCODE = iFSCCODE;
    }

}
