
package com.vg.lms.incred.applicant;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "APPLICATION_ID",
    "APPLICANT_TYPE",
    "FNAME",
    "MNAME",
    "LNAME",
    "GENDER",
    "MOBILE",
    "LOAN_TYPE",
    "DOB",
    "PAN",
    "AADHAAR",
    "ADDRESS"
})
public class AddRequest {

    @JsonProperty("APPLICATION_ID")
    private String aPPLICATIONID;
    @JsonProperty("APPLICANT_TYPE")
    private String aPPLICANTTYPE;
    @JsonProperty("FNAME")
    private String fNAME;
    @JsonProperty("MNAME")
    private String mNAME;
    @JsonProperty("LNAME")
    private String lNAME;
    @JsonProperty("MOBILE")
    private String mOBILE;
    @JsonProperty("LOAN_TYPE")
    private String lOANTYPE;
    @JsonProperty("DOB")
    private String dOB;
    @JsonProperty("PAN")
    private String pAN;
    @JsonProperty("GENDER")
    private String gENDER;
    @JsonProperty("AADHAAR")
    private String aADHAAR;
    @JsonProperty("ADDRESS")
    private List<ADDRESS> aDDRESS = null;

    @JsonProperty("APPLICATION_ID")
    public String getAPPLICATIONID() {
        return aPPLICATIONID;
    }

    @JsonProperty("APPLICATION_ID")
    public void setAPPLICATIONID(String aPPLICATIONID) {
        this.aPPLICATIONID = aPPLICATIONID;
    }

    @JsonProperty("APPLICANT_TYPE")
    public String getAPPLICANTTYPE() {
        return aPPLICANTTYPE;
    }

    @JsonProperty("APPLICANT_TYPE")
    public void setAPPLICANTTYPE(String aPPLICANTTYPE) {
        this.aPPLICANTTYPE = aPPLICANTTYPE;
    }

    @JsonProperty("FNAME")
    public String getFNAME() {
        return fNAME;
    }

    @JsonProperty("FNAME")
    public void setFNAME(String fNAME) {
        this.fNAME = fNAME;
    }

    @JsonProperty("MNAME")
    public String getMNAME() {
        return mNAME;
    }

    @JsonProperty("MNAME")
    public void setMNAME(String mNAME) {
        this.mNAME = mNAME;
    }

    @JsonProperty("LNAME")
    public String getLNAME() {
        return lNAME;
    }

    @JsonProperty("LNAME")
    public void setLNAME(String lNAME) {
        this.lNAME = lNAME;
    }

    @JsonProperty("MOBILE")
    public String getMOBILE() {
        return mOBILE;
    }

    @JsonProperty("MOBILE")
    public void setMOBILE(String mOBILE) {
        this.mOBILE = mOBILE;
    }

    @JsonProperty("LOAN_TYPE")
    public String getLOANTYPE() {
        return lOANTYPE;
    }

    @JsonProperty("LOAN_TYPE")
    public void setLOANTYPE(String lOANTYPE) {
        this.lOANTYPE = lOANTYPE;
    }

    @JsonProperty("DOB")
    public String getDOB() {
        return dOB;
    }

    @JsonProperty("DOB")
    public void setDOB(String dOB) {
        this.dOB = dOB;
    }

    @JsonProperty("PAN")
    public String getPAN() {
        return pAN;
    }

    @JsonProperty("PAN")
    public void setPAN(String pAN) {
        this.pAN = pAN;
    }

    @JsonProperty("AADHAAR")
    public String getAADHAAR() {
        return aADHAAR;
    }

    @JsonProperty("AADHAAR")
    public void setAADHAAR(String aADHAAR) {
        this.aADHAAR = aADHAAR;
    }

    @JsonProperty("ADDRESS")
    public List<ADDRESS> getADDRESS() {
        return aDDRESS;
    }

    @JsonProperty("ADDRESS")
    public void setADDRESS(List<ADDRESS> aDDRESS) {
        this.aDDRESS = aDDRESS;
    }
    
    @JsonProperty("GENDER")
    public String getGENDER() {
        return gENDER;
    }

    @JsonProperty("GENDER")
    public void setGENDER(String gENDER) {
        this.gENDER = gENDER;
    }

}
