
package com.vg.lms.incred.init;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "CUSTOMER_ID",
    "APPLICATION_ID",
    "TRANSACTION_ID"
})
public class Response {

    @JsonProperty("CUSTOMER_ID")
    private String cUSTOMERID;
    @JsonProperty("APPLICATION_ID")
    private String aPPLICATIONID;
    @JsonProperty("TRANSACTION_ID")
    private String tRANSACTIONID;

    @JsonProperty("CUSTOMER_ID")
    public String getCUSTOMERID() {
        return cUSTOMERID;
    }

    @JsonProperty("CUSTOMER_ID")
    public void setCUSTOMERID(String cUSTOMERID) {
        this.cUSTOMERID = cUSTOMERID;
    }

    @JsonProperty("APPLICATION_ID")
    public String getAPPLICATIONID() {
        return aPPLICATIONID;
    }

    @JsonProperty("APPLICATION_ID")
    public void setAPPLICATIONID(String aPPLICATIONID) {
        this.aPPLICATIONID = aPPLICATIONID;
    }

    @JsonProperty("TRANSACTION_ID")
    public String getTRANSACTIONID() {
        return tRANSACTIONID;
    }

    @JsonProperty("TRANSACTION_ID")
    public void setTRANSACTIONID(String tRANSACTIONID) {
        this.tRANSACTIONID = tRANSACTIONID;
    }

}
