
package com.vg.lms.incred.init;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "FNAME",
    "LNAME",
    "MOBILE",
    "LOAN_TYPE",
    "DOB",
    "PAN",
    "AADHAAR",
    "GENDER",
    "PARTNER_REFERENCE",
    "ADDRESS"
})
public class InitRequest {

    @JsonProperty("FNAME")
    private String fNAME;
    @JsonProperty("LNAME")
    private String lNAME;
    @JsonProperty("MOBILE")
    private String mOBILE;
    @JsonProperty("LOAN_TYPE")
    private String lOANTYPE;
    @JsonProperty("DOB")
    private String dOB;
    @JsonProperty("PAN")
    private String pAN;
    @JsonProperty("AADHAAR")
    private String aADHAAR;
    @JsonProperty("GENDER")
    private String gENDER;
    @JsonProperty("PARTNER_REFERENCE")
    private String pARTNERREFERENCE;
    @JsonProperty("ADDRESS")
    private List<ADDRESS> aDDRESS = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("FNAME")
    public String getFNAME() {
        return fNAME;
    }

    @JsonProperty("FNAME")
    public void setFNAME(String fNAME) {
        this.fNAME = fNAME;
    }

    @JsonProperty("LNAME")
    public String getLNAME() {
        return lNAME;
    }

    @JsonProperty("LNAME")
    public void setLNAME(String lNAME) {
        this.lNAME = lNAME;
    }

    @JsonProperty("MOBILE")
    public String getMOBILE() {
        return mOBILE;
    }

    @JsonProperty("MOBILE")
    public void setMOBILE(String mOBILE) {
        this.mOBILE = mOBILE;
    }

    @JsonProperty("LOAN_TYPE")
    public String getLOANTYPE() {
        return lOANTYPE;
    }

    @JsonProperty("LOAN_TYPE")
    public void setLOANTYPE(String lOANTYPE) {
        this.lOANTYPE = lOANTYPE;
    }

    @JsonProperty("DOB")
    public String getDOB() {
        return dOB;
    }

    @JsonProperty("DOB")
    public void setDOB(String dOB) {
        this.dOB = dOB;
    }

    @JsonProperty("PAN")
    public String getPAN() {
        return pAN;
    }

    @JsonProperty("PAN")
    public void setPAN(String pAN) {
        this.pAN = pAN;
    }

    @JsonProperty("AADHAAR")
    public String getAADHAAR() {
        return aADHAAR;
    }

    @JsonProperty("AADHAAR")
    public void setAADHAAR(String aADHAAR) {
        this.aADHAAR = aADHAAR;
    }

    @JsonProperty("GENDER")
    public String getGENDER() {
        return gENDER;
    }

    @JsonProperty("GENDER")
    public void setGENDER(String gENDER) {
        this.gENDER = gENDER;
    }
    
    @JsonProperty("PARTNER_REFERENCE")
    public String getPARTNERREFERENCE() {
        return pARTNERREFERENCE;
    }

    @JsonProperty("PARTNER_REFERENCE")
    public void setPARTNERREFERENCE(String pARTNERREFERENCE) {
        this.pARTNERREFERENCE = pARTNERREFERENCE;
    }

    @JsonProperty("ADDRESS")
    public List<ADDRESS> getADDRESS() {
        return aDDRESS;
    }

    @JsonProperty("ADDRESS")
    public void setADDRESS(List<ADDRESS> aDDRESS) {
        this.aDDRESS = aDDRESS;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    
    

}
