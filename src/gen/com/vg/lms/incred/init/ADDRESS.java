
package com.vg.lms.incred.init;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ADDRESS",
    "PINCODE"
})
public class ADDRESS {

    @JsonProperty("ADDRESS")
    private String aDDRESS;
    @JsonProperty("PINCODE")
    private String pINCODE;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ADDRESS")
    public String getADDRESS() {
        return aDDRESS;
    }

    @JsonProperty("ADDRESS")
    public void setADDRESS(String aDDRESS) {
        this.aDDRESS = aDDRESS;
    }

    @JsonProperty("PINCODE")
    public String getPINCODE() {
        return pINCODE;
    }

    @JsonProperty("PINCODE")
    public void setPINCODE(String pINCODE) {
        this.pINCODE = pINCODE;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
