
package com.vg.lms.incred.vehicle;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "VEHICLE_BRAND",
    "VEHICLE_MODEL"
})
public class VEHICLEDETAILS {

    @JsonProperty("VEHICLE_BRAND")
    private String vEHICLEBRAND;
    @JsonProperty("VEHICLE_MODEL")
    private String vEHICLEMODEL;

    @JsonProperty("VEHICLE_BRAND")
    public String getVEHICLEBRAND() {
        return vEHICLEBRAND;
    }

    @JsonProperty("VEHICLE_BRAND")
    public void setVEHICLEBRAND(String vEHICLEBRAND) {
        this.vEHICLEBRAND = vEHICLEBRAND;
    }

    @JsonProperty("VEHICLE_MODEL")
    public String getVEHICLEMODEL() {
        return vEHICLEMODEL;
    }

    @JsonProperty("VEHICLE_MODEL")
    public void setVEHICLEMODEL(String vEHICLEMODEL) {
        this.vEHICLEMODEL = vEHICLEMODEL;
    }

}
