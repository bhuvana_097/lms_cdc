
package com.vg.lms.incred.vehicle;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "APPLICATION_ID",
    "DEALER_DETAILS"
})
public class UpdateRequest {

    @JsonProperty("APPLICATION_ID")
    private String aPPLICATIONID;
    @JsonProperty("DEALER_DETAILS")
    private DEALERDETAILS dEALERDETAILS;
   

    @JsonProperty("APPLICATION_ID")
    public String getAPPLICATIONID() {
        return aPPLICATIONID;
    }

    @JsonProperty("APPLICATION_ID")
    public void setAPPLICATIONID(String aPPLICATIONID) {
        this.aPPLICATIONID = aPPLICATIONID;
    }

    @JsonProperty("DEALER_DETAILS")
    public DEALERDETAILS getDEALERDETAILS() {
        return dEALERDETAILS;
    }

    @JsonProperty("DEALER_DETAILS")
    public void setDEALERDETAILS(DEALERDETAILS dEALERDETAILS) {
        this.dEALERDETAILS = dEALERDETAILS;
    }

   

}
