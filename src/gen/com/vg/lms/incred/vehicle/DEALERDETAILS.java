
package com.vg.lms.incred.vehicle;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "DEALER_NAME"
})
public class DEALERDETAILS {

    @JsonProperty("DEALER_NAME")
    private String dEALERNAME;

    @JsonProperty("DEALER_NAME")
    public String getDEALERNAME() {
        return dEALERNAME;
    }

    @JsonProperty("DEALER_NAME")
    public void setDEALERNAME(String dEALERNAME) {
        this.dEALERNAME = dEALERNAME;
    }

}
