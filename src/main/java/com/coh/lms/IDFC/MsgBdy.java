package com.coh.lms.IDFC;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class MsgBdy implements Serializable
{

@SerializedName("paymentReq")
@Expose
private PaymentReq paymentReq;
private final static long serialVersionUID = 465722553992096183L;

/**
* No args constructor for use in serialization
*
*/
public MsgBdy() {
}

/**
*
* @param paymentReq
*/
public MsgBdy(PaymentReq paymentReq) {
super();
this.paymentReq = paymentReq;
}

public PaymentReq getPaymentReq() {
return paymentReq;
}

public void setPaymentReq(PaymentReq paymentReq) {
this.paymentReq = paymentReq;
}



}
