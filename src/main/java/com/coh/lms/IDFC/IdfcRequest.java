package com.coh.lms.IDFC;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class IdfcRequest implements Serializable
{

@SerializedName("paymentTransactionReq")
@Expose
private PaymentTransactionReq paymentTransactionReq;
private final static long serialVersionUID = -8185369906402138735L;

/**
* No args constructor for use in serialization
*
*/
public IdfcRequest() {
}

/**
*
* @param paymentTransactionReq
*/
public IdfcRequest(PaymentTransactionReq paymentTransactionReq) {
super();
this.paymentTransactionReq = paymentTransactionReq;
}

public PaymentTransactionReq getPaymentTransactionReq() {
return paymentTransactionReq;
}

public void setPaymentTransactionReq(PaymentTransactionReq paymentTransactionReq) {
this.paymentTransactionReq = paymentTransactionReq;
}



}