package com.coh.lms.IDFC;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.vg.lms.model.IdfcTransaction;
import com.vg.lms.repository.IdfcTranRespository;




@Service
@Qualifier
public class IdfcServiceImpl implements IdfcService {
	
	 @Autowired
	  private IdfcTranRespository idfcRepository;
		
	private final Logger log = LoggerFactory.getLogger(IdfcServiceImpl.class);
	
	@Override
	@Transactional
		public MobileResponse IdfcValidation(MobileRequest mobileRequest) {
		
		log.info("Mobile Request:" + mobileRequest);
		IdfcTransaction idfctrans = new IdfcTransaction();
		MobileResponse response = new MobileResponse();
		idfctrans.setAccountNumber("123345566");
		idfctrans.setCustomerName("PAUL");
		idfctrans.setIfscCode("HDFC0000001");
		response.setStatus("1");
		response.setMessage("SUCCESS");
		return response;
		
	}

}
