package com.coh.lms.IDFC;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "CUSTOMER_NAME",
	"CUSTOMER_BANK_ACCOUNT",
	"IFSC_CODE",
	"TRAN_CODE",
	"UNAME",
	"APPLNO",
	"LEADNO"
	
})

public class MobileRequest {
	
	
	@JsonProperty("CUSTOMER_NAME")
	private String cUSTOMERNAME;
	@JsonProperty("CUSTOMER_BANK_ACCOUNT")
	private String cUSTOMERBANKACCOUNT;
	@JsonProperty("TRAN_CODE")
	private String tRANCODE;
	@JsonProperty("IFSC_CODE")
	private String iFSCCODE;
	@JsonProperty("UNAME")
	private String uNAME;
	@JsonProperty("APPLNO")
	private String aPPLNO;
	@JsonProperty("LEADNO")
	private String lEADNO;
	public String getcUSTOMERNAME() {
		return cUSTOMERNAME;
	}
	public void setcUSTOMERNAME(String cUSTOMERNAME) {
		this.cUSTOMERNAME = cUSTOMERNAME;
	}
	public String getcUSTOMERBANKACCOUNT() {
		return cUSTOMERBANKACCOUNT;
	}
	public void setcUSTOMERBANKACCOUNT(String cUSTOMERBANKACCOUNT) {
		this.cUSTOMERBANKACCOUNT = cUSTOMERBANKACCOUNT;
	}
	public String gettRANCODE() {
		return tRANCODE;
	}
	public void settRANCODE(String tRANCODE) {
		this.tRANCODE = tRANCODE;
	}
	public String getiFSCCODE() {
		return iFSCCODE;
	}
	public void setiFSCCODE(String iFSCCODE) {
		this.iFSCCODE = iFSCCODE;
	}
	public String getuNAME() {
		return uNAME;
	}
	public void setuNAME(String uNAME) {
		this.uNAME = uNAME;
	}
	public String getaPPLNO() {
		return aPPLNO;
	}
	public void setaPPLNO(String aPPLNO) {
		this.aPPLNO = aPPLNO;
	}
	public String getlEADNO() {
		return lEADNO;
	}
	public void setlEADNO(String lEADNO) {
		this.lEADNO = lEADNO;
	}
		
	
	@Override
	public String toString() {
		return "MobileRequest [cUSTOMERNAME=" + cUSTOMERNAME + ", cUSTOMERBANKACCOUNT=" + cUSTOMERBANKACCOUNT + ",tRANCODE=" + tRANCODE + ","
				+ "iFSCCODE=" + iFSCCODE + ",uNAME=" + uNAME + ",aPPLNO=" + aPPLNO + ",lEADNO=" + lEADNO + ",toString()="
				+ super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}

}
