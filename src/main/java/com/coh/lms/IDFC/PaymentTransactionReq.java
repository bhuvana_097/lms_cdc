package com.coh.lms.IDFC;


import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PaymentTransactionReq implements Serializable
{

@SerializedName("msgHdr")
@Expose
private MsgHdr msgHdr;
@SerializedName("msgBdy")
@Expose
private MsgBdy msgBdy;
private final static long serialVersionUID = -103387663429273372L;

/**
* No args constructor for use in serialization
*
*/
public PaymentTransactionReq() {
}

/**
*
* @param msgHdr
* @param msgBdy
*/
public PaymentTransactionReq(MsgHdr msgHdr, MsgBdy msgBdy) {
super();
this.msgHdr = msgHdr;
this.msgBdy = msgBdy;
}

public MsgHdr getMsgHdr() {
return msgHdr;
}

public void setMsgHdr(MsgHdr msgHdr) {
this.msgHdr = msgHdr;
}

public MsgBdy getMsgBdy() {
return msgBdy;
}

public void setMsgBdy(MsgBdy msgBdy) {
this.msgBdy = msgBdy;
}



}