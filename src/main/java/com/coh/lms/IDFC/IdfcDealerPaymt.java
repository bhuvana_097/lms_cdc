package com.coh.lms.IDFC;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    
	"TYPE",
	"LOAN_NO",
	"DEALER_ACCOUNT",
	"DEALER_IFSC",
	"DEALER_CODE",
	"DEALER_DISBURSEMENT_AMOUNT",
	"DEALER_CITY",
	"DEALER_STATE",
	"TRAN_CODE"
	
})
public class IdfcDealerPaymt {

	
	@Override
	public String toString() {
		return "IdfcDealerPaymt [TYPE=" + TYPE + ", lOANNO=" + lOANNO + ", dEALERACCOUNT=" + dEALERACCOUNT
				+ ", dEALERIFSC=" + dEALERIFSC + ", dEALERCODE=" + dEALERCODE + ", dEALERDISBURSEMENTAMOUNT="
				+ dEALERDISBURSEMENTAMOUNT + ", dEALERCITY=" + dEALERCITY + ", dEALERSTATE=" + dEALERSTATE
				+ ", tRANCODE=" + tRANCODE + "]";
	}
	@JsonProperty("TYPE")
	private String TYPE;
	@JsonProperty("LOAN_NO")
	private String lOANNO;
	@JsonProperty("DEALER_ACCOUNT")
	private String dEALERACCOUNT;
	@JsonProperty("DEALER_IFSC")
	private String dEALERIFSC;
	@JsonProperty("DEALER_CODE")
	private String dEALERCODE;
	@JsonProperty("DEALER_DISBURSEMENT_AMOUNT")
	private String dEALERDISBURSEMENTAMOUNT;
	@JsonProperty("DEALER_CITY")
	private String dEALERCITY;
	@JsonProperty("DEALER_STATE")
	private String dEALERSTATE;
	@JsonProperty("TRAN_CODE")
	private String tRANCODE;
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public String getlOANNO() {
		return lOANNO;
	}
	public void setlOANNO(String lOANNO) {
		this.lOANNO = lOANNO;
	}
	public String getdEALERACCOUNT() {
		return dEALERACCOUNT;
	}
	public void setdEALERACCOUNT(String dEALERACCOUNT) {
		this.dEALERACCOUNT = dEALERACCOUNT;
	}
	public String getdEALERIFSC() {
		return dEALERIFSC;
	}
	public void setdEALERIFSC(String dEALERIFSC) {
		this.dEALERIFSC = dEALERIFSC;
	}
	public String getdEALERCODE() {
		return dEALERCODE;
	}
	public void setdEALERCODE(String dEALERCODE) {
		this.dEALERCODE = dEALERCODE;
	}
	public String getdEALERDISBURSEMENTAMOUNT() {
		return dEALERDISBURSEMENTAMOUNT;
	}
	public void setdEALERDISBURSEMENTAMOUNT(String dEALERDISBURSEMENTAMOUNT) {
		this.dEALERDISBURSEMENTAMOUNT = dEALERDISBURSEMENTAMOUNT;
	}
	public String getdEALERCITY() {
		return dEALERCITY;
	}
	public void setdEALERCITY(String dEALERCITY) {
		this.dEALERCITY = dEALERCITY;
	}
	public String getdEALERSTATE() {
		return dEALERSTATE;
	}
	public void setdEALERSTATE(String dEALERSTATE) {
		this.dEALERSTATE = dEALERSTATE;
	}
	public String gettRANCODE() {
		return tRANCODE;
	}
	public void settRANCODE(String tRANCODE) {
		this.tRANCODE = tRANCODE;
	}
	

	

}
