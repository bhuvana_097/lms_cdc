package com.coh.lms.IDFC;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class InvoiceDetail implements Serializable
{

@SerializedName("invAmount")
@Expose
private String invAmount;
@SerializedName("invDate")
@Expose
private String invDate;
@SerializedName("invDesc")
@Expose
private String invDesc;
@SerializedName("invNumber")
@Expose
private String invNumber;
@SerializedName("payAmount")
@Expose
private String payAmount;
@SerializedName("filler1")
@Expose
private String filler1;
@SerializedName("filler2")
@Expose
private String filler2;
@SerializedName("filler3")
@Expose
private String filler3;
@SerializedName("filler4")
@Expose
private String filler4;
@SerializedName("filler5")
@Expose
private String filler5;
@SerializedName("filler6")
@Expose
private String filler6;
@SerializedName("filler7")
@Expose
private String filler7;
@SerializedName("filler8")
@Expose
private String filler8;
@SerializedName("filler9")
@Expose
private String filler9;
@SerializedName("filler10")
@Expose
private String filler10;
@SerializedName("filler11")
@Expose
private String filler11;
@SerializedName("filler12")
@Expose
private String filler12;
@SerializedName("filler13")
@Expose
private String filler13;
@SerializedName("filler14")
@Expose
private String filler14;
@SerializedName("filler15")
@Expose
private String filler15;
@SerializedName("filler16")
@Expose
private String filler16;
@SerializedName("filler17")
@Expose
private String filler17;
@SerializedName("filler18")
@Expose
private String filler18;
@SerializedName("filler19")
@Expose
private String filler19;
@SerializedName("filler20")
@Expose
private String filler20;
@SerializedName("filler21")
@Expose
private String filler21;
@SerializedName("filler22")
@Expose
private String filler22;
@SerializedName("filler23")
@Expose
private String filler23;
@SerializedName("filler24")
@Expose
private String filler24;
@SerializedName("filler25")
@Expose
private String filler25;
@SerializedName("filler26")
@Expose
private String filler26;
@SerializedName("filler27")
@Expose
private String filler27;
@SerializedName("filler28")
@Expose
private String filler28;
@SerializedName("filler29")
@Expose
private String filler29;
@SerializedName("filler30")
@Expose
private String filler30;
@SerializedName("filler41")
@Expose
private String filler41;
@SerializedName("filler42")
@Expose
private String filler42;
@SerializedName("filler43")
@Expose
private String filler43;
@SerializedName("filler44")
@Expose
private String filler44;
@SerializedName("filler45")
@Expose
private String filler45;
@SerializedName("filler46")
@Expose
private String filler46;
@SerializedName("filler47")
@Expose
private String filler47;
@SerializedName("filler48")
@Expose
private String filler48;
@SerializedName("filler49")
@Expose
private String filler49;
@SerializedName("filler50")
@Expose
private String filler50;
@SerializedName("filler51")
@Expose
private String filler51;
@SerializedName("filler52")
@Expose
private String filler52;
@SerializedName("filler53")
@Expose
private String filler53;
@SerializedName("filler54")
@Expose
private String filler54;
private final static long serialVersionUID = 8483602979662203717L;

/**
* No args constructor for use in serialization
*
*/
public InvoiceDetail() {
}

/**
*
* @param filler14
* @param filler13
* @param filler12
* @param filler11
* @param filler18
* @param filler17
* @param filler16
* @param filler15
* @param filler19
* @param payAmount
* @param filler50
* @param filler10
* @param filler54
* @param filler53
* @param filler52
* @param filler51
* @param filler25
* @param filler24
* @param filler23
* @param filler22
* @param filler29
* @param filler28
* @param filler27
* @param filler26
* @param invNumber
* @param invAmount
* @param filler21
* @param filler20
* @param filler9
* @param filler8
* @param filler5
* @param filler4
* @param filler7
* @param filler6
* @param filler1
* @param filler3
* @param filler2
* @param filler30
* @param invDesc
* @param filler47
* @param filler46
* @param filler45
* @param filler44
* @param filler49
* @param filler48
* @param filler43
* @param invDate
* @param filler42
* @param filler41
*/
public InvoiceDetail(String invAmount, String invDate, String invDesc, String invNumber, String payAmount, String filler1, String filler2, String filler3, String filler4, String filler5, String filler6, String filler7, String filler8, String filler9, String filler10, String filler11, String filler12, String filler13, String filler14, String filler15, String filler16, String filler17, String filler18, String filler19, String filler20, String filler21, String filler22, String filler23, String filler24, String filler25, String filler26, String filler27, String filler28, String filler29, String filler30, String filler41, String filler42, String filler43, String filler44, String filler45, String filler46, String filler47, String filler48, String filler49, String filler50, String filler51, String filler52, String filler53, String filler54) {
super();
this.invAmount = invAmount;
this.invDate = invDate;
this.invDesc = invDesc;
this.invNumber = invNumber;
this.payAmount = payAmount;
this.filler1 = filler1;
this.filler2 = filler2;
this.filler3 = filler3;
this.filler4 = filler4;
this.filler5 = filler5;
this.filler6 = filler6;
this.filler7 = filler7;
this.filler8 = filler8;
this.filler9 = filler9;
this.filler10 = filler10;
this.filler11 = filler11;
this.filler12 = filler12;
this.filler13 = filler13;
this.filler14 = filler14;
this.filler15 = filler15;
this.filler16 = filler16;
this.filler17 = filler17;
this.filler18 = filler18;
this.filler19 = filler19;
this.filler20 = filler20;
this.filler21 = filler21;
this.filler22 = filler22;
this.filler23 = filler23;
this.filler24 = filler24;
this.filler25 = filler25;
this.filler26 = filler26;
this.filler27 = filler27;
this.filler28 = filler28;
this.filler29 = filler29;
this.filler30 = filler30;
this.filler41 = filler41;
this.filler42 = filler42;
this.filler43 = filler43;
this.filler44 = filler44;
this.filler45 = filler45;
this.filler46 = filler46;
this.filler47 = filler47;
this.filler48 = filler48;
this.filler49 = filler49;
this.filler50 = filler50;
this.filler51 = filler51;
this.filler52 = filler52;
this.filler53 = filler53;
this.filler54 = filler54;
}

public String getInvAmount() {
return invAmount;
}

public void setInvAmount(String invAmount) {
this.invAmount = invAmount;
}

public String getInvDate() {
return invDate;
}

public void setInvDate(String invDate) {
this.invDate = invDate;
}

public String getInvDesc() {
return invDesc;
}

public void setInvDesc(String invDesc) {
this.invDesc = invDesc;
}

public String getInvNumber() {
return invNumber;
}

public void setInvNumber(String invNumber) {
this.invNumber = invNumber;
}

public String getPayAmount() {
return payAmount;
}

public void setPayAmount(String payAmount) {
this.payAmount = payAmount;
}

public String getFiller1() {
return filler1;
}

public void setFiller1(String filler1) {
this.filler1 = filler1;
}

public String getFiller2() {
return filler2;
}

public void setFiller2(String filler2) {
this.filler2 = filler2;
}

public String getFiller3() {
return filler3;
}

public void setFiller3(String filler3) {
this.filler3 = filler3;
}

public String getFiller4() {
return filler4;
}

public void setFiller4(String filler4) {
this.filler4 = filler4;
}

public String getFiller5() {
return filler5;
}

public void setFiller5(String filler5) {
this.filler5 = filler5;
}

public String getFiller6() {
return filler6;
}

public void setFiller6(String filler6) {
this.filler6 = filler6;
}

public String getFiller7() {
return filler7;
}

public void setFiller7(String filler7) {
this.filler7 = filler7;
}

public String getFiller8() {
return filler8;
}

public void setFiller8(String filler8) {
this.filler8 = filler8;
}

public String getFiller9() {
return filler9;
}

public void setFiller9(String filler9) {
this.filler9 = filler9;
}

public String getFiller10() {
return filler10;
}

public void setFiller10(String filler10) {
this.filler10 = filler10;
}

public String getFiller11() {
return filler11;
}

public void setFiller11(String filler11) {
this.filler11 = filler11;
}

public String getFiller12() {
return filler12;
}

public void setFiller12(String filler12) {
this.filler12 = filler12;
}

public String getFiller13() {
return filler13;
}

public void setFiller13(String filler13) {
this.filler13 = filler13;
}

public String getFiller14() {
return filler14;
}

public void setFiller14(String filler14) {
this.filler14 = filler14;
}

public String getFiller15() {
return filler15;
}

public void setFiller15(String filler15) {
this.filler15 = filler15;
}

public String getFiller16() {
return filler16;
}

public void setFiller16(String filler16) {
this.filler16 = filler16;
}

public String getFiller17() {
return filler17;
}

public void setFiller17(String filler17) {
this.filler17 = filler17;
}

public String getFiller18() {
return filler18;
}

public void setFiller18(String filler18) {
this.filler18 = filler18;
}

public String getFiller19() {
return filler19;
}

public void setFiller19(String filler19) {
this.filler19 = filler19;
}

public String getFiller20() {
return filler20;
}

public void setFiller20(String filler20) {
this.filler20 = filler20;
}

public String getFiller21() {
return filler21;
}

public void setFiller21(String filler21) {
this.filler21 = filler21;
}

public String getFiller22() {
return filler22;
}

public void setFiller22(String filler22) {
this.filler22 = filler22;
}

public String getFiller23() {
return filler23;
}

public void setFiller23(String filler23) {
this.filler23 = filler23;
}

public String getFiller24() {
return filler24;
}

public void setFiller24(String filler24) {
this.filler24 = filler24;
}

public String getFiller25() {
return filler25;
}

public void setFiller25(String filler25) {
this.filler25 = filler25;
}

public String getFiller26() {
return filler26;
}

public void setFiller26(String filler26) {
this.filler26 = filler26;
}

public String getFiller27() {
return filler27;
}

public void setFiller27(String filler27) {
this.filler27 = filler27;
}

public String getFiller28() {
return filler28;
}

public void setFiller28(String filler28) {
this.filler28 = filler28;
}

public String getFiller29() {
return filler29;
}

public void setFiller29(String filler29) {
this.filler29 = filler29;
}

public String getFiller30() {
return filler30;
}

public void setFiller30(String filler30) {
this.filler30 = filler30;
}

public String getFiller41() {
return filler41;
}

public void setFiller41(String filler41) {
this.filler41 = filler41;
}

public String getFiller42() {
return filler42;
}

public void setFiller42(String filler42) {
this.filler42 = filler42;
}

public String getFiller43() {
return filler43;
}

public void setFiller43(String filler43) {
this.filler43 = filler43;
}

public String getFiller44() {
return filler44;
}

public void setFiller44(String filler44) {
this.filler44 = filler44;
}

public String getFiller45() {
return filler45;
}

public void setFiller45(String filler45) {
this.filler45 = filler45;
}

public String getFiller46() {
return filler46;
}

public void setFiller46(String filler46) {
this.filler46 = filler46;
}

public String getFiller47() {
return filler47;
}

public void setFiller47(String filler47) {
this.filler47 = filler47;
}

public String getFiller48() {
return filler48;
}

public void setFiller48(String filler48) {
this.filler48 = filler48;
}

public String getFiller49() {
return filler49;
}

public void setFiller49(String filler49) {
this.filler49 = filler49;
}

public String getFiller50() {
return filler50;
}

public void setFiller50(String filler50) {
this.filler50 = filler50;
}

public String getFiller51() {
return filler51;
}

public void setFiller51(String filler51) {
this.filler51 = filler51;
}

public String getFiller52() {
return filler52;
}

public void setFiller52(String filler52) {
this.filler52 = filler52;
}

public String getFiller53() {
return filler53;
}

public void setFiller53(String filler53) {
this.filler53 = filler53;
}

public String getFiller54() {
return filler54;
}

public void setFiller54(String filler54) {
this.filler54 = filler54;
}



}