package com.coh.lms.IDFC;

import com.vg.lms.response.BaseResponseDTO;

public class MobileResponse extends BaseResponseDTO {
	
	private String status;
	private String message;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
	public String toString() {
		return "MobileResponse [status=" + status + ", message=" +  message + ", getStatusCode()="
				+ getStatusCode() + ", getStatusMessage()=" + getStatusMessage() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}
	

}
