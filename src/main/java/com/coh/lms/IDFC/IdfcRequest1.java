package com.coh.lms.IDFC;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"custTxnRef", 
"beneAccNo", 
"beneName", 
"beneAddr1",
"beneAddr2",
"ifsc", 
"valueDate", 
"tranCcy",
"tranAmount",
"purposeCode", 
"remitInfo1", 
"remitInfo2", 
"remitInfo3", 
"remitInfo4", 
"remitInfo5",
"remitInfo6",
"clientCode",
"paymentType",
"beneAccType",
"remarks", 
"addLine1", 
"addLine2", 
"addLine3",
"addLine4", 
"delvMode", 
"printBranch",
"authPersonName",
"authPersonID", 
"payLocation", 
"routingCode", 
"signCode1", 
"signCode2", 
"signCode3", 
"signName1", 
"signName2", 
"signName3", 
"beneMail",
"beneMobile", 
"instNumber", 
"instDate", 
"corpBranch", 
"filler1", 
"filler2", 
"filler3", 
"filler4", 
"filler5", 
"filler6", 
"filler7", 
"filler8", 
"filler9",
"filler10", 
"filler11",
"filler12",
"filler13",
"filler14",
"filler15",
"filler16",
"filler17",
"filler18",
"filler19",
"filler20",
"filler21",
"filler22",
"filler23",
"filler24",
"filler25",
"filler26",
"filler27", 
"filler28",
"filler29", 
"filler30", 
"filler41", 
"filler42", 
"filler43", 
"filler44", 
"filler45", 
"filler46", 
"filler47", 
"filler48", 
"filler49", 
"filler50", 
"filler51", 
"filler52", 
"filler53",
"filler54"
})

public class IdfcRequest1 {
	@JsonProperty("custTxnRef") 
	private String custTxnRef;
	@JsonProperty("beneAccNo") 
	private String beneAccNo;
	@JsonProperty("beneName") 
	private String beneName;
	@JsonProperty("beneAddr1")
	private String beneAddr1;
	@JsonProperty("beneAddr2")
	private String beneAddr2;
	@JsonProperty("ifsc") 
	private String ifsc;
	@JsonProperty("valueDate") 
	private String valueDate;
	@JsonProperty("tranCcy") 
	private String tranCcy;
	@JsonProperty("tranAmount")
	private String tranAmount;
	@JsonProperty("purposeCode") 
	private String purposeCode;
	@JsonProperty("remitInfo1") 
	private String remitInfo1;
	@JsonProperty("remitInfo2") 
	private String remitInfo2;
	@JsonProperty("remitInfo3") 
	private String remitInfo3;
	@JsonProperty("remitInfo4") 
	private String remitInfo4;
	@JsonProperty("remitInfo5")
	private String remitInfo5;
	@JsonProperty("remitInfo6")
	private String remitInfo6;
	@JsonProperty("clientCode")
	private String clientCode;
	@JsonProperty("paymentType")
	private String paymentType;
	@JsonProperty("beneAccType")
	private String beneAccType;
	@JsonProperty("remarks") 
	private String remarks;
	@JsonProperty("addLine1")
	private String addLine1;
	@JsonProperty("addLine2") 
	private String addLine2;
	@JsonProperty("addLine3")
	private String addLine3;
	@JsonProperty("addLine4") 
	private String addLine4;
	@JsonProperty("delvMode") 
	private String delvMode;
	@JsonProperty("printBranch")
	private String printBranch;
	@JsonProperty("authPersonName")
	private String authPersonName;
	@JsonProperty("authPersonID") 
	private String authPersonID;
	@JsonProperty("payLocation") 
	private String payLocation;
	@JsonProperty("routingCode") 
	private String routingCode;
	@JsonProperty("signCode1")
	private String signCode1;
	@JsonProperty("signCode2")
	private String signCode2;
	@JsonProperty("signCode3")
	private String signCode3;
	@JsonProperty("signName1")
	private String signName1;
	@JsonProperty("signName2")
	private String signName2;
	@JsonProperty("signName3")
	private String signName3;
	@JsonProperty("beneMail")
	private String beneMail;
	@JsonProperty("beneMobile") 
	private String beneMobile;
	@JsonProperty("instNumber")
	private String instNumber;
	@JsonProperty("instDate") 
	private String instDate;
	@JsonProperty("corpBranch") 
	private String corpBranch;
	@JsonProperty("filler1")
	private String filler1;
	@JsonProperty("filler2")
	private String filler2;
	@JsonProperty("filler3")
	private String filler3;
	@JsonProperty("filler4")
	private String filler4;
	@JsonProperty("filler5")
	private String filler5;
	@JsonProperty("filler6")
	private String filler6;
	@JsonProperty("filler7")
	private String filler7;
	@JsonProperty("filler8")
	private String filler8;
	@JsonProperty("filler9")
	private String filler9;
	@JsonProperty("filler10")
	private String filler10;
	@JsonProperty("filler11")
	private String filler11;
	@JsonProperty("filler12")
	private String filler12;
	@JsonProperty("filler13")
	private String filler13;
	@JsonProperty("filler14")
	private String filler14;
	@JsonProperty("filler15")
	private String filler15;
	@JsonProperty("filler16")
	private String filler16;
	@JsonProperty("filler17")
	private String filler17;
	@JsonProperty("filler18")
	private String filler18;
	@JsonProperty("filler19")
	private String filler19;
	@JsonProperty("filler20")
	private String filler20;
	@JsonProperty("filler21")
	private String filler21;
	@JsonProperty("filler22")
	private String filler22;
	@JsonProperty("filler23")
	private String filler23;
	@JsonProperty("filler24")
	private String filler24;
	@JsonProperty("filler25")
	private String filler25;
	@JsonProperty("filler26")
	private String filler26;
	@JsonProperty("filler27")
	private String filler27;
	@JsonProperty("filler28")
	private String filler28;
	@JsonProperty("filler29")
	private String filler29;
	@JsonProperty("filler30")
	private String filler30;
	@JsonProperty("filler41")
	private String filler41;
	@JsonProperty("filler42")
	private String filler42;
	@JsonProperty("filler43")
	private String filler43;
	@JsonProperty("filler44")
	private String filler44;
	@JsonProperty("filler45")
	private String filler45;
	@JsonProperty("filler46")
	private String filler46;
	@JsonProperty("filler47")
	private String filler47;
	@JsonProperty("filler48")
	private String filler48;
	@JsonProperty("filler49")
	private String filler49;
	@JsonProperty("filler50")
	private String filler50;
	@JsonProperty("filler51")
	private String filler51;
	@JsonProperty("filler52")
	private String filler52;
	@JsonProperty("filler53")
	private String filler53;
	@JsonProperty("filler54")
	private String filler54;
	public String getCustTxnRef() {
		return custTxnRef;
	}
	public void setCustTxnRef(String custTxnRef) {
		this.custTxnRef = custTxnRef;
	}
	public String getBeneAccNo() {
		return beneAccNo;
	}
	public void setBeneAccNo(String beneAccNo) {
		this.beneAccNo = beneAccNo;
	}
	public String getBeneName() {
		return beneName;
	}
	public void setBeneName(String beneName) {
		this.beneName = beneName;
	}
	public String getBeneAddr1() {
		return beneAddr1;
	}
	public void setBeneAddr1(String beneAddr1) {
		this.beneAddr1 = beneAddr1;
	}
	public String getBeneAddr2() {
		return beneAddr2;
	}
	public void setBeneAddr2(String beneAddr2) {
		this.beneAddr2 = beneAddr2;
	}
	public String getIfsc() {
		return ifsc;
	}
	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}
	public String getValueDate() {
		return valueDate;
	}
	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}
	public String getTranCcy() {
		return tranCcy;
	}
	public void setTranCcy(String tranCcy) {
		this.tranCcy = tranCcy;
	}
	public String getTranAmount() {
		return tranAmount;
	}
	public void setTranAmount(String tranAmount) {
		this.tranAmount = tranAmount;
	}
	public String getPurposeCode() {
		return purposeCode;
	}
	public void setPurposeCode(String purposeCode) {
		this.purposeCode = purposeCode;
	}
	public String getRemitInfo1() {
		return remitInfo1;
	}
	public void setRemitInfo1(String remitInfo1) {
		this.remitInfo1 = remitInfo1;
	}
	public String getRemitInfo2() {
		return remitInfo2;
	}
	public void setRemitInfo2(String remitInfo2) {
		this.remitInfo2 = remitInfo2;
	}
	public String getRemitInfo3() {
		return remitInfo3;
	}
	public void setRemitInfo3(String remitInfo3) {
		this.remitInfo3 = remitInfo3;
	}
	public String getRemitInfo4() {
		return remitInfo4;
	}
	public void setRemitInfo4(String remitInfo4) {
		this.remitInfo4 = remitInfo4;
	}
	public String getRemitInfo5() {
		return remitInfo5;
	}
	public void setRemitInfo5(String remitInfo5) {
		this.remitInfo5 = remitInfo5;
	}
	public String getRemitInfo6() {
		return remitInfo6;
	}
	public void setRemitInfo6(String remitInfo6) {
		this.remitInfo6 = remitInfo6;
	}
	public String getClientCode() {
		return clientCode;
	}
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getBeneAccType() {
		return beneAccType;
	}
	public void setBeneAccType(String beneAccType) {
		this.beneAccType = beneAccType;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getAddLine1() {
		return addLine1;
	}
	public void setAddLine1(String addLine1) {
		this.addLine1 = addLine1;
	}
	public String getAddLine2() {
		return addLine2;
	}
	public void setAddLine2(String addLine2) {
		this.addLine2 = addLine2;
	}
	public String getAddLine3() {
		return addLine3;
	}
	public void setAddLine3(String addLine3) {
		this.addLine3 = addLine3;
	}
	public String getAddLine4() {
		return addLine4;
	}
	public void setAddLine4(String addLine4) {
		this.addLine4 = addLine4;
	}
	public String getDelvMode() {
		return delvMode;
	}
	public void setDelvMode(String delvMode) {
		this.delvMode = delvMode;
	}
	public String getPrintBranch() {
		return printBranch;
	}
	public void setPrintBranch(String printBranch) {
		this.printBranch = printBranch;
	}
	public String getAuthPersonName() {
		return authPersonName;
	}
	public void setAuthPersonName(String authPersonName) {
		this.authPersonName = authPersonName;
	}
	public String getAuthPersonID() {
		return authPersonID;
	}
	public void setAuthPersonID(String authPersonID) {
		this.authPersonID = authPersonID;
	}
	public String getPayLocation() {
		return payLocation;
	}
	public void setPayLocation(String payLocation) {
		this.payLocation = payLocation;
	}
	public String getRoutingCode() {
		return routingCode;
	}
	public void setRoutingCode(String routingCode) {
		this.routingCode = routingCode;
	}
	public String getSignCode1() {
		return signCode1;
	}
	public void setSignCode1(String signCode1) {
		this.signCode1 = signCode1;
	}
	public String getSignCode2() {
		return signCode2;
	}
	public void setSignCode2(String signCode2) {
		this.signCode2 = signCode2;
	}
	public String getSignCode3() {
		return signCode3;
	}
	public void setSignCode3(String signCode3) {
		this.signCode3 = signCode3;
	}
	public String getSignName1() {
		return signName1;
	}
	public void setSignName1(String signName1) {
		this.signName1 = signName1;
	}
	public String getSignName2() {
		return signName2;
	}
	public void setSignName2(String signName2) {
		this.signName2 = signName2;
	}
	public String getSignName3() {
		return signName3;
	}
	public void setSignName3(String signName3) {
		this.signName3 = signName3;
	}
	public String getBeneMail() {
		return beneMail;
	}
	public void setBeneMail(String beneMail) {
		this.beneMail = beneMail;
	}
	public String getBeneMobile() {
		return beneMobile;
	}
	public void setBeneMobile(String beneMobile) {
		this.beneMobile = beneMobile;
	}
	public String getInstNumber() {
		return instNumber;
	}
	public void setInstNumber(String instNumber) {
		this.instNumber = instNumber;
	}
	public String getInstDate() {
		return instDate;
	}
	public void setInstDate(String instDate) {
		this.instDate = instDate;
	}
	public String getCorpBranch() {
		return corpBranch;
	}
	public void setCorpBranch(String corpBranch) {
		this.corpBranch = corpBranch;
	}
	public String getFiller1() {
		return filler1;
	}
	public void setFiller1(String filler1) {
		this.filler1 = filler1;
	}
	public String getFiller2() {
		return filler2;
	}
	public void setFiller2(String filler2) {
		this.filler2 = filler2;
	}
	public String getFiller3() {
		return filler3;
	}
	public void setFiller3(String filler3) {
		this.filler3 = filler3;
	}
	public String getFiller4() {
		return filler4;
	}
	public void setFiller4(String filler4) {
		this.filler4 = filler4;
	}
	public String getFiller5() {
		return filler5;
	}
	public void setFiller5(String filler5) {
		this.filler5 = filler5;
	}
	public String getFiller6() {
		return filler6;
	}
	public void setFiller6(String filler6) {
		this.filler6 = filler6;
	}
	public String getFiller7() {
		return filler7;
	}
	public void setFiller7(String filler7) {
		this.filler7 = filler7;
	}
	public String getFiller8() {
		return filler8;
	}
	public void setFiller8(String filler8) {
		this.filler8 = filler8;
	}
	public String getFiller9() {
		return filler9;
	}
	public void setFiller9(String filler9) {
		this.filler9 = filler9;
	}
	public String getFiller10() {
		return filler10;
	}
	public void setFiller10(String filler10) {
		this.filler10 = filler10;
	}
	public String getFiller11() {
		return filler11;
	}
	public void setFiller11(String filler11) {
		this.filler11 = filler11;
	}
	public String getFiller12() {
		return filler12;
	}
	public void setFiller12(String filler12) {
		this.filler12 = filler12;
	}
	public String getFiller13() {
		return filler13;
	}
	public void setFiller13(String filler13) {
		this.filler13 = filler13;
	}
	public String getFiller14() {
		return filler14;
	}
	public void setFiller14(String filler14) {
		this.filler14 = filler14;
	}
	public String getFiller15() {
		return filler15;
	}
	public void setFiller15(String filler15) {
		this.filler15 = filler15;
	}
	public String getFiller16() {
		return filler16;
	}
	public void setFiller16(String filler16) {
		this.filler16 = filler16;
	}
	public String getFiller17() {
		return filler17;
	}
	public void setFiller17(String filler17) {
		this.filler17 = filler17;
	}
	public String getFiller18() {
		return filler18;
	}
	public void setFiller18(String filler18) {
		this.filler18 = filler18;
	}
	public String getFiller19() {
		return filler19;
	}
	public void setFiller19(String filler19) {
		this.filler19 = filler19;
	}
	public String getFiller20() {
		return filler20;
	}
	public void setFiller20(String filler20) {
		this.filler20 = filler20;
	}
	public String getFiller21() {
		return filler21;
	}
	public void setFiller21(String filler21) {
		this.filler21 = filler21;
	}
	public String getFiller22() {
		return filler22;
	}
	public void setFiller22(String filler22) {
		this.filler22 = filler22;
	}
	public String getFiller23() {
		return filler23;
	}
	public void setFiller23(String filler23) {
		this.filler23 = filler23;
	}
	public String getFiller24() {
		return filler24;
	}
	public void setFiller24(String filler24) {
		this.filler24 = filler24;
	}
	public String getFiller25() {
		return filler25;
	}
	public void setFiller25(String filler25) {
		this.filler25 = filler25;
	}
	public String getFiller26() {
		return filler26;
	}
	public void setFiller26(String filler26) {
		this.filler26 = filler26;
	}
	public String getFiller27() {
		return filler27;
	}
	public void setFiller27(String filler27) {
		this.filler27 = filler27;
	}
	public String getFiller28() {
		return filler28;
	}
	public void setFiller28(String filler28) {
		this.filler28 = filler28;
	}
	public String getFiller29() {
		return filler29;
	}
	public void setFiller29(String filler29) {
		this.filler29 = filler29;
	}
	public String getFiller30() {
		return filler30;
	}
	public void setFiller30(String filler30) {
		this.filler30 = filler30;
	}
	public String getFiller41() {
		return filler41;
	}
	public void setFiller41(String filler41) {
		this.filler41 = filler41;
	}
	public String getFiller42() {
		return filler42;
	}
	public void setFiller42(String filler42) {
		this.filler42 = filler42;
	}
	public String getFiller43() {
		return filler43;
	}
	public void setFiller43(String filler43) {
		this.filler43 = filler43;
	}
	public String getFiller44() {
		return filler44;
	}
	public void setFiller44(String filler44) {
		this.filler44 = filler44;
	}
	public String getFiller45() {
		return filler45;
	}
	public void setFiller45(String filler45) {
		this.filler45 = filler45;
	}
	public String getFiller46() {
		return filler46;
	}
	public void setFiller46(String filler46) {
		this.filler46 = filler46;
	}
	public String getFiller47() {
		return filler47;
	}
	public void setFiller47(String filler47) {
		this.filler47 = filler47;
	}
	public String getFiller48() {
		return filler48;
	}
	public void setFiller48(String filler48) {
		this.filler48 = filler48;
	}
	public String getFiller49() {
		return filler49;
	}
	public void setFiller49(String filler49) {
		this.filler49 = filler49;
	}
	public String getFiller50() {
		return filler50;
	}
	public void setFiller50(String filler50) {
		this.filler50 = filler50;
	}
	public String getFiller51() {
		return filler51;
	}
	public void setFiller51(String filler51) {
		this.filler51 = filler51;
	}
	public String getFiller52() {
		return filler52;
	}
	public void setFiller52(String filler52) {
		this.filler52 = filler52;
	}
	public String getFiller53() {
		return filler53;
	}
	public void setFiller53(String filler53) {
		this.filler53 = filler53;
	}
	public String getFiller54() {
		return filler54;
	}
	public void setFiller54(String filler54) {
		this.filler54 = filler54;
	}

}
