package com.coh.lms.IDFC;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PaymentReq implements Serializable
{

@SerializedName("custTxnRef")
@Expose
private String custTxnRef;
@SerializedName("beneAccNo")
@Expose
private String beneAccNo;
@SerializedName("beneName")
@Expose
private String beneName;
@SerializedName("beneAddr1")
@Expose
private String beneAddr1;
@SerializedName("beneAddr2")
@Expose
private String beneAddr2;
@SerializedName("ifsc")
@Expose
private String ifsc;
@SerializedName("valueDate")
@Expose
private String valueDate;
@SerializedName("tranCcy")
@Expose
private String tranCcy;
@SerializedName("tranAmount")
@Expose
private String tranAmount;
@SerializedName("purposeCode")
@Expose
private String purposeCode;
@SerializedName("remitInfo1")
@Expose
private String remitInfo1;
@SerializedName("remitInfo2")
@Expose
private String remitInfo2;
@SerializedName("remitInfo3")
@Expose
private String remitInfo3;
@SerializedName("remitInfo4")
@Expose
private String remitInfo4;
@SerializedName("remitInfo5")
@Expose
private String remitInfo5;
@SerializedName("remitInfo6")
@Expose
private String remitInfo6;
@SerializedName("clientCode")
@Expose
private String clientCode;
@SerializedName("paymentType")
@Expose
private String paymentType;
@SerializedName("beneAccType")
@Expose
private String beneAccType;
@SerializedName("remarks")
@Expose
private String remarks;
@SerializedName("addLine1")
@Expose
private String addLine1;
@SerializedName("addLine2")
@Expose
private String addLine2;
@SerializedName("addLine3")
@Expose
private String addLine3;
@SerializedName("addLine4")
@Expose
private String addLine4;
@SerializedName("delvMode")
@Expose
private String delvMode;
@SerializedName("printBranch")
@Expose
private String printBranch;
@SerializedName("authPersonName")
@Expose
private String authPersonName;
@SerializedName("authPersonID")
@Expose
private String authPersonID;
@SerializedName("payLocation")
@Expose
private String payLocation;
@SerializedName("routingCode")
@Expose
private String routingCode;
@SerializedName("signCode1")
@Expose
private String signCode1;
@SerializedName("signCode2")
@Expose
private String signCode2;
@SerializedName("signCode3")
@Expose
private String signCode3;
@SerializedName("signName1")
@Expose
private String signName1;
@SerializedName("signName2")
@Expose
private String signName2;
@SerializedName("signName3")
@Expose
private String signName3;
@SerializedName("beneMail")
@Expose
private String beneMail;
@SerializedName("beneMobile")
@Expose
private String beneMobile;
@SerializedName("instNumber")
@Expose
private String instNumber;
@SerializedName("instDate")
@Expose
private String instDate;
@SerializedName("corpBranch")
@Expose
private String corpBranch;
@SerializedName("filler1")
@Expose
private String filler1;
@SerializedName("filler2")
@Expose
private String filler2;
@SerializedName("filler3")
@Expose
private String filler3;
@SerializedName("filler4")
@Expose
private String filler4;
@SerializedName("filler5")
@Expose
private String filler5;
@SerializedName("filler6")
@Expose
private String filler6;
@SerializedName("filler7")
@Expose
private String filler7;
@SerializedName("filler8")
@Expose
private String filler8;
@SerializedName("filler9")
@Expose
private String filler9;
@SerializedName("filler10")
@Expose
private String filler10;
@SerializedName("filler11")
@Expose
private String filler11;
@SerializedName("filler12")
@Expose
private String filler12;
@SerializedName("filler13")
@Expose
private String filler13;
@SerializedName("filler14")
@Expose
private String filler14;
@SerializedName("filler15")
@Expose
private String filler15;
@SerializedName("filler16")
@Expose
private String filler16;
@SerializedName("filler17")
@Expose
private String filler17;
@SerializedName("filler18")
@Expose
private String filler18;
@SerializedName("filler19")
@Expose
private String filler19;
@SerializedName("filler20")
@Expose
private String filler20;
@SerializedName("filler21")
@Expose
private String filler21;
@SerializedName("filler22")
@Expose
private String filler22;
@SerializedName("filler23")
@Expose
private String filler23;
@SerializedName("filler24")
@Expose
private String filler24;
@SerializedName("filler25")
@Expose
private String filler25;
@SerializedName("filler26")
@Expose
private String filler26;
@SerializedName("filler27")
@Expose
private String filler27;
@SerializedName("filler28")
@Expose
private String filler28;
@SerializedName("filler29")
@Expose
private String filler29;
@SerializedName("filler30")
@Expose
private String filler30;
@SerializedName("filler41")
@Expose
private String filler41;
@SerializedName("filler42")
@Expose
private String filler42;
@SerializedName("filler43")
@Expose
private String filler43;
@SerializedName("filler44")
@Expose
private String filler44;
@SerializedName("filler45")
@Expose
private String filler45;
@SerializedName("filler46")
@Expose
private String filler46;
@SerializedName("filler47")
@Expose
private String filler47;
@SerializedName("filler48")
@Expose
private String filler48;
@SerializedName("filler49")
@Expose
private String filler49;
@SerializedName("filler50")
@Expose
private String filler50;
@SerializedName("filler51")
@Expose
private String filler51;
@SerializedName("filler52")
@Expose
private String filler52;
@SerializedName("filler53")
@Expose
private String filler53;
@SerializedName("filler54")
@Expose
private String filler54;
@SerializedName("invoiceDetails")
@Expose
private List<InvoiceDetail> invoiceDetails = null;
private final static long serialVersionUID = -6438109002900183659L;

/**
* No args constructor for use in serialization
*
*/
public PaymentReq() {
}

/**
*
* @param filler14
* @param filler13
* @param filler12
* @param filler11
* @param filler18
* @param beneAccType
* @param filler17
* @param filler16
* @param filler15
* @param tranCcy
* @param filler19
* @param instNumber
* @param beneName
* @param filler50
* @param payLocation
* @param beneAccNo
* @param invoiceDetails
* @param ifsc
* @param filler10
* @param filler54
* @param instDate
* @param filler53
* @param filler52
* @param beneMobile
* @param filler51
* @param signName1
* @param filler25
* @param filler24
* @param signName3
* @param filler23
* @param signName2
* @param filler22
* @param filler29
* @param filler28
* @param filler27
* @param filler26
* @param remitInfo3
* @param remitInfo4
* @param remitInfo5
* @param remitInfo6
* @param custTxnRef
* @param delvMode
* @param filler21
* @param filler20
* @param routingCode
* @param filler9
* @param filler8
* @param filler5
* @param filler4
* @param corpBranch
* @param filler7
* @param filler6
* @param filler1
* @param filler3
* @param paymentType
* @param filler2
* @param remitInfo1
* @param remitInfo2
* @param printBranch
* @param beneAddr2
* @param beneAddr1
* @param authPersonID
* @param addLine3
* @param addLine4
* @param addLine1
* @param addLine2
* @param authPersonName
* @param filler30
* @param beneMail
* @param filler47
* @param filler46
* @param filler45
* @param filler44
* @param filler49
* @param filler48
* @param valueDate
* @param signCode1
* @param clientCode
* @param purposeCode
* @param tranAmount
* @param signCode2
* @param filler43
* @param signCode3
* @param filler42
* @param remarks
* @param filler41
*/
public PaymentReq(String custTxnRef, String beneAccNo, String beneName, String beneAddr1, String beneAddr2, String ifsc, String valueDate, String tranCcy, String tranAmount, String purposeCode, String remitInfo1, String remitInfo2, String remitInfo3, String remitInfo4, String remitInfo5, String remitInfo6, String clientCode, String paymentType, String beneAccType, String remarks, String addLine1, String addLine2, String addLine3, String addLine4, String delvMode, String printBranch, String authPersonName, String authPersonID, String payLocation, String routingCode, String signCode1, String signCode2, String signCode3, String signName1, String signName2, String signName3, String beneMail, String beneMobile, String instNumber, String instDate, String corpBranch, String filler1, String filler2, String filler3, String filler4, String filler5, String filler6, String filler7, String filler8, String filler9, String filler10, String filler11, String filler12, String filler13, String filler14, String filler15, String filler16, String filler17, String filler18, String filler19, String filler20, String filler21, String filler22, String filler23, String filler24, String filler25, String filler26, String filler27, String filler28, String filler29, String filler30, String filler41, String filler42, String filler43, String filler44, String filler45, String filler46, String filler47, String filler48, String filler49, String filler50, String filler51, String filler52, String filler53, String filler54, List<InvoiceDetail> invoiceDetails) {
super();
this.custTxnRef = custTxnRef;
this.beneAccNo = beneAccNo;
this.beneName = beneName;
this.beneAddr1 = beneAddr1;
this.beneAddr2 = beneAddr2;
this.ifsc = ifsc;
this.valueDate = valueDate;
this.tranCcy = tranCcy;
this.tranAmount = tranAmount;
this.purposeCode = purposeCode;
this.remitInfo1 = remitInfo1;
this.remitInfo2 = remitInfo2;
this.remitInfo3 = remitInfo3;
this.remitInfo4 = remitInfo4;
this.remitInfo5 = remitInfo5;
this.remitInfo6 = remitInfo6;
this.clientCode = clientCode;
this.paymentType = paymentType;
this.beneAccType = beneAccType;
this.remarks = remarks;
this.addLine1 = addLine1;
this.addLine2 = addLine2;
this.addLine3 = addLine3;
this.addLine4 = addLine4;
this.delvMode = delvMode;
this.printBranch = printBranch;
this.authPersonName = authPersonName;
this.authPersonID = authPersonID;
this.payLocation = payLocation;
this.routingCode = routingCode;
this.signCode1 = signCode1;
this.signCode2 = signCode2;
this.signCode3 = signCode3;
this.signName1 = signName1;
this.signName2 = signName2;
this.signName3 = signName3;
this.beneMail = beneMail;
this.beneMobile = beneMobile;
this.instNumber = instNumber;
this.instDate = instDate;
this.corpBranch = corpBranch;
this.filler1 = filler1;
this.filler2 = filler2;
this.filler3 = filler3;
this.filler4 = filler4;
this.filler5 = filler5;
this.filler6 = filler6;
this.filler7 = filler7;
this.filler8 = filler8;
this.filler9 = filler9;
this.filler10 = filler10;
this.filler11 = filler11;
this.filler12 = filler12;
this.filler13 = filler13;
this.filler14 = filler14;
this.filler15 = filler15;
this.filler16 = filler16;
this.filler17 = filler17;
this.filler18 = filler18;
this.filler19 = filler19;
this.filler20 = filler20;
this.filler21 = filler21;
this.filler22 = filler22;
this.filler23 = filler23;
this.filler24 = filler24;
this.filler25 = filler25;
this.filler26 = filler26;
this.filler27 = filler27;
this.filler28 = filler28;
this.filler29 = filler29;
this.filler30 = filler30;
this.filler41 = filler41;
this.filler42 = filler42;
this.filler43 = filler43;
this.filler44 = filler44;
this.filler45 = filler45;
this.filler46 = filler46;
this.filler47 = filler47;
this.filler48 = filler48;
this.filler49 = filler49;
this.filler50 = filler50;
this.filler51 = filler51;
this.filler52 = filler52;
this.filler53 = filler53;
this.filler54 = filler54;
this.invoiceDetails = invoiceDetails;
}

public String getCustTxnRef() {
return custTxnRef;
}

public void setCustTxnRef(String custTxnRef) {
this.custTxnRef = custTxnRef;
}

public String getBeneAccNo() {
return beneAccNo;
}

public void setBeneAccNo(String beneAccNo) {
this.beneAccNo = beneAccNo;
}

public String getBeneName() {
return beneName;
}

public void setBeneName(String beneName) {
this.beneName = beneName;
}

public String getBeneAddr1() {
return beneAddr1;
}

public void setBeneAddr1(String beneAddr1) {
this.beneAddr1 = beneAddr1;
}

public String getBeneAddr2() {
return beneAddr2;
}

public void setBeneAddr2(String beneAddr2) {
this.beneAddr2 = beneAddr2;
}

public String getIfsc() {
return ifsc;
}

public void setIfsc(String ifsc) {
this.ifsc = ifsc;
}

public String getValueDate() {
return valueDate;
}

public void setValueDate(String valueDate) {
this.valueDate = valueDate;
}

public String getTranCcy() {
return tranCcy;
}

public void setTranCcy(String tranCcy) {
this.tranCcy = tranCcy;
}

public String getTranAmount() {
return tranAmount;
}

public void setTranAmount(String tranAmount) {
this.tranAmount = tranAmount;
}

public String getPurposeCode() {
return purposeCode;
}

public void setPurposeCode(String purposeCode) {
this.purposeCode = purposeCode;
}

public String getRemitInfo1() {
return remitInfo1;
}

public void setRemitInfo1(String remitInfo1) {
this.remitInfo1 = remitInfo1;
}

public String getRemitInfo2() {
return remitInfo2;
}

public void setRemitInfo2(String remitInfo2) {
this.remitInfo2 = remitInfo2;
}

public String getRemitInfo3() {
return remitInfo3;
}

public void setRemitInfo3(String remitInfo3) {
this.remitInfo3 = remitInfo3;
}

public String getRemitInfo4() {
return remitInfo4;
}

public void setRemitInfo4(String remitInfo4) {
this.remitInfo4 = remitInfo4;
}

public String getRemitInfo5() {
return remitInfo5;
}

public void setRemitInfo5(String remitInfo5) {
this.remitInfo5 = remitInfo5;
}

public String getRemitInfo6() {
return remitInfo6;
}

public void setRemitInfo6(String remitInfo6) {
this.remitInfo6 = remitInfo6;
}

public String getClientCode() {
return clientCode;
}

public void setClientCode(String clientCode) {
this.clientCode = clientCode;
}

public String getPaymentType() {
return paymentType;
}

public void setPaymentType(String paymentType) {
this.paymentType = paymentType;
}

public String getBeneAccType() {
return beneAccType;
}

public void setBeneAccType(String beneAccType) {
this.beneAccType = beneAccType;
}

public String getRemarks() {
return remarks;
}

public void setRemarks(String remarks) {
this.remarks = remarks;
}

public String getAddLine1() {
return addLine1;
}

public void setAddLine1(String addLine1) {
this.addLine1 = addLine1;
}

public String getAddLine2() {
return addLine2;
}

public void setAddLine2(String addLine2) {
this.addLine2 = addLine2;
}

public String getAddLine3() {
return addLine3;
}

public void setAddLine3(String addLine3) {
this.addLine3 = addLine3;
}

public String getAddLine4() {
return addLine4;
}

public void setAddLine4(String addLine4) {
this.addLine4 = addLine4;
}

public String getDelvMode() {
return delvMode;
}

public void setDelvMode(String delvMode) {
this.delvMode = delvMode;
}

public String getPrintBranch() {
return printBranch;
}

public void setPrintBranch(String printBranch) {
this.printBranch = printBranch;
}

public String getAuthPersonName() {
return authPersonName;
}

public void setAuthPersonName(String authPersonName) {
this.authPersonName = authPersonName;
}

public String getAuthPersonID() {
return authPersonID;
}

public void setAuthPersonID(String authPersonID) {
this.authPersonID = authPersonID;
}

public String getPayLocation() {
return payLocation;
}

public void setPayLocation(String payLocation) {
this.payLocation = payLocation;
}

public String getRoutingCode() {
return routingCode;
}

public void setRoutingCode(String routingCode) {
this.routingCode = routingCode;
}

public String getSignCode1() {
return signCode1;
}

public void setSignCode1(String signCode1) {
this.signCode1 = signCode1;
}

public String getSignCode2() {
return signCode2;
}

public void setSignCode2(String signCode2) {
this.signCode2 = signCode2;
}

public String getSignCode3() {
return signCode3;
}

public void setSignCode3(String signCode3) {
this.signCode3 = signCode3;
}

public String getSignName1() {
return signName1;
}

public void setSignName1(String signName1) {
this.signName1 = signName1;
}

public String getSignName2() {
return signName2;
}

public void setSignName2(String signName2) {
this.signName2 = signName2;
}

public String getSignName3() {
return signName3;
}

public void setSignName3(String signName3) {
this.signName3 = signName3;
}

public String getBeneMail() {
return beneMail;
}

public void setBeneMail(String beneMail) {
this.beneMail = beneMail;
}

public String getBeneMobile() {
return beneMobile;
}

public void setBeneMobile(String beneMobile) {
this.beneMobile = beneMobile;
}

public String getInstNumber() {
return instNumber;
}

public void setInstNumber(String instNumber) {
this.instNumber = instNumber;
}

public String getInstDate() {
return instDate;
}

public void setInstDate(String instDate) {
this.instDate = instDate;
}

public String getCorpBranch() {
return corpBranch;
}

public void setCorpBranch(String corpBranch) {
this.corpBranch = corpBranch;
}

public String getFiller1() {
return filler1;
}

public void setFiller1(String filler1) {
this.filler1 = filler1;
}

public String getFiller2() {
return filler2;
}

public void setFiller2(String filler2) {
this.filler2 = filler2;
}

public String getFiller3() {
return filler3;
}

public void setFiller3(String filler3) {
this.filler3 = filler3;
}

public String getFiller4() {
return filler4;
}

public void setFiller4(String filler4) {
this.filler4 = filler4;
}

public String getFiller5() {
return filler5;
}

public void setFiller5(String filler5) {
this.filler5 = filler5;
}

public String getFiller6() {
return filler6;
}

public void setFiller6(String filler6) {
this.filler6 = filler6;
}

public String getFiller7() {
return filler7;
}

public void setFiller7(String filler7) {
this.filler7 = filler7;
}

public String getFiller8() {
return filler8;
}

public void setFiller8(String filler8) {
this.filler8 = filler8;
}

public String getFiller9() {
return filler9;
}

public void setFiller9(String filler9) {
this.filler9 = filler9;
}

public String getFiller10() {
return filler10;
}

public void setFiller10(String filler10) {
this.filler10 = filler10;
}

public String getFiller11() {
return filler11;
}

public void setFiller11(String filler11) {
this.filler11 = filler11;
}

public String getFiller12() {
return filler12;
}

public void setFiller12(String filler12) {
this.filler12 = filler12;
}

public String getFiller13() {
return filler13;
}

public void setFiller13(String filler13) {
this.filler13 = filler13;
}

public String getFiller14() {
return filler14;
}

public void setFiller14(String filler14) {
this.filler14 = filler14;
}

public String getFiller15() {
return filler15;
}

public void setFiller15(String filler15) {
this.filler15 = filler15;
}

public String getFiller16() {
return filler16;
}

public void setFiller16(String filler16) {
this.filler16 = filler16;
}

public String getFiller17() {
return filler17;
}

public void setFiller17(String filler17) {
this.filler17 = filler17;
}

public String getFiller18() {
return filler18;
}

public void setFiller18(String filler18) {
this.filler18 = filler18;
}

public String getFiller19() {
return filler19;
}

public void setFiller19(String filler19) {
this.filler19 = filler19;
}

public String getFiller20() {
return filler20;
}

public void setFiller20(String filler20) {
this.filler20 = filler20;
}

public String getFiller21() {
return filler21;
}

public void setFiller21(String filler21) {
this.filler21 = filler21;
}

public String getFiller22() {
return filler22;
}

public void setFiller22(String filler22) {
this.filler22 = filler22;
}

public String getFiller23() {
return filler23;
}

public void setFiller23(String filler23) {
this.filler23 = filler23;
}

public String getFiller24() {
return filler24;
}

public void setFiller24(String filler24) {
this.filler24 = filler24;
}

public String getFiller25() {
return filler25;
}

public void setFiller25(String filler25) {
this.filler25 = filler25;
}

public String getFiller26() {
return filler26;
}

public void setFiller26(String filler26) {
this.filler26 = filler26;
}

public String getFiller27() {
return filler27;
}

public void setFiller27(String filler27) {
this.filler27 = filler27;
}

public String getFiller28() {
return filler28;
}

public void setFiller28(String filler28) {
this.filler28 = filler28;
}

public String getFiller29() {
return filler29;
}

public void setFiller29(String filler29) {
this.filler29 = filler29;
}

public String getFiller30() {
return filler30;
}

public void setFiller30(String filler30) {
this.filler30 = filler30;
}

public String getFiller41() {
return filler41;
}

public void setFiller41(String filler41) {
this.filler41 = filler41;
}

public String getFiller42() {
return filler42;
}

public void setFiller42(String filler42) {
this.filler42 = filler42;
}

public String getFiller43() {
return filler43;
}

public void setFiller43(String filler43) {
this.filler43 = filler43;
}

public String getFiller44() {
return filler44;
}

public void setFiller44(String filler44) {
this.filler44 = filler44;
}

public String getFiller45() {
return filler45;
}

public void setFiller45(String filler45) {
this.filler45 = filler45;
}

public String getFiller46() {
return filler46;
}

public void setFiller46(String filler46) {
this.filler46 = filler46;
}

public String getFiller47() {
return filler47;
}

public void setFiller47(String filler47) {
this.filler47 = filler47;
}

public String getFiller48() {
return filler48;
}

public void setFiller48(String filler48) {
this.filler48 = filler48;
}

public String getFiller49() {
return filler49;
}

public void setFiller49(String filler49) {
this.filler49 = filler49;
}

public String getFiller50() {
return filler50;
}

public void setFiller50(String filler50) {
this.filler50 = filler50;
}

public String getFiller51() {
return filler51;
}

public void setFiller51(String filler51) {
this.filler51 = filler51;
}

public String getFiller52() {
return filler52;
}

public void setFiller52(String filler52) {
this.filler52 = filler52;
}

public String getFiller53() {
return filler53;
}

public void setFiller53(String filler53) {
this.filler53 = filler53;
}

public String getFiller54() {
return filler54;
}

public void setFiller54(String filler54) {
this.filler54 = filler54;
}

public List<InvoiceDetail> getInvoiceDetails() {
return invoiceDetails;
}

public void setInvoiceDetails(List<InvoiceDetail> invoiceDetails) {
this.invoiceDetails = invoiceDetails;
}


}