package com.coh.lms.IDFC;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class MsgHdr implements Serializable
{

@SerializedName("msgId")
@Expose
private String msgId;
@SerializedName("cnvId")
@Expose
private String cnvId;
@SerializedName("extRefId")
@Expose
private String extRefId;
@SerializedName("bizObjId")
@Expose
private String bizObjId;
@SerializedName("appId")
@Expose
private String appId;
@SerializedName("timestamp")
@Expose
private String timestamp;
private final static long serialVersionUID = -1579942685706702044L;

/**
* No args constructor for use in serialization
*
*/
public MsgHdr() {
}

/**
*
* @param cnvId
* @param bizObjId
* @param appId
* @param msgId
* @param extRefId
* @param timestamp
*/
public MsgHdr(String msgId, String cnvId, String extRefId, String bizObjId, String appId, String timestamp) {
super();
this.msgId = msgId;
this.cnvId = cnvId;
this.extRefId = extRefId;
this.bizObjId = bizObjId;
this.appId = appId;
this.timestamp = timestamp;
}

public String getMsgId() {
return msgId;
}

public void setMsgId(String msgId) {
this.msgId = msgId;
}

public String getCnvId() {
return cnvId;
}

public void setCnvId(String cnvId) {
this.cnvId = cnvId;
}

public String getExtRefId() {
return extRefId;
}

public void setExtRefId(String extRefId) {
this.extRefId = extRefId;
}

public String getBizObjId() {
return bizObjId;
}

public void setBizObjId(String bizObjId) {
this.bizObjId = bizObjId;
}

public String getAppId() {
return appId;
}

public void setAppId(String appId) {
this.appId = appId;
}

public String getTimestamp() {
return timestamp;
}

public void setTimestamp(String timestamp) {
this.timestamp = timestamp;
}



}