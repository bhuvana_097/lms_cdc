// Generated with g9.

package com.vg.lms.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity(name="lms_asset")
public class Asset implements Serializable, Comparable<Asset> {

   

    /**
	 * 
	 */
	private static final long serialVersionUID = -875819542557467767L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="asset_type", length=20)
    private String assetType;
    @Column(length=100)
    private String make;
    @Column(length=100)
    private String model;
    @Column(length=15)
    private String variant;
    @Column(name="engine_no", length=15)
    private String engineNo;
    @Column(name="chassis_no", length=20)
    private String chassisNo;
    @Column(name="reg_no", length=12)
    private String regNo;
    @Column(length=12)
    private String color;
    @Column(name="key_available", nullable=false, length=3)
    private boolean keyAvailable;
    @Column(name="rc_available", nullable=false, length=3)
    private boolean rcAvailable;
    @Column(name="fin_name", length=25)
    private String finName;
    @Column(name="fin_address", length=40)
    private String finAddress;
    @Column(name="fin_from", length=13)
    private String finFrom;
    @Column(name="scheme_type", length=50)
    private String schemeType;
    @Column(name="prod_subcode", length=50)
    private String prodSubcode;
    @Column(name="total_cost", precision=10, scale=2)
    private float totalCost;
    @Column(name="grid_price", precision=10, scale=2)
    private float gridPrice;
    @Column(name="onroad_price", precision=10, scale=2)
    private float onroadPrice;
    @Column(name="actual_ltv", precision=10, scale=2)
    private float actualLtv;
    @Column(name="rc_no", length=20)
    private String rcNo;
    @Column(name="rc_photo", length=30)
    private String rcPhoto;
    
    @Column(name="ins_no", length=20)
    private String insNo;
    
    @Column(name="ins_provider", length=20)
    private String insProvider;
    
    @Column(name="create_user", length=10)
    private String createUser;
    /*@Column(name="create_time")
    private Date createTime;*/
    @Column(name="modify_user", length=10)
    private String modifyUser;
   /* @Column(name="modify_time")
    private Date modifyTime;*/
    @OneToOne
    @JoinColumn(name="loan_id")
    private Loan loan;

    /** Default constructor. */
    public Asset() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for assetType.
     *
     * @return the current value of assetType
     */
    public String getAssetType() {
        return assetType;
    }

    /**
     * Setter method for assetType.
     *
     * @param aAssetType the new value for assetType
     */
    public void setAssetType(String aAssetType) {
        assetType = aAssetType;
    }

    /**
     * Access method for make.
     *
     * @return the current value of make
     */
    public String getMake() {
        return make;
    }

    /**
     * Setter method for make.
     *
     * @param aMake the new value for make
     */
    public void setMake(String aMake) {
        make = aMake;
    }

    /**
     * Access method for model.
     *
     * @return the current value of model
     */
    public String getModel() {
        return model;
    }

    /**
     * Setter method for model.
     *
     * @param aModel the new value for model
     */
    public void setModel(String aModel) {
        model = aModel;
    }

    /**
     * Access method for variant.
     *
     * @return the current value of variant
     */
    public String getVariant() {
        return variant;
    }

    /**
     * Setter method for variant.
     *
     * @param aVariant the new value for variant
     */
    public void setVariant(String aVariant) {
        variant = aVariant;
    }

    /**
     * Access method for engineNo.
     *
     * @return the current value of engineNo
     */
    public String getEngineNo() {
        return engineNo;
    }

    /**
     * Setter method for engineNo.
     *
     * @param aEngineNo the new value for engineNo
     */
    public void setEngineNo(String aEngineNo) {
        engineNo = aEngineNo;
    }

    /**
     * Access method for chassisNo.
     *
     * @return the current value of chassisNo
     */
    public String getChassisNo() {
        return chassisNo;
    }

    /**
     * Setter method for chassisNo.
     *
     * @param aChassisNo the new value for chassisNo
     */
    public void setChassisNo(String aChassisNo) {
        chassisNo = aChassisNo;
    }

    /**
     * Access method for regNo.
     *
     * @return the current value of regNo
     */
    public String getRegNo() {
        return regNo;
    }

    /**
     * Setter method for regNo.
     *
     * @param aRegNo the new value for regNo
     */
    public void setRegNo(String aRegNo) {
        regNo = aRegNo;
    }

    /**
     * Access method for color.
     *
     * @return the current value of color
     */
    public String getColor() {
        return color;
    }

    /**
     * Setter method for color.
     *
     * @param aColor the new value for color
     */
    public void setColor(String aColor) {
        color = aColor;
    }

    /**
     * Access method for finName.
     *
     * @return the current value of finName
     */
    public String getFinName() {
        return finName;
    }

    /**
     * Setter method for finName.
     *
     * @param aFinName the new value for finName
     */
    public void setFinName(String aFinName) {
        finName = aFinName;
    }

    /**
     * Access method for finAddress.
     *
     * @return the current value of finAddress
     */
    public String getFinAddress() {
        return finAddress;
    }

    /**
     * Setter method for finAddress.
     *
     * @param aFinAddress the new value for finAddress
     */
    public void setFinAddress(String aFinAddress) {
        finAddress = aFinAddress;
    }

    /**
     * Access method for finFrom.
     *
     * @return the current value of finFrom
     */
    public String getFinFrom() {
        return finFrom;
    }

    /**
     * Setter method for finFrom.
     *
     * @param aFinFrom the new value for finFrom
     */
    public void setFinFrom(String aFinFrom) {
        finFrom = aFinFrom;
    }

    /**
     * Access method for schemeType.
     *
     * @return the current value of schemeType
     */
    public String getSchemeType() {
        return schemeType;
    }

    /**
     * Setter method for schemeType.
     *
     * @param aSchemeType the new value for schemeType
     */
    public void setSchemeType(String aSchemeType) {
        schemeType = aSchemeType;
    }

    /**
     * Access method for prodSubcode.
     *
     * @return the current value of prodSubcode
     */
    public String getProdSubcode() {
        return prodSubcode;
    }

    /**
     * Setter method for prodSubcode.
     *
     * @param aProdSubcode the new value for prodSubcode
     */
    public void setProdSubcode(String aProdSubcode) {
        prodSubcode = aProdSubcode;
    }

    /**
     * Access method for totalCost.
     *
     * @return the current value of totalCost
     */
    public float getTotalCost() {
        return totalCost;
    }

    /**
     * Setter method for totalCost.
     *
     * @param aTotalCost the new value for totalCost
     */
    public void setTotalCost(float aTotalCost) {
        totalCost = aTotalCost;
    }

    /**
     * Access method for gridPrice.
     *
     * @return the current value of gridPrice
     */
    public float getGridPrice() {
        return gridPrice;
    }

    /**
     * Setter method for gridPrice.
     *
     * @param aGridPrice the new value for gridPrice
     */
    public void setGridPrice(float aGridPrice) {
        gridPrice = aGridPrice;
    }

    /**
     * Access method for onroadPrice.
     *
     * @return the current value of onroadPrice
     */
    public float getOnroadPrice() {
        return onroadPrice;
    }

    /**
     * Setter method for onroadPrice.
     *
     * @param aOnroadPrice the new value for onroadPrice
     */
    public void setOnroadPrice(float aOnroadPrice) {
        onroadPrice = aOnroadPrice;
    }

    /**
     * Access method for actualLtv.
     *
     * @return the current value of actualLtv
     */
    public float getActualLtv() {
        return actualLtv;
    }

    /**
     * Setter method for actualLtv.
     *
     * @param aActualLtv the new value for actualLtv
     */
    public void setActualLtv(float aActualLtv) {
        actualLtv = aActualLtv;
    }

    /**
     * Access method for rcNo.
     *
     * @return the current value of rcNo
     */
    public String getRcNo() {
        return rcNo;
    }

    /**
     * Setter method for rcNo.
     *
     * @param aRcNo the new value for rcNo
     */
    public void setRcNo(String aRcNo) {
        rcNo = aRcNo;
    }

    /**
     * Access method for rcPhoto.
     *
     * @return the current value of rcPhoto
     */
    public String getRcPhoto() {
        return rcPhoto;
    }

    /**
     * Setter method for rcPhoto.
     *
     * @param aRcPhoto the new value for rcPhoto
     */
    public void setRcPhoto(String aRcPhoto) {
        rcPhoto = aRcPhoto;
    }

    /**
     * Access method for createUser.
     *
     * @return the current value of createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * Setter method for createUser.
     *
     * @param aCreateUser the new value for createUser
     */
    public void setCreateUser(String aCreateUser) {
        createUser = aCreateUser;
    }

    /**
     * Access method for modifyUser.
     *
     * @return the current value of modifyUser
     */
    public String getModifyUser() {
        return modifyUser;
    }

    /**
     * Setter method for modifyUser.
     *
     * @param aModifyUser the new value for modifyUser
     */
    public void setModifyUser(String aModifyUser) {
        modifyUser = aModifyUser;
    }

    /**
     * Access method for loan.
     *
     * @return the current value of loan
     */
    public Loan getLoan() {
        return loan;
    }

    /**
     * Setter method for loan.
     *
     * @param aLoan the new value for loan
     */
    public void setLoan(Loan aLoan) {
        loan = aLoan;
    }  

	public boolean isKeyAvailable() {
		return keyAvailable;
	}

	public void setKeyAvailable(boolean keyAvailable) {
		this.keyAvailable = keyAvailable;
	}

	public boolean isRcAvailable() {
		return rcAvailable;
	}

	public void setRcAvailable(boolean rcAvailable) {
		this.rcAvailable = rcAvailable;
	}	

	public String getInsNo() {
		return insNo;
	}

	public void setInsNo(String insNo) {
		this.insNo = insNo;
	}

	public String getInsProvider() {
		return insProvider;
	}

	public void setInsProvider(String insProvider) {
		this.insProvider = insProvider;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(actualLtv);
		result = prime * result + ((assetType == null) ? 0 : assetType.hashCode());
		result = prime * result + ((chassisNo == null) ? 0 : chassisNo.hashCode());
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + ((createUser == null) ? 0 : createUser.hashCode());
		result = prime * result + ((engineNo == null) ? 0 : engineNo.hashCode());
		result = prime * result + ((finAddress == null) ? 0 : finAddress.hashCode());
		result = prime * result + ((finFrom == null) ? 0 : finFrom.hashCode());
		result = prime * result + ((finName == null) ? 0 : finName.hashCode());
		result = prime * result + Float.floatToIntBits(gridPrice);
		result = prime * result + id;
		result = prime * result + (keyAvailable ? 1231 : 1237);
		result = prime * result + ((make == null) ? 0 : make.hashCode());
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + ((modifyUser == null) ? 0 : modifyUser.hashCode());
		result = prime * result + Float.floatToIntBits(onroadPrice);
		result = prime * result + ((prodSubcode == null) ? 0 : prodSubcode.hashCode());
		result = prime * result + (rcAvailable ? 1231 : 1237);
		result = prime * result + ((rcNo == null) ? 0 : rcNo.hashCode());
		result = prime * result + ((insNo == null) ? 0 : insNo.hashCode());
		result = prime * result + ((insProvider == null) ? 0 : insProvider.hashCode());
		result = prime * result + ((rcPhoto == null) ? 0 : rcPhoto.hashCode());
		result = prime * result + ((regNo == null) ? 0 : regNo.hashCode());
		result = prime * result + ((schemeType == null) ? 0 : schemeType.hashCode());
		result = prime * result + Float.floatToIntBits(totalCost);
		result = prime * result + ((variant == null) ? 0 : variant.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Asset other = (Asset) obj;
		if (Float.floatToIntBits(actualLtv) != Float.floatToIntBits(other.actualLtv))
			return false;
		if (assetType == null) {
			if (other.assetType != null)
				return false;
		} else if (!assetType.equals(other.assetType))
			return false;
		if (chassisNo == null) {
			if (other.chassisNo != null)
				return false;
		} else if (!chassisNo.equals(other.chassisNo))
			return false;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		if (createUser == null) {
			if (other.createUser != null)
				return false;
		} else if (!createUser.equals(other.createUser))
			return false;
		if (engineNo == null) {
			if (other.engineNo != null)
				return false;
		} else if (!engineNo.equals(other.engineNo))
			return false;
		if (finAddress == null) {
			if (other.finAddress != null)
				return false;
		} else if (!finAddress.equals(other.finAddress))
			return false;
		if (finFrom == null) {
			if (other.finFrom != null)
				return false;
		} else if (!finFrom.equals(other.finFrom))
			return false;
		if (finName == null) {
			if (other.finName != null)
				return false;
		} else if (!finName.equals(other.finName))
			return false;
		if (Float.floatToIntBits(gridPrice) != Float.floatToIntBits(other.gridPrice))
			return false;
		if (id != other.id)
			return false;
		if (keyAvailable != other.keyAvailable)
			return false;
		if (make == null) {
			if (other.make != null)
				return false;
		} else if (!make.equals(other.make))
			return false;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		if (modifyUser == null) {
			if (other.modifyUser != null)
				return false;
		} else if (!modifyUser.equals(other.modifyUser))
			return false;
		if (Float.floatToIntBits(onroadPrice) != Float.floatToIntBits(other.onroadPrice))
			return false;
		if (prodSubcode == null) {
			if (other.prodSubcode != null)
				return false;
		} else if (!prodSubcode.equals(other.prodSubcode))
			return false;
		if (rcAvailable != other.rcAvailable)
			return false;
		if (rcNo == null) {
			if (other.rcNo != null)
				return false;
		} else if (!rcNo.equals(other.rcNo))
			return false;
		
		if (insNo == null) {
			if (other.insNo != null)
				return false;
		} else if (!insNo.equals(other.insNo))
			return false;
		
		if (insProvider == null) {
			if (other.insProvider != null)
				return false;
		} else if (!insProvider.equals(other.insProvider))
			return false;
		
		if (rcPhoto == null) {
			if (other.rcPhoto != null)
				return false;
		} else if (!rcPhoto.equals(other.rcPhoto))
			return false;
		if (regNo == null) {
			if (other.regNo != null)
				return false;
		} else if (!regNo.equals(other.regNo))
			return false;
		if (schemeType == null) {
			if (other.schemeType != null)
				return false;
		} else if (!schemeType.equals(other.schemeType))
			return false;
		if (Float.floatToIntBits(totalCost) != Float.floatToIntBits(other.totalCost))
			return false;
		if (variant == null) {
			if (other.variant != null)
				return false;
		} else if (!variant.equals(other.variant))
			return false;
		return true;
	}

	@Override
	public int compareTo(Asset compareObj) {
		return this.id - compareObj.getId();
	}

 

}
