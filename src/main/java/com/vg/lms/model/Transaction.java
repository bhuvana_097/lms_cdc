// Generated with g9.

package com.vg.lms.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="lms_transaction")
public class Transaction implements Serializable,Comparable<Transaction> {

   

    /**
	 * 
	 */
	private static final long serialVersionUID = -4668532966646227759L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="txn_date")
    private Date txnDate;
    @Column(name="val_date")
    private Date valDate;
    @Column(name="txn_type", length=10)
    private String txnType;
    @Column(precision=10, scale=2)
    private float debit;
    @Column(precision=10, scale=2)
    private float credit;
    @Column(name="create_user", length=10)
    private String createUser;
    
    @Column(name="modify_user", length=10)
    private String modifyUser;
   
   /* @ManyToOne
    @JoinColumn(name="note_id")
    private CrdrNote crdrNote;*/
    @ManyToOne
    @JoinColumn(name="receivable_id")
    private Receivable receivable;
    @ManyToOne
    @JoinColumn(name="loan_id")
    private Loan loan;

    /** Default constructor. */
    public Transaction() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for txnDate.
     *
     * @return the current value of txnDate
     */
    public Date getTxnDate() {
        return txnDate;
    }

    /**
     * Setter method for txnDate.
     *
     * @param aTxnDate the new value for txnDate
     */
    public void setTxnDate(Date aTxnDate) {
        txnDate = aTxnDate;
    }

    /**
     * Access method for valDate.
     *
     * @return the current value of valDate
     */
    public Date getValDate() {
        return valDate;
    }

    /**
     * Setter method for valDate.
     *
     * @param aValDate the new value for valDate
     */
    public void setValDate(Date aValDate) {
        valDate = aValDate;
    }

    /**
     * Access method for txnType.
     *
     * @return the current value of txnType
     */
    public String getTxnType() {
        return txnType;
    }

    /**
     * Setter method for txnType.
     *
     * @param aTxnType the new value for txnType
     */
    public void setTxnType(String aTxnType) {
        txnType = aTxnType;
    }

    /**
     * Access method for debit.
     *
     * @return the current value of debit
     */
    public float getDebit() {
        return debit;
    }

    /**
     * Setter method for debit.
     *
     * @param aDebit the new value for debit
     */
    public void setDebit(float aDebit) {
        debit = aDebit;
    }

    /**
     * Access method for credit.
     *
     * @return the current value of credit
     */
    public float getCredit() {
        return credit;
    }

    /**
     * Setter method for credit.
     *
     * @param aCredit the new value for credit
     */
    public void setCredit(float aCredit) {
        credit = aCredit;
    }

    /**
     * Access method for createUser.
     *
     * @return the current value of createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * Setter method for createUser.
     *
     * @param aCreateUser the new value for createUser
     */
    public void setCreateUser(String aCreateUser) {
        createUser = aCreateUser;
    }

    
    /**
     * Access method for modifyUser.
     *
     * @return the current value of modifyUser
     */
    public String getModifyUser() {
        return modifyUser;
    }

    /**
     * Setter method for modifyUser.
     *
     * @param aModifyUser the new value for modifyUser
     */
    public void setModifyUser(String aModifyUser) {
        modifyUser = aModifyUser;
    }

    /**
     * Access method for crdrNote.
     *
     * @return the current value of crdrNote
     */
   /* public CrdrNote getCrdrNote() {
        return crdrNote;
    }
*/
    /**
     * Setter method for crdrNote.
     *
     * @param aCrdrNote the new value for crdrNote
     */
    /*public void setCrdrNote(CrdrNote aCrdrNote) {
        crdrNote = aCrdrNote;
    }*/

    /**
     * Access method for receivable.
     *
     * @return the current value of receivable
     */
    public Receivable getReceivable() {
        return receivable;
    }

    /**
     * Setter method for receivable.
     *
     * @param aReceivable the new value for receivable
     */
    public void setReceivable(Receivable aReceivable) {
        receivable = aReceivable;
    }

    /**
     * Access method for loan.
     *
     * @return the current value of loan
     */
    public Loan getLoan() {
        return loan;
    }

    /**
     * Setter method for loan.
     *
     * @param aLoan the new value for loan
     */
    public void setLoan(Loan aLoan) {
        loan = aLoan;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((createUser == null) ? 0 : createUser.hashCode());
		result = prime * result + Float.floatToIntBits(credit);
		result = prime * result + Float.floatToIntBits(debit);
		result = prime * result + id;
		result = prime * result + ((modifyUser == null) ? 0 : modifyUser.hashCode());
		result = prime * result + ((txnDate == null) ? 0 : txnDate.hashCode());
		result = prime * result + ((txnType == null) ? 0 : txnType.hashCode());
		result = prime * result + ((valDate == null) ? 0 : valDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transaction other = (Transaction) obj;
		if (createUser == null) {
			if (other.createUser != null)
				return false;
		} else if (!createUser.equals(other.createUser))
			return false;
		if (Float.floatToIntBits(credit) != Float.floatToIntBits(other.credit))
			return false;
		if (Float.floatToIntBits(debit) != Float.floatToIntBits(other.debit))
			return false;
		if (id != other.id)
			return false;
		if (modifyUser == null) {
			if (other.modifyUser != null)
				return false;
		} else if (!modifyUser.equals(other.modifyUser))
			return false;
		if (txnDate == null) {
			if (other.txnDate != null)
				return false;
		} else if (!txnDate.equals(other.txnDate))
			return false;
		if (txnType == null) {
			if (other.txnType != null)
				return false;
		} else if (!txnType.equals(other.txnType))
			return false;
		if (valDate == null) {
			if (other.valDate != null)
				return false;
		} else if (!valDate.equals(other.valDate))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format(
				"Transaction [id=%s, txnDate=%s, valDate=%s, txnType=%s, debit=%s, credit=%s, createUser=%s, createTime=%s, modifyUser=%s, modifyTime=%s, receivable=%s, loan=%s]",
				id, txnDate, valDate, txnType, debit, credit, createUser, modifyUser,
				receivable, loan);
	}

	@Override
	public int compareTo(Transaction compareObj) {
		return this.id - compareObj.getId();
	}

	



}
