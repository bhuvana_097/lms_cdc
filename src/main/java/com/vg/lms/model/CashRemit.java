// Generated with g9.

package com.vg.lms.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="cash_remit")
public class CashRemit implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 9113059174122735710L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="remittance_no", length=50)
    private String remittanceNo;
    @Column(name="ao_code", length=15)
    private String aoCode;
    @Column(name="webuser_code", length=20)
    private String webuserCode;
    @Column(name="collection_agent_code", length=15)
    private String collectionAgentCode;
    @Column(name="from_receipt", precision=19)
    private long fromReceipt;
    @Column(name="to_receipt", precision=19)
    private long toReceipt;
    private Date date;
    @Column(precision=15, scale=2)
    private float amount;
    @Column(name="bank_name", length=20)
    private String bankName;
    @Column(length=30)
    private String branch;
    @Column(length=30)
    private String state;
    @Column(name="remit_approved", length=2)
    private String remitApproved;
    @Column(name="app_or_rej_time")
    private Date appOrRejTime;

    /** Default constructor. */
    public CashRemit() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for remittanceNo.
     *
     * @return the current value of remittanceNo
     */
    public String getRemittanceNo() {
        return remittanceNo;
    }

    /**
     * Setter method for remittanceNo.
     *
     * @param aRemittanceNo the new value for remittanceNo
     */
    public void setRemittanceNo(String aRemittanceNo) {
        remittanceNo = aRemittanceNo;
    }

    /**
     * Access method for aoCode.
     *
     * @return the current value of aoCode
     */
    public String getAoCode() {
        return aoCode;
    }

    /**
     * Setter method for aoCode.
     *
     * @param aAoCode the new value for aoCode
     */
    public void setAoCode(String aAoCode) {
        aoCode = aAoCode;
    }

    /**
     * Access method for webuserCode.
     *
     * @return the current value of webuserCode
     */
    public String getWebuserCode() {
        return webuserCode;
    }

    /**
     * Setter method for webuserCode.
     *
     * @param aWebuserCode the new value for webuserCode
     */
    public void setWebuserCode(String aWebuserCode) {
        webuserCode = aWebuserCode;
    }

    /**
     * Access method for collectionAgentCode.
     *
     * @return the current value of collectionAgentCode
     */
    public String getCollectionAgentCode() {
        return collectionAgentCode;
    }

    /**
     * Setter method for collectionAgentCode.
     *
     * @param aCollectionAgentCode the new value for collectionAgentCode
     */
    public void setCollectionAgentCode(String aCollectionAgentCode) {
        collectionAgentCode = aCollectionAgentCode;
    }

    /**
     * Access method for fromReceipt.
     *
     * @return the current value of fromReceipt
     */
    public long getFromReceipt() {
        return fromReceipt;
    }

    /**
     * Setter method for fromReceipt.
     *
     * @param aFromReceipt the new value for fromReceipt
     */
    public void setFromReceipt(long aFromReceipt) {
        fromReceipt = aFromReceipt;
    }

    /**
     * Access method for toReceipt.
     *
     * @return the current value of toReceipt
     */
    public long getToReceipt() {
        return toReceipt;
    }

    /**
     * Setter method for toReceipt.
     *
     * @param aToReceipt the new value for toReceipt
     */
    public void setToReceipt(long aToReceipt) {
        toReceipt = aToReceipt;
    }

    /**
     * Access method for date.
     *
     * @return the current value of date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Setter method for date.
     *
     * @param aDate the new value for date
     */
    public void setDate(Date aDate) {
        date = aDate;
    }

    /**
     * Access method for amount.
     *
     * @return the current value of amount
     */
    public float getAmount() {
        return amount;
    }

    /**
     * Setter method for amount.
     *
     * @param aAmount the new value for amount
     */
    public void setAmount(float aAmount) {
        amount = aAmount;
    }

    /**
     * Access method for bankName.
     *
     * @return the current value of bankName
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * Setter method for bankName.
     *
     * @param aBankName the new value for bankName
     */
    public void setBankName(String aBankName) {
        bankName = aBankName;
    }

    /**
     * Access method for branch.
     *
     * @return the current value of branch
     */
    public String getBranch() {
        return branch;
    }

    /**
     * Setter method for branch.
     *
     * @param aBranch the new value for branch
     */
    public void setBranch(String aBranch) {
        branch = aBranch;
    }

    /**
     * Access method for state.
     *
     * @return the current value of state
     */
    public String getState() {
        return state;
    }

    /**
     * Setter method for state.
     *
     * @param aState the new value for state
     */
    public void setState(String aState) {
        state = aState;
    }

    /**
     * Access method for remitApproved.
     *
     * @return the current value of remitApproved
     */
    public String getRemitApproved() {
        return remitApproved;
    }

    /**
     * Setter method for remitApproved.
     *
     * @param aRemitApproved the new value for remitApproved
     */
    public void setRemitApproved(String aRemitApproved) {
        remitApproved = aRemitApproved;
    }

    /**
     * Access method for appOrRejTime.
     *
     * @return the current value of appOrRejTime
     */
    public Date getAppOrRejTime() {
        return appOrRejTime;
    }

    /**
     * Setter method for appOrRejTime.
     *
     * @param aAppOrRejTime the new value for appOrRejTime
     */
    public void setAppOrRejTime(Date aAppOrRejTime) {
        appOrRejTime = aAppOrRejTime;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(amount);
		result = prime * result + ((aoCode == null) ? 0 : aoCode.hashCode());
		result = prime * result + ((appOrRejTime == null) ? 0 : appOrRejTime.hashCode());
		result = prime * result + ((bankName == null) ? 0 : bankName.hashCode());
		result = prime * result + ((branch == null) ? 0 : branch.hashCode());
		result = prime * result + ((collectionAgentCode == null) ? 0 : collectionAgentCode.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + (int) (fromReceipt ^ (fromReceipt >>> 32));
		result = prime * result + id;
		result = prime * result + ((remitApproved == null) ? 0 : remitApproved.hashCode());
		result = prime * result + ((remittanceNo == null) ? 0 : remittanceNo.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + (int) (toReceipt ^ (toReceipt >>> 32));
		result = prime * result + ((webuserCode == null) ? 0 : webuserCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CashRemit other = (CashRemit) obj;
		if (Float.floatToIntBits(amount) != Float.floatToIntBits(other.amount))
			return false;
		if (aoCode == null) {
			if (other.aoCode != null)
				return false;
		} else if (!aoCode.equals(other.aoCode))
			return false;
		if (appOrRejTime == null) {
			if (other.appOrRejTime != null)
				return false;
		} else if (!appOrRejTime.equals(other.appOrRejTime))
			return false;
		if (bankName == null) {
			if (other.bankName != null)
				return false;
		} else if (!bankName.equals(other.bankName))
			return false;
		if (branch == null) {
			if (other.branch != null)
				return false;
		} else if (!branch.equals(other.branch))
			return false;
		if (collectionAgentCode == null) {
			if (other.collectionAgentCode != null)
				return false;
		} else if (!collectionAgentCode.equals(other.collectionAgentCode))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (fromReceipt != other.fromReceipt)
			return false;
		if (id != other.id)
			return false;
		if (remitApproved == null) {
			if (other.remitApproved != null)
				return false;
		} else if (!remitApproved.equals(other.remitApproved))
			return false;
		if (remittanceNo == null) {
			if (other.remittanceNo != null)
				return false;
		} else if (!remittanceNo.equals(other.remittanceNo))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (toReceipt != other.toReceipt)
			return false;
		if (webuserCode == null) {
			if (other.webuserCode != null)
				return false;
		} else if (!webuserCode.equals(other.webuserCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format(
				"CashRemit [id=%s, remittanceNo=%s, aoCode=%s, webuserCode=%s, collectionAgentCode=%s, fromReceipt=%s, toReceipt=%s, date=%s, amount=%s, bankName=%s, branch=%s, state=%s, remitApproved=%s, appOrRejTime=%s]",
				id, remittanceNo, aoCode, webuserCode, collectionAgentCode, fromReceipt, toReceipt, date, amount,
				bankName, branch, state, remitApproved, appOrRejTime);
	}

  
}
