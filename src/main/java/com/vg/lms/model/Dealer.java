package com.vg.lms.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="dealer_master")
public class Dealer  implements Serializable{
	@Override
	public String toString() {
		return "Dealer [dealerType=" + dealerType + ", mainDealerCode=" + mainDealerCode + ", dealerName=" + dealerName
				+ ", paymentDealer=" + paymentDealer + ", iFsc=" + iFsc + ", accNo=" + accNo + ", dealerGlCode="
				+ dealerGlCode + ", dealerCode=" + dealerCode + ", dealerState=" + dealerState + "]";
	}
	public String getDealerType() {
		return dealerType;
	}
	public void setDealerType(String dealerType) {
		this.dealerType = dealerType;
	}
	public String getMainDealerCode() {
		return mainDealerCode;
	}
	public void setMainDealerCode(String mainDealerCode) {
		this.mainDealerCode = mainDealerCode;
	}
	public String getDealerName() {
		return dealerName;
	}
	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}
	public String getPaymentDealer() {
		return paymentDealer;
	}
	public void setPaymentDealer(String paymentDealer) {
		this.paymentDealer = paymentDealer;
	}
	public String getiFsc() {
		return iFsc;
	}
	public void setiFsc(String iFsc) {
		this.iFsc = iFsc;
	}
	public String getAccNo() {
		return accNo;
	}
	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}
	public String getDealerGlCode() {
		return dealerGlCode;
	}
	public void setDealerGlCode(String dealerGlCode) {
		this.dealerGlCode = dealerGlCode;
	}
	public int getDealerCode() {
		return dealerCode;
	}
	public void setDealerCode(int dealerCode) {
		this.dealerCode = dealerCode;
	}
	public int getDealerState() {
		return dealerState;
	}
	public void setDealerState(int dealerState) {
		this.dealerState = dealerState;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	private static final long serialVersionUID = 2361447040422992317L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
       @Column(name="dealer_type", length=100)
    private String dealerType;
    @Column(name="main_dealer_code", length=30)
    private String mainDealerCode;
    @Column(name="dealer_name", length=100)
    private String dealerName;
     @Column(name="payment_dealer", length=20)
    private String paymentDealer;
    @Column(name="ifsc", length=100)
    private String iFsc;
    @Column(name="acc_no", length=50)
    private String accNo;
    @Column(name="dealer_gl_code", length=500)
    private String dealerGlCode;
    @Column(name="dealer_code", length=500)
    private int dealerCode;
    @Column(name="state", length=500)
    private int dealerState;
    
}
