package com.vg.lms.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity(name="spl_waiver_scheme_details")
public class SplWaiver implements Serializable  {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8634290503878895664L;

	@Id
 @GeneratedValue(strategy=GenerationType.IDENTITY)
 @Column(unique=true, nullable=false, precision=10)
 private int id;

 @Column(name="loan_no", length=20)
 private String loanNo;

 @Column(name="scheme_type", length=50)
 private String schemeType;
 
 @Column(name="scheme_percentage", length=10)
 private String schemePercentage;

 @Column(name="tbc_prin_arrears", precision=15, scale=2)
 private String tbcPrinArrears;
 
 @Column(name="tbc_prin_os", precision=15, scale=2)
 private String tbcPrinOS;
 
 
 @Column(name="tbc_int_on_pos_since_last_due", precision=15, scale=2)
 private String tbcIntOnPosSinceLastDue;
 
 
 @Column(name="tbc_odc_due", precision=15, scale=2)
 private String tbcOdcDue;
 
 @Column(name="tbc_moratorium_int", precision=15, scale=2)
 private String tbcMoratoriumInt;
 
 @Column(name="tbc_fcl_charges", precision=15, scale=2)
 private String tbcFclCharges;
 
 @Column(name="tbc_charges_due", precision=15, scale=2)
 private String tbcChargesDue;
 
 @Column(name="tbc_fcl_amount", precision=15, scale=2)
 private String tbcFclAmount;

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getLoanNo() {
	return loanNo;
}

public void setLoanNo(String loanNo) {
	this.loanNo = loanNo;
}

public String getSchemeType() {
	return schemeType;
}

public void setSchemeType(String schemeType) {
	this.schemeType = schemeType;
}

public String getSchemePercentage() {
	return schemePercentage;
}

public void setSchemePercentage(String schemePercentage) {
	this.schemePercentage = schemePercentage;
}

public String getTbcPrinArrears() {
	return tbcPrinArrears;
}

public void setTbcPrinArrears(String tbcPrinArrears) {
	this.tbcPrinArrears = tbcPrinArrears;
}

public String getTbcPrinOS() {
	return tbcPrinOS;
}

public void setTbcPrinOS(String tbcPrinOS) {
	this.tbcPrinOS = tbcPrinOS;
}

public String getTbcIntOnPosSinceLastDue() {
	return tbcIntOnPosSinceLastDue;
}

public void setTbcIntOnPosSinceLastDue(String tbcIntOnPosSinceLastDue) {
	this.tbcIntOnPosSinceLastDue = tbcIntOnPosSinceLastDue;
}

public String getTbcOdcDue() {
	return tbcOdcDue;
}

public void setTbcOdcDue(String tbcOdcDue) {
	this.tbcOdcDue = tbcOdcDue;
}

public String getTbcMoratoriumInt() {
	return tbcMoratoriumInt;
}

public void setTbcMoratoriumInt(String tbcMoratoriumInt) {
	this.tbcMoratoriumInt = tbcMoratoriumInt;
}

public String getTbcFclCharges() {
	return tbcFclCharges;
}

public void setTbcFclCharges(String tbcFclCharges) {
	this.tbcFclCharges = tbcFclCharges;
}

public String getTbcChargesDue() {
	return tbcChargesDue;
}

public void setTbcChargesDue(String tbcChargesDue) {
	this.tbcChargesDue = tbcChargesDue;
}

public String getTbcFclAmount() {
	return tbcFclAmount;
}

public void setTbcFclAmount(String tbcFclAmount) {
	this.tbcFclAmount = tbcFclAmount;
}

public static long getSerialversionuid() {
	return serialVersionUID;
}

@Override
public String toString() {
	return "SplWaiver [id=" + id + ", brnetClientId=" + loanNo + ", schemeType=" + schemeType
			+ ", schemePercentage=" + schemePercentage + ", tbcPrinArrears=" + tbcPrinArrears + ", tbcPrinOS="
			+ tbcPrinOS + ", tbcIntOnPosSinceLastDue=" + tbcIntOnPosSinceLastDue + ", tbcOdcDue=" + tbcOdcDue
			+ ", tbcMoratoriumInt=" + tbcMoratoriumInt + ", tbcFclCharges=" + tbcFclCharges + ", tbcChargesDue="
			+ tbcChargesDue + ", tbcFclAmount=" + tbcFclAmount + ", getId()=" + getId() + ", getBrnetClientId()="
			+ getLoanNo() + ", getSchemeType()=" + getSchemeType() + ", getSchemePercentage()="
			+ getSchemePercentage() + ", getTbcPrinArrears()=" + getTbcPrinArrears() + ", getTbcPrinOS()="
			+ getTbcPrinOS() + ", getTbcIntOnPosSinceLastDue()=" + getTbcIntOnPosSinceLastDue() + ", getTbcOdcDue()="
			+ getTbcOdcDue() + ", getTbcMoratoriumInt()=" + getTbcMoratoriumInt() + ", getTbcFclCharges()="
			+ getTbcFclCharges() + ", getTbcChargesDue()=" + getTbcChargesDue() + ", getTbcFclAmount()="
			+ getTbcFclAmount() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
			+ super.toString() + "]";
}
 
  }
