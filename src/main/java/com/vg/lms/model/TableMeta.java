// Generated with g9.

package com.vg.lms.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="lms_table_meta")
public class TableMeta implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5840659194190949232L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="table_name", length=50)
    private String tableName;
    @Column(name="disp_name", length=30)
    private String dispName;
    @Column(name="table_no", length=3)
    private String tableNo;
    @Column(name="table_type", length=15)
    private String tableType;
    @Column(name="is_disp", precision=3)
    private int isDisp;
    @Column(name="disp_order", precision=10)
    private int dispOrder;
    @Column(name="create_user", length=10)
    private String createUser;
    @Column(name="create_time")
    private Date createTime;
    @Column(name="modify_user", length=10)
    private String modifyUser;
    @Column(name="modify_time")
    private Date modifyTime;

    /** Default constructor. */
    public TableMeta() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for tableName.
     *
     * @return the current value of tableName
     */
    public String getTableName() {
        return tableName;
    }

    /**
     * Setter method for tableName.
     *
     * @param aTableName the new value for tableName
     */
    public void setTableName(String aTableName) {
        tableName = aTableName;
    }

    /**
     * Access method for dispName.
     *
     * @return the current value of dispName
     */
    public String getDispName() {
        return dispName;
    }

    /**
     * Setter method for dispName.
     *
     * @param aDispName the new value for dispName
     */
    public void setDispName(String aDispName) {
        dispName = aDispName;
    }

    /**
     * Access method for tableNo.
     *
     * @return the current value of tableNo
     */
    public String getTableNo() {
        return tableNo;
    }

    /**
     * Setter method for tableNo.
     *
     * @param aTableNo the new value for tableNo
     */
    public void setTableNo(String aTableNo) {
        tableNo = aTableNo;
    }

    /**
     * Access method for tableType.
     *
     * @return the current value of tableType
     */
    public String getTableType() {
        return tableType;
    }

    /**
     * Setter method for tableType.
     *
     * @param aTableType the new value for tableType
     */
    public void setTableType(String aTableType) {
        tableType = aTableType;
    }

    /**
     * Access method for isDisp.
     *
     * @return the current value of isDisp
     */
    public int getIsDisp() {
        return isDisp;
    }

    /**
     * Setter method for isDisp.
     *
     * @param aIsDisp the new value for isDisp
     */
    public void setIsDisp(int aIsDisp) {
        isDisp = aIsDisp;
    }

    /**
     * Access method for dispOrder.
     *
     * @return the current value of dispOrder
     */
    public int getDispOrder() {
        return dispOrder;
    }

    /**
     * Setter method for dispOrder.
     *
     * @param aDispOrder the new value for dispOrder
     */
    public void setDispOrder(int aDispOrder) {
        dispOrder = aDispOrder;
    }

    /**
     * Access method for createUser.
     *
     * @return the current value of createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * Setter method for createUser.
     *
     * @param aCreateUser the new value for createUser
     */
    public void setCreateUser(String aCreateUser) {
        createUser = aCreateUser;
    }

    /**
     * Access method for createTime.
     *
     * @return the current value of createTime
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * Setter method for createTime.
     *
     * @param aCreateTime the new value for createTime
     */
    public void setCreateTime(Date aCreateTime) {
        createTime = aCreateTime;
    }

    /**
     * Access method for modifyUser.
     *
     * @return the current value of modifyUser
     */
    public String getModifyUser() {
        return modifyUser;
    }

    /**
     * Setter method for modifyUser.
     *
     * @param aModifyUser the new value for modifyUser
     */
    public void setModifyUser(String aModifyUser) {
        modifyUser = aModifyUser;
    }

    /**
     * Access method for modifyTime.
     *
     * @return the current value of modifyTime
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * Setter method for modifyTime.
     *
     * @param aModifyTime the new value for modifyTime
     */
    public void setModifyTime(Date aModifyTime) {
        modifyTime = aModifyTime;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((createTime == null) ? 0 : createTime.hashCode());
		result = prime * result + ((createUser == null) ? 0 : createUser.hashCode());
		result = prime * result + ((dispName == null) ? 0 : dispName.hashCode());
		result = prime * result + dispOrder;
		result = prime * result + id;
		result = prime * result + isDisp;
		result = prime * result + ((modifyTime == null) ? 0 : modifyTime.hashCode());
		result = prime * result + ((modifyUser == null) ? 0 : modifyUser.hashCode());
		result = prime * result + ((tableName == null) ? 0 : tableName.hashCode());
		result = prime * result + ((tableNo == null) ? 0 : tableNo.hashCode());
		result = prime * result + ((tableType == null) ? 0 : tableType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TableMeta other = (TableMeta) obj;
		if (createTime == null) {
			if (other.createTime != null)
				return false;
		} else if (!createTime.equals(other.createTime))
			return false;
		if (createUser == null) {
			if (other.createUser != null)
				return false;
		} else if (!createUser.equals(other.createUser))
			return false;
		if (dispName == null) {
			if (other.dispName != null)
				return false;
		} else if (!dispName.equals(other.dispName))
			return false;
		if (dispOrder != other.dispOrder)
			return false;
		if (id != other.id)
			return false;
		if (isDisp != other.isDisp)
			return false;
		if (modifyTime == null) {
			if (other.modifyTime != null)
				return false;
		} else if (!modifyTime.equals(other.modifyTime))
			return false;
		if (modifyUser == null) {
			if (other.modifyUser != null)
				return false;
		} else if (!modifyUser.equals(other.modifyUser))
			return false;
		if (tableName == null) {
			if (other.tableName != null)
				return false;
		} else if (!tableName.equals(other.tableName))
			return false;
		if (tableNo == null) {
			if (other.tableNo != null)
				return false;
		} else if (!tableNo.equals(other.tableNo))
			return false;
		if (tableType == null) {
			if (other.tableType != null)
				return false;
		} else if (!tableType.equals(other.tableType))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format(
				"TableMeta [id=%s, tableName=%s, dispName=%s, tableNo=%s, tableType=%s, isDisp=%s, dispOrder=%s, createUser=%s, createTime=%s, modifyUser=%s, modifyTime=%s]",
				id, tableName, dispName, tableNo, tableType, isDisp, dispOrder, createUser, createTime, modifyUser,
				modifyTime);
	}

}
