package com.vg.lms.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="lms_registry")
public class LMSRegistryModel implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4852877081954331330L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="register_type", length=15)
    private String registryType;
    @Column(name="register_value", length=15)
    private String registryValue;
    @Column(name="active")
    private boolean active;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRegistryType() {
		return registryType;
	}
	public void setRegistryType(String registryType) {
		this.registryType = registryType;
	}
	public String getRegistryValue() {
		return registryValue;
	}
	public void setRegistryValue(String registryValue) {
		this.registryValue = registryValue;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	@Override
	public String toString() {
		return "LMSRegistryModel [id=" + id + ", registryType=" + registryType + ", registryValue=" + registryValue
				+ ", active=" + active + "]";
	}
	
    
    
}
