// Generated with g9.

package com.vg.lms.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="hht_waiver")
public class Waiver implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 3592237728515885260L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="book_no", length=10)
    private String bookNo;
    @Column(name="receipt_no", length=50)
    private String receiptNo;
    @Column(name="loan_no", length=30)
    private String loanNo;
    @Column(name="receipt_amount", precision=15, scale=2)
    private float receiptAmount;
    @Column(name="waiver_type", length=50)
    private String waiverType;
    
    @Column(name="waiver_remarks", length=50)
    private String waiverRemarks;
    
    @Column(name="receipt_date")
    private Date receiptDate;
    @Column(name="lms_absorb_time")
    private Date lmsAbsorbTime;
    @Column(name="create_user", length=50)
    private String createUser;
    @Column(name="modify_user", length=50)
    private String modifyUser;
   
    @Column(name="lms_reject_reason", length=24)
    private String lmsRejectReason;

    /** Default constructor. */
    public Waiver() {
        super();
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBookNo() {
		return bookNo;
	}

	public void setBookNo(String bookNo) {
		this.bookNo = bookNo;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	public String getLoanNo() {
		return loanNo;
	}

	public void setLoanNo(String loanNo) {
		this.loanNo = loanNo;
	}

	public float getReceiptAmount() {
		return receiptAmount;
	}

	public void setReceiptAmount(float receiptAmount) {
		this.receiptAmount = receiptAmount;
	}

	public String getWaiverType() {
		return waiverType;
	}

	public void setWaiverType(String waiverType) {
		this.waiverType = waiverType;
	}

	public Date getReceiptDate() {
		return receiptDate;
	}

	public void setReceiptDate(Date receiptDate) {
		this.receiptDate = receiptDate;
	}

	public Date getLmsAbsorbTime() {
		return lmsAbsorbTime;
	}

	public void setLmsAbsorbTime(Date lmsAbsorbTime) {
		this.lmsAbsorbTime = lmsAbsorbTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getModifyUser() {
		return modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	public String getLmsRejectReason() {
		return lmsRejectReason;
	}

	public void setLmsRejectReason(String lmsRejectReason) {
		this.lmsRejectReason = lmsRejectReason;
	}

	public String getWaiverRemarks() {
		return waiverRemarks;
	}

	public void setWaiverRemarks(String waiverRemarks) {
		this.waiverRemarks = waiverRemarks;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bookNo == null) ? 0 : bookNo.hashCode());
		result = prime * result + ((createUser == null) ? 0 : createUser.hashCode());
		result = prime * result + id;
		result = prime * result + ((lmsAbsorbTime == null) ? 0 : lmsAbsorbTime.hashCode());
		result = prime * result + ((lmsRejectReason == null) ? 0 : lmsRejectReason.hashCode());
		result = prime * result + ((loanNo == null) ? 0 : loanNo.hashCode());
		result = prime * result + ((modifyUser == null) ? 0 : modifyUser.hashCode());
		result = prime * result + Float.floatToIntBits(receiptAmount);
		result = prime * result + ((receiptDate == null) ? 0 : receiptDate.hashCode());
		result = prime * result + ((receiptNo == null) ? 0 : receiptNo.hashCode());
		result = prime * result + ((waiverRemarks == null) ? 0 : waiverRemarks.hashCode());
		result = prime * result + ((waiverType == null) ? 0 : waiverType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Waiver other = (Waiver) obj;
		if (bookNo == null) {
			if (other.bookNo != null)
				return false;
		} else if (!bookNo.equals(other.bookNo))
			return false;
		if (createUser == null) {
			if (other.createUser != null)
				return false;
		} else if (!createUser.equals(other.createUser))
			return false;
		if (id != other.id)
			return false;
		if (lmsAbsorbTime == null) {
			if (other.lmsAbsorbTime != null)
				return false;
		} else if (!lmsAbsorbTime.equals(other.lmsAbsorbTime))
			return false;
		if (lmsRejectReason == null) {
			if (other.lmsRejectReason != null)
				return false;
		} else if (!lmsRejectReason.equals(other.lmsRejectReason))
			return false;
		if (loanNo == null) {
			if (other.loanNo != null)
				return false;
		} else if (!loanNo.equals(other.loanNo))
			return false;
		if (modifyUser == null) {
			if (other.modifyUser != null)
				return false;
		} else if (!modifyUser.equals(other.modifyUser))
			return false;
		if (Float.floatToIntBits(receiptAmount) != Float.floatToIntBits(other.receiptAmount))
			return false;
		if (receiptDate == null) {
			if (other.receiptDate != null)
				return false;
		} else if (!receiptDate.equals(other.receiptDate))
			return false;
		if (receiptNo == null) {
			if (other.receiptNo != null)
				return false;
		} else if (!receiptNo.equals(other.receiptNo))
			return false;
		if (waiverRemarks == null) {
			if (other.waiverRemarks != null)
				return false;
		} else if (!waiverRemarks.equals(other.waiverRemarks))
			return false;
		if (waiverType == null) {
			if (other.waiverType != null)
				return false;
		} else if (!waiverType.equals(other.waiverType))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Waiver [id=" + id + ", bookNo=" + bookNo + ", receiptNo=" + receiptNo + ", loanNo=" + loanNo
				+ ", receiptAmount=" + receiptAmount + ", waiverType=" + waiverType + ", waiverRemarks=" + waiverRemarks
				+ ", receiptDate=" + receiptDate + ", lmsAbsorbTime=" + lmsAbsorbTime + ", createUser=" + createUser
				+ ", modifyUser=" + modifyUser + ", lmsRejectReason=" + lmsRejectReason + "]";
	}

	

}
