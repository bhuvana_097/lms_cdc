//Generated with g9.

package com.vg.lms.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="lms_customer_archive")
public class CustomerArchive implements Serializable {
 /**
	 * 
	 */
	private static final long serialVersionUID = -2628701456016401238L;
	@Id
 @GeneratedValue(strategy=GenerationType.IDENTITY)
 @Column(unique=true, nullable=false, precision=10)
 private int id;
 @Column(length=50)
 private String name;
 @Column(length=10)
 private String phone1;
 @Column(length=10)
 private String phone2;
 @Column(name="email_Id", length=100)
 private String emailId;
 @Column(length=20)
 private String dob;
 @Column(precision=10)
 private int age;
 @Column(name="cibil_score", length=6)
 private String cibilScore;
 @Column(length=10)
 private String gender;
 @Column(length=30)
 private String religion;
 @Column(length=30)
 private String caste;
 @Column(length=30)
 private String firstname;
 @Column(length=30)
 private String lastname;
 @Column(name="father_name", length=60)
 private String fatherName;
 @Column(name="mother_name", length=60)
 private String motherName;
 @Column(name="spouse_name", length=60)
 private String spouseName;
 @Column(name="passport_no", length=15)
 private String passportNo;
 @Column(length=25)
 private String voterid;
 @Column(name="driving_license", length=30)
 private String drivingLicense;
 @Column(name="ration_card", length=25)
 private String rationCard;
 @Column(length=10)
 private String panno;
 @Column(name="name_in_bank", length=50)
 private String nameInBank;
 @Column(name="account_no", length=20)
 private String accountNo;
 @Column(name="ifsc_code", length=11)
 private String ifscCode;
 @Column(name="acccount_type", length=10)
 private String acccountType;
 @Column(name="marital_status", length=10)
 private String maritalStatus;
 @Column(name="job_type", length=50)
 private String jobType;
 @Column(length=200)
 private String profile;
 @Column(length=100)
 private String salorself;
 @Column(name="annual_income", length=50)
 private String annualIncome;
 @Column(name="current_emi", length=25)
 private String currentEmi;
 @Column(name="loan_reason", length=20)
 private String loanReason;
 @Column(length=25)
 private String education;
 @Column(name="own_house", length=3)
 private boolean ownHouse;
 @Column(name="two_wheeler", length=3)
 private boolean twoWheeler;
 @Column(name="four_wheeler", length=3)
 private boolean fourWheeler;
 @Column(name="resi_addr", length=80)
 private String resiAddr;
 @Column(name="resi_address2", length=80)
 private String resiAddress2;
 @Column(name="resi_city", length=40)
 private String resiCity;
 @Column(name="resi_pin", length=6)
 private String resiPin;
 @Column(name="resi_state", length=50)
 private String resiState;
 @Column(name="resi_ph", length=20)
 private String resiPh;
 @Column(name="resi_mobile", length=15)
 private String resiMobile;
 @Column(name="perm_addr", length=80)
 private String permAddr;
 @Column(name="perm_addr2", length=80)
 private String permAddr2;
 @Column(name="perm_city", length=40)
 private String permCity;
 @Column(name="perm_state", length=50)
 private String permState;
 @Column(name="perm_pincode", length=6)
 private String permPincode;
 @Column(name="off_name", length=100)
 private String offName;
 @Column(name="off_addr", length=500)
 private String offAddr;
 @Column(name="off_addr2", length=500)
 private String offAddr2;
 @Column(name="off_city", length=100)
 private String offCity;
 @Column(name="off_state", length=100)
 private String offState;
 @Column(name="off_pin", length=6)
 private String offPin;
 @Column(name="off_std", length=5)
 private String offStd;
 @Column(name="off_ph", length=10)
 private String offPh;
 @Column(name="resi_cum_off", length=4)
 private String resiCumOff;
 @Column(name="aadhar_no", length=50)
 private String aadharNo;
 @Column(name="aadhar_name", length=70)
 private String aadharName;
 @Column(name="aadhar_phone", length=10)
 private String aadharPhone;
 @Column(name="aadhaar_email", length=30)
 private String aadhaarEmail;
 @Column(name="aadhar_co", length=50)
 private String aadharCo;
 @Column(name="aadhar_house", length=50)
 private String aadharHouse;
 @Column(name="aadhar_street", length=50)
 private String aadharStreet;
 @Column(name="aadhar_lm", length=50)
 private String aadharLm;
 @Column(name="aadhar_vtc", length=50)
 private String aadharVtc;
 @Column(name="aadhar_sub_dist", length=50)
 private String aadharSubDist;
 @Column(name="aadhar_dist", length=50)
 private String aadharDist;
 @Column(name="aadhar_state", length=50)
 private String aadharState;
 @Column(name="aadhar_po", length=50)
 private String aadharPo;
 @Column(name="aadhar_dob", length=10)
 private String aadharDob;
 @Column(name="aadhar_pincode", length=8)
 private String aadharPincode;
 @Column(name="aadhar_gender", length=6)
 private String aadharGender;
 @Column(name="aadhar_loc", length=20)
 private String aadharLoc;
 @Column(name="pan_pic", length=30)
 private String panPic;
 @Column(name="customer_pic", length=30)
 private String customerPic;
 @Column(name="passport_pic", length=30)
 private String passportPic;
 @Column(name="voterid_pic", length=30)
 private String voteridPic;
 @Column(name="dl_pic", length=30)
 private String dlPic;
 @Column(name="ration_pic", length=30)
 private String rationPic;
 @Column(length=30)
 private String h1;
 @Column(length=30)
 private String h2;
 @Column(length=30)
 private String h3;
 @Column(length=30)
 private String h4;
 @Column(length=30)
 private String h5;
 @Column(name="create_user", length=10)
 private String createUser;

 @Column(name="modify_user", length=10)
 private String modifyUser;

 @OneToMany(mappedBy="customer")
 private Set<Loan> loan;
 @OneToMany(mappedBy="customer")
 private Set<Receivable> receivable;

 /** Default constructor. */
 public CustomerArchive() {
     super();
 }

 /**
  * Access method for id.
  *
  * @return the current value of id
  */
 public int getId() {
     return id;
 }

 /**
  * Setter method for id.
  *
  * @param aId the new value for id
  */
 public void setId(int aId) {
     id = aId;
 }

 /**
  * Access method for name.
  *
  * @return the current value of name
  */
 public String getName() {
     return name;
 }

 /**
  * Setter method for name.
  *
  * @param aName the new value for name
  */
 public void setName(String aName) {
     name = aName;
 }

 /**
  * Access method for phone1.
  *
  * @return the current value of phone1
  */
 public String getPhone1() {
     return phone1;
 }

 /**
  * Setter method for phone1.
  *
  * @param aPhone1 the new value for phone1
  */
 public void setPhone1(String aPhone1) {
     phone1 = aPhone1;
 }

 /**
  * Access method for phone2.
  *
  * @return the current value of phone2
  */
 public String getPhone2() {
     return phone2;
 }

 /**
  * Setter method for phone2.
  *
  * @param aPhone2 the new value for phone2
  */
 public void setPhone2(String aPhone2) {
     phone2 = aPhone2;
 }

 /**
  * Access method for emailId.
  *
  * @return the current value of emailId
  */
 public String getEmailId() {
     return emailId;
 }

 /**
  * Setter method for emailId.
  *
  * @param aEmailId the new value for emailId
  */
 public void setEmailId(String aEmailId) {
     emailId = aEmailId;
 }

 /**
  * Access method for dob.
  *
  * @return the current value of dob
  */
 public String getDob() {
     return dob;
 }

 /**
  * Setter method for dob.
  *
  * @param aDob the new value for dob
  */
 public void setDob(String aDob) {
     dob = aDob;
 }

 /**
  * Access method for age.
  *
  * @return the current value of age
  */
 public int getAge() {
     return age;
 }

 /**
  * Setter method for age.
  *
  * @param aAge the new value for age
  */
 public void setAge(int aAge) {
     age = aAge;
 }

 /**
  * Access method for cibilScore.
  *
  * @return the current value of cibilScore
  */
 public String getCibilScore() {
     return cibilScore;
 }

 /**
  * Setter method for cibilScore.
  *
  * @param aCibilScore the new value for cibilScore
  */
 public void setCibilScore(String aCibilScore) {
     cibilScore = aCibilScore;
 }

 /**
  * Access method for gender.
  *
  * @return the current value of gender
  */
 public String getGender() {
     return gender;
 }

 /**
  * Setter method for gender.
  *
  * @param aGender the new value for gender
  */
 public void setGender(String aGender) {
     gender = aGender;
 }

 /**
  * Access method for religion.
  *
  * @return the current value of religion
  */
 public String getReligion() {
     return religion;
 }

 /**
  * Setter method for religion.
  *
  * @param aReligion the new value for religion
  */
 public void setReligion(String aReligion) {
     religion = aReligion;
 }

 /**
  * Access method for caste.
  *
  * @return the current value of caste
  */
 public String getCaste() {
     return caste;
 }

 /**
  * Setter method for caste.
  *
  * @param aCaste the new value for caste
  */
 public void setCaste(String aCaste) {
     caste = aCaste;
 }

 /**
  * Access method for firstname.
  *
  * @return the current value of firstname
  */
 public String getFirstname() {
     return firstname;
 }

 /**
  * Setter method for firstname.
  *
  * @param aFirstname the new value for firstname
  */
 public void setFirstname(String aFirstname) {
     firstname = aFirstname;
 }

 /**
  * Access method for lastname.
  *
  * @return the current value of lastname
  */
 public String getLastname() {
     return lastname;
 }

 /**
  * Setter method for lastname.
  *
  * @param aLastname the new value for lastname
  */
 public void setLastname(String aLastname) {
     lastname = aLastname;
 }

 /**
  * Access method for fatherName.
  *
  * @return the current value of fatherName
  */
 public String getFatherName() {
     return fatherName;
 }

 /**
  * Setter method for fatherName.
  *
  * @param aFatherName the new value for fatherName
  */
 public void setFatherName(String aFatherName) {
     fatherName = aFatherName;
 }

 /**
  * Access method for motherName.
  *
  * @return the current value of motherName
  */
 public String getMotherName() {
     return motherName;
 }

 /**
  * Setter method for motherName.
  *
  * @param aMotherName the new value for motherName
  */
 public void setMotherName(String aMotherName) {
     motherName = aMotherName;
 }

 /**
  * Access method for spouseName.
  *
  * @return the current value of spouseName
  */
 public String getSpouseName() {
     return spouseName;
 }

 /**
  * Setter method for spouseName.
  *
  * @param aSpouseName the new value for spouseName
  */
 public void setSpouseName(String aSpouseName) {
     spouseName = aSpouseName;
 }

 /**
  * Access method for passportNo.
  *
  * @return the current value of passportNo
  */
 public String getPassportNo() {
     return passportNo;
 }

 /**
  * Setter method for passportNo.
  *
  * @param aPassportNo the new value for passportNo
  */
 public void setPassportNo(String aPassportNo) {
     passportNo = aPassportNo;
 }

 /**
  * Access method for voterid.
  *
  * @return the current value of voterid
  */
 public String getVoterid() {
     return voterid;
 }

 /**
  * Setter method for voterid.
  *
  * @param aVoterid the new value for voterid
  */
 public void setVoterid(String aVoterid) {
     voterid = aVoterid;
 }

 /**
  * Access method for drivingLicense.
  *
  * @return the current value of drivingLicense
  */
 public String getDrivingLicense() {
     return drivingLicense;
 }

 /**
  * Setter method for drivingLicense.
  *
  * @param aDrivingLicense the new value for drivingLicense
  */
 public void setDrivingLicense(String aDrivingLicense) {
     drivingLicense = aDrivingLicense;
 }

 /**
  * Access method for rationCard.
  *
  * @return the current value of rationCard
  */
 public String getRationCard() {
     return rationCard;
 }

 /**
  * Setter method for rationCard.
  *
  * @param aRationCard the new value for rationCard
  */
 public void setRationCard(String aRationCard) {
     rationCard = aRationCard;
 }

 /**
  * Access method for panno.
  *
  * @return the current value of panno
  */
 public String getPanno() {
     return panno;
 }

 /**
  * Setter method for panno.
  *
  * @param aPanno the new value for panno
  */
 public void setPanno(String aPanno) {
     panno = aPanno;
 }

 /**
  * Access method for nameInBank.
  *
  * @return the current value of nameInBank
  */
 public String getNameInBank() {
     return nameInBank;
 }

 /**
  * Setter method for nameInBank.
  *
  * @param aNameInBank the new value for nameInBank
  */
 public void setNameInBank(String aNameInBank) {
     nameInBank = aNameInBank;
 }

 /**
  * Access method for accountNo.
  *
  * @return the current value of accountNo
  */
 public String getAccountNo() {
     return accountNo;
 }

 /**
  * Setter method for accountNo.
  *
  * @param aAccountNo the new value for accountNo
  */
 public void setAccountNo(String aAccountNo) {
     accountNo = aAccountNo;
 }

 /**
  * Access method for ifscCode.
  *
  * @return the current value of ifscCode
  */
 public String getIfscCode() {
     return ifscCode;
 }

 /**
  * Setter method for ifscCode.
  *
  * @param aIfscCode the new value for ifscCode
  */
 public void setIfscCode(String aIfscCode) {
     ifscCode = aIfscCode;
 }

 /**
  * Access method for acccountType.
  *
  * @return the current value of acccountType
  */
 public String getAcccountType() {
     return acccountType;
 }

 /**
  * Setter method for acccountType.
  *
  * @param aAcccountType the new value for acccountType
  */
 public void setAcccountType(String aAcccountType) {
     acccountType = aAcccountType;
 }

 /**
  * Access method for maritalStatus.
  *
  * @return the current value of maritalStatus
  */
 public String getMaritalStatus() {
     return maritalStatus;
 }

 /**
  * Setter method for maritalStatus.
  *
  * @param aMaritalStatus the new value for maritalStatus
  */
 public void setMaritalStatus(String aMaritalStatus) {
     maritalStatus = aMaritalStatus;
 }

 /**
  * Access method for jobType.
  *
  * @return the current value of jobType
  */
 public String getJobType() {
     return jobType;
 }

 /**
  * Setter method for jobType.
  *
  * @param aJobType the new value for jobType
  */
 public void setJobType(String aJobType) {
     jobType = aJobType;
 }

 /**
  * Access method for profile.
  *
  * @return the current value of profile
  */
 public String getProfile() {
     return profile;
 }

 /**
  * Setter method for profile.
  *
  * @param aProfile the new value for profile
  */
 public void setProfile(String aProfile) {
     profile = aProfile;
 }

 /**
  * Access method for salorself.
  *
  * @return the current value of salorself
  */
 public String getSalorself() {
     return salorself;
 }

 /**
  * Setter method for salorself.
  *
  * @param aSalorself the new value for salorself
  */
 public void setSalorself(String aSalorself) {
     salorself = aSalorself;
 }

 /**
  * Access method for annualIncome.
  *
  * @return the current value of annualIncome
  */
 public String getAnnualIncome() {
     return annualIncome;
 }

 /**
  * Setter method for annualIncome.
  *
  * @param aAnnualIncome the new value for annualIncome
  */
 public void setAnnualIncome(String aAnnualIncome) {
     annualIncome = aAnnualIncome;
 }

 /**
  * Access method for currentEmi.
  *
  * @return the current value of currentEmi
  */
 public String getCurrentEmi() {
     return currentEmi;
 }

 /**
  * Setter method for currentEmi.
  *
  * @param aCurrentEmi the new value for currentEmi
  */
 public void setCurrentEmi(String aCurrentEmi) {
     currentEmi = aCurrentEmi;
 }

 /**
  * Access method for loanReason.
  *
  * @return the current value of loanReason
  */
 public String getLoanReason() {
     return loanReason;
 }

 /**
  * Setter method for loanReason.
  *
  * @param aLoanReason the new value for loanReason
  */
 public void setLoanReason(String aLoanReason) {
     loanReason = aLoanReason;
 }

 /**
  * Access method for education.
  *
  * @return the current value of education
  */
 public String getEducation() {
     return education;
 }

 /**
  * Setter method for education.
  *
  * @param aEducation the new value for education
  */
 public void setEducation(String aEducation) {
     education = aEducation;
 }

 /**
  * Access method for ownHouse.
  *
  * @return true if and only if ownHouse is currently true
  */
 public boolean getOwnHouse() {
     return ownHouse;
 }

 /**
  * Setter method for ownHouse.
  *
  * @param aOwnHouse the new value for ownHouse
  */
 public void setOwnHouse(boolean aOwnHouse) {
     ownHouse = aOwnHouse;
 }

 /**
  * Access method for twoWheeler.
  *
  * @return true if and only if twoWheeler is currently true
  */
 public boolean getTwoWheeler() {
     return twoWheeler;
 }

 /**
  * Setter method for twoWheeler.
  *
  * @param aTwoWheeler the new value for twoWheeler
  */
 public void setTwoWheeler(boolean aTwoWheeler) {
     twoWheeler = aTwoWheeler;
 }

 /**
  * Access method for fourWheeler.
  *
  * @return true if and only if fourWheeler is currently true
  */
 public boolean getFourWheeler() {
     return fourWheeler;
 }

 /**
  * Setter method for fourWheeler.
  *
  * @param aFourWheeler the new value for fourWheeler
  */
 public void setFourWheeler(boolean aFourWheeler) {
     fourWheeler = aFourWheeler;
 }

 /**
  * Access method for resiAddr.
  *
  * @return the current value of resiAddr
  */
 public String getResiAddr() {
     return resiAddr;
 }

 /**
  * Setter method for resiAddr.
  *
  * @param aResiAddr the new value for resiAddr
  */
 public void setResiAddr(String aResiAddr) {
     resiAddr = aResiAddr;
 }

 /**
  * Access method for resiAddress2.
  *
  * @return the current value of resiAddress2
  */
 public String getResiAddress2() {
     return resiAddress2;
 }

 /**
  * Setter method for resiAddress2.
  *
  * @param aResiAddress2 the new value for resiAddress2
  */
 public void setResiAddress2(String aResiAddress2) {
     resiAddress2 = aResiAddress2;
 }

 /**
  * Access method for resiCity.
  *
  * @return the current value of resiCity
  */
 public String getResiCity() {
     return resiCity;
 }

 /**
  * Setter method for resiCity.
  *
  * @param aResiCity the new value for resiCity
  */
 public void setResiCity(String aResiCity) {
     resiCity = aResiCity;
 }

 /**
  * Access method for resiPin.
  *
  * @return the current value of resiPin
  */
 public String getResiPin() {
     return resiPin;
 }

 /**
  * Setter method for resiPin.
  *
  * @param aResiPin the new value for resiPin
  */
 public void setResiPin(String aResiPin) {
     resiPin = aResiPin;
 }

 /**
  * Access method for resiState.
  *
  * @return the current value of resiState
  */
 public String getResiState() {
     return resiState;
 }

 /**
  * Setter method for resiState.
  *
  * @param aResiState the new value for resiState
  */
 public void setResiState(String aResiState) {
     resiState = aResiState;
 }

 /**
  * Access method for resiPh.
  *
  * @return the current value of resiPh
  */
 public String getResiPh() {
     return resiPh;
 }

 /**
  * Setter method for resiPh.
  *
  * @param aResiPh the new value for resiPh
  */
 public void setResiPh(String aResiPh) {
     resiPh = aResiPh;
 }

 /**
  * Access method for resiMobile.
  *
  * @return the current value of resiMobile
  */
 public String getResiMobile() {
     return resiMobile;
 }

 /**
  * Setter method for resiMobile.
  *
  * @param aResiMobile the new value for resiMobile
  */
 public void setResiMobile(String aResiMobile) {
     resiMobile = aResiMobile;
 }

 /**
  * Access method for permAddr.
  *
  * @return the current value of permAddr
  */
 public String getPermAddr() {
     return permAddr;
 }

 /**
  * Setter method for permAddr.
  *
  * @param aPermAddr the new value for permAddr
  */
 public void setPermAddr(String aPermAddr) {
     permAddr = aPermAddr;
 }

 /**
  * Access method for permAddr2.
  *
  * @return the current value of permAddr2
  */
 public String getPermAddr2() {
     return permAddr2;
 }

 /**
  * Setter method for permAddr2.
  *
  * @param aPermAddr2 the new value for permAddr2
  */
 public void setPermAddr2(String aPermAddr2) {
     permAddr2 = aPermAddr2;
 }

 /**
  * Access method for permCity.
  *
  * @return the current value of permCity
  */
 public String getPermCity() {
     return permCity;
 }

 /**
  * Setter method for permCity.
  *
  * @param aPermCity the new value for permCity
  */
 public void setPermCity(String aPermCity) {
     permCity = aPermCity;
 }

 /**
  * Access method for permState.
  *
  * @return the current value of permState
  */
 public String getPermState() {
     return permState;
 }

 /**
  * Setter method for permState.
  *
  * @param aPermState the new value for permState
  */
 public void setPermState(String aPermState) {
     permState = aPermState;
 }

 /**
  * Access method for permPincode.
  *
  * @return the current value of permPincode
  */
 public String getPermPincode() {
     return permPincode;
 }

 /**
  * Setter method for permPincode.
  *
  * @param aPermPincode the new value for permPincode
  */
 public void setPermPincode(String aPermPincode) {
     permPincode = aPermPincode;
 }

 /**
  * Access method for offName.
  *
  * @return the current value of offName
  */
 public String getOffName() {
     return offName;
 }

 /**
  * Setter method for offName.
  *
  * @param aOffName the new value for offName
  */
 public void setOffName(String aOffName) {
     offName = aOffName;
 }

 /**
  * Access method for offAddr.
  *
  * @return the current value of offAddr
  */
 public String getOffAddr() {
     return offAddr;
 }

 /**
  * Setter method for offAddr.
  *
  * @param aOffAddr the new value for offAddr
  */
 public void setOffAddr(String aOffAddr) {
     offAddr = aOffAddr;
 }

 /**
  * Access method for offAddr2.
  *
  * @return the current value of offAddr2
  */
 public String getOffAddr2() {
     return offAddr2;
 }

 /**
  * Setter method for offAddr2.
  *
  * @param aOffAddr2 the new value for offAddr2
  */
 public void setOffAddr2(String aOffAddr2) {
     offAddr2 = aOffAddr2;
 }

 /**
  * Access method for offCity.
  *
  * @return the current value of offCity
  */
 public String getOffCity() {
     return offCity;
 }

 /**
  * Setter method for offCity.
  *
  * @param aOffCity the new value for offCity
  */
 public void setOffCity(String aOffCity) {
     offCity = aOffCity;
 }

 /**
  * Access method for offState.
  *
  * @return the current value of offState
  */
 public String getOffState() {
     return offState;
 }

 /**
  * Setter method for offState.
  *
  * @param aOffState the new value for offState
  */
 public void setOffState(String aOffState) {
     offState = aOffState;
 }

 /**
  * Access method for offPin.
  *
  * @return the current value of offPin
  */
 public String getOffPin() {
     return offPin;
 }

 /**
  * Setter method for offPin.
  *
  * @param aOffPin the new value for offPin
  */
 public void setOffPin(String aOffPin) {
     offPin = aOffPin;
 }

 /**
  * Access method for offStd.
  *
  * @return the current value of offStd
  */
 public String getOffStd() {
     return offStd;
 }

 /**
  * Setter method for offStd.
  *
  * @param aOffStd the new value for offStd
  */
 public void setOffStd(String aOffStd) {
     offStd = aOffStd;
 }

 /**
  * Access method for offPh.
  *
  * @return the current value of offPh
  */
 public String getOffPh() {
     return offPh;
 }

 /**
  * Setter method for offPh.
  *
  * @param aOffPh the new value for offPh
  */
 public void setOffPh(String aOffPh) {
     offPh = aOffPh;
 }

 /**
  * Access method for resiCumOff.
  *
  * @return the current value of resiCumOff
  */
 public String getResiCumOff() {
     return resiCumOff;
 }

 /**
  * Setter method for resiCumOff.
  *
  * @param aResiCumOff the new value for resiCumOff
  */
 public void setResiCumOff(String aResiCumOff) {
     resiCumOff = aResiCumOff;
 }

 /**
  * Access method for aadharNo.
  *
  * @return the current value of aadharNo
  */
 public String getAadharNo() {
     return aadharNo;
 }

 /**
  * Setter method for aadharNo.
  *
  * @param aAadharNo the new value for aadharNo
  */
 public void setAadharNo(String aAadharNo) {
     aadharNo = aAadharNo;
 }

 /**
  * Access method for aadharName.
  *
  * @return the current value of aadharName
  */
 public String getAadharName() {
     return aadharName;
 }

 /**
  * Setter method for aadharName.
  *
  * @param aAadharName the new value for aadharName
  */
 public void setAadharName(String aAadharName) {
     aadharName = aAadharName;
 }

 /**
  * Access method for aadharPhone.
  *
  * @return the current value of aadharPhone
  */
 public String getAadharPhone() {
     return aadharPhone;
 }

 /**
  * Setter method for aadharPhone.
  *
  * @param aAadharPhone the new value for aadharPhone
  */
 public void setAadharPhone(String aAadharPhone) {
     aadharPhone = aAadharPhone;
 }

 /**
  * Access method for aadhaarEmail.
  *
  * @return the current value of aadhaarEmail
  */
 public String getAadhaarEmail() {
     return aadhaarEmail;
 }

 /**
  * Setter method for aadhaarEmail.
  *
  * @param aAadhaarEmail the new value for aadhaarEmail
  */
 public void setAadhaarEmail(String aAadhaarEmail) {
     aadhaarEmail = aAadhaarEmail;
 }

 /**
  * Access method for aadharCo.
  *
  * @return the current value of aadharCo
  */
 public String getAadharCo() {
     return aadharCo;
 }

 /**
  * Setter method for aadharCo.
  *
  * @param aAadharCo the new value for aadharCo
  */
 public void setAadharCo(String aAadharCo) {
     aadharCo = aAadharCo;
 }

 /**
  * Access method for aadharHouse.
  *
  * @return the current value of aadharHouse
  */
 public String getAadharHouse() {
     return aadharHouse;
 }

 /**
  * Setter method for aadharHouse.
  *
  * @param aAadharHouse the new value for aadharHouse
  */
 public void setAadharHouse(String aAadharHouse) {
     aadharHouse = aAadharHouse;
 }

 /**
  * Access method for aadharStreet.
  *
  * @return the current value of aadharStreet
  */
 public String getAadharStreet() {
     return aadharStreet;
 }

 /**
  * Setter method for aadharStreet.
  *
  * @param aAadharStreet the new value for aadharStreet
  */
 public void setAadharStreet(String aAadharStreet) {
     aadharStreet = aAadharStreet;
 }

 /**
  * Access method for aadharLm.
  *
  * @return the current value of aadharLm
  */
 public String getAadharLm() {
     return aadharLm;
 }

 /**
  * Setter method for aadharLm.
  *
  * @param aAadharLm the new value for aadharLm
  */
 public void setAadharLm(String aAadharLm) {
     aadharLm = aAadharLm;
 }

 /**
  * Access method for aadharVtc.
  *
  * @return the current value of aadharVtc
  */
 public String getAadharVtc() {
     return aadharVtc;
 }

 /**
  * Setter method for aadharVtc.
  *
  * @param aAadharVtc the new value for aadharVtc
  */
 public void setAadharVtc(String aAadharVtc) {
     aadharVtc = aAadharVtc;
 }

 /**
  * Access method for aadharSubDist.
  *
  * @return the current value of aadharSubDist
  */
 public String getAadharSubDist() {
     return aadharSubDist;
 }

 /**
  * Setter method for aadharSubDist.
  *
  * @param aAadharSubDist the new value for aadharSubDist
  */
 public void setAadharSubDist(String aAadharSubDist) {
     aadharSubDist = aAadharSubDist;
 }

 /**
  * Access method for aadharDist.
  *
  * @return the current value of aadharDist
  */
 public String getAadharDist() {
     return aadharDist;
 }

 /**
  * Setter method for aadharDist.
  *
  * @param aAadharDist the new value for aadharDist
  */
 public void setAadharDist(String aAadharDist) {
     aadharDist = aAadharDist;
 }

 /**
  * Access method for aadharState.
  *
  * @return the current value of aadharState
  */
 public String getAadharState() {
     return aadharState;
 }

 /**
  * Setter method for aadharState.
  *
  * @param aAadharState the new value for aadharState
  */
 public void setAadharState(String aAadharState) {
     aadharState = aAadharState;
 }

 /**
  * Access method for aadharPo.
  *
  * @return the current value of aadharPo
  */
 public String getAadharPo() {
     return aadharPo;
 }

 /**
  * Setter method for aadharPo.
  *
  * @param aAadharPo the new value for aadharPo
  */
 public void setAadharPo(String aAadharPo) {
     aadharPo = aAadharPo;
 }

 /**
  * Access method for aadharDob.
  *
  * @return the current value of aadharDob
  */
 public String getAadharDob() {
     return aadharDob;
 }

 /**
  * Setter method for aadharDob.
  *
  * @param aAadharDob the new value for aadharDob
  */
 public void setAadharDob(String aAadharDob) {
     aadharDob = aAadharDob;
 }

 /**
  * Access method for aadharPincode.
  *
  * @return the current value of aadharPincode
  */
 public String getAadharPincode() {
     return aadharPincode;
 }

 /**
  * Setter method for aadharPincode.
  *
  * @param aAadharPincode the new value for aadharPincode
  */
 public void setAadharPincode(String aAadharPincode) {
     aadharPincode = aAadharPincode;
 }

 /**
  * Access method for aadharGender.
  *
  * @return the current value of aadharGender
  */
 public String getAadharGender() {
     return aadharGender;
 }

 /**
  * Setter method for aadharGender.
  *
  * @param aAadharGender the new value for aadharGender
  */
 public void setAadharGender(String aAadharGender) {
     aadharGender = aAadharGender;
 }

 /**
  * Access method for aadharLoc.
  *
  * @return the current value of aadharLoc
  */
 public String getAadharLoc() {
     return aadharLoc;
 }

 /**
  * Setter method for aadharLoc.
  *
  * @param aAadharLoc the new value for aadharLoc
  */
 public void setAadharLoc(String aAadharLoc) {
     aadharLoc = aAadharLoc;
 }

 /**
  * Access method for panPic.
  *
  * @return the current value of panPic
  */
 public String getPanPic() {
     return panPic;
 }

 /**
  * Setter method for panPic.
  *
  * @param aPanPic the new value for panPic
  */
 public void setPanPic(String aPanPic) {
     panPic = aPanPic;
 }

 /**
  * Access method for customerPic.
  *
  * @return the current value of customerPic
  */
 public String getCustomerPic() {
     return customerPic;
 }

 /**
  * Setter method for customerPic.
  *
  * @param aCustomerPic the new value for customerPic
  */
 public void setCustomerPic(String aCustomerPic) {
     customerPic = aCustomerPic;
 }

 /**
  * Access method for passportPic.
  *
  * @return the current value of passportPic
  */
 public String getPassportPic() {
     return passportPic;
 }

 /**
  * Setter method for passportPic.
  *
  * @param aPassportPic the new value for passportPic
  */
 public void setPassportPic(String aPassportPic) {
     passportPic = aPassportPic;
 }

 /**
  * Access method for voteridPic.
  *
  * @return the current value of voteridPic
  */
 public String getVoteridPic() {
     return voteridPic;
 }

 /**
  * Setter method for voteridPic.
  *
  * @param aVoteridPic the new value for voteridPic
  */
 public void setVoteridPic(String aVoteridPic) {
     voteridPic = aVoteridPic;
 }

 /**
  * Access method for dlPic.
  *
  * @return the current value of dlPic
  */
 public String getDlPic() {
     return dlPic;
 }

 /**
  * Setter method for dlPic.
  *
  * @param aDlPic the new value for dlPic
  */
 public void setDlPic(String aDlPic) {
     dlPic = aDlPic;
 }

 /**
  * Access method for rationPic.
  *
  * @return the current value of rationPic
  */
 public String getRationPic() {
     return rationPic;
 }

 /**
  * Setter method for rationPic.
  *
  * @param aRationPic the new value for rationPic
  */
 public void setRationPic(String aRationPic) {
     rationPic = aRationPic;
 }

 /**
  * Access method for h1.
  *
  * @return the current value of h1
  */
 public String getH1() {
     return h1;
 }

 /**
  * Setter method for h1.
  *
  * @param aH1 the new value for h1
  */
 public void setH1(String aH1) {
     h1 = aH1;
 }

 /**
  * Access method for h2.
  *
  * @return the current value of h2
  */
 public String getH2() {
     return h2;
 }

 /**
  * Setter method for h2.
  *
  * @param aH2 the new value for h2
  */
 public void setH2(String aH2) {
     h2 = aH2;
 }

 /**
  * Access method for h3.
  *
  * @return the current value of h3
  */
 public String getH3() {
     return h3;
 }

 /**
  * Setter method for h3.
  *
  * @param aH3 the new value for h3
  */
 public void setH3(String aH3) {
     h3 = aH3;
 }

 /**
  * Access method for h4.
  *
  * @return the current value of h4
  */
 public String getH4() {
     return h4;
 }

 /**
  * Setter method for h4.
  *
  * @param aH4 the new value for h4
  */
 public void setH4(String aH4) {
     h4 = aH4;
 }

 /**
  * Access method for h5.
  *
  * @return the current value of h5
  */
 public String getH5() {
     return h5;
 }

 /**
  * Setter method for h5.
  *
  * @param aH5 the new value for h5
  */
 public void setH5(String aH5) {
     h5 = aH5;
 }

 /**
  * Access method for createUser.
  *
  * @return the current value of createUser
  */
 public String getCreateUser() {
     return createUser;
 }

 /**
  * Setter method for createUser.
  *
  * @param aCreateUser the new value for createUser
  */
 public void setCreateUser(String aCreateUser) {
     createUser = aCreateUser;
 }

 /**
  * Access method for modifyUser.
  *
  * @return the current value of modifyUser
  */
 public String getModifyUser() {
     return modifyUser;
 }

 /**
  * Setter method for modifyUser.
  *
  * @param aModifyUser the new value for modifyUser
  */
 public void setModifyUser(String aModifyUser) {
     modifyUser = aModifyUser;
 }

 /**
  * Access method for loan.
  *
  * @return the current value of loan
  */
 public Set<Loan> getLoan() {
     return loan;
 }

 /**
  * Setter method for loan.
  *
  * @param aLoan the new value for loan
  */
 public void setLoan(Set<Loan> aLoan) {
     loan = aLoan;
 }

 /**
  * Access method for receivable.
  *
  * @return the current value of receivable
  */
 public Set<Receivable> getReceivable() {
     return receivable;
 }

 /**
  * Setter method for receivable.
  *
  * @param aReceivable the new value for receivable
  */
 public void setReceivable(Set<Receivable> aReceivable) {
     receivable = aReceivable;
 }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aadhaarEmail == null) ? 0 : aadhaarEmail.hashCode());
		result = prime * result + ((aadharCo == null) ? 0 : aadharCo.hashCode());
		result = prime * result + ((aadharDist == null) ? 0 : aadharDist.hashCode());
		result = prime * result + ((aadharDob == null) ? 0 : aadharDob.hashCode());
		result = prime * result + ((aadharGender == null) ? 0 : aadharGender.hashCode());
		result = prime * result + ((aadharHouse == null) ? 0 : aadharHouse.hashCode());
		result = prime * result + ((aadharLm == null) ? 0 : aadharLm.hashCode());
		result = prime * result + ((aadharLoc == null) ? 0 : aadharLoc.hashCode());
		result = prime * result + ((aadharName == null) ? 0 : aadharName.hashCode());
		result = prime * result + ((aadharNo == null) ? 0 : aadharNo.hashCode());
		result = prime * result + ((aadharPhone == null) ? 0 : aadharPhone.hashCode());
		result = prime * result + ((aadharPincode == null) ? 0 : aadharPincode.hashCode());
		result = prime * result + ((aadharPo == null) ? 0 : aadharPo.hashCode());
		result = prime * result + ((aadharState == null) ? 0 : aadharState.hashCode());
		result = prime * result + ((aadharStreet == null) ? 0 : aadharStreet.hashCode());
		result = prime * result + ((aadharSubDist == null) ? 0 : aadharSubDist.hashCode());
		result = prime * result + ((aadharVtc == null) ? 0 : aadharVtc.hashCode());
		result = prime * result + ((acccountType == null) ? 0 : acccountType.hashCode());
		result = prime * result + ((accountNo == null) ? 0 : accountNo.hashCode());
		result = prime * result + age;
		result = prime * result + ((annualIncome == null) ? 0 : annualIncome.hashCode());
		result = prime * result + ((caste == null) ? 0 : caste.hashCode());
		result = prime * result + ((cibilScore == null) ? 0 : cibilScore.hashCode());
		result = prime * result + ((createUser == null) ? 0 : createUser.hashCode());
		result = prime * result + ((currentEmi == null) ? 0 : currentEmi.hashCode());
		result = prime * result + ((customerPic == null) ? 0 : customerPic.hashCode());
		result = prime * result + ((dlPic == null) ? 0 : dlPic.hashCode());
		result = prime * result + ((dob == null) ? 0 : dob.hashCode());
		result = prime * result + ((drivingLicense == null) ? 0 : drivingLicense.hashCode());
		result = prime * result + ((education == null) ? 0 : education.hashCode());
		result = prime * result + ((emailId == null) ? 0 : emailId.hashCode());
		result = prime * result + ((fatherName == null) ? 0 : fatherName.hashCode());
		result = prime * result + ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + (fourWheeler ? 1231 : 1237);
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + ((h1 == null) ? 0 : h1.hashCode());
		result = prime * result + ((h2 == null) ? 0 : h2.hashCode());
		result = prime * result + ((h3 == null) ? 0 : h3.hashCode());
		result = prime * result + ((h4 == null) ? 0 : h4.hashCode());
		result = prime * result + ((h5 == null) ? 0 : h5.hashCode());
		result = prime * result + id;
		result = prime * result + ((ifscCode == null) ? 0 : ifscCode.hashCode());
		result = prime * result + ((jobType == null) ? 0 : jobType.hashCode());
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((loanReason == null) ? 0 : loanReason.hashCode());
		result = prime * result + ((maritalStatus == null) ? 0 : maritalStatus.hashCode());
		result = prime * result + ((modifyUser == null) ? 0 : modifyUser.hashCode());
		result = prime * result + ((motherName == null) ? 0 : motherName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((nameInBank == null) ? 0 : nameInBank.hashCode());
		result = prime * result + ((offAddr == null) ? 0 : offAddr.hashCode());
		result = prime * result + ((offAddr2 == null) ? 0 : offAddr2.hashCode());
		result = prime * result + ((offCity == null) ? 0 : offCity.hashCode());
		result = prime * result + ((offName == null) ? 0 : offName.hashCode());
		result = prime * result + ((offPh == null) ? 0 : offPh.hashCode());
		result = prime * result + ((offPin == null) ? 0 : offPin.hashCode());
		result = prime * result + ((offState == null) ? 0 : offState.hashCode());
		result = prime * result + ((offStd == null) ? 0 : offStd.hashCode());
		result = prime * result + (ownHouse ? 1231 : 1237);
		result = prime * result + ((panPic == null) ? 0 : panPic.hashCode());
		result = prime * result + ((panno == null) ? 0 : panno.hashCode());
		result = prime * result + ((passportNo == null) ? 0 : passportNo.hashCode());
		result = prime * result + ((passportPic == null) ? 0 : passportPic.hashCode());
		result = prime * result + ((permAddr == null) ? 0 : permAddr.hashCode());
		result = prime * result + ((permAddr2 == null) ? 0 : permAddr2.hashCode());
		result = prime * result + ((permCity == null) ? 0 : permCity.hashCode());
		result = prime * result + ((permPincode == null) ? 0 : permPincode.hashCode());
		result = prime * result + ((permState == null) ? 0 : permState.hashCode());
		result = prime * result + ((phone1 == null) ? 0 : phone1.hashCode());
		result = prime * result + ((phone2 == null) ? 0 : phone2.hashCode());
		result = prime * result + ((profile == null) ? 0 : profile.hashCode());
		result = prime * result + ((rationCard == null) ? 0 : rationCard.hashCode());
		result = prime * result + ((rationPic == null) ? 0 : rationPic.hashCode());
		result = prime * result + ((religion == null) ? 0 : religion.hashCode());
		result = prime * result + ((resiAddr == null) ? 0 : resiAddr.hashCode());
		result = prime * result + ((resiAddress2 == null) ? 0 : resiAddress2.hashCode());
		result = prime * result + ((resiCity == null) ? 0 : resiCity.hashCode());
		result = prime * result + ((resiCumOff == null) ? 0 : resiCumOff.hashCode());
		result = prime * result + ((resiMobile == null) ? 0 : resiMobile.hashCode());
		result = prime * result + ((resiPh == null) ? 0 : resiPh.hashCode());
		result = prime * result + ((resiPin == null) ? 0 : resiPin.hashCode());
		result = prime * result + ((resiState == null) ? 0 : resiState.hashCode());
		result = prime * result + ((salorself == null) ? 0 : salorself.hashCode());
		result = prime * result + ((spouseName == null) ? 0 : spouseName.hashCode());
		result = prime * result + (twoWheeler ? 1231 : 1237);
		result = prime * result + ((voterid == null) ? 0 : voterid.hashCode());
		result = prime * result + ((voteridPic == null) ? 0 : voteridPic.hashCode());
		return result;
	}

	

	@Override
	public String toString() {
		return String.format(
				"Customer [id=%s, name=%s, phone1=%s, phone2=%s, emailId=%s, dob=%s, age=%s, cibilScore=%s, gender=%s, religion=%s, caste=%s, firstname=%s, lastname=%s, fatherName=%s, motherName=%s, spouseName=%s, passportNo=%s, voterid=%s, drivingLicense=%s, rationCard=%s, panno=%s, nameInBank=%s, accountNo=%s, ifscCode=%s, acccountType=%s, maritalStatus=%s, jobType=%s, profile=%s, salorself=%s, annualIncome=%s, currentEmi=%s, loanReason=%s, education=%s, ownHouse=%s, twoWheeler=%s, fourWheeler=%s, resiAddr=%s, resiAddress2=%s, resiCity=%s, resiPin=%s, resiState=%s, resiPh=%s, resiMobile=%s, permAddr=%s, permAddr2=%s, permCity=%s, permState=%s, permPincode=%s, offName=%s, offAddr=%s, offAddr2=%s, offCity=%s, offState=%s, offPin=%s, offStd=%s, offPh=%s, resiCumOff=%s, aadharNo=%s, aadharName=%s, aadharPhone=%s, aadhaarEmail=%s, aadharCo=%s, aadharHouse=%s, aadharStreet=%s, aadharLm=%s, aadharVtc=%s, aadharSubDist=%s, aadharDist=%s, aadharState=%s, aadharPo=%s, aadharDob=%s, aadharPincode=%s, aadharGender=%s, aadharLoc=%s, panPic=%s, customerPic=%s, passportPic=%s, voteridPic=%s, dlPic=%s, rationPic=%s, h1=%s, h2=%s, h3=%s, h4=%s, h5=%s, createUser=%s, modifyUser=%s, loan=%s, receivable=%s]",
				id, name, phone1, phone2, emailId, dob, age, cibilScore, gender, religion, caste, firstname, lastname,
				fatherName, motherName, spouseName, passportNo, voterid, drivingLicense, rationCard, panno, nameInBank,
				accountNo, ifscCode, acccountType, maritalStatus, jobType, profile, salorself, annualIncome, currentEmi,
				loanReason, education, ownHouse, twoWheeler, fourWheeler, resiAddr, resiAddress2, resiCity, resiPin,
				resiState, resiPh, resiMobile, permAddr, permAddr2, permCity, permState, permPincode, offName, offAddr,
				offAddr2, offCity, offState, offPin, offStd, offPh, resiCumOff, aadharNo, aadharName, aadharPhone,
				aadhaarEmail, aadharCo, aadharHouse, aadharStreet, aadharLm, aadharVtc, aadharSubDist, aadharDist,
				aadharState, aadharPo, aadharDob, aadharPincode, aadharGender, aadharLoc, panPic, customerPic,
				passportPic, voteridPic, dlPic, rationPic, h1, h2, h3, h4, h5, createUser, modifyUser,
				loan, receivable);
	}

}
