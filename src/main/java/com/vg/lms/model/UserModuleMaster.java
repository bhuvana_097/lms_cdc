package com.vg.lms.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="lms_user_module_master")
public class UserModuleMaster implements Serializable {
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="module", length=15)
    private String module;
    @Column(name="dept", length=15)
    private String dept;
    @Column(name="l1")
    private int l1;
    @Column(name="l2")
    private int l2;
    @Column(name="l3")
    private int l3;
    @Column(name="l4")
    private int l4;
    @Column(name="l5")
    private int l5;
    @Column(name="create_user", length=50)
    private String createUser;   
    @Column(name="create_time")
    private Date createTime;
    @Column(name="modify_user", length=50)
    private String modifyUser;    
    @Column(name="modify_time")
    private Date modifyTime;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public int getL1() {
		return l1;
	}
	public void setL1(int l1) {
		this.l1 = l1;
	}
	public int getL2() {
		return l2;
	}
	public void setL2(int l2) {
		this.l2 = l2;
	}
	public int getL3() {
		return l3;
	}
	public void setL3(int l3) {
		this.l3 = l3;
	}
	public int getL4() {
		return l4;
	}
	public void setL4(int l4) {
		this.l4 = l4;
	}
	public int getL5() {
		return l5;
	}
	public void setL5(int l5) {
		this.l5 = l5;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
	public Date getModifyTime() {
		return modifyTime;
	}
	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}
	@Override
	public String toString() {
		return "UserModuleMaster [id=" + id + ", module=" + module + ", dept=" + dept + ", l1=" + l1 + ", l2=" + l2
				+ ", l3=" + l3 + ", l4=" + l4 + ", l5=" + l5 + ", createUser=" + createUser + ", createTime="
				+ createTime + ", modifyUser=" + modifyUser + ", modifyTime=" + modifyTime + "]";
	}
    
    
}
