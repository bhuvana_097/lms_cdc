package com.vg.lms.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity(name="lms_loan_status")
public class WriteOff {

	
	
	private static final long serialVersionUID = 2361447040422992317L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, length=26)
	 private int id;
    @Column(name="loan_no", length=32)
    private String loanNo;
    @Column(name="status", length=25)
    private String status;
    @Column(name="status_disposition", length=50)
    private String statusDisposition;
    @Column(name="created_by", length=30)
    private String createdBy;
    @Column(name="create_time")
    private Timestamp createTime;
    @Column(name="modified_by", length=30)
    private String modifiedBy;
    @Column(name="status_time")
    private Timestamp statusTime;
    @Column(name="modified_time")
    private Timestamp modifiedTime;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoanNo() {
		return loanNo;
	}
	public void setLoanNo(String loanNo) {
		this.loanNo = loanNo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusDisposition() {
		return statusDisposition;
	}
	public void setStatusDisposition(String statusDisposition) {
		this.statusDisposition = statusDisposition;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Timestamp getStatusTime() {
		return statusTime;
	}
	public void setStatusTime(Timestamp statusTime) {
		this.statusTime = statusTime;
	}
	public Timestamp getModifiedTime() {
		return modifiedTime;
	}
	public void setModifiedTime(Timestamp modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
