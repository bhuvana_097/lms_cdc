// Generated with g9.

package com.vg.lms.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="scheme_master")
public class SchemeMaster implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1829507439093042386L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="s_no", unique=true, nullable=false, precision=10)
    private int sNo;
    @Column(name="scheme_id", length=15)
    private String schemeId;
    @Column(length=50)
    private String name;
    @Column(name="start_date")
    private Timestamp startDate;
    @Column(name="end_date")
    private Timestamp endDate;
    @Column(name="created_by", length=50)
    private String createdBy;
    @Column(length=250)
    private String state;
    @Column(length=250)
    private String branch;
    @Column(length=250)
    private String make;
    @Column(length=250)
    private String model;
    @Column(length=250)
    private String dealer;
    @Column(length=250)
    private String gender;
    @Column(name="vehicle_type", length=250)
    private String vehicleType;
    @Column(precision=5, scale=2)
    private float ltv;
    @Column(name="irr_ach_reg", precision=5, scale=2)
    private float irrAchReg;
    @Column(name="irr_ach_ireg_cash", precision=5, scale=2)
    private float irrAchIregCash;
    @Column(name="cash_charges_adv", precision=15, scale=2)
    private float cashChargesAdv;
    @Column(name="cash_charges_tail", precision=15, scale=2)
    private float cashChargesTail;
    @Column(name="adv_emi", precision=5, scale=2)
    private float advEmi;
    @Column(name="tenor_min", precision=10)
    private int tenorMin;
    @Column(name="tenor_max", precision=10)
    private int tenorMax;
    @Column(name="pro_fee_adv", precision=15, scale=2)
    private float proFeeAdv;
    @Column(name="pro_fee_tail", precision=15, scale=2)
    private float proFeeTail;
    @Column(length=3)
    private boolean active;
    @Column(name="create_time", nullable=false)
    private Timestamp createTime;
    @Column(name="update_time", nullable=false)
    private Timestamp updateTime;

    /** Default constructor. */
    public SchemeMaster() {
        super();
    }

    /**
     * Access method for sNo.
     *
     * @return the current value of sNo
     */
    public int getSNo() {
        return sNo;
    }

    /**
     * Setter method for sNo.
     *
     * @param aSNo the new value for sNo
     */
    public void setSNo(int aSNo) {
        sNo = aSNo;
    }

    /**
     * Access method for schemeId.
     *
     * @return the current value of schemeId
     */
    public String getSchemeId() {
        return schemeId;
    }

    /**
     * Setter method for schemeId.
     *
     * @param aSchemeId the new value for schemeId
     */
    public void setSchemeId(String aSchemeId) {
        schemeId = aSchemeId;
    }

    /**
     * Access method for name.
     *
     * @return the current value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for name.
     *
     * @param aName the new value for name
     */
    public void setName(String aName) {
        name = aName;
    }

    /**
     * Access method for startDate.
     *
     * @return the current value of startDate
     */
    public Timestamp getStartDate() {
        return startDate;
    }

    /**
     * Setter method for startDate.
     *
     * @param aStartDate the new value for startDate
     */
    public void setStartDate(Timestamp aStartDate) {
        startDate = aStartDate;
    }

    /**
     * Access method for endDate.
     *
     * @return the current value of endDate
     */
    public Timestamp getEndDate() {
        return endDate;
    }

    /**
     * Setter method for endDate.
     *
     * @param aEndDate the new value for endDate
     */
    public void setEndDate(Timestamp aEndDate) {
        endDate = aEndDate;
    }

    /**
     * Access method for createdBy.
     *
     * @return the current value of createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Setter method for createdBy.
     *
     * @param aCreatedBy the new value for createdBy
     */
    public void setCreatedBy(String aCreatedBy) {
        createdBy = aCreatedBy;
    }

    /**
     * Access method for state.
     *
     * @return the current value of state
     */
    public String getState() {
        return state;
    }

    /**
     * Setter method for state.
     *
     * @param aState the new value for state
     */
    public void setState(String aState) {
        state = aState;
    }

    /**
     * Access method for branch.
     *
     * @return the current value of branch
     */
    public String getBranch() {
        return branch;
    }

    /**
     * Setter method for branch.
     *
     * @param aBranch the new value for branch
     */
    public void setBranch(String aBranch) {
        branch = aBranch;
    }

    /**
     * Access method for make.
     *
     * @return the current value of make
     */
    public String getMake() {
        return make;
    }

    /**
     * Setter method for make.
     *
     * @param aMake the new value for make
     */
    public void setMake(String aMake) {
        make = aMake;
    }

    /**
     * Access method for model.
     *
     * @return the current value of model
     */
    public String getModel() {
        return model;
    }

    /**
     * Setter method for model.
     *
     * @param aModel the new value for model
     */
    public void setModel(String aModel) {
        model = aModel;
    }

    /**
     * Access method for dealer.
     *
     * @return the current value of dealer
     */
    public String getDealer() {
        return dealer;
    }

    /**
     * Setter method for dealer.
     *
     * @param aDealer the new value for dealer
     */
    public void setDealer(String aDealer) {
        dealer = aDealer;
    }

    /**
     * Access method for gender.
     *
     * @return the current value of gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * Setter method for gender.
     *
     * @param aGender the new value for gender
     */
    public void setGender(String aGender) {
        gender = aGender;
    }

    /**
     * Access method for vehicleType.
     *
     * @return the current value of vehicleType
     */
    public String getVehicleType() {
        return vehicleType;
    }

    /**
     * Setter method for vehicleType.
     *
     * @param aVehicleType the new value for vehicleType
     */
    public void setVehicleType(String aVehicleType) {
        vehicleType = aVehicleType;
    }

    /**
     * Access method for ltv.
     *
     * @return the current value of ltv
     */
    public float getLtv() {
        return ltv;
    }

    /**
     * Setter method for ltv.
     *
     * @param aLtv the new value for ltv
     */
    public void setLtv(float aLtv) {
        ltv = aLtv;
    }

    /**
     * Access method for irrAchReg.
     *
     * @return the current value of irrAchReg
     */
    public float getIrrAchReg() {
        return irrAchReg;
    }

    /**
     * Setter method for irrAchReg.
     *
     * @param aIrrAchReg the new value for irrAchReg
     */
    public void setIrrAchReg(float aIrrAchReg) {
        irrAchReg = aIrrAchReg;
    }

    /**
     * Access method for irrAchIregCash.
     *
     * @return the current value of irrAchIregCash
     */
    public float getIrrAchIregCash() {
        return irrAchIregCash;
    }

    /**
     * Setter method for irrAchIregCash.
     *
     * @param aIrrAchIregCash the new value for irrAchIregCash
     */
    public void setIrrAchIregCash(float aIrrAchIregCash) {
        irrAchIregCash = aIrrAchIregCash;
    }

    /**
     * Access method for cashChargesAdv.
     *
     * @return the current value of cashChargesAdv
     */
    public float getCashChargesAdv() {
        return cashChargesAdv;
    }

    /**
     * Setter method for cashChargesAdv.
     *
     * @param aCashChargesAdv the new value for cashChargesAdv
     */
    public void setCashChargesAdv(float aCashChargesAdv) {
        cashChargesAdv = aCashChargesAdv;
    }

    /**
     * Access method for cashChargesTail.
     *
     * @return the current value of cashChargesTail
     */
    public float getCashChargesTail() {
        return cashChargesTail;
    }

    /**
     * Setter method for cashChargesTail.
     *
     * @param aCashChargesTail the new value for cashChargesTail
     */
    public void setCashChargesTail(float aCashChargesTail) {
        cashChargesTail = aCashChargesTail;
    }

    /**
     * Access method for advEmi.
     *
     * @return the current value of advEmi
     */
    public float getAdvEmi() {
        return advEmi;
    }

    /**
     * Setter method for advEmi.
     *
     * @param aAdvEmi the new value for advEmi
     */
    public void setAdvEmi(float aAdvEmi) {
        advEmi = aAdvEmi;
    }

    /**
     * Access method for tenorMin.
     *
     * @return the current value of tenorMin
     */
    public int getTenorMin() {
        return tenorMin;
    }

    /**
     * Setter method for tenorMin.
     *
     * @param aTenorMin the new value for tenorMin
     */
    public void setTenorMin(int aTenorMin) {
        tenorMin = aTenorMin;
    }

    /**
     * Access method for tenorMax.
     *
     * @return the current value of tenorMax
     */
    public int getTenorMax() {
        return tenorMax;
    }

    /**
     * Setter method for tenorMax.
     *
     * @param aTenorMax the new value for tenorMax
     */
    public void setTenorMax(int aTenorMax) {
        tenorMax = aTenorMax;
    }

    /**
     * Access method for proFeeAdv.
     *
     * @return the current value of proFeeAdv
     */
    public float getProFeeAdv() {
        return proFeeAdv;
    }

    /**
     * Setter method for proFeeAdv.
     *
     * @param aProFeeAdv the new value for proFeeAdv
     */
    public void setProFeeAdv(float aProFeeAdv) {
        proFeeAdv = aProFeeAdv;
    }

    /**
     * Access method for proFeeTail.
     *
     * @return the current value of proFeeTail
     */
    public float getProFeeTail() {
        return proFeeTail;
    }

    /**
     * Setter method for proFeeTail.
     *
     * @param aProFeeTail the new value for proFeeTail
     */
    public void setProFeeTail(float aProFeeTail) {
        proFeeTail = aProFeeTail;
    }

    /**
     * Access method for active.
     *
     * @return true if and only if active is currently true
     */
    public boolean getActive() {
        return active;
    }

    /**
     * Setter method for active.
     *
     * @param aActive the new value for active
     */
    public void setActive(boolean aActive) {
        active = aActive;
    }

    /**
     * Access method for createTime.
     *
     * @return the current value of createTime
     */
    public Timestamp getCreateTime() {
        return createTime;
    }

    /**
     * Setter method for createTime.
     *
     * @param aCreateTime the new value for createTime
     */
    public void setCreateTime(Timestamp aCreateTime) {
        createTime = aCreateTime;
    }

    /**
     * Access method for updateTime.
     *
     * @return the current value of updateTime
     */
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    /**
     * Setter method for updateTime.
     *
     * @param aUpdateTime the new value for updateTime
     */
    public void setUpdateTime(Timestamp aUpdateTime) {
        updateTime = aUpdateTime;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (active ? 1231 : 1237);
		result = prime * result + Float.floatToIntBits(advEmi);
		result = prime * result + ((branch == null) ? 0 : branch.hashCode());
		result = prime * result + Float.floatToIntBits(cashChargesAdv);
		result = prime * result + Float.floatToIntBits(cashChargesTail);
		result = prime * result + ((createTime == null) ? 0 : createTime.hashCode());
		result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
		result = prime * result + ((dealer == null) ? 0 : dealer.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + Float.floatToIntBits(irrAchIregCash);
		result = prime * result + Float.floatToIntBits(irrAchReg);
		result = prime * result + Float.floatToIntBits(ltv);
		result = prime * result + ((make == null) ? 0 : make.hashCode());
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + Float.floatToIntBits(proFeeAdv);
		result = prime * result + Float.floatToIntBits(proFeeTail);
		result = prime * result + sNo;
		result = prime * result + ((schemeId == null) ? 0 : schemeId.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + tenorMax;
		result = prime * result + tenorMin;
		result = prime * result + ((updateTime == null) ? 0 : updateTime.hashCode());
		result = prime * result + ((vehicleType == null) ? 0 : vehicleType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SchemeMaster other = (SchemeMaster) obj;
		if (active != other.active)
			return false;
		if (Float.floatToIntBits(advEmi) != Float.floatToIntBits(other.advEmi))
			return false;
		if (branch == null) {
			if (other.branch != null)
				return false;
		} else if (!branch.equals(other.branch))
			return false;
		if (Float.floatToIntBits(cashChargesAdv) != Float.floatToIntBits(other.cashChargesAdv))
			return false;
		if (Float.floatToIntBits(cashChargesTail) != Float.floatToIntBits(other.cashChargesTail))
			return false;
		if (createTime == null) {
			if (other.createTime != null)
				return false;
		} else if (!createTime.equals(other.createTime))
			return false;
		if (createdBy == null) {
			if (other.createdBy != null)
				return false;
		} else if (!createdBy.equals(other.createdBy))
			return false;
		if (dealer == null) {
			if (other.dealer != null)
				return false;
		} else if (!dealer.equals(other.dealer))
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (gender == null) {
			if (other.gender != null)
				return false;
		} else if (!gender.equals(other.gender))
			return false;
		if (Float.floatToIntBits(irrAchIregCash) != Float.floatToIntBits(other.irrAchIregCash))
			return false;
		if (Float.floatToIntBits(irrAchReg) != Float.floatToIntBits(other.irrAchReg))
			return false;
		if (Float.floatToIntBits(ltv) != Float.floatToIntBits(other.ltv))
			return false;
		if (make == null) {
			if (other.make != null)
				return false;
		} else if (!make.equals(other.make))
			return false;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Float.floatToIntBits(proFeeAdv) != Float.floatToIntBits(other.proFeeAdv))
			return false;
		if (Float.floatToIntBits(proFeeTail) != Float.floatToIntBits(other.proFeeTail))
			return false;
		if (sNo != other.sNo)
			return false;
		if (schemeId == null) {
			if (other.schemeId != null)
				return false;
		} else if (!schemeId.equals(other.schemeId))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (tenorMax != other.tenorMax)
			return false;
		if (tenorMin != other.tenorMin)
			return false;
		if (updateTime == null) {
			if (other.updateTime != null)
				return false;
		} else if (!updateTime.equals(other.updateTime))
			return false;
		if (vehicleType == null) {
			if (other.vehicleType != null)
				return false;
		} else if (!vehicleType.equals(other.vehicleType))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SchemeMaster [sNo=" + sNo + ", schemeId=" + schemeId + ", name=" + name + ", startDate=" + startDate
				+ ", endDate=" + endDate + ", createdBy=" + createdBy + ", state=" + state + ", branch=" + branch
				+ ", make=" + make + ", model=" + model + ", dealer=" + dealer + ", gender=" + gender + ", vehicleType="
				+ vehicleType + ", ltv=" + ltv + ", irrAchReg=" + irrAchReg + ", irrAchIregCash=" + irrAchIregCash
				+ ", cashChargesAdv=" + cashChargesAdv + ", cashChargesTail=" + cashChargesTail + ", advEmi=" + advEmi
				+ ", tenorMin=" + tenorMin + ", tenorMax=" + tenorMax + ", proFeeAdv=" + proFeeAdv + ", proFeeTail="
				+ proFeeTail + ", active=" + active + ", createTime=" + createTime + ", updateTime=" + updateTime + "]";
	}



}
