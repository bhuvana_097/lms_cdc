// Generated with g9.

package com.vg.lms.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity(name="lms_loan")
public class Loan implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8634290503878895664L;

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
   
    @Column(name="brnet_clientID", length=20)
    private String brnetClientId;
   
    @Column(name="appl_no", length=50)
    private String applNo;
    @Column(name="loan_no", length=30)
    private String loanNo;
   
    @Column(length=100)
    private String name;
    @Column(length=15)
    private String mobile;
    @Column(name="repay_mode", length=20)
    private String repayMode;
    @Column(name="agmnt_date")
    private Date agmntDate;
    @Column(name="real_agmnt_date")
    private Date realAgmntDate;
    @Column(name="first_due_date")
    private Date firstDueDate;
    @Column(name="last_due_date")
    private Date lastDueDate;
    @Column(precision=10, scale=2)
    private float amount;
    @Column(precision=3, scale=1)
    private float tenure;
    @Column(precision=5, scale=2)
    private float irr;
    @Column(name="proc_fee", precision=10, scale=2)
    private float procFee;
    @Column(name="pf_base", precision=10, scale=2)
    private float pfBase;
    @Column(name="pf_tax", precision=10, scale=2)
    private float pfTax;
    @Column(name="advance_emi", precision=3)
    private int advanceEmi;
    @Column(name="insurance_amount", precision=10, scale=2)
    private float insuranceAmount;
    @Column(name="ins_base", precision=10, scale=2)
    private float insBase;
    @Column(name="ins_tax", precision=10, scale=2)
    private float insTax;
    @Column(name="cash_coll_charges", precision=10, scale=2)
    private float cashCollCharges;
    @Column(name="ccc_base", precision=10, scale=2)
    private float cccBase;
    @Column(name="ccc_tax", precision=10, scale=2)
    private float cccTax;
    @Column(name="dealer_disbursement_amount", precision=15, scale=2)
    private float dealerDisbursementAmount;
    @Column(name="down_payment", precision=15, scale=2)
    private float downPayment;
    @Column(precision=10, scale=2)
    private float emi;
    @Column(name="future_principle", precision=10, scale=2)
    private float futurePrinciple;
    @Column(name="excess_paid", precision=10, scale=2)
    private float excessPaid;
    @Column(precision=10, scale=2)
    private float arrears;
    @Column(precision=10, scale=2)
    private float bc;
    @Column(name="bc_tax", precision=10, scale=2)
    private float bcTax;
    @Column(name="bc_paid", precision=10, scale=2)
    private float bcPaid;
    @Column(name="bc_tax_paid", precision=10, scale=2)
    private float bcTaxPaid;
    @Column(name="od_amount", precision=10, scale=2)
    private float odAmount;
   /* @Column(precision=10, scale=2)
    private float odc;*/
    @Column(name="odc_paid", precision=10, scale=2)
    private float odcPaid;
    @Column(precision=10, scale=2)
    private float oc;
    @Column(name="to_be_collected", precision=10, scale=2)
    private float toBeCollected;
    @Column(precision=10, scale=2)
    private float collected;
    @Column(name="od_days", precision=10)
    private int odDays;
    @Column(name="od_months", precision=10)
    private int odMonths;
    @Column(name="bc_count", precision=5)
    private short bcCount;
    @Column(length=10)
    private String status;
    @Column(name="cancel_reason", length=100)
    private String cancelReason;
    @Column(length=3)
    private String book;
    @Column(length=20)
    private String hypothecation;
    @Column(name="hypothecation_history", length=200)
    private String hypothecationHistory;
    @Column(name="co_lender", length=32)
    private String coLender;
    @Column(name="co_lender_status", length=3)
    private String coLenderStatus;
    @Column(name="co_lender_disposition", length=20)
    private String coLenderDisposition;
    @Column(name="co_lend_percent", precision=5, scale=2)
    private float coLendPercent;
    @Column(name="co_lender_roi", precision=6, scale=2)
    private float coLenderRoi;
    @Column(precision=10, scale=2)
    private float expense;
    @Column(name="dealer_code", length=15)
    private String dealerCode;
    @Column(name="dealer_name", length=100)
    private String dealerName;
    @Column(name="loan_agreement", length=30)
    private String loanAgreement;
    @Column(name="mandate_file", length=30)
    private String mandateFile;
    @Column(length=30)
    private String h1;
    @Column(length=30)
    private String h2;
    @Column(length=30)
    private String h3;
    @Column(length=30)
    private String h4;
    @Column(length=30)
    private String h5;
    @Column(name="create_user", length=10)
    private String createUser;
   
    @Column(name="modify_user", length=10)
    private String modifyUser;
  
    @Column(name="disbursement_time")
    private Date disbursementTime;
    @Column(name="gl_entry", precision=3)
    private short glEntry;
    @Column(name="principle_paid", precision=10, scale=2)
    private float principlePaid;
    @Column(name="interest_paid", precision=10, scale=2)
    private float interestPaid;
    @Column(name="total_paid", precision=10, scale=2)
    private float totalPaid;
    @Column(name="principle_tot", precision=10, scale=2)
    private float principleTot;
    @Column(name="interest_tot", precision=10, scale=2)
    private float interestTot;
    @Column(name="tot_out", precision=10, scale=2)
    private float totOut;
    @Column(name="overdue_amount", precision=10, scale=2)
    private float overdueAmount;
    @Column(name="branch_code", precision=5)
    private int branchCode;
   /* @Column(name="tmp_bc_paid", precision=10, scale=2)
    private float tmpBcPaid;
    @Column(name="tmp_bc_tax_paid", precision=10, scale=2)
    private float tmpBcTaxPaid;
    @Column(name="tmp_odc_paid", precision=10, scale=2)
    private float tmpOdcPaid;
    @Column(name="tmp_prin_paid", precision=10, scale=2)
    private float tmpPrinPaid;
    @Column(name="tmp_int_paid", precision=10, scale=2)
    private float tmpIntPaid;*/
    @Column(name="od_count", precision=5)
    private short odCount;
  /*  @Column(name="tmp_rcpt_dt")
    private Date tmpRcptDt;*/
    @Column(length=50)
    private String umrn;
    @Column(name="pf_tail", precision=15, scale=2)
    private float pfTail;
    @Column(name="pf_tail_base", precision=15, scale=2)
    private float pfTailBase;
    @Column(name="pf_tail_tax", precision=15, scale=2)
    private float pfTailTax;
    @Column(name="cc_tail", precision=15, scale=2)
    private float ccTail;
    @Column(name="cc_tail_base", precision=15, scale=2)
    private float ccTailBase;
    @Column(name="cc_tail_tax", precision=15, scale=2)
    private float ccTailTax;
    
    
    @Column(name="prin_arrears", precision=15, scale=2)
    private float prinArears;
    @Column(name="int_arrears", precision=15, scale=2)
    private float intArrears;
    @Column(name="prin_os", precision=15, scale=2)
    private float prinOutstanding;
    @Column(name="int_on_pos_since_last_due", precision=15, scale=2)
    private float intLastDue;
    @Column(name="moratorium_int", precision=15, scale=2)
    private float moratoriumInterest;
    @Column(name="odc_due", precision=15, scale=2)
    private float odc;
    @Column(name="charges_due", precision=15, scale=2)
    private float chargesDue;
    @Column(name="fcl_one_day_int", precision=15, scale=2)
    private float fclOneDayInterest;
    @Column(name="fcl_charges", precision=15, scale=2)
    private float fclCharges;
    @Column(name="fcl_amount", precision=15, scale=2)
    private float fclAmount;   
    @Column(name="status_disposition", length=24)
    private String statusDisposition; 
    @Column(name="closed_time")
    private Date closedTime;
    @Column(name="cancel_time")
    private Date cancelTime;
    @Column(name="cancel_user", length=30)
    private String cancelUser;
    @Column(name="sc_charge", precision=15, scale=2)
    private float scCharge;   

    @Column(name="securitisation", length=55)
    private String securitisation;
    
    @Column(name="product", length=30)
    private String product;
    
    @Column(name="ckyc_number", length=30)
    private String ckycNumber;
    
    @Column(name="morat_reason", length=55)
    private String moratReason;
    
    @Column(name="morat_int", precision=15, scale=2)
    private float moratInterest;
    
    @Column(name="morat_tenure", precision=15)
    private float moratTenure;
    
    
    @Column(name="moratorium_int_waiver", precision=15, scale=2)
    private float moratoriumIntWaiver;
    
    @Column(name="covid_pay_int", precision=15, scale=2)
    private float covidPayInt;
   
    @Column(name="sec", length=2)
    private String flagSec;
    
  @Column(name="document_charge", precision=15, scale=2)
    private float documentCharge;
    @Column(name="document_charge_base", precision=15, scale=2)
    private float documentChargeBase;
    @Column(name="document_charge_tax", precision=15, scale=2)
    private float documentChargeTax;
    
    @Column(name="fcl_block", length=11)
    private String fclBlock;
    
    
    /*@Column(name="ex_gratia_credit_amount", precision=15, scale=2)
    private float ExGratiaCreditAmount;
   
    @Column(name="ex_gratia_credit_amount_excess", precision=15, scale=2)
    private float ExGratiaCreditAmountExcess;*/
    
    
    /*@OneToMany(fetch = FetchType.EAGER,mappedBy="loan",cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<Asset> asset;*/
    @OneToOne(fetch = FetchType.EAGER,mappedBy="loan",cascade = {CascadeType.ALL})
    @JoinColumn(name="asset_id", referencedColumnName="id")
    private Asset asset;
   
    @OneToMany(fetch = FetchType.EAGER,mappedBy="loan",cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<Disbursement> disbursement;
    @ManyToOne
    @JoinColumn(name="cust_id")
    private Customer customer;
    @ManyToOne
    @JoinColumn(name="gurantor_id")
    private Customer gurantor;
    @OneToMany(fetch = FetchType.EAGER,mappedBy="loan",cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<Receivable> receivable;
    @OneToMany(fetch = FetchType.EAGER,mappedBy="loan",cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<Transaction> transaction;    
    

 
	/** Default constructor. */
    public Loan() {
        super();
    }



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getBrnetClientId() {
		return brnetClientId;
	}



	public void setBrnetClientId(String brnetClientId) {
		this.brnetClientId = brnetClientId;
	}



	public String getApplNo() {
		return applNo;
	}



	public void setApplNo(String applNo) {
		this.applNo = applNo;
	}



	public String getLoanNo() {
		return loanNo;
	}



	public void setLoanNo(String loanNo) {
		this.loanNo = loanNo;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getMobile() {
		return mobile;
	}



	public void setMobile(String mobile) {
		this.mobile = mobile;
	}



	public String getRepayMode() {
		return repayMode;
	}



	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode;
	}



	public Date getAgmntDate() {
		return agmntDate;
	}



	public void setAgmntDate(Date agmntDate) {
		this.agmntDate = agmntDate;
	}



	public Date getRealAgmntDate() {
		return realAgmntDate;
	}



	public void setRealAgmntDate(Date realAgmntDate) {
		this.realAgmntDate = realAgmntDate;
	}



	public Date getFirstDueDate() {
		return firstDueDate;
	}



	public void setFirstDueDate(Date firstDueDate) {
		this.firstDueDate = firstDueDate;
	}



	public Date getLastDueDate() {
		return lastDueDate;
	}



	public void setLastDueDate(Date lastDueDate) {
		this.lastDueDate = lastDueDate;
	}



	public float getAmount() {
		return amount;
	}



	public void setAmount(float amount) {
		this.amount = amount;
	}



	public float getTenure() {
		return tenure;
	}



	public void setTenure(float tenure) {
		this.tenure = tenure;
	}



	public float getIrr() {
		return irr;
	}



	public void setIrr(float irr) {
		this.irr = irr;
	}



	public float getProcFee() {
		return procFee;
	}



	public void setProcFee(float procFee) {
		this.procFee = procFee;
	}



	public float getPfBase() {
		return pfBase;
	}



	public void setPfBase(float pfBase) {
		this.pfBase = pfBase;
	}



	public float getPfTax() {
		return pfTax;
	}



	public void setPfTax(float pfTax) {
		this.pfTax = pfTax;
	}



	public int getAdvanceEmi() {
		return advanceEmi;
	}



	public void setAdvanceEmi(int advanceEmi) {
		this.advanceEmi = advanceEmi;
	}



	public float getInsuranceAmount() {
		return insuranceAmount;
	}



	public void setInsuranceAmount(float insuranceAmount) {
		this.insuranceAmount = insuranceAmount;
	}



	public float getInsBase() {
		return insBase;
	}



	public void setInsBase(float insBase) {
		this.insBase = insBase;
	}



	public float getInsTax() {
		return insTax;
	}



	public void setInsTax(float insTax) {
		this.insTax = insTax;
	}



	public float getCashCollCharges() {
		return cashCollCharges;
	}



	public void setCashCollCharges(float cashCollCharges) {
		this.cashCollCharges = cashCollCharges;
	}



	public float getCccBase() {
		return cccBase;
	}



	public void setCccBase(float cccBase) {
		this.cccBase = cccBase;
	}



	public float getCccTax() {
		return cccTax;
	}



	public void setCccTax(float cccTax) {
		this.cccTax = cccTax;
	}



	public float getDealerDisbursementAmount() {
		return dealerDisbursementAmount;
	}



	public void setDealerDisbursementAmount(float dealerDisbursementAmount) {
		this.dealerDisbursementAmount = dealerDisbursementAmount;
	}



	public float getDownPayment() {
		return downPayment;
	}



	public void setDownPayment(float downPayment) {
		this.downPayment = downPayment;
	}



	public float getEmi() {
		return emi;
	}



	public void setEmi(float emi) {
		this.emi = emi;
	}



	public float getFuturePrinciple() {
		return futurePrinciple;
	}



	public void setFuturePrinciple(float futurePrinciple) {
		this.futurePrinciple = futurePrinciple;
	}



	public float getExcessPaid() {
		return excessPaid;
	}



	public void setExcessPaid(float excessPaid) {
		this.excessPaid = excessPaid;
	}



	public float getArrears() {
		return arrears;
	}



	public void setArrears(float arrears) {
		this.arrears = arrears;
	}



	public float getBc() {
		return bc;
	}



	public void setBc(float bc) {
		this.bc = bc;
	}



	public float getBcTax() {
		return bcTax;
	}



	public void setBcTax(float bcTax) {
		this.bcTax = bcTax;
	}



	public float getBcPaid() {
		return bcPaid;
	}



	public void setBcPaid(float bcPaid) {
		this.bcPaid = bcPaid;
	}



	public float getBcTaxPaid() {
		return bcTaxPaid;
	}



	public void setBcTaxPaid(float bcTaxPaid) {
		this.bcTaxPaid = bcTaxPaid;
	}



	public float getOdAmount() {
		return odAmount;
	}



	public void setOdAmount(float odAmount) {
		this.odAmount = odAmount;
	}



	public float getOdcPaid() {
		return odcPaid;
	}



	public void setOdcPaid(float odcPaid) {
		this.odcPaid = odcPaid;
	}



	public float getOc() {
		return oc;
	}



	public void setOc(float oc) {
		this.oc = oc;
	}



	public float getToBeCollected() {
		return toBeCollected;
	}



	public void setToBeCollected(float toBeCollected) {
		this.toBeCollected = toBeCollected;
	}



	public float getCollected() {
		return collected;
	}



	public void setCollected(float collected) {
		this.collected = collected;
	}



	public int getOdDays() {
		return odDays;
	}



	public void setOdDays(int odDays) {
		this.odDays = odDays;
	}



	public int getOdMonths() {
		return odMonths;
	}



	public void setOdMonths(int odMonths) {
		this.odMonths = odMonths;
	}



	public short getBcCount() {
		return bcCount;
	}



	public void setBcCount(short bcCount) {
		this.bcCount = bcCount;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	public String getCancelReason() {
		return cancelReason;
	}



	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}



	public String getBook() {
		return book;
	}



	public void setBook(String book) {
		this.book = book;
	}



	public String getHypothecation() {
		return hypothecation;
	}



	public void setHypothecation(String hypothecation) {
		this.hypothecation = hypothecation;
	}



	public String getHypothecationHistory() {
		return hypothecationHistory;
	}



	public void setHypothecationHistory(String hypothecationHistory) {
		this.hypothecationHistory = hypothecationHistory;
	}



	public String getCoLender() {
		return coLender;
	}



	public void setCoLender(String coLender) {
		this.coLender = coLender;
	}



	public String getCoLenderStatus() {
		return coLenderStatus;
	}



	public void setCoLenderStatus(String coLenderStatus) {
		this.coLenderStatus = coLenderStatus;
	}



	public String getCoLenderDisposition() {
		return coLenderDisposition;
	}



	public void setCoLenderDisposition(String coLenderDisposition) {
		this.coLenderDisposition = coLenderDisposition;
	}



	public float getCoLendPercent() {
		return coLendPercent;
	}



	public void setCoLendPercent(float coLendPercent) {
		this.coLendPercent = coLendPercent;
	}



	public float getCoLenderRoi() {
		return coLenderRoi;
	}



	public void setCoLenderRoi(float coLenderRoi) {
		this.coLenderRoi = coLenderRoi;
	}



	public float getExpense() {
		return expense;
	}



	public void setExpense(float expense) {
		this.expense = expense;
	}



	public String getDealerCode() {
		return dealerCode;
	}



	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}



	public String getDealerName() {
		return dealerName;
	}



	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}



	public String getLoanAgreement() {
		return loanAgreement;
	}



	public void setLoanAgreement(String loanAgreement) {
		this.loanAgreement = loanAgreement;
	}



	public String getMandateFile() {
		return mandateFile;
	}



	public void setMandateFile(String mandateFile) {
		this.mandateFile = mandateFile;
	}



	public String getH1() {
		return h1;
	}



	public void setH1(String h1) {
		this.h1 = h1;
	}



	public String getH2() {
		return h2;
	}



	public void setH2(String h2) {
		this.h2 = h2;
	}



	public String getH3() {
		return h3;
	}



	public void setH3(String h3) {
		this.h3 = h3;
	}



	public String getH4() {
		return h4;
	}



	public void setH4(String h4) {
		this.h4 = h4;
	}



	public String getH5() {
		return h5;
	}



	public void setH5(String h5) {
		this.h5 = h5;
	}



	public String getCreateUser() {
		return createUser;
	}



	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}



	public String getModifyUser() {
		return modifyUser;
	}



	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}



	public Date getDisbursementTime() {
		return disbursementTime;
	}



	public void setDisbursementTime(Date disbursementTime) {
		this.disbursementTime = disbursementTime;
	}



	public short getGlEntry() {
		return glEntry;
	}



	public void setGlEntry(short glEntry) {
		this.glEntry = glEntry;
	}



	public float getPrinciplePaid() {
		return principlePaid;
	}



	public void setPrinciplePaid(float principlePaid) {
		this.principlePaid = principlePaid;
	}



	public float getInterestPaid() {
		return interestPaid;
	}



	public void setInterestPaid(float interestPaid) {
		this.interestPaid = interestPaid;
	}



	public float getTotalPaid() {
		return totalPaid;
	}



	public void setTotalPaid(float totalPaid) {
		this.totalPaid = totalPaid;
	}



	public float getPrincipleTot() {
		return principleTot;
	}



	public void setPrincipleTot(float principleTot) {
		this.principleTot = principleTot;
	}



	public float getInterestTot() {
		return interestTot;
	}



	public void setInterestTot(float interestTot) {
		this.interestTot = interestTot;
	}



	public float getTotOut() {
		return totOut;
	}



	public void setTotOut(float totOut) {
		this.totOut = totOut;
	}



	public float getOverdueAmount() {
		return overdueAmount;
	}



	public void setOverdueAmount(float overdueAmount) {
		this.overdueAmount = overdueAmount;
	}



	public int getBranchCode() {
		return branchCode;
	}



	public void setBranchCode(int branchCode) {
		this.branchCode = branchCode;
	}



	public short getOdCount() {
		return odCount;
	}



	public void setOdCount(short odCount) {
		this.odCount = odCount;
	}



	public String getUmrn() {
		return umrn;
	}



	public void setUmrn(String umrn) {
		this.umrn = umrn;
	}



	public float getPfTail() {
		return pfTail;
	}



	public void setPfTail(float pfTail) {
		this.pfTail = pfTail;
	}



	public float getPfTailBase() {
		return pfTailBase;
	}



	public void setPfTailBase(float pfTailBase) {
		this.pfTailBase = pfTailBase;
	}



	public float getPfTailTax() {
		return pfTailTax;
	}



	public void setPfTailTax(float pfTailTax) {
		this.pfTailTax = pfTailTax;
	}



	public float getCcTail() {
		return ccTail;
	}



	public void setCcTail(float ccTail) {
		this.ccTail = ccTail;
	}



	public float getCcTailBase() {
		return ccTailBase;
	}



	public void setCcTailBase(float ccTailBase) {
		this.ccTailBase = ccTailBase;
	}



	public float getCcTailTax() {
		return ccTailTax;
	}



	public void setCcTailTax(float ccTailTax) {
		this.ccTailTax = ccTailTax;
	}



	public float getPrinArears() {
		return prinArears;
	}



	public void setPrinArears(float prinArears) {
		this.prinArears = prinArears;
	}



	public float getIntArrears() {
		return intArrears;
	}



	public void setIntArrears(float intArrears) {
		this.intArrears = intArrears;
	}



	public float getPrinOutstanding() {
		return prinOutstanding;
	}



	public void setPrinOutstanding(float prinOutstanding) {
		this.prinOutstanding = prinOutstanding;
	}



	public float getIntLastDue() {
		return intLastDue;
	}



	public void setIntLastDue(float intLastDue) {
		this.intLastDue = intLastDue;
	}



	public float getMoratoriumInterest() {
		return moratoriumInterest;
	}



	public void setMoratoriumInterest(float moratoriumInterest) {
		this.moratoriumInterest = moratoriumInterest;
	}



	public float getOdc() {
		return odc;
	}



	public void setOdc(float odc) {
		this.odc = odc;
	}



	public float getChargesDue() {
		return chargesDue;
	}



	public void setChargesDue(float chargesDue) {
		this.chargesDue = chargesDue;
	}



	public float getFclOneDayInterest() {
		return fclOneDayInterest;
	}



	public void setFclOneDayInterest(float fclOneDayInterest) {
		this.fclOneDayInterest = fclOneDayInterest;
	}



	public float getFclCharges() {
		return fclCharges;
	}



	public void setFclCharges(float fclCharges) {
		this.fclCharges = fclCharges;
	}



	public float getFclAmount() {
		return fclAmount;
	}



	public void setFclAmount(float fclAmount) {
		this.fclAmount = fclAmount;
	}



	public String getStatusDisposition() {
		return statusDisposition;
	}



	public void setStatusDisposition(String statusDisposition) {
		this.statusDisposition = statusDisposition;
	}



	public Date getClosedTime() {
		return closedTime;
	}



	public void setClosedTime(Date closedTime) {
		this.closedTime = closedTime;
	}



	public Date getCancelTime() {
		return cancelTime;
	}



	public void setCancelTime(Date cancelTime) {
		this.cancelTime = cancelTime;
	}



	public String getCancelUser() {
		return cancelUser;
	}



	public void setCancelUser(String cancelUser) {
		this.cancelUser = cancelUser;
	}
	


	public Asset getAsset() {
		return asset;
	}



	public void setAsset(Asset asset) {
		this.asset = asset;
	}



	public Set<Disbursement> getDisbursement() {
		return disbursement;
	}



	public void setDisbursement(Set<Disbursement> disbursement) {
		this.disbursement = disbursement;
	}



	public Customer getCustomer() {
		return customer;
	}



	public void setCustomer(Customer customer) {
		this.customer = customer;
	}



	public Customer getGurantor() {
		return gurantor;
	}



	public void setGurantor(Customer gurantor) {
		this.gurantor = gurantor;
	}



	public Set<Receivable> getReceivable() {
		return receivable;
	}



	public void setReceivable(Set<Receivable> receivable) {
		this.receivable = receivable;
	}



	public Set<Transaction> getTransaction() {
		return transaction;
	}



	public void setTransaction(Set<Transaction> transaction) {
		this.transaction = transaction;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + advanceEmi;
		result = prime * result + ((agmntDate == null) ? 0 : agmntDate.hashCode());
		result = prime * result + Float.floatToIntBits(amount);
		result = prime * result + ((applNo == null) ? 0 : applNo.hashCode());
		result = prime * result + Float.floatToIntBits(arrears);
		result = prime * result + ((asset == null) ? 0 : asset.hashCode());
		result = prime * result + Float.floatToIntBits(bc);
		result = prime * result + bcCount;
		result = prime * result + Float.floatToIntBits(bcPaid);
		result = prime * result + Float.floatToIntBits(bcTax);
		result = prime * result + Float.floatToIntBits(bcTaxPaid);
		result = prime * result + ((book == null) ? 0 : book.hashCode());
		result = prime * result + branchCode;
		result = prime * result + ((brnetClientId == null) ? 0 : brnetClientId.hashCode());
		result = prime * result + ((cancelReason == null) ? 0 : cancelReason.hashCode());
		result = prime * result + ((cancelTime == null) ? 0 : cancelTime.hashCode());
		result = prime * result + ((cancelUser == null) ? 0 : cancelUser.hashCode());
		result = prime * result + Float.floatToIntBits(cashCollCharges);
		result = prime * result + Float.floatToIntBits(ccTail);
		result = prime * result + Float.floatToIntBits(ccTailBase);
		result = prime * result + Float.floatToIntBits(ccTailTax);
		result = prime * result + Float.floatToIntBits(cccBase);
		result = prime * result + Float.floatToIntBits(cccTax);
		result = prime * result + Float.floatToIntBits(chargesDue);
		result = prime * result + ((closedTime == null) ? 0 : closedTime.hashCode());
		result = prime * result + Float.floatToIntBits(coLendPercent);
		result = prime * result + ((coLender == null) ? 0 : coLender.hashCode());
		result = prime * result + ((coLenderDisposition == null) ? 0 : coLenderDisposition.hashCode());
		result = prime * result + Float.floatToIntBits(coLenderRoi);
		result = prime * result + ((coLenderStatus == null) ? 0 : coLenderStatus.hashCode());
		result = prime * result + Float.floatToIntBits(collected);
		result = prime * result + ((createUser == null) ? 0 : createUser.hashCode());
		result = prime * result + ((customer == null) ? 0 : customer.hashCode());
		result = prime * result + ((dealerCode == null) ? 0 : dealerCode.hashCode());
		result = prime * result + Float.floatToIntBits(dealerDisbursementAmount);
		result = prime * result + ((dealerName == null) ? 0 : dealerName.hashCode());
		result = prime * result + ((disbursementTime == null) ? 0 : disbursementTime.hashCode());
		result = prime * result + Float.floatToIntBits(downPayment);
		result = prime * result + Float.floatToIntBits(emi);
		result = prime * result + Float.floatToIntBits(excessPaid);
		result = prime * result + Float.floatToIntBits(expense);
		result = prime * result + Float.floatToIntBits(fclAmount);
		result = prime * result + Float.floatToIntBits(fclCharges);
		result = prime * result + Float.floatToIntBits(fclOneDayInterest);
		result = prime * result + ((firstDueDate == null) ? 0 : firstDueDate.hashCode());
		result = prime * result + Float.floatToIntBits(futurePrinciple);
		result = prime * result + glEntry;
		result = prime * result + ((gurantor == null) ? 0 : gurantor.hashCode());
		result = prime * result + ((h1 == null) ? 0 : h1.hashCode());
		result = prime * result + ((h2 == null) ? 0 : h2.hashCode());
		result = prime * result + ((h3 == null) ? 0 : h3.hashCode());
		result = prime * result + ((h4 == null) ? 0 : h4.hashCode());
		result = prime * result + ((h5 == null) ? 0 : h5.hashCode());
		result = prime * result + ((hypothecation == null) ? 0 : hypothecation.hashCode());
		result = prime * result + ((hypothecationHistory == null) ? 0 : hypothecationHistory.hashCode());
		result = prime * result + id;
		result = prime * result + Float.floatToIntBits(insBase);
		result = prime * result + Float.floatToIntBits(insTax);
		result = prime * result + Float.floatToIntBits(insuranceAmount);
		result = prime * result + Float.floatToIntBits(intArrears);
		result = prime * result + Float.floatToIntBits(intLastDue);
		result = prime * result + Float.floatToIntBits(interestPaid);
		result = prime * result + Float.floatToIntBits(interestTot);
		result = prime * result + Float.floatToIntBits(irr);
		result = prime * result + ((lastDueDate == null) ? 0 : lastDueDate.hashCode());
		result = prime * result + ((loanAgreement == null) ? 0 : loanAgreement.hashCode());
		result = prime * result + ((loanNo == null) ? 0 : loanNo.hashCode());
		result = prime * result + ((mandateFile == null) ? 0 : mandateFile.hashCode());
		result = prime * result + ((mobile == null) ? 0 : mobile.hashCode());
		result = prime * result + ((modifyUser == null) ? 0 : modifyUser.hashCode());
		result = prime * result + Float.floatToIntBits(moratoriumInterest);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + Float.floatToIntBits(oc);
		result = prime * result + Float.floatToIntBits(odAmount);
		result = prime * result + odCount;
		result = prime * result + odDays;
		result = prime * result + odMonths;
		result = prime * result + Float.floatToIntBits(odc);
		result = prime * result + Float.floatToIntBits(odcPaid);
		result = prime * result + Float.floatToIntBits(overdueAmount);
		result = prime * result + Float.floatToIntBits(pfBase);
		result = prime * result + Float.floatToIntBits(pfTail);
		result = prime * result + Float.floatToIntBits(pfTailBase);
		result = prime * result + Float.floatToIntBits(pfTailTax);
		result = prime * result + Float.floatToIntBits(pfTax);
		result = prime * result + Float.floatToIntBits(prinArears);
		result = prime * result + Float.floatToIntBits(prinOutstanding);
		result = prime * result + Float.floatToIntBits(principlePaid);
		result = prime * result + Float.floatToIntBits(principleTot);
		result = prime * result + Float.floatToIntBits(procFee);
		result = prime * result + ((realAgmntDate == null) ? 0 : realAgmntDate.hashCode());
		result = prime * result + ((repayMode == null) ? 0 : repayMode.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((statusDisposition == null) ? 0 : statusDisposition.hashCode());
		result = prime * result + Float.floatToIntBits(tenure);
		result = prime * result + Float.floatToIntBits(toBeCollected);
		result = prime * result + Float.floatToIntBits(totOut);
		result = prime * result + Float.floatToIntBits(totalPaid);
		result = prime * result + ((umrn == null) ? 0 : umrn.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Loan other = (Loan) obj;
		if (advanceEmi != other.advanceEmi)
			return false;
		if (agmntDate == null) {
			if (other.agmntDate != null)
				return false;
		} else if (!agmntDate.equals(other.agmntDate))
			return false;
		if (Float.floatToIntBits(amount) != Float.floatToIntBits(other.amount))
			return false;
		if (applNo == null) {
			if (other.applNo != null)
				return false;
		} else if (!applNo.equals(other.applNo))
			return false;
		if (Float.floatToIntBits(arrears) != Float.floatToIntBits(other.arrears))
			return false;
		if (asset == null) {
			if (other.asset != null)
				return false;
		} else if (!asset.equals(other.asset))
			return false;
		if (Float.floatToIntBits(bc) != Float.floatToIntBits(other.bc))
			return false;
		if (bcCount != other.bcCount)
			return false;
		if (Float.floatToIntBits(bcPaid) != Float.floatToIntBits(other.bcPaid))
			return false;
		if (Float.floatToIntBits(bcTax) != Float.floatToIntBits(other.bcTax))
			return false;
		if (Float.floatToIntBits(bcTaxPaid) != Float.floatToIntBits(other.bcTaxPaid))
			return false;
		if (book == null) {
			if (other.book != null)
				return false;
		} else if (!book.equals(other.book))
			return false;
		if (branchCode != other.branchCode)
			return false;
		if (brnetClientId == null) {
			if (other.brnetClientId != null)
				return false;
		} else if (!brnetClientId.equals(other.brnetClientId))
			return false;
		if (cancelReason == null) {
			if (other.cancelReason != null)
				return false;
		} else if (!cancelReason.equals(other.cancelReason))
			return false;
		if (cancelTime == null) {
			if (other.cancelTime != null)
				return false;
		} else if (!cancelTime.equals(other.cancelTime))
			return false;
		if (cancelUser == null) {
			if (other.cancelUser != null)
				return false;
		} else if (!cancelUser.equals(other.cancelUser))
			return false;
		if (Float.floatToIntBits(cashCollCharges) != Float.floatToIntBits(other.cashCollCharges))
			return false;
		if (Float.floatToIntBits(ccTail) != Float.floatToIntBits(other.ccTail))
			return false;
		if (Float.floatToIntBits(ccTailBase) != Float.floatToIntBits(other.ccTailBase))
			return false;
		if (Float.floatToIntBits(ccTailTax) != Float.floatToIntBits(other.ccTailTax))
			return false;
		if (Float.floatToIntBits(cccBase) != Float.floatToIntBits(other.cccBase))
			return false;
		if (Float.floatToIntBits(cccTax) != Float.floatToIntBits(other.cccTax))
			return false;
		if (Float.floatToIntBits(chargesDue) != Float.floatToIntBits(other.chargesDue))
			return false;
		if (closedTime == null) {
			if (other.closedTime != null)
				return false;
		} else if (!closedTime.equals(other.closedTime))
			return false;
		if (Float.floatToIntBits(coLendPercent) != Float.floatToIntBits(other.coLendPercent))
			return false;
		if (coLender == null) {
			if (other.coLender != null)
				return false;
		} else if (!coLender.equals(other.coLender))
			return false;
		if (coLenderDisposition == null) {
			if (other.coLenderDisposition != null)
				return false;
		} else if (!coLenderDisposition.equals(other.coLenderDisposition))
			return false;
		if (Float.floatToIntBits(coLenderRoi) != Float.floatToIntBits(other.coLenderRoi))
			return false;
		if (coLenderStatus == null) {
			if (other.coLenderStatus != null)
				return false;
		} else if (!coLenderStatus.equals(other.coLenderStatus))
			return false;
		if (Float.floatToIntBits(collected) != Float.floatToIntBits(other.collected))
			return false;
		if (createUser == null) {
			if (other.createUser != null)
				return false;
		} else if (!createUser.equals(other.createUser))
			return false;
		if (customer == null) {
			if (other.customer != null)
				return false;
		} else if (!customer.equals(other.customer))
			return false;
		if (dealerCode == null) {
			if (other.dealerCode != null)
				return false;
		} else if (!dealerCode.equals(other.dealerCode))
			return false;
		if (Float.floatToIntBits(dealerDisbursementAmount) != Float.floatToIntBits(other.dealerDisbursementAmount))
			return false;
		if (dealerName == null) {
			if (other.dealerName != null)
				return false;
		} else if (!dealerName.equals(other.dealerName))
			return false;
		if (disbursement == null) {
			if (other.disbursement != null)
				return false;
		} else if (!disbursement.equals(other.disbursement))
			return false;
		if (disbursementTime == null) {
			if (other.disbursementTime != null)
				return false;
		} else if (!disbursementTime.equals(other.disbursementTime))
			return false;
		if (Float.floatToIntBits(downPayment) != Float.floatToIntBits(other.downPayment))
			return false;
		if (Float.floatToIntBits(emi) != Float.floatToIntBits(other.emi))
			return false;
		if (Float.floatToIntBits(excessPaid) != Float.floatToIntBits(other.excessPaid))
			return false;
		if (Float.floatToIntBits(expense) != Float.floatToIntBits(other.expense))
			return false;
		if (Float.floatToIntBits(fclAmount) != Float.floatToIntBits(other.fclAmount))
			return false;
		if (Float.floatToIntBits(fclCharges) != Float.floatToIntBits(other.fclCharges))
			return false;
		if (Float.floatToIntBits(fclOneDayInterest) != Float.floatToIntBits(other.fclOneDayInterest))
			return false;
		if (firstDueDate == null) {
			if (other.firstDueDate != null)
				return false;
		} else if (!firstDueDate.equals(other.firstDueDate))
			return false;
		if (Float.floatToIntBits(futurePrinciple) != Float.floatToIntBits(other.futurePrinciple))
			return false;
		if (glEntry != other.glEntry)
			return false;
		if (gurantor == null) {
			if (other.gurantor != null)
				return false;
		} else if (!gurantor.equals(other.gurantor))
			return false;
		if (h1 == null) {
			if (other.h1 != null)
				return false;
		} else if (!h1.equals(other.h1))
			return false;
		if (h2 == null) {
			if (other.h2 != null)
				return false;
		} else if (!h2.equals(other.h2))
			return false;
		if (h3 == null) {
			if (other.h3 != null)
				return false;
		} else if (!h3.equals(other.h3))
			return false;
		if (h4 == null) {
			if (other.h4 != null)
				return false;
		} else if (!h4.equals(other.h4))
			return false;
		if (h5 == null) {
			if (other.h5 != null)
				return false;
		} else if (!h5.equals(other.h5))
			return false;
		if (hypothecation == null) {
			if (other.hypothecation != null)
				return false;
		} else if (!hypothecation.equals(other.hypothecation))
			return false;
		if (hypothecationHistory == null) {
			if (other.hypothecationHistory != null)
				return false;
		} else if (!hypothecationHistory.equals(other.hypothecationHistory))
			return false;
		if (id != other.id)
			return false;
		if (Float.floatToIntBits(insBase) != Float.floatToIntBits(other.insBase))
			return false;
		if (Float.floatToIntBits(insTax) != Float.floatToIntBits(other.insTax))
			return false;
		if (Float.floatToIntBits(insuranceAmount) != Float.floatToIntBits(other.insuranceAmount))
			return false;
		if (Float.floatToIntBits(intArrears) != Float.floatToIntBits(other.intArrears))
			return false;
		if (Float.floatToIntBits(intLastDue) != Float.floatToIntBits(other.intLastDue))
			return false;
		if (Float.floatToIntBits(interestPaid) != Float.floatToIntBits(other.interestPaid))
			return false;
		if (Float.floatToIntBits(interestTot) != Float.floatToIntBits(other.interestTot))
			return false;
		if (Float.floatToIntBits(irr) != Float.floatToIntBits(other.irr))
			return false;
		if (lastDueDate == null) {
			if (other.lastDueDate != null)
				return false;
		} else if (!lastDueDate.equals(other.lastDueDate))
			return false;
		if (loanAgreement == null) {
			if (other.loanAgreement != null)
				return false;
		} else if (!loanAgreement.equals(other.loanAgreement))
			return false;
		if (loanNo == null) {
			if (other.loanNo != null)
				return false;
		} else if (!loanNo.equals(other.loanNo))
			return false;
		if (mandateFile == null) {
			if (other.mandateFile != null)
				return false;
		} else if (!mandateFile.equals(other.mandateFile))
			return false;
		if (mobile == null) {
			if (other.mobile != null)
				return false;
		} else if (!mobile.equals(other.mobile))
			return false;
		if (modifyUser == null) {
			if (other.modifyUser != null)
				return false;
		} else if (!modifyUser.equals(other.modifyUser))
			return false;
		if (Float.floatToIntBits(moratoriumInterest) != Float.floatToIntBits(other.moratoriumInterest))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Float.floatToIntBits(oc) != Float.floatToIntBits(other.oc))
			return false;
		if (Float.floatToIntBits(odAmount) != Float.floatToIntBits(other.odAmount))
			return false;
		if (odCount != other.odCount)
			return false;
		if (odDays != other.odDays)
			return false;
		if (odMonths != other.odMonths)
			return false;
		if (Float.floatToIntBits(odc) != Float.floatToIntBits(other.odc))
			return false;
		if (Float.floatToIntBits(odcPaid) != Float.floatToIntBits(other.odcPaid))
			return false;
		if (Float.floatToIntBits(overdueAmount) != Float.floatToIntBits(other.overdueAmount))
			return false;
		if (Float.floatToIntBits(pfBase) != Float.floatToIntBits(other.pfBase))
			return false;
		if (Float.floatToIntBits(pfTail) != Float.floatToIntBits(other.pfTail))
			return false;
		if (Float.floatToIntBits(pfTailBase) != Float.floatToIntBits(other.pfTailBase))
			return false;
		if (Float.floatToIntBits(pfTailTax) != Float.floatToIntBits(other.pfTailTax))
			return false;
		if (Float.floatToIntBits(pfTax) != Float.floatToIntBits(other.pfTax))
			return false;
		if (Float.floatToIntBits(prinArears) != Float.floatToIntBits(other.prinArears))
			return false;
		if (Float.floatToIntBits(prinOutstanding) != Float.floatToIntBits(other.prinOutstanding))
			return false;
		if (Float.floatToIntBits(principlePaid) != Float.floatToIntBits(other.principlePaid))
			return false;
		if (Float.floatToIntBits(principleTot) != Float.floatToIntBits(other.principleTot))
			return false;
		if (Float.floatToIntBits(procFee) != Float.floatToIntBits(other.procFee))
			return false;
		if (realAgmntDate == null) {
			if (other.realAgmntDate != null)
				return false;
		} else if (!realAgmntDate.equals(other.realAgmntDate))
			return false;
		if (receivable == null) {
			if (other.receivable != null)
				return false;
		} else if (!receivable.equals(other.receivable))
			return false;
		if (repayMode == null) {
			if (other.repayMode != null)
				return false;
		} else if (!repayMode.equals(other.repayMode))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (statusDisposition == null) {
			if (other.statusDisposition != null)
				return false;
		} else if (!statusDisposition.equals(other.statusDisposition))
			return false;
		if (Float.floatToIntBits(tenure) != Float.floatToIntBits(other.tenure))
			return false;
		if (Float.floatToIntBits(toBeCollected) != Float.floatToIntBits(other.toBeCollected))
			return false;
		if (Float.floatToIntBits(totOut) != Float.floatToIntBits(other.totOut))
			return false;
		if (Float.floatToIntBits(totalPaid) != Float.floatToIntBits(other.totalPaid))
			return false;
		if (transaction == null) {
			if (other.transaction != null)
				return false;
		} else if (!transaction.equals(other.transaction))
			return false;
		if (umrn == null) {
			if (other.umrn != null)
				return false;
		} else if (!umrn.equals(other.umrn))
			return false;
		return true;
	}


public float getScCharge()
{
	return scCharge;
}
	public void setScCharge(float scCharge) {
		// TODO Auto-generated method stub
		this.scCharge =scCharge;
	}
    
	
  public String getSecuritisation()
  {
	  return securitisation;
  }
  public void setSecuritisation(String securitisation)
  {
	  this.securitisation =securitisation;
  }
  
  public String getProduct() {
		return product;
	}

	public void setId(String product) {
		this.product = product;
	}


	
  public String getCkycNumber()
  {
	  return ckycNumber;
  }
  public void setCkycNumber(String ckycNumber)
  {
	  this.ckycNumber =ckycNumber;
  }
  
  public String getMoratReason()
  {
	  return moratReason;
  }
  public void setMoratReason(String moratReason)
  {
	  this.moratReason =moratReason;
  }
	
  
	
  public float getMoratInterest()
  {
	  return moratInterest;
  }
  public void setMoratInterest(float moratInterest)
  {
	  this.moratInterest =moratInterest;
  }
  
  
	
  public float getMoratTenure()
  {
	  return moratTenure;
  }
  public void setMoratTenure(float moratTenure)
  {
	  this.moratTenure =moratTenure;
  }
  
  
  public float getMoratoriumIntWaiver()
  {
	  return moratoriumIntWaiver;
  }
  public void setMoratoriumIntWaiver(float moratoriumIntWaiver)
  {
	  this.moratoriumIntWaiver =moratoriumIntWaiver;
  }
  
  
  public float getCovidPayInt()
  {
	  return covidPayInt;
  }
  public void setCovidPayInt(float covidPayInt)
  {
	  this.covidPayInt =covidPayInt;
  }
  
 public String getFlagSec()
  {
	  return flagSec;
  }
  public void setFlagSec(String flagSec)
  {
	  this.flagSec =flagSec;
  }
  
 public float getDocumentCharge()
  {
	  return documentCharge;
  }
  public void setDocumentCharge(float documentCharge)
  {
	  this.documentCharge =documentCharge;
  }
  
  public float getDocumentChargeBase()
  {
	  return documentChargeBase;
  }
  public void setDocumentChargeBase(float documentChargeBase)
  {
	  this.documentChargeBase =documentChargeBase;
  }
  
  public float getDocumentChargeTax()
  {
	  return documentChargeTax;
  }
  public void setDocumentChargeTax(float documentChargeTax)
  {
	  this.documentChargeTax =documentChargeTax;
  }
  
   
  
  public String getFclBlock()
  {
	  return fclBlock;
  }
  public void setFclBlock(String fclBlock)
  {
	  this.fclBlock =fclBlock;
  }
  
 /* public float getExGratiaCreditAmount()
  {
	  return ExGratiaCreditAmount;
  }
  public void setExGratiaCreditAmount(float ExGratiaCreditAmount)
  {
	  this.ExGratiaCreditAmount =ExGratiaCreditAmount;
  }
  
  public float getExGratiaCreditAmountExcess()
  {
	  return ExGratiaCreditAmountExcess;
  }
  public void setExGratiaCreditAmountExcess(float ExGratiaCreditAmountExcess)
  {
	  this.ExGratiaCreditAmountExcess =ExGratiaCreditAmountExcess;
  }*/
  
}
