package com.vg.lms.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="lms_odc_daily")
public class LmsOdcDaily implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 737815402257352060L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="loan_no", length=40)
    private String loanNo;
    @Column(name="od_amount", precision=10, scale=2)
    private float odAmount;
    @Column(name="odc", precision=10, scale=2)
    private float odc;
    @Column(name="type", length=25)
    private String type;  
    @Column(name="value_date")
    private Date valueDate;   
    @Column(name="create_time")
    private Date createTime; 
    @Column(name="created_by", length=24)
    private String createdBy;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoanNo() {
		return loanNo;
	}
	public void setLoanNo(String loanNo) {
		this.loanNo = loanNo;
	}
	public float getOdAmount() {
		return odAmount;
	}
	public void setOdAmount(float odAmount) {
		this.odAmount = odAmount;
	}
	public float getOdc() {
		return odc;
	}
	public void setOdc(float odc) {
		this.odc = odc;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getValueDate() {
		return valueDate;
	}
	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
    
    
    
}
