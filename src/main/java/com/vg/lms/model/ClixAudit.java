

package com.vg.lms.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity(name="lms_clix_audit")
public class ClixAudit implements Serializable {
   

    /**
	 * 
	 */
	private static final long serialVersionUID = 3202451980523999105L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="los_app_name", nullable=false, length=30)
    private String losAppName;
    @Column(name="loan_id", precision=10)
    private int loanId;
    @Column(name="loan_no", length=32)
    private String loanNo;
    @Column(name="cust_id", length=22)
    private String custId;
    @Column(name="app_id", length=22)
    private String appId;
    @Column(name="txn_id", length=32)
    private String txnId;
    @Column(name="state",length=10)
    private String state;
    @Column(length=10)
    private String status;
    @Column(length=100)
    private String message;
    @Transient
    @Column(name="create_time")
    private Date createTime;
    @Transient
    @Column(name="modify_time")
    private Date modifyTime;
    @Column(name="last_sync_time")
    private Date syncTime;

    /** Default constructor. */
    public ClixAudit() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for losAppName.
     *
     * @return the current value of losAppName
     */
    public String getLosAppName() {
        return losAppName;
    }

    /**
     * Setter method for losAppName.
     *
     * @param aLosAppName the new value for losAppName
     */
    public void setLosAppName(String aLosAppName) {
        losAppName = aLosAppName;
    }

    /**
     * Access method for custId.
     *
     * @return the current value of custId
     */
    public String getCustId() {
        return custId;
    }

    /**
     * Setter method for custId.
     *
     * @param aCustId the new value for custId
     */
    public void setCustId(String aCustId) {
        custId = aCustId;
    }

    /**
     * Access method for appId.
     *
     * @return the current value of appId
     */
    public String getAppId() {
        return appId;
    }

    /**
     * Setter method for appId.
     *
     * @param aAppId the new value for appId
     */
    public void setAppId(String aAppId) {
        appId = aAppId;
    }

    /**
     * Access method for txnId.
     *
     * @return the current value of txnId
     */
    public String getTxnId() {
        return txnId;
    }

    /**
     * Setter method for txnId.
     *
     * @param aTxnId the new value for txnId
     */
    public void setTxnId(String aTxnId) {
        txnId = aTxnId;
    }

    /**
     * Access method for status.
     *
     * @return the current value of status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Setter method for status.
     *
     * @param aStatus the new value for status
     */
    public void setStatus(String aStatus) {
        status = aStatus;
    }    

	public int getLoanId() {
		return loanId;
	}

	public void setLoanId(int loanId) {
		this.loanId = loanId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getLoanNo() {
		return loanNo;
	}

	public void setLoanNo(String loanNo) {
		this.loanNo = loanNo;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public Date getSyncTime() {
		return syncTime;
	}

	public void setSyncTime(Date syncTime) {
		this.syncTime = syncTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((appId == null) ? 0 : appId.hashCode());
		result = prime * result + ((createTime == null) ? 0 : createTime.hashCode());
		result = prime * result + ((custId == null) ? 0 : custId.hashCode());
		result = prime * result + id;
		result = prime * result + loanId;
		result = prime * result + ((loanNo == null) ? 0 : loanNo.hashCode());
		result = prime * result + ((losAppName == null) ? 0 : losAppName.hashCode());
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + ((modifyTime == null) ? 0 : modifyTime.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((syncTime == null) ? 0 : syncTime.hashCode());
		result = prime * result + ((txnId == null) ? 0 : txnId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClixAudit other = (ClixAudit) obj;
		if (appId == null) {
			if (other.appId != null)
				return false;
		} else if (!appId.equals(other.appId))
			return false;
		if (createTime == null) {
			if (other.createTime != null)
				return false;
		} else if (!createTime.equals(other.createTime))
			return false;
		if (custId == null) {
			if (other.custId != null)
				return false;
		} else if (!custId.equals(other.custId))
			return false;
		if (id != other.id)
			return false;
		if (loanId != other.loanId)
			return false;
		if (loanNo == null) {
			if (other.loanNo != null)
				return false;
		} else if (!loanNo.equals(other.loanNo))
			return false;
		if (losAppName == null) {
			if (other.losAppName != null)
				return false;
		} else if (!losAppName.equals(other.losAppName))
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (modifyTime == null) {
			if (other.modifyTime != null)
				return false;
		} else if (!modifyTime.equals(other.modifyTime))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (syncTime == null) {
			if (other.syncTime != null)
				return false;
		} else if (!syncTime.equals(other.syncTime))
			return false;
		if (txnId == null) {
			if (other.txnId != null)
				return false;
		} else if (!txnId.equals(other.txnId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ClixAudit [id=" + id + ", losAppName=" + losAppName + ", loanId=" + loanId + ", loanNo=" + loanNo
				+ ", custId=" + custId + ", appId=" + appId + ", txnId=" + txnId + ", state=" + state + ", status="
				+ status + ", message=" + message + ", createTime=" + createTime + ", modifyTime=" + modifyTime
				+ ", syncTime=" + syncTime + "]";
	}
	
}

