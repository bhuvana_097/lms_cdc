package com.vg.lms.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="lms_waiver_close")
public class LmsWaiverClose implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7309065292777966665L;

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
	
    @Column(name="loan_no", length=28)
    private String loanNo;
    
    @Column(name="create_user", length=40)
    private String createUser;
    
    @Column(name="create_time")
    private Date createTime; 
    
    @Column(name="prin_waived", precision=12, scale=2)
    private float prinWaived;
    
    @Column(name="int_waived", precision=12, scale=2)
    private float intWaived;
    
    @Column(name="odc_waived", precision=12, scale=2)
    private float odcWaived;
    
    @Column(name="charges_waived", precision=12, scale=2)
    private float chargesWaived;
    
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoanNo() {
		return loanNo;
	}
	public void setLoanNo(String loanNo) {
		this.loanNo = loanNo;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public float getPrinWaived() {
		return prinWaived;
	}
	public void setPrinWaived(float prinWaived) {
		this.prinWaived = prinWaived;
	}
	public float getIntWaived() {
		return intWaived;
	}
	public void setIntWaived(float intWaived) {
		this.intWaived = intWaived;
	}
	public float getOdcWaived() {
		return odcWaived;
	}
	public void setOdcWaived(float odcWaived) {
		this.odcWaived = odcWaived;
	}
	public float getChargesWaived() {
		return chargesWaived;
	}
	public void setChargesWaived(float chargesWaived) {
		this.chargesWaived = chargesWaived;
	}
	
    
    
    
}
