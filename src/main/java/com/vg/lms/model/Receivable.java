// Generated with g9.

package com.vg.lms.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity(name="lms_receivable")
public class Receivable implements Serializable, Comparable<Receivable>{    

    /**
	 * 
	 */
	private static final long serialVersionUID = 1643288569942724161L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="due_no", precision=10)
    private int dueNo;
    @Column(name="due_date")
    private Date dueDate;
    @Column(precision=10, scale=2)
    private float emi;
    @Column(precision=10, scale=2)
    private float principle;
    @Column(precision=10, scale=2)
    private float interest;
    @Column(name="principle_remaining", precision=10, scale=2)
    private float principleRemaining;
    @Column(precision=10)
    private int trycount;
    @Column(length=10)
    private String status;
    @Column(name="loan_no", length=10)
    private String loanNo;
    @Column(name="create_user", length=10)
    private String createUser;
   
    @Column(name="modify_user", length=10)
    private String modifyUser;
   
    @ManyToOne
    @JoinColumn(name="loan_id")
    private Loan loan;
    @ManyToOne
    @JoinColumn(name="cust_id")
    private Customer customer;
    @OneToMany(mappedBy="receivable")
    private Set<Transaction> transaction;
    
    
    @Column(name="txn_date")
    private Date txnDate;   
    @Column(name="value_date")
    private Date valueDate;
    
    
    @Column(name="prin_paid", precision=10, scale=2)
    private float prinPaid;
    @Column(name="int_paid", precision=10, scale=2)
    private float intPaid;

    @Column(name="revised_prin_remaining", precision=20, scale=2)
    private float revisedPrinRemaining;
    
    @Column(name="moratorium_interest", precision=20, scale=2)
    private float  moratInterest;
    
    /** Default constructor. */
    public Receivable() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for dueNo.
     *
     * @return the current value of dueNo
     */
    public int getDueNo() {
        return dueNo;
    }

    /**
     * Setter method for dueNo.
     *
     * @param aDueNo the new value for dueNo
     */
    public void setDueNo(int aDueNo) {
        dueNo = aDueNo;
    }

    /**
     * Access method for dueDate.
     *
     * @return the current value of dueDate
     */
    public Date getDueDate() {
        return dueDate;
    }

    /**
     * Setter method for dueDate.
     *
     * @param aDueDate the new value for dueDate
     */
    public void setDueDate(Date aDueDate) {
        dueDate = aDueDate;
    }

    /**
     * Access method for emi.
     *
     * @return the current value of emi
     */
    public float getEmi() {
        return emi;
    }

    /**
     * Setter method for emi.
     *
     * @param aEmi the new value for emi
     */
    public void setEmi(float aEmi) {
        emi = aEmi;
    }

    /**
     * Access method for principle.
     *
     * @return the current value of principle
     */
    public float getPrinciple() {
        return principle;
    }

    /**
     * Setter method for principle.
     *
     * @param aPrinciple the new value for principle
     */
    public void setPrinciple(float aPrinciple) {
        principle = aPrinciple;
    }

    /**
     * Access method for interest.
     *
     * @return the current value of interest
     */
    public float getInterest() {
        return interest;
    }

    /**
     * Setter method for interest.
     *
     * @param aInterest the new value for interest
     */
    public void setInterest(float aInterest) {
        interest = aInterest;
    }

    /**
     * Access method for principleRemaining.
     *
     * @return the current value of principleRemaining
     */
    public float getPrincipleRemaining() {
        return principleRemaining;
    }

    /**
     * Setter method for principleRemaining.
     *
     * @param aPrincipleRemaining the new value for principleRemaining
     */
    public void setPrincipleRemaining(float aPrincipleRemaining) {
        principleRemaining = aPrincipleRemaining;
    }

    /**
     * Access method for trycount.
     *
     * @return the current value of trycount
     */
    public int getTrycount() {
        return trycount;
    }

    /**
     * Setter method for trycount.
     *
     * @param aTrycount the new value for trycount
     */
    public void setTrycount(int aTrycount) {
        trycount = aTrycount;
    }

    /**
     * Access method for status.
     *
     * @return the current value of status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Setter method for status.
     *
     * @param aStatus the new value for status
     */
    public void setStatus(String aStatus) {
        status = aStatus;
    }

    /**
     * Access method for createUser.
     *
     * @return the current value of createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * Setter method for createUser.
     *
     * @param aCreateUser the new value for createUser
     */
    public void setCreateUser(String aCreateUser) {
        createUser = aCreateUser;
    }

    /**
     * Access method for modifyUser.
     *
     * @return the current value of modifyUser
     */
    public String getModifyUser() {
        return modifyUser;
    }

    /**
     * Setter method for modifyUser.
     *
     * @param aModifyUser the new value for modifyUser
     */
    public void setModifyUser(String aModifyUser) {
        modifyUser = aModifyUser;
    }

   /**
     * Access method for loan.
     *
     * @return the current value of loan
     */
    public Loan getLoan() {
        return loan;
    }

    /**
     * Setter method for loan.
     *
     * @param aLoan the new value for loan
     */
    public void setLoan(Loan aLoan) {
        loan = aLoan;
    }

    /**
     * Access method for customer.
     *
     * @return the current value of customer
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * Setter method for customer.
     *
     * @param aCustomer the new value for customer
     */
    public void setCustomer(Customer aCustomer) {
        customer = aCustomer;
    }

    /**
     * Access method for transaction.
     *
     * @return the current value of transaction
     */
    public Set<Transaction> getTransaction() {
        return transaction;
    }

    /**
     * Setter method for transaction.
     *
     * @param aTransaction the new value for transaction
     */
    public void setTransaction(Set<Transaction> aTransaction) {
        transaction = aTransaction;
    }

	public String getLoanNo() {
		return loanNo;
	}

	public void setLoanNo(String loanNo) {
		this.loanNo = loanNo;
	}

	
	public Date getTxnDate() {
		return txnDate;
	}

	public void setTxnDate(Date txnDate) {
		this.txnDate = txnDate;
	}

	public Date getValueDate() {
		return valueDate;
	}

	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}

	public float getPrinPaid() {
		return prinPaid;
	}

	public void setPrinPaid(float prinPaid) {
		this.prinPaid = prinPaid;
	}

	public float getIntPaid() {
		return intPaid;
	}

	public void setIntPaid(float intPaid) {
		this.intPaid = intPaid;
	}
	
	
	 public float getRevisedPrinRemaining() {
        return revisedPrinRemaining;
    }

   
    public void setRevisedPrinRemaining(float revisedPrincipleRemaining) {
    	revisedPrinRemaining = revisedPrincipleRemaining;
    }
    
    
    
    public float getMoratInterest() {
        return moratInterest;
    }

   
    public void setMoratInterest(float moratInterest) {
    	moratInterest = moratInterest;
    }
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((createUser == null) ? 0 : createUser.hashCode());
		result = prime * result + ((dueDate == null) ? 0 : dueDate.hashCode());
		result = prime * result + ((txnDate == null) ? 0 : txnDate.hashCode());
		result = prime * result + ((valueDate == null) ? 0 : valueDate.hashCode());
		result = prime * result + dueNo;
		result = prime * result + ((loanNo == null) ? 0 : loanNo.hashCode());
		result = prime * result + Float.floatToIntBits(emi);
		result = prime * result + Float.floatToIntBits(prinPaid);
		result = prime * result + Float.floatToIntBits(intPaid);
		result = prime * result + id;
		result = prime * result + Float.floatToIntBits(interest);
		result = prime * result + ((modifyUser == null) ? 0 : modifyUser.hashCode());
		result = prime * result + Float.floatToIntBits(principle);
		result = prime * result + Float.floatToIntBits(principleRemaining);
		result = prime * result + Float.floatToIntBits(revisedPrinRemaining);
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + trycount;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Receivable other = (Receivable) obj;
		if (createUser == null) {
			if (other.createUser != null)
				return false;
		} else if (!createUser.equals(other.createUser))
			return false;
		if (dueDate == null) {
			if (other.dueDate != null)
				return false;
		} else if (!dueDate.equals(other.dueDate))
			return false;
		
		if (txnDate == null) {
			if (other.txnDate != null)
				return false;
		} else if (!txnDate.equals(other.txnDate))
			return false;
		
		if (valueDate == null) {
			if (other.valueDate != null)
				return false;
		} else if (!valueDate.equals(other.valueDate))
			return false;
		
		if (dueNo != other.dueNo)
			return false;
		if (Float.floatToIntBits(emi) != Float.floatToIntBits(other.emi))
			return false;
		if (id != other.id)
			return false;
		if (Float.floatToIntBits(interest) != Float.floatToIntBits(other.interest))
			return false;
		if (modifyUser == null) {
			if (other.modifyUser != null)
				return false;
		} else if (!modifyUser.equals(other.modifyUser))
			return false;
		if (Float.floatToIntBits(principle) != Float.floatToIntBits(other.principle))
			return false;
		
		if (Float.floatToIntBits(prinPaid) != Float.floatToIntBits(other.prinPaid))
			return false;
		if (Float.floatToIntBits(intPaid) != Float.floatToIntBits(other.intPaid))
			return false;
		
		if (Float.floatToIntBits(principleRemaining) != Float.floatToIntBits(other.principleRemaining))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		
		if (loanNo == null) {
			if (other.loanNo != null)
				return false;
		} else if (!loanNo.equals(other.loanNo))
			return false;
		
		if (trycount != other.trycount)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Receivable [id=" + id + ", dueNo=" + dueNo + ", dueDate=" + dueDate + ", emi=" + emi + ", principle="
				+ principle + ", interest=" + interest + ", principleRemaining=" + principleRemaining + ", trycount="
				+ trycount + ", status=" + status + ", loanNo=" + loanNo + ", createUser=" + createUser
				+ ", modifyUser=" + modifyUser + ", txnDate=" + txnDate + ", valueDate=" + valueDate + ", prinPaid="
				+ prinPaid + ", intPaid=" + intPaid + "]";
	}

	@Override
	public int compareTo(Receivable compareObj) {
		return this.dueNo - compareObj.getDueNo();
	}

 

}
