// Generated with g9.

package com.vg.lms.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="hht")
public class Receipt implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -829903763419568603L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="book_no", length=10)
    private String bookNo;
    @Column(name="receipt_no", precision=19)
    private long receiptNo;
    @Column(name="loan_no", length=15)
    private String loanNo;
    @Column(name="agent_code", length=10)
    private String agentCode;
    @Column(name="agent_name", length=100)
    private String agentName;
    @Column(name="receipt_date")
    private Date receiptDate;
    @Column(name="sync_time")
    private Date syncTime;
    @Column(name="lms_absorb_time")
    private Date lmsAbsorbTime;
    @Column(precision=15, scale=2)
    private float arrears;
    @Column(precision=15, scale=2)
    private float bc;
    @Column(precision=15, scale=2)
    private float odc;
    @Column(precision=15, scale=2)
    private float exp;
    @Column(name="receipt_amount", precision=15, scale=2)
    private float receiptAmount;
    @Column(length=20)
    private String unit;
    @Column(length=30)
    private String state;
    @Column(length=1)
    private String mode;
    @Column(length=10)
    private String chqno;
    @Column(name="chq_date", length=15)
    private String chqDate;
    @Column(name="bank_branch", length=100)
    private String bankBranch;
    @Column(name="hht_to_uno_date", length=15)
    private String hhtToUnoDate;
    @Column(name="update_date")
    private Date updateDate;
    @Column(name="modify_date", length=15)
    private String modifyDate;
    @Column(name="Error_message", length=150)
    private String errorMessage;
    @Column(name="collection_flag", length=1)
    private String collectionFlag;
    @Column(name="cash_in_hand", length=1)
    private String cashInHand;
    @Column(name="remittance_no", length=50)
    private String remittanceNo;
    @Column(length=10)
    private String cancelled;
    @Column(name="cancel_reason", length=100)
    private String cancelReason;
    @Column(name="SMS_guid", length=100)
    private String smsGuid;
    @Column(name="SMS_Status", length=10)
    private String smsStatus;
    @Column(name="SMS_Reason", length=25)
    private String smsReason;
    @Column(name="new_mobile_no", length=20)
    private String newMobileNo;
    @Column(name="new_phone2", length=20)
    private String newPhone2;
    @Column(precision=15, scale=2)
    private float debitnote;
    @Column(length=25)
    private String product;
    @Column(length=45)
    private String bank;
    @Column(name="bank_remittance", length=1)
    private String bankRemittance;
    @Column(length=100)
    private String item;
    @Column(length=200)
    private String remarks;
    @Column(length=15)
    private String pancard;
    @Column(name="pancard_no", length=15)
    private String pancardNo;
    @Column(name="pay_type", length=25)
    private String payType;
    @Column(length=25)
    private String payable;
    @Column(length=300)
    private String referenceno;
    @Column(length=20)
    private String latitude;
    @Column(length=20)
    private String longitude;

    /** Default constructor. */
    public Receipt() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for bookNo.
     *
     * @return the current value of bookNo
     */
    public String getBookNo() {
        return bookNo;
    }

    /**
     * Setter method for bookNo.
     *
     * @param aBookNo the new value for bookNo
     */
    public void setBookNo(String aBookNo) {
        bookNo = aBookNo;
    }

    /**
     * Access method for receiptNo.
     *
     * @return the current value of receiptNo
     */
    public long getReceiptNo() {
        return receiptNo;
    }

    /**
     * Setter method for receiptNo.
     *
     * @param aReceiptNo the new value for receiptNo
     */
    public void setReceiptNo(long aReceiptNo) {
        receiptNo = aReceiptNo;
    }

    /**
     * Access method for loanNo.
     *
     * @return the current value of loanNo
     */
    public String getLoanNo() {
        return loanNo;
    }

    /**
     * Setter method for loanNo.
     *
     * @param aLoanNo the new value for loanNo
     */
    public void setLoanNo(String aLoanNo) {
        loanNo = aLoanNo;
    }

    /**
     * Access method for agentCode.
     *
     * @return the current value of agentCode
     */
    public String getAgentCode() {
        return agentCode;
    }

    /**
     * Setter method for agentCode.
     *
     * @param aAgentCode the new value for agentCode
     */
    public void setAgentCode(String aAgentCode) {
        agentCode = aAgentCode;
    }

    /**
     * Access method for agentName.
     *
     * @return the current value of agentName
     */
    public String getAgentName() {
        return agentName;
    }

    /**
     * Setter method for agentName.
     *
     * @param aAgentName the new value for agentName
     */
    public void setAgentName(String aAgentName) {
        agentName = aAgentName;
    }

    /**
     * Access method for receiptDate.
     *
     * @return the current value of receiptDate
     */
    public Date getReceiptDate() {
        return receiptDate;
    }

    /**
     * Setter method for receiptDate.
     *
     * @param aReceiptDate the new value for receiptDate
     */
    public void setReceiptDate(Date aReceiptDate) {
        receiptDate = aReceiptDate;
    }

    /**
     * Access method for syncTime.
     *
     * @return the current value of syncTime
     */
    public Date getSyncTime() {
        return syncTime;
    }

    /**
     * Setter method for syncTime.
     *
     * @param aSyncTime the new value for syncTime
     */
    public void setSyncTime(Date aSyncTime) {
        syncTime = aSyncTime;
    }

    /**
     * Access method for arrears.
     *
     * @return the current value of arrears
     */
    public float getArrears() {
        return arrears;
    }

    /**
     * Setter method for arrears.
     *
     * @param aArrears the new value for arrears
     */
    public void setArrears(float aArrears) {
        arrears = aArrears;
    }

    /**
     * Access method for bc.
     *
     * @return the current value of bc
     */
    public float getBc() {
        return bc;
    }

    /**
     * Setter method for bc.
     *
     * @param aBc the new value for bc
     */
    public void setBc(float aBc) {
        bc = aBc;
    }

    /**
     * Access method for odc.
     *
     * @return the current value of odc
     */
    public float getOdc() {
        return odc;
    }

    /**
     * Setter method for odc.
     *
     * @param aOdc the new value for odc
     */
    public void setOdc(float aOdc) {
        odc = aOdc;
    }

    /**
     * Access method for exp.
     *
     * @return the current value of exp
     */
    public float getExp() {
        return exp;
    }

    /**
     * Setter method for exp.
     *
     * @param aExp the new value for exp
     */
    public void setExp(float aExp) {
        exp = aExp;
    }

    /**
     * Access method for receiptAmount.
     *
     * @return the current value of receiptAmount
     */
    public float getReceiptAmount() {
        return receiptAmount;
    }

    /**
     * Setter method for receiptAmount.
     *
     * @param aReceiptAmount the new value for receiptAmount
     */
    public void setReceiptAmount(float aReceiptAmount) {
        receiptAmount = aReceiptAmount;
    }

    /**
     * Access method for unit.
     *
     * @return the current value of unit
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Setter method for unit.
     *
     * @param aUnit the new value for unit
     */
    public void setUnit(String aUnit) {
        unit = aUnit;
    }

    /**
     * Access method for state.
     *
     * @return the current value of state
     */
    public String getState() {
        return state;
    }

    /**
     * Setter method for state.
     *
     * @param aState the new value for state
     */
    public void setState(String aState) {
        state = aState;
    }

    /**
     * Access method for mode.
     *
     * @return the current value of mode
     */
    public String getMode() {
        return mode;
    }

    /**
     * Setter method for mode.
     *
     * @param aMode the new value for mode
     */
    public void setMode(String aMode) {
        mode = aMode;
    }

    /**
     * Access method for chqno.
     *
     * @return the current value of chqno
     */
    public String getChqno() {
        return chqno;
    }

    /**
     * Setter method for chqno.
     *
     * @param aChqno the new value for chqno
     */
    public void setChqno(String aChqno) {
        chqno = aChqno;
    }

    /**
     * Access method for chqDate.
     *
     * @return the current value of chqDate
     */
    public String getChqDate() {
        return chqDate;
    }

    /**
     * Setter method for chqDate.
     *
     * @param aChqDate the new value for chqDate
     */
    public void setChqDate(String aChqDate) {
        chqDate = aChqDate;
    }

    /**
     * Access method for bankBranch.
     *
     * @return the current value of bankBranch
     */
    public String getBankBranch() {
        return bankBranch;
    }

    /**
     * Setter method for bankBranch.
     *
     * @param aBankBranch the new value for bankBranch
     */
    public void setBankBranch(String aBankBranch) {
        bankBranch = aBankBranch;
    }

    /**
     * Access method for hhtToUnoDate.
     *
     * @return the current value of hhtToUnoDate
     */
    public String getHhtToUnoDate() {
        return hhtToUnoDate;
    }

    /**
     * Setter method for hhtToUnoDate.
     *
     * @param aHhtToUnoDate the new value for hhtToUnoDate
     */
    public void setHhtToUnoDate(String aHhtToUnoDate) {
        hhtToUnoDate = aHhtToUnoDate;
    }

    /**
     * Access method for updateDate.
     *
     * @return the current value of updateDate
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * Setter method for updateDate.
     *
     * @param aUpdateDate the new value for updateDate
     */
    public void setUpdateDate(Date aUpdateDate) {
        updateDate = aUpdateDate;
    }

    /**
     * Access method for modifyDate.
     *
     * @return the current value of modifyDate
     */
    public String getModifyDate() {
        return modifyDate;
    }

    /**
     * Setter method for modifyDate.
     *
     * @param aModifyDate the new value for modifyDate
     */
    public void setModifyDate(String aModifyDate) {
        modifyDate = aModifyDate;
    }

    
    /**
     * Access method for errorMessage.
     *
     * @return the current value of errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Setter method for errorMessage.
     *
     * @param aErrorMessage the new value for errorMessage
     */
    public void setErrorMessage(String aErrorMessage) {
        errorMessage = aErrorMessage;
    }

    /**
     * Access method for collectionFlag.
     *
     * @return the current value of collectionFlag
     */
    public String getCollectionFlag() {
        return collectionFlag;
    }

    /**
     * Setter method for collectionFlag.
     *
     * @param aCollectionFlag the new value for collectionFlag
     */
    public void setCollectionFlag(String aCollectionFlag) {
        collectionFlag = aCollectionFlag;
    }

    /**
     * Access method for cashInHand.
     *
     * @return the current value of cashInHand
     */
    public String getCashInHand() {
        return cashInHand;
    }

    /**
     * Setter method for cashInHand.
     *
     * @param aCashInHand the new value for cashInHand
     */
    public void setCashInHand(String aCashInHand) {
        cashInHand = aCashInHand;
    }

    /**
     * Access method for remittanceNo.
     *
     * @return the current value of remittanceNo
     */
    public String getRemittanceNo() {
        return remittanceNo;
    }

    /**
     * Setter method for remittanceNo.
     *
     * @param aRemittanceNo the new value for remittanceNo
     */
    public void setRemittanceNo(String aRemittanceNo) {
        remittanceNo = aRemittanceNo;
    }

    /**
     * Access method for cancelled.
     *
     * @return the current value of cancelled
     */
    public String getCancelled() {
        return cancelled;
    }

    /**
     * Setter method for cancelled.
     *
     * @param aCancelled the new value for cancelled
     */
    public void setCancelled(String aCancelled) {
        cancelled = aCancelled;
    }

    /**
     * Access method for cancelReason.
     *
     * @return the current value of cancelReason
     */
    public String getCancelReason() {
        return cancelReason;
    }

    /**
     * Setter method for cancelReason.
     *
     * @param aCancelReason the new value for cancelReason
     */
    public void setCancelReason(String aCancelReason) {
        cancelReason = aCancelReason;
    }

    /**
     * Access method for smsGuid.
     *
     * @return the current value of smsGuid
     */
    public String getSmsGuid() {
        return smsGuid;
    }

    /**
     * Setter method for smsGuid.
     *
     * @param aSmsGuid the new value for smsGuid
     */
    public void setSmsGuid(String aSmsGuid) {
        smsGuid = aSmsGuid;
    }

    /**
     * Access method for smsStatus.
     *
     * @return the current value of smsStatus
     */
    public String getSmsStatus() {
        return smsStatus;
    }

    /**
     * Setter method for smsStatus.
     *
     * @param aSmsStatus the new value for smsStatus
     */
    public void setSmsStatus(String aSmsStatus) {
        smsStatus = aSmsStatus;
    }

    /**
     * Access method for smsReason.
     *
     * @return the current value of smsReason
     */
    public String getSmsReason() {
        return smsReason;
    }

    /**
     * Setter method for smsReason.
     *
     * @param aSmsReason the new value for smsReason
     */
    public void setSmsReason(String aSmsReason) {
        smsReason = aSmsReason;
    }

    /**
     * Access method for newMobileNo.
     *
     * @return the current value of newMobileNo
     */
    public String getNewMobileNo() {
        return newMobileNo;
    }

    /**
     * Setter method for newMobileNo.
     *
     * @param aNewMobileNo the new value for newMobileNo
     */
    public void setNewMobileNo(String aNewMobileNo) {
        newMobileNo = aNewMobileNo;
    }

    /**
     * Access method for newPhone2.
     *
     * @return the current value of newPhone2
     */
    public String getNewPhone2() {
        return newPhone2;
    }

    /**
     * Setter method for newPhone2.
     *
     * @param aNewPhone2 the new value for newPhone2
     */
    public void setNewPhone2(String aNewPhone2) {
        newPhone2 = aNewPhone2;
    }

    /**
     * Access method for debitnote.
     *
     * @return the current value of debitnote
     */
    public float getDebitnote() {
        return debitnote;
    }

    /**
     * Setter method for debitnote.
     *
     * @param aDebitnote the new value for debitnote
     */
    public void setDebitnote(float aDebitnote) {
        debitnote = aDebitnote;
    }

    /**
     * Access method for product.
     *
     * @return the current value of product
     */
    public String getProduct() {
        return product;
    }

    /**
     * Setter method for product.
     *
     * @param aProduct the new value for product
     */
    public void setProduct(String aProduct) {
        product = aProduct;
    }

    /**
     * Access method for bank.
     *
     * @return the current value of bank
     */
    public String getBank() {
        return bank;
    }

    /**
     * Setter method for bank.
     *
     * @param aBank the new value for bank
     */
    public void setBank(String aBank) {
        bank = aBank;
    }

    /**
     * Access method for bankRemittance.
     *
     * @return the current value of bankRemittance
     */
    public String getBankRemittance() {
        return bankRemittance;
    }

    /**
     * Setter method for bankRemittance.
     *
     * @param aBankRemittance the new value for bankRemittance
     */
    public void setBankRemittance(String aBankRemittance) {
        bankRemittance = aBankRemittance;
    }

    /**
     * Access method for item.
     *
     * @return the current value of item
     */
    public String getItem() {
        return item;
    }

    /**
     * Setter method for item.
     *
     * @param aItem the new value for item
     */
    public void setItem(String aItem) {
        item = aItem;
    }

    /**
     * Access method for remarks.
     *
     * @return the current value of remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * Setter method for remarks.
     *
     * @param aRemarks the new value for remarks
     */
    public void setRemarks(String aRemarks) {
        remarks = aRemarks;
    }

    /**
     * Access method for pancard.
     *
     * @return the current value of pancard
     */
    public String getPancard() {
        return pancard;
    }

    /**
     * Setter method for pancard.
     *
     * @param aPancard the new value for pancard
     */
    public void setPancard(String aPancard) {
        pancard = aPancard;
    }

    /**
     * Access method for pancardNo.
     *
     * @return the current value of pancardNo
     */
    public String getPancardNo() {
        return pancardNo;
    }

    /**
     * Setter method for pancardNo.
     *
     * @param aPancardNo the new value for pancardNo
     */
    public void setPancardNo(String aPancardNo) {
        pancardNo = aPancardNo;
    }

    /**
     * Access method for payType.
     *
     * @return the current value of payType
     */
    public String getPayType() {
        return payType;
    }

    /**
     * Setter method for payType.
     *
     * @param aPayType the new value for payType
     */
    public void setPayType(String aPayType) {
        payType = aPayType;
    }

    /**
     * Access method for payable.
     *
     * @return the current value of payable
     */
    public String getPayable() {
        return payable;
    }

    /**
     * Setter method for payable.
     *
     * @param aPayable the new value for payable
     */
    public void setPayable(String aPayable) {
        payable = aPayable;
    }

    /**
     * Access method for referenceno.
     *
     * @return the current value of referenceno
     */
    public String getReferenceno() {
        return referenceno;
    }

    /**
     * Setter method for referenceno.
     *
     * @param aReferenceno the new value for referenceno
     */
    public void setReferenceno(String aReferenceno) {
        referenceno = aReferenceno;
    }

    /**
     * Access method for latitude.
     *
     * @return the current value of latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * Setter method for latitude.
     *
     * @param aLatitude the new value for latitude
     */
    public void setLatitude(String aLatitude) {
        latitude = aLatitude;
    }

    /**
     * Access method for longitude.
     *
     * @return the current value of longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * Setter method for longitude.
     *
     * @param aLongitude the new value for longitude
     */
    public void setLongitude(String aLongitude) {
        longitude = aLongitude;
    }

	public Date getLmsAbsorbTime() {
		return lmsAbsorbTime;
	}

	public void setLmsAbsorbTime(Date lmsAbsorbTime) {
		this.lmsAbsorbTime = lmsAbsorbTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((agentCode == null) ? 0 : agentCode.hashCode());
		result = prime * result + ((agentName == null) ? 0 : agentName.hashCode());
		result = prime * result + Float.floatToIntBits(arrears);
		result = prime * result + ((bank == null) ? 0 : bank.hashCode());
		result = prime * result + ((bankBranch == null) ? 0 : bankBranch.hashCode());
		result = prime * result + ((bankRemittance == null) ? 0 : bankRemittance.hashCode());
		result = prime * result + Float.floatToIntBits(bc);
		result = prime * result + ((bookNo == null) ? 0 : bookNo.hashCode());
		result = prime * result + ((cancelReason == null) ? 0 : cancelReason.hashCode());
		result = prime * result + ((cancelled == null) ? 0 : cancelled.hashCode());
		result = prime * result + ((cashInHand == null) ? 0 : cashInHand.hashCode());
		result = prime * result + ((chqDate == null) ? 0 : chqDate.hashCode());
		result = prime * result + ((chqno == null) ? 0 : chqno.hashCode());
		result = prime * result + ((collectionFlag == null) ? 0 : collectionFlag.hashCode());
		result = prime * result + Float.floatToIntBits(debitnote);
		result = prime * result + ((errorMessage == null) ? 0 : errorMessage.hashCode());
		result = prime * result + Float.floatToIntBits(exp);
		result = prime * result + ((hhtToUnoDate == null) ? 0 : hhtToUnoDate.hashCode());
		result = prime * result + id;
		result = prime * result + ((item == null) ? 0 : item.hashCode());
		result = prime * result + ((latitude == null) ? 0 : latitude.hashCode());
		result = prime * result + ((loanNo == null) ? 0 : loanNo.hashCode());
		result = prime * result + ((longitude == null) ? 0 : longitude.hashCode());
		result = prime * result + ((mode == null) ? 0 : mode.hashCode());
		result = prime * result + ((modifyDate == null) ? 0 : modifyDate.hashCode());
		result = prime * result + ((newMobileNo == null) ? 0 : newMobileNo.hashCode());
		result = prime * result + ((newPhone2 == null) ? 0 : newPhone2.hashCode());
		result = prime * result + Float.floatToIntBits(odc);
		result = prime * result + ((pancard == null) ? 0 : pancard.hashCode());
		result = prime * result + ((pancardNo == null) ? 0 : pancardNo.hashCode());
		result = prime * result + ((payType == null) ? 0 : payType.hashCode());
		result = prime * result + ((payable == null) ? 0 : payable.hashCode());
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		result = prime * result + Float.floatToIntBits(receiptAmount);
		result = prime * result + ((receiptDate == null) ? 0 : receiptDate.hashCode());
		result = prime * result + (int) (receiptNo ^ (receiptNo >>> 32));
		result = prime * result + ((referenceno == null) ? 0 : referenceno.hashCode());
		result = prime * result + ((remarks == null) ? 0 : remarks.hashCode());
		result = prime * result + ((remittanceNo == null) ? 0 : remittanceNo.hashCode());
		result = prime * result + ((smsGuid == null) ? 0 : smsGuid.hashCode());
		result = prime * result + ((smsReason == null) ? 0 : smsReason.hashCode());
		result = prime * result + ((smsStatus == null) ? 0 : smsStatus.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((syncTime == null) ? 0 : syncTime.hashCode());
		result = prime * result + ((lmsAbsorbTime == null) ? 0 : lmsAbsorbTime.hashCode());
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		result = prime * result + ((updateDate == null) ? 0 : updateDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Receipt other = (Receipt) obj;
		if (agentCode == null) {
			if (other.agentCode != null)
				return false;
		} else if (!agentCode.equals(other.agentCode))
			return false;
		if (agentName == null) {
			if (other.agentName != null)
				return false;
		} else if (!agentName.equals(other.agentName))
			return false;
		if (Float.floatToIntBits(arrears) != Float.floatToIntBits(other.arrears))
			return false;
		if (bank == null) {
			if (other.bank != null)
				return false;
		} else if (!bank.equals(other.bank))
			return false;
		if (bankBranch == null) {
			if (other.bankBranch != null)
				return false;
		} else if (!bankBranch.equals(other.bankBranch))
			return false;
		if (bankRemittance == null) {
			if (other.bankRemittance != null)
				return false;
		} else if (!bankRemittance.equals(other.bankRemittance))
			return false;		
		if (Float.floatToIntBits(bc) != Float.floatToIntBits(other.bc))
			return false;
		if (bookNo == null) {
			if (other.bookNo != null)
				return false;
		} else if (!bookNo.equals(other.bookNo))
			return false;
		if (cancelReason == null) {
			if (other.cancelReason != null)
				return false;
		} else if (!cancelReason.equals(other.cancelReason))
			return false;
		if (cancelled == null) {
			if (other.cancelled != null)
				return false;
		} else if (!cancelled.equals(other.cancelled))
			return false;
		if (cashInHand == null) {
			if (other.cashInHand != null)
				return false;
		} else if (!cashInHand.equals(other.cashInHand))
			return false;
		if (chqDate == null) {
			if (other.chqDate != null)
				return false;
		} else if (!chqDate.equals(other.chqDate))
			return false;
		if (chqno == null) {
			if (other.chqno != null)
				return false;
		} else if (!chqno.equals(other.chqno))
			return false;
		if (collectionFlag == null) {
			if (other.collectionFlag != null)
				return false;
		} else if (!collectionFlag.equals(other.collectionFlag))
			return false;
		if (Float.floatToIntBits(debitnote) != Float.floatToIntBits(other.debitnote))
			return false;
		if (errorMessage == null) {
			if (other.errorMessage != null)
				return false;
		} else if (!errorMessage.equals(other.errorMessage))
			return false;
		if (Float.floatToIntBits(exp) != Float.floatToIntBits(other.exp))
			return false;
		if (hhtToUnoDate == null) {
			if (other.hhtToUnoDate != null)
				return false;
		} else if (!hhtToUnoDate.equals(other.hhtToUnoDate))
			return false;
		if (id != other.id)
			return false;
		if (item == null) {
			if (other.item != null)
				return false;
		} else if (!item.equals(other.item))
			return false;
		if (latitude == null) {
			if (other.latitude != null)
				return false;
		} else if (!latitude.equals(other.latitude))
			return false;
		if (loanNo == null) {
			if (other.loanNo != null)
				return false;
		} else if (!loanNo.equals(other.loanNo))
			return false;
		if (longitude == null) {
			if (other.longitude != null)
				return false;
		} else if (!longitude.equals(other.longitude))
			return false;
		if (mode == null) {
			if (other.mode != null)
				return false;
		} else if (!mode.equals(other.mode))
			return false;
		if (modifyDate == null) {
			if (other.modifyDate != null)
				return false;
		} else if (!modifyDate.equals(other.modifyDate))
			return false;
		if (newMobileNo == null) {
			if (other.newMobileNo != null)
				return false;
		} else if (!newMobileNo.equals(other.newMobileNo))
			return false;
		if (newPhone2 == null) {
			if (other.newPhone2 != null)
				return false;
		} else if (!newPhone2.equals(other.newPhone2))
			return false;
		if (Float.floatToIntBits(odc) != Float.floatToIntBits(other.odc))
			return false;
		if (pancard == null) {
			if (other.pancard != null)
				return false;
		} else if (!pancard.equals(other.pancard))
			return false;
		if (pancardNo == null) {
			if (other.pancardNo != null)
				return false;
		} else if (!pancardNo.equals(other.pancardNo))
			return false;
		if (payType == null) {
			if (other.payType != null)
				return false;
		} else if (!payType.equals(other.payType))
			return false;
		if (payable == null) {
			if (other.payable != null)
				return false;
		} else if (!payable.equals(other.payable))
			return false;
		if (product == null) {
			if (other.product != null)
				return false;
		} else if (!product.equals(other.product))
			return false;
		if (Float.floatToIntBits(receiptAmount) != Float.floatToIntBits(other.receiptAmount))
			return false;
		if (receiptDate == null) {
			if (other.receiptDate != null)
				return false;
		} else if (!receiptDate.equals(other.receiptDate))
			return false;
		if (receiptNo != other.receiptNo)
			return false;
		if (referenceno == null) {
			if (other.referenceno != null)
				return false;
		} else if (!referenceno.equals(other.referenceno))
			return false;
		if (remarks == null) {
			if (other.remarks != null)
				return false;
		} else if (!remarks.equals(other.remarks))
			return false;
		if (remittanceNo == null) {
			if (other.remittanceNo != null)
				return false;
		} else if (!remittanceNo.equals(other.remittanceNo))
			return false;
		if (smsGuid == null) {
			if (other.smsGuid != null)
				return false;
		} else if (!smsGuid.equals(other.smsGuid))
			return false;
		if (smsReason == null) {
			if (other.smsReason != null)
				return false;
		} else if (!smsReason.equals(other.smsReason))
			return false;
		if (smsStatus == null) {
			if (other.smsStatus != null)
				return false;
		} else if (!smsStatus.equals(other.smsStatus))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (syncTime == null) {
			if (other.syncTime != null)
				return false;
		} else if (!syncTime.equals(other.syncTime))
			return false;
		if (lmsAbsorbTime == null) {
			if (other.lmsAbsorbTime != null)
				return false;
		} else if (!lmsAbsorbTime.equals(other.lmsAbsorbTime))
			return false;
		if (unit == null) {
			if (other.unit != null)
				return false;
		} else if (!unit.equals(other.unit))
			return false;
		if (updateDate == null) {
			if (other.updateDate != null)
				return false;
		} else if (!updateDate.equals(other.updateDate))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Receipt [id=" + id + ", bookNo=" + bookNo + ", receiptNo=" + receiptNo + ", loanNo=" + loanNo
				+ ", agentCode=" + agentCode + ", agentName=" + agentName + ", receiptDate=" + receiptDate
				+ ", syncTime=" + syncTime + ", lmsAbsorbTime=" + lmsAbsorbTime + ", arrears=" + arrears + ", bc=" + bc
				+ ", odc=" + odc + ", exp=" + exp + ", receiptAmount=" + receiptAmount + ", unit=" + unit + ", state="
				+ state + ", mode=" + mode + ", chqno=" + chqno + ", chqDate=" + chqDate + ", bankBranch=" + bankBranch
				+ ", hhtToUnoDate=" + hhtToUnoDate + ", updateDate=" + updateDate + ", modifyDate=" + modifyDate
				+ ", errorMessage=" + errorMessage + ", collectionFlag=" + collectionFlag + ", cashInHand=" + cashInHand
				+ ", remittanceNo=" + remittanceNo + ", cancelled=" + cancelled + ", cancelReason=" + cancelReason
				+ ", smsGuid=" + smsGuid + ", smsStatus=" + smsStatus + ", smsReason=" + smsReason + ", newMobileNo="
				+ newMobileNo + ", newPhone2=" + newPhone2 + ", debitnote=" + debitnote + ", product=" + product
				+ ", bank=" + bank + ", bankRemittance=" + bankRemittance + ", item=" + item + ", remarks=" + remarks
				+ ", pancard=" + pancard + ", pancardNo=" + pancardNo + ", payType=" + payType + ", payable=" + payable
				+ ", referenceno=" + referenceno + ", latitude=" + latitude + ", longitude=" + longitude + "]";
	}

	

  

}
