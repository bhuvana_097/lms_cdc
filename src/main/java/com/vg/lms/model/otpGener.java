package com.vg.lms.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity(name="OTP_Generation_master")
public class otpGener implements Serializable {
	
	
	
	   private static final long serialVersionUID = -8634290503878895664L;
	   @Id
	    @GeneratedValue(strategy=GenerationType.IDENTITY)
	    @Column(unique=true, nullable=false, precision=10)
	    private int id;
	   
	    @Column(name="generated_otp", length=11)
	    private int generatedOtp;
	   
	    @Column(name="otp_validated", length=2)
	    private String otpValidated;
	    @Column(name="employee_name", length=45)
	    private String employeeName;
	  
	    @Column(name="otp_created_time")
	    private Date otpCreatedTime;
	    @Column(name="otp_valid_date")
	    private Date otpValidDate;
	    @Column(name="otp_Validated_time")
	    private Date otpValidatedTime;
		
			 
		/** Default constructor. */
	    public otpGener() {
	        super();
	    }


		public int getId() {
			return id;
		}


		public void setId(int id) {
			this.id = id;
		}


		public int getGeneratedOtp() {
			return generatedOtp;
		}


		public void setGeneratedOtp(int generatedOtp) {
			this.generatedOtp = generatedOtp;
		}


		public String getOtpValidated() {
			return otpValidated;
		}


		public void setOtpValidated(String otpValidated) {
			this.otpValidated = otpValidated;
		}


		public String getEmployeeName() {
			return employeeName;
		}


		public void setEmployeeName(String employeeName) {
			this.employeeName = employeeName;
		}


		public Date getOtpCreatedTime() {
			return otpCreatedTime;
		}


		public void setOtpCreatedTime(Date otpCreatedTime) {
			this.otpCreatedTime = otpCreatedTime;
		}


		public Date getOtpValidDate() {
			return otpValidDate;
		}


		public void setOtpValidDate(Date otpValidDate) {
			this.otpValidDate = otpValidDate;
		}


		public Date getOtpValidatedTime() {
			return otpValidatedTime;
		}


		public void setOtpValidatedTime(Date otpValidatedTime) {
			this.otpValidatedTime = otpValidatedTime;
		}


		public static long getSerialversionuid() {
			return serialVersionUID;
		}

	    
	  
	}



