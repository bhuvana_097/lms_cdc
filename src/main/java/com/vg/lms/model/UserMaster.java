package com.vg.lms.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.DynamicUpdate;

@Entity(name="lms_user_master")
@DynamicUpdate
public class UserMaster implements Serializable {
	
	private static final long serialVersionUID = -8634290503878895664L;
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
   
    @Column(name="code", length=15)
    private String code;
    @Column(name="name", length=30)
    private String name;
    @Column(name="passwd", length=35)
    private String passwd;
    @Column(name="dept", length=30)
    private String dept;
    @Column(name="hier_level", length=15)
    private String hierLevel;
    @Column(name="status", length=15)
    private String status;
    @Column(name="role", length=15)
    private String role;
    @Column(name="create_user", length=50)
    private String create_user;    
    @Column(name="create_time")
    private Date createTime;
    @Column(name="modify_user", length=50)
    private String modifyUser;    
    @Column(name="modify_time")
    private Date modifyTime;
    
    @Column(name="reason", length=50)
    private String reason; 
    
    @Column(name="branch", length=50)
    private String branch; 
   /* @Column(name="sessionid", length=100)
    private String sessionid;    
    
    @Column(name="last_login")
    private Date lastLogin;
	*/
	    
    
    
    /** Default constructor. */
    public UserMaster() {
        super();
    }
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getHierLevel() {
		return hierLevel;
	}
	public void setHierLevel(String hierLevel) {
		this.hierLevel = hierLevel;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getCreate_user() {
		return create_user;
	}
	public void setCreate_user(String create_user) {
		this.create_user = create_user;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
	public Date getModifyTime() {
		return modifyTime;
	}
	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}


  /* public String getSessionId() {
		return sessionid;
	}
	public void setSessionId(String sessionid) {
		this.sessionid = sessionid;
	}
	public Date getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	@Override
	public String toString() {
		return "UserMaster [id=" + id + ", code=" + code + ", name=" + name + ", passwd=" + passwd + ", dept=" + dept
				+ ", hierLevel=" + hierLevel + ", status=" + status + ", role=" + role + ", create_user=" + create_user
				+ ", createTime=" + createTime + ", modifyUser=" + modifyUser + ", modifyTime=" + modifyTime
				+ ", reason=" + reason + ", sessionid=" + sessionid + ", lastLogin=" + lastLogin + ", getId()="
				+ getId() + ", getCode()=" + getCode() + ", getName()=" + getName() + ", getPasswd()=" + getPasswd()
				+ ", getDept()=" + getDept() + ", getHierLevel()=" + getHierLevel() + ", getRole()=" + getRole()
				+ ", getCreate_user()=" + getCreate_user() + ", getCreateTime()=" + getCreateTime()
				+ ", getModifyUser()=" + getModifyUser() + ", getModifyTime()=" + getModifyTime() + ", getStatus()="
				+ getStatus() + ", getReason()=" + getReason() + ", getSessionId()=" + getSessionId()
				+ ", getLastLogin()=" + getLastLogin() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}*/

	
	
}
