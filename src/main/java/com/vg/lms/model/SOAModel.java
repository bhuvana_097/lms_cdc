package com.vg.lms.model;

import java.util.List;


public class SOAModel {
	private String loanid;
	private String loanno;
	private List<SOACreditDebitScheduleModel> soacreditdebitmodel;
	public String getLoanid() {
		return loanid;
	}
	public void setLoanid(String loanid) {
		this.loanid = loanid;
	}
	public String getLoanno() {
		return loanno;
	}
	public void setLoanno(String loanno) {
		this.loanno = loanno;
	}
	public List<SOACreditDebitScheduleModel> getSoacreditdebitmodel() {
		return soacreditdebitmodel;
	}
	public void setSoacreditdebitmodel(List<SOACreditDebitScheduleModel> soacreditdebitmodel) {
		this.soacreditdebitmodel = soacreditdebitmodel;
	}
	
	
}
