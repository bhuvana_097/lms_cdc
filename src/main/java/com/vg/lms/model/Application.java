// Generated with g9.

package com.vg.lms.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="application")
public class Application implements Serializable {

  
    /**
	 * 
	 */
	private static final long serialVersionUID = 2361447040422992317L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, length=26)
    private String appno;
    @Column(length=25)
    private String leadno;
    @Column(length=10)
    private String type;
    @Column(name="app_bl_code", length=100)
    private String appBlCode;
    @Column(name="fe_state", length=30)
    private String feState;
    @Column(length=20)
    private String originator;
    @Column(name="fe_name", length=100)
    private String feName;
    private Timestamp intime;
    @Column(name="app_edit_intime")
    private Timestamp appEditIntime;
    @Column(length=20)
    private String product;
    @Column(name="global_or_local_scheme", length=20)
    private String globalOrLocalScheme;
    @Column(name="scheme_type", length=100)
    private String schemeType;
    @Column(length=20)
    private String make;
    @Column(name="prod_subcode", length=50)
    private String prodSubcode;
    @Column(length=40)
    private String variant;
    @Column(name="dealer_name", length=500)
    private String dealerName;
    @Column(name="dealer_code", length=500)
    private int dealerCode;
    @Column(length=3)
    private String tenure;
    @Column(precision=15, scale=2)
    private float emi;
    @Column(name="customer_type", length=20)
    private String customerType;
    @Column(name="entity_type", length=2)
    private String entityType;
    @Column(length=10)
    private String gender;
    @Column(length=4)
    private String salutation;
    @Column(length=60)
    private String name;
    @Column(length=100)
    private String salorself;
    @Column(length=200)
    private String profile;
    @Column(name="house_type", length=20)
    private String houseType;
    @Column(name="wall_type", length=20)
    private String wallType;
    @Column(name="house_size", length=20)
    private String houseSize;
    @Column(name="house_size_type", length=20)
    private String houseSizeType;
    @Column(name="mode_type", length=20)
    private String modeType;
    @Column(name="income_level", length=20)
    private String incomeLevel;
    @Column(name="income_noincome", length=10)
    private String incomeNoincome;
    @Column(length=20)
    private String dob;
    @Column(name="resi_addr", length=80)
    private String resiAddr;
    @Column(name="resi_address2", length=80)
    private String resiAddress2;
    @Column(name="resi_city", length=40)
    private String resiCity;
    @Column(name="resi_pin", length=6)
    private String resiPin;
    @Column(name="resi_state", length=50)
    private String resiState;
    @Column(name="resi_ph", length=20)
    private String resiPh;
    @Column(length=15)
    private String mobileno;
    @Column(length=200)
    private String category;
    @Column(name="curr_addr_is_perm_addr", length=4)
    private String currAddrIsPermAddr;
    @Column(name="perm_addr", length=80)
    private String permAddr;
    @Column(name="perm_addr2", length=80)
    private String permAddr2;
    @Column(name="perm_city", length=40)
    private String permCity;
    @Column(name="perm_state", length=50)
    private String permState;
    @Column(name="perm_pincode", length=6)
    private String permPincode;
    @Column(name="off_name", length=100)
    private String offName;
    @Column(name="off_addr", length=500)
    private String offAddr;
    @Column(name="off_addr2", length=500)
    private String offAddr2;
    @Column(name="off_city", length=100)
    private String offCity;
    @Column(name="off_state", length=100)
    private String offState;
    @Column(name="off_pin", length=6)
    private String offPin;
    @Column(name="off_std", length=5)
    private String offStd;
    @Column(name="off_ph", length=10)
    private String offPh;
    @Column(length=10)
    private String panno;
    @Column(name="pan_holder_name", length=60)
    private String panHolderName;
    @Column(name="passport_no", length=15)
    private String passportNo;
    @Column(length=25)
    private String voterid;
    @Column(name="driving_license", length=30)
    private String drivingLicense;
    @Column(name="ration_card", length=25)
    private String rationCard;
    @Column(length=12)
    private String aadhar;
    @Column(name="post_office_no", length=20)
    private String postOfficeNo;
    @Column(name="father_name", length=60)
    private String fatherName;
    @Column(name="mother_name", length=60)
    private String motherName;
    @Column(name="spouse_name", length=60)
    private String spouseName;
    @Column(name="email_id", length=50)
    private String emailId;
    @Column(name="resi_cum_off", length=4)
    private String resiCumOff;
    @Column(name="no_of_vehicles", precision=10)
    private int noOfVehicles;
    @Column(name="cost_per_vehicle", precision=20, scale=2)
    private float costPerVehicle;
    @Column(name="finance_per_vehicle", precision=20, scale=2)
    private float financePerVehicle;
    @Column(name="total_cost", precision=20, scale=2)
    private float totalCost;
    @Column(name="total_finance_amount", precision=20, scale=2)
    private float totalFinanceAmount;
    @Column(name="vehicle_usage", length=9)
    private String vehicleUsage;
    @Column(name="loan_amt", precision=20)
    private float loanAmt;
    @Column(length=30)
    private String religion;
    @Column(length=30)
    private String caste;
    @Column(name="ref1_name", length=100)
    private String ref1Name;
    @Column(name="ref1_relationship", length=100)
    private String ref1Relationship;
    @Column(name="ref1_mobileno", length=15)
    private String ref1Mobileno;
    @Column(name="ref2_name", length=100)
    private String ref2Name;
    @Column(name="ref2_relationship", length=100)
    private String ref2Relationship;
    @Column(name="ref2_mobileno", length=15)
    private String ref2Mobileno;
    @Column(name="guarantor_avail", length=4)
    private String guarantorAvail;
    @Column(name="relationship_with_hirer", length=60)
    private String relationshipWithHirer;
    @Column(name="guarantor_completed", precision=10)
    private int guarantorCompleted;
    @Column(name="gua_retrigger_name", length=20)
    private String guaRetriggerName;
    @Column(name="app_latitude", length=20)
    private String appLatitude;
    @Column(name="app_longitude", length=20)
    private String appLongitude;
    @Column(name="app_gpslocation", length=500)
    private String appGpslocation;
    @Column(name="app_cid", length=10)
    private String appCid;
    @Column(name="app_lac", length=10)
    private String appLac;
   /*@Column(name="app_lat", precision=20, scale=2)
    private float appLat;
    @Column(name="app_long", precision=20, scale=2)
    private float appLong;*/
    /*@Column(name="guarantor_latitude", precision=20, scale=2)
    private float guarantorLatitude;
    @Column(name="guarantor_longitude", precision=20, scale=2)
    private float guarantorLongitude;*/
    @Column(name="guarantor_cid", length=10)
    private String guarantorCid;
    @Column(name="guarantor_lac", length=10)
    private String guarantorLac;
   /* @Column(name="guarantor_lat", precision=20, scale=2)
    private float guarantorLat;
    @Column(name="guarantor_long", precision=20, scale=2)
    private float guarantorLong;*/
    @Column(length=25)
    private String status;
    @Column(name="resi_verified", precision=10)
    private int resiVerified;
    @Column(name="fi_business_name", length=100)
    private String fiBusinessName;
    @Column(name="resi_fe", length=20)
    private String resiFe;
    @Column(name="resi_fe_allocated", length=20)
    private String resiFeAllocated;
    @Column(name="resi_verify_time")
    private Timestamp resiVerifyTime;
    @Column(name="fi_resi_fathername", length=60)
    private String fiResiFathername;
    @Column(name="fi_address_confirm", length=10)
    private String fiAddressConfirm;
    @Column(name="fi_profile", length=200)
    private String fiProfile;
    @Column(name="fi_experience", precision=10)
    private int fiExperience;
    @Column(name="fi_self", length=100)
    private String fiSelf;
    @Column(name="fi_relationship", length=100)
    private String fiRelationship;
    @Column(name="fi_family_member", length=100)
    private String fiFamilyMember;
    @Column(name="fi_earning_member", length=100)
    private String fiEarningMember;
    @Column(name="fi_staying_since", length=100)
    private String fiStayingSince;
    @Column(name="fi_residence_area", length=100)
    private String fiResidenceArea;
    @Column(name="fi_residence_ownership", length=100)
    private String fiResidenceOwnership;
    @Column(name="fi_residence_owner_name", length=100)
    private String fiResidenceOwnerName;
    @Column(name="fi_rent_amount", length=100)
    private String fiRentAmount;
    @Column(name="fi_is_permanent_address", length=100)
    private String fiIsPermanentAddress;
    @Column(name="fi_resi_type", length=100)
    private String fiResiType;
    @Column(name="fi_resi_construction", length=100)
    private String fiResiConstruction;
    @Column(name="fi_accessibility", length=100)
    private String fiAccessibility;
    @Column(name="fi_resi_locality", length=100)
    private String fiResiLocality;
    @Column(name="fi_interior", length=100)
    private String fiInterior;
    @Column(name="fi_exterior", length=100)
    private String fiExterior;
    @Column(name="fi_assets", length=500)
    private String fiAssets;
    @Column(name="fi_education", length=100)
    private String fiEducation;
    @Column(name="fi_maritial_status", length=100)
    private String fiMaritialStatus;
    @Column(name="fi_accommodation", length=100)
    private String fiAccommodation;
    @Column(name="fi_resi_area", length=100)
    private String fiResiArea;
    @Column(name="fi_neighbour_chk", length=100)
    private String fiNeighbourChk;
    @Column(name="fi_ref1_name", length=100)
    private String fiRef1Name;
    @Column(name="fi_ref2_name", length=100)
    private String fiRef2Name;
    @Column(name="fi_cpv_status", length=100)
    private String fiCpvStatus;
    @Column(name="fi_remark", length=500)
    private String fiRemark;
    @Column(name="fi_dis_from_branch", length=100)
    private String fiDisFromBranch;
    @Column(name="fi_verification_name", length=100)
    private String fiVerificationName;
    @Column(name="fi_resi_addr", length=80)
    private String fiResiAddr;
    @Column(name="fi_resi_addr2", length=80)
    private String fiResiAddr2;
    @Column(name="fi_resi_city", length=40)
    private String fiResiCity;
    @Column(name="fi_resi_state", length=50)
    private String fiResiState;
    @Column(name="fi_resi_pincode", length=6)
    private String fiResiPincode;
    @Column(name="fi_resi_ph", length=15)
    private String fiResiPh;
    @Column(name="resi_mobile", length=15)
    private String resiMobile;
    @Column(name="fi_resi_perm_address", length=80)
    private String fiResiPermAddress;
    @Column(name="fi_resi_perm_address2", length=80)
    private String fiResiPermAddress2;
    @Column(name="fi_resi_perm_city", length=40)
    private String fiResiPermCity;
    @Column(name="fi_resi_perm_state", length=50)
    private String fiResiPermState;
    @Column(name="fi_resi_perm_pincode", length=6)
    private String fiResiPermPincode;
    @Column(name="fi_resi_perm_landmark", length=100)
    private String fiResiPermLandmark;
    @Column(precision=10)
    private int age;
    @Column(name="marital_status", length=20)
    private String maritalStatus;
    @Column(name="fam_aware", length=3)
    private String famAware;
    @Column(name="resi_ownhouse", length=20)
    private String resiOwnhouse;
    @Column(name="resi_locality", length=20)
    private String resiLocality;
    @Column(name="resi_interior", length=20)
    private String resiInterior;
    @Column(name="resi_locatability", length=30)
    private String resiLocatability;
    @Column(name="resi_landmark", length=100)
    private String resiLandmark;
    @Column(name="resi_right", length=40)
    private String resiRight;
    @Column(name="resi_left", length=40)
    private String resiLeft;
    @Column(name="resi_front", length=40)
    private String resiFront;
    @Column(name="resi_rear", length=40)
    private String resiRear;
    @Column(name="resi_years", precision=10)
    private int resiYears;
    @Column(name="resi_distance", precision=10)
    private int resiDistance;
    @Column(name="resi_per_cur", length=3)
    private String resiPerCur;
    @Column(name="resi_dependents", length=3)
    private String resiDependents;
    @Column(name="resi_loan_amount", length=50)
    private String resiLoanAmount;
    @Column(name="prod_delivered", length=3)
    private String prodDelivered;
    @Column(name="regno_vehi", length=40)
    private String regnoVehi;
    @Column(name="product_match", length=3)
    private String productMatch;
    @Column(name="resi_netearning", precision=19)
    private long resiNetearning;
    @Column(name="resi_fam_income", precision=19)
    private long resiFamIncome;
    @Column(name="resi_resicumoff", length=3)
    private String resiResicumoff;
    @Column(name="resi_prev_loan", length=3)
    private String resiPrevLoan;
    @Column(name="resi_prev_loanbank", length=100)
    private String resiPrevLoanbank;
    @Column(length=3)
    private String kyc;
    @Column(name="resi_panno", length=10)
    private String resiPanno;
    @Column(name="resi_passport_no", length=15)
    private String resiPassportNo;
    @Column(name="resi_voterid", length=25)
    private String resiVoterid;
    @Column(name="resi_driving_license", length=30)
    private String resiDrivingLicense;
    @Column(name="resi_ration_card", length=25)
    private String resiRationCard;
    @Column(name="resi_aadhar", length=12)
    private String resiAadhar;
    @Column(name="resi_postoffice_no", length=20)
    private String resiPostofficeNo;
    @Column(name="resi_neigh1_name", length=60)
    private String resiNeigh1Name;
    @Column(name="resi_neigh1_mobileno", length=11)
    private String resiNeigh1Mobileno;
    @Column(name="resi_neigh1", length=60)
    private String resiNeigh1;
    @Column(name="resi_neigh2_name", length=60)
    private String resiNeigh2Name;
    @Column(name="resi_neigh2_mobileno", length=11)
    private String resiNeigh2Mobileno;
    @Column(name="resi_neigh2", length=60)
    private String resiNeigh2;
    @Column(name="resi_ref1_name", length=60)
    private String resiRef1Name;
    @Column(name="resi_ref1_mobileno", length=11)
    private String resiRef1Mobileno;
    @Column(name="resi_ref2_name", length=60)
    private String resiRef2Name;
    @Column(name="resi_ref2_mobileno", length=11)
    private String resiRef2Mobileno;
    @Column(name="resi_refcode_ecn", length=15)
    private String resiRefcodeEcn;
    @Column(name="resi_person_met", length=60)
    private String resiPersonMet;
    @Column(name="resi_occupation", length=100)
    private String resiOccupation;
    @Column(name="resi_attempts", precision=10)
    private int resiAttempts;
    @Column(name="resi_observation", length=30)
    private String resiObservation;
    @Column(name="resi_behaviour", length=30)
    private String resiBehaviour;
    @Column(name="resi_recomendation", length=30)
    private String resiRecomendation;
    @Column(name="resi_feedback", length=30)
    private String resiFeedback;
    @Column(name="resi_verifier_comments", length=400)
    private String resiVerifierComments;
    @Column(name="resi_rejectreason", length=100)
    private String resiRejectreason;
    @Column(name="resi_latitude", precision=20, scale=2)
    private float resiLatitude;
    @Column(name="resi_longitude", precision=20, scale=2)
    private float resiLongitude;
    @Column(name="resi_cid", length=10)
    private String resiCid;
    @Column(name="resi_lac", length=10)
    private String resiLac;
    @Column(name="resi_lat", precision=20, scale=2)
    private float resiLat;
    @Column(name="resi_long", precision=20, scale=2)
    private float resiLong;
    @Column(name="fi_off_addr", length=80)
    private String fiOffAddr;
    @Column(name="fi_off_addr2", length=80)
    private String fiOffAddr2;
    @Column(name="fi_off_city", length=40)
    private String fiOffCity;
    @Column(name="fi_off_pincode", length=60)
    private String fiOffPincode;
    @Column(name="fi_off_state", length=50)
    private String fiOffState;
    @Column(name="fi_off_ph", length=15)
    private String fiOffPh;
    @Column(name="reference_no", length=20)
    private String referenceNo;
    @Column(name="branch_code", length=60)
    private String branchCode;
    @Column(name="direct_make", length=100)
    private String directMake;
    @Column(name="direct_variant", length=150)
    private String directVariant;
    /*@Column(name="fi_priority", precision=10)
    private int fiPriority;*/
    @Column(name="father_spouse_address_proof", length=100)
    private String fatherSpouseAddressProof;
    @Column(name="father_spouse_id_proof", length=100)
    private String fatherSpouseIdProof;
    @Column(name="father_spouse_id_proof_value", length=100)
    private String fatherSpouseIdProofValue;
    @Column(name="father_spouse_address_proof_value", length=100)
    private String fatherSpouseAddressProofValue;
    @Column(name="father_spouse_panholder_name", length=100)
    private String fatherSpousePanholderName;
    @Column(name="father_spouse_dob")
    private Timestamp fatherSpouseDob;
    @Column(name="father_spouse_mobile_no", length=12)
    private String fatherSpouseMobileNo;
    @Column(name="father_spouse_name", length=100)
    private String fatherSpouseName;
    @Column(name="father_spouse_is_guarantor", length=4)
    private String fatherSpouseIsGuarantor;
    @Column(length=30)
    private String firstname;
    @Column(length=30)
    private String lastname;
    @Column(name="app_syntime")
    private Timestamp appSyntime;
    @Column(name="app_edit_syntime")
    private Timestamp appEditSyntime;
    @Column(name="resi_verify_syntime")
    private Timestamp resiVerifySyntime;
    @Column(name="cibil_score", length=6)
    private String cibilScore;
    @Column(name="crif_score", length=30)
    private String crifScore;
    @Column(precision=5, scale=2)
    private float roi;
    @Column(name="resi_std", length=5)
    private String resiStd;
    @Column(name="fi_off_std", length=5)
    private String fiOffStd;
    @Column(name="fi_resi_std", length=5)
    private String fiResiStd;
    @Column(name="resi_status", length=20)
    private String resiStatus;
    @Column(name="guarantor_firstname", length=60)
    private String guarantorFirstname;
    @Column(name="guarantor_lastname", length=60)
    private String guarantorLastname;
    @Column(name="no_of_cow", precision=10)
    private int noOfCow;
    @Column(name="no_of_buffalo", precision=10)
    private int noOfBuffalo;
    @Column(name="no_of_bulls", precision=10)
    private int noOfBulls;
    @Column(name="kcc_loan", length=100)
    private String kccLoan;
    @Column(name="kcc_loanbank", length=100)
    private String kccLoanbank;
    @Column(name="avg_landvalue", length=100)
    private String avgLandvalue;
    @Column(name="photo_not_allowed", length=10)
    private String photoNotAllowed;
    @Column(length=100)
    private String unit;
    @Column(length=100)
    private String typeofroof;
    @Column(length=100)
    private String flooring;
    @Column(name="kcc_loanavail", length=10)
    private String kccLoanavail;
    @Column(name="installment_type", length=20)
    private String installmentType;
    @Column(length=20)
    private String noofinstallments;
    /*@Column(name="cibil_tot_od", precision=20, scale=2)
    private float cibilTotOd;*/
    @Column(name="cibil_detect_flag", length=1)
    private String cibilDetectFlag;
    @Column(name="passport_expiry_date")
    private Timestamp passportExpiryDate;
    @Column(name="driving_license_expiry_date")
    private Timestamp drivingLicenseExpiryDate;
    @Column(name="noof_variant", precision=10)
    private int noofVariant;
    @Column(name="noof_variant_completed", precision=10)
    private int noofVariantCompleted;
    @Column(name="noof_guarantor", length=20)
    private String noofGuarantor;
    @Column(name="noof_guarantor_completed", precision=10)
    private int noofGuarantorCompleted;
    @Column(name="resi_neigh_check_done", length=5)
    private String resiNeighCheckDone;
    @Column(name="resi_neigh_type", length=50)
    private String resiNeighType;
    @Column(length=5)
    private String borrowercoapplicantresidinginsame;
    @Column(name="no_of_beneficiaries_completed", precision=10)
    private int noOfBeneficiariesCompleted;
    @Column(name="existing_customer", length=10)
    private String existingCustomer;
    @Column(name="customer_code", length=20)
    private String customerCode;
    @Column(name="verification_status", length=10)
    private String verificationStatus;
    @Column(name="app_marital_status", length=20)
    private String appMaritalStatus;
    @Column(length=1)
    private String ekycstatus;
    @Column(name="cust_qualification", length=10)
    private String custQualification;
    @Column(name="comm_language", length=10)
    private String commLanguage;
    @Column(name="class_ofactivity", length=10)
    private String classOfactivity;
    @Column(length=10)
    private String loantype;
    @Column(name="resi_neigh_type2", length=50)
    private String resiNeighType2;
    @Column(name="guarantor_or_coapp", length=20)
    private String guarantorOrCoapp;
    @Column(name="auto_approved", precision=10)
    private int autoApproved;
    @Column(name="app_autoapproved_status", length=2)
    private String appAutoapprovedStatus;
    @Column(name="app_deviation_reason", length=100)
    private String appDeviationReason;
    @Column(name="app_credit_level", length=2)
    private String appCreditLevel;
    @Column(name="app_credit_approver_name", length=50)
    private String appCreditApproverName;
    @Column(name="app_credit_approved_time")
    private Timestamp appCreditApprovedTime;
    @Column(name="app_credit_approved_status", length=2)
    private String appCreditApprovedStatus;
    @Column(name="credit_recommender_name", length=50)
    private String creditRecommenderName;
    @Column(name="credit_recommended_status", length=2)
    private String creditRecommendedStatus;
    @Column(name="credit_recommended_comments", length=500)
    private String creditRecommendedComments;
    @Column(name="credit_hold_comments", length=1000)
    private String creditHoldComments;
    @Column(name="credit_edit_app", length=100)
    private String creditEditApp;
    @Column(name="relook_otp", length=10)
    private String relookOtp;
    @Column(name="grid_price", precision=15, scale=2)
    private float gridPrice;
    @Column(name="onroad_price", precision=15, scale=2)
    private float onroadPrice;
    @Column(name="actual_ltv", precision=15, scale=2)
    private float actualLtv;
    @Column(name="advance_emi", length=10)
    private String advanceEmi;
    @Column(length=100)
    private String resilandmark;
    @Column(length=100)
    private String permlandmark;
    @Column(length=100)
    private String offlandmark;
    @Column(length=500)
    private String ref1addr;
    @Column(length=100)
    private String ref1city;
    @Column(length=100)
    private String ref1state;
    @Column(length=6)
    private String ref1pincode;
    @Column(length=500)
    private String ref2addr;
    @Column(length=100)
    private String ref2city;
    @Column(length=100)
    private String ref2state;
    @Column(length=100)
    private String bankname;
    @Column(length=100)
    private String bankaccno;
    @Column(length=100)
    private String bankbranch;
    @Column(length=100)
    private String bankifsc;
    @Column(name="insurance_amount", precision=15, scale=2)
    private float insuranceAmount;
    @Column(name="ins_base", precision=15, scale=2)
    private float insBase;
    @Column(name="ins_tax", precision=15, scale=2)
    private float insTax;
    
    @Column(name="processing_fee", precision=15, scale=2)
    private float processingFee;
    @Column(name="pf_base", precision=15, scale=2)
    private float pfBase;
    @Column(name="pf_tax", precision=15, scale=2)
    private float pftax;
    
    @Column(name="pf_tail", precision=15, scale=2)
    private float pfTail;
    @Column(name="pf_tail_base", precision=15, scale=2)
    private float pfTailBase;
    @Column(name="pf_tail_tax", precision=15, scale=2)
    private float pfTailTax;
    
    @Column(name="cash_coll_charges", precision=15, scale=2)
    private float cashCollCharges;
    @Column(name="cc_base", precision=15, scale=2)
    private float ccBase;
    @Column(name="cc_tax", precision=15, scale=2)
    private float ccTax;
    
    @Column(name="cc_tail", precision=15, scale=2)
    private float cashCollChargesTail;
    @Column(name="cc_tail_base", precision=15, scale=2)
    private float ccTailBase;
    @Column(name="cc_tail_tax", precision=15, scale=2)
    private float ccTailTax;
    
    @Column(name="dealer_disbursement_amount", precision=15, scale=2)
    private float dealerDisbursementAmount;
    @Column(name="app_autoreject_reason", length=1000)
    private String appAutorejectReason;
    @Column(name="down_payment", precision=15, scale=2)
    private float downPayment;
    @Column(name="unhold_reason", length=1000)
    private String unholdReason;
    @Column(name="ops_comments", length=200)
    private String opsComments;
    @Column(name="ops_hold_comments", length=200)
    private String opsHoldComments;
    @Column(name="ops_approve_status", length=2)
    private String opsApproveStatus;
    @Column(name="ops_approver_comments", length=1000)
    private String opsApproverComments;
    @Column(name="ops_approver_name", length=20)
    private String opsApproverName;
    @Column(name="ops_approve_time")
    private Timestamp opsApproveTime;
    @Column(name="app_deactivate", precision=10)
    private int appDeactivate;
    @Column(name="ops_status", length=15)
    private String opsStatus;
    @Column(name="ops_disburse_time")
    private Timestamp opsDisburseTime;
    @Column(name="lms_update", precision=10)
    private int lmsUpdate;
    @Column(name="pdd_path", length=200)
    private String pddPath;
    @Column(name="pre_emi_charges", precision=15, scale=2)
    private float preEmiCharges;
    @Column(name="co_lender", length=15)
    private String coLender;
    @Column(name="co_lender_percentage", precision=11)
	private int coLenderPercentage;
	@Column(name="co_lender_roi", precision=6, scale=2)
	private float coLenderRoi;
    @Column(name="co_lender_status", length=3)
    private String coLenderStatus;
    @Column(name="co_lender_disposition", length=20)
    private String coLenderDisposition;
    @Column(name="sc_charge", precision=15, scale=2)
    private float scCharge;   
    @Column(name="lms_custid")
    private String lmsCusT;
    
   
    

    /** Default constructor. */
    public Application() {
        super();
    }

    /**
     * Access method for appno.
     *
     * @return the current value of appno
     */
    public String getAppno() {
        return appno;
    }

    /**
     * Setter method for appno.
     *
     * @param aAppno the new value for appno
     */
    public void setAppno(String aAppno) {
        appno = aAppno;
    }

    /**
     * Access method for leadno.
     *
     * @return the current value of leadno
     */
    public String getLeadno() {
        return leadno;
    }

    /**
     * Setter method for leadno.
     *
     * @param aLeadno the new value for leadno
     */
    public void setLeadno(String aLeadno) {
        leadno = aLeadno;
    }

    /**
     * Access method for type.
     *
     * @return the current value of type
     */
    public String getType() {
        return type;
    }

    /**
     * Setter method for type.
     *
     * @param aType the new value for type
     */
    public void setType(String aType) {
        type = aType;
    }

    /**
     * Access method for appBlCode.
     *
     * @return the current value of appBlCode
     */
    public String getAppBlCode() {
        return appBlCode;
    }

    /**
     * Setter method for appBlCode.
     *
     * @param aAppBlCode the new value for appBlCode
     */
    public void setAppBlCode(String aAppBlCode) {
        appBlCode = aAppBlCode;
    }

    /**
     * Access method for feState.
     *
     * @return the current value of feState
     */
    public String getFeState() {
        return feState;
    }

    /**
     * Setter method for feState.
     *
     * @param aFeState the new value for feState
     */
    public void setFeState(String aFeState) {
        feState = aFeState;
    }

    /**
     * Access method for originator.
     *
     * @return the current value of originator
     */
    public String getOriginator() {
        return originator;
    }

    /**
     * Setter method for originator.
     *
     * @param aOriginator the new value for originator
     */
    public void setOriginator(String aOriginator) {
        originator = aOriginator;
    }

    /**
     * Access method for feName.
     *
     * @return the current value of feName
     */
    public String getFeName() {
        return feName;
    }

    /**
     * Setter method for feName.
     *
     * @param aFeName the new value for feName
     */
    public void setFeName(String aFeName) {
        feName = aFeName;
    }

    /**
     * Access method for intime.
     *
     * @return the current value of intime
     */
    public Timestamp getIntime() {
        return intime;
    }

    /**
     * Setter method for intime.
     *
     * @param aIntime the new value for intime
     */
    public void setIntime(Timestamp aIntime) {
        intime = aIntime;
    }

    /**
     * Access method for appEditIntime.
     *
     * @return the current value of appEditIntime
     */
    public Timestamp getAppEditIntime() {
        return appEditIntime;
    }

    /**
     * Setter method for appEditIntime.
     *
     * @param aAppEditIntime the new value for appEditIntime
     */
    public void setAppEditIntime(Timestamp aAppEditIntime) {
        appEditIntime = aAppEditIntime;
    }

    /**
     * Access method for product.
     *
     * @return the current value of product
     */
    public String getProduct() {
        return product;
    }

    /**
     * Setter method for product.
     *
     * @param aProduct the new value for product
     */
    public void setProduct(String aProduct) {
        product = aProduct;
    }

    /**
     * Access method for globalOrLocalScheme.
     *
     * @return the current value of globalOrLocalScheme
     */
    public String getGlobalOrLocalScheme() {
        return globalOrLocalScheme;
    }

    /**
     * Setter method for globalOrLocalScheme.
     *
     * @param aGlobalOrLocalScheme the new value for globalOrLocalScheme
     */
    public void setGlobalOrLocalScheme(String aGlobalOrLocalScheme) {
        globalOrLocalScheme = aGlobalOrLocalScheme;
    }

    /**
     * Access method for schemeType.
     *
     * @return the current value of schemeType
     */
    public String getSchemeType() {
        return schemeType;
    }

    /**
     * Setter method for schemeType.
     *
     * @param aSchemeType the new value for schemeType
     */
    public void setSchemeType(String aSchemeType) {
        schemeType = aSchemeType;
    }

    /**
     * Access method for make.
     *
     * @return the current value of make
     */
    public String getMake() {
        return make;
    }

    /**
     * Setter method for make.
     *
     * @param aMake the new value for make
     */
    public void setMake(String aMake) {
        make = aMake;
    }

    /**
     * Access method for prodSubcode.
     *
     * @return the current value of prodSubcode
     */
    public String getProdSubcode() {
        return prodSubcode;
    }

    /**
     * Setter method for prodSubcode.
     *
     * @param aProdSubcode the new value for prodSubcode
     */
    public void setProdSubcode(String aProdSubcode) {
        prodSubcode = aProdSubcode;
    }

    /**
     * Access method for variant.
     *
     * @return the current value of variant
     */
    public String getVariant() {
        return variant;
    }

    /**
     * Setter method for variant.
     *
     * @param aVariant the new value for variant
     */
    public void setVariant(String aVariant) {
        variant = aVariant;
    }

    /**
     * Access method for dealerName.
     *
     * @return the current value of dealerName
     */
    public String getDealerName() {
        return dealerName;
    }

    /**
     * Setter method for dealerName.
     *
     * @param aDealerName the new value for dealerName
     */
    public void setDealerName(String aDealerName) {
        dealerName = aDealerName;
    }

    /**
     * Access method for tenure.
     *
     * @return the current value of tenure
     */
    public String getTenure() {
        return tenure;
    }

    /**
     * Setter method for tenure.
     *
     * @param aTenure the new value for tenure
     */
    public void setTenure(String aTenure) {
        tenure = aTenure;
    }

    /**
     * Access method for emi.
     *
     * @return the current value of emi
     */
    public float getEmi() {
        return emi;
    }

    /**
     * Setter method for emi.
     *
     * @param aEmi the new value for emi
     */
    public void setEmi(float aEmi) {
        emi = aEmi;
    }
    
    
    public float getScCharge() {
    	return scCharge;
    }
    
    public void setScCharge(float cCharge) {
    	scCharge = cCharge;
    }
   public String getlmsCusT(){
    	return lmsCusT;
    }
   
   public void setlmsCusT(String lmsCusT){
   	 lmsCusT = lmsCusT;
   }
    /**
     * Access method for customerType.
     *
     * @return the current value of customerType
     */
    public String getCustomerType() {
        return customerType;
    }

    /**
     * Setter method for customerType.
     *
     * @param aCustomerType the new value for customerType
     */
    public void setCustomerType(String aCustomerType) {
        customerType = aCustomerType;
    }

    /**
     * Access method for entityType.
     *
     * @return the current value of entityType
     */
    public String getEntityType() {
        return entityType;
    }

    /**
     * Setter method for entityType.
     *
     * @param aEntityType the new value for entityType
     */
    public void setEntityType(String aEntityType) {
        entityType = aEntityType;
    }

    /**
     * Access method for gender.
     *
     * @return the current value of gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * Setter method for gender.
     *
     * @param aGender the new value for gender
     */
    public void setGender(String aGender) {
        gender = aGender;
    }

    /**
     * Access method for salutation.
     *
     * @return the current value of salutation
     */
    public String getSalutation() {
        return salutation;
    }

    /**
     * Setter method for salutation.
     *
     * @param aSalutation the new value for salutation
     */
    public void setSalutation(String aSalutation) {
        salutation = aSalutation;
    }

    /**
     * Access method for name.
     *
     * @return the current value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for name.
     *
     * @param aName the new value for name
     */
    public void setName(String aName) {
        name = aName;
    }

    /**
     * Access method for salorself.
     *
     * @return the current value of salorself
     */
    public String getSalorself() {
        return salorself;
    }

    /**
     * Setter method for salorself.
     *
     * @param aSalorself the new value for salorself
     */
    public void setSalorself(String aSalorself) {
        salorself = aSalorself;
    }

    /**
     * Access method for profile.
     *
     * @return the current value of profile
     */
    public String getProfile() {
        return profile;
    }

    /**
     * Setter method for profile.
     *
     * @param aProfile the new value for profile
     */
    public void setProfile(String aProfile) {
        profile = aProfile;
    }

    /**
     * Access method for houseType.
     *
     * @return the current value of houseType
     */
    public String getHouseType() {
        return houseType;
    }

    /**
     * Setter method for houseType.
     *
     * @param aHouseType the new value for houseType
     */
    public void setHouseType(String aHouseType) {
        houseType = aHouseType;
    }

    /**
     * Access method for wallType.
     *
     * @return the current value of wallType
     */
    public String getWallType() {
        return wallType;
    }

    /**
     * Setter method for wallType.
     *
     * @param aWallType the new value for wallType
     */
    public void setWallType(String aWallType) {
        wallType = aWallType;
    }

    /**
     * Access method for houseSize.
     *
     * @return the current value of houseSize
     */
    public String getHouseSize() {
        return houseSize;
    }

    /**
     * Setter method for houseSize.
     *
     * @param aHouseSize the new value for houseSize
     */
    public void setHouseSize(String aHouseSize) {
        houseSize = aHouseSize;
    }

    /**
     * Access method for houseSizeType.
     *
     * @return the current value of houseSizeType
     */
    public String getHouseSizeType() {
        return houseSizeType;
    }

    /**
     * Setter method for houseSizeType.
     *
     * @param aHouseSizeType the new value for houseSizeType
     */
    public void setHouseSizeType(String aHouseSizeType) {
        houseSizeType = aHouseSizeType;
    }

    /**
     * Access method for modeType.
     *
     * @return the current value of modeType
     */
    public String getModeType() {
        return modeType;
    }

    /**
     * Setter method for modeType.
     *
     * @param aModeType the new value for modeType
     */
    public void setModeType(String aModeType) {
        modeType = aModeType;
    }

    /**
     * Access method for incomeLevel.
     *
     * @return the current value of incomeLevel
     */
    public String getIncomeLevel() {
        return incomeLevel;
    }

    /**
     * Setter method for incomeLevel.
     *
     * @param aIncomeLevel the new value for incomeLevel
     */
    public void setIncomeLevel(String aIncomeLevel) {
        incomeLevel = aIncomeLevel;
    }

    /**
     * Access method for incomeNoincome.
     *
     * @return the current value of incomeNoincome
     */
    public String getIncomeNoincome() {
        return incomeNoincome;
    }

    /**
     * Setter method for incomeNoincome.
     *
     * @param aIncomeNoincome the new value for incomeNoincome
     */
    public void setIncomeNoincome(String aIncomeNoincome) {
        incomeNoincome = aIncomeNoincome;
    }

    /**
     * Access method for dob.
     *
     * @return the current value of dob
     */
    public String getDob() {
        return dob;
    }

    /**
     * Setter method for dob.
     *
     * @param aDob the new value for dob
     */
    public void setDob(String aDob) {
        dob = aDob;
    }

    /**
     * Access method for resiAddr.
     *
     * @return the current value of resiAddr
     */
    public String getResiAddr() {
        return resiAddr;
    }

    /**
     * Setter method for resiAddr.
     *
     * @param aResiAddr the new value for resiAddr
     */
    public void setResiAddr(String aResiAddr) {
        resiAddr = aResiAddr;
    }

    /**
     * Access method for resiAddress2.
     *
     * @return the current value of resiAddress2
     */
    public String getResiAddress2() {
        return resiAddress2;
    }

    /**
     * Setter method for resiAddress2.
     *
     * @param aResiAddress2 the new value for resiAddress2
     */
    public void setResiAddress2(String aResiAddress2) {
        resiAddress2 = aResiAddress2;
    }

    /**
     * Access method for resiCity.
     *
     * @return the current value of resiCity
     */
    public String getResiCity() {
        return resiCity;
    }

    /**
     * Setter method for resiCity.
     *
     * @param aResiCity the new value for resiCity
     */
    public void setResiCity(String aResiCity) {
        resiCity = aResiCity;
    }

    /**
     * Access method for resiPin.
     *
     * @return the current value of resiPin
     */
    public String getResiPin() {
        return resiPin;
    }

    /**
     * Setter method for resiPin.
     *
     * @param aResiPin the new value for resiPin
     */
    public void setResiPin(String aResiPin) {
        resiPin = aResiPin;
    }

    /**
     * Access method for resiState.
     *
     * @return the current value of resiState
     */
    public String getResiState() {
        return resiState;
    }

    /**
     * Setter method for resiState.
     *
     * @param aResiState the new value for resiState
     */
    public void setResiState(String aResiState) {
        resiState = aResiState;
    }

    /**
     * Access method for resiPh.
     *
     * @return the current value of resiPh
     */
    public String getResiPh() {
        return resiPh;
    }

    /**
     * Setter method for resiPh.
     *
     * @param aResiPh the new value for resiPh
     */
    public void setResiPh(String aResiPh) {
        resiPh = aResiPh;
    }

    /**
     * Access method for mobileno.
     *
     * @return the current value of mobileno
     */
    public String getMobileno() {
        return mobileno;
    }

    /**
     * Setter method for mobileno.
     *
     * @param aMobileno the new value for mobileno
     */
    public void setMobileno(String aMobileno) {
        mobileno = aMobileno;
    }

    /**
     * Access method for category.
     *
     * @return the current value of category
     */
    public String getCategory() {
        return category;
    }

    /**
     * Setter method for category.
     *
     * @param aCategory the new value for category
     */
    public void setCategory(String aCategory) {
        category = aCategory;
    }

    /**
     * Access method for currAddrIsPermAddr.
     *
     * @return the current value of currAddrIsPermAddr
     */
    public String getCurrAddrIsPermAddr() {
        return currAddrIsPermAddr;
    }

    /**
     * Setter method for currAddrIsPermAddr.
     *
     * @param aCurrAddrIsPermAddr the new value for currAddrIsPermAddr
     */
    public void setCurrAddrIsPermAddr(String aCurrAddrIsPermAddr) {
        currAddrIsPermAddr = aCurrAddrIsPermAddr;
    }

    /**
     * Access method for permAddr.
     *
     * @return the current value of permAddr
     */
    public String getPermAddr() {
        return permAddr;
    }

    /**
     * Setter method for permAddr.
     *
     * @param aPermAddr the new value for permAddr
     */
    public void setPermAddr(String aPermAddr) {
        permAddr = aPermAddr;
    }

    /**
     * Access method for permAddr2.
     *
     * @return the current value of permAddr2
     */
    public String getPermAddr2() {
        return permAddr2;
    }

    /**
     * Setter method for permAddr2.
     *
     * @param aPermAddr2 the new value for permAddr2
     */
    public void setPermAddr2(String aPermAddr2) {
        permAddr2 = aPermAddr2;
    }

    /**
     * Access method for permCity.
     *
     * @return the current value of permCity
     */
    public String getPermCity() {
        return permCity;
    }

    /**
     * Setter method for permCity.
     *
     * @param aPermCity the new value for permCity
     */
    public void setPermCity(String aPermCity) {
        permCity = aPermCity;
    }

    /**
     * Access method for permState.
     *
     * @return the current value of permState
     */
    public String getPermState() {
        return permState;
    }

    /**
     * Setter method for permState.
     *
     * @param aPermState the new value for permState
     */
    public void setPermState(String aPermState) {
        permState = aPermState;
    }

    /**
     * Access method for permPincode.
     *
     * @return the current value of permPincode
     */
    public String getPermPincode() {
        return permPincode;
    }

    /**
     * Setter method for permPincode.
     *
     * @param aPermPincode the new value for permPincode
     */
    public void setPermPincode(String aPermPincode) {
        permPincode = aPermPincode;
    }

    /**
     * Access method for offName.
     *
     * @return the current value of offName
     */
    public String getOffName() {
        return offName;
    }

    /**
     * Setter method for offName.
     *
     * @param aOffName the new value for offName
     */
    public void setOffName(String aOffName) {
        offName = aOffName;
    }

    /**
     * Access method for offAddr.
     *
     * @return the current value of offAddr
     */
    public String getOffAddr() {
        return offAddr;
    }

    /**
     * Setter method for offAddr.
     *
     * @param aOffAddr the new value for offAddr
     */
    public void setOffAddr(String aOffAddr) {
        offAddr = aOffAddr;
    }

    /**
     * Access method for offAddr2.
     *
     * @return the current value of offAddr2
     */
    public String getOffAddr2() {
        return offAddr2;
    }

    /**
     * Setter method for offAddr2.
     *
     * @param aOffAddr2 the new value for offAddr2
     */
    public void setOffAddr2(String aOffAddr2) {
        offAddr2 = aOffAddr2;
    }

    /**
     * Access method for offCity.
     *
     * @return the current value of offCity
     */
    public String getOffCity() {
        return offCity;
    }

    /**
     * Setter method for offCity.
     *
     * @param aOffCity the new value for offCity
     */
    public void setOffCity(String aOffCity) {
        offCity = aOffCity;
    }

    /**
     * Access method for offState.
     *
     * @return the current value of offState
     */
    public String getOffState() {
        return offState;
    }

    /**
     * Setter method for offState.
     *
     * @param aOffState the new value for offState
     */
    public void setOffState(String aOffState) {
        offState = aOffState;
    }

    /**
     * Access method for offPin.
     *
     * @return the current value of offPin
     */
    public String getOffPin() {
        return offPin;
    }

    /**
     * Setter method for offPin.
     *
     * @param aOffPin the new value for offPin
     */
    public void setOffPin(String aOffPin) {
        offPin = aOffPin;
    }

    /**
     * Access method for offStd.
     *
     * @return the current value of offStd
     */
    public String getOffStd() {
        return offStd;
    }

    /**
     * Setter method for offStd.
     *
     * @param aOffStd the new value for offStd
     */
    public void setOffStd(String aOffStd) {
        offStd = aOffStd;
    }

    /**
     * Access method for offPh.
     *
     * @return the current value of offPh
     */
    public String getOffPh() {
        return offPh;
    }

    /**
     * Setter method for offPh.
     *
     * @param aOffPh the new value for offPh
     */
    public void setOffPh(String aOffPh) {
        offPh = aOffPh;
    }

    /**
     * Access method for panno.
     *
     * @return the current value of panno
     */
    public String getPanno() {
        return panno;
    }

    /**
     * Setter method for panno.
     *
     * @param aPanno the new value for panno
     */
    public void setPanno(String aPanno) {
        panno = aPanno;
    }

    /**
     * Access method for panHolderName.
     *
     * @return the current value of panHolderName
     */
    public String getPanHolderName() {
        return panHolderName;
    }

    /**
     * Setter method for panHolderName.
     *
     * @param aPanHolderName the new value for panHolderName
     */
    public void setPanHolderName(String aPanHolderName) {
        panHolderName = aPanHolderName;
    }

    /**
     * Access method for passportNo.
     *
     * @return the current value of passportNo
     */
    public String getPassportNo() {
        return passportNo;
    }

    /**
     * Setter method for passportNo.
     *
     * @param aPassportNo the new value for passportNo
     */
    public void setPassportNo(String aPassportNo) {
        passportNo = aPassportNo;
    }

    /**
     * Access method for voterid.
     *
     * @return the current value of voterid
     */
    public String getVoterid() {
        return voterid;
    }

    /**
     * Setter method for voterid.
     *
     * @param aVoterid the new value for voterid
     */
    public void setVoterid(String aVoterid) {
        voterid = aVoterid;
    }

    /**
     * Access method for drivingLicense.
     *
     * @return the current value of drivingLicense
     */
    public String getDrivingLicense() {
        return drivingLicense;
    }

    /**
     * Setter method for drivingLicense.
     *
     * @param aDrivingLicense the new value for drivingLicense
     */
    public void setDrivingLicense(String aDrivingLicense) {
        drivingLicense = aDrivingLicense;
    }

    /**
     * Access method for rationCard.
     *
     * @return the current value of rationCard
     */
    public String getRationCard() {
        return rationCard;
    }

    /**
     * Setter method for rationCard.
     *
     * @param aRationCard the new value for rationCard
     */
    public void setRationCard(String aRationCard) {
        rationCard = aRationCard;
    }

    /**
     * Access method for aadhar.
     *
     * @return the current value of aadhar
     */
    public String getAadhar() {
        return aadhar;
    }

    /**
     * Setter method for aadhar.
     *
     * @param aAadhar the new value for aadhar
     */
    public void setAadhar(String aAadhar) {
        aadhar = aAadhar;
    }

    /**
     * Access method for postOfficeNo.
     *
     * @return the current value of postOfficeNo
     */
    public String getPostOfficeNo() {
        return postOfficeNo;
    }

    /**
     * Setter method for postOfficeNo.
     *
     * @param aPostOfficeNo the new value for postOfficeNo
     */
    public void setPostOfficeNo(String aPostOfficeNo) {
        postOfficeNo = aPostOfficeNo;
    }

    /**
     * Access method for fatherName.
     *
     * @return the current value of fatherName
     */
    public String getFatherName() {
        return fatherName;
    }

    /**
     * Setter method for fatherName.
     *
     * @param aFatherName the new value for fatherName
     */
    public void setFatherName(String aFatherName) {
        fatherName = aFatherName;
    }

    /**
     * Access method for motherName.
     *
     * @return the current value of motherName
     */
    public String getMotherName() {
        return motherName;
    }

    /**
     * Setter method for motherName.
     *
     * @param aMotherName the new value for motherName
     */
    public void setMotherName(String aMotherName) {
        motherName = aMotherName;
    }

    /**
     * Access method for spouseName.
     *
     * @return the current value of spouseName
     */
    public String getSpouseName() {
        return spouseName;
    }

    /**
     * Setter method for spouseName.
     *
     * @param aSpouseName the new value for spouseName
     */
    public void setSpouseName(String aSpouseName) {
        spouseName = aSpouseName;
    }

    /**
     * Access method for emailId.
     *
     * @return the current value of emailId
     */
    public String getEmailId() {
        return emailId;
    }

    /**
     * Setter method for emailId.
     *
     * @param aEmailId the new value for emailId
     */
    public void setEmailId(String aEmailId) {
        emailId = aEmailId;
    }

    /**
     * Access method for resiCumOff.
     *
     * @return the current value of resiCumOff
     */
    public String getResiCumOff() {
        return resiCumOff;
    }

    /**
     * Setter method for resiCumOff.
     *
     * @param aResiCumOff the new value for resiCumOff
     */
    public void setResiCumOff(String aResiCumOff) {
        resiCumOff = aResiCumOff;
    }

    /**
     * Access method for noOfVehicles.
     *
     * @return the current value of noOfVehicles
     */
    public int getNoOfVehicles() {
        return noOfVehicles;
    }

    /**
     * Setter method for noOfVehicles.
     *
     * @param aNoOfVehicles the new value for noOfVehicles
     */
    public void setNoOfVehicles(int aNoOfVehicles) {
        noOfVehicles = aNoOfVehicles;
    }

    /**
     * Access method for costPerVehicle.
     *
     * @return the current value of costPerVehicle
     */
    public float getCostPerVehicle() {
        return costPerVehicle;
    }

    /**
     * Setter method for costPerVehicle.
     *
     * @param aCostPerVehicle the new value for costPerVehicle
     */
    public void setCostPerVehicle(float aCostPerVehicle) {
        costPerVehicle = aCostPerVehicle;
    }

    /**
     * Access method for financePerVehicle.
     *
     * @return the current value of financePerVehicle
     */
    public float getFinancePerVehicle() {
        return financePerVehicle;
    }

    /**
     * Setter method for financePerVehicle.
     *
     * @param aFinancePerVehicle the new value for financePerVehicle
     */
    public void setFinancePerVehicle(float aFinancePerVehicle) {
        financePerVehicle = aFinancePerVehicle;
    }

    /**
     * Access method for totalCost.
     *
     * @return the current value of totalCost
     */
    public float getTotalCost() {
        return totalCost;
    }

    /**
     * Setter method for totalCost.
     *
     * @param aTotalCost the new value for totalCost
     */
    public void setTotalCost(float aTotalCost) {
        totalCost = aTotalCost;
    }

    /**
     * Access method for totalFinanceAmount.
     *
     * @return the current value of totalFinanceAmount
     */
    public float getTotalFinanceAmount() {
        return totalFinanceAmount;
    }

    /**
     * Setter method for totalFinanceAmount.
     *
     * @param aTotalFinanceAmount the new value for totalFinanceAmount
     */
    public void setTotalFinanceAmount(float aTotalFinanceAmount) {
        totalFinanceAmount = aTotalFinanceAmount;
    }

    /**
     * Access method for vehicleUsage.
     *
     * @return the current value of vehicleUsage
     */
    public String getVehicleUsage() {
        return vehicleUsage;
    }

    /**
     * Setter method for vehicleUsage.
     *
     * @param aVehicleUsage the new value for vehicleUsage
     */
    public void setVehicleUsage(String aVehicleUsage) {
        vehicleUsage = aVehicleUsage;
    }

    /**
     * Access method for loanAmt.
     *
     * @return the current value of loanAmt
     */
    public float getLoanAmt() {
        return loanAmt;
    }

    /**
     * Setter method for loanAmt.
     *
     * @param aLoanAmt the new value for loanAmt
     */
    public void setLoanAmt(float aLoanAmt) {
        loanAmt = aLoanAmt;
    }

    /**
     * Access method for religion.
     *
     * @return the current value of religion
     */
    public String getReligion() {
        return religion;
    }

    /**
     * Setter method for religion.
     *
     * @param aReligion the new value for religion
     */
    public void setReligion(String aReligion) {
        religion = aReligion;
    }

    /**
     * Access method for caste.
     *
     * @return the current value of caste
     */
    public String getCaste() {
        return caste;
    }

    /**
     * Setter method for caste.
     *
     * @param aCaste the new value for caste
     */
    public void setCaste(String aCaste) {
        caste = aCaste;
    }

    /**
     * Access method for ref1Name.
     *
     * @return the current value of ref1Name
     */
    public String getRef1Name() {
        return ref1Name;
    }

    /**
     * Setter method for ref1Name.
     *
     * @param aRef1Name the new value for ref1Name
     */
    public void setRef1Name(String aRef1Name) {
        ref1Name = aRef1Name;
    }

    /**
     * Access method for ref1Relationship.
     *
     * @return the current value of ref1Relationship
     */
    public String getRef1Relationship() {
        return ref1Relationship;
    }

    /**
     * Setter method for ref1Relationship.
     *
     * @param aRef1Relationship the new value for ref1Relationship
     */
    public void setRef1Relationship(String aRef1Relationship) {
        ref1Relationship = aRef1Relationship;
    }

    /**
     * Access method for ref1Mobileno.
     *
     * @return the current value of ref1Mobileno
     */
    public String getRef1Mobileno() {
        return ref1Mobileno;
    }

    /**
     * Setter method for ref1Mobileno.
     *
     * @param aRef1Mobileno the new value for ref1Mobileno
     */
    public void setRef1Mobileno(String aRef1Mobileno) {
        ref1Mobileno = aRef1Mobileno;
    }

    /**
     * Access method for ref2Name.
     *
     * @return the current value of ref2Name
     */
    public String getRef2Name() {
        return ref2Name;
    }

    /**
     * Setter method for ref2Name.
     *
     * @param aRef2Name the new value for ref2Name
     */
    public void setRef2Name(String aRef2Name) {
        ref2Name = aRef2Name;
    }

    /**
     * Access method for ref2Relationship.
     *
     * @return the current value of ref2Relationship
     */
    public String getRef2Relationship() {
        return ref2Relationship;
    }

    /**
     * Setter method for ref2Relationship.
     *
     * @param aRef2Relationship the new value for ref2Relationship
     */
    public void setRef2Relationship(String aRef2Relationship) {
        ref2Relationship = aRef2Relationship;
    }

    /**
     * Access method for ref2Mobileno.
     *
     * @return the current value of ref2Mobileno
     */
    public String getRef2Mobileno() {
        return ref2Mobileno;
    }

    /**
     * Setter method for ref2Mobileno.
     *
     * @param aRef2Mobileno the new value for ref2Mobileno
     */
    public void setRef2Mobileno(String aRef2Mobileno) {
        ref2Mobileno = aRef2Mobileno;
    }

    /**
     * Access method for guarantorAvail.
     *
     * @return the current value of guarantorAvail
     */
    public String getGuarantorAvail() {
        return guarantorAvail;
    }

    /**
     * Setter method for guarantorAvail.
     *
     * @param aGuarantorAvail the new value for guarantorAvail
     */
    public void setGuarantorAvail(String aGuarantorAvail) {
        guarantorAvail = aGuarantorAvail;
    }

    /**
     * Access method for relationshipWithHirer.
     *
     * @return the current value of relationshipWithHirer
     */
    public String getRelationshipWithHirer() {
        return relationshipWithHirer;
    }

    /**
     * Setter method for relationshipWithHirer.
     *
     * @param aRelationshipWithHirer the new value for relationshipWithHirer
     */
    public void setRelationshipWithHirer(String aRelationshipWithHirer) {
        relationshipWithHirer = aRelationshipWithHirer;
    }

    /**
     * Access method for guarantorCompleted.
     *
     * @return the current value of guarantorCompleted
     */
    public int getGuarantorCompleted() {
        return guarantorCompleted;
    }

    /**
     * Setter method for guarantorCompleted.
     *
     * @param aGuarantorCompleted the new value for guarantorCompleted
     */
    public void setGuarantorCompleted(int aGuarantorCompleted) {
        guarantorCompleted = aGuarantorCompleted;
    }

    /**
     * Access method for guaRetriggerName.
     *
     * @return the current value of guaRetriggerName
     */
    public String getGuaRetriggerName() {
        return guaRetriggerName;
    }

    /**
     * Setter method for guaRetriggerName.
     *
     * @param aGuaRetriggerName the new value for guaRetriggerName
     */
    public void setGuaRetriggerName(String aGuaRetriggerName) {
        guaRetriggerName = aGuaRetriggerName;
    }

    /**
     * Access method for appLatitude.
     *
     * @return the current value of appLatitude
     */
    public String getAppLatitude() {
        return appLatitude;
    }

    /**
     * Setter method for appLatitude.
     *
     * @param aAppLatitude the new value for appLatitude
     */
    public void setAppLatitude(String aAppLatitude) {
        appLatitude = aAppLatitude;
    }

    /**
     * Access method for appLongitude.
     *
     * @return the current value of appLongitude
     */
    public String getAppLongitude() {
        return appLongitude;
    }

    /**
     * Setter method for appLongitude.
     *
     * @param aAppLongitude the new value for appLongitude
     */
    public void setAppLongitude(String aAppLongitude) {
        appLongitude = aAppLongitude;
    }

    /**
     * Access method for appGpslocation.
     *
     * @return the current value of appGpslocation
     */
    public String getAppGpslocation() {
        return appGpslocation;
    }

    /**
     * Setter method for appGpslocation.
     *
     * @param aAppGpslocation the new value for appGpslocation
     */
    public void setAppGpslocation(String aAppGpslocation) {
        appGpslocation = aAppGpslocation;
    }

    /**
     * Access method for appCid.
     *
     * @return the current value of appCid
     */
    public String getAppCid() {
        return appCid;
    }

    /**
     * Setter method for appCid.
     *
     * @param aAppCid the new value for appCid
     */
    public void setAppCid(String aAppCid) {
        appCid = aAppCid;
    }

    /**
     * Access method for appLac.
     *
     * @return the current value of appLac
     */
    public String getAppLac() {
        return appLac;
    }

    /**
     * Setter method for appLac.
     *
     * @param aAppLac the new value for appLac
     */
    public void setAppLac(String aAppLac) {
        appLac = aAppLac;
    }

   /* *//**
     * Access method for appLat.
     *
     * @return the current value of appLat
     *//*
    public float getAppLat() {
        return appLat;
    }

    *//**
     * Setter method for appLat.
     *
     * @param aAppLat the new value for appLat
     *//*
    public void setAppLat(float aAppLat) {
        appLat = aAppLat;
    }

    *//**
     * Access method for appLong.
     *
     * @return the current value of appLong
     *//*
    public float getAppLong() {
        return appLong;
    }*/

   /* *//**
     * Setter method for appLong.
     *
     * @param aAppLong the new value for appLong
     *//*
    public void setAppLong(float aAppLong) {
        appLong = aAppLong;
    }

    *//**
     * Access method for guarantorLatitude.
     *
     * @return the current value of guarantorLatitude
     *//*
    public float getGuarantorLatitude() {
        return guarantorLatitude;
    }

    *//**
     * Setter method for guarantorLatitude.
     *
     * @param aGuarantorLatitude the new value for guarantorLatitude
     *//*
    public void setGuarantorLatitude(float aGuarantorLatitude) {
        guarantorLatitude = aGuarantorLatitude;
    }

    *//**
     * Access method for guarantorLongitude.
     *
     * @return the current value of guarantorLongitude
     *//*
    public float getGuarantorLongitude() {
        return guarantorLongitude;
    }

    *//**
     * Setter method for guarantorLongitude.
     *
     * @param aGuarantorLongitude the new value for guarantorLongitude
     *//*
    public void setGuarantorLongitude(float aGuarantorLongitude) {
        guarantorLongitude = aGuarantorLongitude;
    }*/

    /**
     * Access method for guarantorCid.
     *
     * @return the current value of guarantorCid
     */
    public String getGuarantorCid() {
        return guarantorCid;
    }

    /**
     * Setter method for guarantorCid.
     *
     * @param aGuarantorCid the new value for guarantorCid
     */
    public void setGuarantorCid(String aGuarantorCid) {
        guarantorCid = aGuarantorCid;
    }

    /**
     * Access method for guarantorLac.
     *
     * @return the current value of guarantorLac
     */
    public String getGuarantorLac() {
        return guarantorLac;
    }

    /**
     * Setter method for guarantorLac.
     *
     * @param aGuarantorLac the new value for guarantorLac
     */
    public void setGuarantorLac(String aGuarantorLac) {
        guarantorLac = aGuarantorLac;
    }

   /* *//**
     * Access method for guarantorLat.
     *
     * @return the current value of guarantorLat
     *//*
    public float getGuarantorLat() {
        return guarantorLat;
    }

    *//**
     * Setter method for guarantorLat.
     *
     * @param aGuarantorLat the new value for guarantorLat
     *//*
    public void setGuarantorLat(float aGuarantorLat) {
        guarantorLat = aGuarantorLat;
    }*/

   /* *//**
     * Access method for guarantorLong.
     *
     * @return the current value of guarantorLong
     *//*
    public float getGuarantorLong() {
        return guarantorLong;
    }

    *//**
     * Setter method for guarantorLong.
     *
     * @param aGuarantorLong the new value for guarantorLong
     *//*
    public void setGuarantorLong(float aGuarantorLong) {
        guarantorLong = aGuarantorLong;
    }
*/
    /**
     * Access method for status.
     *
     * @return the current value of status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Setter method for status.
     *
     * @param aStatus the new value for status
     */
    public void setStatus(String aStatus) {
        status = aStatus;
    }

    /**
     * Access method for resiVerified.
     *
     * @return the current value of resiVerified
     */
    public int getResiVerified() {
        return resiVerified;
    }

    /**
     * Setter method for resiVerified.
     *
     * @param aResiVerified the new value for resiVerified
     */
    public void setResiVerified(int aResiVerified) {
        resiVerified = aResiVerified;
    }

    /**
     * Access method for fiBusinessName.
     *
     * @return the current value of fiBusinessName
     */
    public String getFiBusinessName() {
        return fiBusinessName;
    }

    /**
     * Setter method for fiBusinessName.
     *
     * @param aFiBusinessName the new value for fiBusinessName
     */
    public void setFiBusinessName(String aFiBusinessName) {
        fiBusinessName = aFiBusinessName;
    }

    /**
     * Access method for resiFe.
     *
     * @return the current value of resiFe
     */
    public String getResiFe() {
        return resiFe;
    }

    /**
     * Setter method for resiFe.
     *
     * @param aResiFe the new value for resiFe
     */
    public void setResiFe(String aResiFe) {
        resiFe = aResiFe;
    }

    /**
     * Access method for resiFeAllocated.
     *
     * @return the current value of resiFeAllocated
     */
    public String getResiFeAllocated() {
        return resiFeAllocated;
    }

    /**
     * Setter method for resiFeAllocated.
     *
     * @param aResiFeAllocated the new value for resiFeAllocated
     */
    public void setResiFeAllocated(String aResiFeAllocated) {
        resiFeAllocated = aResiFeAllocated;
    }

    /**
     * Access method for resiVerifyTime.
     *
     * @return the current value of resiVerifyTime
     */
    public Timestamp getResiVerifyTime() {
        return resiVerifyTime;
    }

    /**
     * Setter method for resiVerifyTime.
     *
     * @param aResiVerifyTime the new value for resiVerifyTime
     */
    public void setResiVerifyTime(Timestamp aResiVerifyTime) {
        resiVerifyTime = aResiVerifyTime;
    }

    /**
     * Access method for fiResiFathername.
     *
     * @return the current value of fiResiFathername
     */
    public String getFiResiFathername() {
        return fiResiFathername;
    }

    /**
     * Setter method for fiResiFathername.
     *
     * @param aFiResiFathername the new value for fiResiFathername
     */
    public void setFiResiFathername(String aFiResiFathername) {
        fiResiFathername = aFiResiFathername;
    }

    /**
     * Access method for fiAddressConfirm.
     *
     * @return the current value of fiAddressConfirm
     */
    public String getFiAddressConfirm() {
        return fiAddressConfirm;
    }

    /**
     * Setter method for fiAddressConfirm.
     *
     * @param aFiAddressConfirm the new value for fiAddressConfirm
     */
    public void setFiAddressConfirm(String aFiAddressConfirm) {
        fiAddressConfirm = aFiAddressConfirm;
    }

    /**
     * Access method for fiProfile.
     *
     * @return the current value of fiProfile
     */
    public String getFiProfile() {
        return fiProfile;
    }

    /**
     * Setter method for fiProfile.
     *
     * @param aFiProfile the new value for fiProfile
     */
    public void setFiProfile(String aFiProfile) {
        fiProfile = aFiProfile;
    }

    /**
     * Access method for fiExperience.
     *
     * @return the current value of fiExperience
     */
    public int getFiExperience() {
        return fiExperience;
    }

    /**
     * Setter method for fiExperience.
     *
     * @param aFiExperience the new value for fiExperience
     */
    public void setFiExperience(int aFiExperience) {
        fiExperience = aFiExperience;
    }

    /**
     * Access method for fiSelf.
     *
     * @return the current value of fiSelf
     */
    public String getFiSelf() {
        return fiSelf;
    }

    /**
     * Setter method for fiSelf.
     *
     * @param aFiSelf the new value for fiSelf
     */
    public void setFiSelf(String aFiSelf) {
        fiSelf = aFiSelf;
    }

    /**
     * Access method for fiRelationship.
     *
     * @return the current value of fiRelationship
     */
    public String getFiRelationship() {
        return fiRelationship;
    }

    /**
     * Setter method for fiRelationship.
     *
     * @param aFiRelationship the new value for fiRelationship
     */
    public void setFiRelationship(String aFiRelationship) {
        fiRelationship = aFiRelationship;
    }

    /**
     * Access method for fiFamilyMember.
     *
     * @return the current value of fiFamilyMember
     */
    public String getFiFamilyMember() {
        return fiFamilyMember;
    }

    /**
     * Setter method for fiFamilyMember.
     *
     * @param aFiFamilyMember the new value for fiFamilyMember
     */
    public void setFiFamilyMember(String aFiFamilyMember) {
        fiFamilyMember = aFiFamilyMember;
    }

    /**
     * Access method for fiEarningMember.
     *
     * @return the current value of fiEarningMember
     */
    public String getFiEarningMember() {
        return fiEarningMember;
    }

    /**
     * Setter method for fiEarningMember.
     *
     * @param aFiEarningMember the new value for fiEarningMember
     */
    public void setFiEarningMember(String aFiEarningMember) {
        fiEarningMember = aFiEarningMember;
    }

    /**
     * Access method for fiStayingSince.
     *
     * @return the current value of fiStayingSince
     */
    public String getFiStayingSince() {
        return fiStayingSince;
    }

    /**
     * Setter method for fiStayingSince.
     *
     * @param aFiStayingSince the new value for fiStayingSince
     */
    public void setFiStayingSince(String aFiStayingSince) {
        fiStayingSince = aFiStayingSince;
    }

    /**
     * Access method for fiResidenceArea.
     *
     * @return the current value of fiResidenceArea
     */
    public String getFiResidenceArea() {
        return fiResidenceArea;
    }

    /**
     * Setter method for fiResidenceArea.
     *
     * @param aFiResidenceArea the new value for fiResidenceArea
     */
    public void setFiResidenceArea(String aFiResidenceArea) {
        fiResidenceArea = aFiResidenceArea;
    }

    /**
     * Access method for fiResidenceOwnership.
     *
     * @return the current value of fiResidenceOwnership
     */
    public String getFiResidenceOwnership() {
        return fiResidenceOwnership;
    }

    /**
     * Setter method for fiResidenceOwnership.
     *
     * @param aFiResidenceOwnership the new value for fiResidenceOwnership
     */
    public void setFiResidenceOwnership(String aFiResidenceOwnership) {
        fiResidenceOwnership = aFiResidenceOwnership;
    }

    /**
     * Access method for fiResidenceOwnerName.
     *
     * @return the current value of fiResidenceOwnerName
     */
    public String getFiResidenceOwnerName() {
        return fiResidenceOwnerName;
    }

    /**
     * Setter method for fiResidenceOwnerName.
     *
     * @param aFiResidenceOwnerName the new value for fiResidenceOwnerName
     */
    public void setFiResidenceOwnerName(String aFiResidenceOwnerName) {
        fiResidenceOwnerName = aFiResidenceOwnerName;
    }

    /**
     * Access method for fiRentAmount.
     *
     * @return the current value of fiRentAmount
     */
    public String getFiRentAmount() {
        return fiRentAmount;
    }

    /**
     * Setter method for fiRentAmount.
     *
     * @param aFiRentAmount the new value for fiRentAmount
     */
    public void setFiRentAmount(String aFiRentAmount) {
        fiRentAmount = aFiRentAmount;
    }

    /**
     * Access method for fiIsPermanentAddress.
     *
     * @return the current value of fiIsPermanentAddress
     */
    public String getFiIsPermanentAddress() {
        return fiIsPermanentAddress;
    }

    /**
     * Setter method for fiIsPermanentAddress.
     *
     * @param aFiIsPermanentAddress the new value for fiIsPermanentAddress
     */
    public void setFiIsPermanentAddress(String aFiIsPermanentAddress) {
        fiIsPermanentAddress = aFiIsPermanentAddress;
    }

    /**
     * Access method for fiResiType.
     *
     * @return the current value of fiResiType
     */
    public String getFiResiType() {
        return fiResiType;
    }

    /**
     * Setter method for fiResiType.
     *
     * @param aFiResiType the new value for fiResiType
     */
    public void setFiResiType(String aFiResiType) {
        fiResiType = aFiResiType;
    }

    /**
     * Access method for fiResiConstruction.
     *
     * @return the current value of fiResiConstruction
     */
    public String getFiResiConstruction() {
        return fiResiConstruction;
    }

    /**
     * Setter method for fiResiConstruction.
     *
     * @param aFiResiConstruction the new value for fiResiConstruction
     */
    public void setFiResiConstruction(String aFiResiConstruction) {
        fiResiConstruction = aFiResiConstruction;
    }

    /**
     * Access method for fiAccessibility.
     *
     * @return the current value of fiAccessibility
     */
    public String getFiAccessibility() {
        return fiAccessibility;
    }

    /**
     * Setter method for fiAccessibility.
     *
     * @param aFiAccessibility the new value for fiAccessibility
     */
    public void setFiAccessibility(String aFiAccessibility) {
        fiAccessibility = aFiAccessibility;
    }

    /**
     * Access method for fiResiLocality.
     *
     * @return the current value of fiResiLocality
     */
    public String getFiResiLocality() {
        return fiResiLocality;
    }

    /**
     * Setter method for fiResiLocality.
     *
     * @param aFiResiLocality the new value for fiResiLocality
     */
    public void setFiResiLocality(String aFiResiLocality) {
        fiResiLocality = aFiResiLocality;
    }

    /**
     * Access method for fiInterior.
     *
     * @return the current value of fiInterior
     */
    public String getFiInterior() {
        return fiInterior;
    }

    /**
     * Setter method for fiInterior.
     *
     * @param aFiInterior the new value for fiInterior
     */
    public void setFiInterior(String aFiInterior) {
        fiInterior = aFiInterior;
    }

    /**
     * Access method for fiExterior.
     *
     * @return the current value of fiExterior
     */
    public String getFiExterior() {
        return fiExterior;
    }

    /**
     * Setter method for fiExterior.
     *
     * @param aFiExterior the new value for fiExterior
     */
    public void setFiExterior(String aFiExterior) {
        fiExterior = aFiExterior;
    }

    /**
     * Access method for fiAssets.
     *
     * @return the current value of fiAssets
     */
    public String getFiAssets() {
        return fiAssets;
    }

    /**
     * Setter method for fiAssets.
     *
     * @param aFiAssets the new value for fiAssets
     */
    public void setFiAssets(String aFiAssets) {
        fiAssets = aFiAssets;
    }

    /**
     * Access method for fiEducation.
     *
     * @return the current value of fiEducation
     */
    public String getFiEducation() {
        return fiEducation;
    }

    /**
     * Setter method for fiEducation.
     *
     * @param aFiEducation the new value for fiEducation
     */
    public void setFiEducation(String aFiEducation) {
        fiEducation = aFiEducation;
    }

    /**
     * Access method for fiMaritialStatus.
     *
     * @return the current value of fiMaritialStatus
     */
    public String getFiMaritialStatus() {
        return fiMaritialStatus;
    }

    /**
     * Setter method for fiMaritialStatus.
     *
     * @param aFiMaritialStatus the new value for fiMaritialStatus
     */
    public void setFiMaritialStatus(String aFiMaritialStatus) {
        fiMaritialStatus = aFiMaritialStatus;
    }

    /**
     * Access method for fiAccommodation.
     *
     * @return the current value of fiAccommodation
     */
    public String getFiAccommodation() {
        return fiAccommodation;
    }

    /**
     * Setter method for fiAccommodation.
     *
     * @param aFiAccommodation the new value for fiAccommodation
     */
    public void setFiAccommodation(String aFiAccommodation) {
        fiAccommodation = aFiAccommodation;
    }

    /**
     * Access method for fiResiArea.
     *
     * @return the current value of fiResiArea
     */
    public String getFiResiArea() {
        return fiResiArea;
    }

    /**
     * Setter method for fiResiArea.
     *
     * @param aFiResiArea the new value for fiResiArea
     */
    public void setFiResiArea(String aFiResiArea) {
        fiResiArea = aFiResiArea;
    }

    /**
     * Access method for fiNeighbourChk.
     *
     * @return the current value of fiNeighbourChk
     */
    public String getFiNeighbourChk() {
        return fiNeighbourChk;
    }

    /**
     * Setter method for fiNeighbourChk.
     *
     * @param aFiNeighbourChk the new value for fiNeighbourChk
     */
    public void setFiNeighbourChk(String aFiNeighbourChk) {
        fiNeighbourChk = aFiNeighbourChk;
    }

    /**
     * Access method for fiRef1Name.
     *
     * @return the current value of fiRef1Name
     */
    public String getFiRef1Name() {
        return fiRef1Name;
    }

    /**
     * Setter method for fiRef1Name.
     *
     * @param aFiRef1Name the new value for fiRef1Name
     */
    public void setFiRef1Name(String aFiRef1Name) {
        fiRef1Name = aFiRef1Name;
    }

    /**
     * Access method for fiRef2Name.
     *
     * @return the current value of fiRef2Name
     */
    public String getFiRef2Name() {
        return fiRef2Name;
    }

    /**
     * Setter method for fiRef2Name.
     *
     * @param aFiRef2Name the new value for fiRef2Name
     */
    public void setFiRef2Name(String aFiRef2Name) {
        fiRef2Name = aFiRef2Name;
    }

    /**
     * Access method for fiCpvStatus.
     *
     * @return the current value of fiCpvStatus
     */
    public String getFiCpvStatus() {
        return fiCpvStatus;
    }

    /**
     * Setter method for fiCpvStatus.
     *
     * @param aFiCpvStatus the new value for fiCpvStatus
     */
    public void setFiCpvStatus(String aFiCpvStatus) {
        fiCpvStatus = aFiCpvStatus;
    }

    /**
     * Access method for fiRemark.
     *
     * @return the current value of fiRemark
     */
    public String getFiRemark() {
        return fiRemark;
    }

    /**
     * Setter method for fiRemark.
     *
     * @param aFiRemark the new value for fiRemark
     */
    public void setFiRemark(String aFiRemark) {
        fiRemark = aFiRemark;
    }

    /**
     * Access method for fiDisFromBranch.
     *
     * @return the current value of fiDisFromBranch
     */
    public String getFiDisFromBranch() {
        return fiDisFromBranch;
    }

    /**
     * Setter method for fiDisFromBranch.
     *
     * @param aFiDisFromBranch the new value for fiDisFromBranch
     */
    public void setFiDisFromBranch(String aFiDisFromBranch) {
        fiDisFromBranch = aFiDisFromBranch;
    }

    /**
     * Access method for fiVerificationName.
     *
     * @return the current value of fiVerificationName
     */
    public String getFiVerificationName() {
        return fiVerificationName;
    }

    /**
     * Setter method for fiVerificationName.
     *
     * @param aFiVerificationName the new value for fiVerificationName
     */
    public void setFiVerificationName(String aFiVerificationName) {
        fiVerificationName = aFiVerificationName;
    }

    /**
     * Access method for fiResiAddr.
     *
     * @return the current value of fiResiAddr
     */
    public String getFiResiAddr() {
        return fiResiAddr;
    }

    /**
     * Setter method for fiResiAddr.
     *
     * @param aFiResiAddr the new value for fiResiAddr
     */
    public void setFiResiAddr(String aFiResiAddr) {
        fiResiAddr = aFiResiAddr;
    }

    /**
     * Access method for fiResiAddr2.
     *
     * @return the current value of fiResiAddr2
     */
    public String getFiResiAddr2() {
        return fiResiAddr2;
    }

    /**
     * Setter method for fiResiAddr2.
     *
     * @param aFiResiAddr2 the new value for fiResiAddr2
     */
    public void setFiResiAddr2(String aFiResiAddr2) {
        fiResiAddr2 = aFiResiAddr2;
    }

    /**
     * Access method for fiResiCity.
     *
     * @return the current value of fiResiCity
     */
    public String getFiResiCity() {
        return fiResiCity;
    }

    /**
     * Setter method for fiResiCity.
     *
     * @param aFiResiCity the new value for fiResiCity
     */
    public void setFiResiCity(String aFiResiCity) {
        fiResiCity = aFiResiCity;
    }

    /**
     * Access method for fiResiState.
     *
     * @return the current value of fiResiState
     */
    public String getFiResiState() {
        return fiResiState;
    }

    /**
     * Setter method for fiResiState.
     *
     * @param aFiResiState the new value for fiResiState
     */
    public void setFiResiState(String aFiResiState) {
        fiResiState = aFiResiState;
    }

    /**
     * Access method for fiResiPincode.
     *
     * @return the current value of fiResiPincode
     */
    public String getFiResiPincode() {
        return fiResiPincode;
    }

    /**
     * Setter method for fiResiPincode.
     *
     * @param aFiResiPincode the new value for fiResiPincode
     */
    public void setFiResiPincode(String aFiResiPincode) {
        fiResiPincode = aFiResiPincode;
    }

    /**
     * Access method for fiResiPh.
     *
     * @return the current value of fiResiPh
     */
    public String getFiResiPh() {
        return fiResiPh;
    }

    /**
     * Setter method for fiResiPh.
     *
     * @param aFiResiPh the new value for fiResiPh
     */
    public void setFiResiPh(String aFiResiPh) {
        fiResiPh = aFiResiPh;
    }

    /**
     * Access method for resiMobile.
     *
     * @return the current value of resiMobile
     */
    public String getResiMobile() {
        return resiMobile;
    }

    /**
     * Setter method for resiMobile.
     *
     * @param aResiMobile the new value for resiMobile
     */
    public void setResiMobile(String aResiMobile) {
        resiMobile = aResiMobile;
    }

    /**
     * Access method for fiResiPermAddress.
     *
     * @return the current value of fiResiPermAddress
     */
    public String getFiResiPermAddress() {
        return fiResiPermAddress;
    }

    /**
     * Setter method for fiResiPermAddress.
     *
     * @param aFiResiPermAddress the new value for fiResiPermAddress
     */
    public void setFiResiPermAddress(String aFiResiPermAddress) {
        fiResiPermAddress = aFiResiPermAddress;
    }

    /**
     * Access method for fiResiPermAddress2.
     *
     * @return the current value of fiResiPermAddress2
     */
    public String getFiResiPermAddress2() {
        return fiResiPermAddress2;
    }

    /**
     * Setter method for fiResiPermAddress2.
     *
     * @param aFiResiPermAddress2 the new value for fiResiPermAddress2
     */
    public void setFiResiPermAddress2(String aFiResiPermAddress2) {
        fiResiPermAddress2 = aFiResiPermAddress2;
    }

    /**
     * Access method for fiResiPermCity.
     *
     * @return the current value of fiResiPermCity
     */
    public String getFiResiPermCity() {
        return fiResiPermCity;
    }

    /**
     * Setter method for fiResiPermCity.
     *
     * @param aFiResiPermCity the new value for fiResiPermCity
     */
    public void setFiResiPermCity(String aFiResiPermCity) {
        fiResiPermCity = aFiResiPermCity;
    }

    /**
     * Access method for fiResiPermState.
     *
     * @return the current value of fiResiPermState
     */
    public String getFiResiPermState() {
        return fiResiPermState;
    }

    /**
     * Setter method for fiResiPermState.
     *
     * @param aFiResiPermState the new value for fiResiPermState
     */
    public void setFiResiPermState(String aFiResiPermState) {
        fiResiPermState = aFiResiPermState;
    }

    /**
     * Access method for fiResiPermPincode.
     *
     * @return the current value of fiResiPermPincode
     */
    public String getFiResiPermPincode() {
        return fiResiPermPincode;
    }

    /**
     * Setter method for fiResiPermPincode.
     *
     * @param aFiResiPermPincode the new value for fiResiPermPincode
     */
    public void setFiResiPermPincode(String aFiResiPermPincode) {
        fiResiPermPincode = aFiResiPermPincode;
    }

    /**
     * Access method for fiResiPermLandmark.
     *
     * @return the current value of fiResiPermLandmark
     */
    public String getFiResiPermLandmark() {
        return fiResiPermLandmark;
    }

    /**
     * Setter method for fiResiPermLandmark.
     *
     * @param aFiResiPermLandmark the new value for fiResiPermLandmark
     */
    public void setFiResiPermLandmark(String aFiResiPermLandmark) {
        fiResiPermLandmark = aFiResiPermLandmark;
    }

    /**
     * Access method for age.
     *
     * @return the current value of age
     */
    public int getAge() {
        return age;
    }

    /**
     * Setter method for age.
     *
     * @param aAge the new value for age
     */
    public void setAge(int aAge) {
        age = aAge;
    }

    /**
     * Access method for maritalStatus.
     *
     * @return the current value of maritalStatus
     */
    public String getMaritalStatus() {
        return maritalStatus;
    }

    /**
     * Setter method for maritalStatus.
     *
     * @param aMaritalStatus the new value for maritalStatus
     */
    public void setMaritalStatus(String aMaritalStatus) {
        maritalStatus = aMaritalStatus;
    }

    /**
     * Access method for famAware.
     *
     * @return the current value of famAware
     */
    public String getFamAware() {
        return famAware;
    }

    /**
     * Setter method for famAware.
     *
     * @param aFamAware the new value for famAware
     */
    public void setFamAware(String aFamAware) {
        famAware = aFamAware;
    }

    /**
     * Access method for resiOwnhouse.
     *
     * @return the current value of resiOwnhouse
     */
    public String getResiOwnhouse() {
        return resiOwnhouse;
    }

    /**
     * Setter method for resiOwnhouse.
     *
     * @param aResiOwnhouse the new value for resiOwnhouse
     */
    public void setResiOwnhouse(String aResiOwnhouse) {
        resiOwnhouse = aResiOwnhouse;
    }

    /**
     * Access method for resiLocality.
     *
     * @return the current value of resiLocality
     */
    public String getResiLocality() {
        return resiLocality;
    }

    /**
     * Setter method for resiLocality.
     *
     * @param aResiLocality the new value for resiLocality
     */
    public void setResiLocality(String aResiLocality) {
        resiLocality = aResiLocality;
    }

    /**
     * Access method for resiInterior.
     *
     * @return the current value of resiInterior
     */
    public String getResiInterior() {
        return resiInterior;
    }

    /**
     * Setter method for resiInterior.
     *
     * @param aResiInterior the new value for resiInterior
     */
    public void setResiInterior(String aResiInterior) {
        resiInterior = aResiInterior;
    }

    /**
     * Access method for resiLocatability.
     *
     * @return the current value of resiLocatability
     */
    public String getResiLocatability() {
        return resiLocatability;
    }

    /**
     * Setter method for resiLocatability.
     *
     * @param aResiLocatability the new value for resiLocatability
     */
    public void setResiLocatability(String aResiLocatability) {
        resiLocatability = aResiLocatability;
    }

    /**
     * Access method for resiLandmark.
     *
     * @return the current value of resiLandmark
     */
    public String getResiLandmark() {
        return resiLandmark;
    }

    /**
     * Setter method for resiLandmark.
     *
     * @param aResiLandmark the new value for resiLandmark
     */
    public void setResiLandmark(String aResiLandmark) {
        resiLandmark = aResiLandmark;
    }

    /**
     * Access method for resiRight.
     *
     * @return the current value of resiRight
     */
    public String getResiRight() {
        return resiRight;
    }

    /**
     * Setter method for resiRight.
     *
     * @param aResiRight the new value for resiRight
     */
    public void setResiRight(String aResiRight) {
        resiRight = aResiRight;
    }

    /**
     * Access method for resiLeft.
     *
     * @return the current value of resiLeft
     */
    public String getResiLeft() {
        return resiLeft;
    }

    /**
     * Setter method for resiLeft.
     *
     * @param aResiLeft the new value for resiLeft
     */
    public void setResiLeft(String aResiLeft) {
        resiLeft = aResiLeft;
    }

    /**
     * Access method for resiFront.
     *
     * @return the current value of resiFront
     */
    public String getResiFront() {
        return resiFront;
    }

    /**
     * Setter method for resiFront.
     *
     * @param aResiFront the new value for resiFront
     */
    public void setResiFront(String aResiFront) {
        resiFront = aResiFront;
    }

    /**
     * Access method for resiRear.
     *
     * @return the current value of resiRear
     */
    public String getResiRear() {
        return resiRear;
    }

    /**
     * Setter method for resiRear.
     *
     * @param aResiRear the new value for resiRear
     */
    public void setResiRear(String aResiRear) {
        resiRear = aResiRear;
    }

    /**
     * Access method for resiYears.
     *
     * @return the current value of resiYears
     */
    public int getResiYears() {
        return resiYears;
    }

    /**
     * Setter method for resiYears.
     *
     * @param aResiYears the new value for resiYears
     */
    public void setResiYears(int aResiYears) {
        resiYears = aResiYears;
    }

    /**
     * Access method for resiDistance.
     *
     * @return the current value of resiDistance
     */
    public int getResiDistance() {
        return resiDistance;
    }

    /**
     * Setter method for resiDistance.
     *
     * @param aResiDistance the new value for resiDistance
     */
    public void setResiDistance(int aResiDistance) {
        resiDistance = aResiDistance;
    }

    /**
     * Access method for resiPerCur.
     *
     * @return the current value of resiPerCur
     */
    public String getResiPerCur() {
        return resiPerCur;
    }

    /**
     * Setter method for resiPerCur.
     *
     * @param aResiPerCur the new value for resiPerCur
     */
    public void setResiPerCur(String aResiPerCur) {
        resiPerCur = aResiPerCur;
    }

    /**
     * Access method for resiDependents.
     *
     * @return the current value of resiDependents
     */
    public String getResiDependents() {
        return resiDependents;
    }

    /**
     * Setter method for resiDependents.
     *
     * @param aResiDependents the new value for resiDependents
     */
    public void setResiDependents(String aResiDependents) {
        resiDependents = aResiDependents;
    }

    /**
     * Access method for resiLoanAmount.
     *
     * @return the current value of resiLoanAmount
     */
    public String getResiLoanAmount() {
        return resiLoanAmount;
    }

    /**
     * Setter method for resiLoanAmount.
     *
     * @param aResiLoanAmount the new value for resiLoanAmount
     */
    public void setResiLoanAmount(String aResiLoanAmount) {
        resiLoanAmount = aResiLoanAmount;
    }

    /**
     * Access method for prodDelivered.
     *
     * @return the current value of prodDelivered
     */
    public String getProdDelivered() {
        return prodDelivered;
    }

    /**
     * Setter method for prodDelivered.
     *
     * @param aProdDelivered the new value for prodDelivered
     */
    public void setProdDelivered(String aProdDelivered) {
        prodDelivered = aProdDelivered;
    }

    /**
     * Access method for regnoVehi.
     *
     * @return the current value of regnoVehi
     */
    public String getRegnoVehi() {
        return regnoVehi;
    }

    /**
     * Setter method for regnoVehi.
     *
     * @param aRegnoVehi the new value for regnoVehi
     */
    public void setRegnoVehi(String aRegnoVehi) {
        regnoVehi = aRegnoVehi;
    }

    /**
     * Access method for productMatch.
     *
     * @return the current value of productMatch
     */
    public String getProductMatch() {
        return productMatch;
    }

    /**
     * Setter method for productMatch.
     *
     * @param aProductMatch the new value for productMatch
     */
    public void setProductMatch(String aProductMatch) {
        productMatch = aProductMatch;
    }

    /**
     * Access method for resiNetearning.
     *
     * @return the current value of resiNetearning
     */
    public long getResiNetearning() {
        return resiNetearning;
    }

    /**
     * Setter method for resiNetearning.
     *
     * @param aResiNetearning the new value for resiNetearning
     */
    public void setResiNetearning(long aResiNetearning) {
        resiNetearning = aResiNetearning;
    }

    /**
     * Access method for resiFamIncome.
     *
     * @return the current value of resiFamIncome
     */
    public long getResiFamIncome() {
        return resiFamIncome;
    }

    /**
     * Setter method for resiFamIncome.
     *
     * @param aResiFamIncome the new value for resiFamIncome
     */
    public void setResiFamIncome(long aResiFamIncome) {
        resiFamIncome = aResiFamIncome;
    }

    /**
     * Access method for resiResicumoff.
     *
     * @return the current value of resiResicumoff
     */
    public String getResiResicumoff() {
        return resiResicumoff;
    }

    /**
     * Setter method for resiResicumoff.
     *
     * @param aResiResicumoff the new value for resiResicumoff
     */
    public void setResiResicumoff(String aResiResicumoff) {
        resiResicumoff = aResiResicumoff;
    }

    /**
     * Access method for resiPrevLoan.
     *
     * @return the current value of resiPrevLoan
     */
    public String getResiPrevLoan() {
        return resiPrevLoan;
    }

    /**
     * Setter method for resiPrevLoan.
     *
     * @param aResiPrevLoan the new value for resiPrevLoan
     */
    public void setResiPrevLoan(String aResiPrevLoan) {
        resiPrevLoan = aResiPrevLoan;
    }

    /**
     * Access method for resiPrevLoanbank.
     *
     * @return the current value of resiPrevLoanbank
     */
    public String getResiPrevLoanbank() {
        return resiPrevLoanbank;
    }

    /**
     * Setter method for resiPrevLoanbank.
     *
     * @param aResiPrevLoanbank the new value for resiPrevLoanbank
     */
    public void setResiPrevLoanbank(String aResiPrevLoanbank) {
        resiPrevLoanbank = aResiPrevLoanbank;
    }

    /**
     * Access method for kyc.
     *
     * @return the current value of kyc
     */
    public String getKyc() {
        return kyc;
    }

    /**
     * Setter method for kyc.
     *
     * @param aKyc the new value for kyc
     */
    public void setKyc(String aKyc) {
        kyc = aKyc;
    }

    /**
     * Access method for resiPanno.
     *
     * @return the current value of resiPanno
     */
    public String getResiPanno() {
        return resiPanno;
    }

    /**
     * Setter method for resiPanno.
     *
     * @param aResiPanno the new value for resiPanno
     */
    public void setResiPanno(String aResiPanno) {
        resiPanno = aResiPanno;
    }

    /**
     * Access method for resiPassportNo.
     *
     * @return the current value of resiPassportNo
     */
    public String getResiPassportNo() {
        return resiPassportNo;
    }

    /**
     * Setter method for resiPassportNo.
     *
     * @param aResiPassportNo the new value for resiPassportNo
     */
    public void setResiPassportNo(String aResiPassportNo) {
        resiPassportNo = aResiPassportNo;
    }

    /**
     * Access method for resiVoterid.
     *
     * @return the current value of resiVoterid
     */
    public String getResiVoterid() {
        return resiVoterid;
    }

    /**
     * Setter method for resiVoterid.
     *
     * @param aResiVoterid the new value for resiVoterid
     */
    public void setResiVoterid(String aResiVoterid) {
        resiVoterid = aResiVoterid;
    }

    /**
     * Access method for resiDrivingLicense.
     *
     * @return the current value of resiDrivingLicense
     */
    public String getResiDrivingLicense() {
        return resiDrivingLicense;
    }

    /**
     * Setter method for resiDrivingLicense.
     *
     * @param aResiDrivingLicense the new value for resiDrivingLicense
     */
    public void setResiDrivingLicense(String aResiDrivingLicense) {
        resiDrivingLicense = aResiDrivingLicense;
    }

    /**
     * Access method for resiRationCard.
     *
     * @return the current value of resiRationCard
     */
    public String getResiRationCard() {
        return resiRationCard;
    }

    /**
     * Setter method for resiRationCard.
     *
     * @param aResiRationCard the new value for resiRationCard
     */
    public void setResiRationCard(String aResiRationCard) {
        resiRationCard = aResiRationCard;
    }

    /**
     * Access method for resiAadhar.
     *
     * @return the current value of resiAadhar
     */
    public String getResiAadhar() {
        return resiAadhar;
    }

    /**
     * Setter method for resiAadhar.
     *
     * @param aResiAadhar the new value for resiAadhar
     */
    public void setResiAadhar(String aResiAadhar) {
        resiAadhar = aResiAadhar;
    }

    /**
     * Access method for resiPostofficeNo.
     *
     * @return the current value of resiPostofficeNo
     */
    public String getResiPostofficeNo() {
        return resiPostofficeNo;
    }

    /**
     * Setter method for resiPostofficeNo.
     *
     * @param aResiPostofficeNo the new value for resiPostofficeNo
     */
    public void setResiPostofficeNo(String aResiPostofficeNo) {
        resiPostofficeNo = aResiPostofficeNo;
    }

    /**
     * Access method for resiNeigh1Name.
     *
     * @return the current value of resiNeigh1Name
     */
    public String getResiNeigh1Name() {
        return resiNeigh1Name;
    }

    /**
     * Setter method for resiNeigh1Name.
     *
     * @param aResiNeigh1Name the new value for resiNeigh1Name
     */
    public void setResiNeigh1Name(String aResiNeigh1Name) {
        resiNeigh1Name = aResiNeigh1Name;
    }

    /**
     * Access method for resiNeigh1Mobileno.
     *
     * @return the current value of resiNeigh1Mobileno
     */
    public String getResiNeigh1Mobileno() {
        return resiNeigh1Mobileno;
    }

    /**
     * Setter method for resiNeigh1Mobileno.
     *
     * @param aResiNeigh1Mobileno the new value for resiNeigh1Mobileno
     */
    public void setResiNeigh1Mobileno(String aResiNeigh1Mobileno) {
        resiNeigh1Mobileno = aResiNeigh1Mobileno;
    }

    /**
     * Access method for resiNeigh1.
     *
     * @return the current value of resiNeigh1
     */
    public String getResiNeigh1() {
        return resiNeigh1;
    }

    /**
     * Setter method for resiNeigh1.
     *
     * @param aResiNeigh1 the new value for resiNeigh1
     */
    public void setResiNeigh1(String aResiNeigh1) {
        resiNeigh1 = aResiNeigh1;
    }

    /**
     * Access method for resiNeigh2Name.
     *
     * @return the current value of resiNeigh2Name
     */
    public String getResiNeigh2Name() {
        return resiNeigh2Name;
    }

    /**
     * Setter method for resiNeigh2Name.
     *
     * @param aResiNeigh2Name the new value for resiNeigh2Name
     */
    public void setResiNeigh2Name(String aResiNeigh2Name) {
        resiNeigh2Name = aResiNeigh2Name;
    }

    /**
     * Access method for resiNeigh2Mobileno.
     *
     * @return the current value of resiNeigh2Mobileno
     */
    public String getResiNeigh2Mobileno() {
        return resiNeigh2Mobileno;
    }

    /**
     * Setter method for resiNeigh2Mobileno.
     *
     * @param aResiNeigh2Mobileno the new value for resiNeigh2Mobileno
     */
    public void setResiNeigh2Mobileno(String aResiNeigh2Mobileno) {
        resiNeigh2Mobileno = aResiNeigh2Mobileno;
    }

    /**
     * Access method for resiNeigh2.
     *
     * @return the current value of resiNeigh2
     */
    public String getResiNeigh2() {
        return resiNeigh2;
    }

    /**
     * Setter method for resiNeigh2.
     *
     * @param aResiNeigh2 the new value for resiNeigh2
     */
    public void setResiNeigh2(String aResiNeigh2) {
        resiNeigh2 = aResiNeigh2;
    }

    /**
     * Access method for resiRef1Name.
     *
     * @return the current value of resiRef1Name
     */
    public String getResiRef1Name() {
        return resiRef1Name;
    }

    /**
     * Setter method for resiRef1Name.
     *
     * @param aResiRef1Name the new value for resiRef1Name
     */
    public void setResiRef1Name(String aResiRef1Name) {
        resiRef1Name = aResiRef1Name;
    }

    /**
     * Access method for resiRef1Mobileno.
     *
     * @return the current value of resiRef1Mobileno
     */
    public String getResiRef1Mobileno() {
        return resiRef1Mobileno;
    }

    /**
     * Setter method for resiRef1Mobileno.
     *
     * @param aResiRef1Mobileno the new value for resiRef1Mobileno
     */
    public void setResiRef1Mobileno(String aResiRef1Mobileno) {
        resiRef1Mobileno = aResiRef1Mobileno;
    }

    /**
     * Access method for resiRef2Name.
     *
     * @return the current value of resiRef2Name
     */
    public String getResiRef2Name() {
        return resiRef2Name;
    }

    /**
     * Setter method for resiRef2Name.
     *
     * @param aResiRef2Name the new value for resiRef2Name
     */
    public void setResiRef2Name(String aResiRef2Name) {
        resiRef2Name = aResiRef2Name;
    }

    /**
     * Access method for resiRef2Mobileno.
     *
     * @return the current value of resiRef2Mobileno
     */
    public String getResiRef2Mobileno() {
        return resiRef2Mobileno;
    }

    /**
     * Setter method for resiRef2Mobileno.
     *
     * @param aResiRef2Mobileno the new value for resiRef2Mobileno
     */
    public void setResiRef2Mobileno(String aResiRef2Mobileno) {
        resiRef2Mobileno = aResiRef2Mobileno;
    }

    /**
     * Access method for resiRefcodeEcn.
     *
     * @return the current value of resiRefcodeEcn
     */
    public String getResiRefcodeEcn() {
        return resiRefcodeEcn;
    }

    /**
     * Setter method for resiRefcodeEcn.
     *
     * @param aResiRefcodeEcn the new value for resiRefcodeEcn
     */
    public void setResiRefcodeEcn(String aResiRefcodeEcn) {
        resiRefcodeEcn = aResiRefcodeEcn;
    }

    /**
     * Access method for resiPersonMet.
     *
     * @return the current value of resiPersonMet
     */
    public String getResiPersonMet() {
        return resiPersonMet;
    }

    /**
     * Setter method for resiPersonMet.
     *
     * @param aResiPersonMet the new value for resiPersonMet
     */
    public void setResiPersonMet(String aResiPersonMet) {
        resiPersonMet = aResiPersonMet;
    }

    /**
     * Access method for resiOccupation.
     *
     * @return the current value of resiOccupation
     */
    public String getResiOccupation() {
        return resiOccupation;
    }

    /**
     * Setter method for resiOccupation.
     *
     * @param aResiOccupation the new value for resiOccupation
     */
    public void setResiOccupation(String aResiOccupation) {
        resiOccupation = aResiOccupation;
    }

    /**
     * Access method for resiAttempts.
     *
     * @return the current value of resiAttempts
     */
    public int getResiAttempts() {
        return resiAttempts;
    }

    /**
     * Setter method for resiAttempts.
     *
     * @param aResiAttempts the new value for resiAttempts
     */
    public void setResiAttempts(int aResiAttempts) {
        resiAttempts = aResiAttempts;
    }

    /**
     * Access method for resiObservation.
     *
     * @return the current value of resiObservation
     */
    public String getResiObservation() {
        return resiObservation;
    }

    /**
     * Setter method for resiObservation.
     *
     * @param aResiObservation the new value for resiObservation
     */
    public void setResiObservation(String aResiObservation) {
        resiObservation = aResiObservation;
    }

    /**
     * Access method for resiBehaviour.
     *
     * @return the current value of resiBehaviour
     */
    public String getResiBehaviour() {
        return resiBehaviour;
    }

    /**
     * Setter method for resiBehaviour.
     *
     * @param aResiBehaviour the new value for resiBehaviour
     */
    public void setResiBehaviour(String aResiBehaviour) {
        resiBehaviour = aResiBehaviour;
    }

    /**
     * Access method for resiRecomendation.
     *
     * @return the current value of resiRecomendation
     */
    public String getResiRecomendation() {
        return resiRecomendation;
    }

    /**
     * Setter method for resiRecomendation.
     *
     * @param aResiRecomendation the new value for resiRecomendation
     */
    public void setResiRecomendation(String aResiRecomendation) {
        resiRecomendation = aResiRecomendation;
    }

    /**
     * Access method for resiFeedback.
     *
     * @return the current value of resiFeedback
     */
    public String getResiFeedback() {
        return resiFeedback;
    }

    /**
     * Setter method for resiFeedback.
     *
     * @param aResiFeedback the new value for resiFeedback
     */
    public void setResiFeedback(String aResiFeedback) {
        resiFeedback = aResiFeedback;
    }

    /**
     * Access method for resiVerifierComments.
     *
     * @return the current value of resiVerifierComments
     */
    public String getResiVerifierComments() {
        return resiVerifierComments;
    }

    /**
     * Setter method for resiVerifierComments.
     *
     * @param aResiVerifierComments the new value for resiVerifierComments
     */
    public void setResiVerifierComments(String aResiVerifierComments) {
        resiVerifierComments = aResiVerifierComments;
    }

    /**
     * Access method for resiRejectreason.
     *
     * @return the current value of resiRejectreason
     */
    public String getResiRejectreason() {
        return resiRejectreason;
    }

    /**
     * Setter method for resiRejectreason.
     *
     * @param aResiRejectreason the new value for resiRejectreason
     */
    public void setResiRejectreason(String aResiRejectreason) {
        resiRejectreason = aResiRejectreason;
    }

    /**
     * Access method for resiLatitude.
     *
     * @return the current value of resiLatitude
     */
    public float getResiLatitude() {
        return resiLatitude;
    }

    /**
     * Setter method for resiLatitude.
     *
     * @param aResiLatitude the new value for resiLatitude
     */
    public void setResiLatitude(float aResiLatitude) {
        resiLatitude = aResiLatitude;
    }

    /**
     * Access method for resiLongitude.
     *
     * @return the current value of resiLongitude
     */
    public float getResiLongitude() {
        return resiLongitude;
    }

    /**
     * Setter method for resiLongitude.
     *
     * @param aResiLongitude the new value for resiLongitude
     */
    public void setResiLongitude(float aResiLongitude) {
        resiLongitude = aResiLongitude;
    }

    /**
     * Access method for resiCid.
     *
     * @return the current value of resiCid
     */
    public String getResiCid() {
        return resiCid;
    }

    /**
     * Setter method for resiCid.
     *
     * @param aResiCid the new value for resiCid
     */
    public void setResiCid(String aResiCid) {
        resiCid = aResiCid;
    }

    /**
     * Access method for resiLac.
     *
     * @return the current value of resiLac
     */
    public String getResiLac() {
        return resiLac;
    }

    /**
     * Setter method for resiLac.
     *
     * @param aResiLac the new value for resiLac
     */
    public void setResiLac(String aResiLac) {
        resiLac = aResiLac;
    }

    /**
     * Access method for resiLat.
     *
     * @return the current value of resiLat
     */
    public float getResiLat() {
        return resiLat;
    }

    /**
     * Setter method for resiLat.
     *
     * @param aResiLat the new value for resiLat
     */
    public void setResiLat(float aResiLat) {
        resiLat = aResiLat;
    }

    /**
     * Access method for resiLong.
     *
     * @return the current value of resiLong
     */
    public float getResiLong() {
        return resiLong;
    }

    /**
     * Setter method for resiLong.
     *
     * @param aResiLong the new value for resiLong
     */
    public void setResiLong(float aResiLong) {
        resiLong = aResiLong;
    }

    /**
     * Access method for fiOffAddr.
     *
     * @return the current value of fiOffAddr
     */
    public String getFiOffAddr() {
        return fiOffAddr;
    }

    /**
     * Setter method for fiOffAddr.
     *
     * @param aFiOffAddr the new value for fiOffAddr
     */
    public void setFiOffAddr(String aFiOffAddr) {
        fiOffAddr = aFiOffAddr;
    }

    /**
     * Access method for fiOffAddr2.
     *
     * @return the current value of fiOffAddr2
     */
    public String getFiOffAddr2() {
        return fiOffAddr2;
    }

    /**
     * Setter method for fiOffAddr2.
     *
     * @param aFiOffAddr2 the new value for fiOffAddr2
     */
    public void setFiOffAddr2(String aFiOffAddr2) {
        fiOffAddr2 = aFiOffAddr2;
    }

    /**
     * Access method for fiOffCity.
     *
     * @return the current value of fiOffCity
     */
    public String getFiOffCity() {
        return fiOffCity;
    }

    /**
     * Setter method for fiOffCity.
     *
     * @param aFiOffCity the new value for fiOffCity
     */
    public void setFiOffCity(String aFiOffCity) {
        fiOffCity = aFiOffCity;
    }

    /**
     * Access method for fiOffPincode.
     *
     * @return the current value of fiOffPincode
     */
    public String getFiOffPincode() {
        return fiOffPincode;
    }

    /**
     * Setter method for fiOffPincode.
     *
     * @param aFiOffPincode the new value for fiOffPincode
     */
    public void setFiOffPincode(String aFiOffPincode) {
        fiOffPincode = aFiOffPincode;
    }

    /**
     * Access method for fiOffState.
     *
     * @return the current value of fiOffState
     */
    public String getFiOffState() {
        return fiOffState;
    }

    /**
     * Setter method for fiOffState.
     *
     * @param aFiOffState the new value for fiOffState
     */
    public void setFiOffState(String aFiOffState) {
        fiOffState = aFiOffState;
    }

    /**
     * Access method for fiOffPh.
     *
     * @return the current value of fiOffPh
     */
    public String getFiOffPh() {
        return fiOffPh;
    }

    /**
     * Setter method for fiOffPh.
     *
     * @param aFiOffPh the new value for fiOffPh
     */
    public void setFiOffPh(String aFiOffPh) {
        fiOffPh = aFiOffPh;
    }

    /**
     * Access method for referenceNo.
     *
     * @return the current value of referenceNo
     */
    public String getReferenceNo() {
        return referenceNo;
    }

    /**
     * Setter method for referenceNo.
     *
     * @param aReferenceNo the new value for referenceNo
     */
    public void setReferenceNo(String aReferenceNo) {
        referenceNo = aReferenceNo;
    }

    /**
     * Access method for branchCode.
     *
     * @return the current value of branchCode
     */
    public String getBranchCode() {
        return branchCode;
    }

    /**
     * Setter method for branchCode.
     *
     * @param aBranchCode the new value for branchCode
     */
    public void setBranchCode(String aBranchCode) {
        branchCode = aBranchCode;
    }

    /**
     * Access method for directMake.
     *
     * @return the current value of directMake
     */
    public String getDirectMake() {
        return directMake;
    }

    /**
     * Setter method for directMake.
     *
     * @param aDirectMake the new value for directMake
     */
    public void setDirectMake(String aDirectMake) {
        directMake = aDirectMake;
    }

    /**
     * Access method for directVariant.
     *
     * @return the current value of directVariant
     */
    public String getDirectVariant() {
        return directVariant;
    }

    /**
     * Setter method for directVariant.
     *
     * @param aDirectVariant the new value for directVariant
     */
    public void setDirectVariant(String aDirectVariant) {
        directVariant = aDirectVariant;
    }

   /* *//**
     * Access method for fiPriority.
     *
     * @return the current value of fiPriority
     *//*
    public int getFiPriority() {
        return fiPriority;
    }

    *//**
     * Setter method for fiPriority.
     *
     * @param aFiPriority the new value for fiPriority
     *//*
    public void setFiPriority(int aFiPriority) {
        fiPriority = aFiPriority;
    }
*/
    /**
     * Access method for fatherSpouseAddressProof.
     *
     * @return the current value of fatherSpouseAddressProof
     */
    public String getFatherSpouseAddressProof() {
        return fatherSpouseAddressProof;
    }

    /**
     * Setter method for fatherSpouseAddressProof.
     *
     * @param aFatherSpouseAddressProof the new value for fatherSpouseAddressProof
     */
    public void setFatherSpouseAddressProof(String aFatherSpouseAddressProof) {
        fatherSpouseAddressProof = aFatherSpouseAddressProof;
    }

    /**
     * Access method for fatherSpouseIdProof.
     *
     * @return the current value of fatherSpouseIdProof
     */
    public String getFatherSpouseIdProof() {
        return fatherSpouseIdProof;
    }

    /**
     * Setter method for fatherSpouseIdProof.
     *
     * @param aFatherSpouseIdProof the new value for fatherSpouseIdProof
     */
    public void setFatherSpouseIdProof(String aFatherSpouseIdProof) {
        fatherSpouseIdProof = aFatherSpouseIdProof;
    }

    /**
     * Access method for fatherSpouseIdProofValue.
     *
     * @return the current value of fatherSpouseIdProofValue
     */
    public String getFatherSpouseIdProofValue() {
        return fatherSpouseIdProofValue;
    }

    /**
     * Setter method for fatherSpouseIdProofValue.
     *
     * @param aFatherSpouseIdProofValue the new value for fatherSpouseIdProofValue
     */
    public void setFatherSpouseIdProofValue(String aFatherSpouseIdProofValue) {
        fatherSpouseIdProofValue = aFatherSpouseIdProofValue;
    }

    /**
     * Access method for fatherSpouseAddressProofValue.
     *
     * @return the current value of fatherSpouseAddressProofValue
     */
    public String getFatherSpouseAddressProofValue() {
        return fatherSpouseAddressProofValue;
    }

    /**
     * Setter method for fatherSpouseAddressProofValue.
     *
     * @param aFatherSpouseAddressProofValue the new value for fatherSpouseAddressProofValue
     */
    public void setFatherSpouseAddressProofValue(String aFatherSpouseAddressProofValue) {
        fatherSpouseAddressProofValue = aFatherSpouseAddressProofValue;
    }

    /**
     * Access method for fatherSpousePanholderName.
     *
     * @return the current value of fatherSpousePanholderName
     */
    public String getFatherSpousePanholderName() {
        return fatherSpousePanholderName;
    }

    /**
     * Setter method for fatherSpousePanholderName.
     *
     * @param aFatherSpousePanholderName the new value for fatherSpousePanholderName
     */
    public void setFatherSpousePanholderName(String aFatherSpousePanholderName) {
        fatherSpousePanholderName = aFatherSpousePanholderName;
    }

    /**
     * Access method for fatherSpouseDob.
     *
     * @return the current value of fatherSpouseDob
     */
    public Timestamp getFatherSpouseDob() {
        return fatherSpouseDob;
    }

    /**
     * Setter method for fatherSpouseDob.
     *
     * @param aFatherSpouseDob the new value for fatherSpouseDob
     */
    public void setFatherSpouseDob(Timestamp aFatherSpouseDob) {
        fatherSpouseDob = aFatherSpouseDob;
    }

    /**
     * Access method for fatherSpouseMobileNo.
     *
     * @return the current value of fatherSpouseMobileNo
     */
    public String getFatherSpouseMobileNo() {
        return fatherSpouseMobileNo;
    }

    /**
     * Setter method for fatherSpouseMobileNo.
     *
     * @param aFatherSpouseMobileNo the new value for fatherSpouseMobileNo
     */
    public void setFatherSpouseMobileNo(String aFatherSpouseMobileNo) {
        fatherSpouseMobileNo = aFatherSpouseMobileNo;
    }

    /**
     * Access method for fatherSpouseName.
     *
     * @return the current value of fatherSpouseName
     */
    public String getFatherSpouseName() {
        return fatherSpouseName;
    }

    /**
     * Setter method for fatherSpouseName.
     *
     * @param aFatherSpouseName the new value for fatherSpouseName
     */
    public void setFatherSpouseName(String aFatherSpouseName) {
        fatherSpouseName = aFatherSpouseName;
    }

    /**
     * Access method for fatherSpouseIsGuarantor.
     *
     * @return the current value of fatherSpouseIsGuarantor
     */
    public String getFatherSpouseIsGuarantor() {
        return fatherSpouseIsGuarantor;
    }

    /**
     * Setter method for fatherSpouseIsGuarantor.
     *
     * @param aFatherSpouseIsGuarantor the new value for fatherSpouseIsGuarantor
     */
    public void setFatherSpouseIsGuarantor(String aFatherSpouseIsGuarantor) {
        fatherSpouseIsGuarantor = aFatherSpouseIsGuarantor;
    }

    /**
     * Access method for firstname.
     *
     * @return the current value of firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * Setter method for firstname.
     *
     * @param aFirstname the new value for firstname
     */
    public void setFirstname(String aFirstname) {
        firstname = aFirstname;
    }

    /**
     * Access method for lastname.
     *
     * @return the current value of lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * Setter method for lastname.
     *
     * @param aLastname the new value for lastname
     */
    public void setLastname(String aLastname) {
        lastname = aLastname;
    }

    /**
     * Access method for appSyntime.
     *
     * @return the current value of appSyntime
     */
    public Timestamp getAppSyntime() {
        return appSyntime;
    }

    /**
     * Setter method for appSyntime.
     *
     * @param aAppSyntime the new value for appSyntime
     */
    public void setAppSyntime(Timestamp aAppSyntime) {
        appSyntime = aAppSyntime;
    }

    /**
     * Access method for appEditSyntime.
     *
     * @return the current value of appEditSyntime
     */
    public Timestamp getAppEditSyntime() {
        return appEditSyntime;
    }

    /**
     * Setter method for appEditSyntime.
     *
     * @param aAppEditSyntime the new value for appEditSyntime
     */
    public void setAppEditSyntime(Timestamp aAppEditSyntime) {
        appEditSyntime = aAppEditSyntime;
    }

    /**
     * Access method for resiVerifySyntime.
     *
     * @return the current value of resiVerifySyntime
     */
    public Timestamp getResiVerifySyntime() {
        return resiVerifySyntime;
    }

    /**
     * Setter method for resiVerifySyntime.
     *
     * @param aResiVerifySyntime the new value for resiVerifySyntime
     */
    public void setResiVerifySyntime(Timestamp aResiVerifySyntime) {
        resiVerifySyntime = aResiVerifySyntime;
    }

    /**
     * Access method for cibilScore.
     *
     * @return the current value of cibilScore
     */
    public String getCibilScore() {
        return cibilScore;
    }

    /**
     * Setter method for cibilScore.
     *
     * @param aCibilScore the new value for cibilScore
     */
    public void setCibilScore(String aCibilScore) {
        cibilScore = aCibilScore;
    }

    /**
     * Access method for crifScore.
     *
     * @return the current value of crifScore
     */
    public String getCrifScore() {
        return crifScore;
    }

    /**
     * Setter method for crifScore.
     *
     * @param aCrifScore the new value for crifScore
     */
    public void setCrifScore(String aCrifScore) {
        crifScore = aCrifScore;
    }

    /**
     * Access method for roi.
     *
     * @return the current value of roi
     */
    public float getRoi() {
        return roi;
    }

    /**
     * Setter method for roi.
     *
     * @param aRoi the new value for roi
     */
    public void setRoi(float aRoi) {
        roi = aRoi;
    }

    /**
     * Access method for resiStd.
     *
     * @return the current value of resiStd
     */
    public String getResiStd() {
        return resiStd;
    }

    /**
     * Setter method for resiStd.
     *
     * @param aResiStd the new value for resiStd
     */
    public void setResiStd(String aResiStd) {
        resiStd = aResiStd;
    }

    /**
     * Access method for fiOffStd.
     *
     * @return the current value of fiOffStd
     */
    public String getFiOffStd() {
        return fiOffStd;
    }

    /**
     * Setter method for fiOffStd.
     *
     * @param aFiOffStd the new value for fiOffStd
     */
    public void setFiOffStd(String aFiOffStd) {
        fiOffStd = aFiOffStd;
    }

    /**
     * Access method for fiResiStd.
     *
     * @return the current value of fiResiStd
     */
    public String getFiResiStd() {
        return fiResiStd;
    }

    /**
     * Setter method for fiResiStd.
     *
     * @param aFiResiStd the new value for fiResiStd
     */
    public void setFiResiStd(String aFiResiStd) {
        fiResiStd = aFiResiStd;
    }

    /**
     * Access method for resiStatus.
     *
     * @return the current value of resiStatus
     */
    public String getResiStatus() {
        return resiStatus;
    }

    /**
     * Setter method for resiStatus.
     *
     * @param aResiStatus the new value for resiStatus
     */
    public void setResiStatus(String aResiStatus) {
        resiStatus = aResiStatus;
    }

    /**
     * Access method for guarantorFirstname.
     *
     * @return the current value of guarantorFirstname
     */
    public String getGuarantorFirstname() {
        return guarantorFirstname;
    }

    /**
     * Setter method for guarantorFirstname.
     *
     * @param aGuarantorFirstname the new value for guarantorFirstname
     */
    public void setGuarantorFirstname(String aGuarantorFirstname) {
        guarantorFirstname = aGuarantorFirstname;
    }

    /**
     * Access method for guarantorLastname.
     *
     * @return the current value of guarantorLastname
     */
    public String getGuarantorLastname() {
        return guarantorLastname;
    }

    /**
     * Setter method for guarantorLastname.
     *
     * @param aGuarantorLastname the new value for guarantorLastname
     */
    public void setGuarantorLastname(String aGuarantorLastname) {
        guarantorLastname = aGuarantorLastname;
    }

    /**
     * Access method for noOfCow.
     *
     * @return the current value of noOfCow
     */
    public int getNoOfCow() {
        return noOfCow;
    }

    /**
     * Setter method for noOfCow.
     *
     * @param aNoOfCow the new value for noOfCow
     */
    public void setNoOfCow(int aNoOfCow) {
        noOfCow = aNoOfCow;
    }

    /**
     * Access method for noOfBuffalo.
     *
     * @return the current value of noOfBuffalo
     */
    public int getNoOfBuffalo() {
        return noOfBuffalo;
    }

    /**
     * Setter method for noOfBuffalo.
     *
     * @param aNoOfBuffalo the new value for noOfBuffalo
     */
    public void setNoOfBuffalo(int aNoOfBuffalo) {
        noOfBuffalo = aNoOfBuffalo;
    }

    /**
     * Access method for noOfBulls.
     *
     * @return the current value of noOfBulls
     */
    public int getNoOfBulls() {
        return noOfBulls;
    }

    /**
     * Setter method for noOfBulls.
     *
     * @param aNoOfBulls the new value for noOfBulls
     */
    public void setNoOfBulls(int aNoOfBulls) {
        noOfBulls = aNoOfBulls;
    }

    /**
     * Access method for kccLoan.
     *
     * @return the current value of kccLoan
     */
    public String getKccLoan() {
        return kccLoan;
    }

    /**
     * Setter method for kccLoan.
     *
     * @param aKccLoan the new value for kccLoan
     */
    public void setKccLoan(String aKccLoan) {
        kccLoan = aKccLoan;
    }

    /**
     * Access method for kccLoanbank.
     *
     * @return the current value of kccLoanbank
     */
    public String getKccLoanbank() {
        return kccLoanbank;
    }

    /**
     * Setter method for kccLoanbank.
     *
     * @param aKccLoanbank the new value for kccLoanbank
     */
    public void setKccLoanbank(String aKccLoanbank) {
        kccLoanbank = aKccLoanbank;
    }

    /**
     * Access method for avgLandvalue.
     *
     * @return the current value of avgLandvalue
     */
    public String getAvgLandvalue() {
        return avgLandvalue;
    }

    /**
     * Setter method for avgLandvalue.
     *
     * @param aAvgLandvalue the new value for avgLandvalue
     */
    public void setAvgLandvalue(String aAvgLandvalue) {
        avgLandvalue = aAvgLandvalue;
    }

    /**
     * Access method for photoNotAllowed.
     *
     * @return the current value of photoNotAllowed
     */
    public String getPhotoNotAllowed() {
        return photoNotAllowed;
    }

    /**
     * Setter method for photoNotAllowed.
     *
     * @param aPhotoNotAllowed the new value for photoNotAllowed
     */
    public void setPhotoNotAllowed(String aPhotoNotAllowed) {
        photoNotAllowed = aPhotoNotAllowed;
    }

    /**
     * Access method for unit.
     *
     * @return the current value of unit
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Setter method for unit.
     *
     * @param aUnit the new value for unit
     */
    public void setUnit(String aUnit) {
        unit = aUnit;
    }

    /**
     * Access method for typeofroof.
     *
     * @return the current value of typeofroof
     */
    public String getTypeofroof() {
        return typeofroof;
    }

    /**
     * Setter method for typeofroof.
     *
     * @param aTypeofroof the new value for typeofroof
     */
    public void setTypeofroof(String aTypeofroof) {
        typeofroof = aTypeofroof;
    }

    /**
     * Access method for flooring.
     *
     * @return the current value of flooring
     */
    public String getFlooring() {
        return flooring;
    }

    /**
     * Setter method for flooring.
     *
     * @param aFlooring the new value for flooring
     */
    public void setFlooring(String aFlooring) {
        flooring = aFlooring;
    }

    /**
     * Access method for kccLoanavail.
     *
     * @return the current value of kccLoanavail
     */
    public String getKccLoanavail() {
        return kccLoanavail;
    }

    /**
     * Setter method for kccLoanavail.
     *
     * @param aKccLoanavail the new value for kccLoanavail
     */
    public void setKccLoanavail(String aKccLoanavail) {
        kccLoanavail = aKccLoanavail;
    }

    /**
     * Access method for installmentType.
     *
     * @return the current value of installmentType
     */
    public String getInstallmentType() {
        return installmentType;
    }

    /**
     * Setter method for installmentType.
     *
     * @param aInstallmentType the new value for installmentType
     */
    public void setInstallmentType(String aInstallmentType) {
        installmentType = aInstallmentType;
    }

    /**
     * Access method for noofinstallments.
     *
     * @return the current value of noofinstallments
     */
    public String getNoofinstallments() {
        return noofinstallments;
    }

    /**
     * Setter method for noofinstallments.
     *
     * @param aNoofinstallments the new value for noofinstallments
     */
    public void setNoofinstallments(String aNoofinstallments) {
        noofinstallments = aNoofinstallments;
    }

   /* *//**
     * Access method for cibilTotOd.
     *
     * @return the current value of cibilTotOd
     *//*
    public float getCibilTotOd() {
        return cibilTotOd;
    }

    *//**
     * Setter method for cibilTotOd.
     *
     * @param aCibilTotOd the new value for cibilTotOd
     *//*
    public void setCibilTotOd(float aCibilTotOd) {
        cibilTotOd = aCibilTotOd;
    }*/

    /**
     * Access method for cibilDetectFlag.
     *
     * @return the current value of cibilDetectFlag
     */
    public String getCibilDetectFlag() {
        return cibilDetectFlag;
    }

    /**
     * Setter method for cibilDetectFlag.
     *
     * @param aCibilDetectFlag the new value for cibilDetectFlag
     */
    public void setCibilDetectFlag(String aCibilDetectFlag) {
        cibilDetectFlag = aCibilDetectFlag;
    }

    /**
     * Access method for passportExpiryDate.
     *
     * @return the current value of passportExpiryDate
     */
    public Timestamp getPassportExpiryDate() {
        return passportExpiryDate;
    }

    /**
     * Setter method for passportExpiryDate.
     *
     * @param aPassportExpiryDate the new value for passportExpiryDate
     */
    public void setPassportExpiryDate(Timestamp aPassportExpiryDate) {
        passportExpiryDate = aPassportExpiryDate;
    }

    /**
     * Access method for drivingLicenseExpiryDate.
     *
     * @return the current value of drivingLicenseExpiryDate
     */
    public Timestamp getDrivingLicenseExpiryDate() {
        return drivingLicenseExpiryDate;
    }

    /**
     * Setter method for drivingLicenseExpiryDate.
     *
     * @param aDrivingLicenseExpiryDate the new value for drivingLicenseExpiryDate
     */
    public void setDrivingLicenseExpiryDate(Timestamp aDrivingLicenseExpiryDate) {
        drivingLicenseExpiryDate = aDrivingLicenseExpiryDate;
    }

    /**
     * Access method for noofVariant.
     *
     * @return the current value of noofVariant
     */
    public int getNoofVariant() {
        return noofVariant;
    }

    /**
     * Setter method for noofVariant.
     *
     * @param aNoofVariant the new value for noofVariant
     */
    public void setNoofVariant(int aNoofVariant) {
        noofVariant = aNoofVariant;
    }

    /**
     * Access method for noofVariantCompleted.
     *
     * @return the current value of noofVariantCompleted
     */
    public int getNoofVariantCompleted() {
        return noofVariantCompleted;
    }

    /**
     * Setter method for noofVariantCompleted.
     *
     * @param aNoofVariantCompleted the new value for noofVariantCompleted
     */
    public void setNoofVariantCompleted(int aNoofVariantCompleted) {
        noofVariantCompleted = aNoofVariantCompleted;
    }

    /**
     * Access method for noofGuarantor.
     *
     * @return the current value of noofGuarantor
     */
    public String getNoofGuarantor() {
        return noofGuarantor;
    }

    /**
     * Setter method for noofGuarantor.
     *
     * @param aNoofGuarantor the new value for noofGuarantor
     */
    public void setNoofGuarantor(String aNoofGuarantor) {
        noofGuarantor = aNoofGuarantor;
    }

    /**
     * Access method for noofGuarantorCompleted.
     *
     * @return the current value of noofGuarantorCompleted
     */
    public int getNoofGuarantorCompleted() {
        return noofGuarantorCompleted;
    }

    /**
     * Setter method for noofGuarantorCompleted.
     *
     * @param aNoofGuarantorCompleted the new value for noofGuarantorCompleted
     */
    public void setNoofGuarantorCompleted(int aNoofGuarantorCompleted) {
        noofGuarantorCompleted = aNoofGuarantorCompleted;
    }

    /**
     * Access method for resiNeighCheckDone.
     *
     * @return the current value of resiNeighCheckDone
     */
    public String getResiNeighCheckDone() {
        return resiNeighCheckDone;
    }

    /**
     * Setter method for resiNeighCheckDone.
     *
     * @param aResiNeighCheckDone the new value for resiNeighCheckDone
     */
    public void setResiNeighCheckDone(String aResiNeighCheckDone) {
        resiNeighCheckDone = aResiNeighCheckDone;
    }

    /**
     * Access method for resiNeighType.
     *
     * @return the current value of resiNeighType
     */
    public String getResiNeighType() {
        return resiNeighType;
    }

    /**
     * Setter method for resiNeighType.
     *
     * @param aResiNeighType the new value for resiNeighType
     */
    public void setResiNeighType(String aResiNeighType) {
        resiNeighType = aResiNeighType;
    }

    /**
     * Access method for borrowercoapplicantresidinginsame.
     *
     * @return the current value of borrowercoapplicantresidinginsame
     */
    public String getBorrowercoapplicantresidinginsame() {
        return borrowercoapplicantresidinginsame;
    }

    /**
     * Setter method for borrowercoapplicantresidinginsame.
     *
     * @param aBorrowercoapplicantresidinginsame the new value for borrowercoapplicantresidinginsame
     */
    public void setBorrowercoapplicantresidinginsame(String aBorrowercoapplicantresidinginsame) {
        borrowercoapplicantresidinginsame = aBorrowercoapplicantresidinginsame;
    }

    /**
     * Access method for noOfBeneficiariesCompleted.
     *
     * @return the current value of noOfBeneficiariesCompleted
     */
    public int getNoOfBeneficiariesCompleted() {
        return noOfBeneficiariesCompleted;
    }

    /**
     * Setter method for noOfBeneficiariesCompleted.
     *
     * @param aNoOfBeneficiariesCompleted the new value for noOfBeneficiariesCompleted
     */
    public void setNoOfBeneficiariesCompleted(int aNoOfBeneficiariesCompleted) {
        noOfBeneficiariesCompleted = aNoOfBeneficiariesCompleted;
    }

    /**
     * Access method for existingCustomer.
     *
     * @return the current value of existingCustomer
     */
    public String getExistingCustomer() {
        return existingCustomer;
    }

    /**
     * Setter method for existingCustomer.
     *
     * @param aExistingCustomer the new value for existingCustomer
     */
    public void setExistingCustomer(String aExistingCustomer) {
        existingCustomer = aExistingCustomer;
    }

    /**
     * Access method for customerCode.
     *
     * @return the current value of customerCode
     */
    public String getCustomerCode() {
        return customerCode;
    }

    /**
     * Setter method for customerCode.
     *
     * @param aCustomerCode the new value for customerCode
     */
    public void setCustomerCode(String aCustomerCode) {
        customerCode = aCustomerCode;
    }

    /**
     * Access method for verificationStatus.
     *
     * @return the current value of verificationStatus
     */
    public String getVerificationStatus() {
        return verificationStatus;
    }

    /**
     * Setter method for verificationStatus.
     *
     * @param aVerificationStatus the new value for verificationStatus
     */
    public void setVerificationStatus(String aVerificationStatus) {
        verificationStatus = aVerificationStatus;
    }

    /**
     * Access method for appMaritalStatus.
     *
     * @return the current value of appMaritalStatus
     */
    public String getAppMaritalStatus() {
        return appMaritalStatus;
    }

    /**
     * Setter method for appMaritalStatus.
     *
     * @param aAppMaritalStatus the new value for appMaritalStatus
     */
    public void setAppMaritalStatus(String aAppMaritalStatus) {
        appMaritalStatus = aAppMaritalStatus;
    }

    /**
     * Access method for ekycstatus.
     *
     * @return the current value of ekycstatus
     */
    public String getEkycstatus() {
        return ekycstatus;
    }

    /**
     * Setter method for ekycstatus.
     *
     * @param aEkycstatus the new value for ekycstatus
     */
    public void setEkycstatus(String aEkycstatus) {
        ekycstatus = aEkycstatus;
    }

    /**
     * Access method for custQualification.
     *
     * @return the current value of custQualification
     */
    public String getCustQualification() {
        return custQualification;
    }

    /**
     * Setter method for custQualification.
     *
     * @param aCustQualification the new value for custQualification
     */
    public void setCustQualification(String aCustQualification) {
        custQualification = aCustQualification;
    }

    /**
     * Access method for commLanguage.
     *
     * @return the current value of commLanguage
     */
    public String getCommLanguage() {
        return commLanguage;
    }

    /**
     * Setter method for commLanguage.
     *
     * @param aCommLanguage the new value for commLanguage
     */
    public void setCommLanguage(String aCommLanguage) {
        commLanguage = aCommLanguage;
    }

    /**
     * Access method for classOfactivity.
     *
     * @return the current value of classOfactivity
     */
    public String getClassOfactivity() {
        return classOfactivity;
    }

    /**
     * Setter method for classOfactivity.
     *
     * @param aClassOfactivity the new value for classOfactivity
     */
    public void setClassOfactivity(String aClassOfactivity) {
        classOfactivity = aClassOfactivity;
    }

    /**
     * Access method for loantype.
     *
     * @return the current value of loantype
     */
    public String getLoantype() {
        return loantype;
    }

    /**
     * Setter method for loantype.
     *
     * @param aLoantype the new value for loantype
     */
    public void setLoantype(String aLoantype) {
        loantype = aLoantype;
    }

    /**
     * Access method for resiNeighType2.
     *
     * @return the current value of resiNeighType2
     */
    public String getResiNeighType2() {
        return resiNeighType2;
    }

    /**
     * Setter method for resiNeighType2.
     *
     * @param aResiNeighType2 the new value for resiNeighType2
     */
    public void setResiNeighType2(String aResiNeighType2) {
        resiNeighType2 = aResiNeighType2;
    }

    /**
     * Access method for guarantorOrCoapp.
     *
     * @return the current value of guarantorOrCoapp
     */
    public String getGuarantorOrCoapp() {
        return guarantorOrCoapp;
    }

    /**
     * Setter method for guarantorOrCoapp.
     *
     * @param aGuarantorOrCoapp the new value for guarantorOrCoapp
     */
    public void setGuarantorOrCoapp(String aGuarantorOrCoapp) {
        guarantorOrCoapp = aGuarantorOrCoapp;
    }

    /**
     * Access method for autoApproved.
     *
     * @return the current value of autoApproved
     */
    public int getAutoApproved() {
        return autoApproved;
    }

    /**
     * Setter method for autoApproved.
     *
     * @param aAutoApproved the new value for autoApproved
     */
    public void setAutoApproved(int aAutoApproved) {
        autoApproved = aAutoApproved;
    }

    /**
     * Access method for appAutoapprovedStatus.
     *
     * @return the current value of appAutoapprovedStatus
     */
    public String getAppAutoapprovedStatus() {
        return appAutoapprovedStatus;
    }

    /**
     * Setter method for appAutoapprovedStatus.
     *
     * @param aAppAutoapprovedStatus the new value for appAutoapprovedStatus
     */
    public void setAppAutoapprovedStatus(String aAppAutoapprovedStatus) {
        appAutoapprovedStatus = aAppAutoapprovedStatus;
    }

    /**
     * Access method for appDeviationReason.
     *
     * @return the current value of appDeviationReason
     */
    public String getAppDeviationReason() {
        return appDeviationReason;
    }

    /**
     * Setter method for appDeviationReason.
     *
     * @param aAppDeviationReason the new value for appDeviationReason
     */
    public void setAppDeviationReason(String aAppDeviationReason) {
        appDeviationReason = aAppDeviationReason;
    }

    /**
     * Access method for appCreditLevel.
     *
     * @return the current value of appCreditLevel
     */
    public String getAppCreditLevel() {
        return appCreditLevel;
    }

    /**
     * Setter method for appCreditLevel.
     *
     * @param aAppCreditLevel the new value for appCreditLevel
     */
    public void setAppCreditLevel(String aAppCreditLevel) {
        appCreditLevel = aAppCreditLevel;
    }

    /**
     * Access method for appCreditApproverName.
     *
     * @return the current value of appCreditApproverName
     */
    public String getAppCreditApproverName() {
        return appCreditApproverName;
    }

    /**
     * Setter method for appCreditApproverName.
     *
     * @param aAppCreditApproverName the new value for appCreditApproverName
     */
    public void setAppCreditApproverName(String aAppCreditApproverName) {
        appCreditApproverName = aAppCreditApproverName;
    }

    /**
     * Access method for appCreditApprovedTime.
     *
     * @return the current value of appCreditApprovedTime
     */
    public Timestamp getAppCreditApprovedTime() {
        return appCreditApprovedTime;
    }

    /**
     * Setter method for appCreditApprovedTime.
     *
     * @param aAppCreditApprovedTime the new value for appCreditApprovedTime
     */
    public void setAppCreditApprovedTime(Timestamp aAppCreditApprovedTime) {
        appCreditApprovedTime = aAppCreditApprovedTime;
    }

    /**
     * Access method for appCreditApprovedStatus.
     *
     * @return the current value of appCreditApprovedStatus
     */
    public String getAppCreditApprovedStatus() {
        return appCreditApprovedStatus;
    }

    /**
     * Setter method for appCreditApprovedStatus.
     *
     * @param aAppCreditApprovedStatus the new value for appCreditApprovedStatus
     */
    public void setAppCreditApprovedStatus(String aAppCreditApprovedStatus) {
        appCreditApprovedStatus = aAppCreditApprovedStatus;
    }

    /**
     * Access method for creditRecommenderName.
     *
     * @return the current value of creditRecommenderName
     */
    public String getCreditRecommenderName() {
        return creditRecommenderName;
    }

    /**
     * Setter method for creditRecommenderName.
     *
     * @param aCreditRecommenderName the new value for creditRecommenderName
     */
    public void setCreditRecommenderName(String aCreditRecommenderName) {
        creditRecommenderName = aCreditRecommenderName;
    }

    /**
     * Access method for creditRecommendedStatus.
     *
     * @return the current value of creditRecommendedStatus
     */
    public String getCreditRecommendedStatus() {
        return creditRecommendedStatus;
    }

    /**
     * Setter method for creditRecommendedStatus.
     *
     * @param aCreditRecommendedStatus the new value for creditRecommendedStatus
     */
    public void setCreditRecommendedStatus(String aCreditRecommendedStatus) {
        creditRecommendedStatus = aCreditRecommendedStatus;
    }

    /**
     * Access method for creditRecommendedComments.
     *
     * @return the current value of creditRecommendedComments
     */
    public String getCreditRecommendedComments() {
        return creditRecommendedComments;
    }

    /**
     * Setter method for creditRecommendedComments.
     *
     * @param aCreditRecommendedComments the new value for creditRecommendedComments
     */
    public void setCreditRecommendedComments(String aCreditRecommendedComments) {
        creditRecommendedComments = aCreditRecommendedComments;
    }

    /**
     * Access method for creditHoldComments.
     *
     * @return the current value of creditHoldComments
     */
    public String getCreditHoldComments() {
        return creditHoldComments;
    }

    /**
     * Setter method for creditHoldComments.
     *
     * @param aCreditHoldComments the new value for creditHoldComments
     */
    public void setCreditHoldComments(String aCreditHoldComments) {
        creditHoldComments = aCreditHoldComments;
    }

    /**
     * Access method for creditEditApp.
     *
     * @return the current value of creditEditApp
     */
    public String getCreditEditApp() {
        return creditEditApp;
    }

    /**
     * Setter method for creditEditApp.
     *
     * @param aCreditEditApp the new value for creditEditApp
     */
    public void setCreditEditApp(String aCreditEditApp) {
        creditEditApp = aCreditEditApp;
    }

    /**
     * Access method for relookOtp.
     *
     * @return the current value of relookOtp
     */
    public String getRelookOtp() {
        return relookOtp;
    }

    /**
     * Setter method for relookOtp.
     *
     * @param aRelookOtp the new value for relookOtp
     */
    public void setRelookOtp(String aRelookOtp) {
        relookOtp = aRelookOtp;
    }

    /**
     * Access method for gridPrice.
     *
     * @return the current value of gridPrice
     */
    public float getGridPrice() {
        return gridPrice;
    }

    /**
     * Setter method for gridPrice.
     *
     * @param aGridPrice the new value for gridPrice
     */
    public void setGridPrice(float aGridPrice) {
        gridPrice = aGridPrice;
    }

    /**
     * Access method for onroadPrice.
     *
     * @return the current value of onroadPrice
     */
    public float getOnroadPrice() {
        return onroadPrice;
    }

    /**
     * Setter method for onroadPrice.
     *
     * @param aOnroadPrice the new value for onroadPrice
     */
    public void setOnroadPrice(float aOnroadPrice) {
        onroadPrice = aOnroadPrice;
    }

    /**
     * Access method for actualLtv.
     *
     * @return the current value of actualLtv
     */
    public float getActualLtv() {
        return actualLtv;
    }

    /**
     * Setter method for actualLtv.
     *
     * @param aActualLtv the new value for actualLtv
     */
    public void setActualLtv(float aActualLtv) {
        actualLtv = aActualLtv;
    }

    /**
     * Access method for advanceEmi.
     *
     * @return the current value of advanceEmi
     */
    public String getAdvanceEmi() {
        return advanceEmi;
    }

    /**
     * Setter method for advanceEmi.
     *
     * @param aAdvanceEmi the new value for advanceEmi
     */
    public void setAdvanceEmi(String aAdvanceEmi) {
        advanceEmi = aAdvanceEmi;
    }

    /**
     * Access method for resilandmark.
     *
     * @return the current value of resilandmark
     */
    public String getResilandmark() {
        return resilandmark;
    }

    /**
     * Setter method for resilandmark.
     *
     * @param aResilandmark the new value for resilandmark
     */
    public void setResilandmark(String aResilandmark) {
        resilandmark = aResilandmark;
    }

    /**
     * Access method for permlandmark.
     *
     * @return the current value of permlandmark
     */
    public String getPermlandmark() {
        return permlandmark;
    }

    /**
     * Setter method for permlandmark.
     *
     * @param aPermlandmark the new value for permlandmark
     */
    public void setPermlandmark(String aPermlandmark) {
        permlandmark = aPermlandmark;
    }

    /**
     * Access method for offlandmark.
     *
     * @return the current value of offlandmark
     */
    public String getOfflandmark() {
        return offlandmark;
    }

    /**
     * Setter method for offlandmark.
     *
     * @param aOfflandmark the new value for offlandmark
     */
    public void setOfflandmark(String aOfflandmark) {
        offlandmark = aOfflandmark;
    }

    /**
     * Access method for ref1addr.
     *
     * @return the current value of ref1addr
     */
    public String getRef1addr() {
        return ref1addr;
    }

    /**
     * Setter method for ref1addr.
     *
     * @param aRef1addr the new value for ref1addr
     */
    public void setRef1addr(String aRef1addr) {
        ref1addr = aRef1addr;
    }

    /**
     * Access method for ref1city.
     *
     * @return the current value of ref1city
     */
    public String getRef1city() {
        return ref1city;
    }

    /**
     * Setter method for ref1city.
     *
     * @param aRef1city the new value for ref1city
     */
    public void setRef1city(String aRef1city) {
        ref1city = aRef1city;
    }

    /**
     * Access method for ref1state.
     *
     * @return the current value of ref1state
     */
    public String getRef1state() {
        return ref1state;
    }

    /**
     * Setter method for ref1state.
     *
     * @param aRef1state the new value for ref1state
     */
    public void setRef1state(String aRef1state) {
        ref1state = aRef1state;
    }

    /**
     * Access method for ref1pincode.
     *
     * @return the current value of ref1pincode
     */
    public String getRef1pincode() {
        return ref1pincode;
    }

    /**
     * Setter method for ref1pincode.
     *
     * @param aRef1pincode the new value for ref1pincode
     */
    public void setRef1pincode(String aRef1pincode) {
        ref1pincode = aRef1pincode;
    }

    /**
     * Access method for ref2addr.
     *
     * @return the current value of ref2addr
     */
    public String getRef2addr() {
        return ref2addr;
    }

    /**
     * Setter method for ref2addr.
     *
     * @param aRef2addr the new value for ref2addr
     */
    public void setRef2addr(String aRef2addr) {
        ref2addr = aRef2addr;
    }

    /**
     * Access method for ref2city.
     *
     * @return the current value of ref2city
     */
    public String getRef2city() {
        return ref2city;
    }

    /**
     * Setter method for ref2city.
     *
     * @param aRef2city the new value for ref2city
     */
    public void setRef2city(String aRef2city) {
        ref2city = aRef2city;
    }

    /**
     * Access method for ref2state.
     *
     * @return the current value of ref2state
     */
    public String getRef2state() {
        return ref2state;
    }

    /**
     * Setter method for ref2state.
     *
     * @param aRef2state the new value for ref2state
     */
    public void setRef2state(String aRef2state) {
        ref2state = aRef2state;
    }

    /**
     * Access method for bankname.
     *
     * @return the current value of bankname
     */
    public String getBankname() {
        return bankname;
    }

    /**
     * Setter method for bankname.
     *
     * @param aBankname the new value for bankname
     */
    public void setBankname(String aBankname) {
        bankname = aBankname;
    }

    /**
     * Access method for bankaccno.
     *
     * @return the current value of bankaccno
     */
    public String getBankaccno() {
        return bankaccno;
    }

    /**
     * Setter method for bankaccno.
     *
     * @param aBankaccno the new value for bankaccno
     */
    public void setBankaccno(String aBankaccno) {
        bankaccno = aBankaccno;
    }

    /**
     * Access method for bankbranch.
     *
     * @return the current value of bankbranch
     */
    public String getBankbranch() {
        return bankbranch;
    }

    /**
     * Setter method for bankbranch.
     *
     * @param aBankbranch the new value for bankbranch
     */
    public void setBankbranch(String aBankbranch) {
        bankbranch = aBankbranch;
    }

    /**
     * Access method for bankifsc.
     *
     * @return the current value of bankifsc
     */
    public String getBankifsc() {
        return bankifsc;
    }

    /**
     * Setter method for bankifsc.
     *
     * @param aBankifsc the new value for bankifsc
     */
    public void setBankifsc(String aBankifsc) {
        bankifsc = aBankifsc;
    }

    /**
     * Access method for insuranceAmount.
     *
     * @return the current value of insuranceAmount
     */
    public float getInsuranceAmount() {
        return insuranceAmount;
    }

    /**
     * Setter method for insuranceAmount.
     *
     * @param aInsuranceAmount the new value for insuranceAmount
     */
    public void setInsuranceAmount(float aInsuranceAmount) {
        insuranceAmount = aInsuranceAmount;
    }

    /**
     * Access method for processingFee.
     *
     * @return the current value of processingFee
     */
    public float getProcessingFee() {
        return processingFee;
    }

    /**
     * Setter method for processingFee.
     *
     * @param aProcessingFee the new value for processingFee
     */
    public void setProcessingFee(float aProcessingFee) {
        processingFee = aProcessingFee;
    }

    /**
     * Access method for cashCollCharges.
     *
     * @return the current value of cashCollCharges
     */
    public float getCashCollCharges() {
        return cashCollCharges;
    }

    /**
     * Setter method for cashCollCharges.
     *
     * @param aCashCollCharges the new value for cashCollCharges
     */
    public void setCashCollCharges(float aCashCollCharges) {
        cashCollCharges = aCashCollCharges;
    }

    /**
     * Access method for dealerDisbursementAmount.
     *
     * @return the current value of dealerDisbursementAmount
     */
    public float getDealerDisbursementAmount() {
        return dealerDisbursementAmount;
    }

    /**
     * Setter method for dealerDisbursementAmount.
     *
     * @param aDealerDisbursementAmount the new value for dealerDisbursementAmount
     */
    public void setDealerDisbursementAmount(float aDealerDisbursementAmount) {
        dealerDisbursementAmount = aDealerDisbursementAmount;
    }

    /**
     * Access method for appAutorejectReason.
     *
     * @return the current value of appAutorejectReason
     */
    public String getAppAutorejectReason() {
        return appAutorejectReason;
    }

    /**
     * Setter method for appAutorejectReason.
     *
     * @param aAppAutorejectReason the new value for appAutorejectReason
     */
    public void setAppAutorejectReason(String aAppAutorejectReason) {
        appAutorejectReason = aAppAutorejectReason;
    }

    /**
     * Access method for downPayment.
     *
     * @return the current value of downPayment
     */
    public float getDownPayment() {
        return downPayment;
    }

    /**
     * Setter method for downPayment.
     *
     * @param aDownPayment the new value for downPayment
     */
    public void setDownPayment(float aDownPayment) {
        downPayment = aDownPayment;
    }

    /**
     * Access method for unholdReason.
     *
     * @return the current value of unholdReason
     */
    public String getUnholdReason() {
        return unholdReason;
    }

    /**
     * Setter method for unholdReason.
     *
     * @param aUnholdReason the new value for unholdReason
     */
    public void setUnholdReason(String aUnholdReason) {
        unholdReason = aUnholdReason;
    }

    /**
     * Access method for opsComments.
     *
     * @return the current value of opsComments
     */
    public String getOpsComments() {
        return opsComments;
    }

    /**
     * Setter method for opsComments.
     *
     * @param aOpsComments the new value for opsComments
     */
    public void setOpsComments(String aOpsComments) {
        opsComments = aOpsComments;
    }

    /**
     * Access method for opsHoldComments.
     *
     * @return the current value of opsHoldComments
     */
    public String getOpsHoldComments() {
        return opsHoldComments;
    }

    /**
     * Setter method for opsHoldComments.
     *
     * @param aOpsHoldComments the new value for opsHoldComments
     */
    public void setOpsHoldComments(String aOpsHoldComments) {
        opsHoldComments = aOpsHoldComments;
    }

    /**
     * Access method for opsApproveStatus.
     *
     * @return the current value of opsApproveStatus
     */
    public String getOpsApproveStatus() {
        return opsApproveStatus;
    }

    /**
     * Setter method for opsApproveStatus.
     *
     * @param aOpsApproveStatus the new value for opsApproveStatus
     */
    public void setOpsApproveStatus(String aOpsApproveStatus) {
        opsApproveStatus = aOpsApproveStatus;
    }
    

    public String getOpsApproverComments() {
		return opsApproverComments;
	}

	public void setOpsApproverComments(String opsApproverComments) {
		this.opsApproverComments = opsApproverComments;
	}

	/**
     * Access method for opsApproverName.
     *
     * @return the current value of opsApproverName
     */
    public String getOpsApproverName() {
        return opsApproverName;
    }

    /**
     * Setter method for opsApproverName.
     *
     * @param aOpsApproverName the new value for opsApproverName
     */
    public void setOpsApproverName(String aOpsApproverName) {
        opsApproverName = aOpsApproverName;
    }

    /**
     * Access method for opsApproveTime.
     *
     * @return the current value of opsApproveTime
     */
    public Timestamp getOpsApproveTime() {
        return opsApproveTime;
    }

    /**
     * Setter method for opsApproveTime.
     *
     * @param aOpsApproveTime the new value for opsApproveTime
     */
    public void setOpsApproveTime(Timestamp aOpsApproveTime) {
        opsApproveTime = aOpsApproveTime;
    }

    /**
     * Access method for appDeactivate.
     *
     * @return the current value of appDeactivate
     */
    public int getAppDeactivate() {
        return appDeactivate;
    }

    /**
     * Setter method for appDeactivate.
     *
     * @param aAppDeactivate the new value for appDeactivate
     */
    public void setAppDeactivate(int aAppDeactivate) {
        appDeactivate = aAppDeactivate;
    }

    /**
     * Access method for opsStatus.
     *
     * @return the current value of opsStatus
     */
    public String getOpsStatus() {
        return opsStatus;
    }

    /**
     * Setter method for opsStatus.
     *
     * @param aOpsStatus the new value for opsStatus
     */
    public void setOpsStatus(String aOpsStatus) {
        opsStatus = aOpsStatus;
    }

    /**
     * Access method for opsDisburseTime.
     *
     * @return the current value of opsDisburseTime
     */
    public Timestamp getOpsDisburseTime() {
        return opsDisburseTime;
    }

    /**
     * Setter method for opsDisburseTime.
     *
     * @param aOpsDisburseTime the new value for opsDisburseTime
     */
    public void setOpsDisburseTime(Timestamp aOpsDisburseTime) {
        opsDisburseTime = aOpsDisburseTime;
    }

    /**
     * Access method for lmsUpdate.
     *
     * @return the current value of lmsUpdate
     */
    public int getLmsUpdate() {
        return lmsUpdate;
    }

    /**
     * Setter method for lmsUpdate.
     *
     * @param aLmsUpdate the new value for lmsUpdate
     */
    public void setLmsUpdate(int aLmsUpdate) {
        lmsUpdate = aLmsUpdate;
    }

    /**
     * Access method for pddPath.
     *
     * @return the current value of pddPath
     */
    public String getPddPath() {
        return pddPath;
    }

    /**
     * Setter method for pddPath.
     *
     * @param aPddPath the new value for pddPath
     */
    public void setPddPath(String aPddPath) {
        pddPath = aPddPath;
    }

    /**
     * Access method for preEmiCharges.
     *
     * @return the current value of preEmiCharges
     */
    public float getPreEmiCharges() {
        return preEmiCharges;
    }

    /**
     * Setter method for preEmiCharges.
     *
     * @param aPreEmiCharges the new value for preEmiCharges
     */
    public void setPreEmiCharges(float aPreEmiCharges) {
        preEmiCharges = aPreEmiCharges;
    }

	public int getDealerCode() {
		return dealerCode;
	}

	public void setDealerCode(int dealerCode) {
		this.dealerCode = dealerCode;
	}

	public float getInsBase() {
		return insBase;
	}

	public void setInsBase(float insBase) {
		this.insBase = insBase;
	}

	public float getInsTax() {
		return insTax;
	}

	public void setInsTax(float insTax) {
		this.insTax = insTax;
	}

	public float getPfBase() {
		return pfBase;
	}

	public void setPfBase(float pfBase) {
		this.pfBase = pfBase;
	}

	public float getPftax() {
		return pftax;
	}

	public void setPftax(float pftax) {
		this.pftax = pftax;
	}

	public float getPfTail() {
		return pfTail;
	}

	public void setPfTail(float pfTail) {
		this.pfTail = pfTail;
	}

	public float getPfTailBase() {
		return pfTailBase;
	}

	public void setPfTailBase(float pfTailBase) {
		this.pfTailBase = pfTailBase;
	}

	public float getPfTailTax() {
		return pfTailTax;
	}

	public void setPfTailTax(float pfTailTax) {
		this.pfTailTax = pfTailTax;
	}

	public float getCcBase() {
		return ccBase;
	}

	public void setCcBase(float ccBase) {
		this.ccBase = ccBase;
	}

	public float getCcTax() {
		return ccTax;
	}

	public void setCcTax(float ccTax) {
		this.ccTax = ccTax;
	}

	public float getCashCollChargesTail() {
		return cashCollChargesTail;
	}

	public void setCashCollChargesTail(float cashCollChargesTail) {
		this.cashCollChargesTail = cashCollChargesTail;
	}

	public float getCcTailBase() {
		return ccTailBase;
	}

	public void setCcTailBase(float ccTailBase) {
		this.ccTailBase = ccTailBase;
	}

	public float getccTailTax() {
		return ccTailTax;
	}

	public void setccTailTax(float ccTailTax) {
		this.ccTailTax = ccTailTax;
	}
	
	

	public String getCoLender() {
		return coLender;
	}

	public void setCoLender(String coLender) {
		this.coLender = coLender;
	}
	public int getCoLenderPercentage() {
		return coLenderPercentage;
	}
	public void setCoLenderPercentage(int coLenderPercentage) {
		this.coLenderPercentage = coLenderPercentage;
	}
	public float getCoLenderRoi() {
		return coLenderRoi;
	}
	public void setCoLenderRoi(float coLenderRoi) {
		this.coLenderRoi = coLenderRoi;
	}

	public String getCoLenderStatus() {
		return coLenderStatus;
	}

	public void setCoLenderStatus(String coLenderStatus) {
		this.coLenderStatus = coLenderStatus;
	}

	public String getCoLenderDisposition() {
		return coLenderDisposition;
	}

	public void setCoLenderDisposition(String coLenderDisposition) {
		this.coLenderDisposition = coLenderDisposition;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aadhar == null) ? 0 : aadhar.hashCode());
		result = prime * result + Float.floatToIntBits(actualLtv);
		result = prime * result + ((advanceEmi == null) ? 0 : advanceEmi.hashCode());
		result = prime * result + age;
		result = prime * result + ((appAutoapprovedStatus == null) ? 0 : appAutoapprovedStatus.hashCode());
		result = prime * result + ((appAutorejectReason == null) ? 0 : appAutorejectReason.hashCode());
		result = prime * result + ((appBlCode == null) ? 0 : appBlCode.hashCode());
		result = prime * result + ((appCid == null) ? 0 : appCid.hashCode());
		result = prime * result + ((appCreditApprovedStatus == null) ? 0 : appCreditApprovedStatus.hashCode());
		result = prime * result + ((appCreditApprovedTime == null) ? 0 : appCreditApprovedTime.hashCode());
		result = prime * result + ((appCreditApproverName == null) ? 0 : appCreditApproverName.hashCode());
		result = prime * result + ((appCreditLevel == null) ? 0 : appCreditLevel.hashCode());
		result = prime * result + appDeactivate;
		result = prime * result + ((appDeviationReason == null) ? 0 : appDeviationReason.hashCode());
		result = prime * result + ((appEditIntime == null) ? 0 : appEditIntime.hashCode());
		result = prime * result + ((appEditSyntime == null) ? 0 : appEditSyntime.hashCode());
		result = prime * result + ((appGpslocation == null) ? 0 : appGpslocation.hashCode());
		result = prime * result + ((appLac == null) ? 0 : appLac.hashCode());
		result = prime * result + ((appLatitude == null) ? 0 : appLatitude.hashCode());
		result = prime * result + ((appLongitude == null) ? 0 : appLongitude.hashCode());
		result = prime * result + ((appMaritalStatus == null) ? 0 : appMaritalStatus.hashCode());
		result = prime * result + ((appSyntime == null) ? 0 : appSyntime.hashCode());
		result = prime * result + ((appno == null) ? 0 : appno.hashCode());
		result = prime * result + autoApproved;
		result = prime * result + ((avgLandvalue == null) ? 0 : avgLandvalue.hashCode());
		result = prime * result + ((bankaccno == null) ? 0 : bankaccno.hashCode());
		result = prime * result + ((bankbranch == null) ? 0 : bankbranch.hashCode());
		result = prime * result + ((bankifsc == null) ? 0 : bankifsc.hashCode());
		result = prime * result + ((bankname == null) ? 0 : bankname.hashCode());
		result = prime * result
				+ ((borrowercoapplicantresidinginsame == null) ? 0 : borrowercoapplicantresidinginsame.hashCode());
		result = prime * result + ((branchCode == null) ? 0 : branchCode.hashCode());
		result = prime * result + Float.floatToIntBits(cashCollCharges);
		result = prime * result + Float.floatToIntBits(cashCollChargesTail);
		result = prime * result + ((caste == null) ? 0 : caste.hashCode());
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + Float.floatToIntBits(ccBase);
		result = prime * result + Float.floatToIntBits(ccTailBase);
		result = prime * result + Float.floatToIntBits(ccTailTax);
		result = prime * result + Float.floatToIntBits(ccTax);
		result = prime * result + ((cibilDetectFlag == null) ? 0 : cibilDetectFlag.hashCode());
		result = prime * result + ((cibilScore == null) ? 0 : cibilScore.hashCode());
		result = prime * result + ((classOfactivity == null) ? 0 : classOfactivity.hashCode());
		result = prime * result + ((coLender == null) ? 0 : coLender.hashCode());
		result = prime * result + ((coLenderDisposition == null) ? 0 : coLenderDisposition.hashCode());
		result = prime * result + coLenderPercentage;
		result = prime * result + Float.floatToIntBits(coLenderRoi);
		result = prime * result + ((coLenderStatus == null) ? 0 : coLenderStatus.hashCode());
		result = prime * result + ((commLanguage == null) ? 0 : commLanguage.hashCode());
		result = prime * result + Float.floatToIntBits(costPerVehicle);
		result = prime * result + ((creditEditApp == null) ? 0 : creditEditApp.hashCode());
		result = prime * result + ((creditHoldComments == null) ? 0 : creditHoldComments.hashCode());
		result = prime * result + ((creditRecommendedComments == null) ? 0 : creditRecommendedComments.hashCode());
		result = prime * result + ((creditRecommendedStatus == null) ? 0 : creditRecommendedStatus.hashCode());
		result = prime * result + ((creditRecommenderName == null) ? 0 : creditRecommenderName.hashCode());
		result = prime * result + ((crifScore == null) ? 0 : crifScore.hashCode());
		result = prime * result + ((currAddrIsPermAddr == null) ? 0 : currAddrIsPermAddr.hashCode());
		result = prime * result + ((custQualification == null) ? 0 : custQualification.hashCode());
		result = prime * result + ((customerCode == null) ? 0 : customerCode.hashCode());
		result = prime * result + ((customerType == null) ? 0 : customerType.hashCode());
		result = prime * result + dealerCode;
		result = prime * result + Float.floatToIntBits(dealerDisbursementAmount);
		result = prime * result + ((dealerName == null) ? 0 : dealerName.hashCode());
		result = prime * result + ((directMake == null) ? 0 : directMake.hashCode());
		result = prime * result + ((directVariant == null) ? 0 : directVariant.hashCode());
		result = prime * result + ((dob == null) ? 0 : dob.hashCode());
		result = prime * result + Float.floatToIntBits(downPayment);
		result = prime * result + ((drivingLicense == null) ? 0 : drivingLicense.hashCode());
		result = prime * result + ((drivingLicenseExpiryDate == null) ? 0 : drivingLicenseExpiryDate.hashCode());
		result = prime * result + ((ekycstatus == null) ? 0 : ekycstatus.hashCode());
		result = prime * result + ((emailId == null) ? 0 : emailId.hashCode());
		result = prime * result + Float.floatToIntBits(emi);
		result = prime * result + ((entityType == null) ? 0 : entityType.hashCode());
		result = prime * result + ((existingCustomer == null) ? 0 : existingCustomer.hashCode());
		result = prime * result + ((famAware == null) ? 0 : famAware.hashCode());
		result = prime * result + ((fatherName == null) ? 0 : fatherName.hashCode());
		result = prime * result + ((fatherSpouseAddressProof == null) ? 0 : fatherSpouseAddressProof.hashCode());
		result = prime * result
				+ ((fatherSpouseAddressProofValue == null) ? 0 : fatherSpouseAddressProofValue.hashCode());
		result = prime * result + ((fatherSpouseDob == null) ? 0 : fatherSpouseDob.hashCode());
		result = prime * result + ((fatherSpouseIdProof == null) ? 0 : fatherSpouseIdProof.hashCode());
		result = prime * result + ((fatherSpouseIdProofValue == null) ? 0 : fatherSpouseIdProofValue.hashCode());
		result = prime * result + ((fatherSpouseIsGuarantor == null) ? 0 : fatherSpouseIsGuarantor.hashCode());
		result = prime * result + ((fatherSpouseMobileNo == null) ? 0 : fatherSpouseMobileNo.hashCode());
		result = prime * result + ((fatherSpouseName == null) ? 0 : fatherSpouseName.hashCode());
		result = prime * result + ((fatherSpousePanholderName == null) ? 0 : fatherSpousePanholderName.hashCode());
		result = prime * result + ((feName == null) ? 0 : feName.hashCode());
		result = prime * result + ((feState == null) ? 0 : feState.hashCode());
		result = prime * result + ((fiAccessibility == null) ? 0 : fiAccessibility.hashCode());
		result = prime * result + ((fiAccommodation == null) ? 0 : fiAccommodation.hashCode());
		result = prime * result + ((fiAddressConfirm == null) ? 0 : fiAddressConfirm.hashCode());
		result = prime * result + ((fiAssets == null) ? 0 : fiAssets.hashCode());
		result = prime * result + ((fiBusinessName == null) ? 0 : fiBusinessName.hashCode());
		result = prime * result + ((fiCpvStatus == null) ? 0 : fiCpvStatus.hashCode());
		result = prime * result + ((fiDisFromBranch == null) ? 0 : fiDisFromBranch.hashCode());
		result = prime * result + ((fiEarningMember == null) ? 0 : fiEarningMember.hashCode());
		result = prime * result + ((fiEducation == null) ? 0 : fiEducation.hashCode());
		result = prime * result + fiExperience;
		result = prime * result + ((fiExterior == null) ? 0 : fiExterior.hashCode());
		result = prime * result + ((fiFamilyMember == null) ? 0 : fiFamilyMember.hashCode());
		result = prime * result + ((fiInterior == null) ? 0 : fiInterior.hashCode());
		result = prime * result + ((fiIsPermanentAddress == null) ? 0 : fiIsPermanentAddress.hashCode());
		result = prime * result + ((fiMaritialStatus == null) ? 0 : fiMaritialStatus.hashCode());
		result = prime * result + ((fiNeighbourChk == null) ? 0 : fiNeighbourChk.hashCode());
		result = prime * result + ((fiOffAddr == null) ? 0 : fiOffAddr.hashCode());
		result = prime * result + ((fiOffAddr2 == null) ? 0 : fiOffAddr2.hashCode());
		result = prime * result + ((fiOffCity == null) ? 0 : fiOffCity.hashCode());
		result = prime * result + ((fiOffPh == null) ? 0 : fiOffPh.hashCode());
		result = prime * result + ((fiOffPincode == null) ? 0 : fiOffPincode.hashCode());
		result = prime * result + ((fiOffState == null) ? 0 : fiOffState.hashCode());
		result = prime * result + ((fiOffStd == null) ? 0 : fiOffStd.hashCode());
		result = prime * result + ((fiProfile == null) ? 0 : fiProfile.hashCode());
		result = prime * result + ((fiRef1Name == null) ? 0 : fiRef1Name.hashCode());
		result = prime * result + ((fiRef2Name == null) ? 0 : fiRef2Name.hashCode());
		result = prime * result + ((fiRelationship == null) ? 0 : fiRelationship.hashCode());
		result = prime * result + ((fiRemark == null) ? 0 : fiRemark.hashCode());
		result = prime * result + ((fiRentAmount == null) ? 0 : fiRentAmount.hashCode());
		result = prime * result + ((fiResiAddr == null) ? 0 : fiResiAddr.hashCode());
		result = prime * result + ((fiResiAddr2 == null) ? 0 : fiResiAddr2.hashCode());
		result = prime * result + ((fiResiArea == null) ? 0 : fiResiArea.hashCode());
		result = prime * result + ((fiResiCity == null) ? 0 : fiResiCity.hashCode());
		result = prime * result + ((fiResiConstruction == null) ? 0 : fiResiConstruction.hashCode());
		result = prime * result + ((fiResiFathername == null) ? 0 : fiResiFathername.hashCode());
		result = prime * result + ((fiResiLocality == null) ? 0 : fiResiLocality.hashCode());
		result = prime * result + ((fiResiPermAddress == null) ? 0 : fiResiPermAddress.hashCode());
		result = prime * result + ((fiResiPermAddress2 == null) ? 0 : fiResiPermAddress2.hashCode());
		result = prime * result + ((fiResiPermCity == null) ? 0 : fiResiPermCity.hashCode());
		result = prime * result + ((fiResiPermLandmark == null) ? 0 : fiResiPermLandmark.hashCode());
		result = prime * result + ((fiResiPermPincode == null) ? 0 : fiResiPermPincode.hashCode());
		result = prime * result + ((fiResiPermState == null) ? 0 : fiResiPermState.hashCode());
		result = prime * result + ((fiResiPh == null) ? 0 : fiResiPh.hashCode());
		result = prime * result + ((fiResiPincode == null) ? 0 : fiResiPincode.hashCode());
		result = prime * result + ((fiResiState == null) ? 0 : fiResiState.hashCode());
		result = prime * result + ((fiResiStd == null) ? 0 : fiResiStd.hashCode());
		result = prime * result + ((fiResiType == null) ? 0 : fiResiType.hashCode());
		result = prime * result + ((fiResidenceArea == null) ? 0 : fiResidenceArea.hashCode());
		result = prime * result + ((fiResidenceOwnerName == null) ? 0 : fiResidenceOwnerName.hashCode());
		result = prime * result + ((fiResidenceOwnership == null) ? 0 : fiResidenceOwnership.hashCode());
		result = prime * result + ((fiSelf == null) ? 0 : fiSelf.hashCode());
		result = prime * result + ((fiStayingSince == null) ? 0 : fiStayingSince.hashCode());
		result = prime * result + ((fiVerificationName == null) ? 0 : fiVerificationName.hashCode());
		result = prime * result + Float.floatToIntBits(financePerVehicle);
		result = prime * result + ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + ((flooring == null) ? 0 : flooring.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + ((globalOrLocalScheme == null) ? 0 : globalOrLocalScheme.hashCode());
		result = prime * result + Float.floatToIntBits(gridPrice);
		result = prime * result + ((guaRetriggerName == null) ? 0 : guaRetriggerName.hashCode());
		result = prime * result + ((guarantorAvail == null) ? 0 : guarantorAvail.hashCode());
		result = prime * result + ((guarantorCid == null) ? 0 : guarantorCid.hashCode());
		result = prime * result + guarantorCompleted;
		result = prime * result + ((guarantorFirstname == null) ? 0 : guarantorFirstname.hashCode());
		result = prime * result + ((guarantorLac == null) ? 0 : guarantorLac.hashCode());
		result = prime * result + ((guarantorLastname == null) ? 0 : guarantorLastname.hashCode());
		result = prime * result + ((guarantorOrCoapp == null) ? 0 : guarantorOrCoapp.hashCode());
		result = prime * result + ((houseSize == null) ? 0 : houseSize.hashCode());
		result = prime * result + ((houseSizeType == null) ? 0 : houseSizeType.hashCode());
		result = prime * result + ((houseType == null) ? 0 : houseType.hashCode());
		result = prime * result + ((incomeLevel == null) ? 0 : incomeLevel.hashCode());
		result = prime * result + ((incomeNoincome == null) ? 0 : incomeNoincome.hashCode());
		result = prime * result + Float.floatToIntBits(insBase);
		result = prime * result + Float.floatToIntBits(insTax);
		result = prime * result + ((installmentType == null) ? 0 : installmentType.hashCode());
		result = prime * result + Float.floatToIntBits(insuranceAmount);
		result = prime * result + ((intime == null) ? 0 : intime.hashCode());
		result = prime * result + ((kccLoan == null) ? 0 : kccLoan.hashCode());
		result = prime * result + ((kccLoanavail == null) ? 0 : kccLoanavail.hashCode());
		result = prime * result + ((kccLoanbank == null) ? 0 : kccLoanbank.hashCode());
		result = prime * result + ((kyc == null) ? 0 : kyc.hashCode());
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((leadno == null) ? 0 : leadno.hashCode());
		result = prime * result + lmsUpdate;
		result = prime * result + Float.floatToIntBits(loanAmt);
		result = prime * result + ((loantype == null) ? 0 : loantype.hashCode());
		result = prime * result + ((make == null) ? 0 : make.hashCode());
		result = prime * result + ((maritalStatus == null) ? 0 : maritalStatus.hashCode());
		result = prime * result + ((mobileno == null) ? 0 : mobileno.hashCode());
		result = prime * result + ((modeType == null) ? 0 : modeType.hashCode());
		result = prime * result + ((motherName == null) ? 0 : motherName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + noOfBeneficiariesCompleted;
		result = prime * result + noOfBuffalo;
		result = prime * result + noOfBulls;
		result = prime * result + noOfCow;
		result = prime * result + noOfVehicles;
		result = prime * result + ((noofGuarantor == null) ? 0 : noofGuarantor.hashCode());
		result = prime * result + noofGuarantorCompleted;
		result = prime * result + noofVariant;
		result = prime * result + noofVariantCompleted;
		result = prime * result + ((noofinstallments == null) ? 0 : noofinstallments.hashCode());
		result = prime * result + ((offAddr == null) ? 0 : offAddr.hashCode());
		result = prime * result + ((offAddr2 == null) ? 0 : offAddr2.hashCode());
		result = prime * result + ((offCity == null) ? 0 : offCity.hashCode());
		result = prime * result + ((offName == null) ? 0 : offName.hashCode());
		result = prime * result + ((offPh == null) ? 0 : offPh.hashCode());
		result = prime * result + ((offPin == null) ? 0 : offPin.hashCode());
		result = prime * result + ((offState == null) ? 0 : offState.hashCode());
		result = prime * result + ((offStd == null) ? 0 : offStd.hashCode());
		result = prime * result + ((offlandmark == null) ? 0 : offlandmark.hashCode());
		result = prime * result + Float.floatToIntBits(onroadPrice);
		result = prime * result + ((opsApproveStatus == null) ? 0 : opsApproveStatus.hashCode());
		result = prime * result + ((opsApproveTime == null) ? 0 : opsApproveTime.hashCode());
		result = prime * result + ((opsApproverComments == null) ? 0 : opsApproverComments.hashCode());
		result = prime * result + ((opsApproverName == null) ? 0 : opsApproverName.hashCode());
		result = prime * result + ((opsComments == null) ? 0 : opsComments.hashCode());
		result = prime * result + ((opsDisburseTime == null) ? 0 : opsDisburseTime.hashCode());
		result = prime * result + ((opsHoldComments == null) ? 0 : opsHoldComments.hashCode());
		result = prime * result + ((opsStatus == null) ? 0 : opsStatus.hashCode());
		result = prime * result + ((originator == null) ? 0 : originator.hashCode());
		result = prime * result + ((panHolderName == null) ? 0 : panHolderName.hashCode());
		result = prime * result + ((panno == null) ? 0 : panno.hashCode());
		result = prime * result + ((passportExpiryDate == null) ? 0 : passportExpiryDate.hashCode());
		result = prime * result + ((passportNo == null) ? 0 : passportNo.hashCode());
		result = prime * result + ((pddPath == null) ? 0 : pddPath.hashCode());
		result = prime * result + ((permAddr == null) ? 0 : permAddr.hashCode());
		result = prime * result + ((permAddr2 == null) ? 0 : permAddr2.hashCode());
		result = prime * result + ((permCity == null) ? 0 : permCity.hashCode());
		result = prime * result + ((permPincode == null) ? 0 : permPincode.hashCode());
		result = prime * result + ((permState == null) ? 0 : permState.hashCode());
		result = prime * result + ((permlandmark == null) ? 0 : permlandmark.hashCode());
		result = prime * result + Float.floatToIntBits(pfBase);
		result = prime * result + Float.floatToIntBits(pfTail);
		result = prime * result + Float.floatToIntBits(pfTailBase);
		result = prime * result + Float.floatToIntBits(pfTailTax);
		result = prime * result + Float.floatToIntBits(pftax);
		result = prime * result + ((photoNotAllowed == null) ? 0 : photoNotAllowed.hashCode());
		result = prime * result + ((postOfficeNo == null) ? 0 : postOfficeNo.hashCode());
		result = prime * result + Float.floatToIntBits(preEmiCharges);
		result = prime * result + Float.floatToIntBits(processingFee);
		result = prime * result + ((prodDelivered == null) ? 0 : prodDelivered.hashCode());
		result = prime * result + ((prodSubcode == null) ? 0 : prodSubcode.hashCode());
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		result = prime * result + ((productMatch == null) ? 0 : productMatch.hashCode());
		result = prime * result + ((profile == null) ? 0 : profile.hashCode());
		result = prime * result + ((rationCard == null) ? 0 : rationCard.hashCode());
		result = prime * result + ((ref1Mobileno == null) ? 0 : ref1Mobileno.hashCode());
		result = prime * result + ((ref1Name == null) ? 0 : ref1Name.hashCode());
		result = prime * result + ((ref1Relationship == null) ? 0 : ref1Relationship.hashCode());
		result = prime * result + ((ref1addr == null) ? 0 : ref1addr.hashCode());
		result = prime * result + ((ref1city == null) ? 0 : ref1city.hashCode());
		result = prime * result + ((ref1pincode == null) ? 0 : ref1pincode.hashCode());
		result = prime * result + ((ref1state == null) ? 0 : ref1state.hashCode());
		result = prime * result + ((ref2Mobileno == null) ? 0 : ref2Mobileno.hashCode());
		result = prime * result + ((ref2Name == null) ? 0 : ref2Name.hashCode());
		result = prime * result + ((ref2Relationship == null) ? 0 : ref2Relationship.hashCode());
		result = prime * result + ((ref2addr == null) ? 0 : ref2addr.hashCode());
		result = prime * result + ((ref2city == null) ? 0 : ref2city.hashCode());
		result = prime * result + ((ref2state == null) ? 0 : ref2state.hashCode());
		result = prime * result + ((referenceNo == null) ? 0 : referenceNo.hashCode());
		result = prime * result + ((regnoVehi == null) ? 0 : regnoVehi.hashCode());
		result = prime * result + ((relationshipWithHirer == null) ? 0 : relationshipWithHirer.hashCode());
		result = prime * result + ((religion == null) ? 0 : religion.hashCode());
		result = prime * result + ((relookOtp == null) ? 0 : relookOtp.hashCode());
		result = prime * result + ((resiAadhar == null) ? 0 : resiAadhar.hashCode());
		result = prime * result + ((resiAddr == null) ? 0 : resiAddr.hashCode());
		result = prime * result + ((resiAddress2 == null) ? 0 : resiAddress2.hashCode());
		result = prime * result + resiAttempts;
		result = prime * result + ((resiBehaviour == null) ? 0 : resiBehaviour.hashCode());
		result = prime * result + ((resiCid == null) ? 0 : resiCid.hashCode());
		result = prime * result + ((resiCity == null) ? 0 : resiCity.hashCode());
		result = prime * result + ((resiCumOff == null) ? 0 : resiCumOff.hashCode());
		result = prime * result + ((resiDependents == null) ? 0 : resiDependents.hashCode());
		result = prime * result + resiDistance;
		result = prime * result + ((resiDrivingLicense == null) ? 0 : resiDrivingLicense.hashCode());
		result = prime * result + (int) (resiFamIncome ^ (resiFamIncome >>> 32));
		result = prime * result + ((resiFe == null) ? 0 : resiFe.hashCode());
		result = prime * result + ((resiFeAllocated == null) ? 0 : resiFeAllocated.hashCode());
		result = prime * result + ((resiFeedback == null) ? 0 : resiFeedback.hashCode());
		result = prime * result + ((resiFront == null) ? 0 : resiFront.hashCode());
		result = prime * result + ((resiInterior == null) ? 0 : resiInterior.hashCode());
		result = prime * result + ((resiLac == null) ? 0 : resiLac.hashCode());
		result = prime * result + ((resiLandmark == null) ? 0 : resiLandmark.hashCode());
		result = prime * result + Float.floatToIntBits(resiLat);
		result = prime * result + Float.floatToIntBits(resiLatitude);
		result = prime * result + ((resiLeft == null) ? 0 : resiLeft.hashCode());
		result = prime * result + ((resiLoanAmount == null) ? 0 : resiLoanAmount.hashCode());
		result = prime * result + ((resiLocality == null) ? 0 : resiLocality.hashCode());
		result = prime * result + ((resiLocatability == null) ? 0 : resiLocatability.hashCode());
		result = prime * result + Float.floatToIntBits(resiLong);
		result = prime * result + Float.floatToIntBits(resiLongitude);
		result = prime * result + ((resiMobile == null) ? 0 : resiMobile.hashCode());
		result = prime * result + ((resiNeigh1 == null) ? 0 : resiNeigh1.hashCode());
		result = prime * result + ((resiNeigh1Mobileno == null) ? 0 : resiNeigh1Mobileno.hashCode());
		result = prime * result + ((resiNeigh1Name == null) ? 0 : resiNeigh1Name.hashCode());
		result = prime * result + ((resiNeigh2 == null) ? 0 : resiNeigh2.hashCode());
		result = prime * result + ((resiNeigh2Mobileno == null) ? 0 : resiNeigh2Mobileno.hashCode());
		result = prime * result + ((resiNeigh2Name == null) ? 0 : resiNeigh2Name.hashCode());
		result = prime * result + ((resiNeighCheckDone == null) ? 0 : resiNeighCheckDone.hashCode());
		result = prime * result + ((resiNeighType == null) ? 0 : resiNeighType.hashCode());
		result = prime * result + ((resiNeighType2 == null) ? 0 : resiNeighType2.hashCode());
		result = prime * result + (int) (resiNetearning ^ (resiNetearning >>> 32));
		result = prime * result + ((resiObservation == null) ? 0 : resiObservation.hashCode());
		result = prime * result + ((resiOccupation == null) ? 0 : resiOccupation.hashCode());
		result = prime * result + ((resiOwnhouse == null) ? 0 : resiOwnhouse.hashCode());
		result = prime * result + ((resiPanno == null) ? 0 : resiPanno.hashCode());
		result = prime * result + ((resiPassportNo == null) ? 0 : resiPassportNo.hashCode());
		result = prime * result + ((resiPerCur == null) ? 0 : resiPerCur.hashCode());
		result = prime * result + ((resiPersonMet == null) ? 0 : resiPersonMet.hashCode());
		result = prime * result + ((resiPh == null) ? 0 : resiPh.hashCode());
		result = prime * result + ((resiPin == null) ? 0 : resiPin.hashCode());
		result = prime * result + ((resiPostofficeNo == null) ? 0 : resiPostofficeNo.hashCode());
		result = prime * result + ((resiPrevLoan == null) ? 0 : resiPrevLoan.hashCode());
		result = prime * result + ((resiPrevLoanbank == null) ? 0 : resiPrevLoanbank.hashCode());
		result = prime * result + ((resiRationCard == null) ? 0 : resiRationCard.hashCode());
		result = prime * result + ((resiRear == null) ? 0 : resiRear.hashCode());
		result = prime * result + ((resiRecomendation == null) ? 0 : resiRecomendation.hashCode());
		result = prime * result + ((resiRef1Mobileno == null) ? 0 : resiRef1Mobileno.hashCode());
		result = prime * result + ((resiRef1Name == null) ? 0 : resiRef1Name.hashCode());
		result = prime * result + ((resiRef2Mobileno == null) ? 0 : resiRef2Mobileno.hashCode());
		result = prime * result + ((resiRef2Name == null) ? 0 : resiRef2Name.hashCode());
		result = prime * result + ((resiRefcodeEcn == null) ? 0 : resiRefcodeEcn.hashCode());
		result = prime * result + ((resiRejectreason == null) ? 0 : resiRejectreason.hashCode());
		result = prime * result + ((resiResicumoff == null) ? 0 : resiResicumoff.hashCode());
		result = prime * result + ((resiRight == null) ? 0 : resiRight.hashCode());
		result = prime * result + ((resiState == null) ? 0 : resiState.hashCode());
		result = prime * result + ((resiStatus == null) ? 0 : resiStatus.hashCode());
		result = prime * result + ((resiStd == null) ? 0 : resiStd.hashCode());
		result = prime * result + resiVerified;
		result = prime * result + ((resiVerifierComments == null) ? 0 : resiVerifierComments.hashCode());
		result = prime * result + ((resiVerifySyntime == null) ? 0 : resiVerifySyntime.hashCode());
		result = prime * result + ((resiVerifyTime == null) ? 0 : resiVerifyTime.hashCode());
		result = prime * result + ((resiVoterid == null) ? 0 : resiVoterid.hashCode());
		result = prime * result + resiYears;
		result = prime * result + ((resilandmark == null) ? 0 : resilandmark.hashCode());
		result = prime * result + Float.floatToIntBits(roi);
		result = prime * result + ((salorself == null) ? 0 : salorself.hashCode());
		result = prime * result + ((salutation == null) ? 0 : salutation.hashCode());
		result = prime * result + ((schemeType == null) ? 0 : schemeType.hashCode());
		result = prime * result + ((spouseName == null) ? 0 : spouseName.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((tenure == null) ? 0 : tenure.hashCode());
		result = prime * result + Float.floatToIntBits(totalCost);
		result = prime * result + Float.floatToIntBits(totalFinanceAmount);
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((typeofroof == null) ? 0 : typeofroof.hashCode());
		result = prime * result + ((unholdReason == null) ? 0 : unholdReason.hashCode());
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		result = prime * result + ((variant == null) ? 0 : variant.hashCode());
		result = prime * result + ((vehicleUsage == null) ? 0 : vehicleUsage.hashCode());
		result = prime * result + ((verificationStatus == null) ? 0 : verificationStatus.hashCode());
		result = prime * result + ((voterid == null) ? 0 : voterid.hashCode());
		result = prime * result + ((wallType == null) ? 0 : wallType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Application other = (Application) obj;
		if (aadhar == null) {
			if (other.aadhar != null)
				return false;
		} else if (!aadhar.equals(other.aadhar))
			return false;
		if (Float.floatToIntBits(actualLtv) != Float.floatToIntBits(other.actualLtv))
			return false;
		if (advanceEmi == null) {
			if (other.advanceEmi != null)
				return false;
		} else if (!advanceEmi.equals(other.advanceEmi))
			return false;
		if (age != other.age)
			return false;
		if (appAutoapprovedStatus == null) {
			if (other.appAutoapprovedStatus != null)
				return false;
		} else if (!appAutoapprovedStatus.equals(other.appAutoapprovedStatus))
			return false;
		if (appAutorejectReason == null) {
			if (other.appAutorejectReason != null)
				return false;
		} else if (!appAutorejectReason.equals(other.appAutorejectReason))
			return false;
		if (appBlCode == null) {
			if (other.appBlCode != null)
				return false;
		} else if (!appBlCode.equals(other.appBlCode))
			return false;
		if (appCid == null) {
			if (other.appCid != null)
				return false;
		} else if (!appCid.equals(other.appCid))
			return false;
		if (appCreditApprovedStatus == null) {
			if (other.appCreditApprovedStatus != null)
				return false;
		} else if (!appCreditApprovedStatus.equals(other.appCreditApprovedStatus))
			return false;
		if (appCreditApprovedTime == null) {
			if (other.appCreditApprovedTime != null)
				return false;
		} else if (!appCreditApprovedTime.equals(other.appCreditApprovedTime))
			return false;
		if (appCreditApproverName == null) {
			if (other.appCreditApproverName != null)
				return false;
		} else if (!appCreditApproverName.equals(other.appCreditApproverName))
			return false;
		if (appCreditLevel == null) {
			if (other.appCreditLevel != null)
				return false;
		} else if (!appCreditLevel.equals(other.appCreditLevel))
			return false;
		if (appDeactivate != other.appDeactivate)
			return false;
		if (appDeviationReason == null) {
			if (other.appDeviationReason != null)
				return false;
		} else if (!appDeviationReason.equals(other.appDeviationReason))
			return false;
		if (appEditIntime == null) {
			if (other.appEditIntime != null)
				return false;
		} else if (!appEditIntime.equals(other.appEditIntime))
			return false;
		if (appEditSyntime == null) {
			if (other.appEditSyntime != null)
				return false;
		} else if (!appEditSyntime.equals(other.appEditSyntime))
			return false;
		if (appGpslocation == null) {
			if (other.appGpslocation != null)
				return false;
		} else if (!appGpslocation.equals(other.appGpslocation))
			return false;
		if (appLac == null) {
			if (other.appLac != null)
				return false;
		} else if (!appLac.equals(other.appLac))
			return false;
		if (appLatitude == null) {
			if (other.appLatitude != null)
				return false;
		} else if (!appLatitude.equals(other.appLatitude))
			return false;
		if (appLongitude == null) {
			if (other.appLongitude != null)
				return false;
		} else if (!appLongitude.equals(other.appLongitude))
			return false;
		if (appMaritalStatus == null) {
			if (other.appMaritalStatus != null)
				return false;
		} else if (!appMaritalStatus.equals(other.appMaritalStatus))
			return false;
		if (appSyntime == null) {
			if (other.appSyntime != null)
				return false;
		} else if (!appSyntime.equals(other.appSyntime))
			return false;
		if (appno == null) {
			if (other.appno != null)
				return false;
		} else if (!appno.equals(other.appno))
			return false;
		if (autoApproved != other.autoApproved)
			return false;
		if (avgLandvalue == null) {
			if (other.avgLandvalue != null)
				return false;
		} else if (!avgLandvalue.equals(other.avgLandvalue))
			return false;
		if (bankaccno == null) {
			if (other.bankaccno != null)
				return false;
		} else if (!bankaccno.equals(other.bankaccno))
			return false;
		if (bankbranch == null) {
			if (other.bankbranch != null)
				return false;
		} else if (!bankbranch.equals(other.bankbranch))
			return false;
		if (bankifsc == null) {
			if (other.bankifsc != null)
				return false;
		} else if (!bankifsc.equals(other.bankifsc))
			return false;
		if (bankname == null) {
			if (other.bankname != null)
				return false;
		} else if (!bankname.equals(other.bankname))
			return false;
		if (borrowercoapplicantresidinginsame == null) {
			if (other.borrowercoapplicantresidinginsame != null)
				return false;
		} else if (!borrowercoapplicantresidinginsame.equals(other.borrowercoapplicantresidinginsame))
			return false;
		if (branchCode == null) {
			if (other.branchCode != null)
				return false;
		} else if (!branchCode.equals(other.branchCode))
			return false;
		if (Float.floatToIntBits(cashCollCharges) != Float.floatToIntBits(other.cashCollCharges))
			return false;
		if (Float.floatToIntBits(cashCollChargesTail) != Float.floatToIntBits(other.cashCollChargesTail))
			return false;
		if (caste == null) {
			if (other.caste != null)
				return false;
		} else if (!caste.equals(other.caste))
			return false;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (Float.floatToIntBits(ccBase) != Float.floatToIntBits(other.ccBase))
			return false;
		if (Float.floatToIntBits(ccTailBase) != Float.floatToIntBits(other.ccTailBase))
			return false;
		if (Float.floatToIntBits(ccTailTax) != Float.floatToIntBits(other.ccTailTax))
			return false;
		if (Float.floatToIntBits(ccTax) != Float.floatToIntBits(other.ccTax))
			return false;
		if (cibilDetectFlag == null) {
			if (other.cibilDetectFlag != null)
				return false;
		} else if (!cibilDetectFlag.equals(other.cibilDetectFlag))
			return false;
		if (cibilScore == null) {
			if (other.cibilScore != null)
				return false;
		} else if (!cibilScore.equals(other.cibilScore))
			return false;
		if (classOfactivity == null) {
			if (other.classOfactivity != null)
				return false;
		} else if (!classOfactivity.equals(other.classOfactivity))
			return false;
		if (coLender == null) {
			if (other.coLender != null)
				return false;
		} else if (!coLender.equals(other.coLender))
			return false;
		if (coLenderDisposition == null) {
			if (other.coLenderDisposition != null)
				return false;
		} else if (!coLenderDisposition.equals(other.coLenderDisposition))
			return false;
		if (coLenderPercentage != other.coLenderPercentage)
			return false;
		if (Float.floatToIntBits(coLenderRoi) != Float.floatToIntBits(other.coLenderRoi))
			return false;
		if (coLenderStatus == null) {
			if (other.coLenderStatus != null)
				return false;
		} else if (!coLenderStatus.equals(other.coLenderStatus))
			return false;
		if (commLanguage == null) {
			if (other.commLanguage != null)
				return false;
		} else if (!commLanguage.equals(other.commLanguage))
			return false;
		if (Float.floatToIntBits(costPerVehicle) != Float.floatToIntBits(other.costPerVehicle))
			return false;
		if (creditEditApp == null) {
			if (other.creditEditApp != null)
				return false;
		} else if (!creditEditApp.equals(other.creditEditApp))
			return false;
		if (creditHoldComments == null) {
			if (other.creditHoldComments != null)
				return false;
		} else if (!creditHoldComments.equals(other.creditHoldComments))
			return false;
		if (creditRecommendedComments == null) {
			if (other.creditRecommendedComments != null)
				return false;
		} else if (!creditRecommendedComments.equals(other.creditRecommendedComments))
			return false;
		if (creditRecommendedStatus == null) {
			if (other.creditRecommendedStatus != null)
				return false;
		} else if (!creditRecommendedStatus.equals(other.creditRecommendedStatus))
			return false;
		if (creditRecommenderName == null) {
			if (other.creditRecommenderName != null)
				return false;
		} else if (!creditRecommenderName.equals(other.creditRecommenderName))
			return false;
		if (crifScore == null) {
			if (other.crifScore != null)
				return false;
		} else if (!crifScore.equals(other.crifScore))
			return false;
		if (currAddrIsPermAddr == null) {
			if (other.currAddrIsPermAddr != null)
				return false;
		} else if (!currAddrIsPermAddr.equals(other.currAddrIsPermAddr))
			return false;
		if (custQualification == null) {
			if (other.custQualification != null)
				return false;
		} else if (!custQualification.equals(other.custQualification))
			return false;
		if (customerCode == null) {
			if (other.customerCode != null)
				return false;
		} else if (!customerCode.equals(other.customerCode))
			return false;
		if (customerType == null) {
			if (other.customerType != null)
				return false;
		} else if (!customerType.equals(other.customerType))
			return false;
		if (dealerCode != other.dealerCode)
			return false;
		if (Float.floatToIntBits(dealerDisbursementAmount) != Float.floatToIntBits(other.dealerDisbursementAmount))
			return false;
		if (dealerName == null) {
			if (other.dealerName != null)
				return false;
		} else if (!dealerName.equals(other.dealerName))
			return false;
		if (directMake == null) {
			if (other.directMake != null)
				return false;
		} else if (!directMake.equals(other.directMake))
			return false;
		if (directVariant == null) {
			if (other.directVariant != null)
				return false;
		} else if (!directVariant.equals(other.directVariant))
			return false;
		if (dob == null) {
			if (other.dob != null)
				return false;
		} else if (!dob.equals(other.dob))
			return false;
		if (Float.floatToIntBits(downPayment) != Float.floatToIntBits(other.downPayment))
			return false;
		if (drivingLicense == null) {
			if (other.drivingLicense != null)
				return false;
		} else if (!drivingLicense.equals(other.drivingLicense))
			return false;
		if (drivingLicenseExpiryDate == null) {
			if (other.drivingLicenseExpiryDate != null)
				return false;
		} else if (!drivingLicenseExpiryDate.equals(other.drivingLicenseExpiryDate))
			return false;
		if (ekycstatus == null) {
			if (other.ekycstatus != null)
				return false;
		} else if (!ekycstatus.equals(other.ekycstatus))
			return false;
		if (emailId == null) {
			if (other.emailId != null)
				return false;
		} else if (!emailId.equals(other.emailId))
			return false;
		if (Float.floatToIntBits(emi) != Float.floatToIntBits(other.emi))
			return false;
		if (entityType == null) {
			if (other.entityType != null)
				return false;
		} else if (!entityType.equals(other.entityType))
			return false;
		if (existingCustomer == null) {
			if (other.existingCustomer != null)
				return false;
		} else if (!existingCustomer.equals(other.existingCustomer))
			return false;
		if (famAware == null) {
			if (other.famAware != null)
				return false;
		} else if (!famAware.equals(other.famAware))
			return false;
		if (fatherName == null) {
			if (other.fatherName != null)
				return false;
		} else if (!fatherName.equals(other.fatherName))
			return false;
		if (fatherSpouseAddressProof == null) {
			if (other.fatherSpouseAddressProof != null)
				return false;
		} else if (!fatherSpouseAddressProof.equals(other.fatherSpouseAddressProof))
			return false;
		if (fatherSpouseAddressProofValue == null) {
			if (other.fatherSpouseAddressProofValue != null)
				return false;
		} else if (!fatherSpouseAddressProofValue.equals(other.fatherSpouseAddressProofValue))
			return false;
		if (fatherSpouseDob == null) {
			if (other.fatherSpouseDob != null)
				return false;
		} else if (!fatherSpouseDob.equals(other.fatherSpouseDob))
			return false;
		if (fatherSpouseIdProof == null) {
			if (other.fatherSpouseIdProof != null)
				return false;
		} else if (!fatherSpouseIdProof.equals(other.fatherSpouseIdProof))
			return false;
		if (fatherSpouseIdProofValue == null) {
			if (other.fatherSpouseIdProofValue != null)
				return false;
		} else if (!fatherSpouseIdProofValue.equals(other.fatherSpouseIdProofValue))
			return false;
		if (fatherSpouseIsGuarantor == null) {
			if (other.fatherSpouseIsGuarantor != null)
				return false;
		} else if (!fatherSpouseIsGuarantor.equals(other.fatherSpouseIsGuarantor))
			return false;
		if (fatherSpouseMobileNo == null) {
			if (other.fatherSpouseMobileNo != null)
				return false;
		} else if (!fatherSpouseMobileNo.equals(other.fatherSpouseMobileNo))
			return false;
		if (fatherSpouseName == null) {
			if (other.fatherSpouseName != null)
				return false;
		} else if (!fatherSpouseName.equals(other.fatherSpouseName))
			return false;
		if (fatherSpousePanholderName == null) {
			if (other.fatherSpousePanholderName != null)
				return false;
		} else if (!fatherSpousePanholderName.equals(other.fatherSpousePanholderName))
			return false;
		if (feName == null) {
			if (other.feName != null)
				return false;
		} else if (!feName.equals(other.feName))
			return false;
		if (feState == null) {
			if (other.feState != null)
				return false;
		} else if (!feState.equals(other.feState))
			return false;
		if (fiAccessibility == null) {
			if (other.fiAccessibility != null)
				return false;
		} else if (!fiAccessibility.equals(other.fiAccessibility))
			return false;
		if (fiAccommodation == null) {
			if (other.fiAccommodation != null)
				return false;
		} else if (!fiAccommodation.equals(other.fiAccommodation))
			return false;
		if (fiAddressConfirm == null) {
			if (other.fiAddressConfirm != null)
				return false;
		} else if (!fiAddressConfirm.equals(other.fiAddressConfirm))
			return false;
		if (fiAssets == null) {
			if (other.fiAssets != null)
				return false;
		} else if (!fiAssets.equals(other.fiAssets))
			return false;
		if (fiBusinessName == null) {
			if (other.fiBusinessName != null)
				return false;
		} else if (!fiBusinessName.equals(other.fiBusinessName))
			return false;
		if (fiCpvStatus == null) {
			if (other.fiCpvStatus != null)
				return false;
		} else if (!fiCpvStatus.equals(other.fiCpvStatus))
			return false;
		if (fiDisFromBranch == null) {
			if (other.fiDisFromBranch != null)
				return false;
		} else if (!fiDisFromBranch.equals(other.fiDisFromBranch))
			return false;
		if (fiEarningMember == null) {
			if (other.fiEarningMember != null)
				return false;
		} else if (!fiEarningMember.equals(other.fiEarningMember))
			return false;
		if (fiEducation == null) {
			if (other.fiEducation != null)
				return false;
		} else if (!fiEducation.equals(other.fiEducation))
			return false;
		if (fiExperience != other.fiExperience)
			return false;
		if (fiExterior == null) {
			if (other.fiExterior != null)
				return false;
		} else if (!fiExterior.equals(other.fiExterior))
			return false;
		if (fiFamilyMember == null) {
			if (other.fiFamilyMember != null)
				return false;
		} else if (!fiFamilyMember.equals(other.fiFamilyMember))
			return false;
		if (fiInterior == null) {
			if (other.fiInterior != null)
				return false;
		} else if (!fiInterior.equals(other.fiInterior))
			return false;
		if (fiIsPermanentAddress == null) {
			if (other.fiIsPermanentAddress != null)
				return false;
		} else if (!fiIsPermanentAddress.equals(other.fiIsPermanentAddress))
			return false;
		if (fiMaritialStatus == null) {
			if (other.fiMaritialStatus != null)
				return false;
		} else if (!fiMaritialStatus.equals(other.fiMaritialStatus))
			return false;
		if (fiNeighbourChk == null) {
			if (other.fiNeighbourChk != null)
				return false;
		} else if (!fiNeighbourChk.equals(other.fiNeighbourChk))
			return false;
		if (fiOffAddr == null) {
			if (other.fiOffAddr != null)
				return false;
		} else if (!fiOffAddr.equals(other.fiOffAddr))
			return false;
		if (fiOffAddr2 == null) {
			if (other.fiOffAddr2 != null)
				return false;
		} else if (!fiOffAddr2.equals(other.fiOffAddr2))
			return false;
		if (fiOffCity == null) {
			if (other.fiOffCity != null)
				return false;
		} else if (!fiOffCity.equals(other.fiOffCity))
			return false;
		if (fiOffPh == null) {
			if (other.fiOffPh != null)
				return false;
		} else if (!fiOffPh.equals(other.fiOffPh))
			return false;
		if (fiOffPincode == null) {
			if (other.fiOffPincode != null)
				return false;
		} else if (!fiOffPincode.equals(other.fiOffPincode))
			return false;
		if (fiOffState == null) {
			if (other.fiOffState != null)
				return false;
		} else if (!fiOffState.equals(other.fiOffState))
			return false;
		if (fiOffStd == null) {
			if (other.fiOffStd != null)
				return false;
		} else if (!fiOffStd.equals(other.fiOffStd))
			return false;
		if (fiProfile == null) {
			if (other.fiProfile != null)
				return false;
		} else if (!fiProfile.equals(other.fiProfile))
			return false;
		if (fiRef1Name == null) {
			if (other.fiRef1Name != null)
				return false;
		} else if (!fiRef1Name.equals(other.fiRef1Name))
			return false;
		if (fiRef2Name == null) {
			if (other.fiRef2Name != null)
				return false;
		} else if (!fiRef2Name.equals(other.fiRef2Name))
			return false;
		if (fiRelationship == null) {
			if (other.fiRelationship != null)
				return false;
		} else if (!fiRelationship.equals(other.fiRelationship))
			return false;
		if (fiRemark == null) {
			if (other.fiRemark != null)
				return false;
		} else if (!fiRemark.equals(other.fiRemark))
			return false;
		if (fiRentAmount == null) {
			if (other.fiRentAmount != null)
				return false;
		} else if (!fiRentAmount.equals(other.fiRentAmount))
			return false;
		if (fiResiAddr == null) {
			if (other.fiResiAddr != null)
				return false;
		} else if (!fiResiAddr.equals(other.fiResiAddr))
			return false;
		if (fiResiAddr2 == null) {
			if (other.fiResiAddr2 != null)
				return false;
		} else if (!fiResiAddr2.equals(other.fiResiAddr2))
			return false;
		if (fiResiArea == null) {
			if (other.fiResiArea != null)
				return false;
		} else if (!fiResiArea.equals(other.fiResiArea))
			return false;
		if (fiResiCity == null) {
			if (other.fiResiCity != null)
				return false;
		} else if (!fiResiCity.equals(other.fiResiCity))
			return false;
		if (fiResiConstruction == null) {
			if (other.fiResiConstruction != null)
				return false;
		} else if (!fiResiConstruction.equals(other.fiResiConstruction))
			return false;
		if (fiResiFathername == null) {
			if (other.fiResiFathername != null)
				return false;
		} else if (!fiResiFathername.equals(other.fiResiFathername))
			return false;
		if (fiResiLocality == null) {
			if (other.fiResiLocality != null)
				return false;
		} else if (!fiResiLocality.equals(other.fiResiLocality))
			return false;
		if (fiResiPermAddress == null) {
			if (other.fiResiPermAddress != null)
				return false;
		} else if (!fiResiPermAddress.equals(other.fiResiPermAddress))
			return false;
		if (fiResiPermAddress2 == null) {
			if (other.fiResiPermAddress2 != null)
				return false;
		} else if (!fiResiPermAddress2.equals(other.fiResiPermAddress2))
			return false;
		if (fiResiPermCity == null) {
			if (other.fiResiPermCity != null)
				return false;
		} else if (!fiResiPermCity.equals(other.fiResiPermCity))
			return false;
		if (fiResiPermLandmark == null) {
			if (other.fiResiPermLandmark != null)
				return false;
		} else if (!fiResiPermLandmark.equals(other.fiResiPermLandmark))
			return false;
		if (fiResiPermPincode == null) {
			if (other.fiResiPermPincode != null)
				return false;
		} else if (!fiResiPermPincode.equals(other.fiResiPermPincode))
			return false;
		if (fiResiPermState == null) {
			if (other.fiResiPermState != null)
				return false;
		} else if (!fiResiPermState.equals(other.fiResiPermState))
			return false;
		if (fiResiPh == null) {
			if (other.fiResiPh != null)
				return false;
		} else if (!fiResiPh.equals(other.fiResiPh))
			return false;
		if (fiResiPincode == null) {
			if (other.fiResiPincode != null)
				return false;
		} else if (!fiResiPincode.equals(other.fiResiPincode))
			return false;
		if (fiResiState == null) {
			if (other.fiResiState != null)
				return false;
		} else if (!fiResiState.equals(other.fiResiState))
			return false;
		if (fiResiStd == null) {
			if (other.fiResiStd != null)
				return false;
		} else if (!fiResiStd.equals(other.fiResiStd))
			return false;
		if (fiResiType == null) {
			if (other.fiResiType != null)
				return false;
		} else if (!fiResiType.equals(other.fiResiType))
			return false;
		if (fiResidenceArea == null) {
			if (other.fiResidenceArea != null)
				return false;
		} else if (!fiResidenceArea.equals(other.fiResidenceArea))
			return false;
		if (fiResidenceOwnerName == null) {
			if (other.fiResidenceOwnerName != null)
				return false;
		} else if (!fiResidenceOwnerName.equals(other.fiResidenceOwnerName))
			return false;
		if (fiResidenceOwnership == null) {
			if (other.fiResidenceOwnership != null)
				return false;
		} else if (!fiResidenceOwnership.equals(other.fiResidenceOwnership))
			return false;
		if (fiSelf == null) {
			if (other.fiSelf != null)
				return false;
		} else if (!fiSelf.equals(other.fiSelf))
			return false;
		if (fiStayingSince == null) {
			if (other.fiStayingSince != null)
				return false;
		} else if (!fiStayingSince.equals(other.fiStayingSince))
			return false;
		if (fiVerificationName == null) {
			if (other.fiVerificationName != null)
				return false;
		} else if (!fiVerificationName.equals(other.fiVerificationName))
			return false;
		if (Float.floatToIntBits(financePerVehicle) != Float.floatToIntBits(other.financePerVehicle))
			return false;
		if (firstname == null) {
			if (other.firstname != null)
				return false;
		} else if (!firstname.equals(other.firstname))
			return false;
		if (flooring == null) {
			if (other.flooring != null)
				return false;
		} else if (!flooring.equals(other.flooring))
			return false;
		if (gender == null) {
			if (other.gender != null)
				return false;
		} else if (!gender.equals(other.gender))
			return false;
		if (globalOrLocalScheme == null) {
			if (other.globalOrLocalScheme != null)
				return false;
		} else if (!globalOrLocalScheme.equals(other.globalOrLocalScheme))
			return false;
		if (Float.floatToIntBits(gridPrice) != Float.floatToIntBits(other.gridPrice))
			return false;
		if (guaRetriggerName == null) {
			if (other.guaRetriggerName != null)
				return false;
		} else if (!guaRetriggerName.equals(other.guaRetriggerName))
			return false;
		if (guarantorAvail == null) {
			if (other.guarantorAvail != null)
				return false;
		} else if (!guarantorAvail.equals(other.guarantorAvail))
			return false;
		if (guarantorCid == null) {
			if (other.guarantorCid != null)
				return false;
		} else if (!guarantorCid.equals(other.guarantorCid))
			return false;
		if (guarantorCompleted != other.guarantorCompleted)
			return false;
		if (guarantorFirstname == null) {
			if (other.guarantorFirstname != null)
				return false;
		} else if (!guarantorFirstname.equals(other.guarantorFirstname))
			return false;
		if (guarantorLac == null) {
			if (other.guarantorLac != null)
				return false;
		} else if (!guarantorLac.equals(other.guarantorLac))
			return false;
		if (guarantorLastname == null) {
			if (other.guarantorLastname != null)
				return false;
		} else if (!guarantorLastname.equals(other.guarantorLastname))
			return false;
		if (guarantorOrCoapp == null) {
			if (other.guarantorOrCoapp != null)
				return false;
		} else if (!guarantorOrCoapp.equals(other.guarantorOrCoapp))
			return false;
		if (houseSize == null) {
			if (other.houseSize != null)
				return false;
		} else if (!houseSize.equals(other.houseSize))
			return false;
		if (houseSizeType == null) {
			if (other.houseSizeType != null)
				return false;
		} else if (!houseSizeType.equals(other.houseSizeType))
			return false;
		if (houseType == null) {
			if (other.houseType != null)
				return false;
		} else if (!houseType.equals(other.houseType))
			return false;
		if (incomeLevel == null) {
			if (other.incomeLevel != null)
				return false;
		} else if (!incomeLevel.equals(other.incomeLevel))
			return false;
		if (incomeNoincome == null) {
			if (other.incomeNoincome != null)
				return false;
		} else if (!incomeNoincome.equals(other.incomeNoincome))
			return false;
		if (Float.floatToIntBits(insBase) != Float.floatToIntBits(other.insBase))
			return false;
		if (Float.floatToIntBits(insTax) != Float.floatToIntBits(other.insTax))
			return false;
		if (installmentType == null) {
			if (other.installmentType != null)
				return false;
		} else if (!installmentType.equals(other.installmentType))
			return false;
		if (Float.floatToIntBits(insuranceAmount) != Float.floatToIntBits(other.insuranceAmount))
			return false;
		if (intime == null) {
			if (other.intime != null)
				return false;
		} else if (!intime.equals(other.intime))
			return false;
		if (kccLoan == null) {
			if (other.kccLoan != null)
				return false;
		} else if (!kccLoan.equals(other.kccLoan))
			return false;
		if (kccLoanavail == null) {
			if (other.kccLoanavail != null)
				return false;
		} else if (!kccLoanavail.equals(other.kccLoanavail))
			return false;
		if (kccLoanbank == null) {
			if (other.kccLoanbank != null)
				return false;
		} else if (!kccLoanbank.equals(other.kccLoanbank))
			return false;
		if (kyc == null) {
			if (other.kyc != null)
				return false;
		} else if (!kyc.equals(other.kyc))
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (leadno == null) {
			if (other.leadno != null)
				return false;
		} else if (!leadno.equals(other.leadno))
			return false;
		if (lmsUpdate != other.lmsUpdate)
			return false;
		if (Float.floatToIntBits(loanAmt) != Float.floatToIntBits(other.loanAmt))
			return false;
		if (loantype == null) {
			if (other.loantype != null)
				return false;
		} else if (!loantype.equals(other.loantype))
			return false;
		if (make == null) {
			if (other.make != null)
				return false;
		} else if (!make.equals(other.make))
			return false;
		if (maritalStatus == null) {
			if (other.maritalStatus != null)
				return false;
		} else if (!maritalStatus.equals(other.maritalStatus))
			return false;
		if (mobileno == null) {
			if (other.mobileno != null)
				return false;
		} else if (!mobileno.equals(other.mobileno))
			return false;
		if (modeType == null) {
			if (other.modeType != null)
				return false;
		} else if (!modeType.equals(other.modeType))
			return false;
		if (motherName == null) {
			if (other.motherName != null)
				return false;
		} else if (!motherName.equals(other.motherName))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (noOfBeneficiariesCompleted != other.noOfBeneficiariesCompleted)
			return false;
		if (noOfBuffalo != other.noOfBuffalo)
			return false;
		if (noOfBulls != other.noOfBulls)
			return false;
		if (noOfCow != other.noOfCow)
			return false;
		if (noOfVehicles != other.noOfVehicles)
			return false;
		if (noofGuarantor == null) {
			if (other.noofGuarantor != null)
				return false;
		} else if (!noofGuarantor.equals(other.noofGuarantor))
			return false;
		if (noofGuarantorCompleted != other.noofGuarantorCompleted)
			return false;
		if (noofVariant != other.noofVariant)
			return false;
		if (noofVariantCompleted != other.noofVariantCompleted)
			return false;
		if (noofinstallments == null) {
			if (other.noofinstallments != null)
				return false;
		} else if (!noofinstallments.equals(other.noofinstallments))
			return false;
		if (offAddr == null) {
			if (other.offAddr != null)
				return false;
		} else if (!offAddr.equals(other.offAddr))
			return false;
		if (offAddr2 == null) {
			if (other.offAddr2 != null)
				return false;
		} else if (!offAddr2.equals(other.offAddr2))
			return false;
		if (offCity == null) {
			if (other.offCity != null)
				return false;
		} else if (!offCity.equals(other.offCity))
			return false;
		if (offName == null) {
			if (other.offName != null)
				return false;
		} else if (!offName.equals(other.offName))
			return false;
		if (offPh == null) {
			if (other.offPh != null)
				return false;
		} else if (!offPh.equals(other.offPh))
			return false;
		if (offPin == null) {
			if (other.offPin != null)
				return false;
		} else if (!offPin.equals(other.offPin))
			return false;
		if (offState == null) {
			if (other.offState != null)
				return false;
		} else if (!offState.equals(other.offState))
			return false;
		if (offStd == null) {
			if (other.offStd != null)
				return false;
		} else if (!offStd.equals(other.offStd))
			return false;
		if (offlandmark == null) {
			if (other.offlandmark != null)
				return false;
		} else if (!offlandmark.equals(other.offlandmark))
			return false;
		if (Float.floatToIntBits(onroadPrice) != Float.floatToIntBits(other.onroadPrice))
			return false;
		if (opsApproveStatus == null) {
			if (other.opsApproveStatus != null)
				return false;
		} else if (!opsApproveStatus.equals(other.opsApproveStatus))
			return false;
		if (opsApproveTime == null) {
			if (other.opsApproveTime != null)
				return false;
		} else if (!opsApproveTime.equals(other.opsApproveTime))
			return false;
		if (opsApproverComments == null) {
			if (other.opsApproverComments != null)
				return false;
		} else if (!opsApproverComments.equals(other.opsApproverComments))
			return false;
		if (opsApproverName == null) {
			if (other.opsApproverName != null)
				return false;
		} else if (!opsApproverName.equals(other.opsApproverName))
			return false;
		if (opsComments == null) {
			if (other.opsComments != null)
				return false;
		} else if (!opsComments.equals(other.opsComments))
			return false;
		if (opsDisburseTime == null) {
			if (other.opsDisburseTime != null)
				return false;
		} else if (!opsDisburseTime.equals(other.opsDisburseTime))
			return false;
		if (opsHoldComments == null) {
			if (other.opsHoldComments != null)
				return false;
		} else if (!opsHoldComments.equals(other.opsHoldComments))
			return false;
		if (opsStatus == null) {
			if (other.opsStatus != null)
				return false;
		} else if (!opsStatus.equals(other.opsStatus))
			return false;
		if (originator == null) {
			if (other.originator != null)
				return false;
		} else if (!originator.equals(other.originator))
			return false;
		if (panHolderName == null) {
			if (other.panHolderName != null)
				return false;
		} else if (!panHolderName.equals(other.panHolderName))
			return false;
		if (panno == null) {
			if (other.panno != null)
				return false;
		} else if (!panno.equals(other.panno))
			return false;
		if (passportExpiryDate == null) {
			if (other.passportExpiryDate != null)
				return false;
		} else if (!passportExpiryDate.equals(other.passportExpiryDate))
			return false;
		if (passportNo == null) {
			if (other.passportNo != null)
				return false;
		} else if (!passportNo.equals(other.passportNo))
			return false;
		if (pddPath == null) {
			if (other.pddPath != null)
				return false;
		} else if (!pddPath.equals(other.pddPath))
			return false;
		if (permAddr == null) {
			if (other.permAddr != null)
				return false;
		} else if (!permAddr.equals(other.permAddr))
			return false;
		if (permAddr2 == null) {
			if (other.permAddr2 != null)
				return false;
		} else if (!permAddr2.equals(other.permAddr2))
			return false;
		if (permCity == null) {
			if (other.permCity != null)
				return false;
		} else if (!permCity.equals(other.permCity))
			return false;
		if (permPincode == null) {
			if (other.permPincode != null)
				return false;
		} else if (!permPincode.equals(other.permPincode))
			return false;
		if (permState == null) {
			if (other.permState != null)
				return false;
		} else if (!permState.equals(other.permState))
			return false;
		if (permlandmark == null) {
			if (other.permlandmark != null)
				return false;
		} else if (!permlandmark.equals(other.permlandmark))
			return false;
		if (Float.floatToIntBits(pfBase) != Float.floatToIntBits(other.pfBase))
			return false;
		if (Float.floatToIntBits(pfTail) != Float.floatToIntBits(other.pfTail))
			return false;
		if (Float.floatToIntBits(pfTailBase) != Float.floatToIntBits(other.pfTailBase))
			return false;
		if (Float.floatToIntBits(pfTailTax) != Float.floatToIntBits(other.pfTailTax))
			return false;
		if (Float.floatToIntBits(pftax) != Float.floatToIntBits(other.pftax))
			return false;
		if (photoNotAllowed == null) {
			if (other.photoNotAllowed != null)
				return false;
		} else if (!photoNotAllowed.equals(other.photoNotAllowed))
			return false;
		if (postOfficeNo == null) {
			if (other.postOfficeNo != null)
				return false;
		} else if (!postOfficeNo.equals(other.postOfficeNo))
			return false;
		if (Float.floatToIntBits(preEmiCharges) != Float.floatToIntBits(other.preEmiCharges))
			return false;
		if (Float.floatToIntBits(processingFee) != Float.floatToIntBits(other.processingFee))
			return false;
		if (prodDelivered == null) {
			if (other.prodDelivered != null)
				return false;
		} else if (!prodDelivered.equals(other.prodDelivered))
			return false;
		if (prodSubcode == null) {
			if (other.prodSubcode != null)
				return false;
		} else if (!prodSubcode.equals(other.prodSubcode))
			return false;
		if (product == null) {
			if (other.product != null)
				return false;
		} else if (!product.equals(other.product))
			return false;
		if (productMatch == null) {
			if (other.productMatch != null)
				return false;
		} else if (!productMatch.equals(other.productMatch))
			return false;
		if (profile == null) {
			if (other.profile != null)
				return false;
		} else if (!profile.equals(other.profile))
			return false;
		if (rationCard == null) {
			if (other.rationCard != null)
				return false;
		} else if (!rationCard.equals(other.rationCard))
			return false;
		if (ref1Mobileno == null) {
			if (other.ref1Mobileno != null)
				return false;
		} else if (!ref1Mobileno.equals(other.ref1Mobileno))
			return false;
		if (ref1Name == null) {
			if (other.ref1Name != null)
				return false;
		} else if (!ref1Name.equals(other.ref1Name))
			return false;
		if (ref1Relationship == null) {
			if (other.ref1Relationship != null)
				return false;
		} else if (!ref1Relationship.equals(other.ref1Relationship))
			return false;
		if (ref1addr == null) {
			if (other.ref1addr != null)
				return false;
		} else if (!ref1addr.equals(other.ref1addr))
			return false;
		if (ref1city == null) {
			if (other.ref1city != null)
				return false;
		} else if (!ref1city.equals(other.ref1city))
			return false;
		if (ref1pincode == null) {
			if (other.ref1pincode != null)
				return false;
		} else if (!ref1pincode.equals(other.ref1pincode))
			return false;
		if (ref1state == null) {
			if (other.ref1state != null)
				return false;
		} else if (!ref1state.equals(other.ref1state))
			return false;
		if (ref2Mobileno == null) {
			if (other.ref2Mobileno != null)
				return false;
		} else if (!ref2Mobileno.equals(other.ref2Mobileno))
			return false;
		if (ref2Name == null) {
			if (other.ref2Name != null)
				return false;
		} else if (!ref2Name.equals(other.ref2Name))
			return false;
		if (ref2Relationship == null) {
			if (other.ref2Relationship != null)
				return false;
		} else if (!ref2Relationship.equals(other.ref2Relationship))
			return false;
		if (ref2addr == null) {
			if (other.ref2addr != null)
				return false;
		} else if (!ref2addr.equals(other.ref2addr))
			return false;
		if (ref2city == null) {
			if (other.ref2city != null)
				return false;
		} else if (!ref2city.equals(other.ref2city))
			return false;
		if (ref2state == null) {
			if (other.ref2state != null)
				return false;
		} else if (!ref2state.equals(other.ref2state))
			return false;
		if (referenceNo == null) {
			if (other.referenceNo != null)
				return false;
		} else if (!referenceNo.equals(other.referenceNo))
			return false;
		if (regnoVehi == null) {
			if (other.regnoVehi != null)
				return false;
		} else if (!regnoVehi.equals(other.regnoVehi))
			return false;
		if (relationshipWithHirer == null) {
			if (other.relationshipWithHirer != null)
				return false;
		} else if (!relationshipWithHirer.equals(other.relationshipWithHirer))
			return false;
		if (religion == null) {
			if (other.religion != null)
				return false;
		} else if (!religion.equals(other.religion))
			return false;
		if (relookOtp == null) {
			if (other.relookOtp != null)
				return false;
		} else if (!relookOtp.equals(other.relookOtp))
			return false;
		if (resiAadhar == null) {
			if (other.resiAadhar != null)
				return false;
		} else if (!resiAadhar.equals(other.resiAadhar))
			return false;
		if (resiAddr == null) {
			if (other.resiAddr != null)
				return false;
		} else if (!resiAddr.equals(other.resiAddr))
			return false;
		if (resiAddress2 == null) {
			if (other.resiAddress2 != null)
				return false;
		} else if (!resiAddress2.equals(other.resiAddress2))
			return false;
		if (resiAttempts != other.resiAttempts)
			return false;
		if (resiBehaviour == null) {
			if (other.resiBehaviour != null)
				return false;
		} else if (!resiBehaviour.equals(other.resiBehaviour))
			return false;
		if (resiCid == null) {
			if (other.resiCid != null)
				return false;
		} else if (!resiCid.equals(other.resiCid))
			return false;
		if (resiCity == null) {
			if (other.resiCity != null)
				return false;
		} else if (!resiCity.equals(other.resiCity))
			return false;
		if (resiCumOff == null) {
			if (other.resiCumOff != null)
				return false;
		} else if (!resiCumOff.equals(other.resiCumOff))
			return false;
		if (resiDependents == null) {
			if (other.resiDependents != null)
				return false;
		} else if (!resiDependents.equals(other.resiDependents))
			return false;
		if (resiDistance != other.resiDistance)
			return false;
		if (resiDrivingLicense == null) {
			if (other.resiDrivingLicense != null)
				return false;
		} else if (!resiDrivingLicense.equals(other.resiDrivingLicense))
			return false;
		if (resiFamIncome != other.resiFamIncome)
			return false;
		if (resiFe == null) {
			if (other.resiFe != null)
				return false;
		} else if (!resiFe.equals(other.resiFe))
			return false;
		if (resiFeAllocated == null) {
			if (other.resiFeAllocated != null)
				return false;
		} else if (!resiFeAllocated.equals(other.resiFeAllocated))
			return false;
		if (resiFeedback == null) {
			if (other.resiFeedback != null)
				return false;
		} else if (!resiFeedback.equals(other.resiFeedback))
			return false;
		if (resiFront == null) {
			if (other.resiFront != null)
				return false;
		} else if (!resiFront.equals(other.resiFront))
			return false;
		if (resiInterior == null) {
			if (other.resiInterior != null)
				return false;
		} else if (!resiInterior.equals(other.resiInterior))
			return false;
		if (resiLac == null) {
			if (other.resiLac != null)
				return false;
		} else if (!resiLac.equals(other.resiLac))
			return false;
		if (resiLandmark == null) {
			if (other.resiLandmark != null)
				return false;
		} else if (!resiLandmark.equals(other.resiLandmark))
			return false;
		if (Float.floatToIntBits(resiLat) != Float.floatToIntBits(other.resiLat))
			return false;
		if (Float.floatToIntBits(resiLatitude) != Float.floatToIntBits(other.resiLatitude))
			return false;
		if (resiLeft == null) {
			if (other.resiLeft != null)
				return false;
		} else if (!resiLeft.equals(other.resiLeft))
			return false;
		if (resiLoanAmount == null) {
			if (other.resiLoanAmount != null)
				return false;
		} else if (!resiLoanAmount.equals(other.resiLoanAmount))
			return false;
		if (resiLocality == null) {
			if (other.resiLocality != null)
				return false;
		} else if (!resiLocality.equals(other.resiLocality))
			return false;
		if (resiLocatability == null) {
			if (other.resiLocatability != null)
				return false;
		} else if (!resiLocatability.equals(other.resiLocatability))
			return false;
		if (Float.floatToIntBits(resiLong) != Float.floatToIntBits(other.resiLong))
			return false;
		if (Float.floatToIntBits(resiLongitude) != Float.floatToIntBits(other.resiLongitude))
			return false;
		if (resiMobile == null) {
			if (other.resiMobile != null)
				return false;
		} else if (!resiMobile.equals(other.resiMobile))
			return false;
		if (resiNeigh1 == null) {
			if (other.resiNeigh1 != null)
				return false;
		} else if (!resiNeigh1.equals(other.resiNeigh1))
			return false;
		if (resiNeigh1Mobileno == null) {
			if (other.resiNeigh1Mobileno != null)
				return false;
		} else if (!resiNeigh1Mobileno.equals(other.resiNeigh1Mobileno))
			return false;
		if (resiNeigh1Name == null) {
			if (other.resiNeigh1Name != null)
				return false;
		} else if (!resiNeigh1Name.equals(other.resiNeigh1Name))
			return false;
		if (resiNeigh2 == null) {
			if (other.resiNeigh2 != null)
				return false;
		} else if (!resiNeigh2.equals(other.resiNeigh2))
			return false;
		if (resiNeigh2Mobileno == null) {
			if (other.resiNeigh2Mobileno != null)
				return false;
		} else if (!resiNeigh2Mobileno.equals(other.resiNeigh2Mobileno))
			return false;
		if (resiNeigh2Name == null) {
			if (other.resiNeigh2Name != null)
				return false;
		} else if (!resiNeigh2Name.equals(other.resiNeigh2Name))
			return false;
		if (resiNeighCheckDone == null) {
			if (other.resiNeighCheckDone != null)
				return false;
		} else if (!resiNeighCheckDone.equals(other.resiNeighCheckDone))
			return false;
		if (resiNeighType == null) {
			if (other.resiNeighType != null)
				return false;
		} else if (!resiNeighType.equals(other.resiNeighType))
			return false;
		if (resiNeighType2 == null) {
			if (other.resiNeighType2 != null)
				return false;
		} else if (!resiNeighType2.equals(other.resiNeighType2))
			return false;
		if (resiNetearning != other.resiNetearning)
			return false;
		if (resiObservation == null) {
			if (other.resiObservation != null)
				return false;
		} else if (!resiObservation.equals(other.resiObservation))
			return false;
		if (resiOccupation == null) {
			if (other.resiOccupation != null)
				return false;
		} else if (!resiOccupation.equals(other.resiOccupation))
			return false;
		if (resiOwnhouse == null) {
			if (other.resiOwnhouse != null)
				return false;
		} else if (!resiOwnhouse.equals(other.resiOwnhouse))
			return false;
		if (resiPanno == null) {
			if (other.resiPanno != null)
				return false;
		} else if (!resiPanno.equals(other.resiPanno))
			return false;
		if (resiPassportNo == null) {
			if (other.resiPassportNo != null)
				return false;
		} else if (!resiPassportNo.equals(other.resiPassportNo))
			return false;
		if (resiPerCur == null) {
			if (other.resiPerCur != null)
				return false;
		} else if (!resiPerCur.equals(other.resiPerCur))
			return false;
		if (resiPersonMet == null) {
			if (other.resiPersonMet != null)
				return false;
		} else if (!resiPersonMet.equals(other.resiPersonMet))
			return false;
		if (resiPh == null) {
			if (other.resiPh != null)
				return false;
		} else if (!resiPh.equals(other.resiPh))
			return false;
		if (resiPin == null) {
			if (other.resiPin != null)
				return false;
		} else if (!resiPin.equals(other.resiPin))
			return false;
		if (resiPostofficeNo == null) {
			if (other.resiPostofficeNo != null)
				return false;
		} else if (!resiPostofficeNo.equals(other.resiPostofficeNo))
			return false;
		if (resiPrevLoan == null) {
			if (other.resiPrevLoan != null)
				return false;
		} else if (!resiPrevLoan.equals(other.resiPrevLoan))
			return false;
		if (resiPrevLoanbank == null) {
			if (other.resiPrevLoanbank != null)
				return false;
		} else if (!resiPrevLoanbank.equals(other.resiPrevLoanbank))
			return false;
		if (resiRationCard == null) {
			if (other.resiRationCard != null)
				return false;
		} else if (!resiRationCard.equals(other.resiRationCard))
			return false;
		if (resiRear == null) {
			if (other.resiRear != null)
				return false;
		} else if (!resiRear.equals(other.resiRear))
			return false;
		if (resiRecomendation == null) {
			if (other.resiRecomendation != null)
				return false;
		} else if (!resiRecomendation.equals(other.resiRecomendation))
			return false;
		if (resiRef1Mobileno == null) {
			if (other.resiRef1Mobileno != null)
				return false;
		} else if (!resiRef1Mobileno.equals(other.resiRef1Mobileno))
			return false;
		if (resiRef1Name == null) {
			if (other.resiRef1Name != null)
				return false;
		} else if (!resiRef1Name.equals(other.resiRef1Name))
			return false;
		if (resiRef2Mobileno == null) {
			if (other.resiRef2Mobileno != null)
				return false;
		} else if (!resiRef2Mobileno.equals(other.resiRef2Mobileno))
			return false;
		if (resiRef2Name == null) {
			if (other.resiRef2Name != null)
				return false;
		} else if (!resiRef2Name.equals(other.resiRef2Name))
			return false;
		if (resiRefcodeEcn == null) {
			if (other.resiRefcodeEcn != null)
				return false;
		} else if (!resiRefcodeEcn.equals(other.resiRefcodeEcn))
			return false;
		if (resiRejectreason == null) {
			if (other.resiRejectreason != null)
				return false;
		} else if (!resiRejectreason.equals(other.resiRejectreason))
			return false;
		if (resiResicumoff == null) {
			if (other.resiResicumoff != null)
				return false;
		} else if (!resiResicumoff.equals(other.resiResicumoff))
			return false;
		if (resiRight == null) {
			if (other.resiRight != null)
				return false;
		} else if (!resiRight.equals(other.resiRight))
			return false;
		if (resiState == null) {
			if (other.resiState != null)
				return false;
		} else if (!resiState.equals(other.resiState))
			return false;
		if (resiStatus == null) {
			if (other.resiStatus != null)
				return false;
		} else if (!resiStatus.equals(other.resiStatus))
			return false;
		if (resiStd == null) {
			if (other.resiStd != null)
				return false;
		} else if (!resiStd.equals(other.resiStd))
			return false;
		if (resiVerified != other.resiVerified)
			return false;
		if (resiVerifierComments == null) {
			if (other.resiVerifierComments != null)
				return false;
		} else if (!resiVerifierComments.equals(other.resiVerifierComments))
			return false;
		if (resiVerifySyntime == null) {
			if (other.resiVerifySyntime != null)
				return false;
		} else if (!resiVerifySyntime.equals(other.resiVerifySyntime))
			return false;
		if (resiVerifyTime == null) {
			if (other.resiVerifyTime != null)
				return false;
		} else if (!resiVerifyTime.equals(other.resiVerifyTime))
			return false;
		if (resiVoterid == null) {
			if (other.resiVoterid != null)
				return false;
		} else if (!resiVoterid.equals(other.resiVoterid))
			return false;
		if (resiYears != other.resiYears)
			return false;
		if (resilandmark == null) {
			if (other.resilandmark != null)
				return false;
		} else if (!resilandmark.equals(other.resilandmark))
			return false;
		if (Float.floatToIntBits(roi) != Float.floatToIntBits(other.roi))
			return false;
		if (salorself == null) {
			if (other.salorself != null)
				return false;
		} else if (!salorself.equals(other.salorself))
			return false;
		if (salutation == null) {
			if (other.salutation != null)
				return false;
		} else if (!salutation.equals(other.salutation))
			return false;
		if (schemeType == null) {
			if (other.schemeType != null)
				return false;
		} else if (!schemeType.equals(other.schemeType))
			return false;
		if (spouseName == null) {
			if (other.spouseName != null)
				return false;
		} else if (!spouseName.equals(other.spouseName))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (tenure == null) {
			if (other.tenure != null)
				return false;
		} else if (!tenure.equals(other.tenure))
			return false;
		if (Float.floatToIntBits(totalCost) != Float.floatToIntBits(other.totalCost))
			return false;
		if (Float.floatToIntBits(totalFinanceAmount) != Float.floatToIntBits(other.totalFinanceAmount))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (typeofroof == null) {
			if (other.typeofroof != null)
				return false;
		} else if (!typeofroof.equals(other.typeofroof))
			return false;
		if (unholdReason == null) {
			if (other.unholdReason != null)
				return false;
		} else if (!unholdReason.equals(other.unholdReason))
			return false;
		if (unit == null) {
			if (other.unit != null)
				return false;
		} else if (!unit.equals(other.unit))
			return false;
		if (variant == null) {
			if (other.variant != null)
				return false;
		} else if (!variant.equals(other.variant))
			return false;
		if (vehicleUsage == null) {
			if (other.vehicleUsage != null)
				return false;
		} else if (!vehicleUsage.equals(other.vehicleUsage))
			return false;
		if (verificationStatus == null) {
			if (other.verificationStatus != null)
				return false;
		} else if (!verificationStatus.equals(other.verificationStatus))
			return false;
		if (voterid == null) {
			if (other.voterid != null)
				return false;
		} else if (!voterid.equals(other.voterid))
			return false;
		if (wallType == null) {
			if (other.wallType != null)
				return false;
		} else if (!wallType.equals(other.wallType))
			return false;
		return true;
	}

		

}
