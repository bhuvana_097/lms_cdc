package com.vg.lms.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="hht_ach")
public class ReceiptOnline  implements Serializable {

 /**
	 * 
	 */
	private static final long serialVersionUID = -829903763419568603L;
	@Id
 @GeneratedValue(strategy=GenerationType.IDENTITY)
 @Column(unique=true, nullable=false, precision=10)
 private int id;
	
 @Column(name="book_no", length=10)
 private String bookNo;
 
 @Column(name="receipt_no", precision=19)
 private long receiptNo;
 
 @Column(name="loan_no", length=15)
 private String loanNo;
 
 @Column(name="customer_name", length=10)
 private String customerName;
 
 @Column(name="umrn", length=100)
 private String umrN;
 
 @Column(name="receipt_amount", precision=15, scale=2)
 private float receiptAmount;
 
 @Column(name="receipt_date")
 private Date receiptDate;
 
 @Column(name="lms_absorb_time")
 private Date lmsAbsorbTime;
 
   @Column(name="bounce", length=50)
 private String bouncE;
   
   @Column(name="real_receipt_date")
   	private Date realReceiptDate;
   
   @Column(name="cancelled", length=2)
 private String cancelleD;
   
 @Column(name="cancel_remarks", length=100)
 private String cancelRemarks;

 @Column(name="create_user", length=50)
 private String createUser;
 
 @Column(name="modify_user", length=50)
 private String modifyUser;
 
 @Column(name="create_time")
 private Date createTime;
 
 @Column(name="modify_time")
 private Date modifyTime;
 
 public ReceiptOnline() {
     super();
 }

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getBookNo() {
	return bookNo;
}

public void setBookNo(String bookNo) {
	this.bookNo = bookNo;
}

public long getReceiptNo() {
	return receiptNo;
}

public void setReceiptNo(long receiptNo) {
	this.receiptNo = receiptNo;
}

public String getLoanNo() {
	return loanNo;
}

public void setLoanNo(String loanNo) {
	this.loanNo = loanNo;
}

public String getCustomerName() {
	return customerName;
}

public void setCustomerName(String customerName) {
	this.customerName = customerName;
}

public String getUmrN() {
	return umrN;
}

public void setUmrN(String umrN) {
	this.umrN = umrN;
}

public float getReceiptAmount() {
	return receiptAmount;
}

public void setReceiptAmount(float receiptAmount) {
	this.receiptAmount = receiptAmount;
}

public Date getReceiptDate() {
	return receiptDate;
}

public void setReceiptDate(Date receiptDate) {
	this.receiptDate = receiptDate;
}

public Date getLmsAbsorbTime() {
	return lmsAbsorbTime;
}

public void setLmsAbsorbTime(Date lmsAbsorbTime) {
	this.lmsAbsorbTime = lmsAbsorbTime;
}

public String getBouncE() {
	return bouncE;
}

public void setBouncE(String bouncE) {
	this.bouncE = bouncE;
}

public Date getRealReceiptDate() {
	return realReceiptDate;
}

public void setRealReceiptDate(Date realReceiptDate) {
	this.realReceiptDate = realReceiptDate;
}

public String getCancelleD() {
	return cancelleD;
}

public void setCancelleD(String cancelleD) {
	this.cancelleD = cancelleD;
}

public String getCancelRemarks() {
	return cancelRemarks;
}

public void setCancelRemarks(String cancelRemarks) {
	this.cancelRemarks = cancelRemarks;
}

public String getCreateUser() {
	return createUser;
}

public void setCreateUser(String createUser) {
	this.createUser = createUser;
}

public String getModifyUser() {
	return modifyUser;
}

public void setModifyUser(String modifyUser) {
	this.modifyUser = modifyUser;
}

public Date getCreateTime() {
	return createTime;
}

public void setCreateTime(Date createTime) {
	this.createTime = createTime;
}

public Date getModifyTime() {
	return modifyTime;
}

public void setModifyTime(Date modifyTime) {
	this.modifyTime = modifyTime;
}

public static long getSerialversionuid() {
	return serialVersionUID;
}
}
