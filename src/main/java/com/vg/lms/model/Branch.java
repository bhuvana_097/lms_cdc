// Generated with g9.

package com.vg.lms.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="fe_branchmaster")
public class Branch implements Serializable {

  
    /**
	 * 
	 */
	private static final long serialVersionUID = 2361447040422992317L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, length=26)
    private int s_no;
	 @Column(name="branch_name", length=100)
    private String branchName;
    @Column(name="branch_code", length=100)
    private String branchCode;
    @Column(name="zone", length=30)
    private String zone;
    @Column(name="state", length=30)
    private String state;
       

    /** Default constructor. */
    public Branch() {
        super();
    }

   
    public int getSNo() {
        return s_no;
    }
    
    public void setSNo(int as_no) {
    	s_no = as_no;
    }

    public String getbranchName() {
        return branchName;
    }

   
    public void setbranchName(String abranchName) {
    	branchName = abranchName;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String abranchCode) {
    	branchCode = abranchCode;
    }

    
    public String getZone() {
        return zone;
    }

    
    public void setZone(String azone) {
    	zone = azone;
    }

    
    public String getState() {
        return state;
    }

    
    public void setState(String astate) {
        state = astate;
    }

    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + s_no;
		result = prime * result + ((branchName == null) ? 0 : branchName.hashCode());
		result = prime * result + ((branchCode == null) ? 0 : branchCode.hashCode());
		result = prime * result + ((zone == null) ? 0 : zone.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Branch other = (Branch) obj;
		if (s_no != other.s_no)
			return false;
		if (branchName == null) {
			if (other.branchName != null)
				return false;
		} else if (!branchName.equals(other.branchName))
			return false;
		if (branchCode == null) {
			if (other.branchCode != null)
				return false;
		} else if (!branchCode.equals(other.branchCode))
			return false;
		if (zone == null) {
			if (other.zone != null)
				return false;
		} else if (!zone.equals(other.zone))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		
		return true;
	}
}
