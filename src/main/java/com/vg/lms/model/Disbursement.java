// Generated with g9.

package com.vg.lms.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity(name="lms_disbursement")
public class Disbursement implements Serializable {

   

    /**
	 * 
	 */
	private static final long serialVersionUID = 786432762581004856L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(precision=10, scale=2)
    private float amount;
    @Column(name="txn_id", length=20)
    private String txnId;
    @Column(length=20)
    private String bank;
    @Column(length=20)
    private String status;
    @Column(name="disb_date")
    private Date disbDate;
    @Column(name="create_user", length=10)
    private String createUser;
    @Transient
    @Column(name="create_time")
    private Date createTime;
    @Column(name="modify_user", length=10)
    private String modifyUser;
    
    @ManyToOne
    @JoinColumn(name="loan_id")
    private Loan loan;

    /** Default constructor. */
    public Disbursement() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for amount.
     *
     * @return the current value of amount
     */
    public float getAmount() {
        return amount;
    }

    /**
     * Setter method for amount.
     *
     * @param aAmount the new value for amount
     */
    public void setAmount(float aAmount) {
        amount = aAmount;
    }

    /**
     * Access method for txnId.
     *
     * @return the current value of txnId
     */
    public String getTxnId() {
        return txnId;
    }

    /**
     * Setter method for txnId.
     *
     * @param aTxnId the new value for txnId
     */
    public void setTxnId(String aTxnId) {
        txnId = aTxnId;
    }

    /**
     * Access method for bank.
     *
     * @return the current value of bank
     */
    public String getBank() {
        return bank;
    }

    /**
     * Setter method for bank.
     *
     * @param aBank the new value for bank
     */
    public void setBank(String aBank) {
        bank = aBank;
    }

    /**
     * Access method for status.
     *
     * @return the current value of status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Setter method for status.
     *
     * @param aStatus the new value for status
     */
    public void setStatus(String aStatus) {
        status = aStatus;
    }

    /**
     * Access method for createUser.
     *
     * @return the current value of createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * Setter method for createUser.
     *
     * @param aCreateUser the new value for createUser
     */
    public void setCreateUser(String aCreateUser) {
        createUser = aCreateUser;
    }

    /**
     * Access method for modifyUser.
     *
     * @return the current value of modifyUser
     */
    public String getModifyUser() {
        return modifyUser;
    }

    /**
     * Setter method for modifyUser.
     *
     * @param aModifyUser the new value for modifyUser
     */
    public void setModifyUser(String aModifyUser) {
        modifyUser = aModifyUser;
    }

   /**
     * Access method for loan.
     *
     * @return the current value of loan
     */
    public Loan getLoan() {
        return loan;
    }

    /**
     * Setter method for loan.
     *
     * @param aLoan the new value for loan
     */
    public void setLoan(Loan aLoan) {
        loan = aLoan;
    }

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getDisbDate() {
		return disbDate;
	}

	public void setDisbDate(Date disbDate) {
		this.disbDate = disbDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(amount);
		result = prime * result + ((bank == null) ? 0 : bank.hashCode());
		result = prime * result + ((createTime == null) ? 0 : createTime.hashCode());
		result = prime * result + ((createUser == null) ? 0 : createUser.hashCode());
		result = prime * result + ((disbDate == null) ? 0 : disbDate.hashCode());
		result = prime * result + id;
		result = prime * result + ((loan == null) ? 0 : loan.hashCode());
		result = prime * result + ((modifyUser == null) ? 0 : modifyUser.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((txnId == null) ? 0 : txnId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Disbursement other = (Disbursement) obj;
		if (Float.floatToIntBits(amount) != Float.floatToIntBits(other.amount))
			return false;
		if (bank == null) {
			if (other.bank != null)
				return false;
		} else if (!bank.equals(other.bank))
			return false;
		if (createTime == null) {
			if (other.createTime != null)
				return false;
		} else if (!createTime.equals(other.createTime))
			return false;
		if (createUser == null) {
			if (other.createUser != null)
				return false;
		} else if (!createUser.equals(other.createUser))
			return false;
		if (disbDate == null) {
			if (other.disbDate != null)
				return false;
		} else if (!disbDate.equals(other.disbDate))
			return false;
		if (id != other.id)
			return false;
		if (loan == null) {
			if (other.loan != null)
				return false;
		} else if (!loan.equals(other.loan))
			return false;
		if (modifyUser == null) {
			if (other.modifyUser != null)
				return false;
		} else if (!modifyUser.equals(other.modifyUser))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (txnId == null) {
			if (other.txnId != null)
				return false;
		} else if (!txnId.equals(other.txnId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Disbursement [id=" + id + ", amount=" + amount + ", txnId=" + txnId + ", bank=" + bank + ", status="
				+ status + ", disbDate=" + disbDate + ", createUser=" + createUser + ", createTime=" + createTime
				+ ", modifyUser=" + modifyUser + ", loan=" + loan + "]";
	}

	

}
