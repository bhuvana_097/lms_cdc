package com.vg.lms.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="loan_morat_details")
public class CovidMorat implements Serializable {

 /**
	 * 
	 */
	private static final long serialVersionUID = 3592237728515885260L;
	@Id
 @GeneratedValue(strategy=GenerationType.IDENTITY)
 @Column(unique=true, nullable=false, precision=10)
 private int id;
 
 @Column(name="loan_no", length=30)
 private String loanNo;
 @Column(name="morat_month", precision=15)
 private int moratMonth;
 @Column(name="morat_year", precision=15)
 private int moratYear;
 
 @Column(name="morat_int", length=15,scale=2)
 private float moratInt;
 @Column(name="loan_pos", length=15,scale=2)
 private float loanPos;
 @Column(name="create_time")
 private Date createTime;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getLoanNo() {
	return loanNo;
}
public void setLoanNo(String loanNo) {
	this.loanNo = loanNo;
}
public int getMoratMonth() {
	return moratMonth;
}
public void setMoratMonth(int moratMonth) {
	this.moratMonth = moratMonth;
}
public int getMoratYear() {
	return moratYear;
}
public void setMoratYear(int moratYear) {
	this.moratYear = moratYear;
}
public float getMoratInt() {
	return moratInt;
}
public void setMoratInt(float moratInt) {
	this.moratInt = moratInt;
}
public Date getCreateTime() {
	return createTime;
}
public void setCreateTime(Date createTime) {
	this.createTime = createTime;
}
public static long getSerialversionuid() {
	return serialVersionUID;
}
public float getLoanPos() {
	return loanPos;
}
public void setLoanPos(float loanPos) {
	this.loanPos = loanPos;
}
@Override
public String toString() {
	return "CovidMorat [id=" + id + ", loanNo=" + loanNo + ", moratMonth=" + moratMonth + ", moratYear=" + moratYear
			+ ", moratInt=" + moratInt + ", loanPOS=" + loanPos + ", createTime=" + createTime + ", getId()=" + getId()
			+ ", getLoanNo()=" + getLoanNo() + ", getMoratMonth()=" + getMoratMonth() + ", getMoratYear()="
			+ getMoratYear() + ", getMoratInt()=" + getMoratInt() + ", getCreateTime()=" + getCreateTime()
			+ ", getLoanPos()=" + getLoanPos() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
			+ ", toString()=" + super.toString() + "]";
}
}
