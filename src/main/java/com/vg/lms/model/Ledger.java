// Generated with g9.

package com.vg.lms.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="lms_gl")
public class Ledger implements Serializable {    

    /**
	 * 
	 */
	private static final long serialVersionUID = 5007808362223566008L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    /*@Column(name="AccountTrxID", precision=19)
    private int accountTrxId;*/
   /* @Column(name="TrxRowID", precision=19)
    private int trxRowId;*/
    @Column(name="TrxBranchID", length=6)
    private String trxBranchId;
    @Column(name="TrxBatchID", length=8)
    private String trxBatchId;
   /* @Column(name="SerialID", precision=10)
    private int serialId;*/
    @Column(name="OurBranchID", length=6)
    private String ourBranchId;
    @Column(name="AccountTypeID", nullable=false, length=25)
    private String accountTypeId;
    @Column(name="AccountID", nullable=false, length=32)
    private String accountId;
    @Column(name="loan_no", length=32)
    private String loanNo;
    @Column(name="ProductID", nullable=false, length=6)
    private String productId;
    @Column(name="TrxDate", nullable=false)
    private Date trxDate;
    @Column(name="TrxTypeID", nullable=false, length=25)
    private String trxTypeId;
    @Column(name="ValueDate")
    private Date valueDate;
    @Column(precision=19, scale=2)
    private float amount;
    @Column(name="description_id", length=10)
    private String descriptionId;
    @Column(length=255)
    private String description;
    /*@Column(name="TrxPrinted", precision=3)
    private int trxPrinted;*/
    @Column(name="MainGLID", length=20)
    private String mainGlid;
    @Column(name="ContraGLID", length=20)
    private String contraGlid;
    @Column(name="reference_no", length=15)
    private String referenceNo;
    @Column(length=500)
    private String remarks;
    @Column(name="book_no", length=10)
    private String bookNo;
    @Column(name="receipt_no", length=20)
    private String receiptNo;
   /* @Column(name="ModuleID", precision=5)
    private int moduleId;*/
   /* @Column(name="TrxCodeID", precision=3)
    private int trxCodeId;*/
    @Column(name="CreatedBy", length=25)
    private String createdBy;
    @Column(name="create_time")
    private Date createTime;
    @Column(name="SupervisedBy", length=25)
    private String supervisedBy;
    @Column(name="SupervisedOn")
    private Date supervisedOn;
    @Column(name="GroupID", length=20)
    private String groupId;
   /* @Column(name="TrxBatchSLNo", precision=5)
    private int trxBatchSlNo;*/
  /*  @Column(name="LoanSeries", precision=5)
    private int loanSeries;*/
    @Column(name="TrxSourceID", length=25)
    private String trxSourceId;
    @Column(name="TrxContraGLID", length=20)
    private String trxContraGlid;
    @Column(name="modify_time")
    private Date modifyTime;

    /** Default constructor. */
    public Ledger() {
        super();
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	/*public int getAccountTrxId() {
		return accountTrxId;
	}

	public void setAccountTrxId(int accountTrxId) {
		this.accountTrxId = accountTrxId;
	}*/

/*	public int getTrxRowId() {
		return trxRowId;
	}

	public void setTrxRowId(int trxRowId) {
		this.trxRowId = trxRowId;
	}*/

	public String getTrxBranchId() {
		return trxBranchId;
	}

	public void setTrxBranchId(String trxBranchId) {
		this.trxBranchId = trxBranchId;
	}

	public String getTrxBatchId() {
		return trxBatchId;
	}

	public void setTrxBatchId(String trxBatchId) {
		this.trxBatchId = trxBatchId;
	}

	/*public int getSerialId() {
		return serialId;
	}

	public void setSerialId(int serialId) {
		this.serialId = serialId;
	}
*/
	public String getOurBranchId() {
		return ourBranchId;
	}

	public void setOurBranchId(String ourBranchId) {
		this.ourBranchId = ourBranchId;
	}

	public String getAccountTypeId() {
		return accountTypeId;
	}

	public void setAccountTypeId(String accountTypeId) {
		this.accountTypeId = accountTypeId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getLoanNo() {
		return loanNo;
	}

	public void setLoanNo(String loanNo) {
		this.loanNo = loanNo;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public Date getTrxDate() {
		return trxDate;
	}

	public void setTrxDate(Date trxDate) {
		this.trxDate = trxDate;
	}

	public String getTrxTypeId() {
		return trxTypeId;
	}

	public void setTrxTypeId(String trxTypeId) {
		this.trxTypeId = trxTypeId;
	}

	public Date getValueDate() {
		return valueDate;
	}

	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public String getDescriptionId() {
		return descriptionId;
	}

	public void setDescriptionId(String descriptionId) {
		this.descriptionId = descriptionId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/*public int getTrxPrinted() {
		return trxPrinted;
	}

	public void setTrxPrinted(int trxPrinted) {
		this.trxPrinted = trxPrinted;
	}*/

	public String getMainGlid() {
		return mainGlid;
	}

	public void setMainGlid(String mainGlid) {
		this.mainGlid = mainGlid;
	}

	public String getContraGlid() {
		return contraGlid;
	}

	public void setContraGlid(String contraGlid) {
		this.contraGlid = contraGlid;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getBookNo() {
		return bookNo;
	}

	public void setBookNo(String bookNo) {
		this.bookNo = bookNo;
	}

	public String getReceiptNo() {
		return receiptNo;
	}

	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}

	/*public int getModuleId() {
		return moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}*/

	/*public int getTrxCodeId() {
		return trxCodeId;
	}

	public void setTrxCodeId(int trxCodeId) {
		this.trxCodeId = trxCodeId;
	}*/

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getSupervisedBy() {
		return supervisedBy;
	}

	public void setSupervisedBy(String supervisedBy) {
		this.supervisedBy = supervisedBy;
	}

	public Date getSupervisedOn() {
		return supervisedOn;
	}

	public void setSupervisedOn(Date supervisedOn) {
		this.supervisedOn = supervisedOn;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	/*public int getTrxBatchSlNo() {
		return trxBatchSlNo;
	}

	public void setTrxBatchSlNo(int trxBatchSlNo) {
		this.trxBatchSlNo = trxBatchSlNo;
	}*/

	/*public int getLoanSeries() {
		return loanSeries;
	}

	public void setLoanSeries(int loanSeries) {
		this.loanSeries = loanSeries;
	}
*/
	public String getTrxSourceId() {
		return trxSourceId;
	}

	public void setTrxSourceId(String trxSourceId) {
		this.trxSourceId = trxSourceId;
	}

	public String getTrxContraGlid() {
		return trxContraGlid;
	}

	public void setTrxContraGlid(String trxContraGlid) {
		this.trxContraGlid = trxContraGlid;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountId == null) ? 0 : accountId.hashCode());
		//result = prime * result + accountTrxId;
		result = prime * result + ((accountTypeId == null) ? 0 : accountTypeId.hashCode());
		result = prime * result + Float.floatToIntBits(amount);
		result = prime * result + ((bookNo == null) ? 0 : bookNo.hashCode());
		result = prime * result + ((contraGlid == null) ? 0 : contraGlid.hashCode());
		result = prime * result + ((createTime == null) ? 0 : createTime.hashCode());
		result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((descriptionId == null) ? 0 : descriptionId.hashCode());
		result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
		result = prime * result + id;
		result = prime * result + ((loanNo == null) ? 0 : loanNo.hashCode());
		//result = prime * result + loanSeries;
		result = prime * result + ((mainGlid == null) ? 0 : mainGlid.hashCode());
		result = prime * result + ((modifyTime == null) ? 0 : modifyTime.hashCode());
		//result = prime * result + moduleId;
		result = prime * result + ((ourBranchId == null) ? 0 : ourBranchId.hashCode());
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		result = prime * result + ((receiptNo == null) ? 0 : receiptNo.hashCode());
		result = prime * result + ((referenceNo == null) ? 0 : referenceNo.hashCode());
		result = prime * result + ((remarks == null) ? 0 : remarks.hashCode());
		//result = prime * result + serialId;
		result = prime * result + ((supervisedBy == null) ? 0 : supervisedBy.hashCode());
		result = prime * result + ((supervisedOn == null) ? 0 : supervisedOn.hashCode());
		result = prime * result + ((trxBatchId == null) ? 0 : trxBatchId.hashCode());
		//result = prime * result + trxBatchSlNo;
		result = prime * result + ((trxBranchId == null) ? 0 : trxBranchId.hashCode());
		//result = prime * result + trxCodeId;
		result = prime * result + ((trxContraGlid == null) ? 0 : trxContraGlid.hashCode());
		result = prime * result + ((trxDate == null) ? 0 : trxDate.hashCode());
		//result = prime * result + trxPrinted;
		//result = prime * result + trxRowId;
		result = prime * result + ((trxSourceId == null) ? 0 : trxSourceId.hashCode());
		result = prime * result + ((trxTypeId == null) ? 0 : trxTypeId.hashCode());
		result = prime * result + ((valueDate == null) ? 0 : valueDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ledger other = (Ledger) obj;
		if (accountId == null) {
			if (other.accountId != null)
				return false;
		} else if (!accountId.equals(other.accountId))
			return false;
	/*	if (accountTrxId != other.accountTrxId)
			return false;*/
		if (accountTypeId == null) {
			if (other.accountTypeId != null)
				return false;
		} else if (!accountTypeId.equals(other.accountTypeId))
			return false;
		if (Float.floatToIntBits(amount) != Float.floatToIntBits(other.amount))
			return false;
		if (bookNo == null) {
			if (other.bookNo != null)
				return false;
		} else if (!bookNo.equals(other.bookNo))
			return false;
		if (contraGlid == null) {
			if (other.contraGlid != null)
				return false;
		} else if (!contraGlid.equals(other.contraGlid))
			return false;
		if (createTime == null) {
			if (other.createTime != null)
				return false;
		} else if (!createTime.equals(other.createTime))
			return false;
		if (createdBy == null) {
			if (other.createdBy != null)
				return false;
		} else if (!createdBy.equals(other.createdBy))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (descriptionId == null) {
			if (other.descriptionId != null)
				return false;
		} else if (!descriptionId.equals(other.descriptionId))
			return false;
		if (groupId == null) {
			if (other.groupId != null)
				return false;
		} else if (!groupId.equals(other.groupId))
			return false;
		if (id != other.id)
			return false;
		if (loanNo == null) {
			if (other.loanNo != null)
				return false;
		} else if (!loanNo.equals(other.loanNo))
			return false;
		/*if (loanSeries != other.loanSeries)
			return false;*/
		if (mainGlid == null) {
			if (other.mainGlid != null)
				return false;
		} else if (!mainGlid.equals(other.mainGlid))
			return false;
		if (modifyTime == null) {
			if (other.modifyTime != null)
				return false;
		} else if (!modifyTime.equals(other.modifyTime))
			return false;
		/*if (moduleId != other.moduleId)
			return false;*/
		if (ourBranchId == null) {
			if (other.ourBranchId != null)
				return false;
		} else if (!ourBranchId.equals(other.ourBranchId))
			return false;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		if (receiptNo == null) {
			if (other.receiptNo != null)
				return false;
		} else if (!receiptNo.equals(other.receiptNo))
			return false;
		if (referenceNo == null) {
			if (other.referenceNo != null)
				return false;
		} else if (!referenceNo.equals(other.referenceNo))
			return false;
		if (remarks == null) {
			if (other.remarks != null)
				return false;
		} else if (!remarks.equals(other.remarks))
			return false;
		/*if (serialId != other.serialId)
			return false;*/
		if (supervisedBy == null) {
			if (other.supervisedBy != null)
				return false;
		} else if (!supervisedBy.equals(other.supervisedBy))
			return false;
		if (supervisedOn == null) {
			if (other.supervisedOn != null)
				return false;
		} else if (!supervisedOn.equals(other.supervisedOn))
			return false;
		if (trxBatchId == null) {
			if (other.trxBatchId != null)
				return false;
		} else if (!trxBatchId.equals(other.trxBatchId))
			return false;
		/*if (trxBatchSlNo != other.trxBatchSlNo)
			return false;*/
		if (trxBranchId == null) {
			if (other.trxBranchId != null)
				return false;
		} else if (!trxBranchId.equals(other.trxBranchId))
			return false;
		/*if (trxCodeId != other.trxCodeId)
			return false;*/
		if (trxContraGlid == null) {
			if (other.trxContraGlid != null)
				return false;
		} else if (!trxContraGlid.equals(other.trxContraGlid))
			return false;
		if (trxDate == null) {
			if (other.trxDate != null)
				return false;
		} else if (!trxDate.equals(other.trxDate))
			return false;
		/*if (trxPrinted != other.trxPrinted)
			return false;
		if (trxRowId != other.trxRowId)
			return false;*/
		if (trxSourceId == null) {
			if (other.trxSourceId != null)
				return false;
		} else if (!trxSourceId.equals(other.trxSourceId))
			return false;
		if (trxTypeId == null) {
			if (other.trxTypeId != null)
				return false;
		} else if (!trxTypeId.equals(other.trxTypeId))
			return false;
		if (valueDate == null) {
			if (other.valueDate != null)
				return false;
		} else if (!valueDate.equals(other.valueDate))
			return false;
		return true;
	}
/*
	@Override
	public String toString() {
		return "Ledger [id=" + id + ", accountTrxId=" + accountTrxId + ", trxRowId=" + trxRowId + ", trxBranchId="
				+ trxBranchId + ", trxBatchId=" + trxBatchId + ", serialId=" + serialId + ", ourBranchId=" + ourBranchId
				+ ", accountTypeId=" + accountTypeId + ", accountId=" + accountId + ", loanNo=" + loanNo
				+ ", productId=" + productId + ", trxDate=" + trxDate + ", trxTypeId=" + trxTypeId + ", valueDate="
				+ valueDate + ", amount=" + amount + ", descriptionId=" + descriptionId + ", description=" + description
				+ ", trxPrinted=" + trxPrinted + ", mainGlid=" + mainGlid + ", contraGlid=" + contraGlid
				+ ", referenceNo=" + referenceNo + ", remarks=" + remarks + ", bookNo=" + bookNo + ", receiptNo="
				+ receiptNo + ", moduleId=" + moduleId + ", trxCodeId=" + trxCodeId + ", createdBy=" + createdBy
				+ ", createTime=" + createTime + ", supervisedBy=" + supervisedBy + ", supervisedOn=" + supervisedOn
				+ ", groupId=" + groupId + ", trxBatchSlNo=" + trxBatchSlNo + ", loanSeries=" + loanSeries
				+ ", trxSourceId=" + trxSourceId + ", trxContraGlid=" + trxContraGlid + ", modifyTime=" + modifyTime
				+ "]";
	}*/

	@Override
	public String toString() {
		return "Ledger [id=" + id + ", trxBranchId=" + trxBranchId + ", trxBatchId=" + trxBatchId + ", ourBranchId="
				+ ourBranchId + ", accountTypeId=" + accountTypeId + ", accountId=" + accountId + ", loanNo=" + loanNo
				+ ", productId=" + productId + ", trxDate=" + trxDate + ", trxTypeId=" + trxTypeId + ", valueDate="
				+ valueDate + ", amount=" + amount + ", descriptionId=" + descriptionId + ", description=" + description
				+ ", mainGlid=" + mainGlid + ", contraGlid=" + contraGlid + ", referenceNo=" + referenceNo
				+ ", remarks=" + remarks + ", bookNo=" + bookNo + ", receiptNo=" + receiptNo + ", createdBy="
				+ createdBy + ", createTime=" + createTime + ", supervisedBy=" + supervisedBy + ", supervisedOn="
				+ supervisedOn + ", groupId=" + groupId + ", trxSourceId=" + trxSourceId + ", trxContraGlid="
				+ trxContraGlid + ", modifyTime=" + modifyTime + "]";
	}

	

    
}
