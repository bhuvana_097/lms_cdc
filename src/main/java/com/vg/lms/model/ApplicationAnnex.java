// Generated with g9.

package com.vg.lms.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="application_annex")
public class ApplicationAnnex implements Serializable {

  
    /**
	 * 
	 */
	private static final long serialVersionUID = 2361447040422992317L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, length=26)
    private String appno;
    	
	@Column(name="document_charge", precision=15, scale=2)
    private float documentCharge;
    @Column(name="document_charge_base", precision=15, scale=2)
    private float documentChargeBase;
    @Column(name="document_charge_tax", precision=15, scale=2)
    private float documentChargeTax;
	public String getAppno() {
		return appno;
	}
	public void setAppno(String appno) {
		this.appno = appno;
	}
	public float getDocumentCharge() {
		return documentCharge;
	}
	public void setDocumentCharge(float documentCharge) {
		this.documentCharge = documentCharge;
	}
	public float getDocumentChargeBase() {
		return documentChargeBase;
	}
	public void setDocumentChargeBase(float documentChargeBase) {
		this.documentChargeBase = documentChargeBase;
	}
	public float getDocumentChargeTax() {
		return documentChargeTax;
	}
	public void setDocumentChargeTax(float documentChargeTax) {
		this.documentChargeTax = documentChargeTax;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
   
}