package com.vg.lms.model;


import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.joda.time.DateTime;


@Entity(name="lms_user_log_user")
public class LmsUserLogUser {

	
	private static final long serialVersionUID = 2361447040422992317L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, length=26)
	 private int id;
    @Column(name="lms_user_log_userid", length=32)
    private String lmsUserLogUserid;
    
    @Column(name="lms_user_log_date")
    private Date lmsUserLogDate;
    
    @Column(name="lms_user_login_time")
    private Date lmsUserLoginTime;
    
    @Column(name="lms_user_logout_time")
    private Date lmsUserLogoutTime;
   
    @Column(name="lms_user_log_userrole", length=32)
    private String lmsUserLogUserrole;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLmsUserLogUserid() {
		return lmsUserLogUserid;
	}
	public void setLmsUserLogUserid(String lmsUserLogUserid) {
		this.lmsUserLogUserid = lmsUserLogUserid;
	}
	public Date getLmsUserLogDate() {
		return lmsUserLogDate;
	}
	public void setLmsUserLogDate(Date lmsUserLogDate) {
		this.lmsUserLogDate = lmsUserLogDate;
	}
	public Date getLmsUserLoginTime() {
		return lmsUserLoginTime;
	}
	public void setLmsUserLoginTime(Date lmsUserLoginTime) {
		this.lmsUserLoginTime = lmsUserLoginTime;
	}
	public Date getLmsUserLogoutTime() {
		return lmsUserLogoutTime;
	}
	public void setLmsUserLogoutTime(Date lmsUserLogoutTime) {
		this.lmsUserLogoutTime = lmsUserLogoutTime;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getLmsUserLogUserrole() {
		return lmsUserLogUserrole;
	}
	public void setLmsUserLogUserrole(String lmsUserLogUserrole) {
		this.lmsUserLogUserrole = lmsUserLogUserrole;
	}
}
