package com.vg.lms;

import java.util.List;

import com.vg.lms.model.UserModuleMaster;

public class UserModuleMasterDetails {
	public static List<UserModuleMaster> moduleDetails;

	public static List<UserModuleMaster> getModuleDetails() {
		return moduleDetails;
	}

	public static void setModuleDetails(List<UserModuleMaster> moduleDetails) {
		UserModuleMasterDetails.moduleDetails = moduleDetails;
	}
	
}
