package com.vg.lms.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vg.lms.bean.SOABean;
import com.vg.lms.controller.WelcomeController;
import com.vg.lms.model.Charges;
import com.vg.lms.model.Ledger;
import com.vg.lms.model.Loan;
import com.vg.lms.model.SOACreditDebitScheduleModel;
import com.vg.lms.model.SOAModel;
import com.vg.lms.repository.LedgerRepository;
import com.vg.lms.repository.LoanRepository;

@Component
public class SOADetailsServiceImpl implements SOADetailsService{
	
	@Autowired
	private LoanRepository loanRepository;
	
	@Autowired
	private LedgerRepository ledgerRepository;
	
	private final Logger log = LoggerFactory.getLogger(WelcomeController.class);


	@Override
	public List<SOABean> fetchSOADetails(int loanid) {
		List<SOABean> SOABeanList = new ArrayList<>();
		Loan loan = loanRepository.findById(loanid).get();
		List<Ledger> ledgerEntries = ledgerRepository.findByaccountIdOrderByValueDateAsc(loan.getLoanNo());
		/*for (Ledger ledgerEntry : ledgerEntries) {
			log.info("ledgerEntry"+ledgerEntry.toString());
		}*/
		float balance = 0.0f;
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		if(ledgerEntries != null && ledgerEntries.size() > 0) {
			for (Ledger ledgerEntry : ledgerEntries) {
				SOABean temp_soaBean = new SOABean();
				temp_soaBean.setValuedate(sdf.format(ledgerEntry.getValueDate()));
				temp_soaBean.setParticulars(ledgerEntry.getDescription());
				
				if(ledgerEntry.getAmount() > 0) {
					temp_soaBean.setCredit(Float.toString(ledgerEntry.getAmount()));
				} else {
					temp_soaBean.setDebit(Float.toString( Math.abs(ledgerEntry.getAmount())));
				}
				balance += ledgerEntry.getAmount();

				if(balance < 0) {
					temp_soaBean.setBalance("("+Float.toString( Math.abs(balance))+")");
				} else {
					temp_soaBean.setBalance(Float.toString(balance));
				}
				temp_soaBean.setBalance(String.valueOf(balance));
				temp_soaBean.setBookno(ledgerEntry.getBookNo());
				temp_soaBean.setReceiptno(ledgerEntry.getReceiptNo());
				SOABeanList.add(temp_soaBean);
			}
	}
		return SOABeanList;
	}

}
