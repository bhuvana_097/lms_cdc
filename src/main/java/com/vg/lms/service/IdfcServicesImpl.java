package com.vg.lms.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.stream.Collectors;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.transaction.Transactional;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;


import com.coh.lms.IDFC.IdfcRequest;
import com.coh.lms.IDFC.IdfcResponse;
import com.coh.lms.IDFC.IdfcServiceImpl;
import com.coh.lms.IDFC.InvoiceDetail;
import com.coh.lms.IDFC.MobileRequest;
import com.coh.lms.IDFC.MobileResponse;
import com.coh.lms.IDFC.MsgBdy;
import com.coh.lms.IDFC.MsgHdr;
import com.coh.lms.IDFC.PaymentReq;
import com.coh.lms.IDFC.PaymentTransactionReq;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.vg.lms.incred.init.InitResponse;
import com.vg.lms.model.Dealer;
import com.vg.lms.model.IdfcTransaction;
import com.vg.lms.repository.IdfcTranRespository;

import okhttp3.OkHttpClient;



@Service
public class IdfcServicesImpl implements IdfcServices {
	 @Autowired
	  private IdfcTranRespository idfcRepository;
		
	private final Logger log = LoggerFactory.getLogger(IdfcServiceImpl.class);
	private int refno = 000001;
	 
	@Override
	public MobileResponse IdfcValidation(MobileRequest mobileRequest) {
		 
		log.info("Mobile Request:" + mobileRequest);
		IdfcTransaction idfctrans = new IdfcTransaction();
		Dealer dealer = new Dealer();
		MobileResponse response = new MobileResponse();
		
		Date date=java.util.Calendar.getInstance().getTime();  
		Calendar now = Calendar.getInstance();
		
		idfctrans.setAccountNumber(mobileRequest.getcUSTOMERBANKACCOUNT());
		idfctrans.setCustomerName(mobileRequest.getcUSTOMERNAME());
		idfctrans.setIfscCode(mobileRequest.getiFSCCODE());
		idfctrans.setuName(mobileRequest.getuNAME());
		idfctrans.setAppNo(mobileRequest.getaPPLNO());
		idfctrans.setLeadNo(mobileRequest.getlEADNO());
		String beneAccNo = mobileRequest.getcUSTOMERBANKACCOUNT();
		String beneName = mobileRequest.getcUSTOMERNAME();
		String beneAddr1;String beneAddr2;
		 String valueDate;
		 
		 String tranAmount;
		 String clientCode="ORANGE";
		 String invNumber;
		String ifsc = mobileRequest.getiFSCCODE();
		 refno++;
		 String Refnumb=new String("REFNO2019");
	
		return response;
		
	}


	
	
	
	
	private static SSLContext checkExpire() {
		 PublicKey publicKey=null;
		 SSLContext sslContext = null; 
	       try {

	           char[] password= "orange@1234".toCharArray();
	           ClassLoader clsldr = new IdfcServiceImpl().getClass().getClassLoader();
	           File file =new File(clsldr.getResource("Orange.pfx").getFile());
	    	       
	           
	           KeyStore clientStore = KeyStore.getInstance("PKCS12");
	           clientStore.load(new FileInputStream(file), password);
	           KeyManagerFactory kmf = javax.net.ssl.KeyManagerFactory.getInstance("SunX509");
	           kmf.init(clientStore, password);
	           
	           Enumeration<String> aliases = clientStore.aliases();
	           while(aliases.hasMoreElements()){
	               String alias = aliases.nextElement();
	               if(clientStore.getCertificate(alias).getType().equals("X.509")){
	               Date expDate = ((X509Certificate) clientStore.getCertificate(alias)).getNotAfter();
	               Date fromDate= ((X509Certificate) clientStore.getCertificate(alias)).getNotBefore();
	               PublicKey publicKey1 = ((X509Certificate) clientStore.getCertificate(alias)).getPublicKey();  
	               byte[] encodedPublicKey = publicKey1.getEncoded();
	               String b64PublicKey = java.util.Base64.getEncoder().encodeToString(encodedPublicKey);
	               System.out.println(b64PublicKey);
	       System.out.println("Expiray Date:-"+expDate );
	       System.out.println("From Date:-"+fromDate);
	       System.out.println("PKey : " + publicKey1);
	               }
	           }
	           
	           
	           
	           KeyManager[] kms = kmf.getKeyManagers();

	         
	          /* KeyStore trustStore = KeyStore.getInstance("JKS");
	           trustStore.load(new FileInputStream("cacerts"), "changeit".toCharArray());*/

	           TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
	           tmf.init(clientStore);
	           TrustManager[] tms = tmf.getTrustManagers();

	           sslContext = SSLContext.getInstance("TLS");
	           sslContext.init(kmf.getKeyManagers(),tms,new SecureRandom());
	           SSLContext.setDefault(sslContext);
	           
	    
               System.out.println("GOPLJI:" +sslContext );
	           
	           
	             return sslContext;
	       } catch (Exception e) {
	           e.printStackTrace();
	           System.out.println("GOPLJ:" +e.getMessage() );
	           return sslContext;
	       }

	   }
	

	
	
}
