package com.vg.lms.service;

import com.vg.lms.model.Loan;

public interface ReceivableService {
	
	public Loan computeReceivable(Loan loan, int emiMonth, int emiDate);

}
