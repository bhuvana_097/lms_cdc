package com.vg.lms.service;

import java.io.IOException;

import com.coh.lms.IDFC.MobileRequest;
import com.coh.lms.IDFC.MobileResponse;

public interface IdfcServices {
	
	public MobileResponse IdfcValidation(MobileRequest mobilerequest);

}
