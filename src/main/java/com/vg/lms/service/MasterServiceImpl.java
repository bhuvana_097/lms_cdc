package com.vg.lms.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Seconds;
import org.jsoup.select.Evaluator.IsEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.vg.lms.UserModuleMasterDetails;
import com.vg.lms.bean.SearchLmsUserBean;
import com.vg.lms.bean.UpdateLmsUserBean;
import com.vg.lms.model.Application;
import com.vg.lms.model.Loan;
import com.vg.lms.model.UserMaster;
import com.vg.lms.model.UserModuleMaster;
import com.vg.lms.repository.ApplicationRepository;
import com.vg.lms.repository.UserMasterRepository;
import com.vg.lms.request.AddLmsUserRequestDTO;
import com.vg.lms.request.SearchLmsUserRequest;
import com.vg.lms.request.SessionRequestDTO;
import com.vg.lms.request.UserPasswordChangeRequest;
import com.vg.lms.request.osclCorrectionRequestDTO;


@Service
public class MasterServiceImpl implements MasterService {
	
	@Autowired
	private UserMasterRepository userMasterRepository;
	
	@Autowired
	private ApplicationRepository applicationRepository;
	
	String check="N";
	
	private final Logger log = LoggerFactory.getLogger(MasterServiceImpl.class);

	@Override
	public int addLmsUser(AddLmsUserRequestDTO addLmsUserRequestDTO) {
		int ret = 0;
		UserMaster userMaster = new UserMaster();
	if(userMasterRepository.existsBycode(addLmsUserRequestDTO.getCode())) {
		return 1;
	}
		userMaster.setCode(addLmsUserRequestDTO.getCode());
		userMaster.setName(addLmsUserRequestDTO.getName());
		userMaster.setPasswd(addLmsUserRequestDTO.getPasswd());
		userMaster.setDept(addLmsUserRequestDTO.getDept());
		userMaster.setHierLevel(addLmsUserRequestDTO.getHierLevel());
		userMaster.setStatus(addLmsUserRequestDTO.getStatus());
		userMaster.setRole(addLmsUserRequestDTO.getRole());
		userMaster.setBranch(addLmsUserRequestDTO.getBranch());
		userMaster.setCreate_user(addLmsUserRequestDTO.getCreateUser());
		userMaster.setCreateTime(addLmsUserRequestDTO.getCreateTime());
		userMaster.setModifyUser(null);
		userMaster.setModifyTime(null);
		userMaster.setStatus(addLmsUserRequestDTO.getStatus());
		//userMaster.setLastLogin(addLmsUserRequestDTO.getCreateTime());

		userMasterRepository.save(userMaster);
		return 0;
	}

	@Override
	public List<UserMaster> lmsUserList() {
		return userMasterRepository.findAll();
	}

	@Override
	public SearchLmsUserBean lmsUserSrchList(String code)
	{
		SearchLmsUserBean srchBean = new SearchLmsUserBean();
//		UpdateLmsUserBean srchBean = new UpdateLmsUserBean();
		try {	
		UserMaster mas = userMasterRepository.findBycode(code);
		if(userMasterRepository.existsBycode(code)) {
			
			 check = "y";
			 srchBean.setId(mas.getId());
		        srchBean.setCode(mas.getCode());
		        srchBean.setName(mas.getName());
		        srchBean.setPasswd(mas.getPasswd());
		        srchBean.setdepartment(mas.getDept());
		        srchBean.setHier_level(mas.getHierLevel());
		        srchBean.setStatus(mas.getStatus());
		        srchBean.setBranch(mas.getBranch());
		        srchBean.setCheck(check);
		        srchBean.setReason(mas.getReason());
		} else {
			 srchBean.setCheck("N");
		}
			
	
		
	}catch(Exception ex) {
	log.info("Exception ..");	
	}
        return srchBean;
	}
	
	@Override
	public int updateLmsUser(AddLmsUserRequestDTO addLmsUserRequestDTO) {
		int ret = 0;
	
		UserMaster userMaster = new UserMaster();
		userMaster = userMasterRepository.getOne(addLmsUserRequestDTO.getId());
		if(userMasterRepository.existsBycode(addLmsUserRequestDTO.getCode())) {
			//addLmsUserRequestDTO.setPasswd(userMaster.getPasswd());
			log.info("Inside Exist" +userMaster.getPasswd());
			userMaster.setPasswd(userMaster.getPasswd());
		}else {
			userMaster.setPasswd(addLmsUserRequestDTO.getPasswd());
		}
		userMaster.setCode(userMaster.getCode());
		userMaster.setName(addLmsUserRequestDTO.getName());
		userMaster.setDept(addLmsUserRequestDTO.getDept());
		userMaster.setHierLevel(addLmsUserRequestDTO.getHierLevel());
		userMaster.setRole(addLmsUserRequestDTO.getRole());
		userMaster.setModifyUser(addLmsUserRequestDTO.getModifyUser());
		userMaster.setModifyTime(addLmsUserRequestDTO.getModifyTime());
		userMaster.setStatus(addLmsUserRequestDTO.getStatus());
		userMaster.setReason(addLmsUserRequestDTO.getReason());
		userMaster.setBranch(addLmsUserRequestDTO.getBranch());
		log.info("Update ends ser impl" + addLmsUserRequestDTO.getReason());
		userMasterRepository.save(userMaster);
		return 0;
	}

	@Override
	public boolean getUserAccess(String userRole, String module) {
		String userDept = "", userHierLevel = "";
		String[] temp = userRole.split("-");
		userDept = temp[0];
		userHierLevel = temp[1];
		for (UserModuleMaster userModuleMaster : UserModuleMasterDetails.getModuleDetails()) {
			if(userDept.equalsIgnoreCase(userModuleMaster.getDept())){
				 if(userModuleMaster.getModule().trim().equalsIgnoreCase(module.trim())){
					 if((userHierLevel.equalsIgnoreCase("1") && userModuleMaster.getL1()==1) ||
						(userHierLevel.equalsIgnoreCase("2") && userModuleMaster.getL2()==1) ||
						(userHierLevel.equalsIgnoreCase("3") && userModuleMaster.getL3()==1) ||
						(userHierLevel.equalsIgnoreCase("4") && userModuleMaster.getL4()==1) ||
						(userHierLevel.equalsIgnoreCase("5") && userModuleMaster.getL5()==1)){
						 log.info("Entering MasterServiceImpl - " + module);
						 return true;
					 }
				 }
			}
		}
		
		return false;
	}

	@Override
	public String getUserRole(Authentication authentication) {
		String usrRole="";
		String uname="";
		for (GrantedAuthority authority : authentication.getAuthorities()) {
		     String role = authority.getAuthority();
                usrRole= role;
                uname = authentication.getName();
                log.info("role==>"+usrRole);
                log.info("uname==>"+uname);
		}
		return usrRole;
	}

	@Override
	public int osclCorrection(osclCorrectionRequestDTO osclCorrectionRequestDTO) {
		// TODO Auto-generated method stub
		int ret = 0;
		Application userMaster = new Application();
		userMaster.setDealerDisbursementAmount(osclCorrectionRequestDTO.getDealerDisbursementAmount());
		userMaster.setEmi(osclCorrectionRequestDTO.getEmi());
		userMaster.setProcessingFee(osclCorrectionRequestDTO.getProcessingFee());
		userMaster.setScCharge(osclCorrectionRequestDTO.getScCharge());
		

		applicationRepository.save(userMaster);
		return 0;
	}
	
	@Override
	public int UserPasswdChange(UserPasswordChangeRequest UserPasswordChangeRequest) {
		int ret = 0;
		UserMaster userMaster = new UserMaster();
		log.info("CHANGE PASSWORD" + UserPasswordChangeRequest.getCode());
		 List <UserMaster> um = userMasterRepository.findAll();
		UserMaster umm = userMasterRepository.findBycode(String. valueOf(UserPasswordChangeRequest.getCode()));
			
		
		log.info("OLD PASSWORD" + umm.getId());
		
		userMaster.setId(umm.getId());
		userMaster.setCode(UserPasswordChangeRequest.getCode());
		userMaster.setName(umm.getName());
		userMaster.setCreate_user(umm.getCreate_user());
		userMaster.setCreateTime(umm.getCreateTime());
		userMaster.setDept(umm.getDept());
		userMaster.setHierLevel(umm.getHierLevel());
		userMaster.setRole(umm.getRole());
		userMaster.setStatus(umm.getStatus());
		  log.info("Inside Service:" + UserPasswordChangeRequest);
              //userMaster.setId(UserPasswordChangeRequest.getId());
		userMaster.setPasswd(UserPasswordChangeRequest.getPasswd());
		
		userMaster.setModifyUser(UserPasswordChangeRequest.getModifyUser());
		userMaster.setModifyTime(UserPasswordChangeRequest.getModifyTime());
		userMaster.setId(userMaster.getId());
		log.info("CHANGE PASSWORD END");
		userMasterRepository.save(userMaster);
          
		return 0;
	}
	
	/*@Override
	public int SessionIdUpdate(String sessionid,String user) {
		
		int x=0;
		Calendar createTime = GregorianCalendar.getInstance();
		log.info("Master Service Impl"+ sessionid+":"+user);
				 
		UserMaster umm = userMasterRepository.findBycode(user);
		
		
		SimpleDateFormat formatd = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
		String dateStart = formatd.format(createTime.getTime());
		String dateStop = formatd.format(umm.getLastLogin());
		
       log.info("TIme:" + dateStart + dateStop);		
		
       	Date d1 = null;
   		Date d2 = null;

   	try {
   		d1 = formatd.parse(dateStart);
   		d2 = formatd.parse(dateStop);

   		DateTime dt1 = new DateTime(d1);
   		DateTime dt2 = new DateTime(d2);

   		System.out.print(Days.daysBetween(dt2, dt1).getDays() + " days, ");
   		System.out.print(Hours.hoursBetween(dt1, dt2).getHours() % 24 + " hours, ");
   		System.out.print(Minutes.minutesBetween(dt2, dt1).getMinutes() % 60 + " minutes, ");
   		System.out.print(Seconds.secondsBetween(dt1, dt2).getSeconds() % 60 + " seconds.");
   	
   		if(Minutes.minutesBetween(dt2, dt1).getMinutes() % 60 >=30 ) {
   			   			
   			umm.setSessionId("");
   			userMasterRepository.save(umm);
   		}
   		
   		if(Days.daysBetween(dt2, dt1).getDays() >=7 ) {
   			return 2;	
   			
   		}
 
   	} catch (Exception e) {
   		e.printStackTrace();
   	 }

       
		if(!(umm.getSessionId().isEmpty()))
		{
			log.info("Inside Service:");
			return 1;
		
		}
		
		umm.setSessionId(sessionid);
		umm.setLastLogin(createTime.getTime());
		userMasterRepository.save(umm);
		return x;
		
	}*/
	
	public UserMaster searchCode(SearchLmsUserRequest  searchLmsUserRequest)
	{
		return userMasterRepository.findBycode(searchLmsUserRequest.getCode());
			
		
	}
	

	/*@Override
	public int Logoff(String code ) {
		int ret = 0;
		UserMaster userMaster = new UserMaster();
		
		 List <UserMaster> um = userMasterRepository.findAll();
		UserMaster umm = userMasterRepository.findBycode(code);
			
		
		log.info("OLD PASSWORD" + umm.getId() + ":" +"CODE"+ umm.getCode());
		
		umm.setSessionId("");
	
		log.info("CHANGE PASSWORD END");
		userMasterRepository.save(umm);
          
		return 0;
	}*/
	
	
}
