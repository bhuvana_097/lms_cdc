package com.vg.lms.service;

import java.io.File;
import java.io.FilenameFilter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.vg.lms.incred.applicant.AddRequest;
import com.vg.lms.incred.application.UpdateRequest;
import com.vg.lms.incred.init.ADDRESS;
import com.vg.lms.incred.init.InitRequest;
import com.vg.lms.incred.init.InitResponse;
import com.vg.lms.incred.profile.ACCOUNT;
import com.vg.lms.incred.profile.BANK;
import com.vg.lms.incred.profile.EMPLOYMENT;
import com.vg.lms.incred.profile.FINANCIAL;
import com.vg.lms.incred.sanction.SANCTIONDETAIL;
import com.vg.lms.incred.vehicle.DEALERDETAILS;
import com.vg.lms.model.Asset;
import com.vg.lms.model.Customer;
import com.vg.lms.model.IncredAudit;
import com.vg.lms.model.Loan;
import com.vg.lms.repository.IncredRepository;
import com.vg.lms.repository.LoanRepository;

@Component
public class IncredServiceImpl {

	@Autowired
	private IncredRepository incredRepository;

	@Autowired
	private LoanRepository loanRepository;
	

	private final Logger log = LoggerFactory.getLogger(IncredServiceImpl.class);	

   //@Scheduled(fixedDelay=600000)
	public void getInCompleteTxns() throws JsonProcessingException{
		long startTimeInMillis=System.currentTimeMillis();		
		Date syncTime = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentTime = sdf.format(syncTime);		
		log.info("Entering GetTxnStatusThread : "+currentTime);
		List<IncredAudit> pendingtxn = incredRepository.findBystateNot("COMPLETE");
		for(IncredAudit incredAudit : pendingtxn) {
			
			try {
				incredAudit.setSyncTime(syncTime);
				String applicationState = incredAudit.getState();
				
				Loan loan = loanRepository.findById(incredAudit.getLoanId()).get();
switch (applicationState)
				{
				case "FRESH":
					initApplication(loan, incredAudit);
					break;
				case "INIT":
					updateApplication(loan, incredAudit);
					break;
				case "UPDATE":
					updateProfile(loan, incredAudit);
					break;
				case "PROFILE":
					uploadFiles(loan, incredAudit);
					//updateCoBorrower(loan, incredAudit);
					break;
				case "FILE":
					//uploadFiles(loan, incredAudit);
					updateCoBorrower(loan, incredAudit);
					break;
				case "COAPPL":
					updateVehicle(loan, incredAudit);				
					break;
				case "VEHICLE":
					updateSanction(loan, incredAudit);
					break;
				case "SANCTION":
					updateComplete(incredAudit);
					break;
					//default: throw new IllegalArgumentException("No Handler Available");
				}

				//break;
			} catch(Exception ex) {
				log.error("Exception occured while pushing data", ex);
			}


		}

		long timeTaken=System.currentTimeMillis()-startTimeInMillis;
		log.info("Time Taken for updateTxnComplete is ==>"+timeTaken );

	}

	public void updateComplete(IncredAudit incredAudit) {
		String url1= "https://api.incred.com/v2/partner/application/complete/"+incredAudit.getAppId();
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
		headers.add("Content-Type", "application/json");
		headers.add("api-key", "f72d00f2ae311815a2f170f4e9db4289c84733fac12c1ef13129350defbd47");
		ResponseEntity<InitResponse> entity = new RestTemplate().exchange(
				url1, HttpMethod.GET, new HttpEntity<Object>(headers),
				InitResponse.class);		
		log.info("Incred updateComplete Response : " + entity);
		incredAudit.setMessage(entity.getBody().getMessage());
		incredAudit.setStatus("COMPLETE");
		if(entity.getBody().getStatus()) {
			incredAudit.setState("COMPLETE");				
		}
		incredRepository.save(incredAudit);
	}

	private void updateSanction(Loan loan, IncredAudit incredAudit) throws JsonProcessingException {
		com.vg.lms.incred.sanction.UpdateRequest sanctionReq =  new com.vg.lms.incred.sanction.UpdateRequest();
		sanctionReq.setAPPLICATIONID(incredAudit.getAppId());
		SANCTIONDETAIL sancDetail = new SANCTIONDETAIL();
		sancDetail.setDUEDATE("5");
		//sancDetail.setLOANAMOUNT((int)loan.getAmount());
		sancDetail.setLOANAMOUNT((int) Math.ceil((loan.getAmount()*0.8)));
		sancDetail.setLOANRATE(15.5f);
		sancDetail.setLOANTENURE((int)loan.getTenure());
		sancDetail.setPROCESSINGFEE((int)loan.getProcFee());
		sancDetail.setLoanScheme("ORFIL-220");
		sancDetail.setADVANCEEMI((int)loan.getAdvanceEmi());

		sanctionReq.setSANCTIONDETAIL(sancDetail);

		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = mapper.writeValueAsString(sanctionReq);
		log.info("Incred sanctionReq Request : " + jsonInString);

		HttpHeaders headers = new HttpHeaders();
		//headers.add("api-key", "f32600fca2331f13acf773f4e9db4088c84733fac0291af5382d390ce4b046");
		headers.add("api-key", "f72d00f2ae311815a2f170f4e9db4289c84733fac12c1ef13129350defbd47");
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> entity = new HttpEntity<String>(jsonInString, headers);	  
		RestTemplate template = new RestTemplate();
		/*ResponseEntity<InitResponse> responseEntity =
				template.postForEntity("https://uat-api.incred.com/v2/partner/sanctiondata/update", entity, InitResponse.class);*/
		ResponseEntity<InitResponse> responseEntity =
				template.postForEntity("https://api.incred.com/v2/partner/sanctiondata/update", entity, InitResponse.class);
		
		log.info("Incred SANCTION Response : " + mapper.writeValueAsString(responseEntity));
		incredAudit.setMessage(responseEntity.getBody().getMessage());
		incredAudit.setStatus("SANCTION");

		if(responseEntity.getBody().getStatus()) {
			log.info("INCRED Response:" + responseEntity.getBody().getStatus());
			incredAudit.setState("SANCTION");	
		}
		incredRepository.save(incredAudit);
	}


	private void updateVehicle(Loan loan, IncredAudit incredAudit) throws JsonProcessingException {
		com.vg.lms.incred.vehicle.UpdateRequest updateRequest = new com.vg.lms.incred.vehicle.UpdateRequest();
		updateRequest.setAPPLICATIONID(incredAudit.getAppId());

		Asset asset = loan.getAsset();

		DEALERDETAILS dealerdetails = new DEALERDETAILS();
		dealerdetails.setDEALERNAME(asset.getMake() + " " + asset.getModel());
		updateRequest.setDEALERDETAILS(dealerdetails);

		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = mapper.writeValueAsString(updateRequest);
		log.info("Incred Vehicle Request : " + jsonInString);

		HttpHeaders headers = new HttpHeaders();
		//headers.add("api-key", "f32600fca2331f13acf773f4e9db4088c84733fac0291af5382d390ce4b046");
		headers.add("api-key", "f72d00f2ae311815a2f170f4e9db4289c84733fac12c1ef13129350defbd47");

		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> entity = new HttpEntity<String>(jsonInString, headers);	  
		RestTemplate template = new RestTemplate();		
		ResponseEntity<InitResponse> responseEntity =
				template.postForEntity("https://api.incred.com/v2/partner/vehicle/update", entity, InitResponse.class);
		log.info("Incred updateVehicle Response : " + mapper.writeValueAsString(responseEntity));
		incredAudit.setMessage(responseEntity.getBody().getMessage());
		incredAudit.setStatus("VEHICLE");	
		if(responseEntity.getBody().getStatus()) {
			incredAudit.setState("VEHICLE");
		}
		incredRepository.save(incredAudit);
	}

	private void updateLMS(Loan loan, IncredAudit incredAudit) {
		// TODO Auto-generated method stub

	}



	private void updateCoBorrower(Loan loan, IncredAudit incredAudit) throws JsonProcessingException {
		AddRequest addRequest = new AddRequest();
		addRequest.setAPPLICATIONID(incredAudit.getAppId());
		addRequest.setAPPLICANTTYPE("Secondary");

		Customer customer = loan.getGurantor();
		if(customer!=null) {
			addRequest.setAADHAAR(customer.getAadharNo());		
			log.info("Customer DOB : " + customer.getDob());
			String[] dateArray = customer.getDob().split("-");
			//addRequest.setDOB(dateArray[2] + "/" + dateArray[1] + "/" + dateArray[0]);
			addRequest.setDOB(dateArray[0] + "/" + dateArray[1] + "/" + dateArray[2]);
			addRequest.setFNAME(customer.getFirstname());
			addRequest.setLNAME(customer.getLastname());
			addRequest.setGENDER(customer.getGender());
			addRequest.setLOANTYPE("AL");
			addRequest.setMOBILE(customer.getPhone1());
			if(customer.getPanno() != null && customer.getPanno().trim().length() > 0) {
				addRequest.setPAN(customer.getPanno());
			}

			com.vg.lms.incred.applicant.ADDRESS address = new com.vg.lms.incred.applicant.ADDRESS();
			address.setADDRESS(customer.getResiAddr() + ", " + customer.getResiAddress2() + ", " + 
					customer.getResiCity() + ", "+ customer.getResiState());
			address.setPINCODE(customer.getResiPin());

			addRequest.setADDRESS(new ArrayList<>());
			addRequest.getADDRESS().add(address);


			ObjectMapper mapper = new ObjectMapper();
			String jsonInString = mapper.writeValueAsString(addRequest);
			log.info("Incred addRequest Request : " + jsonInString);

			HttpHeaders headers = new HttpHeaders();
			//headers.add("api-key", "f32600fca2331f13acf773f4e9db4088c84733fac0291af5382d390ce4b046");
			headers.add("api-key", "f72d00f2ae311815a2f170f4e9db4289c84733fac12c1ef13129350defbd47");

			headers.setContentType(MediaType.APPLICATION_JSON);

			HttpEntity<String> entity = new HttpEntity<String>(jsonInString, headers);	  
			RestTemplate template = new RestTemplate();
			/*ResponseEntity<InitResponse> responseEntity =
				template.postForEntity("https://uat-api.incred.com/v2/partner/applicant/add", entity, InitResponse.class);*/
			ResponseEntity<InitResponse> responseEntity =
					template.postForEntity("https://api.incred.com/v2/partner/applicant/add", entity, InitResponse.class);
			log.info("Incred updateCoBorrower Response : " + mapper.writeValueAsString(responseEntity));
			incredAudit.setMessage(responseEntity.getBody().getMessage());
			incredAudit.setStatus("COAPPL");
			if(responseEntity.getBody().getStatus()) {									
				incredAudit.setState("COAPPL");
			}
		}else {
			incredAudit.setState("COAPPL");
		}

		incredRepository.save(incredAudit);


	}

	private void uploadFiles(Loan loan, IncredAudit incredAudit) {
		String appNo= loan.getApplNo();
		String custID = incredAudit.getCustId();
		String applID = incredAudit.getAppId();
		String photoDir = appNo.substring(appNo.length() - 3);//to get last 3 digits for finding photo directory
		//String uri = "https://uat-api.incred.com/v2/partner/fileupload/statement";//Incred url
		String uri = "https://api.incred.com/v2/partner/fileupload/artifact";//Incred url LIVE
		long startTimeInMillis=System.currentTimeMillis();		
		long randNo = startTimeInMillis/10;//rand number for DOC NUM

		//String photoLocation = "/var/www/html/live/los/wap/photo-goofys/los/"+photoDir+"/";
		String photoLocation = "/var/www/html/live/los/wap/photo/";
		//photoLocation = "/home/ragavan/Pictures";
		//File directory = new File(photoLocation);
		//File[] fList = directory.listFiles();//get all the files from a directory



		File dir = new File(photoLocation);    
		File[] fList = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String nameFilter) {
				return nameFilter.startsWith(appNo);
			}
		});


		log.info("File Count for application : " + incredAudit.getLosAppName() + " File Count : "+ fList.length);
		try{

			if( fList != null) {
				int index = 1;

				for (File file : fList){
					if(file.getName().contains(appNo)){						

						HttpPost post = new HttpPost("https://api.incred.com/v2/partner/fileupload/artifact");
						post.setHeader("api-key", "f72d00f2ae311815a2f170f4e9db4289c84733fac12c1ef13129350defbd47");
						FileBody fileBody = new FileBody(file, ContentType.IMAGE_JPEG);

						MultipartEntityBuilder builder = MultipartEntityBuilder.create();
						builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
						builder.addPart("upfile", fileBody);
						builder.addPart("APPLICATION_ID", new StringBody(applID, ContentType.MULTIPART_FORM_DATA));
						builder.addPart("DOC_TYPE", new StringBody("PL_OTHER", ContentType.MULTIPART_FORM_DATA));
						builder.addPart("DOC_NAME", new StringBody("PL_OTHER", ContentType.MULTIPART_FORM_DATA));
						builder.addPart("DOC_NUM", new StringBody(String.valueOf(index), ContentType.MULTIPART_FORM_DATA));
						builder.addPart("CUSTOMER_ID", new StringBody(custID, ContentType.MULTIPART_FORM_DATA));
						org.apache.http.HttpEntity entity = builder.build();
						HttpClient httpclient = HttpClientBuilder.create().build();
						post.setEntity(entity);		

						java.io.ByteArrayOutputStream out = new java.io.ByteArrayOutputStream((int) entity.getContentLength());
						entity.writeTo(out);
						String entityContentAsString = new String(out.toByteArray());
						log.info("Incred File Request : " +entityContentAsString);
						ResponseHandler<String> responseHandler = new BasicResponseHandler();
						String responseBody = httpclient.execute(post, responseHandler);


						log.info("Incred File Response : " + responseBody);
						index++;
					}			

				}
			}
			incredAudit.setStatus("FILE");
			incredAudit.setState("FILE");

			incredRepository.save(incredAudit);
		} catch (Exception e) {
			log.error("Exception occured in incred file push : ", e);
		}
	}

	/*private void updateProfile(Loan loan, IncredAudit incredAudit) throws JsonProcessingException {
		com.vg.lms.incred.profile.UpdateRequest profileUpdate = new com.vg.lms.incred.profile.UpdateRequest();
		Customer customer = loan.getCustomer();
		profileUpdate.setFNAME(customer.getFirstname());
		profileUpdate.setLNAME(customer.getLastname());
		profileUpdate.setCUSTOMERID(incredAudit.getCustId());

		com.vg.lms.incred.profile.ADDRESS address = new com.vg.lms.incred.profile.ADDRESS();
		address.setADDRESS(customer.getResiAddr() + ", " + customer.getResiAddress2() + ", " + 
				customer.getResiCity() + ", "+ customer.getResiState());
		address.setPINCODE(customer.getResiPin());

		profileUpdate.setADDRESS(new ArrayList<>());
		profileUpdate.getADDRESS().add(address);

		EMPLOYMENT employment = new EMPLOYMENT();
		employment.setADDRESS(customer.getOffAddr() + ", " + customer.getOffAddr2() + ", " + customer.getOffCity());
		employment.setLOCALITY(customer.getOffAddr2());
		employment.setPINCODE(customer.getOffPin());

		int stateCode = 33;
		if(customer.getOffState().equalsIgnoreCase("Kerala"))
			stateCode = 32;
		else if (customer.getOffState().equalsIgnoreCase("ANDHRA PRADESH"))
			stateCode = 28;

		employment.setSTATE(stateCode);

		SALARY salary = new SALARY();
			salary.setMONTHLY(mONTHLY);
			salary.setNETMONTHLY(nETMONTHLY);
			salary.setTYPE(tYPE);
			employment.setSALARY(salary);

		List<EMPLOYMENT> employmentData = new ArrayList<>();
		employmentData.add(employment);

		profileUpdate.setEMPLOYMENT(employmentData);

		ACCOUNT account = new ACCOUNT();
		account.setACCOUNTHOLDERNAME("Orange Retail Finance Pvt Ltd");
		account.setACCOUNTNUM("026866300000182");
		account.setACCOUNTTYPE("CURRENT");
		account.setBANKNAME("98");
		account.setIFSCCODE("YESB0000268");

		List<ACCOUNT> accountList = new ArrayList<>();
		accountList.add(account);

		BANK bank = new BANK();
		bank.setACCOUNT(accountList);

		FINANCIAL financial = new FINANCIAL();
		financial.setBANK(bank);

		profileUpdate.setFINANCIAL(financial);

		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = mapper.writeValueAsString(profileUpdate);
		log.info("Incred profileUpdate Request : " + jsonInString);

		HttpHeaders headers = new HttpHeaders();
		//headers.add("api-key", "f32600fca2331f13acf773f4e9db4088c84733fac0291af5382d390ce4b046");
		headers.add("api-key", "f72d00f2ae311815a2f170f4e9db4289c84733fac12c1ef13129350defbd47");

		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> entity = new HttpEntity<String>(jsonInString, headers);	  
		RestTemplate template = new RestTemplate();
		ResponseEntity<InitResponse> responseEntity =
				template.postForEntity("https://uat-api.incred.com/v2/partner/profile/update", entity, InitResponse.class);
		ResponseEntity<InitResponse> responseEntity =
				template.postForEntity("https://api.incred.com/v2/partner/profile/update", entity, InitResponse.class);
		log.info("Incred Profile Update Response : " + mapper.writeValueAsString(responseEntity));
		incredAudit.setMessage(responseEntity.getBody().getMessage());
		incredAudit.setStatus("PROFILE");	
		if(responseEntity.getBody().getStatus()) {
			incredAudit.setState("PROFILE");				
		}

		incredRepository.save(incredAudit);

	}
*/
	private void updateProfile(Loan loan, IncredAudit incredAudit) throws JsonProcessingException {
        String custId= incredAudit.getCustId();
        Customer customer = loan.getCustomer();

        ResponseEntity<InitResponse> responseEntity = updateProfileDetails(custId, customer, false);

        incredAudit.setStatus("PROFILE");    
        if(responseEntity.getBody().getStatus()) {
            incredAudit.setState("PROFILE");                
        } else {
            if(responseEntity.getBody().getMessage().toLowerCase().contains("invalid pincode")) {
                responseEntity = updateProfileDetails(custId, customer, true);
                if(responseEntity.getBody().getStatus()) {
                    incredAudit.setState("PROFILE");
                }
            }            
        }
        incredAudit.setMessage(responseEntity.getBody().getMessage());

        incredRepository.save(incredAudit);

    }

    private ResponseEntity<InitResponse> updateProfileDetails(String custId, Customer customer, boolean isRetry )
            throws JsonProcessingException {
        com.vg.lms.incred.profile.UpdateRequest profileUpdate = new com.vg.lms.incred.profile.UpdateRequest();
        profileUpdate.setFNAME(customer.getFirstname());
        profileUpdate.setLNAME(customer.getLastname());
        profileUpdate.setCUSTOMERID(custId);

        com.vg.lms.incred.profile.ADDRESS address = new com.vg.lms.incred.profile.ADDRESS();
        address.setADDRESS(customer.getResiAddr() + ", " + customer.getResiAddress2() + ", " + 
                customer.getResiCity() + ", "+ customer.getResiState());
        address.setPINCODE(customer.getResiPin());

        profileUpdate.setADDRESS(new ArrayList<>());
        profileUpdate.getADDRESS().add(address);

        EMPLOYMENT employment = new EMPLOYMENT();
        employment.setADDRESS(customer.getOffAddr() + ", " + customer.getOffAddr2() + ", " + customer.getOffCity());
        employment.setLOCALITY(customer.getOffAddr2());
        if(isRetry) {
            employment.setPINCODE(customer.getResiPin());
        } else {
            employment.setPINCODE(customer.getOffPin());
        }

        int stateCode = 33;
        if(customer.getOffState().equalsIgnoreCase("Kerala"))
            stateCode = 32;
        else if (customer.getOffState().equalsIgnoreCase("ANDHRA PRADESH"))
            stateCode = 28;

        employment.setSTATE(stateCode);

        /*SALARY salary = new SALARY();
            salary.setMONTHLY(mONTHLY);
            salary.setNETMONTHLY(nETMONTHLY);
            salary.setTYPE(tYPE);
            employment.setSALARY(salary);*/

        List<EMPLOYMENT> employmentData = new ArrayList<>();
        employmentData.add(employment);

        profileUpdate.setEMPLOYMENT(employmentData);

        ACCOUNT account = new ACCOUNT();
        account.setACCOUNTHOLDERNAME("Orange Retail Finance Pvt Ltd");
        account.setACCOUNTNUM("026866300000182");
        account.setACCOUNTTYPE("CURRENT");
        account.setBANKNAME("98");
        account.setIFSCCODE("YESB0000268");

        List<ACCOUNT> accountList = new ArrayList<>();
        accountList.add(account);

        BANK bank = new BANK();
        bank.setACCOUNT(accountList);

        FINANCIAL financial = new FINANCIAL();
        financial.setBANK(bank);

        profileUpdate.setFINANCIAL(financial);

        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(profileUpdate);
        log.info("Incred profileUpdate Request : " + jsonInString);

        HttpHeaders headers = new HttpHeaders();
        //headers.add("api-key", "f32600fca2331f13acf773f4e9db4088c84733fac0291af5382d390ce4b046");
        headers.add("api-key", "f72d00f2ae311815a2f170f4e9db4289c84733fac12c1ef13129350defbd47");

        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<String>(jsonInString, headers);      
        RestTemplate template = new RestTemplate();
        /*ResponseEntity<InitResponse> responseEntity =
                template.postForEntity("https://uat-api.incred.com/v2/partner/profile/update", entity, InitResponse.class);*/
        ResponseEntity<InitResponse> responseEntity =
                template.postForEntity("https://api.incred.com/v2/partner/profile/update", entity, InitResponse.class);
        log.info("Incred Profile Update Response : " + mapper.writeValueAsString(responseEntity));
        return responseEntity;
    }

	private void updateApplication(Loan loan, IncredAudit incredAudit) throws JsonProcessingException {
		UpdateRequest updateRequest = new UpdateRequest();
		updateRequest.setAPPLICATIONID(incredAudit.getAppId());
		updateRequest.setLOANSCHEME("AL_TWL_COLEN");

		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = mapper.writeValueAsString(updateRequest);
		log.info("Incred updateRequest Request : " + jsonInString);

		HttpHeaders headers = new HttpHeaders();
		//headers.add("api-key", "f32600fca2331f13acf773f4e9db4088c84733fac0291af5382d390ce4b046");
		headers.add("api-key", "f72d00f2ae311815a2f170f4e9db4289c84733fac12c1ef13129350defbd47");
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> entity = new HttpEntity<String>(jsonInString, headers);	  
		RestTemplate template = new RestTemplate();
		/*ResponseEntity<InitResponse> responseEntity =
				template.postForEntity("https://uat-api.incred.com/v2/partner/application/update", entity, InitResponse.class);*/
		ResponseEntity<InitResponse> responseEntity =
				template.postForEntity("https://api.incred.com/v2/partner/application/update", entity, InitResponse.class);
		log.info("Incred Application updateRequest Response : " + mapper.writeValueAsString(responseEntity));
		incredAudit.setMessage(responseEntity.getBody().getMessage());
		incredAudit.setStatus("UPDATE");
		if(responseEntity.getBody().getStatus()) {			
			incredAudit.setState("UPDATE");
		} 

		incredRepository.save(incredAudit);


	}


	private void initApplication(Loan loan, IncredAudit incredAudit) {
		//IncredAudit incredAudit = new IncredAudit();
		try {
			Customer customer = loan.getCustomer();
			incredAudit.setLosAppName(loan.getApplNo());

			incredAudit.setLoanId(loan.getId());
			InitRequest initRequest = new InitRequest();
			initRequest.setAADHAAR(customer.getAadharNo());		
			log.info("Customer DOB : " + customer.getDob());
			String[] dateArray = customer.getDob().split("-");
			initRequest.setDOB(dateArray[0] + "/" + dateArray[1] + "/" + dateArray[2]);
			initRequest.setFNAME(customer.getFirstname());
			initRequest.setLNAME(customer.getLastname());
			initRequest.setGENDER(customer.getGender());
			initRequest.setLOANTYPE("AL");
			initRequest.setMOBILE(customer.getPhone1());
			initRequest.setPARTNERREFERENCE(loan.getLoanNo());
			//initRequest.setMOBILE("6380306842");
			//initRequest.setPAN(customer.getPanno());

			ADDRESS address = new ADDRESS();
			address.setADDRESS(customer.getResiAddr() + ", " + customer.getResiAddress2() + ", " + 
					customer.getResiCity() + ", "+ customer.getResiState());
			address.setPINCODE(customer.getResiPin());

			initRequest.setADDRESS(new ArrayList<>());
			initRequest.getADDRESS().add(address);


			ObjectMapper mapper = new ObjectMapper();
			String jsonInString = mapper.writeValueAsString(initRequest);
		//	log.info("Incred init Request : " + jsonInString);

			HttpHeaders headers = new HttpHeaders();
			headers.add("api-key", "f72d00f2ae311815a2f170f4e9db4289c84733fac12c1ef13129350defbd47");
			headers.setContentType(MediaType.APPLICATION_JSON);

			HttpEntity<String> entity = new HttpEntity<String>(jsonInString, headers);	  
			RestTemplate template = new RestTemplate();
			ResponseEntity<InitResponse> responseEntity =
					template.postForEntity("https://api.incred.com/v2/partner/application/init", entity, InitResponse.class);
			//log.info("Incred init Response : " + mapper.writeValueAsString(responseEntity));
			incredAudit.setMessage(responseEntity.getBody().getMessage());
			incredAudit.setStatus("INIT");
			
			
			
			if(responseEntity.getBody().getStatus()) {
				log.info("TEST :" + incredAudit.getLoanNo() +":" +responseEntity.getBody().getResponse().getAPPLICATIONID() + ":" + responseEntity.getBody().getResponse().getCUSTOMERID() + ":" +responseEntity.getBody().getResponse().getTRANSACTIONID());
				
				incredAudit.setState("INIT");				
				incredAudit.setAppId(responseEntity.getBody().getResponse().getAPPLICATIONID());
				incredAudit.setCustId(responseEntity.getBody().getResponse().getCUSTOMERID());
				incredAudit.setTxnId(responseEntity.getBody().getResponse().getTRANSACTIONID());				
			}

		} catch (Exception e) {
			log.error("Error Occured in Incred Push :",e);
		}
		incredRepository.save(incredAudit);
	}
	


}