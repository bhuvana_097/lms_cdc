
package com.vg.lms.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vg.lms.bean.CrDrBean;

import com.vg.lms.incred.init.ADDRESS;
import com.vg.lms.incred.init.InitRequest;
import com.vg.lms.incred.init.InitResponse;
import com.vg.lms.model.Application;
import com.vg.lms.model.ApplicationAnnex;
import com.vg.lms.model.Asset;
import com.vg.lms.model.Customer;
import com.vg.lms.model.CustomerArchive;
import com.vg.lms.model.IncredAudit;
import com.vg.lms.model.LMSRegistryModel;
import com.vg.lms.model.Loan;
import com.vg.lms.repository.ApplicationAnnexReposit;
import com.vg.lms.repository.ApplicationRepository;
import com.vg.lms.repository.CustomerArchiveRepository;
import com.vg.lms.repository.CustomerRepository;
import com.vg.lms.repository.IncredRepository;
import com.vg.lms.repository.LMSRegistryRepository;
import com.vg.lms.repository.LoanRepository;
import com.vg.lms.repository.SchemeRepository;
import com.vg.lms.request.CustomerDTO;
import com.vg.lms.request.GetCustomerRequestDTO;
import com.vg.lms.request.LOSRequestDTO;
import com.vg.lms.response.CustomerResponseDTO;
import com.vg.lms.response.LOSResponseDTO;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.vg.lms.colend.ColendService;



@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepository;
	
	@Autowired
	private CustomerArchiveRepository customerArchiveRepository;

	@Autowired
	private ApplicationRepository applicationRepository;

	@Autowired
	private SchemeRepository schemeRepository;

	@Autowired
	private LoanRepository loanRepository;

	@Autowired
	private LoanService loanServiceImpl;

	@Autowired
	private ReceivableService receivableService;
	
	@Autowired
	private LMSRegistryRepository lmsRegistryRepository;
	
	@Autowired
	private ApplicationAnnexReposit appanexxRepo;
	

	private final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);
	//private static final Logger log = Logger.getLogger(CustomerServiceImpl.class);

	@Override
	public CustomerResponseDTO getCustomer(GetCustomerRequestDTO getCustomerRequestDTO) {
		long startTimeInMillis = System.currentTimeMillis();
		log.info("Entering CustomerServiceImpl-getCustomer :"+getCustomerRequestDTO);
		Customer customer = customerRepository.findByaadharNo(getCustomerRequestDTO.getAadhaarNo());

		ModelMapper modelMapper = new ModelMapper();
		CustomerDTO customerDTO = null;
		if(customer!=null)
			customerDTO = modelMapper.map(customer, CustomerDTO.class);


		CustomerResponseDTO response = new CustomerResponseDTO();
		response.setCustomer(customerDTO);
		response.setStatusMessage("SUCCESS");
		response.setStatusCode("1");
		long timeTaken=System.currentTimeMillis()-startTimeInMillis;
		log.info("Time Taken for getCustomer is ==>"+timeTaken);
		return response;
	}

	@Override
	public CustomerResponseDTO addCustomer(CustomerDTO customerRequestDTO) {
		long startTimeInMillis = System.currentTimeMillis();
		log.info("Entering CustomerServiceImpl-addCustomer :"+customerRequestDTO);		

		ModelMapper modelMapper = new ModelMapper();
		Customer customer = modelMapper.map(customerRequestDTO, Customer.class);	
		customer.setCreateUser("LOS");
		//customer.setCreateTime(new Date());
		customer = customerRepository.save(customer);

		CustomerDTO customerDTO = modelMapper.map(customer, CustomerDTO.class);

		CustomerResponseDTO response = new CustomerResponseDTO();
		response.setCustomer(customerDTO);
		response.setStatusMessage("SUCCESS");
		response.setStatusCode("1");
		long timeTaken=System.currentTimeMillis()-startTimeInMillis;
		log.info("Time Taken for getCustomer is ==>"+timeTaken);
		return response;
	}

	@Override
	@Transactional
	public LOSResponseDTO pushLOSData(LOSRequestDTO losRequestDTO) {
		long startTimeInMillis = System.currentTimeMillis();
		log.info("Entering CustomerServiceImpl-pushLOSData :"+losRequestDTO);
		
		Application application = applicationRepository.findByappno(losRequestDTO.getApplicationNo());
		
		
		log.info("Entering CustomerServiceImpl-pushLOSData :" + losRequestDTO.getApplicationNo());
		
		
		//Checking if Customer Exists and add
		Customer customer = customerRepository.findByaadharNo(application.getAadhar());
		
		
		/*if(customer == null) 
			customer = new Customer();*/
		customer = populateCustomerDetails(application, customer);
		
		
		
		customer = customerRepository.save(customer);
		
			
		
		Customer coBorrower = null;
		String secondApplicantType = application.getGuarantorOrCoapp();

		if(secondApplicantType != null && secondApplicantType.trim().length() >0 ) {	
			String secondappNo = "";
			if(secondApplicantType.equalsIgnoreCase("guarantor")) 
				secondappNo = application.getAppno()+"_G";
			else
				secondappNo = application.getAppno()+"_C";	
			Application secondapp = applicationRepository.findByappno(secondappNo);
			if(secondapp==null)
			System.out.println("AA:" + secondappNo);
			else
				System.out.println("BB:" + secondappNo);
			coBorrower = customerRepository.findByaadharNo(secondapp.getAadhar());
			/*if(coBorrower == null) 
				coBorrower = new Customer();*/
			coBorrower = populateCustomerDetails(secondapp, coBorrower);
			coBorrower = customerRepository.save(coBorrower);
		}

		Loan loan = populateLoanDetails(application);
		loan.setCustomer(customer);
		loan.setGurantor(coBorrower);
		loan.setStatus("APPROVED");

		int emiDate = 5;		
		if(!application.getType().equals("app")) {
			emiDate = 7;
		}

		
		/*if(application.getFeState().equalsIgnoreCase("TAMILNADU") && application.getType().equals("app"))
			loan.setCoLender("INCRED");*/

		loan.setCoLender(application.getCoLender());
		loan.setCoLendPercent(application.getCoLenderPercentage());
		loan.setCoLenderRoi(application.getCoLenderRoi());
		loan.setCoLenderStatus(application.getCoLenderStatus());
		loan.setCoLenderDisposition(application.getCoLenderDisposition());
		log.info("SCCharge");
		//if( application.getProduct().contentEquals("TWO WHEELER"))
		loan.setScCharge(application.getScCharge());
		
		//get LMS month end and mid month from the registry table
		LMSRegistryModel monthEndModel = lmsRegistryRepository.findByRegistryType("los_month_end");
		int monthEndDate = 0;//default zero
		monthEndDate = Integer.parseInt(monthEndModel.getRegistryValue());
		
		LMSRegistryModel midMonthModel = lmsRegistryRepository.findByRegistryType("los_mid_month");
		int midMonthDate = 0;//default zero
		midMonthDate = Integer.parseInt(midMonthModel.getRegistryValue());		
		Calendar agreementDate = GregorianCalendar.getInstance();
		loan.setRealAgmntDate(agreementDate.getTime());		
		int realAgmntdayOfMonth = agreementDate.get(Calendar.DAY_OF_MONTH);
		
		if( realAgmntdayOfMonth < 20 ) {
			//int monthEndDay = 1;			
			if(realAgmntdayOfMonth <= monthEndDate) {//always 1 from nov 2018
				agreementDate.add(Calendar.MONTH, -1);
		        int lastDayOfPrevmonth = agreementDate.getActualMaximum(Calendar.DAY_OF_MONTH);
		        agreementDate.set(Calendar.DAY_OF_MONTH, lastDayOfPrevmonth);
			}
		} else {
			//int midMonthEndDay = 21;//mid month always 20 from nov 2018
			if(realAgmntdayOfMonth <= midMonthDate) {
		        agreementDate.set(Calendar.DAY_OF_MONTH, 20);
			}
		}
		
		loan.setAgmntDate(agreementDate.getTime());
		
		
		
		int emiMonth = 1;
		if(agreementDate.get(Calendar.DATE) > 20) {
			emiMonth = 2;
		}

		agreementDate.add(Calendar.MONTH, emiMonth);
		agreementDate.set(agreementDate.get(Calendar.YEAR), agreementDate.get(Calendar.MONTH), emiDate);		
		loan.setFirstDueDate(agreementDate.getTime());		
		agreementDate.add(Calendar.MONTH, (int)loan.getTenure());		
		loan.setLastDueDate(agreementDate.getTime());	
		
		int loanPaymentTenure=(int)loan.getTenure();
		int adVanceEmi = loan.getAdvanceEmi();
		Date tempLastDueDate = null;
		for ( int paymentCount=1; paymentCount<=loanPaymentTenure; paymentCount++){
			if(adVanceEmi < 1) {
				Calendar calendar=Calendar.getInstance();
				calendar.setTime(loan.getAgmntDate());
				calendar.add(Calendar.MONTH, (emiMonth-1));
				calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), emiDate);
				calendar.add(Calendar.MONTH, (paymentCount-loan.getAdvanceEmi())); 
				tempLastDueDate = calendar.getTime();
			}else {
				adVanceEmi -= 1;
			}
				
		}
		if(tempLastDueDate!=null){
			loan.setLastDueDate(tempLastDueDate);
		}
		
		
 		if(application.getType().equals("app")) {
 			loan.setLoanNo("ORFLTW"+customer.getId()+(System.currentTimeMillis()%10000));
 		} else {
 			loan.setLoanNo("ORFLSC"+customer.getId()+(System.currentTimeMillis()%10000)); 			
 		}
 		
		loan = loanRepository.save(loan);

		/*SchemeMaster scheme = schemeRepository.findByschemeId(application.getSchemeType());
		if(scheme != null) {
			if(scheme.getCashChargesTail() > 0)
				loanServiceImpl.addCrDrNote(getCrDrBean(loan.getId(), 3, scheme.getCashChargesTail(), "Cash Collection Charges Tail"));
			if(scheme.getProFeeTail() > 0) {
				float procFee = application.getLoanAmt() * (scheme.getProFeeTail()/100);
				loanServiceImpl.addCrDrNote(getCrDrBean(loan.getId(), 4, procFee , "Processing Fee Tail"));
			}
		}*/
		//String numberAsString = String.format("%010d", number);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String loanNo = String.format("%04d", Integer.parseInt(application.getBranchCode()))+(sdf.format(loan.getAgmntDate())).substring(0, 6)+String.format("%07d",loan.getId());
		String loanNoo = String.format("%04d", Integer.parseInt(application.getBranchCode()))+(sdf.format(loan.getAgmntDate())).substring(0, 6)+String.format("%07d",loan.getId());
		
		if(application.getType().equals("app")) {
				loan.setLoanNo("TW" + loanNo);
		} else {
			loan.setLoanNo("SC" + loanNo);
		}
		if(application.getType().equals("blapp")) {
			loan.setLoanNo("BL" + loanNoo);
		}
		loan = receivableService.computeReceivable(loan,emiMonth,emiDate);	

		loan = loanRepository.save(loan);
		
		
		LOSResponseDTO response = new LOSResponseDTO();
		response.setLmsCustomerId(""+customer.getId());
		response.setLmsLoanId(""+loan.getId());
		response.setStatusMessage("SUCCESS");
		response.setStatusCode("1");		
		try{//push to coleder SDK
			pushToColendService(loan);
			}catch(Exception e){
			log.error("error in colender push",e);
		}
		long timeTaken=System.currentTimeMillis()-startTimeInMillis;
		log.info("Time Taken for pushLOSData is ==>"+timeTaken);
		return response;
	}

	

	/*private CrDrBean getCrDrBean(int loanId, int noteId, float amount, String description) {
		CrDrBean crdrBean = new CrDrBean();
		crdrBean.setAmount(amount);
		crdrBean.setDescription(description);
		crdrBean.setLoanId(loanId);
		crdrBean.setNoteId(noteId);
		return crdrBean;
	}
*/
	private Loan populateLoanDetails(Application application) {
		Loan loan = new Loan();

		//loan.setCustomer("");
		//loan.setId("");
		ApplicationAnnex appannex =new ApplicationAnnex();
		
		appannex.setAppno(application.getAppno());
		ApplicationAnnex appann = appanexxRepo.findByAppno(application.getAppno());
		
		loan.setName(application.getFirstname() + " " + application.getLastname());
		loan.setApplNo(application.getAppno());
		loan.setAgmntDate(application.getAppCreditApprovedTime());
		
		loan.setFirstDueDate(application.getAppCreditApprovedTime());
		loan.setAmount(application.getTotalFinanceAmount());
		loan.setTenure(Float.parseFloat(application.getTenure()));
		loan.setIrr(application.getRoi());
		
		loan.setProcFee(application.getProcessingFee());
		loan.setPfBase(application.getPfBase());
		loan.setPfTax(application.getPftax());
		loan.setPfTail(application.getPfTail());
		loan.setPfTailBase(application.getPfTailBase());
		loan.setPfTailTax(application.getPfTailTax());		
		
		
loan.setDocumentCharge(appann.getDocumentCharge());
		loan.setDocumentChargeBase(appann.getDocumentChargeBase());
		loan.setDocumentChargeTax(appann.getDocumentChargeTax());
	
		
		loan.setEmi(application.getEmi());
		loan.setAdvanceEmi(Integer.parseInt(application.getAdvanceEmi()));
		
		loan.setInsuranceAmount(application.getInsuranceAmount());
		loan.setInsBase(application.getInsBase());
		loan.setInsTax(application.getInsTax());
		
		loan.setCashCollCharges(application.getCashCollCharges());
		loan.setCccBase(application.getCcBase());
		loan.setCccTax(application.getCcTax());
		loan.setCcTail(application.getCashCollChargesTail());
		loan.setCcTailBase(application.getCcTailBase());
		loan.setCcTailTax(application.getccTailTax());
		
		
		loan.setDealerDisbursementAmount(application.getDealerDisbursementAmount());
		loan.setDownPayment(application.getDownPayment());
		loan.setEmi(application.getEmi());
		loan.setH1(application.getOriginator());
		loan.setH2("ORANGE");
		loan.setH3(application.getAppBlCode());
		loan.setH4("");
		loan.setH5("HO");
		loan.setDealerCode(String.valueOf(application.getDealerCode()));
		loan.setDealerName(application.getDealerName());
		loan.setFuturePrinciple(application.getTotalFinanceAmount());
		loan.setRepayMode(application.getModeType());
		if(!application.getBranchCode().trim().equalsIgnoreCase("")){
			loan.setBranchCode(Integer.parseInt(application.getBranchCode().trim()));
		}
		
	

		Asset asset = new Asset();
		asset.setMake(application.getMake());
		asset.setModel(application.getProdSubcode());
		asset.setVariant(application.getVariant());
		asset.setAssetType("TW");
		//asset.setEngineNo("");
		//asset.setChassisNo("");
		asset.setSchemeType(application.getSchemeType());
		asset.setProdSubcode(application.getProdSubcode());
		asset.setTotalCost(application.getTotalCost());
		asset.setGridPrice(application.getGridPrice());
		asset.setOnroadPrice(application.getOnroadPrice());
		asset.setActualLtv(application.getActualLtv());
		asset.setLoan(loan);

		/*Set<Asset> assets = new HashSet<>();
		assets.add(asset);*/

		loan.setAsset(asset);

		return loan;
	}



	private Customer populateCustomerDetails(Application application, Customer customer) {
		log.info("INSIDE CUSTOMER");
		if(customer == null) 
			customer = new Customer();
		customer.setName(application.getFirstname() + " " + application.getLastname());
		customer.setPhone1(application.getMobileno());
		customer.setEmailId(application.getEmailId());
		//java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		customer.setDob(application.getDob());
		customer.setAge(application.getAge());
		customer.setCibilScore(application.getCibilScore());
		customer.setGender(application.getGender());
		customer.setReligion(application.getReligion());
		customer.setCaste(application.getCaste());
		customer.setFirstname(application.getFirstname());
		customer.setLastname(application.getLastname());
		customer.setFatherName(application.getFatherName());
		customer.setMotherName(application.getMotherName());
		customer.setSpouseName(application.getSpouseName());
		customer.setPassportNo(application.getPassportNo());
		customer.setVoterid(application.getVoterid());
		customer.setDrivingLicense(application.getDrivingLicense());
		customer.setRationCard(application.getRationCard());
		customer.setPanno(application.getPanno());
		customer.setNameInBank("");
		customer.setAccountNo(application.getBankaccno());
		customer.setIfscCode(application.getBankifsc());
		customer.setMaritalStatus(application.getMaritalStatus());
		customer.setJobType("");
		customer.setSalorself(application.getSalorself());
		customer.setProfile(application.getProfile());
		customer.setEducation(application.getFiEducation());
		boolean houseType = false;
		if(application.getHouseType().equalsIgnoreCase("OWN")){
			houseType = true;
		}
		customer.setOwnHouse(houseType);
		customer.setResiAddr(application.getResiAddr());
		customer.setResiAddress2(application.getResiAddress2());
		customer.setResiCity(application.getResiCity());
		customer.setResiState(application.getResiState());
		customer.setResiPin(application.getResiPin());
		customer.setResiPh(application.getResiStd()+""+application.getResiPh());
		customer.setResiMobile(application.getResiMobile());
		customer.setPermAddr(application.getPermAddr());
		customer.setPermAddr2(application.getPermAddr2());
		customer.setPermCity(application.getPermCity());
		customer.setPermState(application.getPermState());
		customer.setPermPincode(application.getPermPincode());
		customer.setOffAddr(application.getOffAddr());
		customer.setOffAddr2(application.getOffAddr2());
		customer.setOffCity(application.getOffCity());
		customer.setOffState(application.getOffState());
		customer.setOffPin(application.getOffPin());
		customer.setOffStd(application.getOffStd());
		customer.setOffPh(application.getOffPh());
		customer.setAadharNo(application.getAadhar());
		customer.setH1(application.getOriginator());
		customer.setH2("ORANGE");
		customer.setH3(application.getAppBlCode());
		customer.setH4("");
		customer.setH5("HO");
	/*	CustomerArchive custarc = new CustomerArchive();
		if(application.getlmsCusT() !="NULL") {
			Customer cust = customerRepository.findById(application.getlmsCusT());
			custarc = populateSingleCustomerDetails(application, cust);
			custarc = customerArchiveRepository.save(custarc);	
		}
		else
			log.info("ELSE PART CUSTOMER");*/

		return customer;
	}
	@Autowired	
	private ColendService colendService;	
	private void pushToColendService(Loan loan){	
		boolean pushToColend = false;	
		if(loan.getCoLender() != null	
				&& !loan.getCoLender().equalsIgnoreCase("") 	
				&& !loan.getCoLender().equalsIgnoreCase("none")){	
			pushToColend = true;	
		}	
		if(loan.getCoLenderDisposition() != null	
				&& !loan.getCoLenderDisposition().equalsIgnoreCase("") 	
				&& !loan.getCoLenderDisposition().equalsIgnoreCase("none")){	
			pushToColend = true;	
		}	
		// push only colender loan details along with customer details for loan	
		if(pushToColend){	
			colendService.pushToColend(loan);	
		}	
	}
	
	// for CRM - code edited by Bhuvana 13-04-2021
		@Override
		public String getCustomerData(String mobileNo){
			JSONObject jo = new JSONObject();
			JSONArray array = new JSONArray();
			
			List<Customer> customer = customerRepository.findByphone1StartingWith(mobileNo);
		for(int i=0;i<customer.size();i++) {
			Customer cust = new Customer();
			cust = customer.get(i);
			System.out.println("customer name ::"+cust.getName());
			System.out.println("customer name ::"+mobileNo);
			jo.put("MobileNumber", cust.getPhone1());
			jo.put("CustomerName",cust.getName());
			jo.put("CustomerId", cust.getId());
			String loanno = loanRepository.findcustId(cust.getId());
			jo.put("LoanNumber", loanno);
			array.add(jo);
		}
	        return array.toString();
		}

		
	
	/*private CustomerArchive populateSingleCustomerDetails(Application application, Customer customer) {
		log.info("INSIDE ARCHIVE");
		if(customer == null) 
			customer = new Customer();
		CustomerArchive custar = new CustomerArchive();
		
		custar.setName(customer.getFirstname() + " " + application.getLastname());
		custar.setPhone1(customer.getPhone1());
		custar.setEmailId(customer.getEmailId());
		//java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		custar.setDob(customer.getDob());
		custar.setAge(customer.getAge());
		custar.setCibilScore(customer.getCibilScore());
		custar.setGender(customer.getGender());
		custar.setReligion(customer.getReligion());
		custar.setCaste(customer.getCaste());
		custar.setFirstname(customer.getFirstname());
		custar.setLastname(customer.getLastname());
		custar.setFatherName(customer.getFatherName());
		custar.setMotherName(customer.getMotherName());
		custar.setSpouseName(customer.getSpouseName());
		custar.setPassportNo(customer.getPassportNo());
		custar.setVoterid(customer.getVoterid());
		custar.setDrivingLicense(customer.getDrivingLicense());
		custar.setRationCard(customer.getRationCard());
		custar.setPanno(customer.getPanno());
		custar.setNameInBank("");
		custar.setAccountNo(customer.getAccountNo());
		custar.setIfscCode(customer.getIfscCode());
		custar.setMaritalStatus(customer.getMaritalStatus());
		custar.setJobType("");
		custar.setSalorself(customer.getSalorself());
		custar.setProfile(customer.getProfile());
		custar.setEducation(application.getFiEducation());
		boolean houseType = false;
		if(application.getHouseType().equalsIgnoreCase("OWN")){
			houseType = true;
		}
		custar.setOwnHouse(houseType);
		custar.setResiAddr(customer.getResiAddr());
		custar.setResiAddress2(customer.getResiAddress2());
		custar.setResiCity(customer.getResiCity());
		custar.setResiState(customer.getResiState());
		custar.setResiPin(customer.getResiPin());
		custar.setResiPh(customer.getResiPh());
		custar.setResiMobile(customer.getResiMobile());
		custar.setPermAddr(customer.getPermAddr());
		custar.setPermAddr2(customer.getPermAddr2());
		custar.setPermCity(customer.getPermCity());
		custar.setPermState(customer.getPermState());
		custar.setPermPincode(customer.getPermPincode());
		custar.setOffAddr(customer.getOffAddr());
		custar.setOffAddr2(customer.getOffAddr2());
		custar.setOffCity(customer.getOffCity());
		custar.setOffState(customer.getOffState());
		custar.setOffPin(customer.getOffPin());
		custar.setOffStd(customer.getOffStd());
		custar.setOffPh(customer.getOffPh());
		custar.setAadharNo(customer.getAadharNo());
		custar.setH1(customer.getH1());
		custar.setH2("ORANGE");
		custar.setH3(customer.getH3());
		custar.setH4("");
		custar.setH5("HO");

		return custar;
	}*/
	
	
	
}
