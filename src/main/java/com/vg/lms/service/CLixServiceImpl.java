package com.vg.lms.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.vg.lms.model.Asset;
import com.vg.lms.model.Customer;
import com.vg.lms.model.Loan;
import com.vg.lms.repository.LoanRepository;


@Component
public class CLixServiceImpl {

    @Autowired
    private LoanRepository loanRepository;
    

    

	
    private final Logger log = LoggerFactory.getLogger(CLixServiceImpl.class);


  @Scheduled(fixedDelay = 600000)
    @SuppressWarnings("unused")
	private  void createClixXML() throws NullPointerException, ParserConfigurationException, TransformerException {

	  
        List < Loan > pendingtxn = loanRepository.findByCoLender("Clix");


        for (Loan clixAudit: pendingtxn) {
            Loan loan = loanRepository.findById(clixAudit.getId()).get();
          if(	loan.getLoanNo().contentEquals("TW00012019080093640"))

{              	
    String XmlFilePath = new String("/home/orange/git/lms_cdc/"+loan.getLoanNo()+".xml");        
    BufferedWriter br;
    String testt ="CHENAAI EXPRESS";
	Customer customer = loan.getCustomer();
            Asset asset = loan.getAsset();
            String FirstName = customer.getFirstname();
            String LAstName = customer.getLastname();
            String Mobile = customer.getPhone1();
            String FatherName = customer.getFatherName();
            String Gender = customer.getGender();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
            String agmnt_date = sdf.format(loan.getDisbursementTime());
            int advanceemi = loan.getAdvanceEmi();
            Date date = new Date();
        	SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        	String strDate= formatter.format(date);
            int amtemi = Math.round(loan.getEmi());
            String ademi = Integer.toString(advanceemi);
            String loan_amount = Double.toString((loan.getAmount()*.8));
            int lnamt = (int)(Math.round((loan.getAmount()*.8)));
            int tenure1 = Math.round(loan.getTenure());
            String tenr = Integer.toString(tenure1);
            String laonnamt = Integer.toString(lnamt);
            String loan_no = loan.getLoanNo();
            int endMonth = tenure1 - advanceemi;
            
            String emi_amt = Float.toString(Math.round(loan.getEmi()));
            String amtemi1 = Integer.toString(amtemi);   
            
            String branch = loan.getH3();
            if(loan.getH3().contentEquals("Tenkasi"))
            	branch ="TIRUNELVELI";
            else if(loan.getH3().contentEquals("Tuticorin"))
            	branch="THOOTHUKUDI";
            else if(loan.getH3().contentEquals("THIRUNELVELI"))
            	branch="TIRUNELVELI";
            else if(loan.getH3().contentEquals("Theni"))
            	branch="THENI";
            
            else if(loan.getH3().contentEquals("Madurai"))
            	branch="MADURAI";
            else if(loan.getH3().contentEquals("Sivakasi"))
            	branch="VIRUDHUNAG";
            else if(loan.getH3().contentEquals("Nagercoil"))
            	branch="NAGARCOIL";

          
            if(loan.getH3().contentEquals("Tenkasi"))
            	branch ="TIRUNELVELI";
            else if(loan.getH3().contentEquals("Tuticorin"))
            	branch="THOOTHUKUDI";
            else if(loan.getH3().contentEquals("THIRUNELVELI"))
            	branch="TIRUNELVELI";
            else if(loan.getH3().contentEquals("Theni"))
            	branch="THENI";
            
            else if(loan.getH3().contentEquals("Madurai"))
            	branch="MADURAI";
            else if(loan.getH3().contentEquals("Sivakasi"))
            	branch="VIRUDHUNAG";

            
            String resi_City = customer.getResiCity();

            // String martialStatus = customer.getMaritalStatus();
            String custId = Integer.toString(customer.getId());
            
            
            
           
            
            DocumentBuilderFactory dbFactory =
               DocumentBuilderFactory.newInstance();
           DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
           Document doc = dBuilder.newDocument();


            Element rootElement = doc.createElement("CUST_DTL");
            doc.appendChild(rootElement);

            Element rowset = doc.createElement("ROWSET");
            rootElement.appendChild(rowset);

            Element row = doc.createElement("ROW");
            rowset.appendChild(row);

            Element ijc1 = doc.createElement("IJC1");
            ijc1.appendChild(doc.createTextNode("001"));
            row.appendChild(ijc1);

            Element ijc2 = doc.createElement("IJC2");
            row.appendChild(ijc2);

            Element ijc3 = doc.createElement("IJC3");
            ijc3.appendChild(doc.createTextNode(custId));
            row.appendChild(ijc3);

            Element ijc4 = doc.createElement("IJC4");
            ijc4.appendChild(doc.createTextNode("CU"));
            row.appendChild(ijc4);

            String Gendr = new String();
            if (Gender.contentEquals("M")) {
                Gendr = "Mr.";
            } else {
                Gendr = "Ms.";
            }
            Element ijc5 = doc.createElement("IJC5");
            ijc5.appendChild(doc.createTextNode(Gendr));
            row.appendChild(ijc5);

            Element ijc6 = doc.createElement("IJC6");
            ijc6.appendChild(doc.createTextNode(FirstName));
            row.appendChild(ijc6);



            Element ijc7 = doc.createElement("IJC7");
            row.appendChild(ijc7);

            Element ijc8 = doc.createElement("IJC8");
            ijc8.appendChild(doc.createTextNode(LAstName));
            row.appendChild(ijc8);

            Element ijc9 = doc.createElement("IJC9");
            row.appendChild(ijc9);

            Element ijc10 = doc.createElement("IJC10");
            ijc10.appendChild(doc.createTextNode(Mobile));
            row.appendChild(ijc10);

            Element ijc11 = doc.createElement("IJC11");
            ijc11.appendChild(doc.createTextNode(""));
            row.appendChild(ijc11);


            Element ijc12 = doc.createElement("IJC12");
            row.appendChild(ijc12);

            Element ijc13 = doc.createElement("IJC13");
            row.appendChild(ijc13);

            Element ijc14 = doc.createElement("IJC14");
            ijc14.appendChild(doc.createTextNode("info@orangeretailfinance.com"));
            row.appendChild(ijc14);

            Element ijc15 = doc.createElement("IJC15");
            row.appendChild(ijc15);

            Element ijc16 = doc.createElement("IJC16");
            ijc16.appendChild(doc.createTextNode("  "));
            row.appendChild(ijc16);

            Element ijc17 = doc.createElement("IJC17");
            ijc17.appendChild(doc.createTextNode("10:00AM"));
            row.appendChild(ijc17);

            Element ijc18 = doc.createElement("IJC18");
            ijc18.appendChild(doc.createTextNode("06:00PM"));
            row.appendChild(ijc18);

            Element ijc19 = doc.createElement("IJC19");
            ijc19.appendChild(doc.createTextNode("I"));
            row.appendChild(ijc19);

            Element ijc20 = doc.createElement("IJC20");
            ijc20.appendChild(doc.createTextNode("Phone"));
            row.appendChild(ijc20);

            Element ijc21 = doc.createElement("IJC21");
            row.appendChild(ijc21);
            Element ijc22 = doc.createElement("IJC22");
            row.appendChild(ijc22);

            Element ijc23 = doc.createElement("IJC23");
            row.appendChild(ijc23);

            Element ijc24 = doc.createElement("IJC24");
            row.appendChild(ijc24);

            Element ijc25 = doc.createElement("IJC25");
            row.appendChild(ijc25);

            Element ijc26 = doc.createElement("IJC26");
            row.appendChild(ijc26);

            Element ijc27 = doc.createElement("IJC27");
            row.appendChild(ijc27);

            Element ijc28 = doc.createElement("IJC28");
            row.appendChild(ijc28);

            Element ijc29 = doc.createElement("IJC29");
            ijc29.appendChild(doc.createTextNode("ORANGE"));
            row.appendChild(ijc29);

            Element ijc30 = doc.createElement("IJC30");
            ijc30.appendChild(doc.createTextNode("CLIX_CHENN"));
            row.appendChild(ijc30);

            Element ijc31 = doc.createElement("IJC31");
            row.appendChild(ijc31);
            Element ijc41 = doc.createElement("IJC41");
            row.appendChild(ijc41);
            Element ijc42 = doc.createElement("IJC42");
            row.appendChild(ijc42);
            Element ijc43 = doc.createElement("IJC43");
            row.appendChild(ijc43);
            Element ijc44 = doc.createElement("IJC44");
            row.appendChild(ijc44);
            Element ijc45 = doc.createElement("IJC45");
            row.appendChild(ijc45);
            Element ijc46 = doc.createElement("IJC46");
            row.appendChild(ijc46);
            Element ijc47 = doc.createElement("IJC47");
            row.appendChild(ijc47);
            Element ijc48 = doc.createElement("IJC48");
            row.appendChild(ijc48);
            Element ijc49 = doc.createElement("IJC49");
            row.appendChild(ijc49);
            Element ijc50 = doc.createElement("IJC50");
            row.appendChild(ijc50);
            Element ijc51 = doc.createElement("IJC51");
            row.appendChild(ijc51);
            Element ijc52 = doc.createElement("IJC52");
            row.appendChild(ijc52);

            Element iji3 = doc.createElement("IJI3");
            iji3.appendChild(doc.createTextNode(FatherName));
            row.appendChild(iji3);

            Element iji4 = doc.createElement("IJI4");
            iji4.appendChild(doc.createTextNode(Gender));
            row.appendChild(iji4);

            String[] dateArray = customer.getDob().split("-");
            Element iji5 = doc.createElement("IJI5");
           String month = new String();
            if(dateArray[1].contentEquals("01"))
            {
            	month = "JAN";
            }
            if(dateArray[1].contentEquals("02"))
            {
            	month = "FEB";
            }
            if(dateArray[1].contentEquals("03"))
            {
            	month = "MAR";
            }
            if(dateArray[1].contentEquals("04"))
            {
            	month = "APR";
            }
            if(dateArray[1].contentEquals("05"))
            {
            	month = "MAY";
            }
            if(dateArray[1].contentEquals("06"))
            {
            	month = "JUN";
            }
            if(dateArray[1].contentEquals("07"))
            {
            	month = "JUL";
            }
            if(dateArray[1].contentEquals("08"))
            {
            	month = "AUG";
            }
            if(dateArray[1].contentEquals("09"))
            {
            	month = "SEP";
            }
            if(dateArray[1].contentEquals("10"))
            {
            	month = "OCT";
            }
            if(dateArray[1].contentEquals("11"))
            {
            	month = "NOV";
            }
            if(dateArray[1].contentEquals("12"))
            {
            	month = "DEC";
            }
           
            iji5.appendChild(doc.createTextNode(dateArray[0] + "-" + month + "-" + dateArray[2]));
            row.appendChild(iji5);

            Element iji6 = doc.createElement("IJI6");
            row.appendChild(iji6);

            Element iji7 = doc.createElement("IJI7");
            row.appendChild(iji7);

            Element iji8 = doc.createElement("IJI8");
            iji8.appendChild(doc.createTextNode("0"));
            row.appendChild(iji8);

            Element iji9 = doc.createElement("IJI9");
            row.appendChild(iji9);

            Element iji10 = doc.createElement("IJI10");
            row.appendChild(iji10);

            Element iji11 = doc.createElement("IJI11");
            row.appendChild(iji11);

            Element iji12 = doc.createElement("IJI12");
            row.appendChild(iji12);

            Element iji13 = doc.createElement("IJI13");
            row.appendChild(iji13);

            Element iji14 = doc.createElement("IJI14");
            row.appendChild(iji14);

            Element iji15 = doc.createElement("IJI15");
            row.appendChild(iji15);
            Element iji16 = doc.createElement("IJI16");
            row.appendChild(iji16);
            Element iji17 = doc.createElement("IJI17");
            row.appendChild(iji17);
            Element iji18 = doc.createElement("IJI18");
            row.appendChild(iji18);
            Element iji19 = doc.createElement("IJI19");
            row.appendChild(iji19);
            Element iji20 = doc.createElement("IJI20");
            row.appendChild(iji20);
            Element iji21 = doc.createElement("IJI21");
            row.appendChild(iji21);

            Element iji22 = doc.createElement("IJI22");
            row.appendChild(iji22);

            Element iji23 = doc.createElement("IJI23");
            row.appendChild(iji23);

            Element iji24 = doc.createElement("IJI24");
            row.appendChild(iji24);

            Element iji25 = doc.createElement("IJI25");
            row.appendChild(iji25);

            Element iji26 = doc.createElement("IJI26");
            row.appendChild(iji26);

            Element iji27 = doc.createElement("IJI27");
            row.appendChild(iji27);

            Element iji28 = doc.createElement("IJI28");
            row.appendChild(iji28);

            Element iji29 = doc.createElement("IJI29");
            row.appendChild(iji29);

            Element iji30 = doc.createElement("IJI30");
            row.appendChild(iji30);

            Element iji31 = doc.createElement("IJI31");
            row.appendChild(iji31);


            Element iji32 = doc.createElement("IJI32");
            row.appendChild(iji32);

            Element iji33 = doc.createElement("IJI33");
            row.appendChild(iji33);

            Element iji34 = doc.createElement("IJI34");
            row.appendChild(iji34);

            Element iji35 = doc.createElement("IJI35");
            row.appendChild(iji35);

            Element iji36 = doc.createElement("IJI36");
            row.appendChild(iji36);

            Element iji37 = doc.createElement("IJI37");
            iji37.appendChild(doc.createTextNode("ORANGE"));
            row.appendChild(iji37);
            Element iji38 = doc.createElement("IJI38");
            row.appendChild(iji38);

            Element ijo3 = doc.createElement("IJO3");
            row.appendChild(ijo3);

            Element ijo4 = doc.createElement("IJO4");
            row.appendChild(ijo4);
            Element ijo5 = doc.createElement("IJO5");
            row.appendChild(ijo5);
            Element ijo6 = doc.createElement("IJO6");
            row.appendChild(ijo6);
            Element ijo7 = doc.createElement("IJO7");
            row.appendChild(ijo7);
            Element ijo8 = doc.createElement("IJO8");
            row.appendChild(ijo8);
            Element ijo9 = doc.createElement("IJO9");
            row.appendChild(ijo9);
            Element ijo10 = doc.createElement("IJO10");
            row.appendChild(ijo10);
            Element ijo11 = doc.createElement("IJO11");
            row.appendChild(ijo11);
            Element ijo12 = doc.createElement("IJO12");
            row.appendChild(ijo12);
            Element ijo13 = doc.createElement("IJO13");
            row.appendChild(ijo13);
            Element ijo14 = doc.createElement("IJO14");
            row.appendChild(ijo14);
            Element ijo15 = doc.createElement("IJO15");
            row.appendChild(ijo15);
            Element ijo16 = doc.createElement("IJO16");
            row.appendChild(ijo16);
            Element ijo17 = doc.createElement("IJO17");
            row.appendChild(ijo17);
            Element ijo18 = doc.createElement("IJO18");
            row.appendChild(ijo18);
            Element ijo19 = doc.createElement("IJO19");
            row.appendChild(ijo19);
            Element ijo20 = doc.createElement("IJO20");
            row.appendChild(ijo20);
            Element ijo21 = doc.createElement("IJO21");
            row.appendChild(ijo21);
            Element ijo22 = doc.createElement("IJO22");
            row.appendChild(ijo22);
            Element ijo23 = doc.createElement("IJO23");
            row.appendChild(ijo23);
            Element ijo24 = doc.createElement("IJO24");
            row.appendChild(ijo24);
            Element ijo25 = doc.createElement("IJO25");
            row.appendChild(ijo25);
            Element ijo26 = doc.createElement("IJO26");
            row.appendChild(ijo26);
            Element ijo27 = doc.createElement("IJO27");
            row.appendChild(ijo27);
            Element ijo28 = doc.createElement("IJO28");
            row.appendChild(ijo28);
            Element ijo29 = doc.createElement("IJO29");
            row.appendChild(ijo29);
            Element ijo30 = doc.createElement("IJO30");
            row.appendChild(ijo30);
            Element ijo31 = doc.createElement("IJO31");
            row.appendChild(ijo31);

            DOMSource sourc = new DOMSource(doc);
            
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            String outpt = writer.toString();
         log.info("XML:" + outpt);
        			
        			
            // write the content into xml file
   TransformerFactory transformerFactory = TransformerFactory.newInstance();
           Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
           transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            //StreamResult consoleResult = new StreamResult(System.out);
           StreamResult consoleResult = new StreamResult(new File(XmlFilePath)); 
           transformer.transform(source, consoleResult);
        
           
            DocumentBuilderFactory dbFactory1 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder1 = dbFactory1.newDocumentBuilder();
            Document doc1 = dBuilder1.newDocument();
            
            DocumentBuilderFactory dbFactory121 =
                    DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder121 = dbFactory121.newDocumentBuilder();
                Document doc121 = dBuilder121.newDocument();
                Element rootElement121 = doc121.createElement("GRP_XREF");
                          
                doc121.appendChild(rootElement121);


                TransformerFactory transformerFactory121 = TransformerFactory.newInstance();
                Transformer transformer121 = transformerFactory121.newTransformer();
                transformer121.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                DOMSource source121 = new DOMSource(doc121);
                // Output to console for testing
                StreamResult consoleresult121 = new StreamResult(System.out);
                transformer121.transform(source121, consoleresult121);
                try {
                	log.info("Inside Buffer ;" + source121.toString());
        			FileWriter fr = new FileWriter(XmlFilePath,true);
        			br = new BufferedWriter(fr);
        			br.newLine();
        			br.write(source121.toString());
        			br.close();
        			log.info("Inside Buffer END :" + testt);
        		} catch (IOException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		}

               

            Element rootElement1 = doc1.createElement("ADDRESS_DTL");
            doc1.appendChild(rootElement1);


            Element rowset1 = doc1.createElement("ROWSET");
            rootElement1.appendChild(rowset1);

            Element row1 = doc1.createElement("ROW");
            rowset1.appendChild(row1);

            Element il1 = doc1.createElement("IL1");
            il1.appendChild(doc1.createTextNode("001"));
            row1.appendChild(il1);
            Element il2 = doc1.createElement("IL2");
            il2.appendChild(doc1.createTextNode(custId));
            row1.appendChild(il2);
            Element il3 = doc1.createElement("IL3");
            il3.appendChild(doc1.createTextNode("1"));
            row1.appendChild(il3);
            Element il4 = doc1.createElement("IL4");
            il4.appendChild(doc1.createTextNode("CU"));
            row1.appendChild(il4);
            Element il5 = doc1.createElement("IL5");
            row1.appendChild(il5);
            Element il6 = doc1.createElement("IL6");
            row1.appendChild(il6);
            Element il7 = doc1.createElement("IL7");
            il7.appendChild(doc1.createTextNode(Mobile));
            row1.appendChild(il7);
            Element il8 = doc1.createElement("IL8");
            il8.appendChild(doc1.createTextNode("info@orangeretailfinance.com"));
            row1.appendChild(il8);
            Element il9 = doc1.createElement("IL9");
            il9.appendChild(doc1.createTextNode("CU"));
            row1.appendChild(il9);
            Element il10 = doc1.createElement("IL10");
            il10.appendChild(doc1.createTextNode("10:00AM"));
            row1.appendChild(il10);
            Element il11 = doc1.createElement("IL11");
            il11.appendChild(doc1.createTextNode("6:00PM"));
            row1.appendChild(il11);
            Element il12 = doc1.createElement("IL12");
            il12.appendChild(doc1.createTextNode("ORANGE"));
            row1.appendChild(il12);




            String resi_addr = customer.getResiAddr();
            Element im3 = doc1.createElement("IM3");
            im3.appendChild(doc1.createTextNode(resi_addr));
            row1.appendChild(im3);

            String resi_addr1 = customer.getResiAddress2();
            Element im4 = doc1.createElement("IM4");
            im4.appendChild(doc1.createTextNode(resi_addr1));
            row1.appendChild(im4);

           
            Element im5 = doc1.createElement("IM5");
            im5.appendChild(doc1.createTextNode(branch));
            row1.appendChild(im5);

            String resi_state = customer.getResiState();
            Element im6 = doc1.createElement("IM6");
            im6.appendChild(doc1.createTextNode("TAMILNADU"));
            row1.appendChild(im6);

            String resi_pincode = customer.getResiPin();
            Element im7 = doc1.createElement("IM7");
            im7.appendChild(doc1.createTextNode(resi_pincode));
            row1.appendChild(im7);
            Element im8 = doc1.createElement("IM8");
            row1.appendChild(im8);
            Element im9 = doc1.createElement("IM9");
            row1.appendChild(im9);

            Element im10 = doc1.createElement("IM10");
            row1.appendChild(im10);
            Element im11 = doc1.createElement("IM11");
            row1.appendChild(im11);
            Element im12 = doc1.createElement("IM12");
            row1.appendChild(im12);
            Element im13 = doc1.createElement("IM13");
            row1.appendChild(im13);
            Element im14 = doc1.createElement("IM14");
            row1.appendChild(im14);
            Element im16 = doc1.createElement("IM16");
            row1.appendChild(im16);
            Element row2 = doc1.createElement("ROW");
            rowset1.appendChild(row2);
            Element il01 = doc1.createElement("IL1");
            il01.appendChild(doc1.createTextNode("001"));
            row2.appendChild(il01);
            Element il02 = doc1.createElement("IL2");
            il02.appendChild(doc1.createTextNode(custId));
            row2.appendChild(il02);
            Element il03 = doc1.createElement("IL3");
            il03.appendChild(doc1.createTextNode("1"));
            row2.appendChild(il03);
            Element il04 = doc1.createElement("IL4");
            il04.appendChild(doc1.createTextNode("PR"));
            row2.appendChild(il04);
            Element il05 = doc1.createElement("IL5");
            row2.appendChild(il05);
            Element il06 = doc1.createElement("IL6");
            row2.appendChild(il06);
            Element il07 = doc1.createElement("IL7");
            il07.appendChild(doc1.createTextNode(Mobile));
            row2.appendChild(il07);
            Element il08 = doc1.createElement("IL8");
            il08.appendChild(doc1.createTextNode("info@orangeretailfinance.com"));
            row2.appendChild(il08);
            Element il09 = doc1.createElement("IL9");
            il09.appendChild(doc1.createTextNode("PR"));
            row2.appendChild(il09);
            Element il010 = doc1.createElement("IL10");
            il010.appendChild(doc1.createTextNode("10:00AM"));
            row2.appendChild(il010);
            Element il011 = doc1.createElement("IL11");
            il011.appendChild(doc1.createTextNode("6:00PM"));
            row2.appendChild(il011);
            Element il012 = doc1.createElement("IL12");
            il012.appendChild(doc1.createTextNode("ORANGE"));
            row2.appendChild(il012);

            Element im03 = doc1.createElement("IM3");
            im03.appendChild(doc1.createTextNode(resi_addr));
            row2.appendChild(im03);

            Element im04 = doc1.createElement("IM4");
            im04.appendChild(doc1.createTextNode(resi_addr1));
            row2.appendChild(im04);
            Element im05 = doc1.createElement("IM5");
            im05.appendChild(doc1.createTextNode(branch));
            row2.appendChild(im05);
            Element im06 = doc1.createElement("IM6");
            im06.appendChild(doc1.createTextNode("TAMILNADU"));
            row2.appendChild(im06);
            Element im07 = doc1.createElement("IM7");
            im07.appendChild(doc1.createTextNode(resi_pincode));
            row2.appendChild(im07);
            Element im08 = doc1.createElement("IM8");
            row2.appendChild(im08);
            Element im09 = doc1.createElement("IM9");
            row2.appendChild(im09);

            Element im010 = doc1.createElement("IM10");
            row2.appendChild(im010);
            Element im011 = doc1.createElement("IM11");
            row2.appendChild(im011);
            Element im012 = doc1.createElement("IM12");
            row2.appendChild(im012);
            Element im013 = doc1.createElement("IM13");
            row2.appendChild(im013);
            Element im014 = doc1.createElement("IM14");
            row2.appendChild(im014);
            Element im016 = doc1.createElement("IM16");
            row2.appendChild(im016);


            // write the content into xml file
            TransformerFactory transformerFactory1 = TransformerFactory.newInstance();
            Transformer transformer1 = transformerFactory1.newTransformer();
            transformer1.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source1 = new DOMSource(doc1);


            // Output to console for testing
            StreamResult consoleResult1 = new StreamResult(System.out);
            transformer1.transform(source1, consoleResult1);
            
            DocumentBuilderFactory dbFactory114 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder114 = dbFactory114.newDocumentBuilder();
            Document doc114 = dBuilder114.newDocument();
            Element rootElement114 = doc114.createElement("EMP_DTL");
            doc114.appendChild(rootElement114);


            TransformerFactory transformerFactory114 = TransformerFactory.newInstance();
            Transformer transformer114 = transformerFactory114.newTransformer();
            transformer114.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source114 = new DOMSource(doc114);


            // Output to console for testing
            StreamResult consoleresult114 = new StreamResult(System.out);
            transformer114.transform(source114, consoleresult114);
            DocumentBuilderFactory dbFactory115 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder115 = dbFactory115.newDocumentBuilder();
            Document doc115 = dBuilder115.newDocument();
            Element rootElement115 = doc115.createElement("SELFEMP_DTL");
            doc115.appendChild(rootElement115);


            TransformerFactory transformerFactory115 = TransformerFactory.newInstance();
            Transformer transformer115 = transformerFactory115.newTransformer();
            transformer115.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source115 = new DOMSource(doc115);


            // Output to console for testing
            StreamResult consoleresult115 = new StreamResult(System.out);
            transformer115.transform(source115, consoleresult115);
            DocumentBuilderFactory dbFactory116 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder116 = dbFactory116.newDocumentBuilder();
            Document doc116 = dBuilder116.newDocument();
            Element rootElement116 = doc116.createElement("SALARY_INCOME");
            doc116.appendChild(rootElement116);


            TransformerFactory transformerFactory116 = TransformerFactory.newInstance();
            Transformer transformer116 = transformerFactory116.newTransformer();
            transformer116.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source116 = new DOMSource(doc116);


            // Output to console for testing
            StreamResult consoleresult116 = new StreamResult(System.out);
            transformer116.transform(source116, consoleresult116);
            DocumentBuilderFactory dbFactory117 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder117 = dbFactory117.newDocumentBuilder();
            Document doc117 = dBuilder117.newDocument();
            Element rootElement117 = doc117.createElement("BUSINESS_INCOME");
            doc117.appendChild(rootElement117);


            TransformerFactory transformerFactory117 = TransformerFactory.newInstance();
            Transformer transformer117 = transformerFactory117.newTransformer();
            transformer117.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source117 = new DOMSource(doc117);


            // Output to console for testing
            StreamResult consoleresult117 = new StreamResult(System.out);
            transformer117.transform(source117, consoleresult117);
            DocumentBuilderFactory dbFactory118 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder118 = dbFactory118.newDocumentBuilder();
            Document doc118 = dBuilder118.newDocument();
            Element rootElement118 = doc118.createElement("ORG_INCOME_DETAILS");
            doc118.appendChild(rootElement118);


            TransformerFactory transformerFactory118 = TransformerFactory.newInstance();
            Transformer transformer118 = transformerFactory118.newTransformer();
            transformer118.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source118 = new DOMSource(doc118);


            // Output to console for testing
            StreamResult consoleresult118 = new StreamResult(System.out);
            transformer118.transform(source118, consoleresult118);
            DocumentBuilderFactory dbFactory119 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder119 = dbFactory119.newDocumentBuilder();
            Document doc119 = dBuilder119.newDocument();
            Element rootElement119 = doc119.createElement("OTHER_INCOME_DETAILS");
            doc119.appendChild(rootElement119);


            TransformerFactory transformerFactory119 = TransformerFactory.newInstance();
            Transformer transformer119 = transformerFactory119.newTransformer();
            transformer119.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source119 = new DOMSource(doc119);


            // Output to console for testing
            StreamResult consoleresult119 = new StreamResult(System.out);
            transformer119.transform(source119, consoleresult119);
            DocumentBuilderFactory dbFactory120 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder120 = dbFactory120.newDocumentBuilder();
            Document doc120 = dBuilder120.newDocument();
            Element rootElement120 = doc120.createElement("BANK_DTL");
            doc120.appendChild(rootElement120);
            Element rowset35 = doc120.createElement("ROWSET");
            rootElement120.appendChild(rowset35);

            Element row35 = doc120.createElement("ROW");
            rowset35.appendChild(row35);
			
 		Element ir1 = doc120.createElement("IR1");
            ir1.appendChild(doc120.createTextNode("001"));
            row35.appendChild(ir1);
			
			Element ir2 = doc120.createElement("IR2");
            ir2.appendChild(doc120.createTextNode(custId));
            row35.appendChild(ir2);
			
			Element ir3 = doc120.createElement("IR3");
            ir3.appendChild(doc120.createTextNode("1"));
            row35.appendChild(ir3);
			Element ir4 = doc120.createElement("IR4");
            ir4.appendChild(doc120.createTextNode("ICICI"));
            row35.appendChild(ir4);
			Element ir5 = doc120.createElement("IR5");
            ir5.appendChild(doc120.createTextNode("ICI006022"));
            row35.appendChild(ir5);
			Element ir6 = doc120.createElement("IR6");
            ir6.appendChild(doc120.createTextNode("SA"));
            row35.appendChild(ir6);
			Element ir7 = doc120.createElement("IR7");
            ir7.appendChild(doc120.createTextNode("777705190719"));
            row35.appendChild(ir7);
			Element ir8 = doc120.createElement("IR8");
            ir8.appendChild(doc120.createTextNode(strDate));
            row35.appendChild(ir8);
			Element ir9 = doc120.createElement("IR9");
                       row35.appendChild(ir9);
			Element ir10 = doc120.createElement("IR10");
                        row35.appendChild(ir10);
			Element ir11 = doc120.createElement("IR11");
                        row35.appendChild(ir11);
			Element ir12 = doc120.createElement("IR12");
                        row35.appendChild(ir12);
			Element ir13 = doc120.createElement("IR13");
                        row35.appendChild(ir13);
			Element ir14 = doc120.createElement("IR14");
                        row35.appendChild(ir14);
			Element ir15 = doc120.createElement("IR15");
                        row35.appendChild(ir15);
			Element ir16 = doc120.createElement("IR16");
                        row35.appendChild(ir16);
			Element ir17 = doc120.createElement("IR17");
                       row35.appendChild(ir17);
			Element ir18 = doc120.createElement("IR18");
                       row35.appendChild(ir18);
			Element ir19 = doc120.createElement("IR19");
                       row35.appendChild(ir19);
			Element ir20 = doc120.createElement("IR20");
                       row35.appendChild(ir20);
			Element ir21 = doc120.createElement("IR21");
                       row35.appendChild(ir21);
			Element ir22 = doc120.createElement("IR22");
                       row35.appendChild(ir22);
			Element ir23 = doc120.createElement("IR23");
                       row35.appendChild(ir23);
			Element ir24 = doc120.createElement("IR24");
                       row35.appendChild(ir24);
			Element ir25 = doc120.createElement("IR25");
                       row35.appendChild(ir25);
			Element ir26 = doc120.createElement("IR26");
                       row35.appendChild(ir26);
			Element ir27 = doc120.createElement("IR27");
                       row35.appendChild(ir27);
					   
			Element ir28 = doc120.createElement("IR28");
            ir28.appendChild(doc120.createTextNode("ORANGE"));
            row35.appendChild(ir28);
			
			Element ir31 = doc120.createElement("IR31");
            ir31.appendChild(doc120.createTextNode("Y"));
            row35.appendChild(ir31);
			Element ir32 = doc120.createElement("IR32");
            ir32.appendChild(doc120.createTextNode("Orange Disbursement (CCSPL)"));
            row35.appendChild(ir32);
			Element ir33 = doc120.createElement("IR33");
            ir33.appendChild(doc120.createTextNode(laonnamt));
            row35.appendChild(ir33);
			Element ir34 = doc120.createElement("IR34");
            ir34.appendChild(doc120.createTextNode("D"));
            row35.appendChild(ir34);
            Element ir35 = doc120.createElement("IR35");
            ir35.appendChild(doc120.createTextNode(loan_no));
            row35.appendChild(ir35);
			
			
    			

            TransformerFactory transformerFactory120 = TransformerFactory.newInstance();
            Transformer transformer120 = transformerFactory120.newTransformer();
            transformer120.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source120 = new DOMSource(doc120);


            // Output to console for testing
            StreamResult consoleresult120 = new StreamResult(System.out);
            transformer120.transform(source120, consoleresult120);


            DocumentBuilderFactory dbFactory3 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder3 = dbFactory3.newDocumentBuilder();
            Document doc2 = dBuilder3.newDocument();
            Element rootElement2 = doc2.createElement("LOAN_ACC");
            doc2.appendChild(rootElement2);
            Element rowset2 = doc2.createElement("ROWSET");
            rootElement2.appendChild(rowset2);
            Element row3 = doc2.createElement("ROW");
            rowset2.appendChild(row3);

            Element ia1 = doc2.createElement("IA1");
            ia1.appendChild(doc2.createTextNode("001"));
            row3.appendChild(ia1);

            
            Element ia2 = doc2.createElement("IA2");
            ia2.appendChild(doc2.createTextNode(loan_no));
            row3.appendChild(ia2);
            Element ia3 = doc2.createElement("IA3");
            ia3.appendChild(doc2.createTextNode("CLIX_CHENN"));
            row3.appendChild(ia3);
            Element ia4 = doc2.createElement("IA4");
            ia4.appendChild(doc2.createTextNode("TW"));
            row3.appendChild(ia4);
            Element ia5 = doc2.createElement("IA5");
            ia5.appendChild(doc2.createTextNode("ECS"));
            row3.appendChild(ia5);
            Element ia6 = doc2.createElement("IA6");
            ia6.appendChild(doc2.createTextNode("1"));
            row3.appendChild(ia6);
            Element ia7 = doc2.createElement("IA7");
            ia7.appendChild(doc2.createTextNode("L"));
            row3.appendChild(ia7);
            Element ia8 = doc2.createElement("IA8");
            ia8.appendChild(doc2.createTextNode("Y"));
            row3.appendChild(ia8);
            Element ia9 = doc2.createElement("IA9");
            ia9.appendChild(doc2.createTextNode("N"));
            row3.appendChild(ia9);
            Element ia10 = doc2.createElement("IA10");
            ia10.appendChild(doc2.createTextNode("5"));
            row3.appendChild(ia10);
            Element ia11 = doc2.createElement("IA11");
            ia11.appendChild(doc2.createTextNode("N"));
            row3.appendChild(ia11);
            Element ia12 = doc2.createElement("IA12");
            ia12.appendChild(doc2.createTextNode("0"));
            row3.appendChild(ia12);
            Element ia13 = doc2.createElement("IA13");
            ia13.appendChild(doc2.createTextNode("INR"));
            row3.appendChild(ia13);
            Element ia14 = doc2.createElement("IA14");
            ia14.appendChild(doc2.createTextNode("12"));
            row3.appendChild(ia14);
            Element ia15 = doc2.createElement("IA15");
            row3.appendChild(ia15);
            Element ia16 = doc2.createElement("IA16");
            row3.appendChild(ia16);
            Element ia17 = doc2.createElement("IA17");
            ia17.appendChild(doc2.createTextNode("ORANGE"));
            row3.appendChild(ia17);
            Element ia18 = doc2.createElement("IA18");
            row3.appendChild(ia18);
            Element ia19 = doc2.createElement("IA19");
            row3.appendChild(ia19);
            Element ia20 = doc2.createElement("IA20");
            row3.appendChild(ia20);
            Element ia21 = doc2.createElement("IA21");
            row3.appendChild(ia21);

            Element ia23 = doc2.createElement("IA23");
            row3.appendChild(ia23);
            Element ia24 = doc2.createElement("IA24");
            ia24.appendChild(doc2.createTextNode("D"));
            row3.appendChild(ia24);
            Element ia25 = doc2.createElement("IA25");
            ia25.appendChild(doc2.createTextNode("N"));
            row3.appendChild(ia25);
            Element ia26 = doc2.createElement("IA26");
            row3.appendChild(ia26);
            Element ia27 = doc2.createElement("IA27");
            row3.appendChild(ia27);
            Element ia28 = doc2.createElement("IA28");
            row3.appendChild(ia28);
            Element ia29 = doc2.createElement("IA29");
            row3.appendChild(ia29);
            Element ia30 = doc2.createElement("IA30");
            row3.appendChild(ia30);

            Element ia35 = doc2.createElement("IA35");
            row3.appendChild(ia35);
            Element ia36 = doc2.createElement("IA36");
            row3.appendChild(ia36);
            Element ia37 = doc2.createElement("IA37");
            ia37.appendChild(doc2.createTextNode("CLIX_CHENN"));
            row3.appendChild(ia37);
            Element ia38 = doc2.createElement("IA38");
            ia38.appendChild(doc2.createTextNode("N"));
            row3.appendChild(ia38);
            Element ia39 = doc2.createElement("IA39");
            row3.appendChild(ia39);
            Element ia40 = doc2.createElement("IA40");
            row3.appendChild(ia40);
            Element ia41 = doc2.createElement("IA41");
            ia41.appendChild(doc2.createTextNode("N"));
            row3.appendChild(ia41);
            Element ia61 = doc2.createElement("IA61");
            ia61.appendChild(doc2.createTextNode("002"));
            row3.appendChild(ia61);
            Element ia62 = doc2.createElement("IA62");
            ia62.appendChild(doc2.createTextNode("CO_ORANGE"));
            row3.appendChild(ia62);
            Element ia64 = doc2.createElement("IA64");
            ia64.appendChild(doc2.createTextNode("FINTECH"));
            row3.appendChild(ia64);
            Element ia65 = doc2.createElement("IA65");
            ia65.appendChild(doc2.createTextNode("ORANGE CO LENDING"));
            row3.appendChild(ia65);
            Element ia66 = doc2.createElement("IA66");
            ia66.appendChild(doc2.createTextNode("Y"));
            row3.appendChild(ia66);
            Element ia67 = doc2.createElement("IA67");
            row3.appendChild(ia67);
            Element ia68 = doc2.createElement("IA68");
            row3.appendChild(ia68);
            Element ia69 = doc2.createElement("IA69");
            row3.appendChild(ia69);


            TransformerFactory transformerFactory3 = TransformerFactory.newInstance();
            Transformer transformer3 = transformerFactory3.newTransformer();
            transformer3.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source3 = new DOMSource(doc2);


            // Output to console for testing
            StreamResult consoleResult3 = new StreamResult(System.out);
            transformer3.transform(source3, consoleResult3);

            DocumentBuilderFactory dbFactory4 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder4 = dbFactory4.newDocumentBuilder();
            Document doc3 = dBuilder4.newDocument();
            Element rootElement3 = doc3.createElement("PRODUCT_DTLS");
            doc3.appendChild(rootElement3);
            Element rowset3 = doc3.createElement("ROWSET");
            rootElement3.appendChild(rowset3);
            Element row4 = doc3.createElement("ROW");
            rowset3.appendChild(row4);
            Element is1 = doc3.createElement("IS1");
            is1.appendChild(doc3.createTextNode("001"));
            row4.appendChild(is1);
            Element is2 = doc3.createElement("IS2");
            is2.appendChild(doc3.createTextNode(loan_no));
            row4.appendChild(is2);
            Element is3 = doc3.createElement("IS3");
            is3.appendChild(doc3.createTextNode("1"));
            row4.appendChild(is3);
            Element is4 = doc3.createElement("IS4");
            is4.appendChild(doc3.createTextNode("1"));
            row4.appendChild(is4);
            Element is5 = doc3.createElement("IS5");
            is5.appendChild(doc3.createTextNode("CO_ORANGE"));
            row4.appendChild(is5);
            Element is6 = doc3.createElement("IS6");
            row4.appendChild(is6);
            Element is7 = doc3.createElement("IS7");
            row4.appendChild(is7);
            Element is8 = doc3.createElement("IS8");
            is8.appendChild(doc3.createTextNode("F"));
            row4.appendChild(is8);
            Element is9 = doc3.createElement("IS9");
            is9.appendChild(doc3.createTextNode("0"));
            row4.appendChild(is9);
            Element is10 = doc3.createElement("IS10");
            is10.appendChild(doc3.createTextNode("12"));
            row4.appendChild(is10);
            Element is11 = doc3.createElement("IS11");
            is11.appendChild(doc3.createTextNode("ORANGE"));
            row4.appendChild(is11);
            Element is12 = doc3.createElement("IS12");
            is12.appendChild(doc3.createTextNode("0"));
            row4.appendChild(is12);
            Element is13 = doc3.createElement("IS13");
            is13.appendChild(doc3.createTextNode("0"));
            row4.appendChild(is13);
            Element is14 = doc3.createElement("IS14");
            is14.appendChild(doc3.createTextNode("12"));
            row4.appendChild(is14);
            Element is16 = doc3.createElement("IS16");
            is16.appendChild(doc3.createTextNode("0"));
            row4.appendChild(is16);
            Element is17 = doc3.createElement("IS17");
            is17.appendChild(doc3.createTextNode("3"));
            row4.appendChild(is17);
            Element is18 = doc3.createElement("IS18");
            is18.appendChild(doc3.createTextNode("48"));
            row4.appendChild(is18);
            Element is19 = doc3.createElement("IS19");
            is19.appendChild(doc3.createTextNode("0"));
            row4.appendChild(is19);


           



            Element is20 = doc3.createElement("IS20");
            is20.appendChild(doc3.createTextNode(laonnamt));
            row4.appendChild(is20);
            Element is21 = doc3.createElement("IS21");
            is21.appendChild(doc3.createTextNode("10"));
            row4.appendChild(is21);
            Element is22 = doc3.createElement("IS22");
            row4.appendChild(is22);
            Element is23 = doc3.createElement("IS23");
            row4.appendChild(is23);
            Element is24 = doc3.createElement("IS24");
            row4.appendChild(is24);
            Element is28 = doc3.createElement("IS28");
            row4.appendChild(is28);

            TransformerFactory transformerFactory4 = TransformerFactory.newInstance();
            Transformer transformer4 = transformerFactory4.newTransformer();
            transformer4.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source4 = new DOMSource(doc3);


            // Output to console for testing
            StreamResult consoleResult4 = new StreamResult(System.out);
            transformer4.transform(source4, consoleResult4);


            DocumentBuilderFactory dbFactory5 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder5 = dbFactory5.newDocumentBuilder();
            Document doc4 = dBuilder5.newDocument();
            Element rootElement4 = doc4.createElement("LOAN_REF");
            doc4.appendChild(rootElement4);
            Element rowset4 = doc4.createElement("ROWSET");
            rootElement4.appendChild(rowset4);
            Element row5 = doc4.createElement("ROW");
            rowset4.appendChild(row5);

            Element ib1 = doc4.createElement("IB1");
            ib1.appendChild(doc4.createTextNode("001"));
            row5.appendChild(ib1);
            Element ib2 = doc4.createElement("IB2");
            ib2.appendChild(doc4.createTextNode(loan_no));
            row5.appendChild(ib2);
            Element ib3 = doc4.createElement("IB3");
            ib3.appendChild(doc4.createTextNode(custId));
            row5.appendChild(ib3);
            Element ib4 = doc4.createElement("IB4");
            ib4.appendChild(doc4.createTextNode("B"));
            row5.appendChild(ib4);
            Element ib5 = doc4.createElement("IB5");
            row5.appendChild(ib5);
            Element ib6 = doc4.createElement("IB6");
            ib6.appendChild(doc4.createTextNode("ORANGE"));
            row5.appendChild(ib6);
            Element ib7 = doc4.createElement("IB7");
            ib7.appendChild(doc4.createTextNode("N"));
            row5.appendChild(ib7);

            TransformerFactory transformerFactory5 = TransformerFactory.newInstance();
            Transformer transformer5 = transformerFactory5.newTransformer();
            transformer5.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source5 = new DOMSource(doc4);


            // Output to console for testing
            StreamResult consoleResult5 = new StreamResult(System.out);
            transformer5.transform(source5, consoleResult5);

            DocumentBuilderFactory dbFactory7 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder7 = dbFactory7.newDocumentBuilder();
            Document doc7 = dBuilder7.newDocument();
            Element rootElement7 = doc7.createElement("DISB_DTL");
            doc7.appendChild(rootElement7);
            Element rowset7 = doc7.createElement("ROWSET");
            rootElement7.appendChild(rowset7);
            Element row7 = doc7.createElement("ROW");
            rowset7.appendChild(row7);

            Element ic1 = doc7.createElement("IC1");
            ic1.appendChild(doc7.createTextNode("001"));
            row7.appendChild(ic1);
            Element ic2 = doc7.createElement("IC2");
            ic2.appendChild(doc7.createTextNode(loan_no));
            row7.appendChild(ic2);
            Element ic3 = doc7.createElement("IC3");
            ic3.appendChild(doc7.createTextNode("1"));
            row7.appendChild(ic3);





            Element ic4 = doc7.createElement("IC4");
            ic4.appendChild(doc7.createTextNode(agmnt_date));
            row7.appendChild(ic4);
            Element ic5 = doc7.createElement("IC5");
            row7.appendChild(ic5);
            Element ic6 = doc7.createElement("IC6");
            ic6.appendChild(doc7.createTextNode("ORANGE"));
            row7.appendChild(ic6);
            Element ic7 = doc7.createElement("IC7");
            ic7.appendChild(doc7.createTextNode("Y"));
            row7.appendChild(ic7);

            String emi_date = sdf.format(loan.getFirstDueDate());
            Element ic8 = doc7.createElement("IC8");
            ic8.appendChild(doc7.createTextNode(emi_date));
            row7.appendChild(ic8);
            Element ic10 = doc7.createElement("IC10");
            ic10.appendChild(doc7.createTextNode("1"));
            row7.appendChild(ic10);
            Element ic21 = doc7.createElement("IC21");
            ic21.appendChild(doc7.createTextNode("NEFT"));
            row7.appendChild(ic21);
            Element ic22 = doc7.createElement("IC22");
            row7.appendChild(ic22);

            Element ic23 = doc7.createElement("IC23");
            row7.appendChild(ic23);
            Element ic24 = doc7.createElement("IC24");
            row7.appendChild(ic24);

            TransformerFactory transformerFactory7 = TransformerFactory.newInstance();
            Transformer transformer7 = transformerFactory7.newTransformer();
            transformer7.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source7 = new DOMSource(doc7);

            // Output to console for testing
            StreamResult consoleresult7 = new StreamResult(System.out);
            transformer7.transform(source7, consoleresult7);

            DocumentBuilderFactory dbFactory8 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder8 = dbFactory8.newDocumentBuilder();
            Document doc8 = dBuilder8.newDocument();
            Element rootElement8 = doc8.createElement("TRANCHE_DTL");
            doc8.appendChild(rootElement8);
            Element rowset8 = doc8.createElement("ROWSET");
            rootElement8.appendChild(rowset8);
            Element row8 = doc8.createElement("ROW");
            rowset8.appendChild(row8);

            Element id1 = doc8.createElement("ID1");
            id1.appendChild(doc8.createTextNode("001"));
            row8.appendChild(id1);
            Element id2 = doc8.createElement("ID2");
            id2.appendChild(doc8.createTextNode(loan_no));
            row8.appendChild(id2);
            Element id3 = doc8.createElement("ID3");
            id3.appendChild(doc8.createTextNode("1"));
            row8.appendChild(id3);
            Element id4 = doc8.createElement("ID4");
            row8.appendChild(id4);
            Element id5 = doc8.createElement("ID5");
            row8.appendChild(id5);

            
            Element id6 = doc8.createElement("ID6");
            id6.appendChild(doc8.createTextNode(tenr));
            row8.appendChild(id6);
            Element id7 = doc8.createElement("ID7");
            id7.appendChild(doc8.createTextNode("L"));
            row8.appendChild(id7);
            Element id8 = doc8.createElement("ID8");
            id8.appendChild(doc8.createTextNode(laonnamt));
            row8.appendChild(id8);
            Element id9 = doc8.createElement("ID9");
            id9.appendChild(doc8.createTextNode("ORANGE"));
            row8.appendChild(id9);
            Element id10 = doc8.createElement("ID10");
            id10.appendChild(doc8.createTextNode(emi_date));
            row8.appendChild(id10);
            Element id11 = doc8.createElement("ID11");
            id11.appendChild(doc8.createTextNode(laonnamt));
            row8.appendChild(id11);
            if(advanceemi>=1) {
            Element id12 = doc8.createElement("ID12");
            id12.appendChild(doc8.createTextNode(amtemi1));
            row8.appendChild(id12);
            }else {
            	Element id12 = doc8.createElement("ID12");
                id12.appendChild(doc8.createTextNode("0"));
                row8.appendChild(id12);
            }
            
            if(advanceemi>=1) {
            Element id13 = doc8.createElement("ID13");
            id13.appendChild(doc8.createTextNode(ademi));
            row8.appendChild(id13);
            }else {
            	Element id13 = doc8.createElement("ID13");
                id13.appendChild(doc8.createTextNode("0"));
                row8.appendChild(id13);
            }
            
        
            Element id14 = doc8.createElement("ID14");
            id14.appendChild(doc8.createTextNode("1"));
            row8.appendChild(id14);
            Element id17 = doc8.createElement("ID17");
            id17.appendChild(doc8.createTextNode("0"));
            row8.appendChild(id17);
            Element id18 = doc8.createElement("ID18");
            id18.appendChild(doc8.createTextNode("0"));
            row8.appendChild(id18);
            Element id19 = doc8.createElement("ID19");
            id19.appendChild(doc8.createTextNode("0"));
            row8.appendChild(id19);
            Element id20 = doc8.createElement("ID20");
            id20.appendChild(doc8.createTextNode("N"));
            row8.appendChild(id20);
            Element id21 = doc8.createElement("ID21");
            row8.appendChild(id21);
            Element id22 = doc8.createElement("ID22");
            row8.appendChild(id22);

            Element id23 = doc8.createElement("ID23");
            row8.appendChild(id23);
            Element id24 = doc8.createElement("ID24");
            row8.appendChild(id24);
            
            if(advanceemi>=1)
            {
            	Element id25 = doc8.createElement("ID25");
            	id25.appendChild(doc8.createTextNode("Y"));
            row8.appendChild(id25);
            }else
            {
            Element id25 = doc8.createElement("ID25");
            row8.appendChild(id25);
            }
            
            Element id41 = doc8.createElement("ID41");
            id41.appendChild(doc8.createTextNode("0"));
            row8.appendChild(id41);
            Element id42 = doc8.createElement("ID42");
            row8.appendChild(id42);

            Element id43 = doc8.createElement("ID43");
            row8.appendChild(id43);
            Element id44 = doc8.createElement("ID44");
            row8.appendChild(id44);
            Element id45 = doc8.createElement("ID45");
            row8.appendChild(id45);
            Element id46 = doc8.createElement("ID46");
            row8.appendChild(id46);
            Element id47 = doc8.createElement("ID47");
            row8.appendChild(id47);
            Element id48 = doc8.createElement("ID48");
            row8.appendChild(id48);

            Element id49 = doc8.createElement("ID49");
            row8.appendChild(id49);



            TransformerFactory transformerFactory8 = TransformerFactory.newInstance();
            Transformer transformer8 = transformerFactory8.newTransformer();
            transformer8.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source8 = new DOMSource(doc8);

            // Output to console for testing
            StreamResult consoleresult8 = new StreamResult(System.out);
            transformer8.transform(source8, consoleresult8);


            DocumentBuilderFactory dbFactory9 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder9 = dbFactory9.newDocumentBuilder();
            Document doc9 = dBuilder9.newDocument();
            Element rootElement9 = doc9.createElement("TRANCHE_DISB_REF");
            doc9.appendChild(rootElement9);
            Element rowset9 = doc9.createElement("ROWSET");
            rootElement9.appendChild(rowset9);
            Element row9 = doc9.createElement("ROW");
            rowset9.appendChild(row9);

            Element if1 = doc9.createElement("IF1");
            if1.appendChild(doc9.createTextNode("001"));
            row9.appendChild(if1);
            Element if2 = doc9.createElement("IF2");
            if2.appendChild(doc9.createTextNode(loan_no));
            row9.appendChild(if2);
            Element if3 = doc9.createElement("IF3");
            if3.appendChild(doc9.createTextNode("1"));
            row9.appendChild(if3);
            Element if4 = doc9.createElement("IF4");
            if4.appendChild(doc9.createTextNode("1"));
            row9.appendChild(if4);
            Element if5 = doc9.createElement("IF5");
            if5.appendChild(doc9.createTextNode(laonnamt));
            row9.appendChild(if5);
            Element if6 = doc9.createElement("IF6");
            if6.appendChild(doc9.createTextNode("ORANGE"));
            row9.appendChild(if6);
            TransformerFactory transformerFactory9 = TransformerFactory.newInstance();
            Transformer transformer9 = transformerFactory9.newTransformer();
            transformer9.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source9 = new DOMSource(doc9);

            // Output to console for testing
            StreamResult consoleresult9 = new StreamResult(System.out);
            transformer9.transform(source9, consoleresult9);

            DocumentBuilderFactory dbFactory10 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder10 = dbFactory10.newDocumentBuilder();
            Document doc10 = dBuilder10.newDocument();
            Element rootElement10 = doc10.createElement("TRANCHE_SCH_PARAM");
            doc10.appendChild(rootElement10);
            Element rowset10 = doc10.createElement("ROWSET");
            rootElement10.appendChild(rowset10);
            Element row10 = doc10.createElement("ROW");
            rowset10.appendChild(row10);

            Element ie1 = doc10.createElement("IE1");
            ie1.appendChild(doc10.createTextNode("001"));
            row10.appendChild(ie1);
            Element ie2 = doc10.createElement("IE2");
            ie2.appendChild(doc10.createTextNode(loan_no));
            row10.appendChild(ie2);
            Element ie3 = doc10.createElement("IE3");
            ie3.appendChild(doc10.createTextNode("1"));
            row10.appendChild(ie3);
            Element ie4 = doc10.createElement("IE4");
            ie4.appendChild(doc10.createTextNode("1"));
            row10.appendChild(ie4);
            Element ie5 = doc10.createElement("IE5");
            ie5.appendChild(doc10.createTextNode("1"));
            row10.appendChild(ie5);
            Element ie6 = doc10.createElement("IE6");
            ie6.appendChild(doc10.createTextNode(tenr));
            row10.appendChild(ie6);


            
                    
                        

           
            Element ie7 = doc10.createElement("IE7");
            ie7.appendChild(doc10.createTextNode(amtemi1));
            row10.appendChild(ie7);
            Element ie8 = doc10.createElement("IE8");
            ie8.appendChild(doc10.createTextNode("14"));
            row10.appendChild(ie8);
            Element ie9 = doc10.createElement("IE9");
            row10.appendChild(ie9);
            Element ie10 = doc10.createElement("IE10");
            row10.appendChild(ie10);
            Element ie11 = doc10.createElement("IE11");
            row10.appendChild(ie11);
            Element ie12 = doc10.createElement("IE12");
            row10.appendChild(ie12);
            Element ie13 = doc10.createElement("IE13");
            row10.appendChild(ie13);
            Element ie14 = doc10.createElement("IE14");
            row10.appendChild(ie14);
            Element ie15 = doc10.createElement("IE15");
            row10.appendChild(ie15);
            Element ie16 = doc10.createElement("IE16");
            row10.appendChild(ie16);
            Element ie17 = doc10.createElement("IE17");
            row10.appendChild(ie17);
            Element ie18 = doc10.createElement("IE18");
            ie18.appendChild(doc10.createTextNode("ORANGE"));
            row10.appendChild(ie18);
            Element ie19 = doc10.createElement("IE19");
            ie19.appendChild(doc10.createTextNode("F"));
            row10.appendChild(ie19);
            Element ie25 = doc10.createElement("IE25");
            row10.appendChild(ie25);
            TransformerFactory transformerFactory10 = TransformerFactory.newInstance();
            Transformer transformer10 = transformerFactory10.newTransformer();
            transformer10.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source10 = new DOMSource(doc10);


            // Output to console for testing
            StreamResult consoleresult10 = new StreamResult(System.out);
            transformer10.transform(source10, consoleresult10);

            DocumentBuilderFactory dbFactory101 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder101 = dbFactory101.newDocumentBuilder();
            Document doc101 = dBuilder101.newDocument();
            Element rootElement101 = doc101.createElement("ASSET_DTL");
            doc101.appendChild(rootElement101);
            Element rowset101 = doc101.createElement("ROWSET");
            rootElement101.appendChild(rowset101);
            Element row101 = doc101.createElement("ROW");
            rowset101.appendChild(row101);

            Element ig1 = doc101.createElement("IG1");
            ig1.appendChild(doc101.createTextNode("001"));
            row101.appendChild(ig1);
            Element ig2 = doc101.createElement("IG2");
            ig2.appendChild(doc101.createTextNode(loan_no));
            row101.appendChild(ig2);

            String make = asset.getMake();
            Element ig3 = doc101.createElement("IG3");
            ig3.appendChild(doc101.createTextNode("1"));
            row101.appendChild(ig3);
            Element ig5 = doc101.createElement("IG5");
            ig5.appendChild(doc101.createTextNode("1"));
            row101.appendChild(ig5);

            int onRoadPr = Math.round(asset.getOnroadPrice());
            String onRoadPrice = Integer.toString(onRoadPr);
            Element ig7 = doc101.createElement("IG7");
            ig7.appendChild(doc101.createTextNode(onRoadPrice));
            row101.appendChild(ig7);
            Element ig8 = doc101.createElement("IG8");
            ig8.appendChild(doc101.createTextNode("CO_ORANGE"));
            row101.appendChild(ig8);
            Element ig9 = doc101.createElement("IG9");
            ig9.appendChild(doc101.createTextNode(make));
            row101.appendChild(ig9);
            Element ig10 = doc101.createElement("IG10");
            row101.appendChild(ig10);
            Element ig11 = doc101.createElement("IG11");
            row101.appendChild(ig11);
            Element ig12 = doc101.createElement("IG12");
            ig12.appendChild(doc101.createTextNode("0"));
            row101.appendChild(ig12);
            Element ig13 = doc101.createElement("IG13");
            ig13.appendChild(doc101.createTextNode("0"));
            row101.appendChild(ig13);


            Element ig14 = doc101.createElement("IG14");
            ig14.appendChild(doc101.createTextNode(agmnt_date));
            row101.appendChild(ig14);
            Element ig15 = doc101.createElement("IG15");
            ig15.appendChild(doc101.createTextNode("0"));
            row101.appendChild(ig15);
          
                      Element ig16 = doc101.createElement("IG16");
           
            row101.appendChild(ig16);
            
           
            Element ig17 = doc101.createElement("IG17");
                       row101.appendChild(ig17);
            Element ig18 = doc101.createElement("IG18");
            ig18.appendChild(doc101.createTextNode("0"));
            row101.appendChild(ig18);
            Element ig19 = doc101.createElement("IG19");
            ig19.appendChild(doc101.createTextNode("0"));
            row101.appendChild(ig19);
            Element ig20 = doc101.createElement("IG20");
            ig20.appendChild(doc101.createTextNode("N"));
            row101.appendChild(ig20);
            Element ig21 = doc101.createElement("IG21");
            row101.appendChild(ig21);
            Element ig22 = doc101.createElement("IG22");
            ig22.appendChild(doc101.createTextNode("N"));
            row101.appendChild(ig22);
            Element ig23 = doc101.createElement("IG23");
            row101.appendChild(ig23);
            Element ig24 = doc101.createElement("IG24");
            row101.appendChild(ig24);
            Element ig25 = doc101.createElement("IG25");
            ig25.appendChild(doc101.createTextNode("AUTO"));
            row101.appendChild(ig25);
            Element ig26 = doc101.createElement("IG26");
            row101.appendChild(ig26);
            Element ig27 = doc101.createElement("IG27");
            ig27.appendChild(doc101.createTextNode("AUTO"));
            row101.appendChild(ig27);
            Element ig28 = doc101.createElement("IG28");
            ig28.appendChild(doc101.createTextNode("TWO WHEELER"));
            row101.appendChild(ig28);

            Element ig30 = doc101.createElement("IG30");
            row101.appendChild(ig30);
            Element ig31 = doc101.createElement("IG31");
            row101.appendChild(ig31);
            Element ig32 = doc101.createElement("IG32");
            row101.appendChild(ig32);
            Element ig33 = doc101.createElement("IG33");
            row101.appendChild(ig33);
            Element ig34 = doc101.createElement("IG34");
            row101.appendChild(ig34);
            Element ig35 = doc101.createElement("IG35");
            row101.appendChild(ig35);
            Element ig36 = doc101.createElement("IG36");
            row101.appendChild(ig36);
            Element ig37 = doc101.createElement("IG37");
            row101.appendChild(ig37);
            Element ig38 = doc101.createElement("IG38");
            row101.appendChild(ig38);
            Element ig39 = doc101.createElement("IG39");
            row101.appendChild(ig39);
            Element ig40 = doc101.createElement("IG40");
            row101.appendChild(ig40);
            Element ig41 = doc101.createElement("IG41");
            row101.appendChild(ig41);
            Element ig42 = doc101.createElement("IG42");
            row101.appendChild(ig42);
            Element ig43 = doc101.createElement("IG43");
            row101.appendChild(ig43);
            Element ig44 = doc101.createElement("IG44");
            row101.appendChild(ig44);
            Element ig45 = doc101.createElement("IG45");
            row101.appendChild(ig45);
            Element ig46 = doc101.createElement("IG46");
            row101.appendChild(ig46);

            Element ig47 = doc101.createElement("IG47");
            row101.appendChild(ig47);

            Element ig48 = doc101.createElement("IG48");
            row101.appendChild(ig48);
            Element ig49 = doc101.createElement("IG49");
            row101.appendChild(ig49);
            Element ig50 = doc101.createElement("IG50");
            row101.appendChild(ig50);
            Element ig51 = doc101.createElement("IG51");
            row101.appendChild(ig51);
            Element ig52 = doc101.createElement("IG52");
            row101.appendChild(ig52);
            Element ig53 = doc101.createElement("IG53");
            row101.appendChild(ig53);
            Element ig54 = doc101.createElement("IG54");
            ig54.appendChild(doc101.createTextNode("TAMILNADU"));
            row101.appendChild(ig54);

            Element ig55 = doc101.createElement("IG55");
            ig55.appendChild(doc101.createTextNode(branch));
            row101.appendChild(ig55);
            Element ig56 = doc101.createElement("IG56");
            row101.appendChild(ig56);
            Element ig57 = doc101.createElement("IG57");
            row101.appendChild(ig57);
            Element ig58 = doc101.createElement("IG58");
            row101.appendChild(ig58);
            Element ig59 = doc101.createElement("IG59");
            row101.appendChild(ig59);
            Element ig60 = doc101.createElement("IG60");
            row101.appendChild(ig60);
            Element ig61 = doc101.createElement("IG61");

            row101.appendChild(ig61);
            Element ig62 = doc101.createElement("IG62");

            row101.appendChild(ig62);
            Element ig63 = doc101.createElement("IG63");
            row101.appendChild(ig63);
            Element ig64 = doc101.createElement("IG64");
            row101.appendChild(ig64);
            Element ig65 = doc101.createElement("IG65");
            row101.appendChild(ig65);
            Element ig66 = doc101.createElement("IG66");
            row101.appendChild(ig66);
            Element ig67 = doc101.createElement("IG67");
            row101.appendChild(ig67);



            TransformerFactory transformerFactory101 = TransformerFactory.newInstance();
            Transformer transformer101 = transformerFactory101.newTransformer();
            transformer101.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source101 = new DOMSource(doc101);


            // Output to console for testing
            StreamResult consoleresult101 = new StreamResult(System.out);
            transformer101.transform(source101, consoleresult101);

            DocumentBuilderFactory dbFactory102 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder102 = dbFactory102.newDocumentBuilder();
            Document doc102 = dBuilder102.newDocument();
            Element rootElement102 = doc102.createElement("LOAN_MODIFIER");
            doc102.appendChild(rootElement102);


            TransformerFactory transformerFactory102 = TransformerFactory.newInstance();
            Transformer transformer102 = transformerFactory102.newTransformer();
            transformer102.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source102 = new DOMSource(doc102);


            // Output to console for testing
            StreamResult consoleresult102 = new StreamResult(System.out);
            transformer102.transform(source102, consoleresult102);
            DocumentBuilderFactory dbFactory103 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder103 = dbFactory103.newDocumentBuilder();
            Document doc103 = dBuilder103.newDocument();
            Element rootElement103 = doc103.createElement("LOAN_STATIC_FEE");
            doc103.appendChild(rootElement103);


            TransformerFactory transformerFactory103 = TransformerFactory.newInstance();
            Transformer transformer103 = transformerFactory103.newTransformer();
            transformer103.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source103 = new DOMSource(doc103);


            // Output to console for testing
            StreamResult consoleresult103 = new StreamResult(System.out);
            transformer103.transform(source103, consoleresult103);
            DocumentBuilderFactory dbFactory104 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder104 = dbFactory104.newDocumentBuilder();
            Document doc104 = dBuilder104.newDocument();
            Element rootElement104 = doc104.createElement("LOAN_RANGE_FEE");
            doc104.appendChild(rootElement104);


            TransformerFactory transformerFactory104 = TransformerFactory.newInstance();
            Transformer transformer104 = transformerFactory104.newTransformer();
            transformer104.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source104 = new DOMSource(doc104);


            // Output to console for testing
            StreamResult consoleresult104 = new StreamResult(System.out);
            transformer104.transform(source104, consoleresult104);
            DocumentBuilderFactory dbFactory105 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder105 = dbFactory105.newDocumentBuilder();
            Document doc105 = dBuilder105.newDocument();
            Element rootElement105 = doc105.createElement("LMS_SUBPRODUCT");
            doc105.appendChild(rootElement105);


            TransformerFactory transformerFactory105 = TransformerFactory.newInstance();
            Transformer transformer105 = transformerFactory105.newTransformer();
            transformer105.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source105 = new DOMSource(doc105);


            // Output to console for testing
            StreamResult consoleresult105 = new StreamResult(System.out);
            transformer105.transform(source105, consoleresult105);
            
          if(advanceemi>=1)  
          {
            DocumentBuilderFactory dbFactory106 =
                DocumentBuilderFactory.newInstance();
                    
            DocumentBuilder dBuilder106 = dbFactory106.newDocumentBuilder();
            Document doc106 = dBuilder106.newDocument();
            
            Element rootElement106 = doc106.createElement("STEP_PAY");
            doc106.appendChild(rootElement106);

            Element rowset106 = doc106.createElement("ROWSET");
            rootElement106.appendChild(rowset106);

            Element row106 = doc106.createElement("ROW");
            rowset106.appendChild(row106); 
           
            
            Element iv1 = doc106.createElement("IV1");
            iv1.appendChild(doc106.createTextNode("001"));
            row106.appendChild(iv1);
            Element iv2 = doc106.createElement("IV2");
            iv2.appendChild(doc106.createTextNode(loan_no));
            row106.appendChild(iv2);
            Element iv3 = doc106.createElement("IV3");
            iv3.appendChild(doc106.createTextNode("1"));
            row106.appendChild(iv3);
            Element iv4 = doc106.createElement("IV4");
            iv4.appendChild(doc106.createTextNode("1"));
            row106.appendChild(iv4);
            
            
            String endmon = Integer.toString(endMonth);
            Element iv5 = doc106.createElement("IV5");
            iv5.appendChild(doc106.createTextNode(endmon));
            row106.appendChild(iv5);
            Element iv6 = doc106.createElement("IV6");
            iv6.appendChild(doc106.createTextNode("0"));
            row106.appendChild(iv6);
            Element iv7 = doc106.createElement("IV7");
            iv7.appendChild(doc106.createTextNode("0"));
            row106.appendChild(iv7);
            Element iv8 = doc106.createElement("IV8");
            iv8.appendChild(doc106.createTextNode("0"));
            row106.appendChild(iv8);
            Element iv9 = doc106.createElement("IV9");
            iv9.appendChild(doc106.createTextNode("2"));
            row106.appendChild(iv9);
            Element iv10 = doc106.createElement("IV10");
            iv10.appendChild(doc106.createTextNode(amtemi1));
            row106.appendChild(iv10);
            Element iv11 = doc106.createElement("IV11");
            iv11.appendChild(doc106.createTextNode("1"));
            row106.appendChild(iv11);
            
            TransformerFactory transformerFactory106 = TransformerFactory.newInstance();
            Transformer transformer106 = transformerFactory106.newTransformer();
            transformer106.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source106 = new DOMSource(doc106);


            // Output to console for testing
            StreamResult consoleresult106 = new StreamResult(System.out);
            transformer106.transform(source106, consoleresult106);
            
          }
          else
          {
        	  DocumentBuilderFactory dbFactory206 =
                      DocumentBuilderFactory.newInstance();
                  DocumentBuilder dBuilder206 = dbFactory206.newDocumentBuilder();
                  Document doc206 = dBuilder206.newDocument();
                  Element rootElement206 = doc206.createElement("STEP_PAY");
                  doc206.appendChild(rootElement206);


                  TransformerFactory transformerFactory206 = TransformerFactory.newInstance();
                  Transformer transformer206 = transformerFactory206.newTransformer();
                  transformer206.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                  DOMSource source206 = new DOMSource(doc206);
                  
               // Output to console for testing
                  StreamResult consoleresult206 = new StreamResult(System.out);
                  transformer206.transform(source206, consoleresult206);
          }
            DocumentBuilderFactory dbFactory107 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder107 = dbFactory107.newDocumentBuilder();
            Document doc107 = dBuilder107.newDocument();
            Element rootElement107 = doc107.createElement("SKIP_EMI");
            doc107.appendChild(rootElement107);


            TransformerFactory transformerFactory107 = TransformerFactory.newInstance();
            Transformer transformer107 = transformerFactory107.newTransformer();
            transformer107.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source107 = new DOMSource(doc107);


            // Output to console for testing
            StreamResult consoleresult107 = new StreamResult(System.out);
            transformer107.transform(source107, consoleresult107);
            DocumentBuilderFactory dbFactory108 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder108 = dbFactory108.newDocumentBuilder();
            Document doc108 = dBuilder108.newDocument();
            Element rootElement108 = doc108.createElement("BALLON_PAYMENT");
            doc108.appendChild(rootElement108);


            TransformerFactory transformerFactory108 = TransformerFactory.newInstance();
            Transformer transformer108 = transformerFactory108.newTransformer();
            transformer108.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source108 = new DOMSource(doc108);


            // Output to console for testing
            StreamResult consoleresult108 = new StreamResult(System.out);
            transformer108.transform(source108, consoleresult108);
            DocumentBuilderFactory dbFactory109 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder109 = dbFactory109.newDocumentBuilder();
            Document doc109 = dBuilder109.newDocument();
            Element rootElement109 = doc109.createElement("PDC_NORMAL");
            doc109.appendChild(rootElement109);


            TransformerFactory transformerFactory109 = TransformerFactory.newInstance();
            Transformer transformer109 = transformerFactory109.newTransformer();
            transformer109.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source109 = new DOMSource(doc109);


            // Output to console for testing
            StreamResult consoleresult109 = new StreamResult(System.out);
            transformer109.transform(source109, consoleresult109);
            DocumentBuilderFactory dbFactory110 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder110 = dbFactory110.newDocumentBuilder();
            Document doc110 = dBuilder110.newDocument();
            Element rootElement110 = doc110.createElement("PDC_SECURITY");
            doc110.appendChild(rootElement110);


            TransformerFactory transformerFactory110 = TransformerFactory.newInstance();
            Transformer transformer110 = transformerFactory110.newTransformer();
            transformer110.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source110 = new DOMSource(doc110);


            // Output to console for testing
            StreamResult consoleresult110 = new StreamResult(System.out);
            transformer110.transform(source110, consoleresult110);
            DocumentBuilderFactory dbFactory111 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder111 = dbFactory111.newDocumentBuilder();
            Document doc111 = dBuilder111.newDocument();
            Element rootElement111 = doc111.createElement("INTERNAL_AMORT");
            doc111.appendChild(rootElement111);


            TransformerFactory transformerFactory111 = TransformerFactory.newInstance();
            Transformer transformer111 = transformerFactory111.newTransformer();
            transformer111.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source111 = new DOMSource(doc111);


            // Output to console for testing
            StreamResult consoleresult111 = new StreamResult(System.out);
            transformer111.transform(source111, consoleresult111);
            DocumentBuilderFactory dbFactory112 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder112 = dbFactory112.newDocumentBuilder();
            Document doc112 = dBuilder112.newDocument();
            Element rootElement112 = doc112.createElement("ECS_DETAILS");
            doc112.appendChild(rootElement112);


            TransformerFactory transformerFactory112 = TransformerFactory.newInstance();
            Transformer transformer112 = transformerFactory112.newTransformer();
            transformer112.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source112 = new DOMSource(doc112);


            // Output to console for testing
            StreamResult consoleresult112 = new StreamResult(System.out);
            transformer112.transform(source112, consoleresult112);
            DocumentBuilderFactory dbFactory113 =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder113 = dbFactory113.newDocumentBuilder();
            Document doc113 = dBuilder113.newDocument();
            Element rootElement113 = doc113.createElement("IRE_REF");
            doc113.appendChild(rootElement113);


            TransformerFactory transformerFactory113 = TransformerFactory.newInstance();
            Transformer transformer113 = transformerFactory113.newTransformer();
            transformer113.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            DOMSource source113 = new DOMSource(doc113);


            // Output to console for testing
            StreamResult consoleresult113 = new StreamResult(System.out);
            transformer113.transform(source113, consoleresult113);

            
            
          
            
            
            
               
        }

    }
     
}
    	
}