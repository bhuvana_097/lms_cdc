package com.vg.lms.service;

import java.util.List;

import org.springframework.security.core.Authentication;

import com.vg.lms.bean.SearchLmsUserBean;
import com.vg.lms.bean.UpdateLmsUserBean;
import com.vg.lms.model.UserMaster;
import com.vg.lms.request.AddLmsUserRequestDTO;
import com.vg.lms.request.SearchLmsUserRequest;
import com.vg.lms.request.SessionRequestDTO;
import com.vg.lms.request.UserPasswordChangeRequest;
import com.vg.lms.request.osclCorrectionRequestDTO;

public interface MasterService {
	public int addLmsUser(AddLmsUserRequestDTO addLmsUserRequestDTO);
	public List<UserMaster> lmsUserList();
	public int updateLmsUser(AddLmsUserRequestDTO addLmsUserRequestDTO);
	public boolean getUserAccess(String userRole, String module);
	public String getUserRole(Authentication authentication);
	public int osclCorrection(osclCorrectionRequestDTO osclCorrectionRequestDTO);
	public int UserPasswdChange(UserPasswordChangeRequest UserPasswordChangeRequest);
	//public int SessionIdUpdate(String sessionid,String user);
	
	//public int Logoff(String code);
	public UserMaster searchCode(SearchLmsUserRequest  searchLmsUserRequest);
	
	public SearchLmsUserBean lmsUserSrchList(String code);
//	public UpdateLmsUserBean lmsUserSrchList(String code);
	
	
}
