package com.vg.lms.service;

import java.util.Date;

import com.vg.lms.model.Loan;

public interface LMSService {
	
	public Date getFirstDueDate(Date agmntDate);
	public Loan computeReceivable(Loan loan);
	public Loan updateBounceCharges(Loan Loan);
	public float getForeClosureCharges(Loan loan);
	
	

}
