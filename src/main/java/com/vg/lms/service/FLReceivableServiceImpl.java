package com.vg.lms.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.vg.lms.model.Loan;
import com.vg.lms.model.Receivable;

@Service
public class FLReceivableServiceImpl implements ReceivableService{


	private final Logger log = LoggerFactory.getLogger(FLReceivableServiceImpl.class);


	//private static final int emiDate = 5;


	@Override
	public Loan computeReceivable(Loan loan, int emiMonth, int emiDate) {
		//java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		int loanPaymentTenure=(int)loan.getTenure();
		int adVanceEmi = loan.getAdvanceEmi();
		float irr = loan.getIrr();
		//int emi =  calcualteAdvanceEMI(irr, loanPaymentTenure, loan.getAmount(),adVanceEmi);
		int emi = (int)loan.getEmi();
		log.debug(" Loam EMi : " + loan.getEmi() + " loan ROI : "+loan.getIrr() + " emiMonth : "+emiMonth);

		Map<Integer, Receivable> receivables=new HashMap<>();

		float morotoriumInterest = 0.0f;
		if(emiMonth > 1) {
			Calendar agreementDate = Calendar.getInstance();
			agreementDate.setTime(loan.getAgmntDate());
			int morotoriumDays = (30 - agreementDate.get(Calendar.DATE))+5;
			morotoriumInterest = (loan.getAmount() * (irr/365) * morotoriumDays)/100;
		}


		for ( int paymentCount=1; paymentCount<=loanPaymentTenure; paymentCount++){			
			Receivable receivable=new Receivable();

			receivable.setDueNo(paymentCount);
			//receivable.setCustId(loan.getCustId());
			receivable.setCustomer(loan.getCustomer());
			receivable.setEmi(emi);
			//receivable.setLoanId(loanId);
			receivable.setTrycount(0);

			if(adVanceEmi < 1) {

				float principleRemaining = 0;

				
				if(paymentCount == 1) {
					//principleRemaining = loan.getAmount() + loan.getScCharge();
					principleRemaining = loan.getAmount();
				log.info("LoanAmoount" + principleRemaining );}

				else
					principleRemaining = receivables.get(paymentCount-1).getPrincipleRemaining();

				float interest=(principleRemaining*irr)/1200;
				float interestRounded = round(interest,2);
				receivable.setInterest(interestRounded);

				float principlePaid = round(emi - interestRounded,2);
				receivable.setPrinciple(principlePaid);

				receivable.setPrincipleRemaining(round((principleRemaining-principlePaid),2));
				receivable.setStatus("FUTURE");

				Calendar calendar=Calendar.getInstance();
				calendar.setTime(loan.getAgmntDate());
				calendar.add(Calendar.MONTH, (emiMonth-1));
				/*calendar.add(Calendar.DATE, -1);*/
				//String currentDate= sdf.format(calendar.getTime());
				//receivable.setCreateTime(new Date());
				//receivable.setModifyTime(new Date());


				calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), emiDate);
				calendar.add(Calendar.MONTH, (paymentCount-loan.getAdvanceEmi())); 
				//String rePaymentDate=sdf.format(calendar.getTime());
				receivable.setDueDate(calendar.getTime());	
				if(paymentCount == loanPaymentTenure) {
					receivable.setEmi(receivable.getEmi() + morotoriumInterest);
					receivable.setInterest(receivable.getInterest() + morotoriumInterest);
					if(receivable.getPrincipleRemaining() < 0) {
						receivable.setPrinciple(receivable.getPrinciple() + receivable.getPrincipleRemaining());
						receivable.setEmi(receivable.getEmi() + receivable.getPrincipleRemaining());
						receivable.setPrincipleRemaining(0);
					}
				}
			}else {
				receivable.setInterest(0);
				receivable.setPrinciple(round(emi,2));

				float principleRemaining = 0;
				if(paymentCount == 1)
					principleRemaining = loan.getAmount();
				else
					principleRemaining = receivables.get(paymentCount-1).getPrincipleRemaining();

				receivable.setPrincipleRemaining(principleRemaining - receivable.getPrinciple());
				receivable.setStatus("PAID");
				//Date txnDate = new Date();
				receivable.setDueDate(loan.getAgmntDate());
				receivable.setTxnDate(loan.getAgmntDate());
				receivable.setValueDate(loan.getAgmntDate());
				receivable.setPrinPaid(receivable.getPrinciple());
				adVanceEmi -= 1;
				/*if(adVanceEmi == 0) {
						irr = getUpdatedIrr(receivable.getPrincipleRemaining(), (loanPaymentTenure-loan.getAdvanceEmi()), irr, emi);
					}*/
			}
			receivable.setLoanNo(loan.getLoanNo());
			receivable.setCreateUser("LOS");
			receivable.setLoan(loan);
			receivables.put(paymentCount, receivable);
		}
		printLoanDetails(receivables);
		loan.setReceivable(new TreeSet<Receivable>());
		loan.getReceivable().addAll(receivables.values());
		return loan;
	}



	private void printLoanDetails(Map<Integer, Receivable> loanReceivable) {
		StringBuffer sb = new StringBuffer();
		for (Receivable rePay : loanReceivable.values()) {
			log.debug(" Mont : "+rePay.getDueNo()+" "+ " Principal Remaing : "+rePay.getPrincipleRemaining()+" Principle Paid : "+rePay.getPrinciple()+ " Interst Paid :  "+rePay.getInterest()+ " Repay Date : "+rePay.getDueDate());
		}
		log.debug("    ");
		log.debug("   ");
	}

	/**
	 * This function is used to calculate EMI for given irr, tenure and amount
	 * @param irr
	 * @param tenure
	 * @param amount
	 * @return
	 */
	/*private int calculateEMI(float irr, float tenure, float amount)
	{
		float irrPerMonth=irr/(12*100);
		double emi = ((amount*irrPerMonth*Math.pow(1+irrPerMonth,tenure))/(Math.pow(1+irrPerMonth,tenure)-1));
		 int roundedEMI = (int) Math.ceil(emi);
	     return roundedEMI;

	}*/

	public int calcualteAdvanceEMI(float irr, float tenure, float amount, int adavnceEmi) {
		irr=irr/(12*100);
		double emi = ( amount * Math.pow(1 + irr, tenure)) / (1 + irr * adavnceEmi) / ((Math.pow(1 + irr, tenure) - 1) / irr); 
		int roundedEMI = (int) Math.ceil(emi);
		return roundedEMI;

	}


	/***
	 * This method is used to round of a double to the mentioned number of decimals
	 * @param value
	 * @param places
	 * @return
	 */
	private float round(float value, int places) {
		if (places < 0) throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.floatValue();
	}	

}
