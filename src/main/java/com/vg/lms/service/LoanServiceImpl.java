package com.vg.lms.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vg.lms.UserModuleMasterDetails;
import com.vg.lms.bean.CovidMoratBean;
import com.vg.lms.bean.CrDrBean;
import com.vg.lms.bean.LoanBean;
import com.vg.lms.bean.LoanReceipt;
import com.vg.lms.bean.LoanWaiver;
import com.vg.lms.bean.SplWaiverBean;
import com.vg.lms.bean.WaiverSplitBean;
import com.vg.lms.dealer.ta.OsclClose;
import com.vg.lms.incred.init.ADDRESS;
import com.vg.lms.incred.init.InitRequest;
import com.vg.lms.incred.init.InitResponse;
import com.vg.lms.model.Application;
import com.vg.lms.model.Asset;
import com.vg.lms.model.Charges;
import com.vg.lms.model.CovidMorat;
import com.vg.lms.model.Customer;
import com.vg.lms.model.Disbursement;
import com.vg.lms.model.IncredAudit;
import com.vg.lms.model.LMSRegistryModel;
import com.vg.lms.model.LmsOdcDaily;
import com.vg.lms.model.LmsWaiverClose;
import com.vg.lms.model.Loan;
import com.vg.lms.model.Receipt;
import com.vg.lms.model.Receivable;
import com.vg.lms.model.SplWaiver;
import com.vg.lms.model.UserMaster;
import com.vg.lms.model.UserModuleMaster;
import com.vg.lms.model.Waiver;
import com.vg.lms.model.WriteOff;
import com.vg.lms.repository.ApplicationRepository;
import com.vg.lms.repository.AssetRepository;
import com.vg.lms.repository.BranchRepository;
import com.vg.lms.repository.ChargesRepository;
import com.vg.lms.repository.CovidMoratRepository;
import com.vg.lms.repository.CustomerRepository;
import com.vg.lms.repository.IncredRepository;
import com.vg.lms.repository.LMSRegistryRepository;
import com.vg.lms.repository.LmsOdcDailyRepository;
import com.vg.lms.repository.LmsReceivableRepository;
import com.vg.lms.repository.LmsWaiverCloseRepository;
import com.vg.lms.repository.LoanRepository;
import com.vg.lms.repository.ReceiptsRepository;
import com.vg.lms.repository.SplWaiverRepo;
import com.vg.lms.repository.UserMasterRepository;
import com.vg.lms.repository.WaiverRepository;
import com.vg.lms.repository.WriteOffRepository;
import com.vg.lms.request.GetLoanRequestDTO;
import com.vg.lms.request.LoanRequestDTO;
import com.vg.lms.response.LoanDTO;
import com.vg.lms.response.LoanResponseDTO;
import com.vg.lms.response.LoanWaiveAndCloseResponseDTO;
import com.vg.lms.response.OsclResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;




@Component
public class LoanServiceImpl implements LoanService {

	@Autowired
	private UserMasterRepository usermstRepo;
	
	@Autowired
	private ApplicationRepository applicationRepository;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private LoanRepository loanRepository;

	@Autowired
	private ReceivableService receivableService;

	@Autowired
	private WaiverRepository waiverRepository;	

	@Autowired
	private ReceiptsRepository receiptsRepository;

	@Autowired
	private ChargesRepository chargesRepository;

	@Autowired
	private  SplWaiverRepo splWaiverRepo;
	
	@Autowired
	private IncredRepository incredRepository;
	
	@Autowired
	private LmsReceivableRepository lmsReceivableRepository;
	
	@Autowired
	private LmsOdcDailyRepository lmsOdcDailyRepository;
	
	@Autowired
	private LmsWaiverCloseRepository lmsWaiverCloseRepository;
	
	@Autowired
	private LMSRegistryRepository lmsRegistryRepository;
	
	@Autowired
	private CovidMoratRepository covidMoratRepository;
	

	@Autowired
	private WriteOffRepository writeOffRepository;

	@Autowired
	private BranchRepository branchRepository;

	@Autowired
	private AssetRepository assetRepository; 
	
	private final Logger log = LoggerFactory.getLogger(LoanServiceImpl.class);

	@Override
	public LoanResponseDTO getLoan(GetLoanRequestDTO getLoanRequestDTO) {
		long startTimeInMillis = System.currentTimeMillis();
		log.info("Entering LoanServiceImpl  - getloan :"+getLoanRequestDTO);	

		Loan loan = loanRepository.findById(getLoanRequestDTO.getLoanId()).get();
		LoanDTO loanDTO = null;
		if( loan != null && loan.getId() > 0){
			ModelMapper modelMapper = new ModelMapper();
			loanDTO = modelMapper.map(loan, LoanDTO.class);
		}

		LoanResponseDTO response = new LoanResponseDTO();
		response.setLoan(loanDTO);
		response.setStatusMessage("SUCCESS");
		response.setStatusCode("1");
		long timeTaken=System.currentTimeMillis()-startTimeInMillis;
		log.info("Time Taken for getLoan is ==>"+timeTaken);
		return response;
	}

	@Override
	public LoanResponseDTO addLoan(LoanRequestDTO loanRequestDTO) throws ParseException {
		long startTimeInMillis = System.currentTimeMillis();
		log.info("Entering LoanServiceImpl-addLoan :"+loanRequestDTO);				
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		modelMapper.typeMap(LoanRequestDTO.class, Loan.class);
		//.addMappings(m -> m.map(src -> src.getGurantorId(), Loan::setGurantorId));
		Loan loan = modelMapper.map(loanRequestDTO, Loan.class);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

		loan.setCustomer(customerRepository.findById(loanRequestDTO.getCustId()).get());
		if(loanRequestDTO.getGurantorId()!= -1)
			loan.setGurantor(customerRepository.findById(loanRequestDTO.getGurantorId()).get());

		loan.setLoanNo("ORFLTW"+sdf.format(new Date())+loanRequestDTO.getCustId());

		loan.setFuturePrinciple(loanRequestDTO.getAmount());

		Asset asset = new Asset();
		asset.setAssetType(loanRequestDTO.getAssetType());
		asset.setSchemeType(loanRequestDTO.getSchemeType());
		asset.setProdSubcode(loanRequestDTO.getProdSubcode());
		asset.setTotalCost(loanRequestDTO.getTotalCost());
		asset.setGridPrice(loanRequestDTO.getGridPrice());
		asset.setOnroadPrice(loanRequestDTO.getOnroadPrice());
		asset.setActualLtv(loanRequestDTO.getActualLtv());

		asset.setMake(loanRequestDTO.getMake());
		asset.setModel(loanRequestDTO.getModel());
		asset.setVariant(loanRequestDTO.getVariant());
		asset.setCreateUser("LOS");
		//asset.setCreateTime(new Date());
		asset.setLoan(loan);

		/*//List<Asset> assets = new ArrayList<>();
		Set<Asset> assets = new HashSet<>();
		assets.add(asset);
		 */
		loan.setAsset(asset);


		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//Date agreementDate = sdf.parse(loanRequestDTO.getAgmntDate());
		loan.setAgmntDate(loanRequestDTO.getAgmntDate());		
		//Date firstDueDate = sdf.parse(loanRequestDTO.getFirstDueDate());
		//loan.setFirstDueDate(loanRequestDTO.getFirstDueDate());

		Calendar agreementDate = GregorianCalendar.getInstance();
		agreementDate.setTime(loanRequestDTO.getAgmntDate());

		int emiMonth = 1;
		if(agreementDate.get(Calendar.DATE) > 20)
			emiMonth = 2;

		int emiDate = 5;

		loan = receivableService.computeReceivable(loan,emiMonth,emiDate );	

		Calendar calendar=Calendar.getInstance();
		calendar.add(Calendar.MONTH, emiMonth);
		calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), emiDate);

		loan.setFirstDueDate(calendar.getTime());

		calendar.add(Calendar.MONTH, (int)loan.getTenure());

		loan.setLastDueDate(calendar.getTime());


		loan.setCreateUser("LOS");
		//loan.setCreateTime(new Date());
		loan = loanRepository.save(loan);
		LoanResponseDTO response = new LoanResponseDTO();
		LoanDTO loanDTO = modelMapper.map(loan, LoanDTO.class);
		response.setLoan(loanDTO);
		response.setStatusMessage("SUCCESS");
		response.setStatusCode("1");
		long timeTaken=System.currentTimeMillis()-startTimeInMillis;
		log.info("Time Taken for addLoan is ==>"+timeTaken);
		return response;
	}

	@Override
	public LoanBean getLoan(int loanId,String role,String uname) {
		long startTimeInMillis = System.currentTimeMillis();
		log.info("Entering LoanServiceImpl-getLoan :"+loanId);

		Loan loan = loanRepository.findById(loanId).get();
		Asset asset = loan.getAsset();
       String app = loan.getApplNo();
       int max =app.length()-2;
      
       String ParentLoan=loan.getApplNo().substring(7,max);
      
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

		LoanBean loanBean = null;
		System.out.println("USerLoggedTo check:" + uname);
		if( loan != null && loan.getId() > 0){
			loanBean = new LoanBean();
			loanBean.setUserName(uname);
			//log.info("TEST Loan:"+ uname);
			loanBean.setUserRole(role);			
			
			
			String userDept = "", userHierLevel = "";
			String[] temp = role.split("-");
			userDept = temp[0];
			userHierLevel = temp[1];
			log.info("Entering LoanServiceImpl-userDept :"+userDept + "userHierLevel : " + userHierLevel);
			
			loanBean.setAddWaiverEnabled("0");
			loanBean.setCancelEnabled("0");
			loanBean.setDisburseEnabled("0");
			loanBean.setUpdatePDDEnabled("0");
			loanBean.setAddSOAEnabled("0");
			if(loanBean.getUserName().equalsIgnoreCase("10039"))
			 {
				 
				loanBean.setWaiveCloseEnabled("4");
			 } 
			ModelAndView maView = new ModelAndView("LandingPage");
			System.out.println("Branch:"+loan.getH3());
			UserMaster usm=usermstRepo.findBycode(uname);
			System.out.println("user Branch:"+usm.getBranch());
			
			
		//	if(!uname.equals("Akshata")) {
			for (UserModuleMaster userModuleMaster : UserModuleMasterDetails.getModuleDetails()) {
				if(userDept.equalsIgnoreCase(userModuleMaster.getDept())){
				 if(userModuleMaster.getModule().equalsIgnoreCase("AddWaiver")){
					 if((userHierLevel.equalsIgnoreCase("1") && userModuleMaster.getL1()==1) ||
						(userHierLevel.equalsIgnoreCase("2") && userModuleMaster.getL2()==1) ||
						(userHierLevel.equalsIgnoreCase("3") && userModuleMaster.getL3()==1) ||
						(userHierLevel.equalsIgnoreCase("4") && userModuleMaster.getL4()==1) ||
						(userHierLevel.equalsIgnoreCase("5") && userModuleMaster.getL5()==1)){
						 log.info("Entering LoanServiceImpl-AddWaiverEnabled");
						 loanBean.setAddWaiverEnabled("1");
					 }
				 }else if(userModuleMaster.getModule().equalsIgnoreCase("Cancel")){
					 if((userHierLevel.equalsIgnoreCase("1") && userModuleMaster.getL1()==1) ||
								(userHierLevel.equalsIgnoreCase("2") && userModuleMaster.getL2()==1) ||
								(userHierLevel.equalsIgnoreCase("3") && userModuleMaster.getL3()==1) ||
								(userHierLevel.equalsIgnoreCase("4") && userModuleMaster.getL4()==1) ||
								(userHierLevel.equalsIgnoreCase("5") && userModuleMaster.getL5()==1)){
								 loanBean.setCancelEnabled("1");
							 }
				 }else if(userModuleMaster.getModule().equalsIgnoreCase("Disburse")){
					 if(	(userHierLevel.equalsIgnoreCase("3") && userModuleMaster.getL5()==1)){
								 loanBean.setDisburseEnabled("1");
							 }
				 }else if(userModuleMaster.getModule().equalsIgnoreCase("UpdatePDD")){
					 if((userHierLevel.equalsIgnoreCase("1") && userModuleMaster.getL1()==1) ||
								(userHierLevel.equalsIgnoreCase("2") && userModuleMaster.getL2()==1) ||
								(userHierLevel.equalsIgnoreCase("3") && userModuleMaster.getL3()==1) ||
								(userHierLevel.equalsIgnoreCase("4") && userModuleMaster.getL4()==1) ||
								(userHierLevel.equalsIgnoreCase("5") && userModuleMaster.getL5()==1)){
								 loanBean.setUpdatePDDEnabled("1");
							 }
				 }else if(userModuleMaster.getModule().equalsIgnoreCase("WaiveClose")){
					 if((userHierLevel.equalsIgnoreCase("1") && userModuleMaster.getL1()==1) ||
								(userHierLevel.equalsIgnoreCase("2") && userModuleMaster.getL2()==1) ||
								(userHierLevel.equalsIgnoreCase("3") && userModuleMaster.getL3()==1) ||
								(userHierLevel.equalsIgnoreCase("4") && userModuleMaster.getL4()==1) ||
								(userHierLevel.equalsIgnoreCase("5") && userModuleMaster.getL5()==1)){
						 		log.info("Entering LoanServiceImpl - WaiveCloseEnabled");
								 loanBean.setWaiveCloseEnabled("1");
							 }
				 }
				 else if(userModuleMaster.getModule().equalsIgnoreCase("SOADetails")){
					 if((userHierLevel.equalsIgnoreCase("5") && userModuleMaster.getL5()==1)){
						 		
								 loanBean.setAddSOAEnabled("1");
							 }
				 }
				 
				}
			}
//		}
//			else {
//				loanBean.setAddSOAEnabled("2");
//			}
			if(loanBean.getUserName().equalsIgnoreCase("10857") || 
					loanBean.getUserName().equalsIgnoreCase("C002") ||
					loanBean.getUserName().equalsIgnoreCase("11014")||
					loanBean.getUserName().equalsIgnoreCase("10067")||
					loanBean.getUserName().equalsIgnoreCase("11306"))
			 {
				 
					loanBean.setAddSOAEnabled("2");
			 }
			
			if(loanBean.getUserName().equalsIgnoreCase("10039")||
					loanBean.getUserName().equalsIgnoreCase("11123"))
			 {
				 
					loanBean.setAddSOAEnabled("4");
			 }
			
			if(loanBean.getUserName().equalsIgnoreCase("11066"))
			 {
				 
				loanBean.setWaiveCloseEnabled("2");
			 } 
			
		/*	if(usm.getBranch().contentEquals(loan.getH3()))
			{
				System.out.println("Valid" + loanBean.getAddSOAEnabled());
				loanBean.setAddSOAEnabled("5");
				
			}*/
			
			
			loanBean.setFclBlock(loan.getFclBlock());
			
			System.out.println("addSOAEnabled:"+loanBean.getAddSOAEnabled());
			log.info(String.valueOf(loan.getCovidPayInt()));
			float vald = loan.getMoratInterest() - loan.getCovidPayInt();
			log.info("INT - INT " + String.valueOf(vald));
			loanBean.setCovidPayInt(String.valueOf(vald));
			loanBean.setLoanNo(loan.getLoanNo());
			loanBean.setCustName(loan.getName());
			loanBean.setAdvanceEMI(String.valueOf(loan.getAdvanceEmi()));
			//loanBean.setArrears(String.valueOf(loan.getArrears()));

			//loanBean.setBc(String.valueOf(loan.getBc()));
			loanBean.setCashModeFee(String.valueOf(loan.getCashCollCharges()));
			loanBean.setCoLender(loan.getCoLender());
			Calendar calendar = GregorianCalendar.getInstance();
			calendar.setTime(loan.getFirstDueDate());			
			LocalDate firstDueDate = LocalDate.of(calendar.get(Calendar.YEAR), (calendar.get(Calendar.MONTH)+1), calendar.get(Calendar.DATE));
			LocalDate today = LocalDate.now();
			Period diff = Period.between(firstDueDate, today); 
			int curMonth = diff.getDays()/30;
		//	loanBean.setCurrentMonth(String.valueOf(curMonth));
			loanBean.setAgmntDate(sdf.format(loan.getAgmntDate()));
			log.info("TEST Securitization:" + loan.getSecuritisation());
			loanBean.setSecuritisation(loan.getSecuritisation());
		
			loanBean.setFlagSec(loan.getFlagSec());
			/*loanBean.setExGratiaCreditAmount(String.valueOf(loan.getExGratiaCreditAmount()));
			loanBean.setExGratiaCreditAmountExcess(String.valueOf(loan.getExGratiaCreditAmountExcess()));*/
			
			
		if(loan.getDealerName() !=null && loan.getDealerName().contentEquals("Swift Cash"))
			loanBean.setApplNo(ParentLoan);
		else 
			loanBean.setApplNo("NA");
			
			Date currentDate = new Date();
			if(loan.getStatus().equalsIgnoreCase("APPROVED")){
				loanBean.setCancelable("TRUE");
			}else if(currentDate.before(loan.getFirstDueDate()) && loan.getStatus().equalsIgnoreCase("ACTIVE")) {
				Receivable paidReceivable = loan.getReceivable().stream().filter(receivable -> "PAID".equals(receivable.getStatus())).findAny().orElse(null);
				if( paidReceivable == null) {
					loanBean.setCancelable("TRUE");
				} else {
					//long paidCount = loan.getReceivable().stream().filter(receivable -> "PAID".equals(receivable.getStatus())).filter(receivable -> "PART_PAID".equals(receivable.getStatus())).count();
					long paidCount = loan.getReceivable().stream().filter(receivable -> "PAID".equals(receivable.getStatus())).count();
					long partPaidCount = loan.getReceivable().stream().filter(receivable -> "PART_PAID".equals(receivable.getStatus())).count();
					if((paidCount+partPaidCount) == loan.getAdvanceEmi()){
						loanBean.setCancelable("TRUE");
					}else{
						loanBean.setCancelable("FALSE");
					}
				}
			} else {
				loanBean.setCancelable("FALSE");
			}

			List<Waiver> waivers = waiverRepository.findByloanNo(loan.getLoanNo());

			if(waivers != null && waivers.size() > 0) {
				loanBean.setWaiver("TRUE");
				List<LoanWaiver> loanWaivers = new ArrayList<>();

				for( Waiver waiver : waivers) {
					LoanWaiver loanWaiver = new LoanWaiver();
					loanWaiver.setWaiverAmount(String.valueOf(waiver.getReceiptAmount()));
					loanWaiver.setWaiverType(String.valueOf(waiver.getWaiverType()));
					loanWaiver.setWaiverDate(sdf.format(waiver.getReceiptDate()));
					loanWaiver.setWaiverRemarks(waiver.getWaiverRemarks());
					loanWaivers.add(loanWaiver);
				}

				loanBean.setLoanWaivers(loanWaivers);
			}

			List<Receipt> receipts = receiptsRepository.findByloanNoAndCashInHand(loan.getLoanNo(), "Y");

			if(receipts!= null && receipts.size() > 0) {
				loanBean.setCashInHand("TRUE");
				List<LoanReceipt> loanReceipts = new ArrayList<>();

				for(Receipt receipt : receipts) {
					LoanReceipt loanReceipt = new LoanReceipt();
					
					loanReceipt.setAgentCode(receipt.getAgentCode());
					loanReceipt.setAgentName(receipt.getAgentName());
					loanReceipt.setBookNo(receipt.getBookNo());
					loanReceipt.setCashInHand(receipt.getCashInHand());
					loanReceipt.setLoanNo(receipt.getLoanNo());
					loanReceipt.setReceiptAmount(String.valueOf(receipt.getReceiptAmount()));
					loanReceipt.setReceiptDate(sdf.format(receipt.getReceiptDate()));
					loanReceipt.setReceiptNo(String.valueOf(receipt.getReceiptNo()));
					loanReceipt.setRemittanceNo(receipt.getRemittanceNo());
					loanReceipts.add(loanReceipt);
					
			
				}

				loanBean.setLoanReceipts(loanReceipts);
			}
			List<Charges> chargesList = chargesRepository.findByloanNo(loan.getLoanNo());
			if(chargesList !=null && chargesList.size()>0){
				List<com.vg.lms.bean.Charges> chargesBeanList = new ArrayList<>();
				for(Charges charges : chargesList){
					com.vg.lms.bean.Charges chargesbean= new com.vg.lms.bean.Charges();
					chargesbean.setId(String.valueOf(charges.getId()));
					chargesbean.setLoanno(String.valueOf(charges.getLoanNo()));
					chargesbean.setChargeid(String.valueOf(charges.getChargeID()));
					chargesbean.setChargeamount(String.valueOf(charges.getChargeAmount()));
					chargesbean.setTaxamount(String.valueOf(charges.getTaxAmount()));
					chargesbean.setStatus(String.valueOf(charges.getStatus()));
					chargesbean.setDuedate(String.valueOf(charges.getDueDate()));
					chargesbean.setChargepaid(String.valueOf(charges.getChargeCollected()));
					chargesbean.setTaxpaid(String.valueOf(charges.getTaxCollected()));
					chargesBeanList.add(chargesbean);					
				}
				loanBean.setChargesList(chargesBeanList);
			}
	
			
			List<WriteOff> writeOff = writeOffRepository.findByloanNo(loan.getLoanNo());
			for(WriteOff writeoff1 : writeOff)
				if(writeoff1.getId()=='1')
				//log.info("TEST WRITE OFF:" + writeoff1.getStatus() );

			
			loanBean.setCustId(String.valueOf(loan.getCustomer().getId()));
			loanBean.setCkycNumber(loan.getCkycNumber());
			
			//COVID Changes
			loanBean.setMoratReason(loan.getMoratReason());
			loanBean.setMoratInterest(String.valueOf(loan.getMoratInterest()));
			loanBean.setMoratTenure(String.valueOf(loan.getMoratTenure()));
			
			loanBean.setDealer(loan.getDealerName());
			loanBean.setDealerCode(loan.getDealerCode());
			loanBean.setDealerDisbAmount(String.valueOf(loan.getDealerDisbursementAmount()));

			loanBean.setDownPayment(String.valueOf(loan.getDownPayment()));
			loanBean.setEmi(String.valueOf(loan.getEmi()));
			loanBean.setFirstDueDate(sdf.format(loan.getFirstDueDate()));
			loanBean.setMode(loan.getRepayMode());

			loanBean.setExcessPaid(String.valueOf(loan.getExcessPaid()));
			loanBean.setCusType(loan.getCustomer().getCusCategory());
			
			


			/*Calendar agmnt = GregorianCalendar.getInstance();
			agmnt.setTime(loan.getAgmntDate());
			LocalDate agmntDate = LocalDate.of(agmnt.get(Calendar.YEAR), (agmnt.get(Calendar.MONTH)+1), agmnt.get(Calendar.DATE));
			LocalDate now = LocalDate.now();
			Period noOfDays = Period.between(agmntDate, now); 
			int noOfMonths = noOfDays.getDays()/30;
			float foreclosureAmount = loan.getFuturePrinciple();
			if(noOfMonths < 6)
				foreclosureAmount += loan.getFuturePrinciple()*0.08f;
			else if(noOfMonths < 12)
				foreclosureAmount += loan.getFuturePrinciple()*0.05f;
			else
				foreclosureAmount += loan.getFuturePrinciple()*0.03f;

			foreclosureAmount += (foreclosureAmount*0.15);


			loanBean.setForeClosureAmt(String.valueOf((int)Math.ceil(foreclosureAmount)));
			 */
			loanBean.setCoLender(loan.getCoLender());
			loanBean.setHypothecation(loan.getHypothecation());
			loanBean.setIrr(String.valueOf(loan.getIrr()));
			loanBean.setLastDueDate(sdf.format(loan.getLastDueDate()));
			loanBean.setLoanAmount(String.valueOf(loan.getAmount()));
			loanBean.setstatusDisposition(loan.getStatusDisposition());
			loanBean.setLoanId(String.valueOf(loanId));
			if(loan.getClosedTime() != null){
				loanBean.setClosedTime(String.valueOf(sdf.format(loan.getClosedTime())));
			}
			
			//loanBean.setLtv(String.valueOf(asset.get));

			//loanBean.setOdc(String.valueOf(loan.getOdcCumm()+loan.getOdcCurMonth()));
			loanBean.setOdc(String.valueOf(loan.getOdc()));
			loanBean.setOdMonths(String.valueOf(loan.getOdMonths()));


			loanBean.setPrcesFee(String.valueOf(loan.getProcFee()));

			
			if(loan.getStatus() == null){
				loanBean.setStatus("ACTIVE");
				
			}else if(loan.getStatus().equalsIgnoreCase("")||loan.getStatus().equalsIgnoreCase("ACTIVE")){
				loanBean.setStatus("ACTIVE");
				
			}else{
				loanBean.setStatus(loan.getStatus());
				
			}
			loanBean.setTenure(String.valueOf(loan.getTenure()));

  
			if(loan.getDisbursementTime()!=null) {

				loanBean.setDisbDate(sdf.format(loan.getDisbursementTime()));
				loanBean.setDisbursed("TRUE");
			}else {
				loanBean.setDisbursed("FALSE");
			}

			if(asset != null) {
				loanBean.setActualltv(String.valueOf(asset.getActualLtv()));
				loanBean.setGridAmount(String.valueOf(asset.getGridPrice()));
				loanBean.setMake(asset.getMake());
				loanBean.setModel(asset.getModel());
				loanBean.setOnRoadPrice(String.valueOf(asset.getOnroadPrice()));
				loanBean.setScheme(asset.getSchemeType());

			}

			if(loan.getFclAmount() == -11111111) {
				//prinArrear, intArrear, pos, intAfterDue, odc, oc, moratoriumInterest, oneDayInterst, fclCharge, fclAmount
				loanBean.setPrinArrear("NEW LOAN");
				loanBean.setIntArrear("NEW LOAN");
				loanBean.setPos("NEW LOAN");
				loanBean.setIntAfterDue("NEW LOAN");
				loanBean.setOdc("NEW LOAN");
				loanBean.setOc("NEW LOAN");
				loanBean.setMoratoriumInterest("NEW LOAN");
				loanBean.setOneDayInterst("NEW LOAN");
				loanBean.setFclCharge("NEW LOAN");
				loanBean.setFclAmount("NEW LOAN");			

			} else {
				loanBean.setPrinArrear(String.valueOf(loan.getPrinArears()));
				loanBean.setIntArrear(String.valueOf(loan.getIntArrears()));
				loanBean.setPos(String.valueOf(loan.getPrinOutstanding()));
				loanBean.setIntAfterDue(String.valueOf(loan.getIntLastDue()));
				loanBean.setOdc(String.valueOf(loan.getOdc()));
				loanBean.setOc(String.valueOf(loan.getChargesDue()));
				loanBean.setMoratoriumInterest(String.valueOf(loan.getMoratoriumInterest()));
				loanBean.setOneDayInterst(String.valueOf(loan.getFclOneDayInterest()));
				loanBean.setFclCharge(String.valueOf(loan.getFclCharges()));
			loanBean.setFclAmount(String.valueOf(loan.getFclAmount()));	
		loanBean.setForeClosureAmt(String.valueOf(loan.getFclAmount()));
				
				
			}


		}

		log.debug("Loan Bean : " + loanBean);

		long timeTaken=System.currentTimeMillis()-startTimeInMillis;
		log.info("Loan Bean : " + loanBean);
		log.info("Time Taken for getLoan is ==>"+timeTaken);
		return loanBean;
	}

	/*@Override
	public List<CrDrMasterBean> getCrDrNoteMaster(String noteType) {
		long startTimeInMillis = System.currentTimeMillis();
		log.info("Entering LoanServiceImpl-getCrDrNoteMaster :"+noteType);
		List<CrdrNoteMaster> crdrNoteMasters = crdrMaster.findBycrDr(noteType);
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		modelMapper.typeMap(CrdrNoteMaster.class, CrDrMasterBean.class);
		List<CrDrMasterBean> crdrMaster = new ArrayList<>();
		for(CrdrNoteMaster crdrNoteMaster : crdrNoteMasters){
			crdrMaster.add(modelMapper.map(crdrNoteMaster, CrDrMasterBean.class));
		}

		long timeTaken=System.currentTimeMillis()-startTimeInMillis;
		log.info("Time Taken for getCrDrNoteMaster is ==>"+timeTaken);
		return crdrMaster;
	}*/
	/*
	@Override
	public Map<String,List<CrDrMasterBean>> getCrDrNoteMaster() {
		long startTimeInMillis = System.currentTimeMillis();
		log.info("Entering LoanServiceImpl-getCrDrNoteMaster All");
		List<CrdrNoteMaster> crdrNoteMasters = crdrMaster.findAll();
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		modelMapper.typeMap(CrdrNoteMaster.class, CrDrMasterBean.class);
		Map<String,List<CrDrMasterBean>> noteMaster = new HashMap<>();
		noteMaster.put("cr", new ArrayList<>());
		noteMaster.put("dr", new ArrayList<>());
		for(CrdrNoteMaster crdrNoteMaster : crdrNoteMasters){
			List<CrDrMasterBean> crdrMaster= noteMaster.get(crdrNoteMaster.getCrDr());
			CrDrMasterBean masterBean = modelMapper.map(crdrNoteMaster, CrDrMasterBean.class);
			crdrMaster.add(masterBean);
			noteMaster.put(crdrNoteMaster.getCrDr(), crdrMaster);
		}

		long timeTaken=System.currentTimeMillis()-startTimeInMillis;
		log.info("Time Taken for getCrDrNoteMaster All is ==>"+timeTaken);
		return noteMaster;
	}*/


	@Override
	public int addCrDrNote(CrDrBean crdrBean){
		long startTimeInMillis = System.currentTimeMillis();
		log.info("Entering LoanServiceImpl-getCrDrNoteMaster :"+crdrBean);
		Loan loan = loanRepository.findById(crdrBean.getLoanId()).get();

		Waiver waiver = new Waiver();
		waiver.setBookNo("WAIVER");
		waiver.setReceiptNo("WOFF");
		waiver.setLoanNo(loan.getLoanNo());
		waiver.setCreateUser("ADMIN");
		waiver.setReceiptDate(new Date());
		waiver.setReceiptAmount(crdrBean.getAmount());
		waiver.setWaiverType(crdrBean.getWaiverHead());
		waiver.setWaiverRemarks(crdrBean.getDescription());
		waiverRepository.save(waiver);


		long timeTaken=System.currentTimeMillis()-startTimeInMillis;
		log.info("Time Taken for addCrDrNote is ==>"+timeTaken);
		return loan.getId();

	}


	@Override
	public int disburseLoan(int id) throws JsonProcessingException{
		long startTimeInMillis = System.currentTimeMillis();
		log.info("Entering LoanServiceImpl-disburseLoan :"+id);
		Loan loan = loanRepository.findById(id).get();
		
        if(loan.getDealerName().contentEquals("Swift Cash"))
        {
		OsclClose osclRequest = new OsclClose();
		
		osclRequest.settYPE("OSCLParentLoanClose");
		osclRequest.setaPPLICATIONNO(loan.getApplNo());
		osclRequest.setlOANNO(loan.getLoanNo());
		osclRequest.setsCCHARGE(String.valueOf(loan.getScCharge()));
		


			ObjectMapper mapper = new ObjectMapper();
			String jsonInString = mapper.writeValueAsString(osclRequest);
			log.info("Oscl Close Request : " + jsonInString);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			HttpEntity<String> entity = new HttpEntity<String>(jsonInString, headers);	  
			RestTemplate template = new RestTemplate();
			ResponseEntity<OsclResponse> responseEntity =
					template.postForEntity("http://13.235.120.228/live/MeTool/orangeWebService.php", entity, OsclResponse.class);
			log.info("Oscl Close ParentLoan Response : " + mapper.writeValueAsString(responseEntity) + responseEntity.getBody().getMessage());
			
        }
		
			loan.setStatus("ACTIVE");
		Date disbDate = new Date();
		loan.setDisbursementTime(disbDate);

		Set<Disbursement> disbursements= loan.getDisbursement();
		if(disbursements == null)
			disbursements = new HashSet<>();

		Disbursement disbursement = new Disbursement();
		disbursement.setAmount(loan.getDealerDisbursementAmount());
		disbursement.setCreateUser("LMS");
		disbursement.setLoan(loan);
		disbursement.setStatus("MANUAL");
		disbursement.setDisbDate(disbDate);
		disbursements.add(disbursement);

		loan.setDisbursement(disbursements);
		//loan.setdi
		try{
			if(loan.getLoanNo().startsWith("TW") && loan.getCoLender() != null && loan.getCoLender().equalsIgnoreCase("INCRED02")) {
				pushToInCred(loan.getCustomer(), loan.getApplNo(), loan.getId(), loan.getLoanNo());
			}
		}catch(Exception e){
			log.error("LMS incred audit duplicate push",e);
		}
		
		try{
			if(loan.getLoanNo().startsWith("BL") ) {
				WelcomeLetterBL( loan.getId());
			}
		}catch(Exception e){
			log.error("BL WelcomeLetter Exception",e);
		}

		loanRepository.save(loan);



		long timeTaken=System.currentTimeMillis()-startTimeInMillis;
		log.info("Time Taken for disburseLoan is ==>"+timeTaken);
		return loan.getId();

	}

	private void pushToInCred(Customer customer, String losAppNo, int loanId, String loanNo) {
		IncredAudit incredAudit = new IncredAudit();
		incredAudit.setLosAppName(losAppNo);		
		incredAudit.setLoanId(loanId);
		incredAudit.setLoanNo(loanNo);
		incredAudit.setSyncTime(new Date());
		try {
			InitRequest initRequest = new InitRequest();
			initRequest.setAADHAAR(customer.getAadharNo());		
			log.info("Customer DOB : " + customer.getDob());
			String[] dateArray = customer.getDob().split("-");
			initRequest.setDOB(dateArray[0] + "/" + dateArray[1] + "/" + dateArray[2]);
			initRequest.setFNAME(customer.getFirstname());
			initRequest.setLNAME(customer.getLastname());
			initRequest.setGENDER(customer.getGender());
			initRequest.setLOANTYPE("AL");
			initRequest.setMOBILE(customer.getPhone1());
			initRequest.setPARTNERREFERENCE(loanNo);
			//initRequest.setMOBILE("6380306842");
			//initRequest.setPAN(customer.getPanno());

			ADDRESS address = new ADDRESS();
			address.setADDRESS(customer.getResiAddr() + ", " + customer.getResiAddress2() + ", " + 
					customer.getResiCity() + ", "+ customer.getResiState());
			address.setPINCODE(customer.getResiPin());

			initRequest.setADDRESS(new ArrayList<>());
			initRequest.getADDRESS().add(address);


			ObjectMapper mapper = new ObjectMapper();
			String jsonInString = mapper.writeValueAsString(initRequest);
			log.info("Incred init Request : " + jsonInString);

			HttpHeaders headers = new HttpHeaders();
			headers.add("api-key", "f72d00f2ae311815a2f170f4e9db4289c84733fac12c1ef13129350defbd47");
			headers.setContentType(MediaType.APPLICATION_JSON);

			HttpEntity<String> entity = new HttpEntity<String>(jsonInString, headers);	  
			RestTemplate template = new RestTemplate();
			ResponseEntity<InitResponse> responseEntity =
					template.postForEntity("https://api.incred.com/v2/partner/application/init", entity, InitResponse.class);
			log.info("Incred init Response : " + mapper.writeValueAsString(responseEntity));
			incredAudit.setMessage(responseEntity.getBody().getMessage());
			incredAudit.setStatus("INIT");
			if(responseEntity.getBody().getStatus()) {
				incredAudit.setState("INIT");
				incredAudit.setAppId(responseEntity.getBody().getResponse().getAPPLICATIONID());
				incredAudit.setCustId(responseEntity.getBody().getResponse().getCUSTOMERID());
				incredAudit.setTxnId(responseEntity.getBody().getResponse().getTRANSACTIONID());				
			}else {
				incredAudit.setState("FRESH");
			}
		} catch (Exception e) {
			incredAudit.setState("FRESH");
			log.error("Error Occured in Incred Push :",e);
		}
		incredRepository.save(incredAudit);
	}

	@Override
	public int cancelLoan(int id,String reason, String uname) {
		long startTimeInMillis = System.currentTimeMillis();
		log.info("Entering LoanServiceImpl-cancelLoan :"+id);
		Date cancelTime = new Date();
		Loan loan = loanRepository.findById(id).get();
		loan.setStatus("CANCELLED");//loan table
		loan.setCancelReason(reason);
		loan.setCancelTime(cancelTime);
		loan.setCancelUser(uname);

		Set<Receivable> updatedReceivable = new TreeSet<>();
		List<Receivable> receivablesSorted = loan.getReceivable().stream().collect(Collectors.toList());
		Collections.sort(receivablesSorted);

		for( Receivable receivable : receivablesSorted){
			log.info("due no==>" + receivable.getDueNo()+"\n");
			if(receivable.getStatus().equals("FUTURE")) {
				receivable.setStatus("CANCELLED");//update receivables table	
			}
			updatedReceivable.add(receivable);					
		}
		loan.setReceivable(updatedReceivable);

		/*
		Set<Disbursement> disbursements= loan.getDisbursement();
		Enumeration e=  Collections.enumeration(disbursements);
		e.nextElement();
		log.info("Enum no==>" + e.nextElement()+"\n");
		if(disbursements != null){//update disbursement status as cancelled 				
			//Disbursement disbursement = loan.getDisbursement();
			//disbursement.setStatus("cancelled");
			//disbursements.(disbursement);
		}		
		loan.setDisbursement(disbursements);*/
		loanRepository.save(loan);
		//update ops status in application table
		Timestamp opsApproveTime = new Timestamp(System.currentTimeMillis());
		Application application =applicationRepository.findByappno(loan.getApplNo());
		application.setOpsApproveStatus("R");
		application.setOpsApproverComments(reason);
		application.setOpsApproverName(uname);
		application.setOpsApproveTime(opsApproveTime);
		application.setOpsStatus(null);
		//application.setLmsUpdate(0);
		application.setCoLender(null);
		application.setCoLenderStatus(null);
		applicationRepository.save(application);		


		long timeTaken=System.currentTimeMillis()-startTimeInMillis;
		log.info("Time Taken for cancel loan is ==>"+timeTaken);
		return loan.getId();
	}

	@Override
	public LoanWaiveAndCloseResponseDTO waiveAndCloseLoan(String loanNo, String waivePassword,String userName) {
		Loan lmsLoan = loanRepository.findByLoanNo(loanNo);
		LoanWaiveAndCloseResponseDTO response = new LoanWaiveAndCloseResponseDTO();
		response.setMessage("Successfully completed...");
		response.setLmsLoan(lmsLoan);
		
		LMSRegistryModel lmsRegistryModel = lmsRegistryRepository.findByRegistryType("waive_password");
		String dbWaivePassword = lmsRegistryModel.getRegistryValue();
		
		
		if(waivePassword == null || waivePassword.trim().equalsIgnoreCase("")){
			response.setMessage("Please enter password");
			return response;
		}else if(!waivePassword.trim().equalsIgnoreCase("")){
			if(!waivePassword.trim().equalsIgnoreCase(dbWaivePassword.trim())){
				response.setMessage("Invalid password");
				return response;
			}
		}
		
		float prinWaived = lmsLoan.getPrinArears() + lmsLoan.getPrinOutstanding();
		float intWaived = lmsLoan.getIntArrears() + lmsLoan.getMoratoriumInterest();
		float odcWaived = lmsLoan.getOdc();
		float chargesWaived = lmsLoan.getChargesDue() + lmsLoan.getFclCharges();
		
		if(!lmsLoan.getStatus().equalsIgnoreCase("ACTIVE")){
			response.setMessage(loanNo + " Account not active");
			return response;
		}
		List<Receivable> receivableList = lmsReceivableRepository.findByloanNo(loanNo);
		boolean isArrearPending = false;
		for(Receivable receivable : receivableList){
			if(receivable.getStatus().trim().equalsIgnoreCase("future") || 
					receivable.getStatus().trim().equalsIgnoreCase("part_paid")){
				isArrearPending = true;
			}
		}
		if(isArrearPending){
			//response.setMessage(loanNo + " Receivables pending, cannot waive");
			//return response;
		}
		for(Receivable receivable : receivableList){
			Receivable receivable_temp = lmsReceivableRepository.findById(receivable.getId());
			if(receivable.getStatus().trim().equalsIgnoreCase("future") || 
					receivable.getStatus().trim().equalsIgnoreCase("part_paid")){
				receivable_temp.setStatus("W_OFF");
				lmsReceivableRepository.save(receivable_temp);
			}
		}
		List<Charges> chargesList = chargesRepository.findByloanNo(loanNo);
		for(Charges charges : chargesList){
			Charges charges_temp = chargesRepository.findById(charges.getId());
			if(charges.getStatus().trim().equalsIgnoreCase("future") || 
					charges.getStatus().trim().equalsIgnoreCase("part_paid")){
				charges_temp.setStatus("W_OFF");
				chargesRepository.save(charges_temp);
			}
		}
		float sumODC = 0;
		List<LmsOdcDaily> lmsOdcDailyList = lmsOdcDailyRepository.findByLoanNo(loanNo);
		for(LmsOdcDaily lmsOdcDaily : lmsOdcDailyList){
			sumODC = sumODC+lmsOdcDaily.getOdc();
		}
		LmsOdcDaily lmsOdcDaily = new LmsOdcDaily();
		lmsOdcDaily.setLoanNo(loanNo);
		Calendar createTime = GregorianCalendar.getInstance();
		lmsOdcDaily.setValueDate(createTime.getTime());
		lmsOdcDaily.setType("PW");
		lmsOdcDaily.setOdc(-sumODC);
		lmsOdcDaily.setOdAmount(0);
		lmsOdcDaily.setCreatedBy(userName);
		lmsOdcDaily.setCreateTime(createTime.getTime());
		
		lmsOdcDailyRepository.save(lmsOdcDaily);
		
		Loan lmsLoan_temp = loanRepository.findByLoanNo(loanNo);
		lmsLoan_temp.setStatus("CLOSED");
		lmsLoan_temp.setStatusDisposition("WAIVER");
		lmsLoan_temp.setClosedTime(createTime.getTime());
		lmsLoan_temp.setFclAmount(0);
		lmsLoan_temp.setChargesDue(0);
		lmsLoan_temp.setOdc(0);
		loanRepository.save(lmsLoan_temp);
		
		LmsWaiverClose lmsWaiverClose = new LmsWaiverClose();
		lmsWaiverClose.setLoanNo(loanNo);
		lmsWaiverClose.setCreateUser(userName);
		lmsWaiverClose.setCreateTime(createTime.getTime());
		lmsWaiverClose.setPrinWaived(prinWaived);
		lmsWaiverClose.setIntWaived(intWaived);
		lmsWaiverClose.setOdcWaived(odcWaived);
		lmsWaiverClose.setChargesWaived(chargesWaived);
		
		lmsWaiverCloseRepository.save(lmsWaiverClose);
		return response;
	}
	
	
	
private void WelcomeLetterBL(int loanid) throws JsonProcessingException {

		

		BUSINESSLOANWL businesloanwelltr = new BUSINESSLOANWL();
		
		businesloanwelltr.setLOANiD(String.valueOf(loanid));
			
	

		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = mapper.writeValueAsString(businesloanwelltr);
		log.info("WelcomeLetter BL : " + jsonInString);

		HttpHeaders headers = new HttpHeaders();
	
		headers.add("api-key", "dGhlb3JhbmdlcmV0YWlsZmluYW5jZQ==");

		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> entity = new HttpEntity<String>(jsonInString, headers);	  
		RestTemplate template = new RestTemplate();		
		ResponseEntity<InitResponse> responseEntity =
				template.postForEntity("http://13.235.123.174/uat/los/ui/pdf/Generate/welcome_letter.php", entity, InitResponse.class);
				
	}
@Override
public int waiveCrDrNote(WaiverSplitBean crdrBean) {
  long startTimeInMillis = System.currentTimeMillis();
  log.info("Entering LoanServiceImpl-getCrDrNoteMaster :" + crdrBean);
  Loan loan = loanRepository.findById(crdrBean.getLoanId()).get();

  Waiver waiver = new Waiver();
  List<Waiver> woff = new ArrayList<Waiver>();

  
  
  System.out.println("Principal :" + crdrBean.getPrincipalAmount());
  if (crdrBean.getPrincipalAmount() > 0) {
	  waiver.setBookNo("WAIVER");
	  waiver.setReceiptNo("WOFF");
	  waiver.setCreateUser("ADMIN");
	  waiver.setWaiverRemarks(crdrBean.getDescription());
	  waiver.setLoanNo(loan.getLoanNo());
	  waiver.setReceiptDate(new Date());
    waiver.setReceiptAmount(crdrBean.getPrincipalAmount());
    waiver.setWaiverType("Principle");
    woff.add(waiver);
    // waiverRepository.save(waiver);
  }
  System.out.println("Interest :" + crdrBean.getInterestAmount());
  if (crdrBean.getInterestAmount() > 0) {
	  waiver.setBookNo("WAIVER");
	  waiver.setReceiptNo("WOFF");
	  waiver.setCreateUser("ADMIN");
	  waiver.setWaiverRemarks(crdrBean.getDescription());
	  waiver.setLoanNo(loan.getLoanNo());
	  waiver.setReceiptDate(new Date());
    System.out.println("Value :" + crdrBean.getInterestAmount());
    waiver.setReceiptAmount(crdrBean.getInterestAmount());
    waiver.setWaiverType("Interest");

    woff.add(waiver);
  }

  if (crdrBean.getChargesDueAmt() > 0) {
	  waiver.setBookNo("WAIVER");
	  waiver.setReceiptNo("WOFF");
	  waiver.setCreateUser("ADMIN");
	  waiver.setWaiverRemarks(crdrBean.getDescription());
	  waiver.setLoanNo(loan.getLoanNo());
	  waiver.setReceiptDate(new Date());
    waiver.setReceiptAmount(crdrBean.getChargesDueAmt());
    waiver.setWaiverType("Charges Due");

    woff.add(waiver);
  }

  if (crdrBean.getPenalIntAmt() > 0) {waiver.setBookNo("WAIVER");
  waiver.setReceiptNo("WOFF");
  waiver.setCreateUser("ADMIN");
  waiver.setWaiverRemarks(crdrBean.getDescription());
  waiver.setLoanNo(loan.getLoanNo());
  waiver.setReceiptDate(new Date());
    waiver.setReceiptAmount(crdrBean.getPenalIntAmt());
    waiver.setWaiverType("Penal Interest");
    waiverRepository.save(waiver);
  }

  if (crdrBean.getFclAmt() > 0) {
	  waiver.setBookNo("WAIVER");
	  waiver.setReceiptNo("WOFF");
	  waiver.setCreateUser("ADMIN");
	  waiver.setWaiverRemarks(crdrBean.getDescription());
	  waiver.setLoanNo(loan.getLoanNo());
	  waiver.setReceiptDate(new Date());
    waiver.setReceiptAmount(crdrBean.getFclAmt());
    waiver.setWaiverType("fcl");
    woff.add(waiver);
  }

  if (crdrBean.getBrokPerIntAmt() > 0) {
	  waiver.setBookNo("WAIVER");
	  waiver.setReceiptNo("WOFF");
	  waiver.setCreateUser("ADMIN");
	  waiver.setWaiverRemarks(crdrBean.getDescription());
	  waiver.setLoanNo(loan.getLoanNo());
	  waiver.setReceiptDate(new Date());
    waiver.setReceiptAmount(crdrBean.getBrokPerIntAmt());
    waiver.setWaiverType("brok per interest");
    woff.add(waiver);
  }

  if (crdrBean.getMoratoriumAmt() > 0) {
	  waiver.setBookNo("WAIVER");
	  waiver.setReceiptNo("WOFF");
	  waiver.setCreateUser("ADMIN");
	  waiver.setWaiverRemarks(crdrBean.getDescription());
	  waiver.setLoanNo(loan.getLoanNo());
	  waiver.setReceiptDate(new Date());
    waiver.setReceiptAmount(crdrBean.getMoratoriumAmt());
    waiver.setWaiverType("moratorium");
    woff.add(waiver);
  }

  System.out.println("Values:"+woff);
  waiverRepository.saveAll(woff);
  long timeTaken = System.currentTimeMillis() - startTimeInMillis;
  log.info("Time Taken for addCrDrNote is ==>" + timeTaken);
  return loan.getId();

}


//for CRM - code edited by Bhuvana 16-04-2021
	@Override
	public String getLoanData(String loanNo){
		JSONObject jo = new JSONObject();
		JSONArray array = new JSONArray();
		List<Loan> loan = loanRepository.findByLoanNoStartingWith(loanNo);
	for(int i=0;i<loan.size();i++) {
		Loan loandet = new Loan();
		loandet = loan.get(i);
		System.out.println("customer name ::"+loandet.getName());
		System.out.println("customer name ::"+loanNo);
		jo.put("MobileNumber", loandet.getMobile());
		jo.put("CustomerName",loandet.getName());
		jo.put("CustomerId", loandet.getCustomer().getId());
		jo.put("LoanNumber", loandet.getLoanNo());
	   array.add(jo);
	}
	  
   return array.toString();
	}

	
	@Override
	public String getLoanandCustomerData(String loanNo){
		JSONObject jo = new JSONObject();
		JSONObject lo = new JSONObject();
		JSONObject total = new JSONObject();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");	
		Loan loan = loanRepository.findByLoanNo(loanNo);
		ArrayList<Loan> loanlist = new ArrayList<Loan>();
		loanlist.add(loan);
	for(int i=0;i<loanlist.size();i++) {
		Loan loandet = new Loan();
		loandet =  loanlist.get(i);
		try {
		Application app = applicationRepository.findByappno(loandet.getApplNo());	
		Optional<Customer> customer = customerRepository.findById(loandet.getCustomer().getId());
		System.out.println("customer name ::"+loandet.getName());
		System.out.println("customer name ::"+loanNo);
		jo.put("CustomerId", customer.get().getId());
		jo.put("CustomerName",customer.get().getName());
		jo.put("PAN No", customer.get().getPanno());
		jo.put("Passport No",customer.get().getPassportNo());
		jo.put("DOB", customer.get().getDob());
		jo.put("Sex", customer.get().getGender());
		if(app.getAppMaritalStatus()==null) {
		jo.put("Marital Status","-");
		}else {
		jo.put("Marital Status",app.getAppMaritalStatus());	
		}
	 if(loandet.getCustomer().getEmailId()==null) {
		 jo.put("Email",app.getEmailId()); 
	 }else {
		jo.put("Email", loandet.getCustomer().getEmailId());
	 }
		jo.put("Mobile Phone", loandet.getCustomer().getPhone1());
		jo.put("Address Type","Permenant"); 
		jo.put("Address", loandet.getCustomer().getPermAddr());
		jo.put("Address2",loandet.getCustomer().getPermAddr2());
		jo.put("Created Date", loandet.getCustomer().getCreateTime());
		jo.put("City",loandet.getCustomer().getPermCity());
		jo.put("State",loandet.getCustomer().getPermState());
		jo.put("Zip Code",loandet.getCustomer().getPermPincode());
//		jo.put("SMS Notification Reqd",loandet.getCustomer().getPermState()); - sms notification
		
	//	array.put(jo.toString()); 
//		List<Receivable> rec = lmsReceivableRepository.findByloanNo(loanNo);
		String duedate = lmsReceivableRepository.duedate(loanNo);
		String duecount = lmsReceivableRepository.emicount(loanNo);
		String lastduedate = lmsReceivableRepository.lastduedate(loanNo);
		String vehiclemake = assetRepository.vehiclemake(loandet.getId());
		String vehiclemodel = assetRepository.vehiclemodel(loandet.getId());
		System.out.println("lastduedate ::"+lastduedate);
		System.out.println("vehiclemake ::"+vehiclemake);
		System.out.println("vehiclemodel ::"+vehiclemodel);
		lo.put("Loan Account Number", loandet.getLoanNo());
		lo.put("Status",loandet.getStatus());
		lo.put("Agreement ID",loandet.getId());
//		lo.put("Product ID",loandet.getpr); 
		lo.put("Product Name",loandet.getProduct());  
		lo.put("Vehicle RC Number",loandet.getAsset().getRcNo()); 
//		lo.put("Litigation Remarks By Customer",loandet.getCustomer().getName()); 
//		lo.put("Litigation Remarks By RCF",loandet.getCustomer().getName());
//		lo.put("NPA Stage ID",loandet.getCustomer().getName());
		lo.put("Tenure",loandet.getTenure());
	//	Receivable recieve = (Receivable) loandet.getReceivable();
		lo.put("EMI Due Day",duedate);
		lo.put("EMI Amount",loandet.getEmi());
		lo.put("Installation OD",duecount); 
//		lo.put("EFF Rate",loandet.getCustomer().getName());
		lo.put("Bank",app.getBankname());
		lo.put("Repayment Mode",loandet.getRepayMode());
		String branch = branchRepository.findbranch(app.getBranchCode());
		System.out.println("branch name::"+branch);
		lo.put("Loan Booking Branch",branch); 
		lo.put("Contact",loandet.getCustomer().getPhone1()); 
		String branch1 = branchRepository.findbranch(String.valueOf(loandet.getBranchCode()));
		lo.put("Service Branch",branch1); 
		lo.put("Disbursed Amount",loandet.getDealerDisbursementAmount());
		if(loandet.getInsuranceAmount()!=0.0) {
			lo.put("Insurance Status","Y");	
		}else {
			lo.put("Insurance Status","N");
		}
		lo.put("Loan Amount", loandet.getAmount());
		lo.put("Disbursement Date", sdf.format(loandet.getDisbursementTime()));
		lo.put("Down Payment Amount", loandet.getDownPayment());
		lo.put("EMI Last Paid Date", lastduedate);
		lo.put("Vehicle Make", vehiclemake);
		lo.put("Vehicle Model", vehiclemodel);
		total.put("Customer Details", jo);
		total.put("Loan Details", lo);
		}catch(Exception e) {
			System.out.println("Exception :::"+e);
		}
	}
	return total.toString();
	}


	@Override
	public String getCustomerDatawithmobileno(String mobileNo){
		JSONObject jo = new JSONObject();
		JSONObject lo = new JSONObject();
		JSONObject cus = new JSONObject();
	    JSONArray total = new JSONArray();
		List<Customer> cs = customerRepository.findByphone1(mobileNo);
		Customer customer = new Customer();
		customer = cs.get(0);
		String[] loanno = loanRepository.findphoneId(mobileNo);
		
		for(int a =0;a<loanno.length;a++) {
			
		Loan loan = loanRepository.findByLoanNo(loanno[a]);
	
		ArrayList<Loan> loanlist = new ArrayList<Loan>();
		loanlist.add(loan);
	for(int i=0;i<loanlist.size();i++) {
		Loan loandet = new Loan();
		loandet =  loanlist.get(i);	
		try {
			
		Application app = applicationRepository.findByappno(loandet.getApplNo());	
		System.out.println("customer name ::"+loandet.getName());
		System.out.println("customer name ::"+loandet.getLoanNo());		
	
		jo.put("CustomerId", customer.getId());
		jo.put("CustomerName",customer.getName());
		jo.put("PAN No", customer.getPanno());
		jo.put("Passport No",customer.getPassportNo());
		jo.put("DOB", customer.getDob());
		jo.put("Sex", customer.getGender());
		if(app.getAppMaritalStatus()==null) {
		jo.put("Marital Status","");
		}else {
		jo.put("Marital Status",app.getAppMaritalStatus());	
		}
	 if(customer.getEmailId()==null) {
		 jo.put("Email",app.getEmailId()); 
	 }else {
		jo.put("Email", customer.getEmailId());
	 }
		jo.put("Mobile Phone", customer.getPhone1());
		jo.put("Address Type","Permenant"); 
		jo.put("Address", customer.getPermAddr());
		jo.put("Address2",customer.getPermAddr2());
		jo.put("Created Date", customer.getCreateTime());
		jo.put("City",customer.getPermCity());
		jo.put("State",customer.getPermState());
		jo.put("Zip Code",customer.getPermPincode());

		String duedate = lmsReceivableRepository.duedate(loandet.getLoanNo());
		String duecount = lmsReceivableRepository.emicount(loandet.getLoanNo());
		lo.put("Loan Account Number", loandet.getLoanNo());
//		lo.put("Loan Application No", loandet.getApplNo());
		lo.put("Status",loandet.getStatus());
		lo.put("Agreement ID",loandet.getId());
//		lo.put("Product ID",loandet.getpr); 
		lo.put("Product Name",loandet.getProduct());  
		lo.put("Vehicle RC Number",loandet.getAsset().getRcNo()); 
//		lo.put("Litigation Remarks By Customer",loandet.getCustomer().getName()); 
//		lo.put("Litigation Remarks By RCF",loandet.getCustomer().getName());
//		lo.put("NPA Stage ID",loandet.getCustomer().getName());
		lo.put("Tenure",loandet.getTenure());
	//	Receivable recieve = (Receivable) loandet.getReceivable();
		lo.put("EMI Due Day",duedate);
		lo.put("EMI Amount",loandet.getEmi());
		lo.put("Installation OD",duecount); 
//		lo.put("EFF Rate",loandet.getCustomer().getName());
		lo.put("Bank",app.getBankname());
		lo.put("Repayment Mode",loandet.getRepayMode());
		String branch = branchRepository.findbranch(app.getBranchCode());
		System.out.println("branch name::"+branch);
		lo.put("Loan Booking Branch",branch); 
		lo.put("Contact",loandet.getCustomer().getPhone1()); 
		String branch1 = branchRepository.findbranch(String.valueOf(loandet.getBranchCode()));
		lo.put("Service Branch",branch1); 
		lo.put("Disbursed Amount",loandet.getDealerDisbursementAmount());
		if(loandet.getInsuranceAmount()!=0.0) {
			lo.put("Insurance Status","Y");	
		}else {
			lo.put("Insurance Status","N");
		}
		
		//total.add(jo);
		total.add(lo);
		cus.put("Customer Details", jo);
		cus.put("Loan Details",total); 
		}catch(Exception e) {
			System.out.println("Exception :::"+e);
		}
	}
	
		}
		
	return cus.toString();
	}



}
