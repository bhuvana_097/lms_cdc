package com.vg.lms.service;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;

public class CustomLogoutSuccessHandler  extends
SimpleUrlLogoutSuccessHandler implements LogoutSuccessHandler{
	
	private final Logger log = LoggerFactory.getLogger(MasterServiceImpl.class);
	
	@Autowired
	private MasterService masterServiceImpl;
	
	@Override
    public void onLogoutSuccess(
      HttpServletRequest request, 
      HttpServletResponse response, 
      Authentication authentication) 
      throws IOException, ServletException {
  
        log.info("LogOut:"+ authentication.getName());
        //int r = masterServiceImpl.Logoff(authentication.getName());
        super.onLogoutSuccess(request, response, authentication);
    }
}
