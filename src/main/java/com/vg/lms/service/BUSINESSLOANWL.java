package com.vg.lms.service;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	"LOAN_ID",
})
public class BUSINESSLOANWL {

    @JsonProperty("LOAN_ID")
    private String lOANiD;

    
    @JsonProperty("LOAN_ID")
    public String getLOANiD() {
        return lOANiD;
    }

    @JsonProperty("LOAN_ID")
    public void setLOANiD(String lOANiD) {
        this.lOANiD = lOANiD;
    }


}

