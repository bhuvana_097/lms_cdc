package com.vg.lms.service;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


import com.vg.colend.request.ReceiptRequestDTO;
import com.vg.colend.service.Loans;
import com.vg.colend.service.Receipts;
import com.vg.colend.service.ReceiptsService;
import com.vg.colend.service.VGColendClient;
import com.vg.lms.bean.LoanReceipt;
import com.vg.lms.colend.ColendService;


import com.vg.lms.model.Loan;
import com.vg.colend.model.Receipt;


import com.vg.lms.repository.LoanRepository;
import com.vg.lms.repository.ReceiptsRepository;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Component
public class ColendCollectionDataPush {

    @Autowired
    private LoanRepository loanRepository;
    
   
    
 
	@Autowired	
	private ColendService colendService;
   
    
    @Autowired
	private ReceiptsRepository receiptsRepository;

	
    private final Logger log = LoggerFactory.getLogger(ColendCollectionDataPush.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    
 // @Scheduled(fixedDelay = 60000)
//    @SuppressWarnings("unused")
	public  void pushOldData(String colender) throws NullPointerException{
	  System.out.println("starting...");
        List < Loan > pendingtxn = loanRepository.findByCoLender(colender);
        System.out.println("inside...");  													
int count=0;
        for (Loan clixAudit: pendingtxn) {
        	System.out.println("inside for loop...");
            Loan loan = loanRepository.findById(clixAudit.getId()).get();
            ReceiptRequestDTO loanReceipt = new ReceiptRequestDTO();
          Receipt rect = new Receipt();
/*if(		loan.getLoanNo().contentEquals("TW00282020010111153"))
{*/
          

	com.vg.colend.service.Receipts rctps = new com.vg.colend.service.Receipts("ORFIL");
	SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
	
	List<com.vg.lms.model.Receipt> receipts = receiptsRepository.findByloanNoAndCashInHand(loan.getLoanNo(), "N");
//	List<com.vg.lms.model.Receipt> receipts = receiptsRepository.findByloanNo(loan.getLoanNo());
	VGColendClient colendClient = new VGColendClient("ORFIL", "b3JmaWxAMTIz");
	if(receipts!= null && receipts.size() > 0) {
		List<LoanReceipt> loanReceipts = new ArrayList<>();

	for(com.vg.lms.model.Receipt receipt : receipts) {
			
						
			loanReceipt.setLoanNo(loan.getLoanNo());
			loanReceipt.setCustName(loan.getName());
			loanReceipt.setTransactionNo(receipt.getBookNo()+receipt.getReceiptNo());
			loanReceipt.setBookNo(receipt.getBookNo());
			loanReceipt.setReceiptNo(String.valueOf(receipt.getReceiptNo()));
            System.out.println(sdf.format(receipt.getReceiptDate()));
			loanReceipt.setReceiptDate(sdf.format(receipt.getReceiptDate()));
			loanReceipt.setRealReceiptDate(sdf.format(receipt.getReceiptDate()));
			loanReceipt.setArrears(receipt.getArrears());
			loanReceipt.setCharges(loan.getChargesDue());
			loanReceipt.setOdc(receipt.getOdc());
			loanReceipt.setReceiptAmount(receipt.getReceiptAmount());
			loanReceipt.setCancelled(receipt.getCancelled());
			loanReceipt.setCancelReason(receipt.getCancelReason());
			loanReceipt.setCancelledUser(receipt.getCancelled());
			loanReceipt.setMobileNo1(loan.getMobile());
			loanReceipt.setMobileNo2(loan.getMobile());
			loanReceipt.setNbfcCode("ORFIL");
			loanReceipt.setColenderCode("CLIX");
			loanReceipt.setType(receipt.getRemarks());	
			
			log.info(loan.getLoanNo()
+loan.getName()
+receipt.getBookNo()
+receipt.getReceiptNo()
+receipt.getReceiptDate()
+receipt.getReceiptDate()
+receipt.getArrears()
+loan.getChargesDue()
+receipt.getOdc()
+receipt.getReceiptAmount()
+receipt.getCancelled()
+receipt.getCancelReason()
+receipt.getCancelled()
+loan.getMobile()
+loan.getMobile());
						
			try
			{
				log.info(" COLEND PUSH COLLECTION");
				
				com.vg.colend.model.Receipt rc=colendClient.Loans.Receipts.add(loanReceipt);
				
				System.out.println("Colend-Service1" +"Values:" + rc);
				log.info("Check Response Sample Values :" + rc.getId());
			}catch(Exception e )
			{
				System.out.println("Colend-Service" + e);
			}
			
			
	
		}
	
		
		}
	

	

}
    } 
       
     
//}
    	
}
