package com.vg.lms.service;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vg.lms.model.Loan;
import com.vg.lms.model.Receipt;
import com.vg.lms.model.Receivable;
import com.vg.lms.model.Transaction;
import com.vg.lms.repository.LoanRepository;
import com.vg.lms.repository.ReceiptsRepository;
import com.vg.lms.request.RemitRequestDTO;
import com.vg.lms.response.BaseResponseDTO;


import com.vg.colend.request.ReceiptRequestDTO;
import com.vg.colend.service.Receipts;
import com.vg.colend.service.ReceiptsService;


@Component
public class RemitanceServiceImpl implements RemitanceService {

	@Autowired
	private ReceiptsRepository receiptRepository;

	@Autowired
	private LoanRepository loanRepository;
	
	



	private final Logger log = LoggerFactory.getLogger(RemitanceServiceImpl.class);

	@Override
	public BaseResponseDTO updateRemitance(RemitRequestDTO remitRequestDTO) {
		long startTimeInMillis = System.currentTimeMillis();
		
		ReceiptRequestDTO colendrrdto = new ReceiptRequestDTO();
		log.info("Entering RemitanceServiceImpl-updateRemitance :"+remitRequestDTO);
		List<Receipt> receipts = receiptRepository.findByremittanceNo(remitRequestDTO.getRemitId());

		for(Receipt receipt : receipts){
			
			if(receipt.getLmsAbsorbTime() == null) {
				Loan loan = loanRepository.findById(Integer.parseInt(receipt.getLoanNo())).get();
				
				float totalPaidAmount = loan.getExcessPaid() + receipt.getArrears();
				
				if(totalPaidAmount < loan.getEmi()) {
					loan.setExcessPaid(totalPaidAmount);
				} else {
					int noOfEMI = (int) (totalPaidAmount/loan.getEmi());
					int odMonths = loan.getOdMonths() - noOfEMI;
					loan.setOdMonths(odMonths < 1 ? 0 : odMonths);

					loan.setExcessPaid(totalPaidAmount - (loan.getEmi()*noOfEMI));

					Set<Receivable> updatedReceivable = new TreeSet<>();
					List<Receivable> receivablesSorted = loan.getReceivable().stream().collect(Collectors.toList());
					Collections.sort(receivablesSorted);
					for(int i=0;i<receivablesSorted.size();i++){
						log.info("due no==>" + receivablesSorted.get(i).getDueNo()+"\n");				
					}
					Set<Transaction> transactions = loan.getTransaction();
					if(transactions == null) {
						transactions = new TreeSet<>();
					}
					for( Receivable receivable : receivablesSorted){
						if(noOfEMI > 0 && (receivable.getStatus().equalsIgnoreCase("FUTURE")|| (receivable.getStatus().equalsIgnoreCase("BOUNCE")))){
							receivable.setStatus("PAID");
							loan.setFuturePrinciple(receivable.getPrincipleRemaining());
							loan.setArrears(loan.getArrears() - receivable.getEmi());
							transactions.add(getReceiptTransaction(receivable));
							//txnRepository.save(getReceiptTransaction(receivable));						
							noOfEMI--;
						}
						updatedReceivable.add(receivable);					
					}
					loan.setReceivable(updatedReceivable);
				}
				receipt.setLmsAbsorbTime(new Date());
				receiptRepository.save(receipt);
				
		/*		colendrrdto.setBookNo(receipt.getBookNo());
				colendrrdto.setCustName(loan.getName());
				colendrrdto.setReceiptAmount(receipt.getReceiptAmount());
				colendrrdto.setLoanNo(loan.getLoanNo());
				colendrrdto.setArrears(receipt.getArrears());
				colendrrdto.setCancelled(receipt.getCancelled());
				colendrrdto.setCancelReason(receipt.getCancelReason());
				colendrrdto.setMobileNo1(loan.getMobile());
				colendrrdto.setOdc(receipt.getOdc());
				colendrrdto.setReceiptDate(String.valueOf(receipt.getReceiptDate()));
				colendrrdto.setReceiptNo(String.valueOf(receipt.getReceiptNo()));
				colendrrdto.setCharges(loan.getChargesDue());
				
				try {
					receit.add(colendrrdto);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
				loanRepository.save(loan);
			}
		}
		BaseResponseDTO response = new BaseResponseDTO();
		response.setStatusCode("0");
		response.setStatusMessage("SUCCESS");

		long timeTaken=System.currentTimeMillis()-startTimeInMillis;
		log.info("Time Taken for getCustomer is ==>"+timeTaken);
		return response;
	}

	private Transaction getReceiptTransaction(Receivable receivable) {
		Transaction txn = new Transaction();
		txn.setCredit(receivable.getEmi());
		txn.setTxnDate(new Date());
		txn.setTxnType("RECEIPT");
		txn.setValDate(new Date());
		txn.setReceivable(receivable);
		txn.setLoan(receivable.getLoan());
		return txn;
	}



}
