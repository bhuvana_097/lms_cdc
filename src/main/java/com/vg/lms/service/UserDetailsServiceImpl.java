package com.vg.lms.service;
//package com.fl.lms.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.security.core.userdetails.User;

import com.vg.lms.UserModuleMasterDetails;
import com.vg.lms.WebSecurityConfig;
import com.vg.lms.model.UserMaster;
import com.vg.lms.repository.UserMasterRepository;
import com.vg.lms.repository.UserModuleMasterRepository;
import com.vg.lms.request.AddLmsUserRequestDTO;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{
    @Autowired
    private UserMasterRepository userRepository;
    @Autowired
    private UserModuleMasterRepository userModuleMasterRepository;
    private final Logger log = LoggerFactory.getLogger(WebSecurityConfig.class);
    
    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    	UserMaster userMaster = userRepository.findBycode(username);
    	if(userMaster == null){
    		throw new UsernameNotFoundException("Invalid Username or Password");
    	}
    	
    
    	UserModuleMasterDetails userModuleMasterDetails = new UserModuleMasterDetails();
    	userModuleMasterDetails.setModuleDetails(userModuleMasterRepository.findAll());
    	BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
    	List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>();
    	GrantedAuthority authority = new SimpleGrantedAuthority(userMaster.getDept().toUpperCase() + "-" + userMaster.getHierLevel());
    	grantList.add(authority);
    	UserDetails userDetails = (UserDetails) new User(userMaster.getCode(),bCryptPasswordEncoder.encode(userMaster.getPasswd()), grantList);
    	//log.info("TEST LOGIN :"+ userDetails.getUsername());
    	return userDetails;
    }
    
    @GetMapping("/srchuser")
	public List<UserMaster>  retrieveUser()
	{
		return userRepository.findAll();
	}
}