package com.vg.lms.service;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.vg.lms.bean.CrDrBean;
import com.vg.lms.bean.CrDrMasterBean;
import com.vg.lms.bean.LoanBean;
import com.vg.lms.bean.WaiverSplitBean;
import com.vg.lms.request.GetLoanRequestDTO;
import com.vg.lms.request.LoanRequestDTO;
import com.vg.lms.response.LoanResponseDTO;
import com.vg.lms.response.LoanWaiveAndCloseResponseDTO;

public interface LoanService {
	
	public LoanResponseDTO addLoan(LoanRequestDTO loanRequestDTO) throws ParseException;

	public LoanResponseDTO getLoan(GetLoanRequestDTO getLoanRequestDTO);
	
	public LoanBean getLoan(int loanId,String role,String uname);
	
	//public LoanBean getUser(String role,String uname);
	
	//public List<CrDrMasterBean> getCrDrNoteMaster(String noteType);
	
	//public Map<String,List<CrDrMasterBean>> getCrDrNoteMaster();
	
	public int addCrDrNote(CrDrBean crdrBean);
	
	public int disburseLoan(int id)  throws JsonProcessingException;
	
	public int cancelLoan(int id,String reason, String uname);
	
	//public String addCrDrNote(String )
	
	//public LoanResponseDTO getLoan()
	
	public LoanWaiveAndCloseResponseDTO waiveAndCloseLoan(String loanNo, String waivePassword,String userName);

	public int waiveCrDrNote(WaiverSplitBean crdrBean);
	
	// for CRM - code edited by Bhuvana 16-04-2021
		public String getLoanData(String loanNo);
		public String getLoanandCustomerData(String loanNo);
		public String getCustomerDatawithmobileno(String mobileNo);
		
	
}
