package com.vg.lms.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.vg.lms.request.CustomerDTO;
import com.vg.lms.request.GetCustomerRequestDTO;
import com.vg.lms.request.LOSRequestDTO;
import com.vg.lms.response.CustomerResponseDTO;
import com.vg.lms.response.LOSResponseDTO;

public interface CustomerService {
	
	public CustomerResponseDTO getCustomer(GetCustomerRequestDTO getCustomerRequestDTO);
	public CustomerResponseDTO addCustomer(CustomerDTO customerRequestDTO);
	
	public LOSResponseDTO pushLOSData(LOSRequestDTO losRequestDTO) throws JsonProcessingException;
	// for CRM - code edited by Bhuvana 13-04-2021
		public String getCustomerData(String mobileNo);
}
