package com.vg.lms.service;

import java.io.File;

import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;


/***
 * This is a utility class to send eMails to any individual.
 * @author etp
 *
 */
@Component
public class Mailer {
	
	 @Autowired
	 public JavaMailSender emailSender;

	
	    public void sendMessageWithAttachment(String to, String subject, String text, String pathToAttachment , String docName) {
	        try {
	            MimeMessage message = emailSender.createMimeMessage();
	            // pass 'true' to the constructor to create a multipart message
	            MimeMessageHelper helper = new MimeMessageHelper(message, true);

	            helper.setTo(to);
	            helper.setSubject(subject);
	            helper.setText(text,true);
	            
	            /*MimeBodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(text, "text/html");
				helper.set*/

	            FileSystemResource file = new FileSystemResource(new File(pathToAttachment));
	            helper.addAttachment(docName+".pdf", file);

	            emailSender.send(message);
	        } catch (MessagingException e) {
	            e.printStackTrace();
	        }
	}
}


