package com.vg.lms.service;

import com.vg.lms.request.RemitRequestDTO;
import com.vg.lms.response.BaseResponseDTO;

public interface RemitanceService {
	
	public BaseResponseDTO updateRemitance( RemitRequestDTO remitRequestDTO);

}
