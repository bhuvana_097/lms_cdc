package com.vg.lms.service;

import java.util.List;

import com.vg.lms.bean.SOABean;

public interface SOADetailsService {
	public List<SOABean> fetchSOADetails(int id);
}
