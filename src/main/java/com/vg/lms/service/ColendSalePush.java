package com.vg.lms.service;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


import com.vg.colend.request.ReceiptRequestDTO;
import com.vg.colend.service.Loans;
import com.vg.colend.service.Receipts;
import com.vg.colend.service.ReceiptsService;
import com.vg.colend.service.VGColendClient;
import com.vg.lms.bean.LoanReceipt;
import com.vg.lms.colend.ColendService;


import com.vg.lms.model.Loan;
import com.vg.colend.model.Receipt;


import com.vg.lms.repository.LoanRepository;
import com.vg.lms.repository.ReceiptsRepository;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@Component
public class ColendSalePush {

    @Autowired
    private LoanRepository loanRepository;
    
   
    
 
	@Autowired	
	private ColendService colendService;
   
    
    @Autowired
	private ReceiptsRepository receiptsRepository;

	
    private final Logger log = LoggerFactory.getLogger(ColendSalePush.class);


   //@Scheduled(fixedDelay = 600000)
    @SuppressWarnings("unused")
	public  void pushOldData() throws NullPointerException{
    	log.info("INSIDE COLEND  SALES");
    	System.out.println("starting...");
        List < Loan > pendingtxn = loanRepository.findByCoLender("BHARATPE");
        													
        System.out.println("starting.inside..");
        for (Loan clixAudit: pendingtxn) {
            Loan loan = loanRepository.findById(clixAudit.getId()).get();
         
            System.out.println("starting..forloop.");
                       try{//push to colender SDK
             System.out.println("LoanNo:" + loan.getLoanNo());
    			pushToColendService(loan);
    			}catch(Exception e){
    			log.error("error in colender push",e);
    		}
        }        
     
}
	
	private void pushToColendService(Loan loan){	
		boolean pushToColend = false;	
		if(loan.getCoLender() != null	
				&& !loan.getCoLender().equalsIgnoreCase("") 	
				&& !loan.getCoLender().equalsIgnoreCase("none")){	
			pushToColend = true;	
		}	
		if(loan.getCoLenderDisposition() != null	
				&& !loan.getCoLenderDisposition().equalsIgnoreCase("") 	
				&& !loan.getCoLenderDisposition().equalsIgnoreCase("none")){	
			pushToColend = true;	
		}	
		// push only colender loan details along with customer details for loan	
		if(pushToColend){	
			
			colendService.pushToColend(loan);	
		}	
	}
    	
}
