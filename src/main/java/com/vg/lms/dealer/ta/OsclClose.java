package com.vg.lms.dealer.ta;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	"TYPE",
    "APPLICATION_NO",
	"LOAN_NO",
	"SC_CHARGE"
})

public class OsclClose {
	
	@JsonProperty("TYPE")
	private String tYPE;
	@JsonProperty("APPLICATION_NO")
	private String aPPLICATIONNO;
	@JsonProperty("LOAN_NO")
	private String lOANNO;
	@JsonProperty("SC_CHARGE")
	private String sCCHARGE;
	
	public String gettYPE() {
		return tYPE;
	}
	public void settYPE(String tYPE) {
		this.tYPE = tYPE;
	}
	
	public String getaPPLICATIONNO() {
		return aPPLICATIONNO;
	}
	public void setaPPLICATIONNO(String aPPLICATIONNO) {
		this.aPPLICATIONNO = aPPLICATIONNO;
	}
	public String getlOANNO() {
		return lOANNO;
	}
	public void setlOANNO(String lOANNO) {
		this.lOANNO = lOANNO;
	}
	public String getsCCHARGE() {
		return sCCHARGE;
	}
	public void setsCCHARGE(String sCCHARGE) {
		this.sCCHARGE = sCCHARGE;
	}
	

}
