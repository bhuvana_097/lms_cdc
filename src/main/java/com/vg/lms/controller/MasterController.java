package com.vg.lms.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.catalina.session.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.coh.lms.IDFC.IdfcService;
import com.coh.lms.IDFC.MobileRequest;
import com.coh.lms.IDFC.MobileResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.vg.lms.bean.AddLmsUserBean;
import com.vg.lms.bean.LoanBean;
import com.vg.lms.bean.SearchLmsUserBean;
import com.vg.lms.bean.UpdateLmsUserBean;
import com.vg.lms.bean.UpdateSessionBean;
import com.vg.lms.bean.UpdateUserPassword;
import com.vg.lms.bean.osclCorrection;
import com.vg.lms.model.LmsUserLogUser;
import com.vg.lms.model.SessionDetails;
import com.vg.lms.model.UserMaster;
import com.vg.lms.repository.LmsUserLogUserRepository;
import com.vg.lms.repository.UserMasterRepository;
import com.vg.lms.request.AddLmsUserRequestDTO;
import com.vg.lms.request.ClixRequest;
import com.vg.lms.request.LOSRequestDTO;
import com.vg.lms.request.SearchLmsUserRequest;
import com.vg.lms.request.SessionRequestDTO;
import com.vg.lms.request.UserPasswordChangeRequest;
import com.vg.lms.request.osclCorrectionRequestDTO;
import com.vg.lms.response.LOSResponseDTO;
import com.vg.lms.service.CLixServiceImpl;
import com.vg.lms.service.IdfcServices;
import com.vg.lms.service.MasterService;

@Controller
public class MasterController {
	
	
	
	@Autowired
	private MasterService masterServiceImpl;
		
	@Autowired
	private IdfcServices idfcservice;
	
	@Autowired
	private UserMasterRepository userMasterRepository;
	
	@Autowired
	private LmsUserLogUserRepository lmsUserLogUserRepository;
	
	
	private final Logger log = LoggerFactory.getLogger(MasterController.class);
	
	@RequestMapping(value = "/accessDenied")
	public String accessDenied(Authentication authentication) {
		return "accessDenied"; 
	}

	@RequestMapping(value = "/addlmsuser")
	public ModelAndView addlmsuser(Authentication authentication) {
		ModelAndView maView = new ModelAndView("addlmsuser");
		String usrRole=masterServiceImpl.getUserRole(authentication);
		if(masterServiceImpl.getUserAccess(usrRole, "AddLmsUser")){
			//return "lmsuseredit"; 
		}else{
			maView = new ModelAndView("redirect:/accessDenied");
		}
		return maView;
		//return "addlmsuser"; 
	}
	
	@PostMapping("/insertlmsuser")
    public ModelAndView add(@Valid @ModelAttribute("insertlmsuser") final AddLmsUserBean addLmsUserBean, final BindingResult result, final ModelMap model,Authentication authentication)  {
		ModelAndView maView = new ModelAndView("lmsuserlist");
		
		
		String usrRole=masterServiceImpl.getUserRole(authentication);
		
		if(masterServiceImpl.getUserAccess(usrRole, "AddLmsUser")){
			AddLmsUserRequestDTO addLmsUserRequestDTO = new AddLmsUserRequestDTO();
			Calendar createTime = GregorianCalendar.getInstance();
			addLmsUserRequestDTO.setCode(addLmsUserBean.getLmsusercode());
			addLmsUserRequestDTO.setName(addLmsUserBean.getLmsusername());
			addLmsUserRequestDTO.setPasswd(addLmsUserBean.getLmsuserpwd());
			addLmsUserRequestDTO.setDept(addLmsUserBean.getDept());
			addLmsUserRequestDTO.setHierLevel(addLmsUserBean.getHier_level());
			if(addLmsUserBean.getBranch().equals("HO"))
			{
				
//				addLmsUserRequestDTO.setBranch("");
			}else {
			addLmsUserRequestDTO.setBranch(addLmsUserBean.getBranch());
			}
			addLmsUserRequestDTO.setStatus(addLmsUserBean.getStatus());
			addLmsUserRequestDTO.setRole("");
			addLmsUserRequestDTO.setCreateUser(authentication.getName());
			addLmsUserRequestDTO.setCreateTime(createTime.getTime());
			addLmsUserRequestDTO.setSessionID("");
			
			
			int retval = masterServiceImpl.addLmsUser(addLmsUserRequestDTO);
			if(retval == 1) {
				maView = new ModelAndView("redirect:/UserAlreadyexist");
			}
		}else{
			maView = new ModelAndView("redirect:/accessDenied");
		}		
		return maView;
    }
	@RequestMapping(value = "/lmsuserlist", method = RequestMethod.GET)
    public ModelAndView lmsuserlist(Authentication authentication) {
		ModelAndView maView = new ModelAndView("lmsuserlist");
		String usrRole=masterServiceImpl.getUserRole(authentication);
		if(masterServiceImpl.getUserAccess(usrRole, "AddLmsUser")){
			List<UserMaster> UserMasterList = masterServiceImpl.lmsUserList();
			//ModelAndView modelAndView = new ModelAndView("lmsuserlist");
			maView.addObject("UserMasterList", UserMasterList);
		}else{
			maView = new ModelAndView("redirect:/accessDenied");
		}		
		log.info("insertlmsuser result list: " + maView);
		return maView;
    }
	@RequestMapping(value = "/lmsuseredit")
	public ModelAndView lmsuseredit(Authentication authentication) {
		ModelAndView maView = new ModelAndView("lmsuseredit");
		String usrRole=masterServiceImpl.getUserRole(authentication);
		if(masterServiceImpl.getUserAccess(usrRole, "AddLmsUser")){
			//return "lmsuseredit"; 
		}else{
			maView = new ModelAndView("redirect:/accessDenied");
		}
		return maView;
	}
	
	@PostMapping("/lmsuserupdate")
    public ModelAndView lmsuserupdate(@Valid @ModelAttribute("lmsuserupdate") final UpdateLmsUserBean updateLmsUserBean, final BindingResult result, final ModelMap model,Authentication authentication) {
		
		ModelAndView maView = new ModelAndView("redirect:/lmsuserlist");
		String usrRole=masterServiceImpl.getUserRole(authentication);
		if(masterServiceImpl.getUserAccess(usrRole, "AddLmsUser")){
		AddLmsUserRequestDTO addLmsUserRequestDTO = new AddLmsUserRequestDTO();
		Calendar createTime = GregorianCalendar.getInstance();
		log.info(" insertlmsuser result update : " + updateLmsUserBean.toString());
		addLmsUserRequestDTO.setId(updateLmsUserBean.getId());
		addLmsUserRequestDTO.setCode(updateLmsUserBean.getLmsusercode());
		addLmsUserRequestDTO.setName(updateLmsUserBean.getLmsusername());
		log.info(updateLmsUserBean.getReason());
		addLmsUserRequestDTO.setReason(updateLmsUserBean.getReason());
		addLmsUserRequestDTO.setPasswd(updateLmsUserBean.getLmsuserpwd());
		
		
		log.info("Password:"+ updateLmsUserBean.getLmsuserpwd() +":"+updateLmsUserBean.getStatus());
		if (updateLmsUserBean.getStatus().equals("DeActive"))
		addLmsUserRequestDTO.setPasswd("*****");
		
		if (updateLmsUserBean.getStatus().equals("Active"))
		addLmsUserRequestDTO.setPasswd(updateLmsUserBean.getLmsuserpwd());
		addLmsUserRequestDTO.setDept(updateLmsUserBean.getDept());
		addLmsUserRequestDTO.setHierLevel(updateLmsUserBean.getHier_level());
		addLmsUserRequestDTO.setBranch(updateLmsUserBean.getBranch());
		addLmsUserRequestDTO.setStatus(updateLmsUserBean.getStatus());
		addLmsUserRequestDTO.setRole("");
		addLmsUserRequestDTO.setModifyUser(authentication.getName());
		addLmsUserRequestDTO.setModifyTime(createTime.getTime());
		//log.info("insertlmsuser result update1 : " + addLmsUserRequestDTO);
		log.info("Update ends controller impl");
		masterServiceImpl.updateLmsUser(addLmsUserRequestDTO);
		}else{
			maView = new ModelAndView("redirect:/accessDenied");
		}
		return maView;
    }
	@RequestMapping(value="/IdfcTransaction",method=RequestMethod.POST, consumes={"application/json"})
	public @ResponseBody MobileResponse pennyDropValidation(@RequestBody MobileRequest mobileRequest){			
		log.debug("Entering IDFC-PENNY_DROP" );		
		MobileResponse response = idfcservice.IdfcValidation(mobileRequest);		
		log.debug(" CustomerController IDFC Response:" + response);
		return 	response;
		//return null;
	}

	@RequestMapping(value = "/userpasswordchange")
	public ModelAndView userpasswordchange(Authentication authentication) {
		ModelAndView maView = new ModelAndView("userpasswordchange");
		String usrRole=masterServiceImpl.getUserRole(authentication);
		
		return maView;
	}
	
	@PostMapping(value = "/passwordchange")
	public ModelAndView passwordchange (@Valid @ModelAttribute("userpasswordchange") final UpdateUserPassword updateLmsUserBean, final BindingResult result, final ModelMap model,Authentication authentication)
	{
		ModelAndView maView = new ModelAndView("redirect:/passwordChanged");
			
		
		String usrRole=masterServiceImpl.getUserRole(authentication);
		log.info("Role : " + authentication.getName());
		
			UserPasswordChangeRequest addLmsUserRequestDTO = new UserPasswordChangeRequest();
		Calendar createTime = GregorianCalendar.getInstance();
		
		log.info(" Password Change result : " + updateLmsUserBean.toString());
		addLmsUserRequestDTO.setId(updateLmsUserBean.getId());
		
		updateLmsUserBean.setLmsusercode(authentication.getName());
		addLmsUserRequestDTO.setCode(updateLmsUserBean.getLmsusercode());
		addLmsUserRequestDTO.setName(updateLmsUserBean.getLmsusername());
		log.info("PASS : " + updateLmsUserBean.getLmsuserpwd());
		if(updateLmsUserBean.getNewuserpwd().equals(updateLmsUserBean.getLmsuserpwd()))
			return maView = new ModelAndView("redirect:/newPsswdold");
		if(updateLmsUserBean.getNewuserpwd().length()<8)
			return maView = new ModelAndView("redirect:/wrongPassword");
		if(updateLmsUserBean.getNewuserpwd().equals(updateLmsUserBean.getCnfrmnewpwd()))
		{
		addLmsUserRequestDTO.setPasswd(updateLmsUserBean.getNewuserpwd());
		}else {
			return maView = new ModelAndView("redirect:/newPsswdCnfrm");
		}
		
		addLmsUserRequestDTO.setModifyUser(authentication.getName());
		addLmsUserRequestDTO.setModifyTime(createTime.getTime());
		log.info("insertlmsuser result : " + addLmsUserRequestDTO);
		masterServiceImpl.UserPasswdChange(addLmsUserRequestDTO);
			
		return maView;
		
	}
	
	@RequestMapping(value = "/passwordChanged")
	public String passwordChanged(Authentication authentication) {
		return "passwordChanged"; 
	}
	@RequestMapping(value = "/wrongPassword")
	public String wrongPassword(Authentication authentication) {
		return "wrongPassword"; 
	}
	
	@RequestMapping(value = "/newPsswdCnfrm")
	public String newPsswdCnfrm(Authentication authentication) {
		return "newPsswdCnfrm"; 
	}
	@RequestMapping(value = "/newPsswdold")
	public String newPsswdold(Authentication authentication) {
		return "newPsswdold"; 
	}
	@RequestMapping(value = "/UserAlreadyexist")
	public String UserAlreadyexist(Authentication authentication) {
		return "UserAlreadyexist"; 
	}
	
	SimpleDateFormat sft = new SimpleDateFormat ("yyyy-MM-dd"); 
	SimpleDateFormat sft1 = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss"); 
	
	@RequestMapping(value = "/LandingPage")
	public ModelAndView LandingPage(Authentication authentication) {
		ModelAndView maView = new ModelAndView("LandingPage");
		String usrRole=masterServiceImpl.getUserRole(authentication);
		
		String sessionid=new String();
		String max=(authentication.getDetails().toString());
		int value = max.length();
		int value1 = max.length() - 32;
		sessionid =max.substring(value1,value);
		log.info("INside Session:"+sessionid);
		// Code Edited By Bhuvana(User Log Details) - 31-03-2021 //	
		for (GrantedAuthority authority : authentication.getAuthorities()) {
		     String role = authority.getAuthority();
               usrRole= role;
             String uname = authentication.getName();
		LmsUserLogUser loguser =new LmsUserLogUser();
        loguser.setLmsUserLogUserid(uname);
        loguser.setLmsUserLogUserrole(usrRole);
        Date date = new Date();
        String date1 = sft.format(date);
        String datetime = sft1.format(date);
        try {
			loguser.setLmsUserLogDate(sft.parse(date1));
			loguser.setLmsUserLoginTime(sft1.parse(datetime));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       
        lmsUserLogUserRepository.save(loguser);
		}// Code Edited By Bhuvana - 31-03-2021 END //
		
		
	/*	int ret = masterServiceImpl.SessionIdUpdate(sessionid,authentication.getName());
		if(ret==1)
		{
			return maView = new ModelAndView("redirect:/alreadyLogged");
		}
		if(ret==2)
		{
			return maView = new ModelAndView("redirect:/deactivated");
		}*/
		
		
		return maView;
	}
	
	@RequestMapping(value = "/alreadyLogged")
	public String alreadyLogged(Authentication authentication) {
		return "alreadyLogged"; 
	}
	 @RequestMapping(value = "/searchlmsuser")
	 
	public ModelAndView searchlmsuser(Authentication authentication) {
		ModelAndView maView = new ModelAndView("searchlmsuser");
		String usrRole=masterServiceImpl.getUserRole(authentication);
		
		return maView;
	}
	@PostMapping("/searchuser")
	public ModelAndView searchuser(@Valid @ModelAttribute("user") final SearchLmsUserBean searchLmsUserBean, final BindingResult result, final ModelMap model,Authentication authentication) {
		ModelAndView maView = new ModelAndView("user");
		SearchLmsUserRequest searchLmsUserRequest = new SearchLmsUserRequest();
		System.out.println("Here Search");
		
		
		UserMaster UserMasterList = masterServiceImpl.searchCode(searchLmsUserRequest);
		//ModelAndView modelAndView = new ModelAndView("lmsuserlist");
		maView.addObject("UserMasterList", UserMasterList);
				
		return maView;
		
	}
	@RequestMapping(value = "/usernotfound")
	public String usernotfound(Authentication authentication) {
		return "usernotfound"; 
	}
	
	@RequestMapping(value = "/deactivated")
	public String deactivated(Authentication authentication) {
		return "deactivated"; 
	}
	
	/*@RequestMapping(value = "/logout")
	public ModelAndView Logoff(Authentication authentication) {
		log.info("Logg-off : " + authentication.getName());
		ModelAndView maView = new ModelAndView("loggedoff");
		String usrRole=masterServiceImpl.getUserRole(authentication);
		log.info("Logg-off : " + authentication.getName());
		return maView;
	}*/
	
	/*@PostMapping(value = "/loggedoff")
	public ModelAndView loggedoff (@Valid @ModelAttribute("loggedoff") final UpdateSessionBean updateLmsUserBean , final BindingResult result, final ModelMap model,Authentication authentication)
	{
		ModelAndView maView = new ModelAndView("redirect:/loggedoff");
		SessionRequestDTO sessionRequestDTO = new SessionRequestDTO();
		
		
		
		sessionRequestDTO.setCode(authentication.getName());
		String usrRole=masterServiceImpl.getUserRole(authentication);
		log.info("Logg-off : " + authentication.getName());
						
		sessionRequestDTO.setSessionID("");
		
		log.info("insertlmsuser result : " + sessionRequestDTO);
		masterServiceImpl.Logoff(sessionRequestDTO);
			
		return maView;
		
	}*/
	
	
	
	
}
