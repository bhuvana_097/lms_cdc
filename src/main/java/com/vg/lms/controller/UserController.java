package com.vg.lms.controller;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.vg.lms.bean.SearchLmsUserBean;
import com.vg.lms.bean.UpdateLmsUserBean;
import com.vg.lms.repository.UserMasterRepository;
import com.vg.lms.request.AddLmsUserRequestDTO;
import com.vg.lms.service.MasterService;

@Controller
public class UserController {

	private final Logger log = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	UserMasterRepository userRepos;
	
	@RequestMapping(value = "/searchlmsuserresult")
	public String searchLmsUserResult() {
		return "searchlmsuserresult"; 
	}
	@Autowired
	private MasterService masterServiceImpl;

	@PostMapping("/srchuserresult")
	public String retrieveUsersrch(@Valid @ModelAttribute("searchlmsuserresult") final SearchLmsUserBean searchlmsuserresult,final BindingResult result,final ModelMap model)
	{
		SearchLmsUserBean UserMasterList =new SearchLmsUserBean();
		log.info("search lmsuser: " + searchlmsuserresult.getCode());
		 UserMasterList = masterServiceImpl.lmsUserSrchList(searchlmsuserresult.getCode());
		
			model.addAttribute("searchlmsuserresult", UserMasterList);
			
		log.info("lmsuser result list controller: " + UserMasterList.getName());
		if(UserMasterList.getCheck().equals("y"))
		{
			return "searchlmsuserresult";
		}else {
			return "usernotfound";
		}
			
	}

	@RequestMapping(value = "/searchlmsuser", method = RequestMethod.GET)
	public ModelAndView lmsuseredit(Authentication authentication) {
		ModelAndView maView = new ModelAndView("searchlmsuser");
		String usrRole=masterServiceImpl.getUserRole(authentication);
		if(masterServiceImpl.getUserAccess(usrRole, "AddLmsUser")){
			//return "lmsuseredit"; 
		}else{
			maView = new ModelAndView("redirect:/accessDenied");
		}
		return maView;
	}
	
	
//	@PostMapping("/srchuserresult")
//	public String retrieveUsersrch(@Valid @ModelAttribute("searchlmsuserresult") final UpdateLmsUserBean searchlmsuserresult,final BindingResult result,final ModelMap model)
//	{
//		log.info("search lmsuser: " + searchlmsuserresult.getLmsusercode());
//		UpdateLmsUserBean UserMasterList = masterServiceImpl.lmsUserSrchList(searchlmsuserresult.getLmsusercode());
//			model.addAttribute("searchlmsuserresult", UserMasterList);
//			//model.addAttribute("code", new String(searchlmsuserresult.getLmsusercode()));
//			
//		log.info("lmsuser result list controller: " + UserMasterList.getLmsusername());
//			return "searchlmsuserresult";
//		
//	}
//	
	
	
}
