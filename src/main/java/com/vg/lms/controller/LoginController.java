package com.vg.lms.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.ResourceUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.vg.lms.bean.LoginBean;
import com.vg.lms.model.SessionDetails;
import com.vg.lms.model.UserMaster;
import com.vg.lms.repository.UserMasterRepository;
import com.vg.lms.service.MasterService;

@Controller
@Scope("session")
@SessionAttributes("SessionDetails")
public class LoginController {
	
	@Autowired
	private UserMasterRepository userMasterRepository;
	
	@Autowired
	private MasterService masterServiceImpl;
	
	private final Logger log = LoggerFactory.getLogger(LoginController.class);
	
	/*@RequestMapping(value = "/login",method=RequestMethod.GET)
	public ModelAndView dispayLogin() {
		ModelAndView model = new ModelAndView("login");
		Login loginBean = new Login();
		model.addObject("loginBean", loginBean);
		return model;
	}*/
	
	/*@RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView showForm() {
        return new ModelAndView("login", "login", new LoginBean());
    }*/
	
	@PostMapping("/performlogin")
    public ModelAndView submit(@Valid @ModelAttribute("performlogin") final LoginBean performlogin, final BindingResult result, final ModelMap model) {
		log.info("performLogin" + performlogin.toString());
		UserMaster userMaster = userMasterRepository.findBycode(performlogin.getUserName());
		log.info("UserStatus" + userMaster.getStatus());
		ModelAndView modelAndView = new ModelAndView();
		log.info("PERFORM LOGIN DETAILS");
		if(userMaster!=null && performlogin.getUserName().equals(userMaster.getCode())&& performlogin.getPassword().equals(userMaster.getPasswd())&& userMaster.getStatus().equals("Active")){
			modelAndView =  new ModelAndView("/list");
			SessionDetails sessionDetails = new SessionDetails();
			sessionDetails.setUserName(userMaster.getCode());
			sessionDetails.setPassword(userMaster.getPasswd());
			
			modelAndView.addObject("sessionDetails", sessionDetails);
			
			return modelAndView;
       }else{
    	   modelAndView =  new ModelAndView("/login");
    	   
    	   return modelAndView;
       }
    }
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model, String error, String logout) {
	
							
		if (error != null)
			model.addAttribute("errorMsg", "Your username and password are invalid.");

		if (logout != null) {
			
			return "loggedoff";
		}
								
			return "login";
	}
	
	
	
	
	
	
	@RequestMapping(value = "/performLogin",method=RequestMethod.POST)
	public ModelAndView performlogin(@ModelAttribute("login") LoginBean login) {
		System.out.println("####################Inside Login#########################"+login.getUserName());
		ModelAndView model = new ModelAndView("login");	
		LoginBean loginBean = new LoginBean();
		model.addObject("loginBean", loginBean);
		return model;
	}
	
	@RequestMapping(value = "/logout")
	public ModelAndView Logoff(Authentication authentication) {
		log.info("Logg-off : " + authentication.getName());
		ModelAndView maView = new ModelAndView("loggedoff");
		String usrRole=masterServiceImpl.getUserRole(authentication);
		log.info("Logg-off : " + authentication.getName());
		return maView;
	}


}
