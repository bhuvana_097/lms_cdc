package com.vg.lms.controller;

import java.text.ParseException;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.vg.lms.bean.AddLmsUserBean;
import com.vg.lms.bean.CrDrBean;
import com.vg.lms.bean.LoanBean;
import com.vg.lms.request.GetLoanRequestDTO;
import com.vg.lms.request.GetloanIdRequestDTO;
import com.vg.lms.request.LoanRequestDTO;
import com.vg.lms.request.RemitRequestDTO;
import com.vg.lms.response.BaseResponseDTO;
import com.vg.lms.response.LoanResponseDTO;
import com.vg.lms.response.LoanWaiveAndCloseResponseDTO;
import com.vg.lms.service.LoanService;
import com.vg.lms.service.RemitanceService;

@RestController
public class LoanController {
	
	@Autowired
	private LoanService loanService;	
	
	@Autowired
	private RemitanceService remitanceService;
	
	private final Logger log = LoggerFactory.getLogger(LoanController.class);
	//private static final Logger log = Logger.getLogger(LoanController.class);	
	
	/*@RequestMapping(value="/getLoan",method=RequestMethod.POST, consumes={"application/json"})
	public @ResponseBody LoanResponseDTO getLoan(@RequestBody GetLoanRequestDTO getLoanRequestDTO,Authentication authentication){			
		log.debug("Entering LoanController" );	
		log.info("Entering LoanController" );	
		
		LoanResponseDTO response = loanService.getLoan(getLoanRequestDTO);		
		log.debug(" LoanController getLoan Response:" + response);
		return 	response;
	}
	*/
	
	@RequestMapping(value="/addLoan",method=RequestMethod.POST, consumes={"application/json"})
	public @ResponseBody LoanResponseDTO addLoan(@RequestBody LoanRequestDTO loanRequestDTO) throws ParseException{			
		log.debug("Entering LoanController addLoan" );		
		LoanResponseDTO response = loanService.addLoan(loanRequestDTO);
		log.debug(" LoanController addLoan Response:" + response);
		return 	response;
	}
	
	@RequestMapping(value="/doRemittance",method=RequestMethod.POST, consumes={"application/json"})
	public @ResponseBody BaseResponseDTO doRemittance(@RequestBody RemitRequestDTO remitRequestDTO) throws ParseException{			
		log.debug("Entering LoanController do remittance" );		
		BaseResponseDTO response = remitanceService.updateRemitance(remitRequestDTO);
		log.debug("LoanController remittance Response:" + response);
		return 	response;
	}
	
	/*@RequestMapping("/cancelLoan")
	public int cancelloan(@RequestParam("id") int id)
	//public ModelAndView cancelloan(@Valid @ModelAttribute("loan") final LoanBean loan,@RequestParam("id") int id, final BindingResult result, final ModelMap model) {

	{
		log.debug("Entering LoanController cancel loan" );	
		int i = loanService.cancelLoan(id);
		return i;
		//return new ModelAndView("redirect:/getloan?id="+id);
		//return "loan";
	}*/
	@RequestMapping("/cancelLoan")
	public ModelAndView disburseLoan(@ModelAttribute("loan") final LoanBean loan,@RequestParam("id") int id,@RequestParam("reason") String reason, final BindingResult result, final ModelMap model,Authentication authentication)
	{
		String uname = authentication.getName();
		loanService.cancelLoan(id,reason,uname);
		return new ModelAndView("redirect:/getloan?id="+id);
		//return "loan";
	}
	
	@RequestMapping(value="/waiveAndClose")
	public ModelAndView waiveAndCloseLoan(@RequestParam("id") int id, @RequestParam("loanNo") String loanNo, @RequestParam("waive_password") String waivePassword,Authentication authentication)
	{
		LoanWaiveAndCloseResponseDTO response = new LoanWaiveAndCloseResponseDTO();
		response = loanService.waiveAndCloseLoan(loanNo,waivePassword,authentication.getName());
		log.debug("LoanController waiveAndClose Response:" + response.toString());
		log.info("LoanController waiveAndClose Response:" + response.toString());
		ModelAndView maView = new ModelAndView("redirect:/getloan?id="+id);
		maView.addObject("waiveAndCloseMessage", response.getMessage());
		return maView;
	}
	
	
	@ExceptionHandler({Exception.class })
	public @ResponseBody BaseResponseDTO errorHandling(final Exception exp) {
		log.error("Exception occured in LoanController : ",exp);
		BaseResponseDTO responseDTO=new BaseResponseDTO();
		responseDTO.setStatusCode("0");
		responseDTO.setStatusMessage(exp.getMessage());
		return responseDTO;		
	}

	// for CRM - code edited by Bhuvana 16-04-2021
	
	@RequestMapping(value="/getLoanData",method=RequestMethod.POST,consumes={"application/json"} )
	public @ResponseBody String getloanData(@RequestBody GetloanIdRequestDTO loanNo) throws JsonProcessingException{		
		log.debug("Entering getloanData"+loanNo);
		String response = loanService.getLoanData(loanNo.getLoanNo());
		return response;
	}

	// for CRM - code edited by Bhuvana 16-04-2021
	
	@RequestMapping(value="/getCustomerLoanData",method=RequestMethod.POST,consumes={"application/json"} )
	public @ResponseBody String getcustomerloanData(@RequestBody GetloanIdRequestDTO loanNo) throws JsonProcessingException{		
		log.debug("Entering getloanData"+loanNo);
		String response = loanService.getLoanandCustomerData(loanNo.getLoanNo());
		return response;
	}	

	
}
