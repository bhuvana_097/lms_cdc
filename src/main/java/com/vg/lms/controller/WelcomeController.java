package com.vg.lms.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.vg.lms.bean.AssetBean;
import com.vg.lms.bean.CrDrBean;
import com.vg.lms.bean.LMSRegistryBean;
import com.vg.lms.bean.ListSOABean;
import com.vg.lms.bean.LoanBean;
import com.vg.lms.bean.SOABean;
import com.vg.lms.bean.WaiverSplitBean;
import com.vg.lms.docgen.GenerateBLWelcomeLetterPDF;
import com.vg.lms.docgen.GenerateForeClosurePDF;
import com.vg.lms.docgen.GenerateRepaymentSchedulePDF;
import com.vg.lms.docgen.GenerateSOAPDF;
import com.vg.lms.docgen.GenerateWelcomeLetterPDF;
import com.vg.lms.model.Asset;
import com.vg.lms.model.LMSRegistryModel;
import com.vg.lms.model.Loan;
import com.vg.lms.model.UserMaster;
import com.vg.lms.repository.AssetRepository;
import com.vg.lms.repository.LMSRegistryRepository;
import com.vg.lms.repository.LoanRepository;
import com.vg.lms.repository.TableMetaRepository;
import com.vg.lms.repository.UserMasterRepository;
//import com.vg.lms.service.IncredServiceImpl;
import com.vg.lms.service.LoanService;
import com.vg.lms.service.Mailer;
import com.vg.lms.service.SOADetailsService;


@Controller
public class WelcomeController {

	@Autowired
	private LoanService loanService;	
	
	@Autowired
	private UserMasterRepository usermstRepo;
	
	@Autowired
	private TableMetaRepository tableMeta;
	
	@Autowired
	private GenerateWelcomeLetterPDF generateWelcomeLetterPDF;
	
	@Autowired
	private GenerateRepaymentSchedulePDF generateRepaymentSchedulePDF;
	
	@Autowired
	private GenerateForeClosurePDF generateForeClosurePDF;
	
	@Autowired
	private GenerateSOAPDF generateSOAPDF;
	
	@Autowired
	private LoanRepository loanRepository;
	
	@Autowired
	private Mailer mailer;
	
	@Autowired
	private AssetRepository assetRepository;
	
	@Autowired
	private SOADetailsService fetchSOADetails;
	
	@Autowired
	private LMSRegistryRepository lmsRegistryRepository;
	
	@Autowired
	private GenerateBLWelcomeLetterPDF generateBlWelcomeLetterPDF;
  	
	
	private final Logger log = LoggerFactory.getLogger(WelcomeController.class);	

	@RequestMapping(value = "/greet")
	public String greetPage() {
		return "webview"; 
	}

	@RequestMapping(value = "/list")
	public String listView(final ModelMap model,Authentication authentication) {
		
		return "list"; 
	}
	
	@RequestMapping(value = "/edit")
	public ModelAndView edit(@RequestParam("pkval") int id,@RequestParam("tn") String tn) {
		
		String tableName = tableMeta.findBytableNo(tn).getTableName();
		if(tableName.equals("lms_loan"))
			return new ModelAndView("redirect:/getloan?id="+id);
		else if(tableName.equals("lms_registry"))
			return new ModelAndView("redirect:/getRegistry?id="+id);
		else
			return new ModelAndView("redirect:/list?tn="+tn);

			
		//return "loan"; 
	}

	@RequestMapping(value = "/report")
	public String report() {
		return "excel"; 
	}

	@RequestMapping(value = "/loan")
	public String loan() {
		return "loan"; 
	}

	/*@RequestMapping(value = "/disbLoan")
	public String loan(@RequestParam("id") int id) {
	    LoanBean loanBean = loanService.getLoan(id) ;

	    return "loan";
	}*/

	@RequestMapping("/disbLoan")
	public ModelAndView disburseLoan(@ModelAttribute("loan") final LoanBean loan, final BindingResult result, final ModelMap model) throws NumberFormatException, JsonProcessingException
	{
		loanService.disburseLoan(Integer.parseInt(loan.getLoanId()));
		return new ModelAndView("redirect:/getloan?id="+loan.getLoanId());
		//return "loan";
	}
	
	/*@RequestMapping("/genwl")
	public ModelAndView genwl(@ModelAttribute("loan") final LoanBean loan, final BindingResult result, final ModelMap model, HttpServletResponse response) throws IOException
	{
		//loanService.disburseLoan(Integer.parseInt(loan.getLoanId()));
		//GenerateWelcomeLetterPDF welcomePDF = new GenerateWelcomeLetterPDF();
		//String fileName = welcomePDF.generateWelcomeLetter();
		String fileName = "/FRNDLN/docs/WelcomeLetter.pdf";
		System.out.println(fileName);
	       response.setContentType("application/pdf");
	       response.setHeader("Content-Disposition", "attachment; filename=\"WelcomeLetter.pdf\"");
	       InputStream inputStream = new FileInputStream(new File(fileName));
	           int nRead;
	           while ((nRead = inputStream.read()) != -1) {
	               response.getWriter().write(nRead);
	           }
		return new ModelAndView("redirect:/getloan?id="+loan.getLoanId());
		//return "loan";
	}*/
	
	@RequestMapping(value = "/genwl", produces = "application/pdf")
    public @ResponseBody void genwl(HttpServletResponse response, @ModelAttribute("loan") final LoanBean loan, final BindingResult result, final ModelMap model) throws IOException {
		//GenerateWelcomeLetterPDF welcomePDF = new GenerateWelcomeLetterPDF();
		String fileName = generateWelcomeLetterPDF.generateWelcomeLetter(Integer.parseInt(loan.getLoanId()));
		File file = new File(fileName);
		//File file1 = new File("/FRNDLN/docs/WelcomeLetter.pdf");
        InputStream in = new FileInputStream(file);

        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
        response.setHeader("Content-Length", String.valueOf(file.length()));
        FileCopyUtils.copy(in, response.getOutputStream());
    }
	
	@RequestMapping(value = "/genrepaySchedule", produces = "application/pdf")
    public @ResponseBody void genrepaySchedule(HttpServletResponse response, @ModelAttribute("loan") final LoanBean loan, final BindingResult result, final ModelMap model) throws IOException {
		//GenerateWelcomeLetterPDF welcomePDF = new GenerateWelcomeLetterPDF();
		String fileName = generateRepaymentSchedulePDF.generateRepaymentSchedule(Integer.parseInt(loan.getLoanId()));
		File file = new File(fileName);
		//File file = new File("/FRNDLN/docs/WelcomeLetter.pdf");
        InputStream in = new FileInputStream(file);

        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
        response.setHeader("Content-Length", String.valueOf(file.length()));
        FileCopyUtils.copy(in, response.getOutputStream());
    }
	
	@RequestMapping(value = "/genForeClosure", produces = "application/pdf")
    public @ResponseBody void genForeClosure(HttpServletResponse response, @ModelAttribute("loan") final LoanBean loan, final BindingResult result, final ModelMap model) throws IOException {
		//GenerateWelcomeLetterPDF welcomePDF = new GenerateWelcomeLetterPDF();
		String fileName = generateForeClosurePDF.generateForeClosureDoc(Integer.parseInt(loan.getLoanId()));
		File file = new File(fileName);
		//File file = new File("/FRNDLN/docs/WelcomeLetter.pdf");
        InputStream in = new FileInputStream(file);

        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
        response.setHeader("Content-Length", String.valueOf(file.length()));
        FileCopyUtils.copy(in, response.getOutputStream());
    }
	
	@RequestMapping(value = "/genSOA", produces = "application/pdf")
    public @ResponseBody void genSOA(HttpServletResponse response, @ModelAttribute("loan") final LoanBean loan, final BindingResult result, final ModelMap model) throws IOException {
		//GenerateWelcomeLetterPDF welcomePDF = new GenerateWelcomeLetterPDF();
		String fileName = generateSOAPDF.generateSOA(Integer.parseInt(loan.getLoanId()));
		File file = new File(fileName);
		//File file = new File("/FRNDLN/docs/WelcomeLetter.pdf");
        InputStream in = new FileInputStream(file);

        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
        response.setHeader("Content-Length", String.valueOf(file.length()));
        FileCopyUtils.copy(in, response.getOutputStream());
    }
	@RequestMapping(value = "/genSOANew", produces = "application/pdf")
    public @ResponseBody void genSOANew(HttpServletResponse response, @RequestParam("loanId") String loanId, final BindingResult result, final ModelMap model) throws IOException {
		//GenerateWelcomeLetterPDF welcomePDF = new GenerateWelcomeLetterPDF();
		String fileName = generateSOAPDF.generateSOA(Integer.parseInt(loanId));
		File file = new File(fileName);
		//File file = new File("/FRNDLN/docs/WelcomeLetter.pdf");
        InputStream in = new FileInputStream(file);

        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
        response.setHeader("Content-Length", String.valueOf(file.length()));
        FileCopyUtils.copy(in, response.getOutputStream());
    }
	
	
	@RequestMapping("/mailwl")
	public ModelAndView mailWL(@ModelAttribute("loan") final LoanBean loan, final BindingResult result, final ModelMap model)
	{
		Loan loanDetails = loanRepository.findById(Integer.parseInt(loan.getLoanId())).get() ;
		String mailMessage = "<html>"
				+ "<body> "
				+"<p>Dear "+ loanDetails.getCustomer().getName() +",</p>"
				+"<p>Please find attached your welcome Letter. Please go through and revert for any queries</p>"
				+"<p>Thanks &amp; Regards,</p>"
				+"<p>Customer Support, <br />Orange Retail Finance LImited Pvt Ltd. <br /></p>"
				+"</body>"
				+"</html>";

		mailer.sendMessageWithAttachment("mahendran.ks@orangeretailfinance.com", "Welcome to Orange Retail Finance", mailMessage, "/FRNDLN/docs/WelcomeLetter.pdf", "WelcomeLetter");
		return new ModelAndView("redirect:/getloan?id="+loan.getLoanId());
		//return "loan";
	}
	
	@RequestMapping("/mailrepaySchedule")
	public ModelAndView mailrepaySchedule(@ModelAttribute("loan") final LoanBean loan, final BindingResult result, final ModelMap model)
	{
		Loan loanDetails = loanRepository.findById(Integer.parseInt(loan.getLoanId())).get() ;
		String mailMessage = "<html>"
				+ "<body> "
				+"<p>Dear "+ loanDetails.getCustomer().getName() +",</p>"
				+"<p>Please find attached your Repayment Schedule. Please go through and revert for any queries</p>"
				+"<p>Thanks &amp; Regards,</p>"
				+"<p>Customer Support, <br />Orange Retail Finance LImited Pvt Ltd. <br /></p>"
				+"</body>"
				+"</html>";

		mailer.sendMessageWithAttachment("mahendran.ks@orangeretailfinance.com", "Welcome to Orange Retail Finance", mailMessage, "/FRNDLN/docs/RepaymentSchedule.pdf", "Repayment Schedule");
		return new ModelAndView("redirect:/getloan?id="+loan.getLoanId());
		//return "loan";
	}
	
	@RequestMapping("/mailForeClosure")
	public ModelAndView mailForeClosure(@ModelAttribute("loan") final LoanBean loan, final BindingResult result, final ModelMap model)
	{
		Loan loanDetails = loanRepository.findById(Integer.parseInt(loan.getLoanId())).get() ;
		String mailMessage = "<html>"
				+ "<body> "
				+"<p>Dear "+ loanDetails.getCustomer().getName() +",</p>"
				+"<p>Please find attached your Repayment Schedule. Please go through and revert for any queries</p>"
				+"<p>Thanks &amp; Regards,</p>"
				+"<p>Customer Support, <br />Orange Retail Finance LImited Pvt Ltd. <br /></p>"
				+"</body>"
				+"</html>";

		mailer.sendMessageWithAttachment("mahendran.ks@orangeretailfinance.com", "Welcome to Orange Retail Finance", mailMessage, "/FRNDLN/docs/ForeClosure.pdf", "Foreclosure Document");
		return new ModelAndView("redirect:/getloan?id="+loan.getLoanId());
		//return "loan";
	}



	@RequestMapping("/getloan")
	public String getLoan(@Valid @ModelAttribute("loan") final LoanBean loan,@RequestParam("id") int id, final BindingResult result, final ModelMap model,Authentication authentication) {
		log.info("entering getloan controller");
		String usrRole="";
		String uname="";
		for (GrantedAuthority authority : authentication.getAuthorities()) {
		     String role = authority.getAuthority();
                usrRole= role;
                uname = authentication.getName();
                log.info("role==>"+usrRole);
                log.info("uname==>"+uname);
		}
		UserMaster usm=usermstRepo.findBycode(uname);
		//System.out.println(usm.getBranch());



		LoanBean loanBean = loanService.getLoan(id,usrRole,uname) ;
		
		
		model.addAttribute("loan", loanBean);
		model.addAttribute("id", new Integer(id));
		return "loan";
	}

	@RequestMapping(value = "/addCrDrNote")
	public ModelAndView showForm(@Valid @ModelAttribute("crdrnote") final CrDrBean crdrBean,@RequestParam("id") int id, final BindingResult result, final ModelMap model) {

		if(id >0)
			model.addAttribute("loanid", id);
		model.addAttribute("waiverType", "Principal,Interest,Charges Due,Penal Interest,fcl,brok per interest,moratorium,CovidMoratorium");	
		
		return new ModelAndView("crdr", "crdrnote", new CrDrBean());
	}
	
	@RequestMapping(value = "/fetchSOA")
	public ModelAndView showSOA(@Valid @ModelAttribute("SOA") final SOABean soabean,@RequestParam("id") int id, final BindingResult result, final ModelMap model,Authentication authentication) {
		ListSOABean listSOABean = new ListSOABean();
		String usrRole="";
		String uname="";
		for (GrantedAuthority authority : authentication.getAuthorities()) {
		     String role = authority.getAuthority();
                usrRole= role;
                uname = authentication.getName();
                log.info("role==>"+usrRole);
                log.info("uname==>"+uname);
		}
		LoanBean loanBean = loanService.getLoan(id,usrRole,uname) ;
		model.addAttribute("loan", loanBean);
		if(id >0)
			model.addAttribute("loanid", id);
		List<SOABean> soaList = fetchSOADetails.fetchSOADetails(id);
		log.info("entering showSOA controller");
		listSOABean.setSoaList(soaList);
		
		model.addAttribute("loan", loanBean);
		return new ModelAndView("SOA", "listSOABean", listSOABean);
	}

	@RequestMapping("/saveCrDrNote")
	public ModelAndView saveNote(@Valid @ModelAttribute("crdrnote") final CrDrBean crdrBean, final BindingResult result, final ModelMap model) {
		int noteId = loanService.addCrDrNote(crdrBean);
		/*LoanBean loanBean = loanService.getLoan(crdrBean.getLoanId()) ;
		model.addAttribute("loan", loanBean);*/
		model.addAttribute("noteid", noteId);
		//return "loan";
		return new ModelAndView("redirect:/getloan?id="+crdrBean.getLoanId());

	}
	//Code edited by Bhuvana for Auditlogin
	@RequestMapping("/updateRegistry")
	public ModelAndView updateRegistry(@Valid @ModelAttribute("registry") final LMSRegistryBean regiBean, final BindingResult result, final ModelMap model,Authentication authentication) {
		ModelAndView maView = new ModelAndView("redirect:/list?tn=279");
		LMSRegistryModel lmsRegistryModel = new LMSRegistryModel();// = lmsRegistryRepository.findByRegistryType(String.valueOf(regiBean.getId()));
		//lmsRegistryModel.setRegistryValue(registryValue);
		//LMSRegistryBean lmsRegistryBean = new LMSRegistryBean();
		
		//return new ModelAndView("config", "registry", new LMSRegistryBean());
		String usrRole="";
		String uname="";
		for (GrantedAuthority authority : authentication.getAuthorities()) {
		     String role = authority.getAuthority();
                usrRole= role;
                uname = authentication.getName();
                log.info("role==>"+usrRole);
                log.info("uname==>"+uname);
		}
		if(uname.equals("Akshata")) {
			maView = new ModelAndView("redirect:/accessDenied");
		}else {
			lmsRegistryModel.setId(Integer.parseInt(regiBean.getId()));
			lmsRegistryModel.setRegistryType(regiBean.getConfig_key());
			lmsRegistryModel.setRegistryValue(regiBean.getConfig_value());
			
			lmsRegistryRepository.save(lmsRegistryModel);	
		maView= new ModelAndView("redirect:/list?tn=279");
		}
		return maView;//279 static for this list 

	}
	
	@RequestMapping("/getRegistry")
	public ModelAndView getRegistry(@Valid @ModelAttribute("registry") final LMSRegistryBean regiBean, @RequestParam("id") int id,final BindingResult result, final ModelMap model,Authentication authentication) {
		ModelAndView maView = new ModelAndView("config");
		LMSRegistryModel lmsRegistryModel = lmsRegistryRepository.findById(id);
		LMSRegistryBean lmsRegistryBean = new LMSRegistryBean();
		lmsRegistryBean.setId(String.valueOf(lmsRegistryModel.getId()));
		lmsRegistryBean.setConfig_key(lmsRegistryModel.getRegistryType());
		lmsRegistryBean.setConfig_value(lmsRegistryModel.getRegistryValue());
		String usrRole="";
		String uname="";
		for (GrantedAuthority authority : authentication.getAuthorities()) {
		     String role = authority.getAuthority();
                usrRole= role;
                uname = authentication.getName();
                log.info("role==>"+usrRole);
                log.info("uname==>"+uname);
		}
		if(uname.equals("Akshata")) {
			maView = new ModelAndView("redirect:/accessDenied");
		}else {
		maView= new ModelAndView("config", "registry", lmsRegistryBean);
		}
		return maView;
		//return new ModelAndView("redirect:/getloan?id="+regiBean.getId());

	}

	
	@RequestMapping(value = "/addPDD")
	public ModelAndView showPDDForm(@Valid @ModelAttribute("assetBean") final AssetBean assetBean,@RequestParam("id") int id, final BindingResult result, final ModelMap model) {

		if(id >0)
			model.addAttribute("loanid", id);
		
		Loan loan = loanRepository.findById(id).get();
		Asset asset = loan.getAsset();
		/*model.addAttribute("notetype", "cr,dr");		
		Map<String,List<CrDrMasterBean>> noteMap = loanService.getCrDrNoteMaster();
		model.addAttribute("noteMap",noteMap);*/
		//model.addAttribute(attributeValue)
		AssetBean assetData = new AssetBean();
		assetData.setLoanId(asset.getLoan().getId());
		assetData.setLoanNo(asset.getLoan().getLoanNo());
		assetData.setMake(asset.getMake());
		assetData.setModel(asset.getModel());
		//assetData.setRc(false);
		if(asset.isKeyAvailable())
			assetData.setKey("Yes");
		else
			assetData.setKey("No");
		
		if(asset.isRcAvailable())
			assetData.setRc("Yes");
		else
			assetData.setRc("No");
		
		//assetData.setKey(false);
		assetData.setChassisNo(asset.getChassisNo());
		assetData.setRegNo(asset.getRegNo());
		assetData.setEngineNo(asset.getEngineNo());
		
		assetData.setRcNo(asset.getRcNo());
		assetData.setInsNo(asset.getInsNo());
		assetData.setInsProvider(asset.getInsProvider());
		//model.addAttribute("asset",assetData);
		return new ModelAndView("pdd", "assetBean", assetData);
	}

	@RequestMapping("/updatePDD")
	public ModelAndView updatePDD(@Valid @ModelAttribute("assetBean") final AssetBean assetBean, final BindingResult result, final ModelMap model) {
		//int noteId = loanService.addCrDrNote(crdrBean);
		/*LoanBean loanBean = loanService.getLoan(crdrBean.getLoanId()) ;
		model.addAttribute("loan", loanBean);*/
		//model.addAttribute("noteid", noteId);
		//return "loan";
		System.out.println(assetBean);
		Loan loan = loanRepository.findById(assetBean.getLoanId()).get();
		Asset asset = loan.getAsset();
		
		List<Asset> pendingtxn = assetRepository.findByengineNo(assetBean.getEngineNo());
		List<Asset> pendingtxn1 = assetRepository.findBychassisNo(assetBean.getChassisNo());
		
		log.info("DeDupe" + asset.getId() + "ENGINE" + asset.getEngineNo());
		if(asset.getChassisNo() !=null && asset.getEngineNo() !=null)
		if(!(asset.getEngineNo().isEmpty()))
		for(Asset asset1 : pendingtxn)
		{
					
			log.info("DeDupe" + asset.getId() + "ENGINE" + assetBean.getEngineNo() + "LOAN" + asset1.getId());
			if(assetBean.getKey().equals("Yes"))
				asset.setKeyAvailable(true);
			else
				asset.setKeyAvailable(false);
				
			if(assetBean.getRc().equals("Yes"))
				asset.setRcAvailable(true);
			else
				asset.setRcAvailable(false);
			
			asset.setRcNo(assetBean.getRcNo());
			asset.setInsNo(assetBean.getInsNo());
			asset.setInsProvider(assetBean.getInsProvider());
			asset.setRegNo(assetBean.getRegNo());
			
			loan.setAsset(asset);
			loanRepository.save(loan);
			
			return new ModelAndView("redirect:/getdedupe?id="+assetBean.getLoanId());
		}
		if(asset.getChassisNo() !=null && asset.getEngineNo() !=null)
		if(!(asset.getChassisNo().isEmpty()))
		for(Asset asset2 : pendingtxn1)
		{
					
			log.info("DeDupe" + asset.getId() + "Chassis" + assetBean.getEngineNo() + "LOAN" + asset2.getId());
			if(assetBean.getKey().equals("Yes"))
				asset.setKeyAvailable(true);
			else
				asset.setKeyAvailable(false);
				
			if(assetBean.getRc().equals("Yes"))
				asset.setRcAvailable(true);
			else
				asset.setRcAvailable(false);
			
			asset.setRcNo(assetBean.getRcNo());
			asset.setInsNo(assetBean.getInsNo());
			asset.setInsProvider(assetBean.getInsProvider());
			asset.setRegNo(assetBean.getRegNo());
			
			loan.setAsset(asset);
			loanRepository.save(loan);
			
			return new ModelAndView("redirect:/getdedupe?id="+assetBean.getLoanId());
		}
		asset.setEngineNo(assetBean.getEngineNo());
		asset.setChassisNo(assetBean.getChassisNo());
		if(assetBean.getKey().equals("Yes"))
			asset.setKeyAvailable(true);
		else
			asset.setKeyAvailable(false);
			
		if(assetBean.getRc().equals("Yes"))
			asset.setRcAvailable(true);
		else
			asset.setRcAvailable(false);
		
		asset.setRcNo(assetBean.getRcNo());
		asset.setInsNo(assetBean.getInsNo());
		asset.setInsProvider(assetBean.getInsProvider());
		asset.setRegNo(assetBean.getRegNo());
		
		/*Set<Asset> assets = new HashSet<>();
		assets.add(asset);
*/
		loan.setAsset(asset);
		loanRepository.save(loan);
		
		return new ModelAndView("redirect:/getloan?id="+assetBean.getLoanId());

	}

	/*@RequestMapping(value="/editLoan",params="stmnt",method=RequestMethod.POST)
    public void generateLoanStmnt()
    {
        System.out.println("Action1 block called");
    }*/

	@RequestMapping(value="/editLoan",params="noc",method=RequestMethod.POST)
	public void generateNOC()
	{
		System.out.println("Action2 block called");
	}
	
	
	@RequestMapping("/getdedupe")
	public String getDedupe(@Valid @ModelAttribute("loan") final LoanBean loan,@RequestParam("id") int id, final BindingResult result, final ModelMap model,Authentication authentication) {
		log.info("entering getloan controller");
		String usrRole="";
		String uname="";
		for (GrantedAuthority authority : authentication.getAuthorities()) {
		     String role = authority.getAuthority();
                usrRole= role;
                uname = authentication.getName();
                log.info("role==>"+usrRole);
                log.info("uname==>"+uname);
		}
		LoanBean loanBean = loanService.getLoan(id,usrRole,uname) ;
		model.addAttribute("loan", loanBean);
		model.addAttribute("id", new Integer(id));
		return "dedupe";
	}
	

	/*@RequestMapping("/getuser")
	public String getUser(@Valid @ModelAttribute("user") final LoanBean loan, final BindingResult result, final ModelMap model,Authentication authentication) {
		log.info("entering getloan controller");
		String usrRole="";
		String uname="";
		for (GrantedAuthority authority : authentication.getAuthorities()) {
		     String role = authority.getAuthority();
                usrRole= role;
                uname = authentication.getName();
                log.info("role==>"+usrRole);
                log.info("uname==>"+uname);
		}
		 Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
		    String email = loggedInUser.getName(); 
		LoanBean loanBean = loanService.getUser(usrRole,uname) ;
		log.info("Welome TEst:"+ loanBean.getUserName() + loggedInUser.getName());
		
		model.addAttribute("user", loanBean);
		
		return "userDetailsDisplay";
	}*/
	
	@RequestMapping(value = "/updateColender")
	public ModelAndView showColender(@Valid @ModelAttribute("loanBean") final LoanBean loanBean,@RequestParam("id") int id, final BindingResult result, final ModelMap model) {

		if(id >0)
			model.addAttribute("loanid", id);
		
		Loan loan = loanRepository.findById(id).get();
		
		
		System.out.println(loanBean);
		

		
		loan.setCoLender("");
		loanRepository.save(loan);

		return new ModelAndView("redirect:/getloan?id="+loanBean.getLoanId());

	}
	
	@RequestMapping(value = "/genwlbl", produces = "application/pdf")
    public @ResponseBody void genwlbl(HttpServletResponse response, @ModelAttribute("loan") final LoanBean loan, final BindingResult result, final ModelMap model) throws IOException {
		//GenerateWelcomeLetterPDF welcomePDF = new GenerateWelcomeLetterPDF();
		String fileName = generateBlWelcomeLetterPDF.generateBlWelcomeLetter(Integer.parseInt(loan.getLoanId()));
		File file = new File(fileName);
		//File file1 = new File("/FRNDLN/docs/WelcomeLetter.pdf");
        InputStream in = new FileInputStream(file);

        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
        response.setHeader("Content-Length", String.valueOf(file.length()));
        FileCopyUtils.copy(in, response.getOutputStream());
    }
	
	@RequestMapping("/waiveNote")
	public ModelAndView WaiveNote(@Valid @ModelAttribute("crdrnote") final WaiverSplitBean crdrBean, final BindingResult result, final ModelMap model) {
		System.out.println("Waiver New");
		int noteId = loanService.waiveCrDrNote(crdrBean);
		/*LoanBean loanBean = loanService.getLoan(crdrBean.getLoanId()) ;
		model.addAttribute("loan", loanBean);*/
		model.addAttribute("noteid", noteId);
		//return "loan";
		return new ModelAndView("redirect:/getloan?id="+crdrBean.getLoanId());

	}

	@RequestMapping(value = "/waiveCrDrNote")
	public ModelAndView showForm(@Valid  @ModelAttribute("crdrnote") final WaiverSplitBean crdrBean,@RequestParam("id") int id, final BindingResult result, final ModelMap model) {
		System.out.println("Waiver New 1");
		if(id >0)
			model.addAttribute("loanid", id);
		return new ModelAndView("WaiverTab", "crdrnote", new WaiverSplitBean());
	}


}
