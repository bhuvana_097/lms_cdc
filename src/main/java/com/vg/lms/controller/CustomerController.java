package com.vg.lms.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import com.coh.lms.IDFC.IdfcService;
import com.coh.lms.IDFC.MobileRequest;
import com.coh.lms.IDFC.MobileResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.vg.lms.request.COLRequestDTO;
import com.vg.lms.request.ClixRequest;
import com.vg.lms.request.CustomerDTO;
import com.vg.lms.request.GetCustomerIdRequestDTO;
import com.vg.lms.request.GetCustomerRequestDTO;
import com.vg.lms.request.LOSRequestDTO;
import com.vg.lms.response.BaseResponseDTO;
import com.vg.lms.response.CustomerResponseDTO;
import com.vg.lms.response.LOSResponseDTO;
import com.vg.lms.service.ColendCollectionDataPush;
import com.vg.lms.service.CustomerService;
import com.vg.lms.service.LoanService;


@RestController
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private LoanService loanService;
	
	private final Logger log = LoggerFactory.getLogger(CustomerController.class);
	//private static final Logger log = Logger.getLogger(CustomerController.class);
	
	@RequestMapping(value="/getGreet",method=RequestMethod.GET)
	public String  getGreetTest(){		
		log.debug("Entering GreetingController");
		return "Hello BlueMoon USERS";
	}
	
	@RequestMapping(value="/getCustomer",method=RequestMethod.POST, consumes={"application/json"})
	public @ResponseBody CustomerResponseDTO getCustomer(@RequestBody GetCustomerRequestDTO getCustomerRequestDTO){			
		log.debug("Entering CustomerController" );		
		CustomerResponseDTO response = customerService.getCustomer(getCustomerRequestDTO);		
		log.debug(" CustomerController getCustomer Response:" + response);
		return 	response;
	}
	
	
	@RequestMapping(value="/addCustomer",method=RequestMethod.POST, consumes={"application/json"})
	public @ResponseBody CustomerResponseDTO addCustomer(@RequestBody CustomerDTO customerRequestDTO){			
		log.debug("Entering CustomerController" );		
		CustomerResponseDTO response = customerService.addCustomer(customerRequestDTO);		
		log.debug(" CustomerController addCustomer Response:" + response);
		return 	response;
	}
	
	@RequestMapping(value="/pushToLMS",method=RequestMethod.POST, consumes={"application/json"})
	public @ResponseBody LOSResponseDTO saveLOSDetaails(@RequestBody LOSRequestDTO losRequestDTO) throws JsonProcessingException{			
				log.debug("Entering CustomerController" );		
		LOSResponseDTO response = customerService.pushLOSData(losRequestDTO);		
		log.debug(" CustomerController addCustomer Response:" + response);
		return 	response;
	}
	
	
	@RequestMapping(value="/pushToCol",method=RequestMethod.POST)
	public COLRequestDTO pushcol(@RequestBody COLRequestDTO colRequestDTO){			
				log.info("Entering CustomerController" +colRequestDTO.getCoLender());		
				ColendCollectionDataPush col = new ColendCollectionDataPush();	
				col.pushOldData(colRequestDTO.getCoLender());
		log.debug(" CustomerController addCustomer Response:" );
		return 	colRequestDTO;
		
	}
	
	
	// for CRM - code edited by Bhuvana 13-04-2021
		@RequestMapping(value="/getCustomerData",method=RequestMethod.POST,consumes={"application/json"} )
		public @ResponseBody String customerData(@RequestBody GetCustomerIdRequestDTO mobileNo) throws JsonProcessingException{		
			log.debug("Entering getCustomerData"+mobileNo);
			String response = customerService.getCustomerData(mobileNo.getPhone1());
			return response;
		}
		
		// for CRM - code edited by Bhuvana 04-05-2021	
		
		@RequestMapping(value="/getLoanDatawithMobileNo",method=RequestMethod.POST,consumes={"application/json"} )
		public @ResponseBody String getloanDatawithMobileNo(@RequestBody GetCustomerIdRequestDTO mobileNo) throws JsonProcessingException{		
			log.debug("Entering get phoneno"+mobileNo);
			String response = loanService.getCustomerDatawithmobileno(mobileNo.getPhone1());
			return response;
		}	
	
	
	@ExceptionHandler({Exception.class })
	public @ResponseBody BaseResponseDTO errorHandling(final Exception exp) {
		log.error("Exception occured in CustomerController : ",exp);
		BaseResponseDTO responseDTO=new BaseResponseDTO();
		responseDTO.setStatusCode("0");
		responseDTO.setStatusMessage(exp.getMessage());
		return responseDTO;		
	}
	
	
}
