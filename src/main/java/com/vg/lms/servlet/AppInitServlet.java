package com.vg.lms.servlet;
/*package com.fl.lms.servlet;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

*//**
 * Servlet implementation class AppInitServlet
 *//*
public class AppInitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(AppInitServlet.class);


	*//**
	 * @see HttpServlet#HttpServlet()
	 *//*
	public AppInitServlet() {
		super();
	}

	*//**
	 * @see Servlet#init(ServletConfig)
	 *//*
	public void init(ServletConfig config) throws ServletException {
		System.out.println("In application logger servlet");
		try {

			Properties log4jProps = new Properties();

			log4jProps.load(new FileInputStream("/FRNDLN/Config/lms_log4j.properties"));
			PropertyConfigurator.configure(log4jProps);
			System.out.println("Loaded log4j properties");

		} catch (IOException e) {
			System.out.println("IOException while loading log4j.properties "+e.getMessage());
			logger.error("IOException while loading log4j.properties",e);
		}

		logger.debug("Starting the system.");  

	}

	*//**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 *//*
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	*//**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 *//*
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	public void destroy()
	{

	}

}
*/