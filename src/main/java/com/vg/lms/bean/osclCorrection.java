package com.vg.lms.bean;

public class osclCorrection {
	private int id, loanId;
	
	private float dealerDisbursementAmount,emi,processingFee,scCharge;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getLoanId() {
		return loanId;
	}
	public void setLoanId(int loanId) {
		this.loanId = loanId;
	}
	public float getDealerDisbursementAmount() {
		return dealerDisbursementAmount;
	}
	public void setDealerDisbursementAmount(float dealerDisbursementAmount) {
		this.dealerDisbursementAmount = dealerDisbursementAmount;
	}
	public float getEmi() {
		return emi;
	}
	public void setEmi(float emi) {
		this.emi = emi;
	}
	public float getProcessingFee() {
		return processingFee;
	}
	public void setProcessingFee(float processingFee) {
		this.processingFee = processingFee;
	}
	public float getScCharge() {
		return scCharge;
	}
	public void setScCharge(float scCharge) {
		this.scCharge = scCharge;
	}
	@Override
	public String toString() {
		return "osclCorrection [id=" + id + ", loanId=" + loanId + ", dealerDisbursementAmount="
				+ dealerDisbursementAmount + ", emi=" + emi + ", processingFee=" + processingFee + ", scCharge="
				+ scCharge + ", getId()=" + getId() + ", getLoanId()=" + getLoanId()
				+ ", getDealerDisbursementAmount()=" + getDealerDisbursementAmount() + ", getEmi()=" + getEmi()
				+ ", getProcessingFee()=" + getProcessingFee() + ", getScCharge()=" + getScCharge() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}
	
	

}

