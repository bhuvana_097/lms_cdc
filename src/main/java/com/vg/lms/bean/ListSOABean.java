package com.vg.lms.bean;

import java.util.ArrayList;
import java.util.List;

public class ListSOABean {
	List<SOABean> soaList = new ArrayList<>();

	public List<SOABean> getSoaList() {
		return soaList;
	}

	public void setSoaList(List<SOABean> soaList) {
		this.soaList = soaList;
	}
	
	
}
