package com.vg.lms.bean;

public class AssetBean {
	private int id, loanId;
	private String make, model, engineNo, chassisNo,regNo,loanNo,key,rc,rcNo, insNo, insProvider ;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getLoanId() {
		return loanId;
	}
	public void setLoanId(int loanId) {
		this.loanId = loanId;
	}	
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getEngineNo() {
		return engineNo;
	}
	public void setEngineNo(String engineNo) {
		this.engineNo = engineNo;
	}
	public String getChassisNo() {
		return chassisNo;
	}
	public void setChassisNo(String chassisNo) {
		this.chassisNo = chassisNo;
	}
	
	public String getRegNo() {
		return regNo;
	}
	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}
	public String getLoanNo() {
		return loanNo;
	}
	public void setLoanNo(String loanNo) {
		this.loanNo = loanNo;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getRc() {
		return rc;
	}
	public void setRc(String rc) {
		this.rc = rc;
	}	
	public String getRcNo() {
		return rcNo;
	}
	public void setRcNo(String rcNo) {
		this.rcNo = rcNo;
	}
	public String getInsNo() {
		return insNo;
	}
	public void setInsNo(String insNo) {
		this.insNo = insNo;
	}
	public String getInsProvider() {
		return insProvider;
	}
	public void setInsProvider(String insProvider) {
		this.insProvider = insProvider;
	}
	
	
	@Override
	public String toString() {
		return "AssetBean [id=" + id + ", loanId=" + loanId + ", make=" + make + ", model=" + model + ", engineNo="
				+ engineNo + ", chassisNo=" + chassisNo + ", loanNo=" + loanNo + ", key=" + key + ", rc=" + rc
				+ ", rcNo=" + rcNo + ", insNo=" + insNo + ", insProvider=" + insProvider + "]";
	}
	
	
}
