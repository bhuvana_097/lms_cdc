package com.vg.lms.bean;

public class UpdateUserPassword {
	
	
	private int id;
	private String lmsusercode;
	private String lmsusername;
	private String lmsuserpwd;
	
	private String newuserpwd;
	private String cnfrmnewpwd;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLmsusercode() {
		return lmsusercode;
	}
	public void setLmsusercode(String lmsusercode) {
		this.lmsusercode = lmsusercode;
	}
	public String getLmsusername() {
		return lmsusername;
	}
	public void setLmsusername(String lmsusername) {
		this.lmsusername = lmsusername;
	}
	public String getLmsuserpwd() {
		return lmsuserpwd;
	}
	public void setLmsuserpwd(String lmsuserpwd) {
		this.lmsuserpwd = lmsuserpwd;
	}
	
	public String getNewuserpwd() {
		return newuserpwd;
	}
	public void setNewuserpwd(String newuserpwd) {
		this.newuserpwd = newuserpwd;
	}
	public String getCnfrmnewpwd() {
		return cnfrmnewpwd;
	}
	public void setCnfrmnewpwd(String cnfrmnewpwd) {
		this.cnfrmnewpwd = cnfrmnewpwd;
	}
	@Override
	public String toString() {
		return "UpdateUserPassword [id=" + id + ", lmsusercode=" + lmsusercode + ", lmsusername=" + lmsusername
				+ ", lmsuserpwd=" + lmsuserpwd + ", newuserpwd=" + newuserpwd + ", cnfrmnewpwd=" + cnfrmnewpwd
				+ ", getId()=" + getId() + ", getLmsusercode()=" + getLmsusercode() + ", getLmsusername()="
				+ getLmsusername() + ", getLmsuserpwd()=" + getLmsuserpwd() + ", getNewuserpwd()=" + getNewuserpwd()
				+ ", getCnfrmnewpwd()=" + getCnfrmnewpwd() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
	

	
	
	
}
