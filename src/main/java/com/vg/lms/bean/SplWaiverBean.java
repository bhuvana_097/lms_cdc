package com.vg.lms.bean;

import javax.persistence.Column;

public class SplWaiverBean {

	 private String schemeType,
	 schemePercentage,
	  tbcPrinArrears,
	  tbcPrinOS,
	  tbcIntOnPosSinceLastDue,
      tbcOdcDue,
	  tbcMoratoriumInt,
	  tbcFclCharges,
	  tbcChargesDue,
	  tbcFclAmount,
	  loanNo;
	
	 public String getSchemeType() {
		return schemeType;
	}
	public void setSchemeType(String schemeType) {
		this.schemeType = schemeType;
	}
	public String getSchemePercentage() {
		return schemePercentage;
	}
	public void setSchemePercentage(String schemePercentage) {
		this.schemePercentage = schemePercentage;
	}
	public String getTbcPrinArrears() {
		return tbcPrinArrears;
	}
	public void setTbcPrinArrears(String tbcPrinArrears) {
		this.tbcPrinArrears = tbcPrinArrears;
	}
	public String getTbcPrinOS() {
		return tbcPrinOS;
	}
	public void setTbcPrinOS(String tbcPrinOS) {
		this.tbcPrinOS = tbcPrinOS;
	}
	public String getTbcIntOnPosSinceLastDue() {
		return tbcIntOnPosSinceLastDue;
	}
	public void setTbcIntOnPosSinceLastDue(String tbcIntOnPosSinceLastDue) {
		this.tbcIntOnPosSinceLastDue = tbcIntOnPosSinceLastDue;
	}
	public String getTbcOdcDue() {
		return tbcOdcDue;
	}
	public void setTbcOdcDue(String tbcOdcDue) {
		this.tbcOdcDue = tbcOdcDue;
	}
	public String getTbcMoratoriumInt() {
		return tbcMoratoriumInt;
	}
	public void setTbcMoratoriumInt(String tbcMoratoriumInt) {
		this.tbcMoratoriumInt = tbcMoratoriumInt;
	}
	public String getTbcFclCharges() {
		return tbcFclCharges;
	}
	public void setTbcFclCharges(String tbcFclCharges) {
		this.tbcFclCharges = tbcFclCharges;
	}
	public String getTbcChargesDue() {
		return tbcChargesDue;
	}
	public void setTbcChargesDue(String tbcChargesDue) {
		this.tbcChargesDue = tbcChargesDue;
	}
	public String getTbcFclAmount() {
		return tbcFclAmount;
	}
	public void setTbcFclAmount(String tbcFclAmount) {
		this.tbcFclAmount = tbcFclAmount;
	}
	public String getLoanNo() {
		return loanNo;
	}

	public void setLoanNo(String loanNo) {
		this.loanNo = loanNo;
	}


}

