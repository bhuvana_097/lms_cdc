package com.vg.lms.bean;


public class CovidMoratBean {
	private String id;
	private String loanno;
	private String moratMonth;
	private String moratYear;
	private String moratInt;
	private String loanPos;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLoanno() {
		return loanno;
	}
	public void setLoanno(String loanno) {
		this.loanno = loanno;
	}
	public String getMoratMonth() {
		return moratMonth;
	}
	public void setMoratMonth(String moratMonth) {
		this.moratMonth = moratMonth;
	}
	public String getMoratYear() {
		return moratYear;
	}
	public void setMoratYear(String moratYear) {
		this.moratYear = moratYear;
	}
	public String getMoratInt() {
		return moratInt;
	}
	public void setMoratInt(String moratInt) {
		this.moratInt = moratInt;
	}
	

public String getLoanPos() {
	return loanPos;
}
public void setLoanPos(String loanPos) {
	this.loanPos = loanPos;
}
	
	

}
