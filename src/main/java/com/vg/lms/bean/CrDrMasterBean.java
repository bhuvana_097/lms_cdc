package com.vg.lms.bean;

public class CrDrMasterBean {
	
	private int id;
	private String crDr;
    private String type;
    private String description;
    
	public String getCrDr() {
		return crDr;
	}
	public void setCrDr(String crDr) {
		this.crDr = crDr;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return String.format("CrDrMasterBean [id=%s, crDr=%s, type=%s, description=%s]", id, crDr, type, description);
	}
	
	
    

}
