package com.vg.lms.bean;

public class UpdateSessionBean {
	private String sessionId;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public String toString() {
		return "UpdateSessionBean [sessionId=" + sessionId + ", getSessionId()=" + getSessionId() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}
}
