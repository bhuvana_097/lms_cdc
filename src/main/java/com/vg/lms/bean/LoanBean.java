package com.vg.lms.bean;

import java.util.List;

import com.vg.lms.UserModuleMasterDetails;
import com.vg.lms.model.SplWaiver;
import com.vg.lms.model.UserModuleMaster;

public class LoanBean {
	
	private List<UserModuleMaster>  userModuleMastersList = UserModuleMasterDetails.getModuleDetails();
	
	private String loanId, applNo,custId, custName, disbDate, status, loanAmount, tenure, roi, cashInHand,
	bc, odMonths, firstDueDate, lastDueDate, currentMonth, remMonths, paymentMode, coLender, excessPaid,
	hypothecation, foreClosureAmt, scheme, dealer, make, model, gridAmount, onRoadPrice, downPayment, waiver,
	ltv,actualltv,irr,emi,advanceEMI,prcesFee,cashModeFee,dealerDisbAmount,loanNo,disbursed,mode, cancelable,
	prinArrear, intArrear, pos, intAfterDue, odc, oc, moratoriumInterest, oneDayInterst, fclCharge, fclAmount,userRole,userName,
	addWaiverEnabled, cancelEnabled, disburseEnabled, updatePDDEnabled, waiveCloseEnabled, closedTime, agmntDate,
	dealerName,securitisation,statusDisposition,ckycNumber,
	moratReason,moratInterest,moratTenure,moratoriumIntWaiver,covidPayInt,flagSec,dealerCode,addSOAEnabled,
	documentCharge,documentChargeBase,documentChargeTax,exGratiaCreditAmount,exGratiaCreditAmountExcess,cusType,fclBlock;
	

	private List<Charges> chargesList;
	private List<CovidMoratBean> covidMoratList;
	private List<SplWaiverBean> splWaiverList;
	
	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	private List<LoanWaiver> loanWaivers;
	
	private List<LoanReceipt> loanReceipts;

	public String getLoanId() {
		return loanId;
	}

	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getDisbDate() {
		return disbDate;
	}

	public void setDisbDate(String disbDate) {
		this.disbDate = disbDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(String loanAmount) {
		this.loanAmount = loanAmount;
	}

	public String getTenure() {
		return tenure;
	}

	public void setTenure(String tenure) {
		this.tenure = tenure;
	}

	public String getRoi() {
		return roi;
	}

	public void setRoi(String roi) {
		this.roi = roi;
	}

	public String getBc() {
		return bc;
	}

	public void setBc(String bc) {
		this.bc = bc;
	}

	public String getOdMonths() {
		return odMonths;
	}

	public void setOdMonths(String odMonths) {
		this.odMonths = odMonths;
	}

	public String getFirstDueDate() {
		return firstDueDate;
	}

	public void setFirstDueDate(String firstDueDate) {
		this.firstDueDate = firstDueDate;
	}

	public String getLastDueDate() {
		return lastDueDate;
	}

	public void setLastDueDate(String lastDueDate) {
		this.lastDueDate = lastDueDate;
	}

	public String getCurrentMonth() {
		return currentMonth;
	}

	public void setCurrentMonth(String currentMonth) {
		this.currentMonth = currentMonth;
	}

	public String getRemMonths() {
		return remMonths;
	}

	public void setRemMonths(String remMonths) {
		this.remMonths = remMonths;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getCoLender() {
		return coLender;
	}

	public void setCoLender(String coLender) {
		this.coLender = coLender;
	}

	public String getHypothecation() {
		return hypothecation;
	}

	public void setHypothecation(String hypothecation) {
		this.hypothecation = hypothecation;
	}

	public String getForeClosureAmt() {
		return foreClosureAmt;
	}

	public void setForeClosureAmt(String foreClosureAmt) {
		this.foreClosureAmt = foreClosureAmt;
	}

	public String getScheme() {
		return scheme;
	}

	public void setScheme(String scheme) {
		this.scheme = scheme;
	}

	public String getDealer() {
		return dealer;
	}

	public void setDealer(String dealer) {
		this.dealer = dealer;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getGridAmount() {
		return gridAmount;
	}

	public void setGridAmount(String gridAmount) {
		this.gridAmount = gridAmount;
	}

	public String getOnRoadPrice() {
		return onRoadPrice;
	}

	public void setOnRoadPrice(String onRoadPrice) {
		this.onRoadPrice = onRoadPrice;
	}

	public String getDownPayment() {
		return downPayment;
	}

	public void setDownPayment(String downPayment) {
		this.downPayment = downPayment;
	}

	public String getLtv() {
		return ltv;
	}

	public void setLtv(String ltv) {
		this.ltv = ltv;
	}

	public String getActualltv() {
		return actualltv;
	}

	public void setActualltv(String actualltv) {
		this.actualltv = actualltv;
	}

	public String getIrr() {
		return irr;
	}

	public void setIrr(String irr) {
		this.irr = irr;
	}

	public String getEmi() {
		return emi;
	}

	public void setEmi(String emi) {
		this.emi = emi;
	}

	public String getAdvanceEMI() {
		return advanceEMI;
	}

	public void setAdvanceEMI(String advanceEMI) {
		this.advanceEMI = advanceEMI;
	}

	public String getPrcesFee() {
		return prcesFee;
	}

	public void setPrcesFee(String prcesFee) {
		this.prcesFee = prcesFee;
	}

	public String getCashModeFee() {
		return cashModeFee;
	}

	public void setCashModeFee(String cashModeFee) {
		this.cashModeFee = cashModeFee;
	}

	public String getDealerDisbAmount() {
		return dealerDisbAmount;
	}

	public void setDealerDisbAmount(String dealerDisbAmount) {
		this.dealerDisbAmount = dealerDisbAmount;
	}

	public String getLoanNo() {
		return loanNo;
	}

	public void setLoanNo(String loanNo) {
		this.loanNo = loanNo;
	}

	public String getDisbursed() {
		return disbursed;
	}

	public void setDisbursed(String disbursed) {
		this.disbursed = disbursed;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getPrinArrear() {
		return prinArrear;
	}

	public void setPrinArrear(String prinArrear) {
		this.prinArrear = prinArrear;
	}

	public String getIntArrear() {
		return intArrear;
	}

	public void setIntArrear(String intArrear) {
		this.intArrear = intArrear;
	}

	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public String getIntAfterDue() {
		return intAfterDue;
	}

	public void setIntAfterDue(String intAfterDue) {
		this.intAfterDue = intAfterDue;
	}

	public String getOdc() {
		return odc;
	}

	public void setOdc(String odc) {
		this.odc = odc;
	}

	public String getOc() {
		return oc;
	}

	public void setOc(String oc) {
		this.oc = oc;
	}

	public String getMoratoriumInterest() {
		return moratoriumInterest;
	}

	public void setMoratoriumInterest(String moratoriumInterest) {
		this.moratoriumInterest = moratoriumInterest;
	}

	public String getOneDayInterst() {
		return oneDayInterst;
	}

	public void setOneDayInterst(String oneDayInterst) {
		this.oneDayInterst = oneDayInterst;
	}

	public String getFclCharge() {
		return fclCharge;
	}

	public void setFclCharge(String fclCharge) {
		this.fclCharge = fclCharge;
	}

	public String getFclAmount() {
		return fclAmount;
	}

	public void setFclAmount(String fclAmount) {
		this.fclAmount = fclAmount;
	}

	public String getCancelable() {
		return cancelable;
	}

	public void setCancelable(String cancelable) {
		this.cancelable = cancelable;
	}

	public String getWaiver() {
		return waiver;
	}

	public void setWaiver(String waiver) {
		this.waiver = waiver;
	}

	public List<LoanWaiver> getLoanWaivers() {
		return loanWaivers;
	}

	public void setLoanWaivers(List<LoanWaiver> loanWaivers) {
		this.loanWaivers = loanWaivers;
	}

	public String getExcessPaid() {
		return excessPaid;
	}

	public void setExcessPaid(String excessPaid) {
		this.excessPaid = excessPaid;
	}

	public List<LoanReceipt> getLoanReceipts() {
		return loanReceipts;
	}

	public void setLoanReceipts(List<LoanReceipt> loanReceipts) {
		this.loanReceipts = loanReceipts;
	}

	public String getCashInHand() {
		return cashInHand;
	}

	public void setCashInHand(String cashInHand) {
		this.cashInHand = cashInHand;
	}

	public String getAddWaiverEnabled() {
		return addWaiverEnabled;
	}

	public void setAddWaiverEnabled(String addWaiverEnabled) {
		this.addWaiverEnabled = addWaiverEnabled;
	}

	public String getCancelEnabled() {
		return cancelEnabled;
	}

	public void setCancelEnabled(String cancelEnabled) {
		this.cancelEnabled = cancelEnabled;
	}

	public String getDisburseEnabled() {
		return disburseEnabled;
	}

	public void setDisburseEnabled(String disburseEnabled) {
		this.disburseEnabled = disburseEnabled;
	}

	public String getUpdatePDDEnabled() {
		return updatePDDEnabled;
	}

	
	public void setUpdatePDDEnabled(String updatePDDEnabled) {
		this.updatePDDEnabled = updatePDDEnabled;
	}

	public List<Charges> getChargesList() {
		return chargesList;
	}

	public void setChargesList(List<Charges> chargesList) {
		this.chargesList = chargesList;
	}

	public String getWaiveCloseEnabled() {
		return waiveCloseEnabled;
	}

	public void setWaiveCloseEnabled(String waiveCloseEnabled) {
		this.waiveCloseEnabled = waiveCloseEnabled;
	}

	public String getClosedTime() {
		return closedTime;
	}

	public void setClosedTime(String closedTime) {
		this.closedTime = closedTime;
	}

	public String getAgmntDate() {
		return agmntDate;
	}

	public void setAgmntDate(String agmntDate) {
		this.agmntDate = agmntDate;
	}
	
	public String getApplNo() {
		return applNo;
	}

	public void setApplNo(String applNo) {
		this.applNo = applNo;
	}


	public String getDealerName() {
		return dealerName;
	}



	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}

	 public String getSecuritisation()
	  {
		  return securitisation;
	  }
	  public void setSecuritisation(String securitisation)
	  {
		  this.securitisation =securitisation;
	  }
	  public String getstatusDisposition()
	  {
		  return statusDisposition;
	  }
	  public void setstatusDisposition(String statusDisposition)
	  {
		  this.statusDisposition =statusDisposition;
	  }
	  
	  public String getCkycNumber()
	  {
		  return ckycNumber;
	  }
	  public void setCkycNumber(String ckycNumber)
	  {
		  this.ckycNumber =ckycNumber;
	  }
	  
	  
	  public String getMoratReason()
	  {
		  return moratReason;
	  }
	  public void setMoratReason(String moratReason)
	  {
		  this.moratReason =moratReason;
	  }
	  
		
	  public String getMoratInterest()
	  {
		  return moratInterest;
	  }
	  public void setMoratInterest(String moratInterest)
	  {
		  this.moratInterest =moratInterest;
	  }
	  
	  
		
	  public String getMoratTenure()
	  {
		  return moratTenure;
	  }
	  public void setMoratTenure(String moratTenure)
	  {
		  this.moratTenure =moratTenure;
	  }
	  
	  	  
	  public String getMoratoriumIntWaiver()
	  {
		  return moratoriumIntWaiver;
	  }
	  public void setMoratoriumIntWaiver(String moratoriumIntWaiver)
	  {
		  this.moratoriumIntWaiver =moratoriumIntWaiver;
	  }
	  public String getCovidPayInt()
	  {
		  return covidPayInt;
	  }
	  public void setCovidPayInt(String covidPayInt)
	  {
		  this.covidPayInt =covidPayInt;
	  }
	  
	  public List<CovidMoratBean> getCovidMoratList() {
			return covidMoratList;
		}

		public void setCovidMoratList(List<CovidMoratBean> covidMoratList) {
			this.covidMoratList = covidMoratList;
		}
		
		
		  public String getFlagSec()
		  {
			  return flagSec;
		  }
		  public void setFlagSec(String flagSec)
		  {
			  this.flagSec =flagSec;
		  }

		  
		  
		  public String getDealerCode()
		  {
			  return dealerCode;
		  }
		  public void setDealerCode(String dealerCode)
		  {
			  this.dealerCode =dealerCode;
		  }
		  
		  public String getAddSOAEnabled() {
				return addSOAEnabled;
			}

			public void setAddSOAEnabled(String addSOAEnabled) {
				this.addSOAEnabled = addSOAEnabled;
			}
			
			  public String getDocumentCharge()
			  {
				  return documentCharge;
			  }
			  public void setDocumentCharge(String documentCharge)
			  {
				  this.documentCharge =documentCharge;
			  }
			  
			  public String getDocumentChargeBase()
			  {
				  return documentChargeBase;
			  }
			  public void setDocumentChargeBase(String documentChargeBase)
			  {
				  this.documentChargeBase =documentChargeBase;
			  }
			  
			  public String getDocumentChargeTax()
			  {
				  return documentChargeTax;
			  }
			  public void setDocumentChargeTax(String documentChargeTax)
			  {
				  this.documentChargeTax =documentChargeTax;
			  }
			  
			  
			  public String getExGratiaCreditAmount()
			  {
				  return exGratiaCreditAmount;
			  }
			  public void setExGratiaCreditAmount(String exGratiaCreditAmount)
			  {
				  this.exGratiaCreditAmount =exGratiaCreditAmount;
			  }
			  
			  
			  
			  public String getExGratiaCreditAmountExcess()
			  {
				  return exGratiaCreditAmountExcess;
			  }
			  public void setExGratiaCreditAmountExcess(String exGratiaCreditAmountExcess)
			  {
				  this.exGratiaCreditAmountExcess =exGratiaCreditAmountExcess;
			  }
			  
			  public List<SplWaiverBean> getSplWaiverList() {
					return splWaiverList;
				}

				public void setSplWaiverList(List<SplWaiverBean> splWaiverList) {
					this.splWaiverList = splWaiverList;
				}
				
				public String getFclBlock() {
					return fclBlock;
				}

				public void setFclBlock(String fclBlock) {
					this.fclBlock = fclBlock;
				}
// For Customer Categorization - Code Edited BY Bhuvana 25-06-2021
				public String getCusType() {
					return cusType;
				}

				public void setCusType(String acustype) {
					this.cusType = acustype;
				}
}
