package com.vg.lms.bean;

public class LoanReceipt {
	
    private String bookNo;
    private String receiptNo;
    private String loanNo;
    private String agentCode;
    private String agentName;
    private String receiptDate;
    private String receiptAmount;
    private String cashInHand;
    private String remittanceNo;
    
	public String getBookNo() {
		return bookNo;
	}
	public void setBookNo(String bookNo) {
		this.bookNo = bookNo;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public String getLoanNo() {
		return loanNo;
	}
	public void setLoanNo(String loanNo) {
		this.loanNo = loanNo;
	}
	public String getAgentCode() {
		return agentCode;
	}
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public String getReceiptDate() {
		return receiptDate;
	}
	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}
	public String getReceiptAmount() {
		return receiptAmount;
	}
	public void setReceiptAmount(String receiptAmount) {
		this.receiptAmount = receiptAmount;
	}
	public String getCashInHand() {
		return cashInHand;
	}
	public void setCashInHand(String cashInHand) {
		this.cashInHand = cashInHand;
	}
	public String getRemittanceNo() {
		return remittanceNo;
	}
	public void setRemittanceNo(String remittanceNo) {
		this.remittanceNo = remittanceNo;
	}
    
    
    

}
