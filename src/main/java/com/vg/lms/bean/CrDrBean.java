package com.vg.lms.bean;

public class CrDrBean {

	private int loanId;    
	private String waiverHead;
    private float amount;
    private String description;
    
	public int getLoanId() {
		return loanId;
	}
	public void setLoanId(int loanId) {
		this.loanId = loanId;
	}
	public String getWaiverHead() {
		return waiverHead;
	}
	public void setWaiverHead(String waiverHead) {
		this.waiverHead = waiverHead;
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return "CrDrBean [loanId=" + loanId + ", waiverHead=" + waiverHead + ", amount=" + amount + ", description="
				+ description + "]";
	}
    
	
}
