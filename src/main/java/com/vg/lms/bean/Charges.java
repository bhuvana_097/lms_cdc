package com.vg.lms.bean;

public class Charges {
	private String id;
	private String loanno;
	private String chargeid;
	private String duedate;
	private String chargeamount;
	private String taxamount;
	private String status;
	private String chargepaid;
	private String taxpaid;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLoanno() {
		return loanno;
	}
	public void setLoanno(String loanno) {
		this.loanno = loanno;
	}
	public String getChargeid() {
		return chargeid;
	}
	public void setChargeid(String chargeid) {
		this.chargeid = chargeid;
	}
	public String getDuedate() {
		return duedate;
	}
	public void setDuedate(String duedate) {
		this.duedate = duedate;
	}
	public String getChargeamount() {
		return chargeamount;
	}
	public void setChargeamount(String chargeamount) {
		this.chargeamount = chargeamount;
	}
	public String getTaxamount() {
		return taxamount;
	}
	public void setTaxamount(String taxamount) {
		this.taxamount = taxamount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getChargepaid() {
		return chargepaid;
	}
	public void setChargepaid(String chargepaid) {
		this.chargepaid = chargepaid;
	}
	public String getTaxpaid() {
		return taxpaid;
	}
	public void setTaxpaid(String taxpaid) {
		this.taxpaid = taxpaid;
	}
	
	

}
