package com.vg.lms.bean;

public class WaiverSplitBean {



	private int loanId;    
	
	private float principalAmount;
	private float interestAmount;
	private float chargesDueAmt;
	private float penalIntAmt;
	private float fclAmt;
	private float brokPerIntAmt;
	private float moratoriumAmt;
	
    private String description;

	public int getLoanId() {
		return loanId;
	}

	public void setLoanId(int loanId) {
		this.loanId = loanId;
	}

	public float getPrincipalAmount() {
		return principalAmount;
	}

	public void setPrincipalAmount(float principalAmount) {
		this.principalAmount = principalAmount;
	}

	public float getInterestAmount() {
		return interestAmount;
	}

	public void setInterestAmount(float interestAmount) {
		this.interestAmount = interestAmount;
	}

	public float getChargesDueAmt() {
		return chargesDueAmt;
	}

	public void setChargesDueAmt(float chargesDueAmt) {
		this.chargesDueAmt = chargesDueAmt;
	}

	public float getPenalIntAmt() {
		return penalIntAmt;
	}

	public void setPenalIntAmt(float penalIntAmt) {
		this.penalIntAmt = penalIntAmt;
	}

	public float getFclAmt() {
		return fclAmt;
	}

	public void setFclAmt(float fclAmt) {
		this.fclAmt = fclAmt;
	}

	public float getBrokPerIntAmt() {
		return brokPerIntAmt;
	}

	public void setBrokPerIntAmt(float brokPerIntAmt) {
		this.brokPerIntAmt = brokPerIntAmt;
	}

	public float getMoratoriumAmt() {
		return moratoriumAmt;
	}

	public void setMoratoriumAmt(float moratoriumAmt) {
		this.moratoriumAmt = moratoriumAmt;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "waiverSplit [loanId=" + loanId + ", principalAmount=" + principalAmount + ", interestAmount="
				+ interestAmount + ", chargesDueAmt=" + chargesDueAmt + ", penalIntAmt=" + penalIntAmt + ", fclAmt="
				+ fclAmt + ", brokPerIntAmt=" + brokPerIntAmt + ", moratoriumAmt=" + moratoriumAmt + ", description="
				+ description + "]";
	}
    
		
}
