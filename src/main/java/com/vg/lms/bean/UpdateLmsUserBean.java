package com.vg.lms.bean;

public class UpdateLmsUserBean {

	private int id;
	private String lmsusercode;
	private String lmsusername;
	private String lmsuserpwd;
	private String dept;
	private String hier_level;
	private String branch;
	private String status;
	private String reason;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLmsusercode() {
		return lmsusercode;
	}
	public void setLmsusercode(String lmsusercode) {
		this.lmsusercode = lmsusercode;
	}
	public String getLmsusername() {
		return lmsusername;
	}
	public void setLmsusername(String lmsusername) {
		this.lmsusername = lmsusername;
	}
	public String getLmsuserpwd() {
		return lmsuserpwd;
	}
	public void setLmsuserpwd(String lmsuserpwd) {
		this.lmsuserpwd = lmsuserpwd;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getHier_level() {
		return hier_level;
	}
	public void setHier_level(String hier_level) {
		this.hier_level = hier_level;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	@Override
	public String toString() {
		return "UpdateLmsUserBean [id=" + id + ", lmsusercode=" + lmsusercode + ", lmsusername=" + lmsusername
				+ ", lmsuserpwd=" + lmsuserpwd + ", dept=" + dept + ", hier_level=" + hier_level + ", branch=" + branch
				+ ", status=" + status + ", reason=" + reason + ", getId()=" + getId() + ", getLmsusercode()="
				+ getLmsusercode() + ", getLmsusername()=" + getLmsusername() + ", getLmsuserpwd()=" + getLmsuserpwd()
				+ ", getDept()=" + getDept() + ", getHier_level()=" + getHier_level() + ", getStatus()=" + getStatus()
				+ ", getReason()=" + getReason() + ", getBranch()=" + getBranch() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}
	
	
	
}
