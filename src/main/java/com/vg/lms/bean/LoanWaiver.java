package com.vg.lms.bean;

public class LoanWaiver {
	private String waiverDate, waiverType, waiverAmount, waiverRemarks;

	public String getWaiverDate() {
		return waiverDate;
	}

	public void setWaiverDate(String waiverDate) {
		this.waiverDate = waiverDate;
	}

	public String getWaiverType() {
		return waiverType;
	}

	public void setWaiverType(String waiverType) {
		this.waiverType = waiverType;
	}

	public String getWaiverAmount() {
		return waiverAmount;
	}

	public void setWaiverAmount(String waiverAmount) {
		this.waiverAmount = waiverAmount;
	}

	public String getWaiverRemarks() {
		return waiverRemarks;
	}

	public void setWaiverRemarks(String waiverRemarks) {
		this.waiverRemarks = waiverRemarks;
	}
	
	
}
