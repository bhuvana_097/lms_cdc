package com.vg.lms.response;

import java.util.Set;

public class LoanDTO {
	
	private int id;
    private int custId;
    private int gurantorId;
    private String applNo;
    private String loanNo;
    private String agmntDate;
    private String firstDueDate;
    private String lastDueDate;
    private float amount;
    private float tenure;
    private float irr;
    private float procFee;
    private String advanceEmi;
    private float insuranceAmount;
    private float cashCollCharges;
    private float dealerDisbursementAmount;
    private float downPayment;
    private float emi;
    private float futurePrinciple;
    private float excessPaid;
    private float arrears;
    private float bc;
    private float odcCurMonth;
    private float odcCumm;
    private float oc;
    private float toBeCollected;
    private float collected;
    private float odMonths;
    private String status;
    private String book;
    private String hypothecation;
    private String coLender;
    private float coLendPercent;
    private float expense;
    private String dealerCode;
    private String dealerName;
    private String loanAgreement;
    private String mandateFile;
    private String h1;
    private String h2;
    private String h3;
    private String h4;
    private String h5;
    private String createUser;
    private String createTime;
    private String modifyUser;
    private String modifyTime;
    
    private Set<ReceivableDTO> receivable;
    private Set<AssetDetails> asset;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCustId() {
		return custId;
	}
	public void setCustId(int custId) {
		this.custId = custId;
	}
	public int getGurantorId() {
		return gurantorId;
	}
	public void setGurantorId(int gurantorId) {
		this.gurantorId = gurantorId;
	}
	public String getApplNo() {
		return applNo;
	}
	public void setApplNo(String applNo) {
		this.applNo = applNo;
	}
	public String getLoanNo() {
		return loanNo;
	}
	public void setLoanNo(String loanNo) {
		this.loanNo = loanNo;
	}
	public String getAgmntDate() {
		return agmntDate;
	}
	public void setAgmntDate(String agmntDate) {
		this.agmntDate = agmntDate;
	}
	public String getFirstDueDate() {
		return firstDueDate;
	}
	public void setFirstDueDate(String firstDueDate) {
		this.firstDueDate = firstDueDate;
	}
	public String getLastDueDate() {
		return lastDueDate;
	}
	public void setLastDueDate(String lastDueDate) {
		this.lastDueDate = lastDueDate;
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	public float getTenure() {
		return tenure;
	}
	public void setTenure(float tenure) {
		this.tenure = tenure;
	}
	public float getIrr() {
		return irr;
	}
	public void setIrr(float irr) {
		this.irr = irr;
	}
	public float getProcFee() {
		return procFee;
	}
	public void setProcFee(float procFee) {
		this.procFee = procFee;
	}
	public String getAdvanceEmi() {
		return advanceEmi;
	}
	public void setAdvanceEmi(String advanceEmi) {
		this.advanceEmi = advanceEmi;
	}
	public float getInsuranceAmount() {
		return insuranceAmount;
	}
	public void setInsuranceAmount(float insuranceAmount) {
		this.insuranceAmount = insuranceAmount;
	}
	public float getCashCollCharges() {
		return cashCollCharges;
	}
	public void setCashCollCharges(float cashCollCharges) {
		this.cashCollCharges = cashCollCharges;
	}
	public float getDealerDisbursementAmount() {
		return dealerDisbursementAmount;
	}
	public void setDealerDisbursementAmount(float dealerDisbursementAmount) {
		this.dealerDisbursementAmount = dealerDisbursementAmount;
	}
	public float getDownPayment() {
		return downPayment;
	}
	public void setDownPayment(float downPayment) {
		this.downPayment = downPayment;
	}
	public float getEmi() {
		return emi;
	}
	public void setEmi(float emi) {
		this.emi = emi;
	}
	public float getFuturePrinciple() {
		return futurePrinciple;
	}
	public void setFuturePrinciple(float futurePrinciple) {
		this.futurePrinciple = futurePrinciple;
	}
	public float getExcessPaid() {
		return excessPaid;
	}
	public void setExcessPaid(float excessPaid) {
		this.excessPaid = excessPaid;
	}
	public float getArrears() {
		return arrears;
	}
	public void setArrears(float arrears) {
		this.arrears = arrears;
	}
	public float getBc() {
		return bc;
	}
	public void setBc(float bc) {
		this.bc = bc;
	}
	public float getOdcCurMonth() {
		return odcCurMonth;
	}
	public void setOdcCurMonth(float odcCurMonth) {
		this.odcCurMonth = odcCurMonth;
	}
	public float getOdcCumm() {
		return odcCumm;
	}
	public void setOdcCumm(float odcCumm) {
		this.odcCumm = odcCumm;
	}
	public float getOc() {
		return oc;
	}
	public void setOc(float oc) {
		this.oc = oc;
	}
	public float getToBeCollected() {
		return toBeCollected;
	}
	public void setToBeCollected(float toBeCollected) {
		this.toBeCollected = toBeCollected;
	}
	public float getCollected() {
		return collected;
	}
	public void setCollected(float collected) {
		this.collected = collected;
	}
	public float getOdMonths() {
		return odMonths;
	}
	public void setOdMonths(float odMonths) {
		this.odMonths = odMonths;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getBook() {
		return book;
	}
	public void setBook(String book) {
		this.book = book;
	}
	public String getHypothecation() {
		return hypothecation;
	}
	public void setHypothecation(String hypothecation) {
		this.hypothecation = hypothecation;
	}
	public String getCoLender() {
		return coLender;
	}
	public void setCoLender(String coLender) {
		this.coLender = coLender;
	}
	public float getCoLendPercent() {
		return coLendPercent;
	}
	public void setCoLendPercent(float coLendPercent) {
		this.coLendPercent = coLendPercent;
	}
	public float getExpense() {
		return expense;
	}
	public void setExpense(float expense) {
		this.expense = expense;
	}
	public String getDealerCode() {
		return dealerCode;
	}
	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}
	public String getDealerName() {
		return dealerName;
	}
	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}
	public String getLoanAgreement() {
		return loanAgreement;
	}
	public void setLoanAgreement(String loanAgreement) {
		this.loanAgreement = loanAgreement;
	}
	public String getMandateFile() {
		return mandateFile;
	}
	public void setMandateFile(String mandateFile) {
		this.mandateFile = mandateFile;
	}
	public String getH1() {
		return h1;
	}
	public void setH1(String h1) {
		this.h1 = h1;
	}
	public String getH2() {
		return h2;
	}
	public void setH2(String h2) {
		this.h2 = h2;
	}
	public String getH3() {
		return h3;
	}
	public void setH3(String h3) {
		this.h3 = h3;
	}
	public String getH4() {
		return h4;
	}
	public void setH4(String h4) {
		this.h4 = h4;
	}
	public String getH5() {
		return h5;
	}
	public void setH5(String h5) {
		this.h5 = h5;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
	public String getModifyTime() {
		return modifyTime;
	}
	public void setModifyTime(String modifyTime) {
		this.modifyTime = modifyTime;
	}
	public Set<ReceivableDTO> getReceivable() {
		return receivable;
	}
	public void setReceivable(Set<ReceivableDTO> receivable) {
		this.receivable = receivable;
	}
	public Set<AssetDetails> getAsset() {
		return asset;
	}
	public void setAsset(Set<AssetDetails> asset) {
		this.asset = asset;
	}
	@Override
	public String toString() {
		return String.format(
				"LoanDTO [id=%s, custId=%s, gurantorId=%s, applNo=%s, loanNo=%s, agmntDate=%s, firstDueDate=%s, lastDueDate=%s, amount=%s, tenure=%s, irr=%s, procFee=%s, advanceEmi=%s, insuranceAmount=%s, cashCollCharges=%s, dealerDisbursementAmount=%s, downPayment=%s, emi=%s, futurePrinciple=%s, excessPaid=%s, arrears=%s, bc=%s, odcCurMonth=%s, odcCumm=%s, oc=%s, toBeCollected=%s, collected=%s, odMonths=%s, status=%s, book=%s, hypothecation=%s, coLender=%s, coLendPercent=%s, expense=%s, dealerCode=%s, dealerName=%s, loanAgreement=%s, mandateFile=%s, h1=%s, h2=%s, h3=%s, h4=%s, h5=%s, createUser=%s, createTime=%s, modifyUser=%s, modifyTime=%s, receivable=%s, asset=%s]",
				id, custId, gurantorId, applNo, loanNo, agmntDate, firstDueDate, lastDueDate, amount, tenure, irr,
				procFee, advanceEmi, insuranceAmount, cashCollCharges, dealerDisbursementAmount, downPayment, emi,
				futurePrinciple, excessPaid, arrears, bc, odcCurMonth, odcCumm, oc, toBeCollected, collected, odMonths,
				status, book, hypothecation, coLender, coLendPercent, expense, dealerCode, dealerName, loanAgreement,
				mandateFile, h1, h2, h3, h4, h5, createUser, createTime, modifyUser, modifyTime, receivable, asset);
	}
    
	
	

}
