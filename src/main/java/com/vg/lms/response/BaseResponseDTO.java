package com.vg.lms.response;

public class BaseResponseDTO {
	
	private String statusCode;
	private String statusMessage;
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	
	@Override
	public String toString() {
		return String.format("BaseResponseDTO [statusCode=%s, statusMessage=%s]", statusCode, statusMessage);
	}
	
	

}
