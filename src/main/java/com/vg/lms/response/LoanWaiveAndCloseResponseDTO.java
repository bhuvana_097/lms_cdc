package com.vg.lms.response;

import com.vg.lms.model.Loan;

public class LoanWaiveAndCloseResponseDTO {
	private String message;
	private Loan lmsLoan;
	
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Loan getLmsLoan() {
		return lmsLoan;
	}

	public void setLmsLoan(Loan lmsLoan) {
		this.lmsLoan = lmsLoan;
	}

	@Override
	public String toString() {
		return "LoanWaiveAndCloseResponseDTO [message=" + message + ", lmsLoan=" + lmsLoan.toString() + "]";
	}
	
	
	
}
