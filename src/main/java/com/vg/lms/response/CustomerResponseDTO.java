package com.vg.lms.response;

import com.vg.lms.request.CustomerDTO;

public class CustomerResponseDTO extends BaseResponseDTO {
	
	private CustomerDTO customer;

	public CustomerDTO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDTO customer) {
		this.customer = customer;
	}

	@Override
	public String toString() {
		return String.format(
				"CustomerResponseDTO [customer=%s, getCustomer()=%s, getStatusCode()=%s, getStatusMessage()=%s, toString()=%s, getClass()=%s, hashCode()=%s]",
				customer, getCustomer(), getStatusCode(), getStatusMessage(), super.toString(), getClass(), hashCode());
	}

	
}
