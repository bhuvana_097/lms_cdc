package com.vg.lms.response;

public class LOSResponseDTO extends BaseResponseDTO {
	
	private String lmsCustomerId;
	private String lmsLoanId;
	
	public String getLmsCustomerId() {
		return lmsCustomerId;
	}
	public void setLmsCustomerId(String lmsCustomerId) {
		this.lmsCustomerId = lmsCustomerId;
	}
	public String getLmsLoanId() {
		return lmsLoanId;
	}
	public void setLmsLoanId(String lmsLoanId) {
		this.lmsLoanId = lmsLoanId;
	}
	
	@Override
	public String toString() {
		return "LOSResponseDTO [lmsCustomerId=" + lmsCustomerId + ", lmsLoanId=" + lmsLoanId + ", getStatusCode()="
				+ getStatusCode() + ", getStatusMessage()=" + getStatusMessage() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}
	

}
