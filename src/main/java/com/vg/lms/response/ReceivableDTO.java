package com.vg.lms.response;

public class ReceivableDTO {

	private int id;
	private int loanId;
	private int custId;
	private int dueNo;
	private String dueDate;
	private float emi;
	private float principle;
	private float interest;
	private float principleRemaining;
	private int trycount;
	private String status;
	private String createUser;
	private String createTime;
	private String modifyUser;
	private String modifyTime;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getLoanId() {
		return loanId;
	}
	public void setLoanId(int loanId) {
		this.loanId = loanId;
	}
	public int getCustId() {
		return custId;
	}
	public void setCustId(int custId) {
		this.custId = custId;
	}
	public int getDueNo() {
		return dueNo;
	}
	public void setDueNo(int dueNo) {
		this.dueNo = dueNo;
	}
	public String getDueDate() {
		return dueDate;
	}
	public float getEmi() {
		return emi;
	}
	public void setEmi(float emi) {
		this.emi = emi;
	}
	public float getPrinciple() {
		return principle;
	}
	public void setPrinciple(float principle) {
		this.principle = principle;
	}
	public float getInterest() {
		return interest;
	}
	public void setInterest(float interest) {
		this.interest = interest;
	}
	public float getPrincipleRemaining() {
		return principleRemaining;
	}
	public void setPrincipleRemaining(float principleRemaining) {
		this.principleRemaining = principleRemaining;
	}
	public int getTrycount() {
		return trycount;
	}
	public void setTrycount(int trycount) {
		this.trycount = trycount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
	public String getModifyTime() {
		return modifyTime;
	}
	public void setModifyTime(String modifyTime) {
		this.modifyTime = modifyTime;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	@Override
	public String toString() {
		return String.format(
				"ReceivableDTO [id=%s, loanId=%s, custId=%s, dueNo=%s, dueDate=%s, emi=%s, principle=%s, interest=%s, principleRemaining=%s, trycount=%s, status=%s, createUser=%s, createTime=%s, modifyUser=%s, modifyTime=%s]",
				id, loanId, custId, dueNo, dueDate, emi, principle, interest, principleRemaining, trycount, status,
				createUser, createTime, modifyUser, modifyTime);
	}
	
	

	
	
}
