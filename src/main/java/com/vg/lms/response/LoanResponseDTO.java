package com.vg.lms.response;

public class LoanResponseDTO extends BaseResponseDTO {
	
	private LoanDTO loan;

	public LoanDTO getLoan() {
		return loan;
	}

	public void setLoan(LoanDTO loan) {
		this.loan = loan;
	}

	@Override
	public String toString() {
		return String.format(
				"LoanResponseDTO [loan=%s, getStatusCode()=%s, getStatusMessage()=%s, toString()=%s, getClass()=%s, hashCode()=%s]",
				loan, getStatusCode(), getStatusMessage(), super.toString(), getClass(), hashCode());
	}
	
}
