package com.vg.lms.response;

public class AssetDetails {
	
    private int id;
    private int loanId;
    private String assetType;
    private String make;
    private String model;
    private String variant;
    private String schemeType;
    private String prodSubcode;
    private float totalCost;
    private float gridPrice;
    private float onroadPrice;
    private float actualLtv;
    private String rcNo;
    private String rcPhoto;
    private String createUser;
    private String createTime;
    private String modifyUser;
    private String modifyTime;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getLoanId() {
		return loanId;
	}
	public void setLoanId(int loanId) {
		this.loanId = loanId;
	}
	public String getAssetType() {
		return assetType;
	}
	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getVariant() {
		return variant;
	}
	public void setVariant(String variant) {
		this.variant = variant;
	}
	public String getSchemeType() {
		return schemeType;
	}
	public void setSchemeType(String schemeType) {
		this.schemeType = schemeType;
	}
	public String getProdSubcode() {
		return prodSubcode;
	}
	public void setProdSubcode(String prodSubcode) {
		this.prodSubcode = prodSubcode;
	}
	public float getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(float totalCost) {
		this.totalCost = totalCost;
	}
	public float getGridPrice() {
		return gridPrice;
	}
	public void setGridPrice(float gridPrice) {
		this.gridPrice = gridPrice;
	}
	public float getOnroadPrice() {
		return onroadPrice;
	}
	public void setOnroadPrice(float onroadPrice) {
		this.onroadPrice = onroadPrice;
	}
	public float getActualLtv() {
		return actualLtv;
	}
	public void setActualLtv(float actualLtv) {
		this.actualLtv = actualLtv;
	}
	public String getRcNo() {
		return rcNo;
	}
	public void setRcNo(String rcNo) {
		this.rcNo = rcNo;
	}
	public String getRcPhoto() {
		return rcPhoto;
	}
	public void setRcPhoto(String rcPhoto) {
		this.rcPhoto = rcPhoto;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
	public String getModifyTime() {
		return modifyTime;
	}
	public void setModifyTime(String modifyTime) {
		this.modifyTime = modifyTime;
	}
	@Override
	public String toString() {
		return String.format(
				"AssetDetails [id=%s, loanId=%s, assetType=%s, make=%s, model=%s, variant=%s, schemeType=%s, prodSubcode=%s, totalCost=%s, gridPrice=%s, onroadPrice=%s, actualLtv=%s, rcNo=%s, rcPhoto=%s, createUser=%s, createTime=%s, modifyUser=%s, modifyTime=%s]",
				id, loanId, assetType, make, model, variant, schemeType, prodSubcode, totalCost, gridPrice, onroadPrice,
				actualLtv, rcNo, rcPhoto, createUser, createTime, modifyUser, modifyTime);
	}
	
	

}
