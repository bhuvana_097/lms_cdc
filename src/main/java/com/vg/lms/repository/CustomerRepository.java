package com.vg.lms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.Customer;


@Repository("CustomerRepository")
public interface CustomerRepository extends JpaRepository<Customer, Integer>{
	
	public Customer findByaadharNo(String aadharNo);
	public Customer findByvoterid(String voterid);
	public Customer findBydrivingLicense(String drivingLicense);
	public Customer findBypanno(String panno);
	
	public Customer findById(Customer id);
	
	// for CRM - code edited by Bhuvana 13-04-2021
		public List<Customer> findByphone1StartingWith(String mobileno);
		public List<Customer> findByphone1(String mobileNo);
}
