package com.vg.lms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.CashRemit;

@Repository("CashRemitRepository")
public interface CashRemitRepository extends JpaRepository<CashRemit, Integer> {

}
