package com.vg.lms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.Receipt;
import com.vg.lms.model.ReceiptOnline;

@Repository("ReceiptsRepository")
public interface ReceiptsRepository extends JpaRepository<Receipt, Integer>{
	
	public List<Receipt> findByremittanceNo(String remitanceNo);
	
	public List<Receipt> findByloanNoAndCashInHand(String loanNo, String cashInHand);
	
	public List<ReceiptOnline> findByloanNo(String loanNo);
	
	
	

}
