package com.vg.lms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.Receivable;

@Repository("LmsReceivableRepository")
public interface LmsReceivableRepository extends JpaRepository<Receivable, Integer> {
	public Receivable findById(int id);
	public List<Receivable> findByloanNo(String loanNo);

	//Code edited by Bhuvana
	
	@Query(value=" select due_date from lms_receivable where " + 
			" loan_no = ?1 " + 
			" and due_date <= DATE_FORMAT( CURRENT_DATE - INTERVAL 1 MONTH, '%Y/%m/05' )" + 
			" order by due_date desc limit 1", nativeQuery=true)
	public String duedate(String loanno);
	
	@Query(value=" select count(emi) AS OverDueEMI from lms_receivable where loan_no =?1  " + 
			     " and status IN ('PART_PAID','FUTURE')", nativeQuery=true)
	public String emicount(String loanno);	
	
	@Query(value="select max(value_date) from lms_receivable  where loan_no=?1", nativeQuery=true)
	public String lastduedate(String loanno); 

}