package com.vg.lms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.UserDeptMaster;

@Repository("UserDeptMasterRepository")
public interface UserDeptMasterRepository extends JpaRepository<UserDeptMaster, Integer> {
	public UserDeptMaster findByactive(boolean active);

}
