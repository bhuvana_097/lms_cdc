package com.vg.lms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.SplWaiver;

@Repository("SplWaiverRepo")
public interface SplWaiverRepo extends JpaRepository<SplWaiver, Integer> {
	
	List<SplWaiver> findByloanNo(String loanNo);

}
