package com.vg.lms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.Ledger;

@Repository("LedgerRepository")
public interface LedgerRepository extends JpaRepository<Ledger, Integer> {
	
	public List<Ledger> findByaccountIdOrderByValueDateAsc(String accountId);

	/* Changes For SOA*/
	@Query(value="select coalesce(sum(lp.amount),0) as amount FROM lms_pay_components lp "
			+ "WHERE lp.loan_no=:loanNo and lp.value_date <= :valueDate"
			+ " and description in ('PRIN_PAID', 'PRIN_PPAID', 'PRIN_ADVEMI','PRIN_WOFF')" , nativeQuery=true)
			public float findPrinPaid(@Param("loanNo") String loanNo, @Param("valueDate") String valueDate);

			@Query(value="select coalesce(sum(lp.moratorium_interest),0) as morat FROM lms_receivable lp "
			+ "WHERE lp.loan_no=:loanNo and lp.due_date <= :valueDate" , nativeQuery=true)
			public float findMoratInt(@Param("loanNo") String loanNo, @Param("valueDate") String valueDate);
}
