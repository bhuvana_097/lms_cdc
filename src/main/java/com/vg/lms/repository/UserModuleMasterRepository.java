package com.vg.lms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.vg.lms.model.UserModuleMaster;

@Repository("UserModuleMasterRepository")
public interface UserModuleMasterRepository extends JpaRepository<UserModuleMaster, Integer> {
	public UserModuleMaster findBydept(String dept);
	public UserModuleMaster findBymodule(String module);
}
