package com.vg.lms.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.vg.lms.bean.userlogdetails;
import com.vg.lms.model.LmsUserLogUser;
import com.vg.lms.model.UserMaster;


@Repository("LmsUserLogUserRepository")
public interface LmsUserLogUserRepository extends JpaRepository<LmsUserLogUser, Integer> {
	
	@Query(value="select max(lms_user_login_time) from lms_user_log_user ",nativeQuery=true)
	Date lastlogindate();
	
	@Modifying
	@Transactional
	@Query(value="update lms_user_log_user u set u.lms_user_logout_time = :date where u.lms_user_logout_time is null "
			+ " and lms_user_login_time= :logindate  and lms_user_log_userid=:userid",nativeQuery=true)
	int updatelogoutuser(@Param("date") Date date,@Param("logindate") Date logindate,@Param("userid") String userid);
	
//	public List<LmsUserLogUser> findAll();
} 