package com.vg.lms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.IncredAudit;
import com.vg.lms.model.Loan;

@Repository("LoanRepository")
public interface LoanRepository extends JpaRepository<Loan, Integer> {
	Loan findByLoanNo(String loanNo);
	public List<Loan> findByCoLender(String colender);
	
	Loan findById(String loanno);
	
	// for CRM - code edited by Bhuvana 13-04-2021
		@Query(value="select loan_no from lms_loan where cust_id=?1",nativeQuery=true)
		String findcustId(int custId);
		// for CRM - code edited by Bhuvana 16-04-2021
		public List<Loan> findByLoanNoStartingWith(String loanno);
		
		@Query(value=" select l.loan_no from lms_loan l,lms_customer c,application a where  " + 
				" c.phone1=a.mobileno and c.id=l.cust_id and c.phone1=?1",nativeQuery=true)
		String[] findphoneId(String phoneNo); 
}
