package com.vg.lms.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.otpGener;

	@Repository("OTPRepository")
	public interface OTPRepository extends JpaRepository<otpGener, Integer> {
		otpGener findBygeneratedOtp(String otp);
	}


