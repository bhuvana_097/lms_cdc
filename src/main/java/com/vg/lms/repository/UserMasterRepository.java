package com.vg.lms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.Asset;
import com.vg.lms.model.UserMaster;

import java.util.List;

@Repository("UserMasterRepository")
public interface UserMasterRepository extends JpaRepository<UserMaster, Integer> {
	public UserMaster findBycode(String code);
	
	 boolean existsBycode(String code);
	public List<UserMaster> findAll();
	
}
