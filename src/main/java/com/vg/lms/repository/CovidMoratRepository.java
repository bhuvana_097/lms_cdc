package com.vg.lms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.CovidMorat;

@Repository("CovidMoratRepository")
public interface CovidMoratRepository extends JpaRepository<CovidMorat, Integer> {
	
	public CovidMorat findById(int id);
	public List<CovidMorat> findByloanNo(String loanNo);

}