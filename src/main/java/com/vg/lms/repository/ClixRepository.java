package com.vg.lms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.ClixAudit;

@Repository("ClixRepository")
public interface ClixRepository extends JpaRepository<ClixAudit, Integer> {
	
	public List<ClixAudit> findBystateNot(String state);

}

