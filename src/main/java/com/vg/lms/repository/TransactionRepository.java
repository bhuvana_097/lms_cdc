package com.vg.lms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.Transaction;

@Repository("TransactionRepository")
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

}
