package com.vg.lms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.SchemeMaster;

@Repository("SchemeRepository")
public interface SchemeRepository extends JpaRepository<SchemeMaster, Integer> {
	
	public SchemeMaster findByschemeId(String schemeId);			

}

