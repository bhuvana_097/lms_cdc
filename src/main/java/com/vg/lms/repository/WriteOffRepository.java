package com.vg.lms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.WriteOff;


@Repository("WriteOffRepository")
public interface WriteOffRepository extends JpaRepository<WriteOff, Integer> {
	
	public WriteOff findById(int id);
	public List<WriteOff> findByloanNo(String loanNo);

}