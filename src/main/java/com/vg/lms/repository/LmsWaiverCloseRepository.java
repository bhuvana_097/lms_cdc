package com.vg.lms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.LmsWaiverClose;

@Repository("LmsWaiverCloseRepository")
public interface LmsWaiverCloseRepository extends JpaRepository<LmsWaiverClose, Integer> {
	public LmsWaiverClose findById(int id);
}
