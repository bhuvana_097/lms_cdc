package com.vg.lms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.IdfcTransaction;

@Repository("IdfcTranRespository")
public interface IdfcTranRespository extends JpaRepository<IdfcTransaction, Integer>{

	public IdfcTransaction findByuName(String uname);
}



