package com.vg.lms.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.Application;
import com.vg.lms.model.Dealer;

@Repository("DealerRepository")
public interface DealerRepository extends JpaRepository<Application, Integer> {
	
	public Dealer findByDealerName(String dealerName);

}
