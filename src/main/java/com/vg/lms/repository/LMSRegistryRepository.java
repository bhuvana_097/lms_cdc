package com.vg.lms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.LMSRegistryModel;
import com.vg.lms.model.UserDeptMaster;

@Repository("LMSRegistryRepository")
public interface LMSRegistryRepository extends JpaRepository<LMSRegistryModel, Integer> {
	public LMSRegistryModel findByactive(boolean active);
	public LMSRegistryModel findByRegistryType(String key);
	public LMSRegistryModel findById(int id);
}
