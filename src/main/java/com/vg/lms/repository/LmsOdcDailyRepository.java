package com.vg.lms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.LmsOdcDaily;


@Repository("LmsOdcDailyRepository")
public interface LmsOdcDailyRepository extends JpaRepository<LmsOdcDaily, Integer> {

	public LmsOdcDaily findById(int id);
	public List<LmsOdcDaily> findByLoanNo(String loanNo);
}
