package com.vg.lms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import com.vg.lms.model.Asset;


@Repository("AssetRepository")
public interface AssetRepository  extends JpaRepository<Asset, Integer> {
	
	public List<Asset> findByengineNo(String engineNo);
	public List<Asset> findBychassisNo(String chassisNo);
	
	//Code edietd by bhuvana for CRM
	@Query(value=" select make from lms_asset where loan_id=?1", nativeQuery=true)
	public String vehiclemake(int loanid); 
	@Query(value=" select model from lms_asset where loan_id=?1", nativeQuery=true)
	public String vehiclemodel(int loanid); 
//	public List<Asset> findByRcNo(String RcNo);
}
