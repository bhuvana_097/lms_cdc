package com.vg.lms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.ApplicationAnnex;

@Repository("ApplicationAnnex")
public interface ApplicationAnnexReposit extends JpaRepository<ApplicationAnnex, Integer> {
	
	public ApplicationAnnex findByAppno(String appno);
	
	

}
