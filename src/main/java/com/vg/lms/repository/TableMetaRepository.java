package com.vg.lms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.TableMeta;

@Repository("TableMetaRepository")
public interface TableMetaRepository extends JpaRepository<TableMeta, Integer> {
	
	public TableMeta findBytableNo(String tableNo);

}
