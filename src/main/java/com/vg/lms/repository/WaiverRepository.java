package com.vg.lms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.Waiver;

@Repository("WaiverRepository")
public interface WaiverRepository extends JpaRepository<Waiver, Integer> {
	
	public List<Waiver> findByloanNo(String loanNo);

}