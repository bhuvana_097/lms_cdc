package com.vg.lms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.IncredAudit;

@Repository("IncredRepository")
public interface IncredRepository extends JpaRepository<IncredAudit, Integer> {
	
	public List<IncredAudit> findBystateNot(String state);

}
