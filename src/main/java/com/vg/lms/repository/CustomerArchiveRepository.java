package com.vg.lms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.Customer;
import com.vg.lms.model.CustomerArchive;

@Repository("CustomerArchiveRepository")
public interface CustomerArchiveRepository extends JpaRepository<CustomerArchive, Integer>{
	
	public Customer findByaadharNo(String aadharNo);
	public Customer findById(String id);
	
}
