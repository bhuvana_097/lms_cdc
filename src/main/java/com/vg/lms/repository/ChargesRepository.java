package com.vg.lms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.Charges;

@Repository("ChargesRepository")
public interface ChargesRepository extends JpaRepository<Charges, Integer> {
	
	public Charges findById(int id);
	public List<Charges> findByloanNo(String loanNo);

}