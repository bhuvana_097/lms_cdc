package com.vg.lms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vg.lms.model.Application;
import com.vg.lms.model.Branch;

@Repository("BranchRepository")
public interface BranchRepository extends JpaRepository<Branch, Integer> {
	
	@Query(value="select branch_name from fe_branchmaster where s_no=?1",nativeQuery=true)
	String findbranch(String branchcode);
	
	

}
