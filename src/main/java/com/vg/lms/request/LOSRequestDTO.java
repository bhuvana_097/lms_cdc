package com.vg.lms.request;

public class LOSRequestDTO extends BaseRequestDTO {
	
	private String applicationNo;

	public String getApplicationNo() {
		return applicationNo;
	}

	public void setApplicationNo(String applicationNo) {
		this.applicationNo = applicationNo;
	}

	@Override
	public String toString() {
		return "LOSRequestDTO [applicationNo=" + applicationNo + ", getAppName()=" + getAppName() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}
	
	

}
