package com.vg.lms.request;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoanRequestDTO extends BaseRequestDTO {
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private int custId;
	private int gurantorId;
	private String applNo;
	private String loanNo;
	private String agmntDate;
	private String firstDueDate;
	private float amount;
	private float tenure;
	private float irr;
	private float procFee;
	private float emi;
	private String advanceEmi;
	private float insuranceAmount;
	private float cashCollCharges;
	private float dealerDisbursementAmount;
	private float downPayment;
	
	private String make;
	private String model;
	private String variant;
	private String loanAgreement;
	private String mandateFile;

	private String assetType;
	private String schemeType;
	private String prodSubcode;
	private float totalCost;
	private float gridPrice;
	private float onroadPrice;
	private float actualLtv;
	private String rcNo;
	private String rcPhoto;
	private String dealerCode;
	private String dealerName;

	private String h1;
	private String h2;
	private String h3;
	private String h4;
	private String h5;

	private String createUser;
	private String createTime;
	private String modifyUser;
	private String modifyTime;
	
	public int getCustId() {
		return custId;
	}
	public void setCustId(int custId) {
		this.custId = custId;
	}
	public int getGurantorId() {
		return gurantorId;
	}
	public void setGurantorId(int gurantorId) {
		this.gurantorId = gurantorId;
	}
	public String getApplNo() {
		return applNo;
	}
	public void setApplNo(String applNo) {
		this.applNo = applNo;
	}
	public String getLoanNo() {
		return loanNo;
	}
	public void setLoanNo(String loanNo) {
		this.loanNo = loanNo;
	}
	public Date getAgmntDate() throws ParseException {
		return sdf.parse(this.agmntDate);
	}
	public void setAgmntDate(String agmntDate) {
		this.agmntDate = agmntDate;
	}
	public void setAgmntDate(Date agmntDate) {
		this.agmntDate = sdf.format(agmntDate);
	}	
	
	public Date getFirstDueDate() throws ParseException {
		return sdf.parse(this.firstDueDate);
	}
	public void setFirstDueDate(String firstDueDate) {
		this.firstDueDate = firstDueDate;
	}
	public void setFirstDueDate(Date firstDueDate) {
		this.firstDueDate = sdf.format(firstDueDate);
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	public float getTenure() {
		return tenure;
	}
	public void setTenure(float tenure) {
		this.tenure = tenure;
	}
	public float getIrr() {
		return irr;
	}
	public void setIrr(float irr) {
		this.irr = irr;
	}
	public float getProcFee() {
		return procFee;
	}
	public void setProcFee(float procFee) {
		this.procFee = procFee;
	}
	public float getEmi() {
		return emi;
	}
	public void setEmi(float emi) {
		this.emi = emi;
	}
	public String getAdvanceEmi() {
		return advanceEmi;
	}
	public void setAdvanceEmi(String advanceEmi) {
		this.advanceEmi = advanceEmi;
	}
	public float getInsuranceAmount() {
		return insuranceAmount;
	}
	public void setInsuranceAmount(float insuranceAmount) {
		this.insuranceAmount = insuranceAmount;
	}
	public float getCashCollCharges() {
		return cashCollCharges;
	}
	public void setCashCollCharges(float cashCollCharges) {
		this.cashCollCharges = cashCollCharges;
	}
	public float getDealerDisbursementAmount() {
		return dealerDisbursementAmount;
	}
	public void setDealerDisbursementAmount(float dealerDisbursementAmount) {
		this.dealerDisbursementAmount = dealerDisbursementAmount;
	}
	public float getDownPayment() {
		return downPayment;
	}
	public void setDownPayment(float downPayment) {
		this.downPayment = downPayment;
	}
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getVariant() {
		return variant;
	}
	public void setVariant(String variant) {
		this.variant = variant;
	}
	public String getLoanAgreement() {
		return loanAgreement;
	}
	public void setLoanAgreement(String loanAgreement) {
		this.loanAgreement = loanAgreement;
	}
	public String getMandateFile() {
		return mandateFile;
	}
	public void setMandateFile(String mandateFile) {
		this.mandateFile = mandateFile;
	}
	public String getAssetType() {
		return assetType;
	}
	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}
	public String getSchemeType() {
		return schemeType;
	}
	public void setSchemeType(String schemeType) {
		this.schemeType = schemeType;
	}
	public String getProdSubcode() {
		return prodSubcode;
	}
	public void setProdSubcode(String prodSubcode) {
		this.prodSubcode = prodSubcode;
	}
	public float getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(float totalCost) {
		this.totalCost = totalCost;
	}
	public float getGridPrice() {
		return gridPrice;
	}
	public void setGridPrice(float gridPrice) {
		this.gridPrice = gridPrice;
	}
	public float getOnroadPrice() {
		return onroadPrice;
	}
	public void setOnroadPrice(float onroadPrice) {
		this.onroadPrice = onroadPrice;
	}
	public float getActualLtv() {
		return actualLtv;
	}
	public void setActualLtv(float actualLtv) {
		this.actualLtv = actualLtv;
	}
	public String getRcNo() {
		return rcNo;
	}
	public void setRcNo(String rcNo) {
		this.rcNo = rcNo;
	}
	public String getRcPhoto() {
		return rcPhoto;
	}
	public void setRcPhoto(String rcPhoto) {
		this.rcPhoto = rcPhoto;
	}
	public String getDealerCode() {
		return dealerCode;
	}
	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}
	public String getDealerName() {
		return dealerName;
	}
	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}
	public String getH1() {
		return h1;
	}
	public void setH1(String h1) {
		this.h1 = h1;
	}
	public String getH2() {
		return h2;
	}
	public void setH2(String h2) {
		this.h2 = h2;
	}
	public String getH3() {
		return h3;
	}
	public void setH3(String h3) {
		this.h3 = h3;
	}
	public String getH4() {
		return h4;
	}
	public void setH4(String h4) {
		this.h4 = h4;
	}
	public String getH5() {
		return h5;
	}
	public void setH5(String h5) {
		this.h5 = h5;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public Date getCreateTime() throws ParseException {
		return sdf.parse(this.createTime);
		//return createTime;
	}
	
	@JsonProperty("createTime")
    public String getCreaTime() {
       return this.createTime;
	}
	
	
	public void setCreateTime(String createTime) {
		//this.createTime = sdf.format(createTime);
		this.createTime = createTime;
	}
	
	public void setCreateTime(Date createTime) {
		this.createTime = sdf.format(createTime);
	}
	public String getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
	public Date getModifyTime() throws ParseException {
		return sdf.parse(this.modifyTime);
	}
	
	@JsonProperty("modifyTime")
    public String getModTime() {
       return this.modifyTime;
	}
	
	public void setModifyTime(String modifyTime) {
		this.modifyTime = modifyTime;
	}
	public void setModifyTime(Date modifyTime) {
		this.modifyTime = sdf.format(modifyTime);
	}
	@Override
	public String toString() {
		return String.format(
				"LoanRequestDTO [custId=%s, gurantorId=%s, applNo=%s, loanNo=%s, agmntDate=%s, firstDueDate=%s, amount=%s, tenure=%s, irr=%s, procFee=%s, emi=%s, advanceEmi=%s, insuranceAmount=%s, cashCollCharges=%s, dealerDisbursementAmount=%s, downPayment=%s, make=%s, model=%s, variant=%s, loanAgreement=%s, mandateFile=%s, assetType=%s, schemeType=%s, prodSubcode=%s, totalCost=%s, gridPrice=%s, onroadPrice=%s, actualLtv=%s, rcNo=%s, rcPhoto=%s, dealerCode=%s, dealerName=%s, h1=%s, h2=%s, h3=%s, h4=%s, h5=%s, createUser=%s, createTime=%s, modifyUser=%s, modifyTime=%s]",
				custId, gurantorId, applNo, loanNo, agmntDate, firstDueDate, amount, tenure, irr, procFee, emi,
				advanceEmi, insuranceAmount, cashCollCharges, dealerDisbursementAmount, downPayment, make, model,
				variant, loanAgreement, mandateFile, assetType, schemeType, prodSubcode, totalCost, gridPrice,
				onroadPrice, actualLtv, rcNo, rcPhoto, dealerCode, dealerName, h1, h2, h3, h4, h5, createUser,
				createTime, modifyUser, modifyTime);
	}

	
}
