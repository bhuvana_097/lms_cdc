package com.vg.lms.request;

public class GetloanIdRequestDTO {
	
	private String loanNo;

	public String getLoanNo() {
		return loanNo;
	}

	public void setPhone1(String loanNo) {
		this.loanNo = loanNo;
	}

	@Override
	public String toString() {
		return String.format(
				"GetloanIdRequestDTO [loanNo=%s, toString()=%s, getClass()=%s, hashCode()=%s]",
				loanNo, super.toString(), getClass(), hashCode());
	}
}
