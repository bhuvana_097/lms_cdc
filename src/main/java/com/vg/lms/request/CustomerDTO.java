package com.vg.lms.request;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerDTO extends BaseRequestDTO {
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	private int id;
    private String name;
    private String phone1;
    private String phone2;
    private String emailId;
    private String dob;
    private int age;
    private String cibilScore;
    private String gender;
    private String religion;
    private String caste;
    private String firstname;
    private String lastname;
    private String fatherName;
    private String motherName;
    private String spouseName;
    private String passportNo;
    private String voterid;
    private String drivingLicense;
    private String rationCard;
    private String panno;
    private String nameInBank;
    private String accountNo;
    private String ifscCode;
    private String acccountType;
    private String maritalStatus;
    private String jobType;
    private String salorself;
    private String profile;
    private String annualIncome;
    private String currentEmi;
    private String loanReason;
    private String education;
    private boolean ownHouse;
    private boolean twoWheeler;
    private boolean fourWheeler;
    private String resiAddr;
    private String resiAddress2;
    private String resiCity;
    private String resiPin;
    private String resiState;
    private String resiPh;
    private String resiMobile;
    private String permAddr;
    private String permAddr2;
    private String permCity;
    private String permState;
    private String permPincode;
    private String offName;
    private String offAddr;
    private String offAddr2;
    private String offCity;
    private String offState;
    private String offPin;
    private String offStd;
    private String offPh;
    private String resiCumOff;
    private String aadharNo;
    private String aadharName;
    private String aadharPhone;
    private String aadhaarEmail;
    private String aadharCo;
    private String aadharHouse;
    private String aadharStreet;
    private String aadharLm;
    private String aadharVtc;
    private String aadharSubDist;
    private String aadharDist;
    private String aadharState;
    private String aadharPo;
    private String aadharDob;
    private String aadharPincode;
    private String aadharGender;
    private String aadharLoc;
    private String panPic;
    private String customerPic;
    private String passportPic;
    private String voteridPic;
    private String dlPic;
    private String rationPic;
    private String h1;
    private String h2;
    private String h3;
    private String h4;
    private String h5;
    private String createUser;
    private String createTime;
    private String modifyUser;
    private String modifyTime;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone1() {
		return phone1;
	}
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	public String getPhone2() {
		return phone2;
	}
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getCibilScore() {
		return cibilScore;
	}
	public void setCibilScore(String cibilScore) {
		this.cibilScore = cibilScore;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getReligion() {
		return religion;
	}
	public void setReligion(String religion) {
		this.religion = religion;
	}
	public String getCaste() {
		return caste;
	}
	public void setCaste(String caste) {
		this.caste = caste;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public String getMotherName() {
		return motherName;
	}
	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}
	public String getSpouseName() {
		return spouseName;
	}
	public void setSpouseName(String spouseName) {
		this.spouseName = spouseName;
	}
	public String getPassportNo() {
		return passportNo;
	}
	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}
	public String getVoterid() {
		return voterid;
	}
	public void setVoterid(String voterid) {
		this.voterid = voterid;
	}
	public String getDrivingLicense() {
		return drivingLicense;
	}
	public void setDrivingLicense(String drivingLicense) {
		this.drivingLicense = drivingLicense;
	}
	public String getRationCard() {
		return rationCard;
	}
	public void setRationCard(String rationCard) {
		this.rationCard = rationCard;
	}
	public String getPanno() {
		return panno;
	}
	public void setPanno(String panno) {
		this.panno = panno;
	}
	public String getNameInBank() {
		return nameInBank;
	}
	public void setNameInBank(String nameInBank) {
		this.nameInBank = nameInBank;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public String getIfscCode() {
		return ifscCode;
	}
	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}
	public String getAcccountType() {
		return acccountType;
	}
	public void setAcccountType(String acccountType) {
		this.acccountType = acccountType;
	}
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public String getJobType() {
		return jobType;
	}
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	public String getSalorself() {
		return salorself;
	}
	public void setSalorself(String salorself) {
		this.salorself = salorself;
	}
	public String getAnnualIncome() {
		return annualIncome;
	}
	public void setAnnualIncome(String annualIncome) {
		this.annualIncome = annualIncome;
	}
	public String getCurrentEmi() {
		return currentEmi;
	}
	public void setCurrentEmi(String currentEmi) {
		this.currentEmi = currentEmi;
	}
	public String getLoanReason() {
		return loanReason;
	}
	public void setLoanReason(String loanReason) {
		this.loanReason = loanReason;
	}
	public String getEducation() {
		return education;
	}
	public void setEducation(String education) {
		this.education = education;
	}
	public boolean isOwnHouse() {
		return ownHouse;
	}
	public void setOwnHouse(boolean ownHouse) {
		this.ownHouse = ownHouse;
	}
	public boolean isTwoWheeler() {
		return twoWheeler;
	}
	public void setTwoWheeler(boolean twoWheeler) {
		this.twoWheeler = twoWheeler;
	}
	public boolean isFourWheeler() {
		return fourWheeler;
	}
	public void setFourWheeler(boolean fourWheeler) {
		this.fourWheeler = fourWheeler;
	}
	public String getResiAddr() {
		return resiAddr;
	}
	public void setResiAddr(String resiAddr) {
		this.resiAddr = resiAddr;
	}
	public String getResiAddress2() {
		return resiAddress2;
	}
	public void setResiAddress2(String resiAddress2) {
		this.resiAddress2 = resiAddress2;
	}
	public String getResiCity() {
		return resiCity;
	}
	public void setResiCity(String resiCity) {
		this.resiCity = resiCity;
	}
	public String getResiPin() {
		return resiPin;
	}
	public void setResiPin(String resiPin) {
		this.resiPin = resiPin;
	}
	public String getResiState() {
		return resiState;
	}
	public void setResiState(String resiState) {
		this.resiState = resiState;
	}
	public String getResiPh() {
		return resiPh;
	}
	public void setResiPh(String resiPh) {
		this.resiPh = resiPh;
	}
	public String getResiMobile() {
		return resiMobile;
	}
	public void setResiMobile(String resiMobile) {
		this.resiMobile = resiMobile;
	}
	public String getPermAddr() {
		return permAddr;
	}
	public void setPermAddr(String permAddr) {
		this.permAddr = permAddr;
	}
	public String getPermAddr2() {
		return permAddr2;
	}
	public void setPermAddr2(String permAddr2) {
		this.permAddr2 = permAddr2;
	}
	public String getPermCity() {
		return permCity;
	}
	public void setPermCity(String permCity) {
		this.permCity = permCity;
	}
	public String getPermState() {
		return permState;
	}
	public void setPermState(String permState) {
		this.permState = permState;
	}
	public String getPermPincode() {
		return permPincode;
	}
	public void setPermPincode(String permPincode) {
		this.permPincode = permPincode;
	}
	public String getOffName() {
		return offName;
	}
	public void setOffName(String offName) {
		this.offName = offName;
	}
	public String getOffAddr() {
		return offAddr;
	}
	public void setOffAddr(String offAddr) {
		this.offAddr = offAddr;
	}
	public String getOffAddr2() {
		return offAddr2;
	}
	public void setOffAddr2(String offAddr2) {
		this.offAddr2 = offAddr2;
	}
	public String getOffCity() {
		return offCity;
	}
	public void setOffCity(String offCity) {
		this.offCity = offCity;
	}
	public String getOffState() {
		return offState;
	}
	public void setOffState(String offState) {
		this.offState = offState;
	}
	public String getOffPin() {
		return offPin;
	}
	public void setOffPin(String offPin) {
		this.offPin = offPin;
	}
	public String getOffStd() {
		return offStd;
	}
	public void setOffStd(String offStd) {
		this.offStd = offStd;
	}
	public String getOffPh() {
		return offPh;
	}
	public void setOffPh(String offPh) {
		this.offPh = offPh;
	}
	public String getResiCumOff() {
		return resiCumOff;
	}
	public void setResiCumOff(String resiCumOff) {
		this.resiCumOff = resiCumOff;
	}
	public String getAadharNo() {
		return aadharNo;
	}
	public void setAadharNo(String aadharNo) {
		this.aadharNo = aadharNo;
	}
	public String getAadharName() {
		return aadharName;
	}
	public void setAadharName(String aadharName) {
		this.aadharName = aadharName;
	}
	public String getAadharPhone() {
		return aadharPhone;
	}
	public void setAadharPhone(String aadharPhone) {
		this.aadharPhone = aadharPhone;
	}
	public String getAadhaarEmail() {
		return aadhaarEmail;
	}
	public void setAadhaarEmail(String aadhaarEmail) {
		this.aadhaarEmail = aadhaarEmail;
	}
	public String getAadharCo() {
		return aadharCo;
	}
	public void setAadharCo(String aadharCo) {
		this.aadharCo = aadharCo;
	}
	public String getAadharHouse() {
		return aadharHouse;
	}
	public void setAadharHouse(String aadharHouse) {
		this.aadharHouse = aadharHouse;
	}
	public String getAadharStreet() {
		return aadharStreet;
	}
	public void setAadharStreet(String aadharStreet) {
		this.aadharStreet = aadharStreet;
	}
	public String getAadharLm() {
		return aadharLm;
	}
	public void setAadharLm(String aadharLm) {
		this.aadharLm = aadharLm;
	}
	public String getAadharVtc() {
		return aadharVtc;
	}
	public void setAadharVtc(String aadharVtc) {
		this.aadharVtc = aadharVtc;
	}
	public String getAadharSubDist() {
		return aadharSubDist;
	}
	public void setAadharSubDist(String aadharSubDist) {
		this.aadharSubDist = aadharSubDist;
	}
	public String getAadharDist() {
		return aadharDist;
	}
	public void setAadharDist(String aadharDist) {
		this.aadharDist = aadharDist;
	}
	public String getAadharState() {
		return aadharState;
	}
	public void setAadharState(String aadharState) {
		this.aadharState = aadharState;
	}
	public String getAadharPo() {
		return aadharPo;
	}
	public void setAadharPo(String aadharPo) {
		this.aadharPo = aadharPo;
	}
	public String getAadharDob() {
		return aadharDob;
	}
	public void setAadharDob(String aadharDob) {
		this.aadharDob = aadharDob;
	}
	public String getAadharPincode() {
		return aadharPincode;
	}
	public void setAadharPincode(String aadharPincode) {
		this.aadharPincode = aadharPincode;
	}
	public String getAadharGender() {
		return aadharGender;
	}
	public void setAadharGender(String aadharGender) {
		this.aadharGender = aadharGender;
	}
	public String getAadharLoc() {
		return aadharLoc;
	}
	public void setAadharLoc(String aadharLoc) {
		this.aadharLoc = aadharLoc;
	}
	public String getPanPic() {
		return panPic;
	}
	public void setPanPic(String panPic) {
		this.panPic = panPic;
	}
	public String getCustomerPic() {
		return customerPic;
	}
	public void setCustomerPic(String customerPic) {
		this.customerPic = customerPic;
	}
	public String getPassportPic() {
		return passportPic;
	}
	public void setPassportPic(String passportPic) {
		this.passportPic = passportPic;
	}
	public String getVoteridPic() {
		return voteridPic;
	}
	public void setVoteridPic(String voteridPic) {
		this.voteridPic = voteridPic;
	}
	public String getDlPic() {
		return dlPic;
	}
	public void setDlPic(String dlPic) {
		this.dlPic = dlPic;
	}
	public String getRationPic() {
		return rationPic;
	}
	public void setRationPic(String rationPic) {
		this.rationPic = rationPic;
	}
	public String getH1() {
		return h1;
	}
	public void setH1(String h1) {
		this.h1 = h1;
	}
	public String getH2() {
		return h2;
	}
	public void setH2(String h2) {
		this.h2 = h2;
	}
	public String getH3() {
		return h3;
	}
	public void setH3(String h3) {
		this.h3 = h3;
	}
	public String getH4() {
		return h4;
	}
	public void setH4(String h4) {
		this.h4 = h4;
	}
	public String getH5() {
		return h5;
	}
	public void setH5(String h5) {
		this.h5 = h5;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}	
	public Date getCreateTime() throws ParseException {
		return sdf.parse(this.createTime);
		//return createTime;
	}
	
	@JsonProperty("createTime")
    public String getCreaTime() {
       return this.createTime;
	}
	
	public void setCreateTime(String createTime) {
		//this.createTime = sdf.format(createTime);
		this.createTime = createTime;
	}
	
	public void setCreateTime(Date createTime) {
		this.createTime = sdf.format(createTime);
	}
	
	public String getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
	public Date getModifyTime() throws ParseException {
		return sdf.parse(this.modifyTime);
	}
		
	@JsonProperty("modifyTime")
    public String getModTime() {
       return this.modifyTime;
	}
	
	public void setModifyTime(String modifyTime) {
		this.modifyTime = modifyTime;
	}
	public void setModifyTime(Date modifyTime) {
		this.modifyTime = sdf.format(modifyTime);
	}
	
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}
	@Override
	public String toString() {
		return String.format(
				"CustomerDTO [id=%s, name=%s, phone1=%s, phone2=%s, emailId=%s, dob=%s, age=%s, cibilScore=%s, gender=%s, religion=%s, caste=%s, firstname=%s, lastname=%s, fatherName=%s, motherName=%s, spouseName=%s, passportNo=%s, voterid=%s, drivingLicense=%s, rationCard=%s, panno=%s, nameInBank=%s, accountNo=%s, ifscCode=%s, acccountType=%s, maritalStatus=%s, jobType=%s, salorself=%s, profile=%s, annualIncome=%s, currentEmi=%s, loanReason=%s, education=%s, ownHouse=%s, twoWheeler=%s, fourWheeler=%s, resiAddr=%s, resiAddress2=%s, resiCity=%s, resiPin=%s, resiState=%s, resiPh=%s, resiMobile=%s, permAddr=%s, permAddr2=%s, permCity=%s, permState=%s, permPincode=%s, offName=%s, offAddr=%s, offAddr2=%s, offCity=%s, offState=%s, offPin=%s, offStd=%s, offPh=%s, resiCumOff=%s, aadharNo=%s, aadharName=%s, aadharPhone=%s, aadhaarEmail=%s, aadharCo=%s, aadharHouse=%s, aadharStreet=%s, aadharLm=%s, aadharVtc=%s, aadharSubDist=%s, aadharDist=%s, aadharState=%s, aadharPo=%s, aadharDob=%s, aadharPincode=%s, aadharGender=%s, aadharLoc=%s, panPic=%s, customerPic=%s, passportPic=%s, voteridPic=%s, dlPic=%s, rationPic=%s, h1=%s, h2=%s, h3=%s, h4=%s, h5=%s, createUser=%s, createTime=%s, modifyUser=%s, modifyTime=%s, getAppName()=%s, toString()=%s, getClass()=%s, hashCode()=%s]",
				id, name, phone1, phone2, emailId, dob, age, cibilScore, gender, religion, caste, firstname, lastname,
				fatherName, motherName, spouseName, passportNo, voterid, drivingLicense, rationCard, panno, nameInBank,
				accountNo, ifscCode, acccountType, maritalStatus, jobType, salorself, profile, annualIncome, currentEmi,
				loanReason, education, ownHouse, twoWheeler, fourWheeler, resiAddr, resiAddress2, resiCity, resiPin,
				resiState, resiPh, resiMobile, permAddr, permAddr2, permCity, permState, permPincode, offName, offAddr,
				offAddr2, offCity, offState, offPin, offStd, offPh, resiCumOff, aadharNo, aadharName, aadharPhone,
				aadhaarEmail, aadharCo, aadharHouse, aadharStreet, aadharLm, aadharVtc, aadharSubDist, aadharDist,
				aadharState, aadharPo, aadharDob, aadharPincode, aadharGender, aadharLoc, panPic, customerPic,
				passportPic, voteridPic, dlPic, rationPic, h1, h2, h3, h4, h5, createUser, createTime, modifyUser,
				modifyTime, getAppName(), super.toString(), getClass(), hashCode());
	}
	
	
    


}
