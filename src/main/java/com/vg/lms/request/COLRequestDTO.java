package com.vg.lms.request;

public class COLRequestDTO extends BaseRequestDTO {
		
	private String coLender;

	public String getCoLender() {
		return coLender;
	}

	public void setCoLender(String acoLender) {
		this.coLender = acoLender;
	}

	@Override
	public String toString() {
		return "COLRequestDTO [coLender=" + coLender + ",  toString()="
				+ super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}

}
