package com.vg.lms.request;

public class ClixRequest {
	
	private String loanNo;

	public String getLoanNo() {
		return loanNo;
	}

	public void setLoanNo(String loanNo) {
		this.loanNo = loanNo;
	}

	@Override
	public String toString() {
		return "ClixRequest [loanNo=" + loanNo + ", getLoanNo()=" + getLoanNo() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}

}
