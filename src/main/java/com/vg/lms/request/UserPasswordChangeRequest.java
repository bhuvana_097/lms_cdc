package com.vg.lms.request;

import java.util.Date;

public class UserPasswordChangeRequest {

	private int id;
	private String code;
	private String name;
	private String passwd;
	private String newpasswd;
	private String cnfrmpasswd;
	
	private String modifyUser;
	private Date modifyTime;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getNewpasswd() {
		return newpasswd;
	}
	public void setNewpasswd(String newpasswd) {
		this.newpasswd = newpasswd;
	}
	public String getCnfrmpasswd() {
		return cnfrmpasswd;
	}
	public void setCnfrmpasswd(String cnfrmpasswd) {
		this.cnfrmpasswd = cnfrmpasswd;
	}
	public String getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
	public Date getModifyTime() {
		return modifyTime;
	}
	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}
	@Override
	public String toString() {
		return "UserPasswordChangeRequest [id=" + id + ", code=" + code + ", name=" + name + ", passwd=" + passwd
				+ ", newpasswd=" + newpasswd + ", cnfrmpasswd=" + cnfrmpasswd + ", modifyUser=" + modifyUser
				+ ", modifyTime=" + modifyTime + ", getId()=" + getId() + ", getCode()=" + getCode() + ", getName()="
				+ getName() + ", getPasswd()=" + getPasswd() + ", getNewpasswd()=" + getNewpasswd()
				+ ", getCnfrmpasswd()=" + getCnfrmpasswd() + ", getModifyUser()=" + getModifyUser()
				+ ", getModifyTime()=" + getModifyTime() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
	
}


