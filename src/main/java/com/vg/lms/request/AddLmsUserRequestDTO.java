package com.vg.lms.request;
import java.util.Date;

public class AddLmsUserRequestDTO {
	private int id;
	private String code;
	private String name;
	private String passwd;
	private String dept;
	private String hierLevel;
	private String status;
	private String role;
	private String createUser;
	private Date createTime;
	private String modifyUser;
	private Date modifyTime;
	private String sessionID;
	private Date lastLogin; 
	
	private String reason;
	private String branch;
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getHierLevel() {
		return hierLevel;
	}
	public void setHierLevel(String hierLevel) {
		this.hierLevel = hierLevel;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
	public Date getModifyTime() {
		return modifyTime;
	}
	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	public Date getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}
	
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	@Override
	public String toString() {
		return "AddLmsUserRequestDTO [id=" + id + ", code=" + code + ", name=" + name + ", passwd=" + passwd + ", dept="
				+ dept + ", hierLevel=" + hierLevel + ", status=" + status + ", role=" + role + ", createUser="
				+ createUser + ", createTime=" + createTime + ", modifyUser=" + modifyUser + ", modifyTime="
				+ modifyTime + ", sessionID=" + sessionID + ", lastLogin=" + lastLogin + ", reason=" + reason
				+ ", branch=" + branch + ", getId()=" + getId() + ", getCode()=" + getCode() + ", getName()="
				+ getName() + ", getPasswd()=" + getPasswd() + ", getDept()=" + getDept() + ", getHierLevel()="
				+ getHierLevel() + ", getRole()=" + getRole() + ", getCreateUser()=" + getCreateUser()
				+ ", getCreateTime()=" + getCreateTime() + ", getModifyUser()=" + getModifyUser() + ", getModifyTime()="
				+ getModifyTime() + ", getSessionID()=" + getSessionID() + ", getStatus()=" + getStatus()
				+ ", getLastLogin()=" + getLastLogin() + ", getReason()=" + getReason() + ", getBranch()=" + getBranch()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}
	

}
