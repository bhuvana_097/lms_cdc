package com.vg.lms.request;

public class GetCustomerRequestDTO extends BaseRequestDTO {
	
	private String aadhaarNo;

	public String getAadhaarNo() {
		return aadhaarNo;
	}

	public void setAadhaarNo(String aadhaarNo) {
		this.aadhaarNo = aadhaarNo;
	}

	@Override
	public String toString() {
		return String.format(
				"GetCustomerRequestDTO [aadhaarNo=%s, getAppName()=%s, toString()=%s, getClass()=%s, hashCode()=%s]",
				aadhaarNo, getAppName(), super.toString(), getClass(), hashCode());
	}
}
