package com.vg.lms.request;

public class GetLoanRequestDTO extends BaseRequestDTO {
	
	private int loanId;

	public int getLoanId() {
		return loanId;
	}

	public void setLoanId(int loanId) {
		this.loanId = loanId;
	}

	@Override
	public String toString() {
		return String.format(
				"GetLoanRequestDTO [loanId=%s, getAppName()=%s, toString()=%s, getClass()=%s, hashCode()=%s]", loanId,
				getAppName(), super.toString(), getClass(), hashCode());
	}

	
	

}
