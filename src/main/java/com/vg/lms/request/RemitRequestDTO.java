package com.vg.lms.request;

public class RemitRequestDTO extends BaseRequestDTO {
	
	private String remitId;

	public String getRemitId() {
		return remitId;
	}

	public void setRemitId(String remitId) {
		this.remitId = remitId;
	}

	@Override
	public String toString() {
		return String.format("RemitRequestDTO [remitId=%s]", remitId);
	}

}
