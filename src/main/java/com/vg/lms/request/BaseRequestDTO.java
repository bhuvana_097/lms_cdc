package com.vg.lms.request;

public class BaseRequestDTO {
	
	private String appName;

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	@Override
	public String toString() {
		return String.format("BaseRequestDTO [appName=%s]", appName);
	}

}
