package com.vg.lms.request;

public class GetCustomerIdRequestDTO {
	
	private String phone1;

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	@Override
	public String toString() {
		return String.format(
				"GetCustomerIdRequestDTO [phone1=%s, toString()=%s, getClass()=%s, hashCode()=%s]",
				phone1, super.toString(), getClass(), hashCode());
	}
}
