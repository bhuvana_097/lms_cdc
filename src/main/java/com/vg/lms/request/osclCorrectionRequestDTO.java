package com.vg.lms.request;

import java.util.Date;

public class osclCorrectionRequestDTO {
	
	private int id, loanId;
	private float dealerDisbursementAmount, emi, processingFee, scCharge;
	private String createUser;
	private Date createTime;
	private String modifyUser;
	private Date modifyTime;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getLoanId() {
		return loanId;
	}
	public void setLoanId(int loanId) {
		this.loanId = loanId;
	}
	public float getDealerDisbursementAmount() {
		return dealerDisbursementAmount;
	}
	public void setDealerDisbursementAmount(float dealerDisbursementAmount) {
		this.dealerDisbursementAmount = dealerDisbursementAmount;
	}
	public float getEmi() {
		return emi;
	}
	public void setEmi(float emi) {
		this.emi = emi;
	}
	public float getProcessingFee() {
		return processingFee;
	}
	public void setProcessingFee(float processingFee) {
		this.processingFee = processingFee;
	}
	public float getScCharge() {
		return scCharge;
	}
	public void setScCharge(float scCharge) {
		this.scCharge = scCharge;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getModifyUser() {
		return modifyUser;
	}
	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}
	public Date getModifyTime() {
		return modifyTime;
	}
	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}
	@Override
	public String toString() {
		return "osclCorrectionRequestDTO [id=" + id + ", loanId=" + loanId + ", dealerDisbursementAmount="
				+ dealerDisbursementAmount + ", emi=" + emi + ", processingFee=" + processingFee + ", scCharge="
				+ scCharge + ", createUser=" + createUser + ", createTime=" + createTime + ", modifyUser=" + modifyUser
				+ ", modifyTime=" + modifyTime + ", getId()=" + getId() + ", getLoanId()=" + getLoanId()
				+ ", getDealerDisbursementAmount()=" + getDealerDisbursementAmount() + ", getEmi()=" + getEmi()
				+ ", getProcessingFee()=" + getProcessingFee() + ", getScCharge()=" + getScCharge()
				+ ", getCreateUser()=" + getCreateUser() + ", getCreateTime()=" + getCreateTime() + ", getModifyUser()="
				+ getModifyUser() + ", getModifyTime()=" + getModifyTime() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}
	

}
