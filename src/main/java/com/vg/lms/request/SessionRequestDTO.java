package com.vg.lms.request;

public class SessionRequestDTO {
	
	private String sessionID;
	private String code;
	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	@Override
	public String toString() {
		return "SessionRequestDTO [sessionID=" + sessionID + ", code=" + code + ", getSessionID()=" + getSessionID()
				+ ", getCode()=" + getCode() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

}

