package com.vg.lms.request;

public class SearchLmsUserRequest {
	private String code,
	name,
	passwd,
	Department,
	hier_level,
	status;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String getDepartment() {
		return Department;
	}

	public void setDepartment(String department) {
		Department = department;
	}

	public String getHier_level() {
		return hier_level;
	}

	public void setHier_level(String hier_level) {
		this.hier_level = hier_level;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "SearchLmsUserRequest [code=" + code + ", name=" + name + ", passwd=" + passwd + ", Department="
				+ Department + ", hier_level=" + hier_level + ", status=" + status + ", getCode()=" + getCode()
				+ ", getName()=" + getName() + ", getPasswd()=" + getPasswd() + ", getDepartment()=" + getDepartment()
				+ ", getHier_level()=" + getHier_level() + ", getStatus()=" + getStatus() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}


}
