package com.vg.lms.colend;


import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vg.lms.model.Customer;
import com.vg.lms.model.Loan;
import com.vg.colend.model.Asset;
import com.vg.colend.model.Receipt;
import com.vg.colend.request.AssetRequest;
import com.vg.colend.request.CustomerRequest;
import com.vg.colend.request.LoanRequest;
import com.vg.colend.service.VGColendClient;

@Service
public class ColendServiceImpl implements ColendService{
	private final Logger log = LoggerFactory.getLogger(ColendServiceImpl.class);
	
	SimpleDateFormat DATE_YMD = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat DATE_DMY = new SimpleDateFormat("dd-MM-yyyy");
	@Override
	public void pushToColend(Loan loan ) {
		VGColendClient colendClient = new VGColendClient("ORFIL", "b3JmaWxAMTIz");
//		add customer details
		
		CustomerRequest customerRequest = new CustomerRequest();
		Customer customer = loan.getCustomer();
		
		
		customerRequest.setFirstName(customer.getFirstname());
		customerRequest.setLastName(customer.getLastname());
		customerRequest.setUniqueCustID(customer.getAadharNo());
		customerRequest.setAadharNo(customer.getAadharNo());
		customerRequest.setPanNo(customer.getPanno());
		Date date = null;
		try {
			String dobOrang = customer.getDob();
			date = DATE_DMY.parse(dobOrang);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String dob = DATE_YMD.format(date);
		customerRequest.setDOB(dob);//yyyy-mm-dd format
		customerRequest.setMobileNumber(customer.getPhone1());
		customerRequest.setDoorNo(customer.getResiAddr());
		customerRequest.setStreet(customer.getResiAddress2());
		customerRequest.setCity(customer.getResiCity());
		customerRequest.setState(customer.getResiState());
		customerRequest.setPincode(customer.getResiPin());
		customerRequest.setFatherName(customer.getFatherName());
		customerRequest.setMotherName(customer.getMotherName());
		customerRequest.setJobProfile(customer.getProfile());
		customerRequest.setJobType(customer.getJobType());
		customerRequest.setEducation(customer.getEducation());
		customerRequest.setEmail(customer.getEmailId());
		customerRequest.setGender(customer.getGender());
		try {
			com.vg.colend.model.Customer customerResponse = colendClient.Customers.create(customerRequest);
			log.debug("ColenderServImpl customerResponse=>" + customerResponse );
			pushLoanDetails(colendClient, customerResponse.getCustID(), loan);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.error("ColenderServImpl: Error in push customer");
		}
	}
	//add loan details
	private void pushLoanDetails(VGColendClient colendClient, int custId, Loan loan){
		LoanRequest loanRequest = new LoanRequest();
		
		loanRequest.setCustID(custId);
		loanRequest.setFirstName(loan.getCustomer().getFirstname());
		loanRequest.setLastName(loan.getCustomer().getLastname());
		loanRequest.setLoanNo(loan.getLoanNo());
		loanRequest.setLoanAmount(loan.getAmount());
		if(loan.getCoLender() != null
				&& !loan.getCoLender().equalsIgnoreCase("") 
				&& !loan.getCoLender().equalsIgnoreCase("none")){
			loanRequest.setCoLenderCode(loan.getCoLender());
			loanRequest.setCoLenderProgram(loan.getCoLender());
		}
		if(loan.getCoLenderDisposition() != null
				&& !loan.getCoLenderDisposition().equalsIgnoreCase("") 
				&& !loan.getCoLenderDisposition().equalsIgnoreCase("none")){
			loanRequest.setCoLenderCode(loan.getCoLenderDisposition());
			loanRequest.setCoLenderProgram(loan.getCoLenderDisposition());
		}
		
		//loanRequest.setCoLenderProgram(loan.getCoLenderDisposition());
		loanRequest.setCoLenderProgramVersion(1);
		loanRequest.setLoanType("TW");
		loanRequest.setIrr(loan.getIrr());
		loanRequest.setEmi(loan.getEmi());
		loanRequest.setAdvanceEMICount((byte) loan.getAdvanceEmi());
		loanRequest.setAgmntDate(DATE_YMD.format(loan.getAgmntDate()));
		loanRequest.setTenure((byte) loan.getTenure());
		loanRequest.setPaymentMode(loan.getRepayMode());
		loanRequest.setTotalProcessingFee(loan.getProcFee());
		loanRequest.setTotalCashModeFee(loan.getCashCollCharges());
		loanRequest.setTotalInsuranceFee(loan.getInsuranceAmount());
		loanRequest.setCreatedBy("ORFIL");
		
		try {
			com.vg.colend.model.Loan loanResponse = colendClient.Loans.add(loanRequest);
			pushAsset(colendClient,  loanResponse.getLoanID(), loan);
			log.debug("ColenderServImpl loanResponse=>" + loanResponse );
			
		} catch (IOException e) {
			log.error("ColenderServImpl: Error in push loan");
		}
		
	}
	
	
	private void pushAsset(VGColendClient colendClient, int loanId, Loan loan) throws IOException{
		com.vg.lms.model.Asset assetDetails = loan.getAsset();
		AssetRequest assetRequest = new AssetRequest();
		assetRequest.setLoanID(loanId);
		assetRequest.setLoanNo(loan.getLoanNo());
		assetRequest.setLoanAmount(loan.getAmount());
		assetRequest.setMake(assetDetails.getMake());
		assetRequest.setModel(assetDetails.getModel());
		assetRequest.setVarient(assetDetails.getVariant());
		assetRequest.setLoanAmount(assetDetails.getTotalCost());
		assetRequest.setOnRoadPrice(assetDetails.getOnroadPrice());
		assetRequest.setGridPrice(assetDetails.getGridPrice());
		assetRequest.setActualLTV(assetDetails.getActualLtv());
		assetRequest.setEngineNo(assetDetails.getEngineNo());
		assetRequest.setChassisNo(assetDetails.getChassisNo());
		assetRequest.setAssetType(assetDetails.getAssetType());
		colendClient.Loans.Assets.add(assetRequest);
		
	}

	



}
