package com.vg.lms.colend;


import com.vg.lms.model.Customer;
import com.vg.lms.model.Loan;

public interface ColendService {
	public void pushToColend(Loan loan);
}
