package com.vg.lms;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.session.HttpSessionEventPublisher;

import com.vg.lms.bean.UserBean;
import com.vg.lms.model.UserDeptMaster;
import com.vg.lms.model.UserMaster;
import com.vg.lms.repository.UserDeptMasterRepository;
import com.vg.lms.service.CustomLogoutSuccessHandler;
import com.vg.lms.service.UserDetailsServiceImpl;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	
	@Autowired
	private UserDeptMasterRepository userDeptMasterRepository;
	 @Autowired
	    UserDetailsServiceImpl userDetailsService;
	
	private final Logger log = LoggerFactory.getLogger(WebSecurityConfig.class);
	/*@Autowired
    private UserDetailsService userDetailsService;

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.authorizeRequests()
		.antMatchers("/resources/**", "/imgs/*", "/styles/**").permitAll()
		.anyRequest().authenticated()
		.and()
		.formLogin()
		.defaultSuccessUrl("/list")
		.loginPage("/login")
		.loginProcessingUrl("/performLogin")
		.permitAll()
		.and()
		.logout()
		.permitAll();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		//auth.userDetailsService(userDetailsService);
		auth.inMemoryAuthentication()
		.withUser("admin").password("admin123").roles("USER");
	}*/



	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/resources/**", "/imgs/*", "/styles/**", "/pushToLMS", "/IdfcTransaction","/srchuser","/pushToCol","/getCustomerData","/getLoanData","/getCustomerLoanData","/getLoanDatawithMobileNo");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		
				
		List<UserDeptMaster> userDeptMasterList = userDeptMasterRepository.findAll();
		List<String> userDept = new ArrayList<>();
		//String[] roles = new String[]{"OPERATIONS", "SYSADMIN", "FINANCE"};
		UserMaster usermast = new UserMaster();
		System.out.println(usermast.getStatus());
		
		for (UserDeptMaster userDeptMaster : userDeptMasterList) {
			if(userDeptMaster.isActive()){
				userDept.add(userDeptMaster.getDeptName().toUpperCase());
				userDept.add(userDeptMaster.getDeptName().toUpperCase() + "-1");
				userDept.add(userDeptMaster.getDeptName().toUpperCase() + "-2");
				userDept.add(userDeptMaster.getDeptName().toUpperCase() + "-3");
				userDept.add(userDeptMaster.getDeptName().toUpperCase() + "-4");
				userDept.add(userDeptMaster.getDeptName().toUpperCase() + "-5");
			}
		}
		
		
		String roles[]=userDept.toArray(new String[userDept.size()]);
		
		http.authorizeRequests().antMatchers("/").hasAnyRole(roles)
		.anyRequest().authenticated()
		.and()
		//.formLogin().loginPage("/login").defaultSuccessUrl("/list", true).permitAll()
		.formLogin().loginPage("/login").defaultSuccessUrl("/LandingPage", true).permitAll()
		.usernameParameter("username").passwordParameter("password")
		.and().exceptionHandling().accessDeniedPage("/accessDenied.jsp")
		//.and().logout().logoutSuccessHandler(logoutSuccessHandler()).permitAll();
		.and().logout().permitAll();
		
		String s =http.authorizeRequests().toString();
		UserBean usrbean = new UserBean();
		usrbean.setLmsusername(s);
		
		
        
		http.csrf().disable();
	}
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder authenticationMgr) throws Exception {
		
		authenticationMgr.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
		
	}

	/*@Autowired
	public void configureGlobal(AuthenticationManagerBuilder authenticationMgr) throws Exception {
		List<UserMaster> userMasterList = userMasterRepository.findAll();
		for (UserMaster userMaster : userMasterList) {
			log.info("Login userMaster result : " + userMaster.toString());
			SessionDetails sessionDetails = new SessionDetails();
			sessionDetails.setUserName(userMaster.getCode());
			sessionDetails.setPassword(userMaster.getPasswd());
			//authenticationMgr.jdbcAuthentication().usersByUsernameQuery("select code,passwd from lms_user_master where code='?'").authoritiesByUsernameQuery("select code,dept from lms_user_master where code='?'");
			authenticationMgr.inMemoryAuthentication().withUser(userMaster.getCode()).password("{noop}" + userMaster.getPasswd()).authorities("ROLE_" + userMaster.getDept().toUpperCase());
		}
		 authenticationMgr.inMemoryAuthentication().withUser("admin").password("{noop}admin123").authorities("ROLE_ADMIN").and()
	        .withUser("ops").password("{noop}admin123").authorities("ROLE_USER", "ROLE_ADMIN").and().withUser("ashwin").password("{noop}ashwin@123").authorities("ROLE_USER");
	}*/
	
	@Bean
    public BCryptPasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }
	
	/*@Bean
	public HttpSessionEventPublisher httpSessionEventPublisher() {
	    return new HttpSessionEventPublisher();
	}*/
	
	@Bean
	public LogoutSuccessHandler logoutSuccessHandler() {
		return (LogoutSuccessHandler) new CustomLogoutSuccessHandler();
		
	}
}