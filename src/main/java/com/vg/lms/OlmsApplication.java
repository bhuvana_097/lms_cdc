package com.vg.lms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class OlmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(OlmsApplication.class, args);
	}
	
	
}
