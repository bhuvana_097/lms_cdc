package com.vg.lms.docgen;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vg.lms.docgen.model.RepaymentScheduleDataModel;
import com.vg.lms.docgen.model.RepaymentScheduleModel;
import com.vg.lms.model.Asset;
import com.vg.lms.model.Loan;
import com.vg.lms.model.Receivable;
import com.vg.lms.repository.LoanRepository;


@Component
public class GenerateRepaymentSchedulePDF {

	@Autowired
	private LoanRepository loanRepository;


	public String generateRepaymentSchedule(int loanId) {

		System.out.println("Generating RepaymentSchedule");

		String templateFilePath ="/FRNDLN/template/";
		//String templateFilePath ="/home/orange/git/lms_cdc/FLLMS/FRNDLN/template/";

		String PRESCRIPTION_URL = "Repayment_schedule.xsl";
		String filePath = "/FRNDLN/docs/";
		//String filePath = "/home/orange/git/lms_cdc/FLLMS/FRNDLN/docs/";
		String fileName = "RepaymentSchedule";
		
		Loan loan = loanRepository.findById(loanId).get();
		
		RepaymentScheduleDataModel repaymentScheduleDataModel = new RepaymentScheduleDataModel();
		repaymentScheduleDataModel.setAggreNo(loan.getLoanNo());
		repaymentScheduleDataModel.setFinanceAmt(String.valueOf(loan.getAmount()));
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		repaymentScheduleDataModel.setLetterDate(sdf.format(new Date()));
		
		if(loan.getLoanNo().contains("BL"))
		{
			repaymentScheduleDataModel.setLoanType("BUSINESS LOAN");
		}else {
			String loanType = loan.getLoanNo().contains("TW")? "TWO WHEELER" : "SWIFT CASH";
			repaymentScheduleDataModel.setLoanType(loanType);	
		}
		
		repaymentScheduleDataModel.setName(loan.getCustomer().getName());
		repaymentScheduleDataModel.setTenure(String.valueOf(loan.getTenure()));
		//repaymentScheduleDataModel.setTotalInstl(String.valueOf((loan.getTenure() * loan.getEmi())));
		//repaymentScheduleDataModel.setTotDue(totDue);
		//repaymentScheduleDataModel.setTotOsPrincipal(String.valueOf(loan.getFuturePrinciple()));
		//repaymentScheduleDataModel.setTotPrincipal(String.valueOf(loan.getAmount()));
		repaymentScheduleDataModel.setUcic(String.valueOf(loan.getCustomer().getId()));
		
		Set<Receivable> receivables = new TreeSet<>();
		receivables.addAll(loan.getReceivable());
        float sumPrinciple=0.0f,totInstamt=0.0f;
        int tenure1=0;
        String statuschk= new String();
		List<RepaymentScheduleModel> repayScheduleList = new ArrayList<>();
		
		for( Receivable receivable : receivables) {
			RepaymentScheduleModel repaymentScheduleModel = new RepaymentScheduleModel();
		
			
			repaymentScheduleModel.setDueDate(String.valueOf(sdf.format(receivable.getDueDate())));
			repaymentScheduleModel.setDueNo(String.valueOf(receivable.getDueNo()));
		
			if(receivable.getStatus().contentEquals("MORAT"))
			{
				repaymentScheduleModel.setDueAmt("0");
				repaymentScheduleModel.setInterest(String.valueOf(receivable.getMoratInterest()));
			}else
			{
				repaymentScheduleModel.setDueAmt(String.valueOf(receivable.getEmi()));
				repaymentScheduleModel.setInterest(String.valueOf(receivable.getInterest()));	
			}
			
			sumPrinciple +=  receivable.getPrinciple();
			tenure1++;
			totInstamt += receivable.getPrinciple()+receivable.getInterest();
			repaymentScheduleModel.setOsPrincipal(String.valueOf(receivable.getPrincipleRemaining()));
			repaymentScheduleModel.setPostMoratPrincipal(String.valueOf(receivable.getRevisedPrinRemaining()));
			repaymentScheduleModel.setPrincipal(String.valueOf(receivable.getPrinciple()));
			
			repayScheduleList.add(repaymentScheduleModel);
			if(receivable.getStatus().contentEquals("MORAT"))
			{
				statuschk=receivable.getStatus();
			
			}
		}
		
		if(statuschk.contentEquals("MORAT")) {
			repaymentScheduleDataModel.setRevisedTenure(String.valueOf(tenure1));
		}else {
			repaymentScheduleDataModel.setRevisedTenure("0");	
		}
		
		
		repaymentScheduleDataModel.setTotalInstl(String.valueOf(totInstamt));
		repaymentScheduleDataModel.setTotPrincipal(String.valueOf(sumPrinciple));
		repaymentScheduleDataModel.setRepayScheduleList(repayScheduleList);

		PDFHandler handler = new PDFHandler();
		String createdFilePath = "";

		try {

			ByteArrayOutputStream streamSource = handler.getXMLSource(repaymentScheduleDataModel);

			createdFilePath = handler.createPDFFile(streamSource,templateFilePath,PRESCRIPTION_URL,filePath,fileName);
			System.out.println("PDF created in:"+createdFilePath);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return createdFilePath; 

	}
}
