package com.vg.lms.docgen.model;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="FORECLOSURE")
public class ForeClosureDataModel {
	String letterDate;
	String name;
	String address;
	String district;
	String state;
	String pincode;
	String mobileNo;
	String disbursalDate;
	String firstEmiDate;
	String appNo;
	String ucic;
	String calcValidDate;
	String principalArrears;
	String interestArrears;
	String principalOutstand;
	String interestAfterLastEMI;
	String overdueCharges;
	String otherCharges;
	String moratoriumInterest;
	String covidMoratInterest;
	String totalAmount;
	String regNo;
	String engineNo;
	String chassisNo;
	String upcommingDueDate;
	String fclCharges;

	@XmlElement(name = "LETTER_DATE")
	public String getLetterDate() {
		return letterDate;
	}
	public void setLetterDate(String letterDate) {
		this.letterDate = letterDate;
	}
	@XmlElement(name = "NAME")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@XmlElement(name = "ADDRESS")
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@XmlElement(name = "DISTRICT")
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	@XmlElement(name = "STATE")
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@XmlElement(name = "PINCODE")
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	@XmlElement(name = "MOBILE_NO")
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	@XmlElement(name = "DISBURSAL_DATE")
	public String getDisbursalDate() {
		return disbursalDate;
	}
	public void setDisbursalDate(String disbursalDate) {
		this.disbursalDate = disbursalDate;
	}
	@XmlElement(name = "FIRST_EMI_DATE")
	public String getFirstEmiDate() {
		return firstEmiDate;
	}
	public void setFirstEmiDate(String firstEmiDate) {
		this.firstEmiDate = firstEmiDate;
	}
	@XmlElement(name = "APPNO")
	public String getAppNo() {
		return appNo;
	}
	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}
	@XmlElement(name = "UCIC")
	public String getUcic() {
		return ucic;
	}
	public void setUcic(String ucic) {
		this.ucic = ucic;
	}
	@XmlElement(name = "CALC_VALID_DATE")
	public String getCalcValidDate() {
		return calcValidDate;
	}
	public void setCalcValidDate(String calcValidDate) {
		this.calcValidDate = calcValidDate;
	}

	@XmlElement(name = "PRINCIPAL_ARREARS")
	public String getPrincipalArrears() {
		return principalArrears;
	}
	public void setPrincipalArrears(String principalArrears) {
		this.principalArrears = principalArrears;
	}
	@XmlElement(name = "INTEREST_ARREARS")
	public String getInterestArrears() {
		return interestArrears;
	}
	public void setInterestArrears(String interestArrears) {
		this.interestArrears = interestArrears;
	}
	@XmlElement(name = "PRINCIPAL_OUTSTAND")
	public String getPrincipalOutstand() {
		return principalOutstand;
	}
	public void setPrincipalOutstand(String principalOutstand) {
		this.principalOutstand = principalOutstand;
	}
	@XmlElement(name = "INTEREST_AFTER_LAST_EMI")
	public String getInterestAfterLastEMI() {
		return interestAfterLastEMI;
	}
	public void setInterestAfterLastEMI(String interestAfterLastEMI) {
		this.interestAfterLastEMI = interestAfterLastEMI;
	}
	@XmlElement(name = "OVERDUE_CHARGES")
	public String getOverdueCharges() {
		return overdueCharges;
	}
	public void setOverdueCharges(String overdueCharges) {
		this.overdueCharges = overdueCharges;
	}
	@XmlElement(name = "OTHER_CHARGES")
	public String getOtherCharges() {
		return otherCharges;
	}
	public void setOtherCharges(String otherCharges) {
		this.otherCharges = otherCharges;
	}
	@XmlElement(name = "MORATORIUM_INTEREST")
	public String getMoratoriumInterest() {
		return moratoriumInterest;
	}
	public void setMoratoriumInterest(String moratoriumInterest) {
		this.moratoriumInterest = moratoriumInterest;
	}
	@XmlElement(name = "TOTAL_AMOUNT")
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	@XmlElement(name = "REG_NO")
	public String getRegNo() {
		return regNo;
	}
	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}
	@XmlElement(name = "ENGINE_NO")
	public String getEngineNo() {
		return engineNo;
	}
	public void setEngineNo(String engineNo) {
		this.engineNo = engineNo;
	}
	@XmlElement(name = "CHASSIS_NO")
	public String getChassisNo() {
		return chassisNo;
	}
	public void setChassisNo(String chassisNo) {
		this.chassisNo = chassisNo;
	}
	@XmlElement(name = "UPCOMMING_DUE_DATE")
	public String getUpcommingDueDate() {
		return upcommingDueDate;
	}
	public void setUpcommingDueDate(String upcommingDueDate) {
		this.upcommingDueDate = upcommingDueDate;
	}
	@XmlElement(name = "FCL_CHARGES")
	public String getFclCharges() {
		return fclCharges;
	}
	public void setFclCharges(String fclCharges) {
		this.fclCharges = fclCharges;
	}

	@XmlElement(name = "COVID_MORAT_INTEREST")
	public String getCovidMoratInterest() {
		return covidMoratInterest;
	}
	public void setCovidMoratInterest(String covidMoratInterest) {
		this.covidMoratInterest = covidMoratInterest;
	}


}

