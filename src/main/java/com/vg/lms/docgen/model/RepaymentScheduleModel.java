package com.vg.lms.docgen.model;

public class RepaymentScheduleModel {
	String dueNo;
	String dueDate;
	String dueAmt;
	String interest;
	String principal;
	String osPrincipal;
	String postMoratPrincipal;
	String covidMoratInterest;
	
	public String getDueNo() {
		return dueNo;
	}
	public void setDueNo(String dueNo) {
		this.dueNo = dueNo;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getDueAmt() {
		return dueAmt;
	}
	public void setDueAmt(String dueAmt) {
		this.dueAmt = dueAmt;
	}
	
	public String getPrincipal() {
		return principal;
	}
	public void setPrincipal(String principal) {
		this.principal = principal;
	}
	public String getInterest() {
		return interest;
	}
	public void setInterest(String interest) {
		this.interest = interest;
	}
	public String getOsPrincipal() {
		return osPrincipal;
	}
	public void setOsPrincipal(String osPrincipal) {
		this.osPrincipal = osPrincipal;
	}
	
	public String getPostMoratPrincipal() {
		return postMoratPrincipal;
	}
	public void setPostMoratPrincipal(String postMoratPrincipal) {
		this.postMoratPrincipal = postMoratPrincipal;
	}
	
	public String getcovidMoratInterest() {
		return covidMoratInterest;
	}
	public void setcovidMoratInterest(String covidMoratInterest) {
		this.covidMoratInterest = covidMoratInterest;
	}
}
