package com.vg.lms.docgen.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

@XmlRootElement(name="SOA")
@XmlSeeAlso(SOACreditDebitScheduleModel.class)
public class SOADataModel {

	String name;
	String address;
	String city;
	String state;
	String pincode;
	String mobileNo;
	String stmtIssueDate;
	String stmtPeriodFrom;
	String stmtPeriodTo;
	String loanNo;
	String ucic;
	String branch;
	String financeAmt;
	String product;
	String model;
	String emiAmt;
	String tenure;
	String disbDate;
	String instStartDate;
	String instEndDate;
	String roi;
	List<SOACreditDebitScheduleModel> creditDebitScheduleList;

	
	@XmlElement(name = "NAME")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@XmlElement(name = "ADDRESS")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	@XmlElement(name = "CITY")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	@XmlElement(name = "STATE")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	@XmlElement(name = "PINCODE")
	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	@XmlElement(name = "MOBILE_NO")
	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	@XmlElement(name = "STMT_ISSUE_DATE")
	public String getStmtIssueDate() {
		return stmtIssueDate;
	}

	public void setStmtIssueDate(String stmtIssueDate) {
		this.stmtIssueDate = stmtIssueDate;
	}
	@XmlElement(name = "STMT_PERIOD_FROM")
	public String getStmtPeriodFrom() {
		return stmtPeriodFrom;
	}

	public void setStmtPeriodFrom(String stmtPeriodFrom) {
		this.stmtPeriodFrom = stmtPeriodFrom;
	}
	@XmlElement(name = "STMT_PERIOD_TO")
	public String getStmtPeriodTo() {
		return stmtPeriodTo;
	}

	public void setStmtPeriodTo(String stmtPeriodTo) {
		this.stmtPeriodTo = stmtPeriodTo;
	}
	@XmlElement(name = "LOANNO")
	public String getLoanNo() {
		return loanNo;
	}

	public void setLoanNo(String loanNo) {
		this.loanNo = loanNo;
	}
	@XmlElement(name = "UCIC")
	public String getUcic() {
		return ucic;
	}

	public void setUcic(String ucic) {
		this.ucic = ucic;
	}
	@XmlElement(name = "BRANCH")
	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}
	@XmlElement(name = "FINANCE_AMT")
	public String getFinanceAmt() {
		return financeAmt;
	}

	public void setFinanceAmt(String financeAmt) {
		this.financeAmt = financeAmt;
	}
	@XmlElement(name = "PRODUCT")
	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}
	@XmlElement(name = "MODEL")
	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}
	@XmlElement(name = "EMI_AMT")
	public String getEmiAmt() {
		return emiAmt;
	}

	public void setEmiAmt(String emiAmt) {
		this.emiAmt = emiAmt;
	}
	@XmlElement(name = "TENURE")
	public String getTenure() {
		return tenure;
	}

	public void setTenure(String tenure) {
		this.tenure = tenure;
	}
	@XmlElement(name = "DISB_DATE")
	public String getDisbDate() {
		return disbDate;
	}

	public void setDisbDate(String disbDate) {
		this.disbDate = disbDate;
	}
	@XmlElement(name = "INST_START_DATE")
	public String getInstStartDate() {
		return instStartDate;
	}

	public void setInstStartDate(String instStartDate) {
		this.instStartDate = instStartDate;
	}
	@XmlElement(name = "INST_END_DATE")
	public String getInstEndDate() {
		return instEndDate;
	}

	public void setInstEndDate(String instEndDate) {
		this.instEndDate = instEndDate;
	}
	@XmlElement(name = "ROI")
	public String getRoi() {
		return roi;
	}

	public void setRoi(String roi) {
		this.roi = roi;
	}
	@XmlElementWrapper(name = "creditDebitScheduleList")
	@XmlElement(name = "SOACreditDebitScheduleModel")
	public List<SOACreditDebitScheduleModel> getCreditDebitScheduleList() {
		return creditDebitScheduleList;
	}

	public void setCreditDebitScheduleList(List<SOACreditDebitScheduleModel> creditDebitScheduleList) {
		this.creditDebitScheduleList = creditDebitScheduleList;
	}
	

}
