package com.vg.lms.docgen.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;



@SuppressWarnings("serial")
@XmlRootElement(name="WELCOME_LETTER_BL")
public class WelcomeLetterBL implements Serializable{
	String name;
	String date;
	String address;
	String district;
	String state;
	String pinCode;
	String mobileNo;
	String appNo;
	String customerID;
	String LoanAccNumber;
	String disbursedDate;
	String sanctionAmt;
	String disbursedAmt;
	String emiAmount;
	String emiStartDate;
	String emiEndDate;
	String tenure;	
	String roi;
	String repaymentMode;
	String repaymentCycle;
	String procFee;
	String loandocCharges;
	String loanmanagementCharges;
	String insurance;
	
	@XmlElement(name = "NAME")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@XmlElement(name ="LETTER_DATE")
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	@XmlElement(name ="ADDRESS")
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	@XmlElement(name ="DISTRICT")
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	
	@XmlElement(name ="STATE")
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	@XmlElement(name ="PINCODE")
	public String getPinCode() {
		return pinCode;
	}
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}
	
	
	@XmlElement(name ="MOBILE")
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	
	
	@XmlElement(name ="APPNO")
	public String getAppNo() {
		return appNo;
	}
	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}
	
	
	@XmlElement(name ="CUSTOMERID")
	public String getCustomerID() {
		return customerID;
	}
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	
	
	@XmlElement(name ="LOAN_ACCOUNT_NUMBER")
	public String getLoanAccNumber() {
		return LoanAccNumber;
	}
	public void setLoanAccNumber(String loanAccNumber) {
		LoanAccNumber = loanAccNumber;
	}
	
	@XmlElement(name ="DISBURSED_DATE")
	public String getDisbursedDate() {
		return disbursedDate;
	}
	public void setDisbursedDate(String disbursedDate) {
		this.disbursedDate = disbursedDate;
	}
	
	@XmlElement(name ="LOAN_AMOUNT")
	public String getSanctionAmt() {
		return sanctionAmt;
	}
	public void setSanctionAmt(String sanctionAmt) {
		this.sanctionAmt = sanctionAmt;
	}
	
	
	@XmlElement(name ="DISBURSAL_AMOUNT")
	public String getDisbursedAmt() {
		return disbursedAmt;
	}
	public void setDisbursedAmt(String disbursedAmt) {
		this.disbursedAmt = disbursedAmt;
	}
	
	@XmlElement(name ="EMI_AMOUNT")
	public String getEmiAmount() {
		return emiAmount;
	}
	public void setEmiAmount(String emiAmount) {
		this.emiAmount = emiAmount;
	}
	
	@XmlElement(name ="EMI_Start_Date")
	public String getEmiStartDate() {
		return emiStartDate;
	}
	public void setEmiStartDate(String emiStartDate) {
		this.emiStartDate = emiStartDate;
	}
	
	@XmlElement(name ="EMI_End_Date")
	public String getEmiEndDate() {
		return emiEndDate;
	}
	public void setEmiEndDate(String emiEndDate) {
		this.emiEndDate = emiEndDate;
	}
	
	
	@XmlElement(name ="TENURE")
	public String getTenure() {
		return tenure;
	}
	public void setTenure(String tenure) {
		this.tenure = tenure;
	}
	
	@XmlElement(name ="RATE_OF_INTEREST")
	public String getRoi() {
		return roi;
	}
	public void setRoi(String roi) {
		this.roi = roi;
	}
	
	@XmlElement(name ="REPAYMENT_MODE")
	public String getRepaymentMode() {
		return repaymentMode;
	}
	public void setRepaymentMode(String repaymentMode) {
		this.repaymentMode = repaymentMode;
	}
	
	@XmlElement(name ="REPAYMENT_CYCLE")
	public String getRepaymentCycle() {
		return repaymentCycle;
	}
	public void setRepaymentCycle(String repaymentCycle) {
		this.repaymentCycle = repaymentCycle;
	}
	
	
	@XmlElement(name ="PROCESSING_FEE")
	public String getProcFee() {
		return procFee;
	}
	public void setProcFee(String procFee) {
		this.procFee = procFee;
	}
	
	
	@XmlElement(name ="LOAN_DOCUMENT_CHARGES")
	public String getLoandocCharges() {
		return loandocCharges;
	}
	public void setLoandocCharges(String loandocCharges) {
		this.loandocCharges = loandocCharges;
	}
	
	@XmlElement(name ="LOAN_MANAGEMENT_CHARGES")
	public String getLoanManagementCharges() {
		return loanmanagementCharges;
	}
	public void setLoanmanagementCharges(String loanmanagementCharges) {
		this.loanmanagementCharges = loanmanagementCharges;
	}
	
	
	@XmlElement(name ="INSURANCE")
	public String getInsurance() {
		return insurance;
	}
	public void setInsurance(String insurance) {
		this.insurance = insurance;
	}

}
