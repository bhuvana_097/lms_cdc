package com.vg.lms.docgen.model;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="WELCOME_LETTER")
public class WelcomeLetterDataModel {
	String name;
	String date;
	String address;
	String district;
	String state;
	String pinCode;
	String mobileNo;
	String appNo;
	String disbursedDate;
	String sanctionAmt;
	String disbursedAmt;
	String dealerName;
	String makeModel;
	String interestType;
	String interestRate;
	String tenure;
	String emi;
	String dateCommence;
	String repaymentCycle;
	String noAdvEmi;
	String amtAdvEmi;
	String procFee;
	String deduFromDisAmt;
	String netDisbursedAmt;
	
	@XmlElement(name = "NAME")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@XmlElement(name = "LETTER_DATE")
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	@XmlElement(name = "ADDRESS")
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@XmlElement(name = "DISTRICT")
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	@XmlElement(name = "STATE")
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@XmlElement(name = "PINCODE")
	public String getPinCode() {
		return pinCode;
	}
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}
	@XmlElement(name = "MOBILE_NO")
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	@XmlElement(name = "APPNO")
	public String getAppNo() {
		return appNo;
	}
	public void setAppNo(String appNo) {
		this.appNo = appNo;
	}
	@XmlElement(name = "DISBURSED_DATE")
	public String getDisbursedDate() {
		return disbursedDate;
	}
	public void setDisbursedDate(String disbursedDate) {
		this.disbursedDate = disbursedDate;
	}
	@XmlElement(name = "SANCTION_AMT")
	public String getSanctionAmt() {
		return sanctionAmt;
	}
	public void setSanctionAmt(String sanctionAmt) {
		this.sanctionAmt = sanctionAmt;
	}
	@XmlElement(name = "DISBURSED_AMT")
	public String getDisbursedAmt() {
		return disbursedAmt;
	}
	public void setDisbursedAmt(String disbursedAmt) {
		this.disbursedAmt = disbursedAmt;
	}
	@XmlElement(name = "DEALER_NAME")
	public String getDealerName() {
		return dealerName;
	}
	public void setDealerName(String dealerName) {
		this.dealerName = dealerName;
	}
	@XmlElement(name = "MAKE_MODEL")
	public String getMakeModel() {
		return makeModel;
	}
	public void setMakeModel(String makeModel) {
		this.makeModel = makeModel;
	}
	@XmlElement(name = "INTEREST_TYPE")
	public String getInterestType() {
		return interestType;
	}
	public void setInterestType(String interestType) {
		this.interestType = interestType;
	}
	@XmlElement(name = "INTEREST_RATE")
	public String getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(String interestRate) {
		this.interestRate = interestRate;
	}
	@XmlElement(name = "TENURE")
	public String getTenure() {
		return tenure;
	}
	public void setTenure(String tenure) {
		this.tenure = tenure;
	}
	@XmlElement(name = "EMI")
	public String getEmi() {
		return emi;
	}
	public void setEmi(String emi) {
		this.emi = emi;
	}
	@XmlElement(name = "DATE_COMMENCE")
	public String getDateCommence() {
		return dateCommence;
	}
	public void setDateCommence(String dateCommence) {
		this.dateCommence = dateCommence;
	}
	@XmlElement(name = "REPAYMENT_CYCLE")
	public String getRepaymentCycle() {
		return repaymentCycle;
	}
	public void setRepaymentCycle(String repaymentCycle) {
		this.repaymentCycle = repaymentCycle;
	}
	@XmlElement(name = "NO_ADV_EMI")
	public String getNoAdvEmi() {
		return noAdvEmi;
	}
	public void setNoAdvEmi(String noAdvEmi) {
		this.noAdvEmi = noAdvEmi;
	}
	@XmlElement(name = "AMT_ADV_EMI")
	public String getAmtAdvEmi() {
		return amtAdvEmi;
	}
	public void setAmtAdvEmi(String amtAdvEmi) {
		this.amtAdvEmi = amtAdvEmi;
	}
	@XmlElement(name = "PROC_FEE")
	public String getProcFee() {
		return procFee;
	}
	public void setProcFee(String procFee) {
		this.procFee = procFee;
	}
	@XmlElement(name = "DEDU_FROM_DIS_AMT")
	public String getDeduFromDisAmt() {
		return deduFromDisAmt;
	}
	public void setDeduFromDisAmt(String deduFromDisAmt) {
		this.deduFromDisAmt = deduFromDisAmt;
	}
	@XmlElement(name = "NET_DISBURSED_AMT")
	public String getNetDisbursedAmt() {
		return netDisbursedAmt;
	}
	public void setNetDisbursedAmt(String netDisbursedAmt) {
		this.netDisbursedAmt = netDisbursedAmt;
	}
	
	
	
}
