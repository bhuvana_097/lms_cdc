package com.vg.lms.docgen.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

@XmlRootElement(name="REPAYMENT_SCHEDULE")
@XmlSeeAlso(RepaymentScheduleModel.class)
public class RepaymentScheduleDataModel {
	String letterDate;
	String aggreNo;
	String ucic;
	String name;
	String tenure;
	String loanType;
	String totalInstl;
	String financeAmt;
	private List<RepaymentScheduleModel> repayScheduleList;
	String totDue;
	String totPrincipal;
	String totOsPrincipal;
	String revisedTenure;
	

	@XmlElement(name = "DATE")
	public String getLetterDate() {
		return letterDate;
	}

	public void setLetterDate(String letterDate) {
		this.letterDate = letterDate;
	}

	@XmlElement(name = "AGGRE_NO")
	public String getAggreNo() {
		return aggreNo;
	}

	public void setAggreNo(String aggreNo) {
		this.aggreNo = aggreNo;
	}
	@XmlElement(name = "UCIC")
	public String getUcic() {
		return ucic;
	}

	public void setUcic(String ucic) {
		this.ucic = ucic;
	}
	@XmlElement(name = "NAME")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@XmlElement(name = "TENURE")
	public String getTenure() {
		return tenure;
	}

	public void setTenure(String tenure) {
		this.tenure = tenure;
	}
	@XmlElement(name = "LOAN_TYPE")
	public String getLoanType() {
		return loanType;
	}

	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}
	@XmlElement(name = "TOTAL_INSTL")
	public String getTotalInstl() {
		return totalInstl;
	}

	public void setTotalInstl(String totalInstl) {
		this.totalInstl = totalInstl;
	}
	@XmlElement(name = "FINANCE_AMT")
	public String getFinanceAmt() {
		return financeAmt;
	}

	public void setFinanceAmt(String financeAmt) {
		this.financeAmt = financeAmt;
	}

	@XmlElementWrapper(name = "repayScheduleList")
	@XmlElement(name = "RepaymentScheduleModel")
	public List<RepaymentScheduleModel> getRepayScheduleList() {
		return repayScheduleList;
	}

	public void setRepayScheduleList(List<RepaymentScheduleModel> repayScheduleList) {
		this.repayScheduleList = repayScheduleList;
	}
	@XmlElement(name = "TOT_DUE")
	public String getTotDue() {
		return totDue;
	}

	public void setTotDue(String totDue) {
		this.totDue = totDue;
	}
	@XmlElement(name = "TOT_PRINCIPAL")
	public String getTotPrincipal() {
		return totPrincipal;
	}

	public void setTotPrincipal(String totPrincipal) {
		this.totPrincipal = totPrincipal;
	}
	@XmlElement(name = "TOT_OS_PRINCIPAL")
	public String getTotOsPrincipal() {
		return totOsPrincipal;
	}

	public void setTotOsPrincipal(String totOsPrincipal) {
		this.totOsPrincipal = totOsPrincipal;
	}
	
	@XmlElement(name = "REVISEDTENURE")
	public String getRevisedTenure() {
		return revisedTenure;
	}

	public void setRevisedTenure(String revisedTenure) {
		this.revisedTenure = revisedTenure;
	}
	

}
