package com.vg.lms.docgen;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vg.lms.docgen.model.ForeClosureDataModel;
import com.vg.lms.model.Asset;
import com.vg.lms.model.Loan;
import com.vg.lms.repository.LoanRepository;

@Component
public class GenerateForeClosurePDF {	

	@Autowired
	private LoanRepository loanRepository;

	public String generateForeClosureDoc(int loanId) {


		String templateFilePath ="/FRNDLN/template/";
		String PRESCRIPTION_URL = "Foreclosure.xsl";

		Loan loan = loanRepository.findById(loanId).get();

		String filePath = "/FRNDLN/docs/";
		String fileName = "ForeClosure";

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		ForeClosureDataModel foreClosureDataModel = new ForeClosureDataModel();
		foreClosureDataModel.setAddress(loan.getCustomer().getResiAddr());
		foreClosureDataModel.setAppNo(loan.getLoanNo());
		foreClosureDataModel.setCalcValidDate("");
		foreClosureDataModel.setChassisNo("");
		//foreClosureDataModel.setChqBouncCharg(String.valueOf(loan.getBc()));
		//foreClosureDataModel.setOtherCharges(otherCharges);

		foreClosureDataModel.setDisbursalDate(sdf.format(loan.getAgmntDate()));
		foreClosureDataModel.setDistrict(loan.getCustomer().getResiCity());
		foreClosureDataModel.setEngineNo("");
		foreClosureDataModel.setFirstEmiDate(sdf.format(loan.getFirstDueDate()));
		//foreClosureDataModel.setInterestAmtPay("0");
		//foreClosureDataModel.setInterestPrepay("0");
		foreClosureDataModel.setLetterDate(sdf.format(new Date()));
		foreClosureDataModel.setMobileNo(loan.getCustomer().getPhone1());
		foreClosureDataModel.setName(loan.getCustomer().getName());
		//foreClosureDataModel.setOtherCharges(String.valueOf(loan.getOc()));
		//foreClosureDataModel.setOverdueMatInterest("0");
		//foreClosureDataModel.setPendingInst("0");
		//foreClosureDataModel.setPerDayInterest("0");
		foreClosureDataModel.setPincode(loan.getCustomer().getResiPin());

		//not needed
		/*Calendar agmnt = GregorianCalendar.getInstance();
		agmnt.setTime(loan.getAgmntDate());
		LocalDate agmntDate = LocalDate.of(agmnt.get(Calendar.YEAR), agmnt.get(Calendar.MONTH), agmnt.get(Calendar.DATE));
		LocalDate now = LocalDate.now();
		Period noOfDays = Period.between(agmntDate, now); 
		int noOfMonths = noOfDays.getDays()/30;*/
		
		/*float foreclosureCharge = 0;
		if(noOfMonths < 6)
			foreclosureCharge += loan.getFuturePrinciple()*0.08f;
		else if(noOfMonths < 12)
			foreclosureCharge += loan.getFuturePrinciple()*0.05f;
		else
			foreclosureCharge += loan.getFuturePrinciple()*0.03f;

		foreclosureCharge += (foreclosureCharge*0.15);*/

		//foreClosureDataModel.setClosurePayment(""+loan.getFuturePrinciple() + foreclosureCharge);

		//foreClosureDataModel.setPrepayCharge(String.valueOf(foreclosureCharge));
		foreClosureDataModel.setPrincipalOutstand(String.valueOf(loan.getFuturePrinciple()));
		//foreClosureDataModel.setRefunds("0");
		foreClosureDataModel.setRegNo("");
		foreClosureDataModel.setState(loan.getCustomer().getResiState());
		foreClosureDataModel.setUcic(String.valueOf(loan.getCustomer().getId()));
		foreClosureDataModel.setUpcommingDueDate("");


		foreClosureDataModel.setPrincipalArrears(String.valueOf(loan.getPrinArears()));
		foreClosureDataModel.setInterestAfterLastEMI(String.valueOf(loan.getIntLastDue()));
		foreClosureDataModel.setInterestArrears(String.valueOf(loan.getIntArrears()));
		foreClosureDataModel.setPrincipalOutstand(String.valueOf(loan.getPrinOutstanding()));
		foreClosureDataModel.setOverdueCharges(String.valueOf(loan.getOdc()));
		foreClosureDataModel.setOtherCharges(String.valueOf(loan.getChargesDue()));
		foreClosureDataModel.setMoratoriumInterest(String.valueOf(loan.getMoratoriumInterest()));
		foreClosureDataModel.setCovidMoratInterest(String.valueOf(loan.getMoratInterest()-loan.getCovidPayInt()));
		foreClosureDataModel.setTotalAmount(String.valueOf(loan.getFclAmount()));
		foreClosureDataModel.setFclCharges(String.valueOf(loan.getFclCharges()));

		Asset asset = loan.getAsset();
		if(asset != null) {
			foreClosureDataModel.setRegNo(String.valueOf(asset.getRegNo()));
			foreClosureDataModel.setEngineNo(String.valueOf(asset.getEngineNo()));
			foreClosureDataModel.setChassisNo(String.valueOf(asset.getChassisNo()));
		}




		/*
		foreClosureDataModel.setLetterDate("Today");
		foreClosureDataModel.setName("Orange");
		foreClosureDataModel.setAddress("Voicegear Networks address");
		foreClosureDataModel.setDistrict("Chennai");*/

		PDFHandler handler = new PDFHandler();
		String createdFilePath = "";

		try {

			ByteArrayOutputStream streamSource = handler.getXMLSource(foreClosureDataModel);

			createdFilePath = handler.createPDFFile(streamSource,templateFilePath,PRESCRIPTION_URL,filePath,fileName);
			System.out.println("PDF created in:"+createdFilePath);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return createdFilePath;

	}
}
