package com.vg.lms.docgen;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vg.lms.docgen.model.WelcomeLetterDataModel;
import com.vg.lms.model.Customer;
import com.vg.lms.model.Loan;
import com.vg.lms.repository.LoanRepository;


@Component
public class GenerateWelcomeLetterPDF {
	
	@Autowired
	private LoanRepository loanRepository;

	
	public String generateWelcomeLetter(int loanId) {
		System.out.println("Generating Welcome Letter");
		
		Loan loan = loanRepository.findById(loanId).get();

		String templateFilePath ="/FRNDLN/template/";
		

		String PRESCRIPTION_URL = "Welcome_Letter.xsl";
		String filePath = "/FRNDLN/docs/";
		
		String fileName = "WelcomeLetter";
		//String filenamewithpath = filePath + fileName ;
		WelcomeLetterDataModel welcomeLetterDataModel = new WelcomeLetterDataModel();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		welcomeLetterDataModel.setDate(sdf.format(new Date()));
		Customer customer = loan.getCustomer();
		welcomeLetterDataModel.setName(customer.getName());
		welcomeLetterDataModel.setAddress(customer.getResiAddr());
		welcomeLetterDataModel.setDistrict(customer.getResiCity());
		welcomeLetterDataModel.setAmtAdvEmi(String.valueOf(loan.getAdvanceEmi()* loan.getEmi()));
		welcomeLetterDataModel.setAppNo(loan.getLoanNo());
		welcomeLetterDataModel.setDateCommence(sdf.format(loan.getAgmntDate()));
		welcomeLetterDataModel.setDealerName(loan.getDealerName());
		welcomeLetterDataModel.setDeduFromDisAmt(String.valueOf(loan.getDownPayment()));
		welcomeLetterDataModel.setDisbursedAmt(String.valueOf(loan.getDealerDisbursementAmount()));
		welcomeLetterDataModel.setDisbursedDate(sdf.format(loan.getAgmntDate()));
		welcomeLetterDataModel.setEmi(String.valueOf(loan.getEmi()));
		welcomeLetterDataModel.setInterestRate(String.valueOf(loan.getIrr()));
		welcomeLetterDataModel.setInterestType("Diminishing");
		//welcomeLetterDataModel.setMakeModel(loan.getAsset().to);
		welcomeLetterDataModel.setMobileNo(customer.getPhone1());
		welcomeLetterDataModel.setNetDisbursedAmt(String.valueOf(loan.getDealerDisbursementAmount()));
		welcomeLetterDataModel.setNoAdvEmi(String.valueOf(loan.getAdvanceEmi()));
		welcomeLetterDataModel.setProcFee(String.valueOf(loan.getProcFee()));
		welcomeLetterDataModel.setPinCode(customer.getResiPin());
		welcomeLetterDataModel.setRepaymentCycle("Monthly");
		welcomeLetterDataModel.setSanctionAmt(String.valueOf(loan.getAmount()));
		welcomeLetterDataModel.setState(customer.getResiState());
		welcomeLetterDataModel.setTenure(String.valueOf(loan.getTenure()));

		PDFHandler handler = new PDFHandler();
		String createdFilePath = "";
		try {
			ByteArrayOutputStream streamSource = handler.getXMLSource(welcomeLetterDataModel);
			createdFilePath = handler.createPDFFile(streamSource,templateFilePath,PRESCRIPTION_URL,filePath,fileName);
			System.out.println("PDF FILE :"+createdFilePath);
		} catch (Exception e) {
			// TODO Auto-generated catchblock
			e.printStackTrace();
		}
		return createdFilePath;

	}



}
