package com.vg.lms.docgen;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;

import com.vg.lms.docgen.model.ForeClosureDataModel;
import com.vg.lms.docgen.model.RepaymentScheduleDataModel;
import com.vg.lms.docgen.model.RepaymentScheduleModel;
import com.vg.lms.docgen.model.SOADataModel;
import com.vg.lms.docgen.model.WelcomeLetterBL;
import com.vg.lms.docgen.model.WelcomeLetterDataModel;



public class PDFHandler {
	public static final String EXTENSION = ".pdf";
	/*public String PRESCRIPTION_URL = "orange.xsl";
	public String filePath = "/home/ragavan/Documents/STSWorkspace/OrangeDocumentGeneration/";
	public String fileName = "test";*/

	public String createPDFFile(ByteArrayOutputStream xmlSource, String templateFilePath, String PRESCRIPTION_URL, String filePath, String fileName) throws IOException {
		File tempFile = File.createTempFile("" + System.currentTimeMillis(), EXTENSION);
		URL url = new File(templateFilePath + PRESCRIPTION_URL).toURI().toURL();
		// creation of transform source
		StreamSource transformSource = new StreamSource(url.openStream());
		// create an instance of fop factory
		FopFactory fopFactory = FopFactory.newInstance(new File(".").toURI());
		// a user agent is needed for transformation
		FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
		// to store output
		ByteArrayOutputStream pdfoutStream = new ByteArrayOutputStream();
		StreamSource source = new StreamSource(new ByteArrayInputStream(xmlSource.toByteArray()));
		Transformer xslfoTransformer;
		try {
			TransformerFactory transfact = TransformerFactory.newInstance();

			xslfoTransformer = transfact.newTransformer(transformSource);
			// Construct fop with desired output format
			Fop fop;
			try {
				fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, pdfoutStream);
				// Resulting SAX events (the generated FO)
				// must be piped through to FOP
				Result res = new SAXResult(fop.getDefaultHandler());

				// Start XSLT transformation and FOP processing
				try {
					// everything will happen here..
					xslfoTransformer.transform(source, res);

					// if you want to save PDF file use the following code
					OutputStream out = new java.io.FileOutputStream(tempFile);
					out = new java.io.BufferedOutputStream(out);
					FileOutputStream str = new FileOutputStream(tempFile);
					str.write(pdfoutStream.toByteArray());
					str.close();
					out.close();

				} catch (TransformerException e) {
					e.printStackTrace();
				}
			} catch (FOPException e) {
				e.printStackTrace();
			}
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		}
		tempFile.renameTo(new File(filePath+ fileName + EXTENSION));
				return filePath+ fileName + EXTENSION;
	}

	public ByteArrayOutputStream getXMLSource(WelcomeLetterDataModel orangeTestData) {
		JAXBContext context;

		ByteArrayOutputStream outStream = new ByteArrayOutputStream();

		try {
			context = JAXBContext.newInstance(WelcomeLetterDataModel.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(orangeTestData, outStream);
		} catch (JAXBException e) {

			e.printStackTrace();
		}
		return outStream;
	}
	
	public ByteArrayOutputStream getXMLSource(ForeClosureDataModel foreClosureDataModel) {
		
		JAXBContext context;

		ByteArrayOutputStream outStream = new ByteArrayOutputStream();

		try {
			context = JAXBContext.newInstance(ForeClosureDataModel.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(foreClosureDataModel, outStream);
		} catch (JAXBException e) {

			e.printStackTrace();
		}
		return outStream;
	}

	public ByteArrayOutputStream getXMLSource(RepaymentScheduleDataModel repaymentScheduleDataModel) {
		
		JAXBContext context;

		ByteArrayOutputStream outStream = new ByteArrayOutputStream();

		try {
			context = JAXBContext.newInstance(RepaymentScheduleDataModel.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(repaymentScheduleDataModel, outStream);
		} catch (JAXBException e) {

			e.printStackTrace();
		}
		return outStream;
	}

	public ByteArrayOutputStream getXMLSource(SOADataModel soaDataModel) {
		
		JAXBContext context;

		ByteArrayOutputStream outStream = new ByteArrayOutputStream();

		try {
			context = JAXBContext.newInstance(SOADataModel.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(soaDataModel, outStream);
		} catch (JAXBException e) {

			e.printStackTrace();
		}
		return outStream;
	}
	
	
	public ByteArrayOutputStream getXMLSource(WelcomeLetterBL orangeTestData) {
		JAXBContext context;

		ByteArrayOutputStream outStream = new ByteArrayOutputStream();

		try {
			context = JAXBContext.newInstance(WelcomeLetterBL.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			m.marshal(orangeTestData, outStream);
		} catch (JAXBException e) {

			e.printStackTrace();
		}
		return outStream;
	}


}
