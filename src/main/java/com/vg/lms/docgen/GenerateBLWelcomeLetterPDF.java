package com.vg.lms.docgen;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vg.lms.docgen.model.WelcomeLetterBL;
import com.vg.lms.model.Customer;
import com.vg.lms.model.Loan;
import com.vg.lms.repository.LoanRepository;


@Component
public class GenerateBLWelcomeLetterPDF {
	
	@Autowired
	private LoanRepository loanRepository;

	
	public String generateBlWelcomeLetter(int loanId) {
		System.out.println("Generating Welcome Letter");
		
		Loan loan = loanRepository.findById(loanId).get();

		String templateFilePath ="/FRNDLN/template/";
		//String templateFilePath ="/home/orange/git/lms_cdc/FLLMS/FRNDLN/template/";

		String PRESCRIPTION_URL = "Welcome_Letter_BL.xsl";
		String filePath = "/FRNDLN/docs/";
		//String filePath = "/home/orange/git/lms_cdc/FLLMS/FRNDLN/docs/";
		String fileName = "WelcomeLetter";

		WelcomeLetterBL welcomeLettbl = new WelcomeLetterBL();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		welcomeLettbl.setDate(sdf.format(new Date()));
		Customer customer = loan.getCustomer();
		welcomeLettbl.setName(customer.getName());
		welcomeLettbl.setAddress(customer.getResiAddr());
		welcomeLettbl.setDistrict(customer.getResiCity());
		welcomeLettbl.setAppNo(loan.getLoanNo());
		welcomeLettbl.setCustomerID(String.valueOf(customer.getId()));
		welcomeLettbl.setLoanAccNumber(loan.getLoanNo());
		welcomeLettbl.setDisbursedDate(sdf.format(loan.getDisbursementTime()));
		welcomeLettbl.setSanctionAmt(String.valueOf(loan.getAmount()));
		welcomeLettbl.setDisbursedAmt(String.valueOf(loan.getDealerDisbursementAmount()));
		welcomeLettbl.setDisbursedDate(sdf.format(loan.getAgmntDate()));
		welcomeLettbl.setEmiAmount(String.valueOf(loan.getEmi()));
		welcomeLettbl.setEmiStartDate(sdf.format(loan.getFirstDueDate()));
		welcomeLettbl.setEmiEndDate(sdf.format(loan.getLastDueDate()));
		welcomeLettbl.setTenure(String.valueOf(loan.getTenure()));
		welcomeLettbl.setRoi(String.valueOf(loan.getIrr()));
		welcomeLettbl.setRepaymentMode("NACH");
		welcomeLettbl.setRepaymentCycle("Monthly");
		welcomeLettbl.setProcFee(String.valueOf(loan.getProcFee()));
		welcomeLettbl.setLoandocCharges("CHarges");
		welcomeLettbl.setLoanmanagementCharges("loanmanagementCharges");
		welcomeLettbl.setMobileNo(customer.getPhone1());
			
		welcomeLettbl.setPinCode(customer.getResiPin());
		
		welcomeLettbl.setSanctionAmt(String.valueOf(loan.getAmount()));
		welcomeLettbl.setState(customer.getResiState());
		

		PDFHandler handler = new PDFHandler();
		String createdFilePath = "";
		try {
			ByteArrayOutputStream streamSource = handler.getXMLSource(welcomeLettbl);
			createdFilePath = handler.createPDFFile(streamSource,templateFilePath,PRESCRIPTION_URL,filePath,fileName);
			System.out.println("PDF FILE :"+createdFilePath);
		} catch (Exception e) {
			// TODO Auto-generated catchblock
			e.printStackTrace();
		}
		return createdFilePath;

	}



}

