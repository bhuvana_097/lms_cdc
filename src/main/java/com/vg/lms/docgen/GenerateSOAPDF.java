package com.vg.lms.docgen;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vg.lms.docgen.model.SOACreditDebitScheduleModel;
import com.vg.lms.docgen.model.SOADataModel;
import com.vg.lms.model.Asset;
import com.vg.lms.model.Customer;
import com.vg.lms.model.Ledger;
import com.vg.lms.model.Loan;
import com.vg.lms.model.Receivable;
import com.vg.lms.repository.LedgerRepository;
import com.vg.lms.repository.LoanRepository;
import com.vg.lms.service.CustomerServiceImpl;


@Component
public class GenerateSOAPDF {

	@Autowired
	private LoanRepository loanRepository;

	@Autowired
	private LedgerRepository ledgerRepository;
	private final Logger log = LoggerFactory.getLogger(GenerateSOAPDF.class);

	public String generateSOA(int loanId) {
		

		Loan loan = loanRepository.findById(loanId).get();

		String templateFilePath ="/FRNDLN/template/";
		String PRESCRIPTION_URL = "SOA.xsl";
		String filePath = "/FRNDLN/docs/";
		String fileName = "SOA";
		Customer customer = loan.getCustomer();
		
		SOADataModel soaDataModel = new SOADataModel();
		soaDataModel.setAddress(customer.getResiAddr());
		//soaDataModel.setAggNo(loan.getLoanNo());
		//soaDataModel.setBalPrinInterestStmtPeriod(balPrinInterestStmtPeriod);
		soaDataModel.setBranch(loan.getH3());

		soaDataModel.setCity(customer.getResiCity());

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

		//soaDataModel.setDisbursedAmt(String.valueOf(loan.getDealerDisbursementAmount()));
		soaDataModel.setEmiAmt(String.valueOf(loan.getEmi()));
		//soaDataModel.setEmiFreq("Monthly");

		//soaDataModel.setFdColl(String.valueOf(0));
		soaDataModel.setFinanceAmt(String.valueOf(loan.getAmount()));
		//soaDataModel.setInerestType("Reducing");
		soaDataModel.setInstEndDate(sdf.format(loan.getLastDueDate()));
		soaDataModel.setInstStartDate(sdf.format(loan.getFirstDueDate()));
		//soaDataModel.setInterestAppliedFrom(String.valueOf(0));
		//soaDataModel.setInterestAppliedTo(String.valueOf(0));
		//soaDataModel.setInterestStmtPeriod(String.valueOf(0));
		soaDataModel.setLoanNo(loan.getLoanNo());
		//soaDataModel.setLoanStatus(loan.getStatus());
		soaDataModel.setMobileNo(customer.getPhone1());

		soaDataModel.setName(customer.getName());
		//soaDataModel.setOsPrincStmttPeriod(String.valueOf(loan.getFuturePrinciple()));
		soaDataModel.setPincode(customer.getResiPin());
		//soaDataModel.setPrincipalRemainingStmtPeriod(String.valueOf(loan.getFuturePrinciple()));
		soaDataModel.setProduct("TWO WHEELER");

		soaDataModel.setRoi(String.valueOf(loan.getIrr()));
		soaDataModel.setState(customer.getResiState());
		soaDataModel.setStmtIssueDate(sdf.format(new Date()));
		soaDataModel.setStmtPeriodFrom(sdf.format(loan.getFirstDueDate()));
		soaDataModel.setStmtPeriodTo(sdf.format(new Date()));
		soaDataModel.setTenure(String.valueOf(loan.getTenure()));
		//soaDataModel.setTotCbcOs(String.valueOf(0));
		//soaDataModel.setTotEmiOs(String.valueOf(0));
		//soaDataModel.setTotOeiOs(String.valueOf(0));
		//soaDataModel.setTotPrinInterestStmtPeriod(String.valueOf(0));
		//soaDataModel.setTotPymtsOverdue(String.valueOf(0));
		soaDataModel.setUcic(String.valueOf(customer.getId()));

		Asset asset = loan.getAsset();
		if( asset != null) {
			soaDataModel.setModel(asset.getModel());
			//soaDataModel.setChessisNo(chessisNo);
			//soaDataModel.setEngineNo(engineNo);
			//soaDataModel.setRegnNo(regnNo);
		}
		if(loan.getStatus().equals("ACTIVE")) {
			soaDataModel.setDisbDate(sdf.format(loan.getDisbursementTime()));
		}

	
		/*To add Covid Morat Interest to Principal Remaining*/
		float mortint=0.0f;
		Set<Receivable> receivables = new TreeSet<>();
		receivables.addAll(loan.getReceivable());
			for( Receivable receivable : receivables) {
			if(receivable.getStatus().contentEquals("MORAT"))
			{
				mortint += receivable.getMoratInterest(); 
				
			}
		}
			
			/*End to add Covid Morat Interest to Principal Remaining*/
			
			
	/*List<Ledger> ledgerEntries = ledgerRepository.findByaccountIdOrderByValueDateAsc(loan.getLoanNo());
		float balance = 0.0f,balchk=0.0f;
		if(ledgerEntries != null && ledgerEntries.size() > 0) {
			List<SOACreditDebitScheduleModel> ledgerList = new ArrayList<>();
							
					balchk =loan.getAmount()*-1;
				
			for (Ledger ledgerEntry : ledgerEntries) {
				SOACreditDebitScheduleModel ledgerModel = new SOACreditDebitScheduleModel();
				ledgerModel.setValueDate(sdf.format(ledgerEntry.getValueDate()));
				if(ledgerEntry.getAmount() > 0) {
					ledgerModel.setCredit(Float.toString(ledgerEntry.getAmount()));
				} else {
					ledgerModel.setDebit(Float.toString( Math.abs(ledgerEntry.getAmount())));
				}
				//Covid Morat Change
				
					balance += ledgerEntry.getAmount();
			
				
				
				if(balance < 0) {
					
					ledgerModel.setBalance("("+Float.toString( Math.abs(balance))+")");
				} else {
					ledgerModel.setBalance(Float.toString(balance));
				}
				
				//ledgerModel.setBalance(String.valueOf(balance));
				ledgerModel.setParticulars(ledgerEntry.getDescription());
				ledgerList.add(ledgerModel);
			}
			soaDataModel.setCreditDebitScheduleList(ledgerList);
		}*/
			
		SimpleDateFormat sdfValDt = new SimpleDateFormat("yyyy-MM-dd");

			List<Ledger> ledgerEntries = ledgerRepository.findByaccountIdOrderByValueDateAsc(loan.getLoanNo());
			float balance = 0.0f;
			
			String loanNo = loan.getLoanNo();
			if(ledgerEntries != null && ledgerEntries.size() > 0) {
				List<SOACreditDebitScheduleModel> ledgerList = new ArrayList<>();
				//boolean reduceBalance = true;
				boolean intFound = false;
				for (Ledger ledgerEntry : ledgerEntries) {
				float loanAmount = loan.getAmount();

				SOACreditDebitScheduleModel ledgerModel = new SOACreditDebitScheduleModel();
				ledgerModel.setValueDate(sdf.format(ledgerEntry.getValueDate()));
				if(ledgerEntry.getAmount() > 0) {
				ledgerModel.setCredit(Float.toString(ledgerEntry.getAmount()));
				} else {
				ledgerModel.setDebit(Float.toString( Math.abs(ledgerEntry.getAmount())));
				}
				String tmpValueDate = sdfValDt.format(ledgerEntry.getValueDate());

				float moratInt =  ledgerRepository.findMoratInt(loanNo, tmpValueDate);

				loanAmount = loanAmount + moratInt;
				float prinPaid = ledgerRepository.findPrinPaid(loanNo, tmpValueDate);

				if(!intFound && ledgerEntry.getDescription().toLowerCase().contains("interest")){
				intFound = true;//new SOA login once customer starts paying
				}
				if(!intFound){
				balance += ledgerEntry.getAmount();
				}else{
				balance = loanAmount - prinPaid;
				}

				//balance = loanAmount - prinPaid;

				if(balance < 0) {
				ledgerModel.setBalance("("+Float.toString( Math.abs(balance))+")");
				} else {
				ledgerModel.setBalance(Float.toString(balance));
				}

				//ledgerModel.setBalance(String.valueOf(balance));
				ledgerModel.setParticulars(ledgerEntry.getDescription());

				ledgerList.add(ledgerModel);
				}

				soaDataModel.setCreditDebitScheduleList(ledgerList);
				}
			
			PDFHandler handler = new PDFHandler();
		String createdFilePath = "";
		try {
			ByteArrayOutputStream streamSource = handler.getXMLSource(soaDataModel);

			createdFilePath = handler.createPDFFile(streamSource,templateFilePath,PRESCRIPTION_URL,filePath,fileName);
			System.out.println("PDF created in:"+createdFilePath);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return createdFilePath;

	}
}