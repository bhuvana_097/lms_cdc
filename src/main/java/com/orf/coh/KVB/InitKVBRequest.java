package com.orf.coh.KVB;




import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
   
"SourcedBy",
"SourcedByOffice",
"InterestRate",
"LoanAmount",
"SanctionDate",
"EMISanctioned",
"SanctionedTerm",
"HolidayPeriod",
"TotalRepayAmount",
"ReqLoanAmount",
"ReqTerm",
"ReqEMI",
"ChargePaymentComplete",
"DateOfDisbursement",
"DisbursementAmount",
"DateOfAdvancePayment",
"AdvancePaid",
"LoanPurpose",
"FIVerDate",
"FISuccess",
"DisbAccBank",
"DisbAccBranch",
"DisbAccIFSC",
"DisbAccNo",
"SIAccNo",
"SIBank",
"SIBranch",
"SIIFSC",
"SIApplicant",
"ExcessMCLR",
"MCLRType",
"PreClosureRate",
"MCLRRate",
"CASAAccNov",
"MODDate",
"CKYCM",
"ProcessingFeeWaiver",
"BankYear",
"Reasonforwaiver",
"Acknowledgement",
"TotalCharges",
"TotalGSTCharges",
"ScourceType",
"TypeOfLoan",
"Thirdpartyloanaccountno",
"KVBAmountSanction",
"ApplicantDetails",
"FirstName",
"MiddleName",
"LastName",
"FullName",
"PAN",
"AadharNumber",
"CBSCustId",
"Title",
"Nationality",
"Gender",
"Caste",
"BirthDate",
"Maritalstatus",
"FatherName",
"HusbandName",
"BankingWithUs",
"Religion",
"ExServiceMan",
"PhysicallyHandicapped",
"VoterId",
"DrivingLicId",
"RationCard",
"PassportNo",
"PassportExpDate",
"OtherId",
"OtherIdNo",
"CurrentAddress",
"CurrentCity",
"CurrentState",
"CurrentCountry",
"CurrentZip",
"PermanentAddress",
"PermanentCity",
"PermanentState",
"PermanentCountry",
"PermanentZip",
"PhoneNo",
"Mobile",
"Email",
"Fax",
"RelatedToDirector",
"DirectorBank",
"DirectorName",
"CibilScore",
"EmploymentType",
"CompanyName",
"GroupCompanyAddress",
"CompanyPIN",
"CompanyFax",
"CompanyEmail",
"CompanyPhone",
"CustomerType",
"FinancialApplicant",
"AltMobile",
"Landline",
"Experience",
"ClaimedNMIITR",
"ClaimedAgriIncome",
"ApprovedAgriIncome",
"ExistingEMIClaimed",
"PerfiosEMI",
"CIBILEMI",
"KVBEMI",
"CIBILEMIOverride",
"ComputedEMI",
"ExistingEMIApproved",
"TotalChequeBounce",
"PrimaryBank",
"PrimaryBranch",
"PrimaryAccNo",
"PrimaryIFSC",
"GrossIncome1",
"AnnualCapGain1",
"InterestIncome1",
"AnnualTax1",
"DividendIncome1",
"ShareOfProfit1",
"AgricultureITR1",
"GrossIncome2",
"AnnualCapGain2",
"InterestIncome2",
"AnnualTax2",
"DividendIncome2",
"ShareOfProfit2",
"AgricultureITR2",
"SalaryPaid",
"TaxDeducted",
"OtherReccuringIncome",
"EstimatedBusinessIncome",
"AverageCASABalance",
"ReasonforExemption",
"FIExempted"
})
public class InitKVBRequest {

   	
 @JsonProperty("SourcedBy")
 private String SourcedBy;
 @JsonProperty("SourcedByOffice")
 private String SourcedByOffice;
 @JsonProperty("InterestRate")
 private String InterestRate;
 @JsonProperty("LoanAmount")
 private float LoanAmount;
 @JsonProperty("SanctionDate")
 private String SanctionDate;
 @JsonProperty("EMISanctioned")
 private float EMISanctioned;
 @JsonProperty("SanctionedTerm")
 private String SanctionedTerm;
 @JsonProperty("HolidayPeriod")
 private String HolidayPeriod;
 @JsonProperty("TotalRepayAmount")
 private float TotalRepayAmount;
 @JsonProperty("ReqLoanAmount")
 private float ReqLoanAmount;
 @JsonProperty("ReqTerm")
 private float ReqTerm;
 @JsonProperty("ReqEMI")
 private String ReqEMI;
 @JsonProperty("ChargePaymentComplete")
 private String ChargePaymentComplete;
 @JsonProperty("DateOfDisbursement")
 private String DateOfDisbursement;
 @JsonProperty("DisbursementAmount")
 private float DisbursementAmount;
 @JsonProperty("DateOfAdvancePayment")
 private String DateOfAdvancePayment;
 @JsonProperty("AdvancePaid")
 private String AdvancePaid;
 @JsonProperty("LoanPurpose")
 private String LoanPurpose;
 @JsonProperty("FIVerDate")
 private String FIVerDate;
 @JsonProperty("FISuccess")
 private String FISuccess;
 @JsonProperty("DisbAccBank")
 private String DisbAccBank;
 @JsonProperty("DisbAccBranch")
 private String DisbAccBranch;
 @JsonProperty("DisbAccIFSC")
 private String DisbAccIFSC;
 @JsonProperty("DisbAccNo")
 private String DisbAccNo;
 @JsonProperty("SIAccNo")
 private String SIAccNo;
 @JsonProperty("SIBank")
 private String SIBank;
 @JsonProperty("SIBranch")
 private String SIBranch;
 @JsonProperty("SIIFSC")
 private String SIIFSC;
 @JsonProperty("SIApplicant")
 private String SIApplicant;
 @JsonProperty("ExcessMCLR")
 private String ExcessMCLR;
 @JsonProperty("MCLRType")
 private String MCLRType;
 @JsonProperty("PreClosureRate")
 private String PreClosureRate;
 @JsonProperty("MCLRRate")
 private String MCLRRate;
 @JsonProperty("CASAAccNov")
 private String CASAAccNov;
 @JsonProperty("MODDate")
 private String MODDate;
 @JsonProperty("CKYCM")
 private String CKYCM;
 @JsonProperty("Processing_Fee_Waiver")
 private String ProcessingFeeWaiver;
 @JsonProperty("BankYear")
 private String BankYear;
 @JsonProperty("Reason_for_waiver")
 private String Reasonforwaiver;
 @JsonProperty("Acknowledgement")
 private String Acknowledgement;
 @JsonProperty("Total_Charges")
 private String TotalCharges;
 @JsonProperty("Total_GST_Charges")
 private String TotalGSTCharges;
 @JsonProperty("Scource_Type")
 private String ScourceType;
 @JsonProperty("Type_Of_Loan")
 private String TypeOfLoan;
 @JsonProperty("Third_party_loan_account_no")
 private String Thirdpartyloanaccountno;
 @JsonProperty("KVB_Amount_Sanction")
 private String KVBAmountSanction;
 @JsonProperty("ApplicantDetails")
 private String ApplicantDetails;
 @JsonProperty("FirstName")
 private String FirstName;
 @JsonProperty("MiddleName")
 private String MiddleName;
 @JsonProperty("LastName")
 private String LastName;
 @JsonProperty("FullName")
 private String FullName;
 @JsonProperty("PAN")
 private String PAN;
 @JsonProperty("AadharNumber")
 private String AadharNumber;
 @JsonProperty("CBSCustId")
 private String CBSCustId;
 @JsonProperty("Title")
 private String Title;
 @JsonProperty("Nationality")
 private String Nationality;
 @JsonProperty("Gender")
 private String Gender;
 @JsonProperty("Caste")
 private String Caste;
 @JsonProperty("BirthDate")
 private String BirthDate;
 @JsonProperty("Maritalstatus")
 private String Maritalstatus;
 @JsonProperty("FatherName")
 private String FatherName;
 @JsonProperty("HusbandName")
 private String HusbandName;
 @JsonProperty("BankingWithUs")
 private String BankingWithUs;
 @JsonProperty("Religion")
 private String Religion;
 @JsonProperty("ExServiceMan")
 private String ExServiceMan;
 @JsonProperty("PhysicallyHandicapped")
 private String PhysicallyHandicapped;
 @JsonProperty("VoterId")
 private String VoterId;
 @JsonProperty("DrivingLicId")
 private String DrivingLicId;
 @JsonProperty("RationCard")
 private String RationCard;
 @JsonProperty("PassportNo")
 private String PassportNo;
 @JsonProperty("PassportExpDate")
 private String PassportExpDate;
 @JsonProperty("OtherId")
 private String OtherId;
 @JsonProperty("OtherIdNo")
 private String OtherIdNo;
 @JsonProperty("CurrentAddress")
 private String CurrentAddress;
 @JsonProperty("CurrentCity")
 private String CurrentCity;
 @JsonProperty("CurrentState")
 private String CurrentState;
 @JsonProperty("CurrentCountry")
 private String CurrentCountry;
 @JsonProperty("CurrentZip")
 private String CurrentZip;
 @JsonProperty("PermanentAddress")
 private String PermanentAddress;
 @JsonProperty("PermanentCity")
 private String PermanentCity;
 @JsonProperty("PermanentState")
 private String PermanentState;
 @JsonProperty("PermanentCountry")
 private String PermanentCountry;
 @JsonProperty("PermanentZip")
 private String PermanentZip;
 @JsonProperty("PhoneNo")
 private String PhoneNo;
 @JsonProperty("Mobile")
 private String Mobile;
 @JsonProperty("Email")
 private String Email;
 @JsonProperty("Fax")
 private String Fax;
 @JsonProperty("RelatedToDirector")
 private String RelatedToDirector;
 @JsonProperty("DirectorBank")
 private String DirectorBank;
 @JsonProperty("DirectorName")
 private String DirectorName;
 @JsonProperty("CibilScore")
 private String CibilScore;
 @JsonProperty("EmploymentType")
 private String EmploymentType;
 @JsonProperty("CompanyName")
 private String CompanyName;
 @JsonProperty("GroupCompanyAddress")
 private String GroupCompanyAddress;
 @JsonProperty("CompanyPIN")
 private String CompanyPIN;
 @JsonProperty("CompanyFax")
 private String CompanyFax;
 @JsonProperty("CompanyEmail")
 private String CompanyEmail;
 @JsonProperty("CompanyPhone")
 private String CompanyPhone;
 @JsonProperty("CustomerType")
 private String CustomerType;
 @JsonProperty("FinancialApplicant")
 private String FinancialApplicant;
 @JsonProperty("AltMobile")
 private String AltMobile;
 @JsonProperty("Landline")
 private String Landline;
 @JsonProperty("Experience")
 private String Experience;
 @JsonProperty("ClaimedNMIITR")
 private String ClaimedNMIITR;
 @JsonProperty("ClaimedAgriIncome")
 private String ClaimedAgriIncome;
 @JsonProperty("ApprovedAgriIncome")
 private String ApprovedAgriIncome;
 @JsonProperty("ExistingEMIClaimed")
 private String ExistingEMIClaimed;
 @JsonProperty("PerfiosEMI")
 private String PerfiosEMI;
 @JsonProperty("CIBILEMI")
 private String CIBILEMI;
 @JsonProperty("KVBEMI")
 private String KVBEMI;
 @JsonProperty("CIBILEMIOverride")
 private String CIBILEMIOverride;
 @JsonProperty("ComputedEMI")
 private String ComputedEMI;
 @JsonProperty("ExistingEMIApproved")
 private String ExistingEMIApproved;
 @JsonProperty("TotalChequeBounce")
 private String TotalChequeBounce;
 @JsonProperty("PrimaryBank")
 private String PrimaryBank;
 @JsonProperty("PrimaryBranch")
 private String PrimaryBranch;
 @JsonProperty("PrimaryAccNo")
 private String PrimaryAccNo;
 @JsonProperty("PrimaryIFSC")
 private String PrimaryIFSC;
 @JsonProperty("GrossIncome1")
 private String GrossIncome1;
 @JsonProperty("AnnualCapGain1")
 private String AnnualCapGain1;
 @JsonProperty("InterestIncome1")
 private String InterestIncome1;
 @JsonProperty("AnnualTax1")
 private String AnnualTax1;
 @JsonProperty("DividendIncome1")
 private String DividendIncome1;
 @JsonProperty("ShareOfProfit1")
 private String ShareOfProfit1;
 @JsonProperty("AgricultureITR1")
 private String AgricultureITR1;
 @JsonProperty("GrossIncome2")
 private String GrossIncome2;
 @JsonProperty("AnnualCapGain2")
 private String AnnualCapGain2;
 @JsonProperty("InterestIncome2")
 private String InterestIncome2;
 @JsonProperty("AnnualTax2")
 private String AnnualTax2;
 @JsonProperty("DividendIncome2")
 private String DividendIncome2;
 @JsonProperty("ShareOfProfit2")
 private String ShareOfProfit2;
 @JsonProperty("AgricultureITR2")
 private String AgricultureITR2;
 @JsonProperty("SalaryPaid")
 private String SalaryPaid;
 @JsonProperty("TaxDeducted")
 private String TaxDeducted;
 @JsonProperty("OtherReccuringIncome")
 private String OtherReccuringIncome;
 @JsonProperty("EstimatedBusinessIncome")
 private String EstimatedBusinessIncome;
 @JsonProperty("Average_CASA_Balance")
private String AverageCASABalance;
 @JsonProperty("Reason_for_Exemption")
 private String ReasonforExemption;
 @JsonProperty("FI_Exempted")
 private String FIExempted;
 
 
 private final static long serialVersionUID = 6858285497845363453L;


    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


	public String getSourcedBy() {
		return SourcedBy;
	}


	public void setSourcedByM(String sourcedBy) {
		SourcedBy = sourcedBy;
	}


	public String getSourcedByOffice() {
		return SourcedByOffice;
	}


	public void setSourcedByOffice(String sourcedByOffice) {
		SourcedByOffice = sourcedByOffice;
	}


	public String getInterestRate() {
		return InterestRate;
	}


	public void setInterestRateM(String interestRate) {
		InterestRate = interestRate;
	}


	public float getLoanAmount() {
		return LoanAmount;
	}


	public void setLoanAmountM(float loanAmount) {
		LoanAmount = loanAmount;
	}


	public String getSanctionDate() {
		return SanctionDate;
	}


	public void setSanctionDateM(String sanctionDate) {
		SanctionDate = sanctionDate;
	}


	public float getEMISanctioned() {
		return EMISanctioned;
	}


	public void setEMISanctionedM(float eMISanctioned) {
		EMISanctioned = eMISanctioned;
	}


	public String getSanctionedTerm() {
		return SanctionedTerm;
	}


	public void setSanctionedTerm(String sanctionedTerm) {
		SanctionedTerm = sanctionedTerm;
	}


	public String getHolidayPeriod() {
		return HolidayPeriod;
	}


	public void setHolidayPeriodM(String holidayPeriod) {
		HolidayPeriod = holidayPeriod;
	}


	public float getTotalRepayAmount() {
		return TotalRepayAmount;
	}


	public void setTotalRepayAmount(float totalRepayAmount) {
		TotalRepayAmount = totalRepayAmount;
	}


	public float getReqLoanAmount() {
		return ReqLoanAmount;
	}


	public void setReqLoanAmount(float reqLoanAmount) {
		ReqLoanAmount = reqLoanAmount;
	}


	public float getReqTerm() {
		return ReqTerm;
	}


	public void setReqTerm(float reqTerm) {
		ReqTerm = reqTerm;
	}


	public String getReqEMI() {
		return ReqEMI;
	}


	public void setReqEMI(String reqEMI) {
		ReqEMI = reqEMI;
	}


	public String getChargePaymentComplete() {
		return ChargePaymentComplete;
	}


	public void setChargePaymentComplete(String chargePaymentComplete) {
		ChargePaymentComplete = chargePaymentComplete;
	}


	public String getDateOfDisbursement() {
		return DateOfDisbursement;
	}


	public void setDateOfDisbursement(String dateOfDisbursement) {
		DateOfDisbursement = dateOfDisbursement;
	}


	public float getDisbursementAmount() {
		return DisbursementAmount;
	}


	public void setDisbursementAmount(float disbursementAmount) {
		DisbursementAmount = disbursementAmount;
	}


	public String getDateOfAdvancePayment() {
		return DateOfAdvancePayment;
	}


	public void setDateOfAdvancePayment(String dateOfAdvancePayment) {
		DateOfAdvancePayment = dateOfAdvancePayment;
	}


	public String getAdvancePaid() {
		return AdvancePaid;
	}


	public void setAdvancePaid(String advancePaid) {
		AdvancePaid = advancePaid;
	}


	public String getLoanPurpose() {
		return LoanPurpose;
	}


	public void setLoanPurposeM(String loanPurpose) {
		LoanPurpose = loanPurpose;
	}


	public String getFIVerDate() {
		return FIVerDate;
	}


	public void setFIVerDate(String fIVerDate) {
		FIVerDate = fIVerDate;
	}


	public String getFISuccess() {
		return FISuccess;
	}


	public void setFISuccess(String fISuccess) {
		FISuccess = fISuccess;
	}


	public String getDisbAccBank() {
		return DisbAccBank;
	}


	public void setDisbAccBank(String disbAccBank) {
		DisbAccBank = disbAccBank;
	}


	public String getDisbAccBranch() {
		return DisbAccBranch;
	}


	public void setDisbAccBranch(String disbAccBranch) {
		DisbAccBranch = disbAccBranch;
	}


	public String getDisbAccIFSC() {
		return DisbAccIFSC;
	}


	public void setDisbAccIFSC(String disbAccIFSC) {
		DisbAccIFSC = disbAccIFSC;
	}


	public String getDisbAccNo() {
		return DisbAccNo;
	}


	public void setDisbAccNo(String disbAccNo) {
		DisbAccNo = disbAccNo;
	}


	public String getSIAccNo() {
		return SIAccNo;
	}


	public void setSIAccNo(String sIAccNo) {
		SIAccNo = sIAccNo;
	}


	public String getSIBank() {
		return SIBank;
	}


	public void setSIBank(String sIBank) {
		SIBank = sIBank;
	}


	public String getSIBranch() {
		return SIBranch;
	}


	public void setSIBranch(String sIBranch) {
		SIBranch = sIBranch;
	}


	public String getSIIFSC() {
		return SIIFSC;
	}


	public void setSIIFSC(String sIIFSC) {
		SIIFSC = sIIFSC;
	}


	public String getSIApplicant() {
		return SIApplicant;
	}


	public void setSIApplicant(String sIApplicant) {
		SIApplicant = sIApplicant;
	}


	public String getExcessMCLR() {
		return ExcessMCLR;
	}


	public void setExcessMCLR(String excessMCLR) {
		ExcessMCLR = excessMCLR;
	}


	public String getMCLRType() {
		return MCLRType;
	}


	public void setMCLRType(String mCLRType) {
		MCLRType = mCLRType;
	}


	public String getPreClosureRate() {
		return PreClosureRate;
	}


	public void setPreClosureRate(String preClosureRate) {
		PreClosureRate = preClosureRate;
	}


	public String getMCLRRate() {
		return MCLRRate;
	}


	public void setMCLRRate(String mCLRRate) {
		MCLRRate = mCLRRate;
	}


	public String getCASAAccNov() {
		return CASAAccNov;
	}


	public void setCASAAccNov(String cASAAccNov) {
		CASAAccNov = cASAAccNov;
	}


	public String getMODDate() {
		return MODDate;
	}


	public void setMODDate(String mODDate) {
		MODDate = mODDate;
	}


	public String getCKYCM() {
		return CKYCM;
	}


	public void setCKYCM(String cKYCM) {
		CKYCM = cKYCM;
	}


	public String getProcessingFeeWaiver() {
		return ProcessingFeeWaiver;
	}


	public void setProcessingFeeWaiver(String processingFeeWaiver) {
		ProcessingFeeWaiver = processingFeeWaiver;
	}


	public String getBankYear() {
		return BankYear;
	}


	public void setBankYear(String bankYear) {
		BankYear = bankYear;
	}


	public String getReasonforwaiver() {
		return Reasonforwaiver;
	}


	public void setReasonforwaiver(String reasonforwaiver) {
		Reasonforwaiver = reasonforwaiver;
	}


	public String getAcknowledgement() {
		return Acknowledgement;
	}


	public void setAcknowledgement(String acknowledgement) {
		Acknowledgement = acknowledgement;
	}


	public String getTotalCharges() {
		return TotalCharges;
	}


	public void setTotalCharges(String totalCharges) {
		TotalCharges = totalCharges;
	}


	public String getTotalGSTCharges() {
		return TotalGSTCharges;
	}


	public void setTotalGSTCharges(String totalGSTCharges) {
		TotalGSTCharges = totalGSTCharges;
	}


	public String getScourceType() {
		return ScourceType;
	}


	public void setScourceTypeM(String scourceType) {
		ScourceType = scourceType;
	}


	public String getTypeOfLoan() {
		return TypeOfLoan;
	}


	public void setTypeOfLoanM(String typeOfLoan) {
		TypeOfLoan = typeOfLoan;
	}


	public String getThirdpartyloanaccountno() {
		return Thirdpartyloanaccountno;
	}


	public void setThirdpartyloanaccountno(String thirdpartyloanaccountno) {
		Thirdpartyloanaccountno = thirdpartyloanaccountno;
	}


	public String getKVBAmountSanction() {
		return KVBAmountSanction;
	}


	public void setKVBAmountSanction(String kVBAmountSanction) {
		KVBAmountSanction = kVBAmountSanction;
	}


	public String getApplicantDetails() {
		return ApplicantDetails;
	}


	public void setApplicantDetails(String applicantDetails) {
		ApplicantDetails = applicantDetails;
	}


	public String getFirstName() {
		return FirstName;
	}


	public void setFirstNameM(String firstName) {
		FirstName = firstName;
	}


	public String getMiddleName() {
		return MiddleName;
	}


	public void setMiddleName(String middleName) {
		MiddleName = middleName;
	}


	public String getLastName() {
		return LastName;
	}


	public void setLastNameM(String lastName) {
		LastName = lastName;
	}


	public String getFullName() {
		return FullName;
	}


	public void setFullName(String fullName) {
		FullName = fullName;
	}


	public String getPAN() {
		return PAN;
	}


	public void setPANM(String pAN) {
		PAN = pAN;
	}


	public String getAadharNumber() {
		return AadharNumber;
	}


	public void setAadharNumberM(String aadharNumber) {
		AadharNumber = aadharNumber;
	}


	public String getCBSCustId() {
		return CBSCustId;
	}


	public void setCBSCustId(String cBSCustId) {
		CBSCustId = cBSCustId;
	}


	public String getTitle() {
		return Title;
	}


	public void setTitleM(String title) {
		Title = title;
	}


	public String getNationality() {
		return Nationality;
	}


	public void setNationalityM(String nationality) {
		Nationality = nationality;
	}


	public String getGender() {
		return Gender;
	}


	public void setGenderM(String gender) {
		Gender = gender;
	}


	public String getCaste() {
		return Caste;
	}


	public void setCaste(String caste) {
		Caste = caste;
	}


	public String getBirthDate() {
		return BirthDate;
	}


	public void setBirthDateM(String birthDate) {
		BirthDate = birthDate;
	}


	public String getMaritalstatus() {
		return Maritalstatus;
	}


	public void setMaritalstatusM(String maritalstatus) {
		Maritalstatus = maritalstatus;
	}


	public String getFatherName() {
		return FatherName;
	}


	public void setFatherName(String fatherName) {
		FatherName = fatherName;
	}


	public String getHusbandName() {
		return HusbandName;
	}


	public void setHusbandName(String husbandName) {
		HusbandName = husbandName;
	}


	public String getBankingWithUs() {
		return BankingWithUs;
	}


	public void setBankingWithUs(String bankingWithUs) {
		BankingWithUs = bankingWithUs;
	}


	public String getReligion() {
		return Religion;
	}


	public void setReligion(String religion) {
		Religion = religion;
	}


	public String getExServiceMan() {
		return ExServiceMan;
	}


	public void setExServiceMan(String exServiceMan) {
		ExServiceMan = exServiceMan;
	}


	public String getPhysicallyHandicapped() {
		return PhysicallyHandicapped;
	}


	public void setPhysicallyHandicapped(String physicallyHandicapped) {
		PhysicallyHandicapped = physicallyHandicapped;
	}


	public String getVoterId() {
		return VoterId;
	}


	public void setVoterId(String voterId) {
		VoterId = voterId;
	}


	public String getDrivingLicId() {
		return DrivingLicId;
	}


	public void setDrivingLicId(String drivingLicId) {
		DrivingLicId = drivingLicId;
	}


	public String getRationCard() {
		return RationCard;
	}


	public void setRationCard(String rationCard) {
		RationCard = rationCard;
	}


	public String getPassportNo() {
		return PassportNo;
	}


	public void setPassportNo(String passportNo) {
		PassportNo = passportNo;
	}


	public String getPassportExpDate() {
		return PassportExpDate;
	}


	public void setPassportExpDate(String passportExpDate) {
		PassportExpDate = passportExpDate;
	}


	public String getOtherId() {
		return OtherId;
	}


	public void setOtherId(String otherId) {
		OtherId = otherId;
	}


	public String getOtherIdNo() {
		return OtherIdNo;
	}


	public void setOtherIdNo(String otherIdNo) {
		OtherIdNo = otherIdNo;
	}


	public String getCurrentAddress() {
		return CurrentAddress;
	}


	public void setCurrentAddress(String currentAddress) {
		CurrentAddress = currentAddress;
	}


	public String getCurrentCity() {
		return CurrentCity;
	}


	public void setCurrentCity(String currentCity) {
		CurrentCity = currentCity;
	}


	public String getCurrentState() {
		return CurrentState;
	}


	public void setCurrentState(String currentState) {
		CurrentState = currentState;
	}


	public String getCurrentCountry() {
		return CurrentCountry;
	}


	public void setCurrentCountry(String currentCountry) {
		CurrentCountry = currentCountry;
	}


	public String getCurrentZip() {
		return CurrentZip;
	}


	public void setCurrentZip(String currentZip) {
		CurrentZip = currentZip;
	}


	public String getPermanentAddress() {
		return PermanentAddress;
	}


	public void setPermanentAddress(String permanentAddress) {
		PermanentAddress = permanentAddress;
	}


	public String getPermanentCity() {
		return PermanentCity;
	}


	public void setPermanentCity(String permanentCity) {
		PermanentCity = permanentCity;
	}


	public String getPermanentState() {
		return PermanentState;
	}


	public void setPermanentState(String permanentState) {
		PermanentState = permanentState;
	}


	public String getPermanentCountry() {
		return PermanentCountry;
	}


	public void setPermanentCountry(String permanentCountry) {
		PermanentCountry = permanentCountry;
	}


	public String getPermanentZip() {
		return PermanentZip;
	}


	public void setPermanentZip(String permanentZip) {
		PermanentZip = permanentZip;
	}


	public String getPhoneNo() {
		return PhoneNo;
	}


	public void setPhoneNo(String phoneNo) {
		PhoneNo = phoneNo;
	}


	public String getMobile() {
		return Mobile;
	}


	public void setMobileM(String mobile) {
		Mobile = mobile;
	}


	public String getEmail() {
		return Email;
	}


	public void setEmailM(String email) {
		Email = email;
	}


	public String getFax() {
		return Fax;
	}


	public void setFax(String fax) {
		Fax = fax;
	}


	public String getRelatedToDirector() {
		return RelatedToDirector;
	}


	public void setRelatedToDirector(String relatedToDirector) {
		RelatedToDirector = relatedToDirector;
	}


	public String getDirectorBank() {
		return DirectorBank;
	}


	public void setDirectorBank(String directorBank) {
		DirectorBank = directorBank;
	}


	public String getDirectorName() {
		return DirectorName;
	}


	public void setDirectorName(String directorName) {
		DirectorName = directorName;
	}


	public String getCibilScore() {
		return CibilScore;
	}


	public void setCibilScore(String cibilScore) {
		CibilScore = cibilScore;
	}


	public String getEmploymentType() {
		return EmploymentType;
	}


	public void setEmploymentTypeM(String employmentType) {
		EmploymentType = employmentType;
	}


	public String getCompanyName() {
		return CompanyName;
	}


	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}


	public String getGroupCompanyAddress() {
		return GroupCompanyAddress;
	}


	public void setGroupCompanyAddress(String groupCompanyAddress) {
		GroupCompanyAddress = groupCompanyAddress;
	}


	public String getCompanyPIN() {
		return CompanyPIN;
	}


	public void setCompanyPIN(String companyPIN) {
		CompanyPIN = companyPIN;
	}


	public String getCompanyFax() {
		return CompanyFax;
	}


	public void setCompanyFax(String companyFax) {
		CompanyFax = companyFax;
	}


	public String getCompanyEmail() {
		return CompanyEmail;
	}


	public void setCompanyEmail(String companyEmail) {
		CompanyEmail = companyEmail;
	}


	public String getCompanyPhone() {
		return CompanyPhone;
	}


	public void setCompanyPhone(String companyPhone) {
		CompanyPhone = companyPhone;
	}


	public String getCustomerType() {
		return CustomerType;
	}


	public void setCustomerTypeM(String customerType) {
		CustomerType = customerType;
	}


	public String getFinancialApplicant() {
		return FinancialApplicant;
	}


	public void setFinancialApplicantM(String financialApplicant) {
		FinancialApplicant = financialApplicant;
	}


	public String getAltMobile() {
		return AltMobile;
	}


	public void setAltMobile(String altMobile) {
		AltMobile = altMobile;
	}


	public String getLandline() {
		return Landline;
	}


	public void setLandline(String landline) {
		Landline = landline;
	}


	public String getExperience() {
		return Experience;
	}


	public void setExperience(String experience) {
		Experience = experience;
	}


	public String getClaimedNMIITR() {
		return ClaimedNMIITR;
	}


	public void setClaimedNMIITR(String claimedNMIITR) {
		ClaimedNMIITR = claimedNMIITR;
	}


	public String getClaimedAgriIncome() {
		return ClaimedAgriIncome;
	}


	public void setClaimedAgriIncome(String claimedAgriIncome) {
		ClaimedAgriIncome = claimedAgriIncome;
	}


	public String getApprovedAgriIncome() {
		return ApprovedAgriIncome;
	}


	public void setApprovedAgriIncome(String approvedAgriIncome) {
		ApprovedAgriIncome = approvedAgriIncome;
	}


	public String getExistingEMIClaimed() {
		return ExistingEMIClaimed;
	}


	public void setExistingEMIClaimed(String existingEMIClaimed) {
		ExistingEMIClaimed = existingEMIClaimed;
	}


	public String getPerfiosEMI() {
		return PerfiosEMI;
	}


	public void setPerfiosEMI(String perfiosEMI) {
		PerfiosEMI = perfiosEMI;
	}


	public String getCIBILEMI() {
		return CIBILEMI;
	}


	public void setCIBILEMI(String cIBILEMI) {
		CIBILEMI = cIBILEMI;
	}


	public String getKVBEMI() {
		return KVBEMI;
	}


	public void setKVBEMI(String kVBEMI) {
		KVBEMI = kVBEMI;
	}


	public String getCIBILEMIOverride() {
		return CIBILEMIOverride;
	}


	public void setCIBILEMIOverride(String cIBILEMIOverride) {
		CIBILEMIOverride = cIBILEMIOverride;
	}


	public String getComputedEMI() {
		return ComputedEMI;
	}


	public void setComputedEMI(String computedEMI) {
		ComputedEMI = computedEMI;
	}


	public String getExistingEMIApproved() {
		return ExistingEMIApproved;
	}


	public void setExistingEMIApproved(String existingEMIApproved) {
		ExistingEMIApproved = existingEMIApproved;
	}


	public String getTotalChequeBounce() {
		return TotalChequeBounce;
	}


	public void setTotalChequeBounce(String totalChequeBounce) {
		TotalChequeBounce = totalChequeBounce;
	}


	public String getPrimaryBank() {
		return PrimaryBank;
	}


	public void setPrimaryBank(String primaryBank) {
		PrimaryBank = primaryBank;
	}


	public String getPrimaryBranch() {
		return PrimaryBranch;
	}


	public void setPrimaryBranch(String primaryBranch) {
		PrimaryBranch = primaryBranch;
	}


	public String getPrimaryAccNo() {
		return PrimaryAccNo;
	}


	public void setPrimaryAccNo(String primaryAccNo) {
		PrimaryAccNo = primaryAccNo;
	}


	public String getPrimaryIFSC() {
		return PrimaryIFSC;
	}


	public void setPrimaryIFSC(String primaryIFSC) {
		PrimaryIFSC = primaryIFSC;
	}


	public String getGrossIncome1() {
		return GrossIncome1;
	}


	public void setGrossIncome1(String grossIncome1) {
		GrossIncome1 = grossIncome1;
	}


	public String getAnnualCapGain1() {
		return AnnualCapGain1;
	}


	public void setAnnualCapGain1(String annualCapGain1) {
		AnnualCapGain1 = annualCapGain1;
	}


	public String getInterestIncome1() {
		return InterestIncome1;
	}


	public void setInterestIncome1(String interestIncome1) {
		InterestIncome1 = interestIncome1;
	}


	public String getAnnualTax1() {
		return AnnualTax1;
	}


	public void setAnnualTax1(String annualTax1) {
		AnnualTax1 = annualTax1;
	}


	public String getDividendIncome1() {
		return DividendIncome1;
	}


	public void setDividendIncome1(String dividendIncome1) {
		DividendIncome1 = dividendIncome1;
	}


	public String getShareOfProfit1() {
		return ShareOfProfit1;
	}


	public void setShareOfProfit1(String shareOfProfit1) {
		ShareOfProfit1 = shareOfProfit1;
	}


	public String getAgricultureITR1() {
		return AgricultureITR1;
	}


	public void setAgricultureITR1(String agricultureITR1) {
		AgricultureITR1 = agricultureITR1;
	}


	public String getGrossIncome2() {
		return GrossIncome2;
	}


	public void setGrossIncome2(String grossIncome2) {
		GrossIncome2 = grossIncome2;
	}


	public String getAnnualCapGain2() {
		return AnnualCapGain2;
	}


	public void setAnnualCapGain2(String annualCapGain2) {
		AnnualCapGain2 = annualCapGain2;
	}


	public String getInterestIncome2() {
		return InterestIncome2;
	}


	public void setInterestIncome2(String interestIncome2) {
		InterestIncome2 = interestIncome2;
	}


	public String getAnnualTax2() {
		return AnnualTax2;
	}


	public void setAnnualTax2(String annualTax2) {
		AnnualTax2 = annualTax2;
	}


	public String getDividendIncome2() {
		return DividendIncome2;
	}


	public void setDividendIncome2(String dividendIncome2) {
		DividendIncome2 = dividendIncome2;
	}


	public String getShareOfProfit2() {
		return ShareOfProfit2;
	}


	public void setShareOfProfit2(String shareOfProfit2) {
		ShareOfProfit2 = shareOfProfit2;
	}


	public String getAgricultureITR2() {
		return AgricultureITR2;
	}


	public void setAgricultureITR2(String agricultureITR2) {
		AgricultureITR2 = agricultureITR2;
	}


	public String getSalaryPaid() {
		return SalaryPaid;
	}


	public void setSalaryPaid(String salaryPaid) {
		SalaryPaid = salaryPaid;
	}


	public String getTaxDeducted() {
		return TaxDeducted;
	}


	public void setTaxDeducted(String taxDeducted) {
		TaxDeducted = taxDeducted;
	}


	public String getOtherReccuringIncome() {
		return OtherReccuringIncome;
	}


	public void setOtherReccuringIncome(String otherReccuringIncome) {
		OtherReccuringIncome = otherReccuringIncome;
	}


	public String getEstimatedBusinessIncome() {
		return EstimatedBusinessIncome;
	}


	public void setEstimatedBusinessIncome(String estimatedBusinessIncome) {
		EstimatedBusinessIncome = estimatedBusinessIncome;
	}


	public String getAverageCASABalance() {
		return AverageCASABalance;
	}


	public void setAverageCASABalance(String averageCASABalance) {
		AverageCASABalance = averageCASABalance;
	}


	public String getReasonforExemption() {
		return ReasonforExemption;
	}


	public void setReasonforExemption(String reasonforExemption) {
		ReasonforExemption = reasonforExemption;
	}


	public String getFIExempted() {
		return FIExempted;
	}


	public void setFIExempted(String fIExempted) {
		FIExempted = fIExempted;
	}


	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}


	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}
    

}

