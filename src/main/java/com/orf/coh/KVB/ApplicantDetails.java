package com.orf.coh.KVB;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ApplicantDetails {

@SerializedName("FirstName")
@Expose
private String firstName;
@SerializedName("MiddleName")
@Expose
private String middleName;
@SerializedName("LastName")
@Expose
private String lastName;
@SerializedName("FullName")
@Expose
private String fullName;
@SerializedName("PAN")
@Expose
private String pAN;
@SerializedName("AadharNumber")
@Expose
private String aadharNumber;
@SerializedName("CBSCustId")
@Expose
private String cBSCustId;
@SerializedName("Title")
@Expose
private String title;
@SerializedName("Nationality")
@Expose
private String nationality;
@SerializedName("Gender")
@Expose
private String gender;
@SerializedName("Caste")
@Expose
private String caste;
@SerializedName("BirthDate")
@Expose
private String birthDate;
@SerializedName("Maritalstatus")
@Expose
private String maritalstatus;
@SerializedName("FatherName")
@Expose
private String fatherName;
@SerializedName("MotherName")
@Expose
private String motherName;
@SerializedName("HusbandName")
@Expose
private String husbandName;
@SerializedName("BankingWithUs")
@Expose
private String bankingWithUs;
@SerializedName("Religion")
@Expose
private String religion;
@SerializedName("ExServiceMan")
@Expose
private String exServiceMan;
@SerializedName("PhysicallyHandicapped")
@Expose
private String physicallyHandicapped;
@SerializedName("VoterId")
@Expose
private String voterId;
@SerializedName("DrivingLicId")
@Expose
private String drivingLicId;
@SerializedName("RationCard")
@Expose
private String rationCard;
@SerializedName("PassportNo")
@Expose
private String passportNo;
@SerializedName("PassportExpDate")
@Expose
private String passportExpDate;
@SerializedName("OtherId")
@Expose
private String otherId;
@SerializedName("OtherIdNo")
@Expose
private String otherIdNo;
@SerializedName("CurrentAddress")
@Expose
private String currentAddress;
@SerializedName("CurrentCity")
@Expose
private String currentCity;
@SerializedName("CurrentState")
@Expose
private String currentState;
@SerializedName("CurrentCountry")
@Expose
private String currentCountry;
@SerializedName("CurrentZip")
@Expose
private String currentZip;
@SerializedName("PermanentAddress")
@Expose
private String permanentAddress;
@SerializedName("PermanentCity")
@Expose
private String permanentCity;
@SerializedName("PermanentState")
@Expose
private String permanentState;
@SerializedName("PermanentCountry")
@Expose
private String permanentCountry;
@SerializedName("PermanentZip")
@Expose
private String permanentZip;
@SerializedName("PhoneNo")
@Expose
private String phoneNo;
@SerializedName("Mobile")
@Expose
private String mobile;
@SerializedName("Email")
@Expose
private String email;
@SerializedName("Fax")
@Expose
private String fax;
@SerializedName("CibilScore")
@Expose
private String cibilScore;
@SerializedName("EmploymentType")
@Expose
private String employmentType;
@SerializedName("CompanyName")
@Expose
private String companyName;
@SerializedName("GroupCompanyAddress")
@Expose
private String groupCompanyAddress;
@SerializedName("CompanyPIN")
@Expose
private String companyPIN;
@SerializedName("CompanyFax")
@Expose
private String companyFax;
@SerializedName("CompanyEmail")
@Expose
private String companyEmail;
@SerializedName("CompanyPhone")
@Expose
private String companyPhone;
@SerializedName("CustomerType")
@Expose
private String customerType;
@SerializedName("FinancialApplicant")
@Expose
private String financialApplicant;
@SerializedName("AltMobile")
@Expose
private String altMobile;
@SerializedName("Landline")
@Expose
private String landline;
@SerializedName("Experience")
@Expose
private String experience;
@SerializedName("ClaimedNMIITR")
@Expose
private String claimedNMIITR;
@SerializedName("ClaimedAgriIncome")
@Expose
private String claimedAgriIncome;
@SerializedName("ApprovedAgriIncome")
@Expose
private String approvedAgriIncome;
@SerializedName("ExistingEMIClaimed")
@Expose
private String existingEMIClaimed;
@SerializedName("PerfiosEMI")
@Expose
private String perfiosEMI;
@SerializedName("CIBILEMI")
@Expose
private String cIBILEMI;
@SerializedName("KVBEMI")
@Expose
private String kVBEMI;
@SerializedName("CIBILEMIOverride")
@Expose
private String cIBILEMIOverride;
@SerializedName("ComputedEMI")
@Expose
private String computedEMI;
@SerializedName("ExistingEMIApproved")
@Expose
private String existingEMIApproved;
@SerializedName("EducationQualification")
@Expose
private String educationQualification;
@SerializedName("TotalChequeBounce")
@Expose
private String totalChequeBounce;
@SerializedName("PrimaryBank")
@Expose
private String primaryBank;
@SerializedName("PrimaryBranch")
@Expose
private String primaryBranch;
@SerializedName("PrimaryAccNo")
@Expose
private String primaryAccNo;
@SerializedName("PrimaryIFSC")
@Expose
private String primaryIFSC;
@SerializedName("GrossIncome1")
@Expose
private String grossIncome1;
@SerializedName("AnnualCapGain1")
@Expose
private String annualCapGain1;
@SerializedName("InterestIncome1")
@Expose
private String interestIncome1;
@SerializedName("AnnualTax1")
@Expose
private String annualTax1;
@SerializedName("DividendIncome1")
@Expose
private String dividendIncome1;
@SerializedName("ShareOfProfit1")
@Expose
private String shareOfProfit1;
@SerializedName("AgricultureITR1")
@Expose
private String agricultureITR1;
@SerializedName("GrossIncome2")
@Expose
private String grossIncome2;
@SerializedName("AnnualCapGain2")
@Expose
private String annualCapGain2;
@SerializedName("InterestIncome2")
@Expose
private String interestIncome2;
@SerializedName("AnnualTax2")
@Expose
private String annualTax2;
@SerializedName("DividendIncome2")
@Expose
private String dividendIncome2;
@SerializedName("ShareOfProfit2")
@Expose
private String shareOfProfit2;
@SerializedName("AgricultureITR2")
@Expose
private String agricultureITR2;
@SerializedName("MonthlyIncome")
@Expose
private String monthlyIncome;
@SerializedName("SalaryPaid")
@Expose
private String salaryPaid;
@SerializedName("TaxDeducted")
@Expose
private String taxDeducted;
@SerializedName("OtherReccuringIncome")
@Expose
private String otherReccuringIncome;
@SerializedName("EstimatedBusinessIncome")
@Expose
private String estimatedBusinessIncome;
@SerializedName("AverageCASABal")
@Expose
private String averageCASABal;
@SerializedName("Average_CASA_Balance")
@Expose
private String averageCASABalance;
@SerializedName("Reason_for_Exemption")
@Expose
private String reasonForExemption;
@SerializedName("FI_Exempted")
@Expose
private String fIExempted;

/**
* No args constructor for use in serialization
* 
*/
public ApplicantDetails() {
}

/**
* 
* @param passportNo
* @param otherIdNo
* @param voterId
* @param primaryBank
* @param companyPhone
* @param permanentCity
* @param approvedAgriIncome
* @param dividendIncome1
* @param dividendIncome2
* @param agricultureITR1
* @param monthlyIncome
* @param permanentCountry
* @param agricultureITR2
* @param religion
* @param averageCASABalance
* @param primaryAccNo
* @param employmentType
* @param shareOfProfit2
* @param educationQualification
* @param maritalstatus
* @param pAN
* @param currentZip
* @param gender
* @param birthDate
* @param shareOfProfit1
* @param cIBILEMIOverride
* @param primaryIFSC
* @param permanentAddress
* @param middleName
* @param lastName
* @param motherName
* @param fax
* @param salaryPaid
* @param financialApplicant
* @param cBSCustId
* @param computedEMI
* @param companyName
* @param phoneNo
* @param companyPIN
* @param email
* @param primaryBranch
* @param totalChequeBounce
* @param husbandName
* @param reasonForExemption
* @param drivingLicId
* @param aadharNumber
* @param otherId
* @param mobile
* @param estimatedBusinessIncome
* @param otherReccuringIncome
* @param currentState
* @param interestIncome2
* @param annualTax1
* @param interestIncome1
* @param caste
* @param annualTax2
* @param exServiceMan
* @param altMobile
* @param grossIncome1
* @param kVBEMI
* @param grossIncome2
* @param perfiosEMI
* @param claimedNMIITR
* @param landline
* @param title
* @param cIBILEMI
* @param taxDeducted
* @param physicallyHandicapped
* @param experience
* @param annualCapGain1
* @param firstName
* @param fIExempted
* @param claimedAgriIncome
* @param customerType
* @param companyFax
* @param companyEmail
* @param annualCapGain2
* @param rationCard
* @param groupCompanyAddress
* @param permanentZip
* @param currentCity
* @param passportExpDate
* @param currentCountry
* @param existingEMIClaimed
* @param nationality
* @param currentAddress
* @param existingEMIApproved
* @param averageCASABal
* @param fullName
* @param bankingWithUs
* @param fatherName
* @param permanentState
* @param cibilScore
*/
public ApplicantDetails(String firstName, String middleName, String lastName, String fullName, String pAN, String aadharNumber, String cBSCustId, String title, String nationality, String gender, String caste, String birthDate, String maritalstatus, String fatherName, String motherName, String husbandName, String bankingWithUs, String religion, String exServiceMan, String physicallyHandicapped, String voterId, String drivingLicId, String rationCard, String passportNo, String passportExpDate, String otherId, String otherIdNo, String currentAddress, String currentCity, String currentState, String currentCountry, String currentZip, String permanentAddress, String permanentCity, String permanentState, String permanentCountry, String permanentZip, String phoneNo, String mobile, String email, String fax, String cibilScore, String employmentType, String companyName, String groupCompanyAddress, String companyPIN, String companyFax, String companyEmail, String companyPhone, String customerType, String financialApplicant, String altMobile, String landline, String experience, String claimedNMIITR, String claimedAgriIncome, String approvedAgriIncome, String existingEMIClaimed, String perfiosEMI, String cIBILEMI, String kVBEMI, String cIBILEMIOverride, String computedEMI, String existingEMIApproved, String educationQualification, String totalChequeBounce, String primaryBank, String primaryBranch, String primaryAccNo, String primaryIFSC, String grossIncome1, String annualCapGain1, String interestIncome1, String annualTax1, String dividendIncome1, String shareOfProfit1, String agricultureITR1, String grossIncome2, String annualCapGain2, String interestIncome2, String annualTax2, String dividendIncome2, String shareOfProfit2, String agricultureITR2, String monthlyIncome, String salaryPaid, String taxDeducted, String otherReccuringIncome, String estimatedBusinessIncome, String averageCASABal, String averageCASABalance, String reasonForExemption, String fIExempted) {
super();
this.firstName = firstName;
this.middleName = middleName;
this.lastName = lastName;
this.fullName = fullName;
this.pAN = pAN;
this.aadharNumber = aadharNumber;
this.cBSCustId = cBSCustId;
this.title = title;
this.nationality = nationality;
this.gender = gender;
this.caste = caste;
this.birthDate = birthDate;
this.maritalstatus = maritalstatus;
this.fatherName = fatherName;
this.motherName = motherName;
this.husbandName = husbandName;
this.bankingWithUs = bankingWithUs;
this.religion = religion;
this.exServiceMan = exServiceMan;
this.physicallyHandicapped = physicallyHandicapped;
this.voterId = voterId;
this.drivingLicId = drivingLicId;
this.rationCard = rationCard;
this.passportNo = passportNo;
this.passportExpDate = passportExpDate;
this.otherId = otherId;
this.otherIdNo = otherIdNo;
this.currentAddress = currentAddress;
this.currentCity = currentCity;
this.currentState = currentState;
this.currentCountry = currentCountry;
this.currentZip = currentZip;
this.permanentAddress = permanentAddress;
this.permanentCity = permanentCity;
this.permanentState = permanentState;
this.permanentCountry = permanentCountry;
this.permanentZip = permanentZip;
this.phoneNo = phoneNo;
this.mobile = mobile;
this.email = email;
this.fax = fax;
this.cibilScore = cibilScore;
this.employmentType = employmentType;
this.companyName = companyName;
this.groupCompanyAddress = groupCompanyAddress;
this.companyPIN = companyPIN;
this.companyFax = companyFax;
this.companyEmail = companyEmail;
this.companyPhone = companyPhone;
this.customerType = customerType;
this.financialApplicant = financialApplicant;
this.altMobile = altMobile;
this.landline = landline;
this.experience = experience;
this.claimedNMIITR = claimedNMIITR;
this.claimedAgriIncome = claimedAgriIncome;
this.approvedAgriIncome = approvedAgriIncome;
this.existingEMIClaimed = existingEMIClaimed;
this.perfiosEMI = perfiosEMI;
this.cIBILEMI = cIBILEMI;
this.kVBEMI = kVBEMI;
this.cIBILEMIOverride = cIBILEMIOverride;
this.computedEMI = computedEMI;
this.existingEMIApproved = existingEMIApproved;
this.educationQualification = educationQualification;
this.totalChequeBounce = totalChequeBounce;
this.primaryBank = primaryBank;
this.primaryBranch = primaryBranch;
this.primaryAccNo = primaryAccNo;
this.primaryIFSC = primaryIFSC;
this.grossIncome1 = grossIncome1;
this.annualCapGain1 = annualCapGain1;
this.interestIncome1 = interestIncome1;
this.annualTax1 = annualTax1;
this.dividendIncome1 = dividendIncome1;
this.shareOfProfit1 = shareOfProfit1;
this.agricultureITR1 = agricultureITR1;
this.grossIncome2 = grossIncome2;
this.annualCapGain2 = annualCapGain2;
this.interestIncome2 = interestIncome2;
this.annualTax2 = annualTax2;
this.dividendIncome2 = dividendIncome2;
this.shareOfProfit2 = shareOfProfit2;
this.agricultureITR2 = agricultureITR2;
this.monthlyIncome = monthlyIncome;
this.salaryPaid = salaryPaid;
this.taxDeducted = taxDeducted;
this.otherReccuringIncome = otherReccuringIncome;
this.estimatedBusinessIncome = estimatedBusinessIncome;
this.averageCASABal = averageCASABal;
this.averageCASABalance = averageCASABalance;
this.reasonForExemption = reasonForExemption;
this.fIExempted = fIExempted;
}

public String getFirstName() {
return firstName;
}

public void setFirstName(String firstName) {
this.firstName = firstName;
}

public String getMiddleName() {
return middleName;
}

public void setMiddleName(String middleName) {
this.middleName = middleName;
}

public String getLastName() {
return lastName;
}

public void setLastName(String lastName) {
this.lastName = lastName;
}

public String getFullName() {
return fullName;
}

public void setFullName(String fullName) {
this.fullName = fullName;
}

public String getPAN() {
return pAN;
}

public void setPAN(String pAN) {
this.pAN = pAN;
}

public String getAadharNumber() {
return aadharNumber;
}

public void setAadharNumber(String aadharNumber) {
this.aadharNumber = aadharNumber;
}

public String getCBSCustId() {
return cBSCustId;
}

public void setCBSCustId(String cBSCustId) {
this.cBSCustId = cBSCustId;
}

public String getTitle() {
return title;
}

public void setTitle(String title) {
this.title = title;
}

public String getNationality() {
return nationality;
}

public void setNationality(String nationality) {
this.nationality = nationality;
}

public String getGender() {
return gender;
}

public void setGender(String gender) {
this.gender = gender;
}

public String getCaste() {
return caste;
}

public void setCaste(String caste) {
this.caste = caste;
}

public String getBirthDate() {
return birthDate;
}

public void setBirthDate(String birthDate) {
this.birthDate = birthDate;
}

public String getMaritalstatus() {
return maritalstatus;
}

public void setMaritalstatus(String maritalstatus) {
this.maritalstatus = maritalstatus;
}

public String getFatherName() {
return fatherName;
}

public void setFatherName(String fatherName) {
this.fatherName = fatherName;
}

public String getMotherName() {
return motherName;
}

public void setMotherName(String motherName) {
this.motherName = motherName;
}

public String getHusbandName() {
return husbandName;
}

public void setHusbandName(String husbandName) {
this.husbandName = husbandName;
}

public String getBankingWithUs() {
return bankingWithUs;
}

public void setBankingWithUs(String bankingWithUs) {
this.bankingWithUs = bankingWithUs;
}

public String getReligion() {
return religion;
}

public void setReligion(String religion) {
this.religion = religion;
}

public String getExServiceMan() {
return exServiceMan;
}

public void setExServiceMan(String exServiceMan) {
this.exServiceMan = exServiceMan;
}

public String getPhysicallyHandicapped() {
return physicallyHandicapped;
}

public void setPhysicallyHandicapped(String physicallyHandicapped) {
this.physicallyHandicapped = physicallyHandicapped;
}

public String getVoterId() {
return voterId;
}

public void setVoterId(String voterId) {
this.voterId = voterId;
}

public String getDrivingLicId() {
return drivingLicId;
}

public void setDrivingLicId(String drivingLicId) {
this.drivingLicId = drivingLicId;
}

public String getRationCard() {
return rationCard;
}

public void setRationCard(String rationCard) {
this.rationCard = rationCard;
}

public String getPassportNo() {
return passportNo;
}

public void setPassportNo(String passportNo) {
this.passportNo = passportNo;
}

public String getPassportExpDate() {
return passportExpDate;
}

public void setPassportExpDate(String passportExpDate) {
this.passportExpDate = passportExpDate;
}

public String getOtherId() {
return otherId;
}

public void setOtherId(String otherId) {
this.otherId = otherId;
}

public String getOtherIdNo() {
return otherIdNo;
}

public void setOtherIdNo(String otherIdNo) {
this.otherIdNo = otherIdNo;
}

public String getCurrentAddress() {
return currentAddress;
}

public void setCurrentAddress(String currentAddress) {
this.currentAddress = currentAddress;
}

public String getCurrentCity() {
return currentCity;
}

public void setCurrentCity(String currentCity) {
this.currentCity = currentCity;
}

public String getCurrentState() {
return currentState;
}

public void setCurrentState(String currentState) {
this.currentState = currentState;
}

public String getCurrentCountry() {
return currentCountry;
}

public void setCurrentCountry(String currentCountry) {
this.currentCountry = currentCountry;
}

public String getCurrentZip() {
return currentZip;
}

public void setCurrentZip(String currentZip) {
this.currentZip = currentZip;
}

public String getPermanentAddress() {
return permanentAddress;
}

public void setPermanentAddress(String permanentAddress) {
this.permanentAddress = permanentAddress;
}

public String getPermanentCity() {
return permanentCity;
}

public void setPermanentCity(String permanentCity) {
this.permanentCity = permanentCity;
}

public String getPermanentState() {
return permanentState;
}

public void setPermanentState(String permanentState) {
this.permanentState = permanentState;
}

public String getPermanentCountry() {
return permanentCountry;
}

public void setPermanentCountry(String permanentCountry) {
this.permanentCountry = permanentCountry;
}

public String getPermanentZip() {
return permanentZip;
}

public void setPermanentZip(String permanentZip) {
this.permanentZip = permanentZip;
}

public String getPhoneNo() {
return phoneNo;
}

public void setPhoneNo(String phoneNo) {
this.phoneNo = phoneNo;
}

public String getMobile() {
return mobile;
}

public void setMobile(String mobile) {
this.mobile = mobile;
}

public String getEmail() {
return email;
}

public void setEmail(String email) {
this.email = email;
}

public String getFax() {
return fax;
}

public void setFax(String fax) {
this.fax = fax;
}

public String getCibilScore() {
return cibilScore;
}

public void setCibilScore(String cibilScore) {
this.cibilScore = cibilScore;
}

public String getEmploymentType() {
return employmentType;
}

public void setEmploymentType(String employmentType) {
this.employmentType = employmentType;
}

public String getCompanyName() {
return companyName;
}

public void setCompanyName(String companyName) {
this.companyName = companyName;
}

public String getGroupCompanyAddress() {
return groupCompanyAddress;
}

public void setGroupCompanyAddress(String groupCompanyAddress) {
this.groupCompanyAddress = groupCompanyAddress;
}

public String getCompanyPIN() {
return companyPIN;
}

public void setCompanyPIN(String companyPIN) {
this.companyPIN = companyPIN;
}

public String getCompanyFax() {
return companyFax;
}

public void setCompanyFax(String companyFax) {
this.companyFax = companyFax;
}

public String getCompanyEmail() {
return companyEmail;
}

public void setCompanyEmail(String companyEmail) {
this.companyEmail = companyEmail;
}

public String getCompanyPhone() {
return companyPhone;
}

public void setCompanyPhone(String companyPhone) {
this.companyPhone = companyPhone;
}

public String getCustomerType() {
return customerType;
}

public void setCustomerType(String customerType) {
this.customerType = customerType;
}

public String getFinancialApplicant() {
return financialApplicant;
}

public void setFinancialApplicant(String financialApplicant) {
this.financialApplicant = financialApplicant;
}

public String getAltMobile() {
return altMobile;
}

public void setAltMobile(String altMobile) {
this.altMobile = altMobile;
}

public String getLandline() {
return landline;
}

public void setLandline(String landline) {
this.landline = landline;
}

public String getExperience() {
return experience;
}

public void setExperience(String experience) {
this.experience = experience;
}

public String getClaimedNMIITR() {
return claimedNMIITR;
}

public void setClaimedNMIITR(String claimedNMIITR) {
this.claimedNMIITR = claimedNMIITR;
}

public String getClaimedAgriIncome() {
return claimedAgriIncome;
}

public void setClaimedAgriIncome(String claimedAgriIncome) {
this.claimedAgriIncome = claimedAgriIncome;
}

public String getApprovedAgriIncome() {
return approvedAgriIncome;
}

public void setApprovedAgriIncome(String approvedAgriIncome) {
this.approvedAgriIncome = approvedAgriIncome;
}

public String getExistingEMIClaimed() {
return existingEMIClaimed;
}

public void setExistingEMIClaimed(String existingEMIClaimed) {
this.existingEMIClaimed = existingEMIClaimed;
}

public String getPerfiosEMI() {
return perfiosEMI;
}

public void setPerfiosEMI(String perfiosEMI) {
this.perfiosEMI = perfiosEMI;
}

public String getCIBILEMI() {
return cIBILEMI;
}

public void setCIBILEMI(String cIBILEMI) {
this.cIBILEMI = cIBILEMI;
}

public String getKVBEMI() {
return kVBEMI;
}

public void setKVBEMI(String kVBEMI) {
this.kVBEMI = kVBEMI;
}

public String getCIBILEMIOverride() {
return cIBILEMIOverride;
}

public void setCIBILEMIOverride(String cIBILEMIOverride) {
this.cIBILEMIOverride = cIBILEMIOverride;
}

public String getComputedEMI() {
return computedEMI;
}

public void setComputedEMI(String computedEMI) {
this.computedEMI = computedEMI;
}

public String getExistingEMIApproved() {
return existingEMIApproved;
}

public void setExistingEMIApproved(String existingEMIApproved) {
this.existingEMIApproved = existingEMIApproved;
}

public String getEducationQualification() {
return educationQualification;
}

public void setEducationQualification(String educationQualification) {
this.educationQualification = educationQualification;
}

public String getTotalChequeBounce() {
return totalChequeBounce;
}

public void setTotalChequeBounce(String totalChequeBounce) {
this.totalChequeBounce = totalChequeBounce;
}

public String getPrimaryBank() {
return primaryBank;
}

public void setPrimaryBank(String primaryBank) {
this.primaryBank = primaryBank;
}

public String getPrimaryBranch() {
return primaryBranch;
}

public void setPrimaryBranch(String primaryBranch) {
this.primaryBranch = primaryBranch;
}

public String getPrimaryAccNo() {
return primaryAccNo;
}

public void setPrimaryAccNo(String primaryAccNo) {
this.primaryAccNo = primaryAccNo;
}

public String getPrimaryIFSC() {
return primaryIFSC;
}

public void setPrimaryIFSC(String primaryIFSC) {
this.primaryIFSC = primaryIFSC;
}

public String getGrossIncome1() {
return grossIncome1;
}

public void setGrossIncome1(String grossIncome1) {
this.grossIncome1 = grossIncome1;
}

public String getAnnualCapGain1() {
return annualCapGain1;
}

public void setAnnualCapGain1(String annualCapGain1) {
this.annualCapGain1 = annualCapGain1;
}

public String getInterestIncome1() {
return interestIncome1;
}

public void setInterestIncome1(String interestIncome1) {
this.interestIncome1 = interestIncome1;
}

public String getAnnualTax1() {
return annualTax1;
}

public void setAnnualTax1(String annualTax1) {
this.annualTax1 = annualTax1;
}

public String getDividendIncome1() {
return dividendIncome1;
}

public void setDividendIncome1(String dividendIncome1) {
this.dividendIncome1 = dividendIncome1;
}

public String getShareOfProfit1() {
return shareOfProfit1;
}

public void setShareOfProfit1(String shareOfProfit1) {
this.shareOfProfit1 = shareOfProfit1;
}

public String getAgricultureITR1() {
return agricultureITR1;
}

public void setAgricultureITR1(String agricultureITR1) {
this.agricultureITR1 = agricultureITR1;
}

public String getGrossIncome2() {
return grossIncome2;
}

public void setGrossIncome2(String grossIncome2) {
this.grossIncome2 = grossIncome2;
}

public String getAnnualCapGain2() {
return annualCapGain2;
}

public void setAnnualCapGain2(String annualCapGain2) {
this.annualCapGain2 = annualCapGain2;
}

public String getInterestIncome2() {
return interestIncome2;
}

public void setInterestIncome2(String interestIncome2) {
this.interestIncome2 = interestIncome2;
}

public String getAnnualTax2() {
return annualTax2;
}

public void setAnnualTax2(String annualTax2) {
this.annualTax2 = annualTax2;
}

public String getDividendIncome2() {
return dividendIncome2;
}

public void setDividendIncome2(String dividendIncome2) {
this.dividendIncome2 = dividendIncome2;
}

public String getShareOfProfit2() {
return shareOfProfit2;
}

public void setShareOfProfit2(String shareOfProfit2) {
this.shareOfProfit2 = shareOfProfit2;
}

public String getAgricultureITR2() {
return agricultureITR2;
}

public void setAgricultureITR2(String agricultureITR2) {
this.agricultureITR2 = agricultureITR2;
}

public String getMonthlyIncome() {
return monthlyIncome;
}

public void setMonthlyIncome(String monthlyIncome) {
this.monthlyIncome = monthlyIncome;
}

public String getSalaryPaid() {
return salaryPaid;
}

public void setSalaryPaid(String salaryPaid) {
this.salaryPaid = salaryPaid;
}

public String getTaxDeducted() {
return taxDeducted;
}

public void setTaxDeducted(String taxDeducted) {
this.taxDeducted = taxDeducted;
}

public String getOtherReccuringIncome() {
return otherReccuringIncome;
}

public void setOtherReccuringIncome(String otherReccuringIncome) {
this.otherReccuringIncome = otherReccuringIncome;
}

public String getEstimatedBusinessIncome() {
return estimatedBusinessIncome;
}

public void setEstimatedBusinessIncome(String estimatedBusinessIncome) {
this.estimatedBusinessIncome = estimatedBusinessIncome;
}

public String getAverageCASABal() {
return averageCASABal;
}

public void setAverageCASABal(String averageCASABal) {
this.averageCASABal = averageCASABal;
}

public String getAverageCASABalance() {
return averageCASABalance;
}

public void setAverageCASABalance(String averageCASABalance) {
this.averageCASABalance = averageCASABalance;
}

public String getReasonForExemption() {
return reasonForExemption;
}

public void setReasonForExemption(String reasonForExemption) {
this.reasonForExemption = reasonForExemption;
}

public String getFIExempted() {
return fIExempted;
}

public void setFIExempted(String fIExempted) {
this.fIExempted = fIExempted;
}

}