package com.orf.coh.KVB;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.scheduling.annotation.Scheduled;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vg.lms.model.Customer;
import com.vg.lms.model.Loan;
import com.vg.lms.repository.IncredRepository;
import com.vg.lms.repository.LoanRepository;


@Component
public class KVBServiceImpl {
	

	@Autowired
	private IncredRepository incredRepository;

	@Autowired
	private LoanRepository loanRepository;
	

	private final Logger log = LoggerFactory.getLogger(KVBServiceImpl.class);	

	//@Scheduled(fixedDelay=6000)
	public void getKVBCompleteTxns(Loan loan) throws JsonProcessingException{
		long startTimeInMillis=System.currentTimeMillis();
		Date syncTime = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentTime = sdf.format(syncTime);		
	try {
		Customer customer = loan.getCustomer();

		InitKVBRequest initRequest = new InitKVBRequest();
		
		initRequest.setSourcedByM("ORFL");
		initRequest.setSourcedByOffice("");
		initRequest.setInterestRateM(Float.toString(loan.getEmi()));
		initRequest.setLoanAmountM(loan.getDealerDisbursementAmount());
		initRequest.setSanctionDateM(sdf.format(loan.getAgmntDate()));
		initRequest.setEMISanctionedM(2300);
		initRequest.setSanctionedTerm(Float.toString(loan.getTenure()));
		initRequest.setHolidayPeriodM("5");
		initRequest.setTotalRepayAmount(loan.getDealerDisbursementAmount());
		initRequest.setReqLoanAmount(loan.getDealerDisbursementAmount());
		initRequest.setReqTerm(loan.getDealerDisbursementAmount());
		initRequest.setReqEMI("");
		initRequest.setChargePaymentComplete("");
		initRequest.setDateOfDisbursement(sdf.format(loan.getDisbursementTime()));
		initRequest.setDisbursementAmount(loan.getDealerDisbursementAmount());
		initRequest.setDateOfAdvancePayment("");
		initRequest.setAdvancePaid("");
		initRequest.setLoanPurposeM("Purchase of a vehicle for self use");
		initRequest.setFIVerDate("");
		initRequest.setFISuccess("");
		initRequest.setDisbAccBank("KVB");
		initRequest.setDisbAccBranch("");
		initRequest.setDisbAccIFSC("");
		initRequest.setDisbAccNo("");
		initRequest.setSIAccNo("");
		initRequest.setSIBank("");
		initRequest.setSIBranch("");
		initRequest.setSIIFSC("");
		initRequest.setSIApplicant("");
		initRequest.setExcessMCLR ("");
		initRequest.setMCLRType("");
		initRequest.setPreClosureRate("");
		initRequest.setMCLRRate("");
		initRequest.setCASAAccNov("");
		initRequest.setMODDate("");
		initRequest.setCKYCM("");
		initRequest.setProcessingFeeWaiver("");
		initRequest.setBankYear("");
		initRequest.setReasonforwaiver("");
		initRequest.setAcknowledgement("");
		initRequest.setTotalCharges("");
		initRequest.setTotalGSTCharges("");
		initRequest.setScourceTypeM("Orange Retail Finance");
		initRequest.setTypeOfLoanM("VL2W");
		initRequest.setThirdpartyloanaccountno("");
		initRequest.setKVBAmountSanction("");
		initRequest.setApplicantDetails("");
		initRequest.setFirstNameM("PAUL");
		initRequest.setMiddleName("");
		initRequest.setLastNameM("RAJ");
		initRequest.setFullName("");
		initRequest.setPANM("AJNPV6165H");
		initRequest.setAadharNumberM("123456789111");
		initRequest.setCBSCustId("");
		initRequest.setTitleM("Mr");
		initRequest.setNationalityM("Indian");
		initRequest.setGenderM("M");
		initRequest.setCaste("");
		initRequest.setBirthDateM("1984-01-03");
		initRequest.setMaritalstatusM("Married");
		initRequest.setFatherName("");
		initRequest.setHusbandName("");
		initRequest.setBankingWithUs("");
		initRequest.setReligion("");
		initRequest.setExServiceMan("");
		initRequest.setPhysicallyHandicapped("");
		initRequest.setVoterId("");
		initRequest.setDrivingLicId("");
		initRequest.setRationCard("");
		initRequest.setPassportNo("");
		initRequest.setPassportExpDate("");
		initRequest.setOtherId("");
		initRequest.setOtherIdNo("");
		initRequest.setCurrentAddress("");
		initRequest.setCurrentCity("");
		initRequest.setCurrentState("");
		initRequest.setCurrentCountry("");
		initRequest.setCurrentZip("");
		initRequest.setPermanentAddress("");
		initRequest.setPermanentCity("");
		initRequest.setPermanentState("");
		initRequest.setPermanentCountry("");
		initRequest.setPermanentZip("");
		initRequest.setPhoneNo("");
		initRequest.setMobileM("7397375399");
		initRequest.setEmailM("info@orangeretialfinance.com");
		initRequest.setFax("");
		initRequest.setRelatedToDirector("");
		initRequest.setDirectorBank("");
		initRequest.setDirectorName("");
		initRequest.setCibilScore("");
		initRequest.setEmploymentTypeM("Salaried");
		initRequest.setCompanyName("");
		initRequest.setGroupCompanyAddress("");
		initRequest.setCompanyPIN("");
		initRequest.setCompanyFax("");
		initRequest.setCompanyEmail("");
		initRequest.setCompanyPhone("");
		initRequest.setCustomerTypeM("Primary Applicant");
		initRequest.setFinancialApplicantM("TRUE");
		initRequest.setAltMobile("");
		initRequest.setLandline("");
		initRequest.setExperience("");
		initRequest.setClaimedNMIITR("");
		initRequest.setClaimedAgriIncome("");
		initRequest.setApprovedAgriIncome("");
		initRequest.setExistingEMIClaimed("");
		initRequest.setPerfiosEMI("");
		initRequest.setCIBILEMI("");
		initRequest.setKVBEMI("");
		initRequest.setCIBILEMIOverride("");
		initRequest.setComputedEMI("");
		initRequest.setExistingEMIApproved("");
		initRequest.setTotalChequeBounce("");
		initRequest.setPrimaryBank("");
		initRequest.setPrimaryBranch("");
		initRequest.setPrimaryAccNo("");
		initRequest.setPrimaryIFSC("");
		initRequest.setGrossIncome1("");
		initRequest.setAnnualCapGain1("");
		initRequest.setInterestIncome1("");
		initRequest.setAnnualTax1("");
		initRequest.setDividendIncome1("");
		initRequest.setShareOfProfit1("");
		initRequest.setAgricultureITR1("");
		initRequest.setGrossIncome2("");
		initRequest.setAnnualCapGain2("");
		initRequest.setInterestIncome2("");
		initRequest.setAnnualTax2("");
		initRequest.setDividendIncome2("");
		initRequest.setShareOfProfit2("");
		initRequest.setAgricultureITR2("");
		initRequest.setSalaryPaid("");
		initRequest.setTaxDeducted("");
		initRequest.setOtherReccuringIncome("");
		initRequest.setEstimatedBusinessIncome("");
		initRequest.setAverageCASABalance("");
		initRequest.setReasonforExemption("");
		initRequest.setFIExempted(""); 




		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = mapper.writeValueAsString(initRequest);
		log.info("KVB init Request : " + jsonInString);

		HttpHeaders headers = new HttpHeaders();
		headers.add("api-key", "Nn1pFaiBXHGi0xQHgd9ASHGEccRV8M6F");
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		HttpEntity<String> entity = new HttpEntity<String>(jsonInString, headers);	  
		RestTemplate template = new RestTemplate();
		ResponseEntity<InitKVBResponse> responseEntity =
				template.postForEntity("https://kvb-test.apigee.net/nbfc/v1/primaryApplication/userData", entity, InitKVBResponse.class);
	
	log.info("KVB init Response : " + mapper.writeValueAsString(responseEntity));
	

	} catch (Exception e) {
		log.error("Error Occured in Incred Push :",e);
	}
	
}
	


}
