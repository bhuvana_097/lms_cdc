<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<title>ORFLEND USER</title>
</head>
<body  style="background-color:powderblue;">
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!--  <set var="title" value="List"/> -->
<script>

	var Msg ='<%=request.getParameter("auth_message")%>';
	if(Msg!=null && Msg!='' && Msg!='null'){
		alert(Msg);
		window.location.href = "/OLMS/list";
	}
	function toggleSearchView() {
		var x = document.getElementById("searchdiv");
		if (x.style.display === "none") {
			x.style.display = "block";
		} else {
			x.style.display = "none";
		}
	}
</script>

<center>
	<h1>Welcome to ORFLEND</h1>
	<%@include file="menu.jsp"%>
	<br></br>
	<h3><a href="addlmsuser"><u>Add User /<a href="searchlmsuser"><u>Search </u></a></u></a></h3>
	
	<br></br>
		<tr>
				<td valign="top">
					<table class="Header" border="0" cellspacing="0" cellpadding="0">
						
	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
	<c:set var="my_url" value="lmsuserlist" />
	<c:set var="page_size" value="20" />
	<c:set var="pageno" value="0" />
	<c:if test="${not empty param.pageno}">
		<c:set var="pageno" value="${param.pageno}" />
	</c:if>
	<c:set var="limit_begin" value="${ pageno * page_size }" />
	<sql:query dataSource="${acessData}" var="tableUserData">
		select * from lms_user_master limit ${limit_begin},${page_size};
	</sql:query>
	<c:set var="download_query" value="select * from lms_user_master limit ${limit_begin},${page_size}" scope="application"/>
	<table class="MainTable" cellspacing="0" cellpadding="0" align="center" border="0">
		<tr>
			<td valign="top">
				<table class="Header" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td class="HeaderLeft"><img alt=""
							src="styles/cloudstyle1/Images/Spacer.gif"></td>
						<td class="th"><strong>User List</strong> &nbsp; &nbsp; &nbsp;
							&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
							&nbsp; &nbsp; &nbsp; &nbsp; <small><a href="report?title=${title}" target="_blank" ><u>Download
										as XLSX</u></a></small></td>
						<td class="HeaderRight"><img alt=""
							src="styles/cloudstyle1/Images/Spacer.gif"></td>
					</tr>
				</table>
				<table class="Grid" cellspacing="0" cellpadding="0">
					<tr class="Caption">
						<th scope="col"><span="Sorter">Edit</span></th>
						<th scope="col"><span class="Sorter">ID</span></th>
						<th scope="col"><span class="Sorter">Code</span></th>
						<th scope="col"><span class="Sorter">Name</span></th>
						<!-- <th scope="col"><span class="Sorter">Password</span></th> -->
						<th scope="col"><span class="Sorter">Department</span></th>
						<th scope="col"><span class="Sorter">Hierarchy Level</span></th>
						<th scope="col"><span class="Sorter">Status</span></th>
						<th scope="col"><span class="Sorter">Reason</span></th>
						<th scope="col"><span class="Sorter">Create User</span></th>
						<th scope="col"><span class="Sorter">Update User</span></th>
						<th scope="col"><span class="Sorter">Update Time</span></th>
					</tr>
					<c:set var="rowclass" value="Row" />
					<c:set var="no_of_cols" value="0" />
					<c:forEach items="${tableUserData.rows}" var="columnData" varStatus="loop">
						<tr class="${rowclass}">
							<td align="center">&nbsp;<a href="lmsuseredit?id=${columnData.id}"><img alt="" height="16" width="16" src="styles/cloudstyle1/Images/Edit.png"></a></td>
							<td>${columnData.id}</td>
							<td>${columnData.code}</td>
							<td>${columnData.name}</td>
							<%-- <td>${columnData.passwd}</td> --%>
							<td>${columnData.dept}</td>
							<td>${columnData.hier_level}</td>
							<td>${columnData.status}</td>
							<td>${columnData.reason}</td>
							<td>${columnData.create_user}</td>
							<td>${columnData.modify_user}</td>
							<td>${columnData.modify_time}</td>
						</tr>
						<c:set var="rowclass" value="${rowclass eq 'Row' ? 'AltRow' : 'Row'}" />
						<c:set var="no_of_cols" value="${loop.count}" />
					</c:forEach>
					<c:set var="result_row_count" value="${tableUserData.rowCount}" />
					<tr class="Footer">
					<c:set var="footer_cols" value="${ no_of_cols + 1 }" />
					<td colspan="${footer_cols}"><c:set var="from_row"
							value="${ limit_begin + 1 }" /> <c:set var="to_row"
							value="${ from_row + result_row_count - 1 }" /> <sql:query
							dataSource="${acessData}" var="recordCount">
							select count(*) as target from lms_user_master where 1=1;
							</sql:query> <c:set var="tot_count" value="${recordCount.rowsByIndex[0][0]}" />

						<c:choose>
							<c:when test="${pageno > 0}">
								<c:set var="prev_pageno" value="${ pageno - 1 }" />
								<a
									href="${my_url}?pageno=0">
									<img alt="First" src="styles/cloudstyle1/Images/First.gif">
								</a>
								<a
									href="${my_url}?pageno=${prev_pageno}">
									<img alt="Prev" src="styles/cloudstyle1/Images/Prev.gif">
								</a>
							</c:when>
							<c:otherwise>
								<img alt="" src="styles/cloudstyle1/Images/FirstOff.gif">
								<img alt="" src="styles/cloudstyle1/Images/PrevOff.gif">
							</c:otherwise>
						</c:choose> <b> ${from_row} </b> to <b> ${to_row} </b> of <b>
							${tot_count} </b> Rows <c:choose>
							<c:when test="${to_row < tot_count}">
								<c:set var="next_pageno" value="${ pageno + 1 }" />
								<c:set var="last_pageno_float"
									value="${(tot_count - 1)/page_size }" />

								<fmt:parseNumber var="last_pageno" integerOnly="true"
									type="number" value="${last_pageno_float}" />
								<a
									href="${my_url}?pageno=${next_pageno}">
									<img alt="Prev" src="styles/cloudstyle1/Images/Next.gif">
								</a>
								<a
									href="${my_url}?pageno=${last_pageno}">
									<img alt="Prev" src="styles/cloudstyle1/Images/Last.gif">
								</a>
							</c:when>
							<c:otherwise>
								<img alt="" src="styles/cloudstyle1/Images/NextOff.gif">
								<img alt="" src="styles/cloudstyle1/Images/LastOff.gif">
							</c:otherwise>
						</c:choose></td>
				</tr>
				</table>
			</td>
		</tr>
	</table>
	
</center>
</body>


