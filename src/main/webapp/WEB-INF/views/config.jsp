<head>
<title>ORFLEND CONFIG</title>
</head>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--  <set var="title" value="List"/> -->
<center>
	<h1>Update Registry</h1>
</center>
<%@include file="menu.jsp"%>
<form:form method="POST" action="updateRegistry" modelAttribute="registry">
<table class="MainTable" border="0" cellspacing="0" cellpadding="0"
	align="center">
	<tr>
		<td valign="top">
			<table class="Header" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="HeaderLeft"><img alt=""
						src="styles/cloudstyle1/Images/Spacer.gif"></td>
					<td class="th"><strong> Registry </strong></td>
					<td class="HeaderRight"><img alt=""
						src="styles/cloudstyle1/Images/Spacer.gif"></td>
				</tr>

			</table>
			<div id="RegistryDetails">
				<table class=Record cellspacing="0" cellpadding="0">
					
					<tr class="Row">
						<td><strong>Id</strong></td>
						<td><form:input path="id" value="${lmsRegistryBean.id}" />
					</tr>
					
					<tr class="Row">
						<td><strong>Config Key</strong></td>
						<td><form:input path="config_key" value="${lmsRegistryBean.config_key}"  />
					</tr>
					
					<tr class="Row">
						<td><strong>Config Value</strong></td>
						<td><form:input path="config_value" value="${lmsRegistryBean.config_value}" /></td>
					</tr>
					
					<tr class="Bottom">
						<td colspan=2><input type='submit' value='Update' /></td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
</table>
</form:form>


<jsp:include page="tail.jsp" />