<head>
<title> ORFLEND LOAN</title>
</head>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--  <set var="title" value="List"/> -->
<center>
	<h1>Search User</h1>
</center>
<%@include file="menu.jsp"%>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<script type="text/javascript">
	var Msg ='<%=request.getParameter("auth_message")%>';
	if(Msg!=null && Msg!='' && Msg!='null'){
		alert(Msg);
		window.location.href = "/OLMS/list";
	}
	
</script>
	
<br/><br/>
<br/><br/>
<!-- <h1>Search User</h1> -->
<!-- <img width="300" height="55"  src="imgs/bluemoon_logo.png" /> -->

<br/><br/>
<br/><br/>
<sql:query dataSource="${acessData}" var="userBranch">
	select branch_name from  hht_branchmaster;
</sql:query>
<sql:query dataSource="${acessData}" var="userDept">
	select dept_name from lms_user_dept_master where active=true;
</sql:query>

<form method="POST" action="lmsusersearchupdate" class="form-signin">
<table class="MainTable" border="0" cellspacing="0" cellpadding="0" align="center">
  	<div id="searchdiv"> 		 
        	<table class="Record" cellspacing="0" cellpadding="0">
	<tr class="Caption">
     <th scope="col"> Edit </th>
     	<th scope="col"> Code </th>	
     	<th scope="col"> Name </th>
     	<th scope="col"> Hierarchy level </th>
		<th scope="col"> Status </th>
		<th scope="col"> Department </th>
		<th scope="col"> Branch </th>
		<th scope="col"> Reason </th>
		</tr>
			<tr class="Controls">		
		        <td align="center">&nbsp;<a href="lmsuseredit?id=${searchlmsuserresult.id}"><img alt="" height="16" width="16" src="styles/cloudstyle1/Images/Edit.png"></a></td>	 
					<td>${searchlmsuserresult.code}</td>
					<td>${searchlmsuserresult.name}</td>
		     		<td>${searchlmsuserresult.hier_level}</td>		
					<td>${searchlmsuserresult.status}</td>
			    	<td>${searchlmsuserresult.department}</td>		
		        	<td>${searchlmsuserresult.branch}</td>		
			    	<td>${searchlmsuserresult.reason}</td>		
						
					
<!-- 				</tr>		 -->
<!-- 	<tr class="Bottom"> -->
<!--         			<td style="TEXT-ALIGN: right" colspan="2"> -->
<!--         			<button class="Button" type="submit"> Save</button></td> -->
<!--         		</tr>							 -->
					
				</tr>
				</table></div>
				</table>

</form>
<jsp:include page="tail.jsp" />
