<head>
<title>ORFLEND LMS</title>
</head>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--  <set var="title" value="List"/> -->
<%@include file="head.jsp"%>


<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ page import="org.springframework.security.core.Authentication" %>
<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<%@ page import="org.springframework.security.core.GrantedAuthority" %>
<%@ page import="com.vg.lms.UserModuleMasterDetails" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>

<%@ page import="com.vg.lms.model.UserModuleMaster" %>
<%@ page import="com.vg.lms.model.UserMaster" %>
<%@ page import="com.vg.lms.repository.UserMasterRepository" %>
<%@include file="commonutilities.jsp"%>


<div class="MenuStyle" id="HeaderMenu1Container">
	<ul class="adxm menu_htb level1">
		<sql:query dataSource="${acessData}" var="tableData">
          select * from lms_table_meta;
      </sql:query>
		<li><a href="" class="submenu" target="" title="">Masters</a>
			<ul class="level2">
		
				
				<c:forEach var="row" items="${tableData.rows}" varStatus="loop">
					<c:if test="${row.table_type == 'master'}">
						<li><a href="list?tn=${row.table_no}" class="">${row.disp_name}</a></li>
					</c:if>
				</c:forEach>
				
			<c:set var="addlmsuseraccess" value='<%= getAccess("AddLmsUser") %>' />		
<%-- 			<c:if test="${addlmsuseraccess == true}"> --%>
				<li><a href="lmsuserlist" class="">User List</a>
				<li><a href="searchlmsuser" class="">Search User</a>
				
				<!-- <ul class="level3">
					<li><a href="addlmsuser" class="">Add User</a></li>
				</ul> -->
				</li>
				<!-- <li><a href="addlmsuser" class="">Add User</a></li> -->
<%-- 				</c:if> --%>
			</ul></li>

		<li><a href="" class="submenu" target="" title="">Transactions</a>
			<ul class="level2">
				<c:forEach var="row" items="${tableData.rows}" varStatus="loop">
					<c:if test="${row.table_type == 'transaction'}">
						<li><a href="list?tn=${row.table_no}" class="">${row.disp_name}</a></li>
					</c:if>
				</c:forEach>
			</ul></li>

		<li><a href="" class="submenu" target="" title="">Reports</a>
			<ul class="level2">
			<!--  	<c:forEach var="row" items="${tableData.rows}" varStatus="loop">
					<c:if test="${row.table_type == 'report'}">
						<li><a href="list?tn=${row.table_no}" class="">${row.disp_name}</a></li>
					</c:if>
				</c:forEach>-->
			</ul></li>

		<li><a href="" class="submenu" target="" title="">Action</a>
		
			<ul class="level2">
			 	<li> <a href="userpasswordchange">Change Password</a></li>
				<!--  <li> <a onclick="document.forms['logoutForm'].submit()">Logout</a></li> -->
				<li> <a href="logout">Logout</a></li>

					</ul></li>

	
	 <form id="logoutForm" method="POST" action="logout"> </form>
	 
</div>
