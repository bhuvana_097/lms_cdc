<head>
<title>ADD USER</title>
</head>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<script type="text/javascript">
	var Msg ='<%=request.getParameter("auth_message")%>';
	if(Msg!=null && Msg!='' && Msg!='null'){
		alert(Msg);
		window.location.href = "/OLMS/list";
	}
</script>
<center>
	<h1>Welcome to ORFLEND</h1>
</center>
<%@include file="menu.jsp"%>
<center>
<br/><br/>
<br/><br/>
<h1>ADD USER</h1>
<!-- <img width="300" height="55"  src="imgs/bluemoon_logo.png" /> -->

<br/><br/>
<br/><br/>

</center>
<sql:query dataSource="${acessData}" var="userDept">
	select dept_name from lms_user_dept_master where active=true;
</sql:query>
 <form method="POST" action="insertlmsuser" class="form-signin">

<table class="MainTable" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td valign="top">
		<div id="searchdiv"> 		 
        	<table class="Record" cellspacing="0" cellpadding="0">
				<tr class="Controls">
					<td class="th"> Code </td>
					<td> <input name="lmsusercode" type="text" class="form-control" placeholder="Code" autofocus="true" tabindex="1" required/></td>
				</tr>
				<tr class="Controls">
					<td class="th"> Name </td>
					<td> <input name="lmsusername" type="text" class="form-control" placeholder="Name" tabindex="2" required/></td>
				</tr>
				<tr class="Controls">
					<td class="th"> Password </td>
					<td> <input name="lmsuserpwd" type="password" class="form-control" placeholder="Password" tabindex="3" required/></td>
				</tr>
				<tr class="Controls">
					<td class="th"> Department </td>
					<td> 
						<select name=dept  tabindex="4" required>
							<option value=''>Select</option>
							<c:forEach var="row" items="${userDept.rows}" varStatus="loop">
						    <option value='${row.dept_name}'>${row.dept_name}</option>
						    </c:forEach>
						</select>
					</td>
				</tr>
				<tr class="Controls">
					<td class="th"> Hierarchy level </td>
					<td> 
						<select name=hier_level  tabindex="5" required>
							<option value=''>Select</option>
						    <option value='1'>1</option>
						    <option value='2'>2</option>
						    <option value='3'>3</option>
						    <option value='4'>4</option>
						    <option value='5'>5</option>
						</select>
					</td>
				</tr>
				<tr class="Controls">
					<td class="th"> Branch/Cluster </td>
					<td> 
						<select name=branch  tabindex="5" required>
							<option value=''>Select</option>
		  
						
<option value='WARANGAL'>	WARANGAL	</option>
<option value='WARANAGAL'>	WARANAGAL	</option>
<option value='Wanaparthy'>	Wanaparthy	</option>
<option value='Vizianagaram'>	Vizianagaram	</option>
<option value='Visakhapatnam'>	Visakhapatnam	</option>
<option value='Vijayawada'>	Vijayawada	</option>
<option value='Vellore'>	Vellore	</option>
<option value='UDUPI'>	UDUPI	</option>
<option value='Tuticorin'>	Tuticorin	</option>
<option value='TUMKUR'>	TUMKUR	</option>
<option value='Trivandrum'>	Trivandrum	</option>
<option value='Trichy'>	Trichy	</option>
<option value='TIRUVARUR'>	TIRUVARUR	</option>
<option value='TIRUPPUR'>	TIRUPPUR	</option>
<option value='Tirupathi'>	Tirupathi	</option>
<option value='Thrissur'>	Thrissur	</option>
<option value='Thiruvannamalai'>	Thiruvannamalai	</option>
<option value='THIRUNELVELI'>	THIRUNELVELI	</option>
<option value='Theni'>	Theni	</option>
<option value='Tenkasi'>	Tenkasi	</option>
<option value='Tanjore'>	Tanjore	</option>
<option value='Srikakulam'>	Srikakulam	</option>
<option value='Sivakasi'>	Sivakasi	</option>
<option value='SHIMOGA'>	SHIMOGA	</option>
<option value='Salem'>	Salem	</option>
<option value='Ramanad'>	Ramanad	</option>
<option value='Rajahmundry'>	Rajahmundry	</option>
<option value='RAICHUR'>	RAICHUR	</option>
<option value='Pudukkottai'>	Pudukkottai	</option>
<option value='Puducherry(TN Geo)'>	Puducherry(TN Geo)	</option>
<option value='PUDUCHERRY'>	PUDUCHERRY	</option>
<option value='Pudhucherry(TN Geo)'>	Pudhucherry(TN Geo)	</option>
<option value='Pattukottai'>	Pattukottai	</option>
<option value='Palakad'>	Palakad	</option>
<option value='Ongole'>	Ongole	</option>
<option value='Omalur'>	Omalur	</option>
<option value='Nizamabad'>	Nizamabad	</option>
<option value='Nellore'>	Nellore	</option>
<option value='NARASARARAOPET'>	NARASARARAOPET	</option>
<option value='NANDYAL'>	NANDYAL	</option>
<option value='NAMAKKAL'>	NAMAKKAL	</option>
<option value='NALGONDA'>	NALGONDA	</option>
<option value='Nagercoil'>	Nagercoil	</option>
<option value='MYSORE'>	MYSORE	</option>
<option value='MIRYALAGUDA'>	MIRYALAGUDA	</option>
<option value='MANGALORE'>	MANGALORE	</option>
<option value='MANDYA'>	MANDYA	</option>
<option value='Malappuram'>	Malappuram	</option>
<option value='MAHABUBNAGAR'>	MAHABUBNAGAR	</option>
<option value='Madurai'>	Madurai	</option>
<option value='Kurnool'>	Kurnool	</option>
<option value='Kumbakonam'>	Kumbakonam	</option>
<option value='Krishnagiri'>	Krishnagiri	</option>
<option value='Kollam'>	Kollam	</option>
<option value='KOLAR'>	KOLAR	</option>
<option value='Kochi'>	Kochi	</option>
<option value='KIDIRI'>	KIDIRI	</option>
<option value='KHAMMAM'>	KHAMMAM	</option>
<option value='Karur'>	Karur	</option>
<option value='KARIMNAGAR'>	KARIMNAGAR	</option>
<option value='Karaikudi'>	Karaikudi	</option>
<option value='Karaikal (TN Geo)'>	Karaikal (TN Geo)	</option>
<option value='KARAIKAL'>	KARAIKAL	</option>
<option value='Kannur'>	Kannur	</option>
<option value='Kanchipuram'>	Kanchipuram	</option>
<option value='KAKINADA'>	KAKINADA	</option>
<option value='Kadiri'>	Kadiri	</option>
<option value='Kadappa'>	Kadappa	</option>
<option value='HUBLI'>	HUBLI	</option>
<option value='HO'>	HO	</option>
<option value='HINDUPUR'>	HINDUPUR	</option>
<option value='HASSAN'>	HASSAN	</option>
<option value='Guntur'>	Guntur	</option>
<option value='Gudur'>	Gudur	</option>
<option value='Erode'>	Erode	</option>
<option value='Eluru'>	Eluru	</option>
<option value='Dindigul'>	Dindigul	</option>
<option value='Dharmapuri'>	Dharmapuri	</option>
<option value='DAVANAGERE'>	DAVANAGERE	</option>
<option value='Cuddalore'>	Cuddalore	</option>
<option value='Coimbatore'>	Coimbatore	</option>
<option value='chennai'>	chennai	</option>
<option value='CHANNAPATNA'>	CHANNAPATNA	</option>
<option value='Changanassery'>	Changanassery	</option>
<option value='Calicut'>	Calicut	</option>
<option value='BIDAR'>	BIDAR	</option>
<option value='BHIMAVARAM'>	BHIMAVARAM	</option>
<option value='BELLARY'>	BELLARY	</option>
<option value='Attur'>	Attur	</option>
<option value='Ananthapur3'>	Ananthapur3	</option>
<option value='Ananthapur2'>	Ananthapur2	</option>
<option value='Ananthapur1'>	Ananthapur1	</option>
<option value='Ananthapur'>	Ananthapur	</option>
<option value='Anakapalli'>	Anakapalli	</option>
<option value='Alappuzha'>	Alappuzha	</option><option value='97'>AP1</option>
<option value='AP2'>AP2</option>
<option value='KA'>KA</option>
<option value='KL'>KL</option>
<option value='PY'>PY</option>
<option value='TN1'>TN1</option>
<option value='TN2'>TN2</option>
<option value='TN3'>TN3</option>
<option value='TN4'>TN4</option>
<option value='TS'>TS</option>

</select>
					</td>
				</tr>

				
				<tr class="Controls">
					<td class="th"> Status </td>
					<td> 
						<select name=status  tabindex="14" required>
							<option value=''>Select</option>
						    <option value='Active' ${status=='Active' ? 'selected' : ''}>Active</option>
						    <option value='DeActive' ${status=='DeActive' ? 'selected' : ''}>DeActive</option>
						   </select>
					</td>
				</tr>
			
        		<tr class="Bottom">
        			<td style="TEXT-ALIGN: right" colspan="2">
        			<button class="Button" type="submit">ADD</button></td>
        		</tr>
      		</table>
    	</div>
      </td> 
    </tr>
  </table>
 </form>

<br/><br/>
<br/><br/>
<jsp:include page="tail.jsp" />
