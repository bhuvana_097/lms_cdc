<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--  <set var="title" value="List"/> -->
<center>
	<h1>Update Colender</h1>
</center>
<%@include file="menu.jsp"%>
<form:form method="POST" action="UpdateColender" modelAttribute="loanBean">
<table class="MainTable" border="0" cellspacing="0" cellpadding="0"
	align="center">
	<tr>
		<td valign="top">
			<table class="Header" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="HeaderLeft"><img alt=""
						src="styles/cloudstyle1/Images/Spacer.gif"></td>
					<td class="th"><strong> Update Colender </strong></td>
					<td class="HeaderRight"><img alt=""
						src="styles/cloudstyle1/Images/Spacer.gif"></td>
				</tr>

			</table>
			<div id="AssetDetails">
				<table class=Record cellspacing="0" cellpadding="0">
					<tr class="Row">
						<td><strong>Loan Id</strong></td>
						<form:hidden path="loanId" value="${loanid}" />
						<td>${loanid}</td>
					</tr>
					<tr class="Row">
						<td><strong>Loan Number</strong></td>
						<td><form:label path="loanNo">
                      ${assetBean.loanNo}</form:label></td>
<%-- 					<td>${assetBean.loanNo}</td> --%>
					</tr>
					<tr class="Row">
						<td><strong>Make</strong></td>
					<td>${assetBean.make}</td>
					</tr>
					<tr class="Row">
						<td><strong>Model</strong></td>
					<td>${assetBean.model}</td>
					</tr>
					<tr class="Row">
						<td><strong>Engine No</strong></td>
					<td><form:input path="engineNo" value="${assetBean.engineNo}"/></td>
					</tr>
					<tr class="Row">
						<td><strong>Chassis No</strong></td>
					<td><form:input path="chassisNo" value="${assetBean.chassisNo}"/></td>
					</tr>
					<tr class="Row">
						<td><strong>Registration No</strong></td>
					<td><form:input path="regNo" value="${assetBean.regNo}"/></td>
					</tr>
					<tr class="Row">
						<td><strong>RC No</strong></td>
					<td><form:input path="rcNo" value="${assetBean.rcNo}"/></td>
					</tr>
					<tr class="Row">
						<td><strong>Insurance No</strong></td>
					<td><form:input path="insNo" value="${assetBean.insNo}"/></td>
					</tr>
					<tr class="Row">
						<td><strong>Insurance Provider</strong></td>
					<td><form:input path="insProvider" value="${assetBean.insProvider}"/></td>
					</tr>
					<tr class="Row">
						<td><strong>Key</strong></td>
						<td><form:radiobutton path="key" value="Yes" />Yes 
						    <form:radiobutton path="key" value="No" />No</td>	
						    </tr>			
<%-- 					<td>${assetBean.key}</td> --%>
					<tr class="Row">
						<td><strong>RC</strong></td>
						<td><form:radiobutton path="rc" value="Yes" />Yes 
						    <form:radiobutton path="rc" value="No" />No</td>
<%-- 					<td>${assetBean.rc}</td> --%>
					</tr>
					<tr class="Bottom">
						<td colspan=2><input type='submit' value='Update PDD' /></td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
</table>
</form:form>


<jsp:include page="tail.jsp" />