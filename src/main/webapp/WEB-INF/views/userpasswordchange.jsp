<head>
<title>CHANGE PASSWORD</title>
</head>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<script type="text/javascript">
	var Msg ='<%=request.getParameter("auth_message")%>';
	if(Msg!=null && Msg!='' && Msg!='null'){
		alert(Msg);
		window.location.href = "/OLMS/list";
	}
	
	
	
</script>
<center>
	
</center>
<%@include file="menu.jsp"%>
<center>
<br/><br/>
<br/><br/>
<h1>CHANGE PASSWORD</h1>
<!-- <img width="300" height="55"  src="imgs/bluemoon_logo.png" /> -->

<br/><br/>
<br/><br/>

</center>

  
<sql:query dataSource="${acessData}" var="userDept">
	select dept_name from lms_user_dept_master where active=true;
</sql:query>
 <form method="POST" action="passwordchange" class="form-signin">

<table class="MainTable" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td valign="top">
		<div id="searchdiv"> 		 
        	<table class="Record" cellspacing="0" cellpadding="0">
				<tr class="Controls">
					<td class="th"> Code </td>
					<td> <input name="code" type="text" class="form-control" placeholder="Code" value=<security:authentication property="principal.username"/> autofocus="true" tabindex="1"  readonly="readonly"/></td>
				</tr>
				<tr class="Controls">
					<td class="th"> Old Password </td>
					<td> <input name="lmsuserpwd" type="password" class="form-control" placeholder="Password"  tabindex="3" required/></td>
				</tr>
				<tr class="Controls">
					<td class="th">New Password </td>
					<td> <input name="newuserpwd" type="password" class="form-control" placeholder="Password" tabindex="3" required/></td>
				</tr>
				<tr class="Controls"> 
					<td class="th"> Confirm Password </td>
					<td> <input name="cnfrmnewpwd" type="password" class="form-control" placeholder="Password" tabindex="3" required/></td>
				</tr>
						
        		<tr class="Bottom">
        			<td style="TEXT-ALIGN: center" colspan="2">
        			<button class="Button" type="submit">CHANGE</button></td>
        		</tr>
      		</table>
    	</div>
      </td> 
    </tr>
  </table>
 </form>

<br/><br/>
<br/><br/>
<script type="text/javascript"> window.onload = alertName; </script>
<jsp:include page="tail.jsp" />
