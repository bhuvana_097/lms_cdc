<head>
<title>ORFLEND LMS</title>
</head>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--  <set var="title" value="List"/> -->
<center>
	<h1>Welcome to ORFLEND</h1>
</center>
<%@include file="menu.jsp"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<%@ page import="org.springframework.security.core.Authentication" %>
<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<%@ page import="org.springframework.security.core.GrantedAuthority" %>
<%@ page import="com.vg.lms.UserModuleMasterDetails" %>

<%@ page import="com.vg.lms.service.UserDetailsServiceImpl" %>
<%@ page import="org.springframework.security.config.annotation.web.builders.WebSecurity" %>
<%@ page import="org.springframework.security.config.annotation.web.builders.HttpSecurity" %>

<%@page import="org.springframework.security.core.authority.SimpleGrantedAuthority"%>
<%@page import="org.springframework.security.core.userdetails.UserDetails"%>
<%@page import="org.springframework.security.core.userdetails.UserDetailsService"%>
<%@page import="org.springframework.security.core.userdetails.UsernameNotFoundException"%>
<%@page import="org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder"%>
<%@page import="org.springframework.security.crypto.factory.PasswordEncoderFactories"%>
<%@page import="org.springframework.security.crypto.password.PasswordEncoder"%>
<%@page import="org.springframework.stereotype.Service"%>
<%@page import="org.springframework.transaction.annotation.Transactional"%>
<%@page import="org.springframework.security.core.userdetails.User"%>
<%@page import="com.vg.lms.model.UserMaster" %>

<%@ page import="com.vg.lms.model.UserModuleMaster" %>
<%@ page import="com.vg.lms.model.UserMaster" %>
<%@ page import="com.vg.lms.repository.UserMasterRepository" %>



<c:set var="table_name" value="lms_customer" />
<c:set var="title" value="Customer"/>
<c:set var="table_number" value="48" />
<c:if test="${not empty param.tn}">
	<c:set var="table_number" value="${param.tn}" />
	<sql:query dataSource="${acessData}" var="tableName">
         select table_name,disp_name as target from lms_web_view where id = ${table_number} and is_primary = 1 limit 1;
      </sql:query>
	<c:set var="table_name" value="${tableName.rowsByIndex[0][0]}" />
	
	<sql:query dataSource="${acessData}" var="dispName">
         select disp_name as target from lms_table_meta where table_no = ${table_number};
      </sql:query>
	<c:set var="title" value="${dispName.rowsByIndex[0][0]}"/>
</c:if>


<sql:query dataSource="${acessData}" var="branchName">
         select branch from lms_user_master where code="<security:authentication property="principal.username"/>" limit 1;
      </sql:query>
      <c:set var="branch" value="${branchName.rowsByIndex[0][0]}"/>
      
      <sql:query dataSource="${acessData}" var="clusterName">
         select cluster from fe_branchmaster where branch_name="${branch}" limit 1;
      </sql:query>
      <c:set var="cluster" value="${clusterName.rowsByIndex[0][0]}"/>
<!-- <c:out value="${cluster }" /> -->






<c:set var="page_size" value="20" />

<c:set var="my_url" value="list?" />
<c:if test="${not empty param.tn}">
<c:set var="my_url" value="${my_url}tn=${param.tn}" />
</c:if>

<%-- <c:forEach items="${param}" var="par" varStatus="loop">
	<c:if test="${par.key != 'order' && par.key != 'order_dir' && par.key != 'pageno'}">
		<c:choose>
			<c:when test="${loop.last}">
				<c:set var="my_url" value="${my_url}${par.key}=${par.value}" />
			</c:when>
			<c:otherwise>
				<c:set var="my_url" value="${my_url}${par.key}=${par.value}&" />
			</c:otherwise>
		</c:choose>
		<c:if test="${fn:length(fn:trim(par.value)) > 0}">
			<c:set var="my_url" value="${my_url}${par.key}=${par.value}&" />
		</c:if>		
	</c:if>
</c:forEach> --%>
<%-- <c:if test="${my_url != 'list?'}">
	<c:set var="my_url_length" value="${fn:length(my_url)}" />
	<c:set var="my_url" value="${fn:substring(my_url, 0, my_url_length - 1)}" />
</c:if> --%>
<%-- <c:out value=" MYURL -->${my_url}====>"></c:out>  --%>



<sql:query dataSource="${acessData}" var="primary_key">
         select column_name as target from lms_web_view where table_name = '${table_name}' and is_primary = 1 limit 1;
</sql:query>
<c:set var="pri_key_col" value="${primary_key.rowsByIndex[0][0]}" />

<c:set var="order" value="${pri_key_col}" />
<c:set var="order_dir" value="" />


<c:if test="${not empty param.order}">
	<c:set var="order" value="${param.order}" />
	<c:set var="order_dir" value="${param.order_dir}" />
</c:if>

<c:set var="pageno" value="0" />
<c:if test="${not empty param.pageno}">
	<c:set var="pageno" value="${param.pageno}" />
</c:if>

<c:set var="limit_begin" value="${ pageno * page_size }" />

<c:set var="where_clause" value="" />


<!-- ---------------SEARCH BEGIN-------------------->

<sql:query dataSource="${acessData}" var="searchColCount">
	select count(*) as target from lms_search_view where table_name = '${table_name}' and is_search = 1;
</sql:query>

<c:set var="search_field_count"
	value="${searchColCount.rowsByIndex[0][0]}" />

<c:set var="search_full_display" value="none" />

<c:if test="${search_field_count > 0}">
	<c:set var="search_full_display" value="block" />
</c:if>

<%-- <c:out value=" SDETAILSL1====> search_field_count-->${search_field_count}====>search_full_display===>${search_full_display } =======> searchpara====${param.searched}"></c:out>
 --%>
<c:set var="search_inner_display" value="none" />

<jsp:useBean id="search_params" class="java.util.HashMap" />

<c:set target="${search_params}" property="none" value="none" />
<c:if test="${not empty param.searched}">
	<c:set var="search_full_display" value="block" />
	<c:forEach items="${param}" var="par" varStatus="loop">
<%-- 	<c:out value=" SDETAILSL2====> Param Key=${par.key}"></c:out>
 --%>		<c:if test="${fn:substring(par.key, 0, 5) == 'sear_'}">
<%-- 				<c:out value=" SDETAILSL3====> Param Key Matched=${par.key}"></c:out>
 --%>				
			<c:set target="${search_params}" property="${par.key}"
				value="${par.value}" />
			  <c:set var="comparison" value="${fn:substring(par.key, 5, 7)}" />
			 		 
			<c:set var="comp_sign" value=" = " />
			<c:choose>
				<c:when test="${comparison == 'gt'}">
					<c:set var="comp_sign" value=" > " />
					
				</c:when>
				<c:when test="${comparison == 'ge'}">
					<c:set var="comp_sign" value=" >= " />
					
				</c:when>
				<c:when test="${comparison == 'lt'}">
					<c:set var="comp_sign" value=" < " />
					
				</c:when>
				<c:when test="${comparison == 'le'}">
					<c:set var="comp_sign" value=" <= " />
					
				</c:when>
				<%-- <c:otherwise>
					<c:set var="comp_sign" value=" = " />
				</c:otherwise> --%>
			</c:choose>
<%-- 			<c:out value=" SDETAILSL4====> comparison=${comparison}==================> comp Sign = ${comp_sign}"></c:out>
 --%>			
<%-- 			<c:set target="${key_w0_name}" --%>
<%-- 				value="${fn:substringAfter(par.key, fn:substring(par.key, 0, 9)}" /> --%>

			<c:set var="comp_key"
				value="${fn:substringAfter(par.key, fn:substring(par.key, 0, 10))}" />
			<c:set var="var_type" value="${fn:substring(par.key, 8, 9)}" />

			<c:if test="${fn:length(fn:trim(par.value)) > 0}">
				<c:choose>
					<c:when test="${var_type == 't'}">
						<c:set var="where_clause"
							value="${where_clause} and ${comp_key} like '%${par.value}%' " />
								</c:when>
					<c:when test="${var_type == 'l'}">
					      
					</c:when>
					<c:when test="${var_type == 'b'}">
						<c:set var="where_clause"
							value="${where_clause} and ${comp_key} = '${par.value}' " />
							
					</c:when>
					<c:when test="${var_type == 'd'}">
						<c:set var="where_clause"
							value="${where_clause} and ${comp_key} >= '${par.value} 00:00:00' 
												and $comp_key <= '${par.value} 23:59:59'  " />
							
					</c:when>
					<c:when test="${var_type == 'n'}">
						<c:set var="where_clause"
							value="${where_clause} and ${comp_key} ${comp_sign} '${par.value}' " />
							
					</c:when>
					<c:otherwise>
						<c:set var="where_clause"
							value="${where_clause} and ${comp_key} ${comp_sign} '${par.value}' " />
							
					</c:otherwise>
				</c:choose>

			</c:if> 
<%-- 			<c:out value=" SDETAILSL5====> comparison Key=${comp_key}==================> comp Sign = ${comp_sign}====> Where ===============>${where_clause}"></c:out>
 --%>
		</c:if>
	</c:forEach>
</c:if>

<script>
	function toggleSearchView() {
		var x = document.getElementById("searchdiv");
		if (x.style.display === "none") {
			x.style.display = "block";
		} else {
			x.style.display = "none";
		}
	}
</script>

<div id="searchfulldiv" style="display:${search_full_display}">
	<form id="search" method="post" name="search" action="${my_url}">
		<input type="hidden" value="1" name="searched">
		<table class="MainTable" border="0" cellspacing="0" cellpadding="0"
			align="center">
			<tr>
				<td valign="top">
					<table class="Header" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="HeaderLeft"><img alt=""
								src="styles/cloudstyle1/Images/Spacer.gif"></td>
							<td class="th"><a onclick="return toggleSearchView();"><img
									src="styles/cloudstyle1/Images/plus_minus.png" height="16"
									width="16"><strong> Search </strong></a></td>
							<td class="HeaderRight"><img alt=""
								src="styles/cloudstyle1/Images/Spacer.gif"></td>
						</tr>

					</table>

					<div id="searchdiv" style="display:${search_inner_display}">
						<table class="Record" cellspacing="0" cellpadding="0">
							<sql:query dataSource="${acessData}" var="searchData">
								select column_name , disp_name , data_type, ui_type , ui_spec , html_args from lms_search_view where table_name = '${table_name}' 
								and is_search = 1 order by search_order;
							</sql:query>
							<c:set var="search_rows" value="${searchData.rowCount}" />
							<c:set var="tabindex" value="1" />
							<c:set var="evenrow" value="false" />

							<c:forEach var="row" items="${searchData.rows}" varStatus="loop">
								<c:set var="column_name" value="${row.column_name}" />
								<c:set var="disp_name" value="${row.disp_name}" />
								<c:set var="data_type" value="${row.data_type}" />
								<c:set var="ui_type" value="${row.ui_type}" />
								<c:set var="ui_spec" value="${row.ui_spec}" />
								<c:set var="html_args" value="${row.html_args}" />
								<c:set var="var_type" value="t" />
								<c:choose>
									<c:when test="${fn:containsIgnoreCase(data_type,'varchar')}">
										<c:set var="var_type" value="t" />
									</c:when>
									<c:when test="${fn:containsIgnoreCase(data_type,'text')}">
										<c:set var="var_type" value="t" />
									</c:when>
									<c:when test="${fn:containsIgnoreCase(data_type,'tinyint')}">
										<c:set var="var_type" value="b" />
									</c:when>
									<c:when test="${fn:containsIgnoreCase(data_type,'int')}">
										<c:set var="var_type" value="n" />
									</c:when>
									<c:when test="${fn:containsIgnoreCase(data_type,'decimal')}">
										<c:set var="var_type" value="n" />
									</c:when>
									<c:when test="${fn:containsIgnoreCase(data_type,'numeric')}">
										<c:set var="var_type" value="n" />
									</c:when>
									<c:when test="${fn:containsIgnoreCase(data_type,'date')}">
										<c:set var="var_type" value="d" />
									</c:when>
									<c:when test="${fn:containsIgnoreCase(data_type,'dropdown')}">
										<c:set var="var_type" value="l" />
									</c:when>
									<c:when test="${fn:containsIgnoreCase(data_type,'list')}">
										<c:set var="var_type" value="l" />
									</c:when>
									<%-- <c:otherwise>
										<c:out value=" nomatch for ${data_type}"></c:out>
									</c:otherwise> --%>
								</c:choose>
								<%-- <c:out value=" SDETAILSL6====> column_name=${column_name}=====>data_type = ${data_type}
								====>ui_type====>${ui_type}====>ui_spec====>${ui_spec}====>var_type====>${var_type}"></c:out> --%>
								

								<c:set var="name" value="sear_eq_${var_type}_${column_name}" />
								<c:set var="value" value="" />

								<c:if test="${not empty search_params[name]}">
									<c:set var="value" value="${search_params[name]}" />
								</c:if>

								<c:if test="${!evenrow}">
									<tr class="Controls">
								</c:if>

								<c:if test="${evenrow}">
									<td></td>
								</c:if>

								<td class="th">${disp_name}</td>

								<c:choose>
									<c:when test="${ui_type == 'text'}">
										<td><input type="text" tabindex="${tabindex}" value="${value}" name="${name}"></td>
									</c:when>
									<c:when test="${ui_type == 'date'}">
										<td><input type="text" tabindex="${tabindex}" value="${value}" name="${name}"></td>
									</c:when>
									<c:when test="${ui_type == 'numeric'}">
											<td><input type="text" tabindex="${tabindex}" value="${value}" name="${name}"></td>
									</c:when>
									<c:when test="${ui_type == 'checkbox'}">
										<td><input type="text" tabindex="${tabindex}"
											value="${value}" name="${name}"></td>
									</c:when>
									<c:when test="${ui_type == 'dropdown'}">
										<td><select tabindex="${tabindex}" name="${name}">
												<option value=""></option>
												<c:choose>
													<c:when test="${fn:containsIgnoreCase(ui_spec,'{')}">
														<c:set var="ui_spec"
															value="${fn:replace(ui_spec, '{', '')}" />
														<c:set var="ui_spec"
															value="${fn:replace(ui_spec, '}', '')}" />
														<c:forTokens items="${ui_spec}" delims="," var="listVal">
															<c:set var="selected" value="" />
															<c:if test="${value == listVal}">
																<c:set var="selected" value=" selected " />
															</c:if>

															<option value="${listVal}" ${selected}>${listVal}</option>
														</c:forTokens>
													</c:when>
													<c:otherwise>
														<sql:query dataSource="${acessData}" var="listData">
         													${ui_spec};
      													</sql:query>

														<c:forEach var="row" items="${listData.rows}"
															varStatus="loop">
															<c:set var="listVal" value="${row[0] }" />

															<c:set var="selected" value="" />
															<c:if test="${value == listVal}">
																<c:set var="selected" value=" selected " />
															</c:if>

															<option value="${listVal}" ${selected}>${listVal}</option>
														</c:forEach>

													</c:otherwise>
												</c:choose>
										</select></td>
									</c:when>
								</c:choose>
								<c:set var="tabindex" value="${tabindex+1 }" />
								<c:if test="${evenrow}">
									</tr>
								</c:if>
								<c:set var="evenrow" value="${!evenrow }" />


							</c:forEach>
							<c:if test="${evenrow}">
								<td></td>
								<td class="th"></td>
								<td class="th"></td>
								</tr>
							</c:if>

							<tr class="Bottom">
								<td style="TEXT-ALIGN: right" colspan="5"><input
									type="submit" id="HHT_AGENTSearchButton_DoSearch"
									class="Button" value="Search" alt="Search"
									name="Button_DoSearch"></td>
							</tr>

						</table>


					</div>
				</td>
			</tr>
		</table>
	</form>
	<br />
</div>



<!-- -----------------------------SEARCH END-------------------------------------- -->

<jsp:useBean id="columnMap" class="java.util.HashMap" scope="request" />

<sql:query dataSource="${acessData}" var="tableMetaData">
	select disp_order, column_name , disp_name from lms_web_view where table_name = '${table_name}' and is_disp = 1 order by disp_order;
</sql:query>

<c:set var="query_columns" value="" />

<c:forEach items="${tableMetaData.rows}" var="columnData"
	varStatus="loop">
	<c:set target="${columnMap}" property="${columnData.column_name}"
		value="${columnData.disp_name}" />
	<c:choose>
		<c:when test="${loop.last}">
			<c:set var="query_columns"
				value="${query_columns}${columnData.column_name}" />
		</c:when>
		<c:otherwise>
			<c:set var="query_columns"
				value="${query_columns}${columnData.column_name}," />
		</c:otherwise>
	</c:choose>
</c:forEach>

<%-- <c:set var="query_data" value="select ${query_columns} from ${table_name} where 1=1 ${where_clause} order by 
										${order} ${order_dir} limit ${limit_begin},${page_size}" /> 
 <c:out value="${query_data}" />  --%>
     
      
          
<c:if test="${param.tn ==1&& not empty branch}">  
       <c:set var="where_clause"
							value="${where_clause} and  (h3 = '${branch}'  or h3 in (select branch_name from fe_branchmaster where cluster = '${branch}')) " />        

</c:if>
  




<sql:query dataSource="${acessData}" var="tableData" >

	select ${query_columns} from ${table_name} where  1=1 ${where_clause} order by ${order} ${order_dir} limit ${limit_begin},${page_size};
</sql:query>
    

<c:set var="download_query" value="select ${query_columns} from ${table_name} where 1=1 ${where_clause} order by ${order} ${order_dir} limit 1000" scope="application"/>

<table class="MainTable" cellspacing="0" cellpadding="0" align="center"
	border="0">
	<tr>
		<td valign="top">
			<table class="Header" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td class="HeaderLeft"><img alt=""
						src="styles/cloudstyle1/Images/Spacer.gif"></td>
					<td class="th"><strong>${title}</strong> &nbsp; &nbsp; &nbsp;
						&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
						&nbsp; &nbsp; &nbsp; &nbsp; <small><a href="report?title=${title}" target="_blank" ><u>Download
									as XLSX</u></a></small></td>
					<td class="HeaderRight"><img alt=""
						src="styles/cloudstyle1/Images/Spacer.gif"></td>
				</tr>
			</table>

			<table class="Grid" cellspacing="0" cellpadding="0">
				<tr class="Caption">
					<th scope="col"><span="Sorter">Edit</span></th>
					<c:set var="no_of_cols" value="0" />
					<c:forTokens items="${query_columns}" delims="," var="headerCol"
						varStatus="loop">
						<c:set var="dir" value="" />
						<c:set var="dirarrow" value="" />
						<c:if test="${order == headerCol}">							
							<c:choose>
								<c:when test="${order_dir == '' || order_dir == 'asc'}">
									<c:set var="dir" value="desc" />
									<c:set var="dirarrow"
										value='<img alt="Ascending" src="styles/cloudstyle1/Images/Asc.gif">' />
								</c:when>
								<c:otherwise>
									<c:set var="dir" value="" />
									<c:set var="dirarrow"
										value='<img alt="Descending" src="styles/cloudstyle1/Images/Desc.gif">' />
								</c:otherwise>
							</c:choose>							
						</c:if>
						<th scope="col"><span class="Sorter"><a
								href="${my_url}&pageno=${pageno}&order=${headerCol}&order_dir=${dir}">${columnMap[headerCol]}</a>
								${dirarrow} </span></th>

						<c:set var="no_of_cols" value="${loop.count}" />
					</c:forTokens>
				</tr>

				<c:set var="rowclass" value="Row" />
				<c:set var="result_row_count" value="${tableData.rowCount}" />
				<c:forEach var="row" items="${tableData.rows}" varStatus="loop">
					<tr class='${rowclass}'>
						<c:set var="pkVal" value="${row[pri_key_col]}" />
						
						<td align="center">&nbsp;<a
							href="edit?tn=${table_number}&pkval=${pkVal}"><img
								alt="" height="16" width="16"
								src="styles/cloudstyle1/Images/Edit.png"></a></td>
						<c:forTokens items="${query_columns}" delims="," var="headerCol">
							<td>${row[headerCol]}</td>
						</c:forTokens>
					</tr>
					<c:set var="rowclass"
						value="${rowclass eq 'Row' ? 'AltRow' : 'Row'}" />

				</c:forEach>
				<tr class="Footer">
					<c:set var="footer_cols" value="${ no_of_cols + 1 }" />
						<c:set var="searched_params" value="" />
	<c:forEach items="${param}" var="par" varStatus="loop">
	<c:if test="${par.key ne 'tn' and par.key ne 'Button_DoSearch' and par.key ne 'pageno'}">
	<c:if test="${fn:length(fn:trim(par.value)) > 0}">
	<c:choose>
	<c:when test="${param} != ''">
	<c:set var="searched_params" value="&${ searched_params }&${par.key}=${par.value }" />
	</c:when>
	<c:otherwise>
	<c:set var="searched_params" value="${ searched_params }&${par.key}=${par.value }" />
	</c:otherwise>
	</c:choose>
	</c:if>
	</c:if>
	</c:forEach>

					
					<td colspan="${footer_cols}"><c:set var="from_row"
							value="${ limit_begin + 1 }" /> <c:set var="to_row"
							value="${ from_row + result_row_count - 1 }" /> <sql:query
							dataSource="${acessData}" var="recordCount">
							select count(*) as target from ${table_name} where 1=1 ${where_clause};
							</sql:query> <c:set var="tot_count" value="${recordCount.rowsByIndex[0][0]}" />

						<c:choose>
							<c:when test="${pageno > 0}">
								<c:set var="prev_pageno" value="${ pageno - 1 }" />
								<a
								href="${my_url}&${searched_params}&pageno=0">
									<img alt="First" src="styles/cloudstyle1/Images/First.gif">
								</a>
								<a
									href="${my_url}&${searched_params}&pageno=${prev_pageno}">
									<img alt="Prev" src="styles/cloudstyle1/Images/Prev.gif">
								</a>
							</c:when>
							<c:otherwise>
								<img alt="" src="styles/cloudstyle1/Images/FirstOff.gif">
								<img alt="" src="styles/cloudstyle1/Images/PrevOff.gif">
							</c:otherwise>
						</c:choose> <b> ${from_row} </b> to <b> ${to_row} </b> of <b>
							${tot_count} </b> Rows <c:choose>
							<c:when test="${to_row < tot_count}">
								<c:set var="next_pageno" value="${ pageno + 1 }" />
								<c:set var="last_pageno_float"
									value="${(tot_count - 1)/page_size }" />

								<fmt:parseNumber var="last_pageno" integerOnly="true"
									type="number" value="${last_pageno_float}" />
								<a
									href="${my_url}&${searched_params}&pageno=${next_pageno}">
									<img alt="Prev" src="styles/cloudstyle1/Images/Next.gif">
								</a>
								<a
								href="${my_url}&${searched_params}&pageno=${last_pageno}">
									<img alt="Prev" src="styles/cloudstyle1/Images/Last.gif">
								</a>
							</c:when>
							<c:otherwise>
								<img alt="" src="styles/cloudstyle1/Images/NextOff.gif">
								<img alt="" src="styles/cloudstyle1/Images/LastOff.gif">
							</c:otherwise>
						</c:choose></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<jsp:include page="tail.jsp" />