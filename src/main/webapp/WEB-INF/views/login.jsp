<head>
<title>ORFLEND LMS LOGIN </title>
</head>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<%@include file="head.jsp"%>


<%--LOGIN NEW PAGE --%>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!-- Include the above in your HEAD tag -->
<style>
@charset "utf-8";




div.main{
    background: #D7D7D7; /* Old browsers */
background: -moz- radial-gradient(ellipse at center, rgba(255,255,255,.15) 1%,#eceae5 100%); /* FF3.6+ */
background: -webkit- radial-gradient(ellipse at center, rgba(255,255,255,.15) 1%,#eceae5 100%);/* Chrome10+,Safari5.1+ */
background: -o- radial-gradient(ellipse at center, rgba(255,255,255,.15) 1%,#eceae5 100%); /* Opera 12+ */
background: -ms- radial-gradient(ellipse at center, rgba(255,255,255,.15) 1%,#eceae5 100%);/* IE10+ */
background:  radial-gradient(ellipse at center, rgba(255,255,255,.15) 1%,#eceae5 100%);/* W3C */
background: radial-gradient(ellipse at center, rgba(255,255,255,.15) 1%,#eceae5 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0264d6', endColorstr='#1c2b5a',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
height:calc(100vh);
width:100%;
}

[class*="fontawesome-"]:before {
  font-family: 'FontAwesome', sans-serif;
}

/* ---------- GENERAL ---------- */

* {
  box-sizing: border-box;
    margin:0px auto;

  &:before,
  &:after {
    box-sizing: border-box;
  }

}



a {
	color: #eee;
	text-decoration: none;
}

a:hover {
	text-decoration: underline;
}

input {
	border: none;
	font-family: 'Open Sans', Arial, sans-serif;
	font-size: 14px;
	line-height: 1.5em;
	padding: 0;
	-webkit-appearance: none;
}

p {
	line-height: 1.5em;
}

.clearfix {
  *zoom: 1;

  &:before,
  &:after {
    content: ' ';
    display: table;
  }

  &:after {
    clear: both;
  }

}

.container {
  left: 50%;
  position: fixed;
  top: 50%;
  transform: translate(-50%, -50%);
}

/* ---------- LOGIN ---------- */

#login form{
	width: 250px;
}
#login, .logo{
    display:inline-block;
    color:#D7D7D7;
    width:40%;
}
#login{
border-right:1px solid #D7D7D7;
  padding: 0px 22px;
  width: 59%;
}
.logo{
color:#D7D7D7;
font-size:50px;
  line-height: 125px;
}

#login form span.fa {
	background-color: #D7D7D7;
	border-radius: 3px 0px 0px 3px;
	color: #000;
	display: block;
	float: left;
	height: 50px;
    font-size:24px;
	line-height: 50px;
	text-align: center;
	width: 50px;
}

#login form input {
	height: 50px;
}
fieldset{
    padding:0;
    border:0;
    margin: 0;

}
#login form input[type="text"], input[type="password"] {
	background-color: #fff;
	border-radius: 0px 3px 3px 0px;
	color: #000;
	margin-bottom: 1em;
	padding: 0 16px;
	width: 200px;
}

#login form input[type="submit"] {
  border-radius: 3px;
  -moz-border-radius: 3px;
  -webkit-border-radius: 3px;
  background-color: #FCA868;
  color: #eee;
  font-weight: bold;
  /* margin-bottom: 2em; */
  text-transform: uppercase;
  padding: 5px 10px;
  height: 30px;
}

#login form input[type="submit"]:hover {
	background-color: #FCA868;
}

#login > p {
	text-align: center;
}

#login > p span {
	padding-left: 5px;
}
.middle {
  display: flex;
  width: 600px;
}

body {
	color: #000000;
	background-image: url('styles/cloudstyle1/Images/PageBg.gif');
	font-family: Arial;
	font-size: 100%;
}
</style>



<head>
<title>LIVE LOGIN</title>
</head>
<body>
<div class="main">
    
    
    <div class="container">
<center>
<div class="middle">

      <div id="login">

        <form method="POST" action="login" class="form-signin">

          <fieldset class="clearfix">
				<p align="center"     style="font-size: large;
    color: darkgreen;-webkit-text-stroke: medium;"> ORFIL LMS  LOGIN </p>
            <p ><span class="fa fa-user"></span><input name="username" type="text" class="form-control" placeholder="Username" autofocus="true" tabindex="1"/></p> <!-- JS because of IE support; better: placeholder="Username" -->
            <p><span class="fa fa-lock"></span> <input name="password" type="password" class="form-control" placeholder="Password" tabindex="2"/> <!-- JS because of IE support; better: placeholder="Password" -->
            
             <div>
                              <span style="width:50%; text-align:right;  display: inline-block;"><input type="submit" value="Sign In"></span>
                            </div>
          </fieldset>
<div class="clearfix"></div>
        </form>

        <div class="clearfix"></div>

      </div> <!-- end login -->
      <div class="logo"> <img width="200" height:auto  src="styles/cloudstyle1/Images/orng.png" />
      
          <img width="100" height:auto  src="styles/cloudstyle1/Images/unnamed.jpg" />
           <img width="100" height:auto  src="http://13.235.120.228/live/Webimg/GreatPlaceToWork.jpeg" />           
          <div class="clearfix"></div>
      </div>
      
      </div>
</center>
    </div>

</div>
</body>
<jsp:include page="tail.jsp" />

