<head>
<title> ORFLEND LOAN</title>
</head>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--  <set var="title" value="List"/> -->
<center>
	<h1>Loan Status</h1>
</center>
<%@include file="menu.jsp"%>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
var Msg ='<%=request.getParameter("waiveAndCloseMessage")%>';
var id ='<%=request.getParameter("id")%>';
if(Msg!=null && Msg!='' && Msg!='null'){
	alert(Msg);
	window.location.href = "/OLMS/getloan?id="+id;
}
function showConfirmDlg(id)
{
	$('dialog-confirm').css('display', 'block');
	 $( "#dialog-confirm" ).dialog({
	      resizable: false,
	      height: "auto",
	      width: 400,
	      modal: true,
	      buttons: {
	        "Yes": function() {
	        	window.location = "cancelLoan?id="+id;
	        	$( this ).dialog( "close" );
	        },
	        "No": function() {
	          $( this ).dialog( "close" );
	        }
	      }
	    });
}

function cancelLoanRemarks(id){
    var reason = prompt('Cancel reason','');
    //document.getElementById('cancel').value=reason;
    if(reason !=null && reason.length >= 3 && reason.length <100){//greater than 3 and less than 100
    	//window.alert("success==>"+reason.length );
    	window.location = "cancelLoan?id="+id+"&reason="+reason;
    }else if (reason !=null && reason.length >=100){
    	window.alert("Reason should be less than 100 characters");
    }else if(reason !=null && reason.length <3){
    	window.alert("Reason should be 3 or more characters");
    }else{
    	//not proceded for cancellation
    }
}
function getWaivePassword(id,loanNo){
    var password = prompt('Waive password','');
     if(password !=null && password.length >= 1){
    	//window.alert("Entered password..." + password + "id" + id + "loanNo" + loanNo);
    	window.location = "waiveAndClose?id="+id+"&loanNo="+loanNo+"&waive_password="+password;
    }
}
  
</script>
<div id="dialog-confirm" title="Cancel Loan" style="display: none">
	<p>
		<span class="ui-icon ui-icon-alert"
			style="float: left; margin: 12px 12px 20px 0;"></span>Loan will be
		cancelled. Are you sure?
	</p>
</div>

<table class="MainTable" border="0" cellspacing="0" cellpadding="0"
	align="center">
	<tr>
	<td><div>
<c:choose>
<c:when test="${loan.cusType == 'P'}" >
<img src="http://13.235.120.228/live/Webimg/Platinum.png" height="100px" width="100px">
</c:when>
</c:choose>
<c:choose>
<c:when test="${loan.cusType == 'G'}" >
<img src="http://13.235.120.228/live/Webimg/Gold.png" height="100px" width="100px">
</c:when>
</c:choose>
<c:choose>
<c:when test="${loan.cusType == 'S'}" >
<img src="http://13.235.120.228/live/Webimg/Silver.png" height="100px" width="100px">
</c:when>
</c:choose>
<c:choose>
<c:when test="${loan.cusType == 'B'}" >
<img src="http://13.235.120.228/live/Webimg/Bronze.png" height="100px" width="100px">
</c:when>
</c:choose>
	</div></td>
	<br></br>
		<td valign="top">
			<table class="Header" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="HeaderLeft"><img alt=""
						src="styles/cloudstyle1/Images/Spacer.gif"></td>
					<td class="th"><strong> Loan Details </strong></td>
					<td class="HeaderRight"><img alt=""
						src="styles/cloudstyle1/Images/Spacer.gif"></td>
				</tr>

			</table>
			<div id="LoanDetails">
				<table class=Record cellspacing="0" cellpadding="0">
					<tr class="Row">
						<td><strong>Loan Id</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.loanId}</td>
						<td></td>
						<td><strong>CKYC Number</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.ckycNumber}</td>
						<td></td>
						<td><strong>Loan No</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.loanNo}</td>
						<td></td>
						<td><strong>Status</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.status}</td>
					</tr>
					<tr class="AltRow">
						<td><strong>Loan Amount(Rs)</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.loanAmount}</td>
						<td></td>
						<td><strong>Tenure</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.tenure}</td>
						<td></td>
						<td><strong>Disbursement Date</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.disbDate}</td>
						<td></td>
						<td><strong>Prin Outstanding</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.pos}</td>
					</tr>
					<tr class="Row">
						<td><strong>Customer Name</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.custName}</td>
						<td></td>
						<td><strong>Arrears(Rs)</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.prinArrear}</td>
						<td></td>
						<td><strong>BC</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.bc}</td>
						<td></td>
						<td><strong>ODC</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.odc}</td>
						<%-- <td></td>
						<td><strong>OverDue Months</strong></td>
						<td>${customerContact}</td>
						<td>${loan.odMonths}</td> --%>
					</tr>
					<tr class="AltRow">
						<td><strong>First Due Date</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.firstDueDate}</td>
						<td></td>
						<td><strong>Maturity Date</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.lastDueDate}</td>
						<td></td>
						<!-- <td><strong>Current Month</strong></td> -->
						<td><strong>Agreement Date</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.agmntDate}</td>
						<td></td>
						<td><strong>Closed Date</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.closedTime}</td>
					</tr>
					<tr class="Row">
						<td class="tdh"><strong>Payment Mode</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.mode}</td>
						<td></td>
						<td><strong>Co-Lender</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.coLender}</td>
						<td></td>
						<td><strong>Hypothecation</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.hypothecation}</td>
						<td></td>
						<c:choose>
								<c:when test="${loan.flagSec == 'N' && loan.addSOAEnabled != '2' && loan.addSOAEnabled != '4'  && loan.fclBlock == '1'}" >
						<td><strong>ForeClosure Amount</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.foreClosureAmt}</td>
</c:when>
</c:choose>
	<c:choose>
								<c:when test="${loan.addSOAEnabled == '2' || loan.addSOAEnabled == '4'}" >
						<td><strong>ForeClosure Amount</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.foreClosureAmt}</td>
</c:when>
</c:choose>
					</tr>
					<tr class="Row">
					<td> <strong>Write-Off Date</strong></td>
					<td></td><td></td>
					<td><strong>Status Reason</strong></td>
					
					<td>${loan.statusDisposition}</td>
					<td></td>
					<td><strong>Parent Loan</strong></td>
					
					<td>${loan.applNo}</td>
					<td></td>
					<td><strong>Securitization</strong></td>
					
					<td>${loan.securitisation}</td>
					<td></td>
					</tr>
					<tr class="Row">	
		<c:choose>
										
     <c:when test="${loan.flagSec == 'Y'}" >
							<td ><strong>REACH</strong></td>
					
					<td>${loan.flagSec}</td>
					<td></td>
					</c:when></c:choose>
					
				
				  <td><strong>Ex Gratia Credit Amount</strong></td>
					
					<td>${loan.exGratiaCreditAmount}</td>
					<td></td>
					<td><strong>ExGratia Credit Amount Excess</strong></td>
					
					<td>${loan.exGratiaCreditAmountExcess}</td>
					<td></td> 
					<td><strong>Customer Type</strong></td>
					<c:choose>
					<c:when test="${loan.cusType == 'P'}" >
					<td>Platinum</td>
					</c:when>
					</c:choose>
					<c:choose>
					<c:when test="${loan.cusType == 'G'}" >
					<td>Gold</td>
					</c:when>
					</c:choose>
					<c:choose>
					<c:when test="${loan.cusType == 'S'}" >
					<td>Silver</td>
					</c:when>
					</c:choose>
					<c:choose>
					<c:when test="${loan.cusType == 'B'}" >
					<td>Bronze</td>
					</c:when>
					</c:choose>
					 <td></td>
		</tr>	
					
				</table>
				
			</div>
		
		</td>
	</tr>
</table>
<br></br>
<!-- 
<table class="MainTable" border="0" cellspacing="0" cellpadding="0"
			align="center">
			<tr>
				<td valign="top">
					<table class="Header" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="HeaderLeft"><img alt=""
								src="styles/cloudstyle1/Images/Spacer.gif"></td>
							<td class="th"><strong>SPL WAIVER SCHEME Details</strong></td>
							<td class="HeaderRight"><img alt=""
								src="styles/cloudstyle1/Images/Spacer.gif"></td>
						</tr>

					</table>					
					
					<div id="SPL WAIVER SCHEME Details">
						<table class=Grid cellspacing="0" cellpadding="0">
							<tr class="Caption">
														
									<th scope="col">SchemeType</th>
									<th scope="col">SchemePercentage</th>
									<th scope="col">PrinArrears</th>
									<th scope="col">PrinOS</th>
									<th scope="col">IntOnPosSinceLastDue</th>
									<th scope="col">Odc Due</th>
									<th scope="col">Moratorium Int</th>
									<th scope="col">Fcl Charges</th>
									<th scope="col">ChargesDue</th>
									<th scope="col">FclAmount</th>
							</tr>
							<c:set var="rowclass" value="Row" />
							<c:forEach items="${loan.splWaiverList}" var="splWaiverDetail">
								<tr class='${rowclass}'>
																		
									<td>${splWaiverDetail.schemeType}</td>
									<td>${splWaiverDetail.schemePercentage}</td>
									<td>${splWaiverDetail.tbcPrinArrears}</td>
									<td>${splWaiverDetail.tbcPrinOS}</td>
									<td>${splWaiverDetail.tbcIntOnPosSinceLastDue}</td>
									<td>${splWaiverDetail.tbcOdcDue}</td>
									<td>${splWaiverDetail.tbcMoratoriumInt}</td>
									<td>${splWaiverDetail.tbcFclCharges}</td>
									<td>${splWaiverDetail.tbcChargesDue}</td>
									<td>${splWaiverDetail.tbcFclAmount}</td>
								</tr>
								<c:set var="rowclass"
									value="${rowclass eq 'Row' ? 'AltRow' : 'Row'}" />
							</c:forEach>

						</table>
					</div>
				</td>
			</tr>
		</table>
<br></br>
 -->

<br></br>
<br></br>
 

<table class="MainTable" border="0" cellspacing="0" cellpadding="0"
	align="center">
	<tr>
		<td valign="top">
			<table class="Header" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="HeaderLeft"><img alt=""
						src="styles/cloudstyle1/Images/Spacer.gif"></td>
					<td class="th"><strong> Scheme Details </strong></td>
					<td class="HeaderRight"><img alt=""
						src="styles/cloudstyle1/Images/Spacer.gif"></td>
				</tr>

			</table>

			<div id="SchemeDetails">
				<table class=Record cellspacing="0" cellpadding="0">
					<tr class="Row">
						<td><strong>Scheme Type</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.scheme}</td>
						<td></td>
						<td><strong>Dealer Name</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.dealer}</td>
						<td></td>
						<td><strong>Dealer Code</strong></td>
							<td>${loan.dealerCode}</td>
						<td></td>
						<td><strong>Make</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.make}</td>
						<td></td>
						<td><strong>Model</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.model}</td>
					</tr>
					<tr class="AltRow">
						<td><strong>Grid Amount(Rs)</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.gridAmount}</td>
						<td></td>
						<td><strong>On Road Price(Rs)</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.onRoadPrice}</td>
						<td></td>
						<td><strong>Loan Amount(Rs)</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.loanAmount}</td>
						<td></td>
						<td><strong>Down Payment(Rs)</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.downPayment}</td>
					</tr>
					<tr class="Row">
						<td><strong>LTV(%)</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.ltv}</td>
						<td></td>
						<td><strong>Actual LTV(%)</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.actualltv}</td>
						<td></td>
						<td><strong>IRR(%)</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.irr}</td>
						<td></td>
						<td><strong>EMI(Rs)</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.emi}</td>
					</tr>
					<tr class="AltRow">
						<td><strong>Advance EMI Count</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.advanceEMI}</td>
						<td></td>
						<td><strong>Process. Fee(Rs)</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.prcesFee}</td>
						<td></td>
						<td><strong>Cash Mode Fee(Rs)</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.cashModeFee}</td>
						<td></td>
						<td><strong>Dealer Disb Amt(Rs)</strong></td>
						<%-- <td>${customerContact}</td> --%>
						<td>${loan.dealerDisbAmount}</td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
</table>

<br></br>
<c:choose>
								<c:when test="${loan.flagSec == 'N' && loan.addSOAEnabled != '2' && loan.addSOAEnabled != '4' && loan.fclBlock == '1'}" >
					
<table class="MainTable" border="0" cellspacing="0" cellpadding="0"
	align="center">
	<tr>
		<td valign="top">
			<table class="Header" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="HeaderLeft"><img alt=""
						src="styles/cloudstyle1/Images/Spacer.gif"></td>
					<td class="th"><strong> Foreclosure Details </strong></td>
					<td class="HeaderRight"><img alt=""
						src="styles/cloudstyle1/Images/Spacer.gif"></td>
				</tr>

			</table>

			<div id="Foreclosure Details">
				<table class=Record cellspacing="0" cellpadding="0">
					<tr class="Row">
						<td><strong>Principal Arrears(Rs)</strong></td>
						<td>${loan.prinArrear}</td>
						<td></td>
						<td><strong>Interest Arrears(Rs)</strong></td>
						<td>${loan.intArrear}</td>
						<td></td>
						<td><strong>Principal Outstanding(Rs)</strong></td>
						<td>${loan.pos}</td>
						<td></td>
						<td><strong>Interest Since Last due(Rs)</strong></td>
						<td>${loan.intAfterDue}</td>
						<td></td>
						<td><strong>OD Charges(Rs)</strong></td>
						<td>${loan.odc}</td>
						
					</tr>
					<tr class="AltRow">	
						<td><strong>Moratorium Interest(Rs)</strong></td>
						<td>${loan.moratoriumInterest}</td>
						<td></td>
						<td><strong>FCL Charges(Rs)</strong></td>
						<td>${loan.fclCharge}</td>
						<td></td>
						<td><strong>One Day Interest(Rs)</strong></td>
						<td>${loan.oneDayInterst}</td>
						<td></td>
						<td><strong>Other Charges(Rs)</strong></td>
						<td>${loan.oc}</td>
						<td></td>
						<td><strong>Excess Amount(Rs)</strong></td>
						<td>${loan.excessPaid}</td>
						
						
					</tr>
				<!-- 	<tr class="Row">
					<td><strong>Covid Moratotium Interest</strong></td>
						<td>${loan.covidPayInt}</td>
					</tr> -->
					<tr class="Row">
						
					</tr>
					<tr class="Row">				
						<td colspan=7><strong>Total Foreclosure Amount(Rs)</strong></td>
						<td colspan=7><strong>${loan.fclAmount}</strong></td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
	
</table>
</c:when>
</c:choose>
<c:choose>
								<c:when test="${loan.addSOAEnabled == '2' || loan.addSOAEnabled == '4'}" >
					
<table class="MainTable" border="0" cellspacing="0" cellpadding="0"
	align="center">
	<tr>
		<td valign="top">
			<table class="Header" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="HeaderLeft"><img alt=""
						src="styles/cloudstyle1/Images/Spacer.gif"></td>
					<td class="th"><strong> Foreclosure Details </strong></td>
					<td class="HeaderRight"><img alt=""
						src="styles/cloudstyle1/Images/Spacer.gif"></td>
				</tr>

			</table>

			<div id="Foreclosure Details">
				<table class=Record cellspacing="0" cellpadding="0">
					<tr class="Row">
						<td><strong>Principal Arrears(Rs)</strong></td>
						<td>${loan.prinArrear}</td>
						<td></td>
						<td><strong>Interest Arrears(Rs)</strong></td>
						<td>${loan.intArrear}</td>
						<td></td>
						<td><strong>Principal Outstanding(Rs)</strong></td>
						<td>${loan.pos}</td>
						<td></td>
						<td><strong>Interest Since Last due(Rs)</strong></td>
						<td>${loan.intAfterDue}</td>
						<td></td>
						<td><strong>OD Charges(Rs)</strong></td>
						<td>${loan.odc}</td>
						
					</tr>
					<tr class="AltRow">	
						<td><strong>Moratorium Interest(Rs)</strong></td>
						<td>${loan.moratoriumInterest}</td>
						<td></td>
						<td><strong>FCL Charges(Rs)</strong></td>
						<td>${loan.fclCharge}</td>
						<td></td>
						<td><strong>One Day Interest(Rs)</strong></td>
						<td>${loan.oneDayInterst}</td>
						<td></td>
						<td><strong>Other Charges(Rs)</strong></td>
						<td>${loan.oc}</td>
						<td></td>
						<td><strong>Excess Amount(Rs)</strong></td>
						<td>${loan.excessPaid}</td>
						
						
					</tr>
				<!-- 	<tr class="Row">
					<td><strong>Covid Moratotium Interest</strong></td>
						<td>${loan.covidPayInt}</td>
					</tr> -->
					<tr class="Row">
						
					</tr>
					<tr class="Row">				
						<td colspan=7><strong>Total Foreclosure Amount(Rs)</strong></td>
						<td colspan=7><strong>${loan.fclAmount}</strong></td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
	
</table>
</c:when>
</c:choose>


<br></br>

<c:choose>
	<c:when test="${loan.waiver == 'TRUE'}">
		<table class="MainTable" border="0" cellspacing="0" cellpadding="0"
			align="center">
			<tr>
				<td valign="top">
					<table class="Header" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="HeaderLeft"><img alt=""
								src="styles/cloudstyle1/Images/Spacer.gif"></td>
							<td class="th"><strong> Waiver Details </strong></td>
							<td class="HeaderRight"><img alt=""
								src="styles/cloudstyle1/Images/Spacer.gif"></td>
						</tr>

					</table>

					<div id="Waiver Details">
						<table class=Grid cellspacing="0" cellpadding="0">
							<tr class="Caption">
								<th scope="col">Waiver Date</th>
								<th scope="col">Waiver Type</th>
								<th scope="col">Waiver Amount</th>
								<th scope="col">Waiver Remarks</th>
							</tr>
							<c:set var="rowclass" value="Row" />
							<c:forEach items="${loan.loanWaivers}" var="waiverDetail">
								<tr class='${rowclass}'>								
									<td>${waiverDetail.waiverDate}</td>
									<td>${waiverDetail.waiverType}</td>
									<td>${waiverDetail.waiverAmount}</td>
									<td>${waiverDetail.waiverRemarks}</td>
								</tr>
								<c:set var="rowclass"
									value="${rowclass eq 'Row' ? 'AltRow' : 'Row'}" />
							</c:forEach>

						</table>
					</div>
				</td>
			</tr>
		</table>
	</c:when>
</c:choose>

<br></br>

<c:choose>
	<c:when test="${loan.cashInHand == 'TRUE'}">
		<table class="MainTable" border="0" cellspacing="0" cellpadding="0"
			align="center">
			<tr>
				<td valign="top">
					<table class="Header" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="HeaderLeft"><img alt=""
								src="styles/cloudstyle1/Images/Spacer.gif"></td>
							<td class="th"><strong>Pending Receipts</strong></td>
							<td class="HeaderRight"><img alt=""
								src="styles/cloudstyle1/Images/Spacer.gif"></td>
						</tr>

					</table>					
					
					<div id="Receipt Details">
						<table class=Grid cellspacing="0" cellpadding="0">
							<tr class="Caption">
								<th scope="col">Loan No</th>
								<th scope="col">Remittance No</th>
								<th scope="col">Book No</th>
								<th scope="col">Receipt No</th>
								<th scope="col">Receipt Date</th>
								<th scope="col">Receipt Amount</th>																
								<th scope="col">Agent Code</th>
								<th scope="col">Agent Name</th>
								<th scope="col">Cash In Hand</th>								
							</tr>
							<c:set var="rowclass" value="Row" />
							<c:forEach items="${loan.loanReceipts}" var="receiptDetail">
								<tr class='${rowclass}'>								
									<td>${receiptDetail.loanNo}</td>
									<td>${receiptDetail.remittanceNo}</td>
									<td>${receiptDetail.bookNo}</td>
									<td>${receiptDetail.receiptNo}</td>
									<td>${receiptDetail.receiptDate}</td>
									<td>${receiptDetail.receiptAmount}</td>
									<td>${receiptDetail.agentCode}</td>
									<td>${receiptDetail.agentName}</td>
									<td>${receiptDetail.cashInHand}</td>
								</tr>
								<c:set var="rowclass"
									value="${rowclass eq 'Row' ? 'AltRow' : 'Row'}" />
							</c:forEach>

						</table>
					</div>
				</td>
			</tr>
		</table>
	</c:when>
</c:choose>


<table class="MainTable" border="0" cellspacing="0" cellpadding="0"
			align="center">
			<tr>
				<td valign="top">
					<table class="Header" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="HeaderLeft"><img alt=""
								src="styles/cloudstyle1/Images/Spacer.gif"></td>
							<td class="th"><strong>Charges List</strong></td>
							<td class="HeaderRight"><img alt=""
								src="styles/cloudstyle1/Images/Spacer.gif"></td>
						</tr>

					</table>					
					
					<div id="Charges Details">
						<table class=Grid cellspacing="0" cellpadding="0">
							<tr class="Caption">
								<th scope="col">ID</th>
								<th scope="col">Loan No</th>
								<th scope="col">Charge ID</th>
								<th scope="col">Due Date</th>
								<th scope="col">ChargeAmount</th>
								<th scope="col">TaxAmount</th>
								<th scope="col">Status</th>					
								<th scope="col">ChargePaid</th>
								<th scope="col">TaxPaid</th>
							</tr>
							<c:set var="rowclass" value="Row" />
							<c:forEach items="${loan.chargesList}" var="chargesDetail">
								<tr class='${rowclass}'>
									<td>${chargesDetail.id}</td>
									<td>${chargesDetail.loanno}</td>
									<td>${chargesDetail.chargeid}</td>
									<td>${chargesDetail.duedate}</td>
									<td>${chargesDetail.chargeamount}</td>
									<td>${chargesDetail.taxamount}</td>
									<td>${chargesDetail.status}</td>
									<td>${chargesDetail.chargepaid}</td>
									<td>${chargesDetail.taxpaid}</td>
								</tr>
								<c:set var="rowclass"
									value="${rowclass eq 'Row' ? 'AltRow' : 'Row'}" />
							</c:forEach>

						</table>
					</div>
				</td>
			</tr>
		</table>
<br></br>


<%-- <form:form action="editLoan" method="post">
 --%>
<table class="MainTable" border="0" cellspacing="0" cellpadding="0"
	align="center">
	<tr>
		<td>
			<div id="Edit Loan">
				<%-- <table class=Record cellspacing="0" cellpadding="0">
						<tr class="Bottom">
							<td style="TEXT-ALIGN: center"><a href="stmnt?id=${loan.loanId}" 
								class="" target="_blank"><input type="submit" class="Button" value="Get Statement" name="Statement"></a></td>
							<td style="TEXT-ALIGN: center"><input type="submit"
								class="Button" value="NOC" name="noc"></td>
							<td style="TEXT-ALIGN: center"><input type="submit"
								class="Button" value="Loan Document" name="loandoc"></td>
							<td style="TEXT-ALIGN: center"><input type="submit"
								class="Button" value="Cancel Loan" name="cancel"></td>
						</tr>
						<tr class="Bottom">
							<td style="TEXT-ALIGN: center"><a href="addCrDrNote?id=${loan.loanId}" 
								class="" target="_blank"><input type="submit" class="Button" value="Add CR/DR Note" name="crdr"></a></td>
							<td style="TEXT-ALIGN: center"><input type="submit"
								class="Button" value="ForeClosure" name="foreclosure"></td>
							<td style="TEXT-ALIGN: center"><input type="submit"
								class="Button" value="Change Next EMI Date" name="changedate"></td>
							<td style="TEXT-ALIGN: center"><input type="submit"
								class="Button" value="Debit Charges" name="charges"></td>
						</tr>
						<tr class="Bottom">
							<td style="TEXT-ALIGN: center" colspan="4"><input
								type="submit" class="Button" value="Disburse" name="disburse"></td>
						</tr>
					</table> --%>
				
				<table class=Record cellspacing="0" cellpadding="0">
				<c:choose>
					<c:when test="${loan.userRole != 'ROLE_USER'}">	
					<tr class="Bottom">
						<td style="TEXT-ALIGN: center" colspan=1>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(loan.addWaiverEnabled, '1')}">
									<a href="addCrDrNote?id=${loan.loanId}" class="" target="_blank"><input
										type="submit" class="Button" value="Add Waiver" name="crdr"></a> 
								</c:when>
							</c:choose>
						</td>
						<td style="TEXT-ALIGN: center" colspan=1>
							 <c:choose>
							 	<c:when test="${fn:containsIgnoreCase(loan.waiveCloseEnabled, '2')}">
							<input type="submit" class="Button" value="Waive And Close" id="WaiveAndClose"
										name="WaiveAndClose" onclick="getWaivePassword('${loan.loanId}','${loan.loanNo}')">
						</c:when>
						</c:choose>
						</td>
<!--  <td>
	<a href="waiveCrDrNote?id=${loan.loanId}" class="" target="_blank"><input
										type="submit" class="Button" value="Waiver" name="WaiverTab"></a> 
</td> -->
						<td style="TEXT-ALIGN:center">
						<c:choose>
								<c:when test="${loan.cancelable == 'TRUE' && fn:containsIgnoreCase(loan.cancelEnabled, '1')}">
									<!-- <input type="submit" class="Button" value="Cancel" name="cancel" onclick="showConfirmDlg(${loan.loanId})"> -->
									<input type="submit" class="Button" value="Cancel" id="cancel"
										name="cancel" onclick="cancelLoanRemarks(${loan.loanId})">
								</c:when>
							</c:choose>
							 <%-- <input type="submit" class="Button" value="Cancel" id="cancel"
										name="cancel" onclick="cancelLoanRemarks(${loan.loanId})"> --%>

						</td>
						<form:form action="disbLoan" method="POST" modelAttribute="loan">
							<form:hidden path="loanId" value="${loan.loanId}" />
							<td style="TEXT-ALIGN: center" colspan=1><c:choose>
									<c:when
										test="${fn:containsIgnoreCase(loan.status, 'APPROVED') && fn:containsIgnoreCase(loan.disburseEnabled, '1')}">
										<c:choose>
											<c:when test="${loan.disbursed == 'FALSE'}">
												<input type="submit" class="Button" value="Disburse"
													name="disburse">
											</c:when>
											<c:otherwise>
												<input type="submit" class="Button" value="Re-Disburse"
													name="redisburse">
											</c:otherwise>
										</c:choose>
									</c:when>
								</c:choose></td>

							<!-- 							<td style="TEXT-ALIGN: center"><input type="submit" -->
							<!-- 								class="Button" value="Disburse" name="disburse"></td> -->
						</form:form>

						<!--  <td style="TEXT-ALIGN: center" colspan=1>
							<c:choose>
								<c:when test="${fn:containsIgnoreCase(loan.updatePDDEnabled, '1')}">
									<a href="addPDD?id=${loan.loanId}" class="" target="_blank"><input type="submit" class="Button" value="Update PDD" name="pdd"></a>
								</c:when>
							</c:choose> 
						 <c:choose>
								<c:when test="${fn:containsIgnoreCase(loan.waiveCloseEnabled, '1')}">
									<input type="submit" class="Button" value="Waive And Close" id="WaiveAndClose"
										name="WaiveAndClose" onclick="getWaivePassword('${loan.loanId}','${loan.loanNo}')">
								</c:when>
							</c:choose> -->
							

					</tr>
					</c:when>	
					</c:choose>
				
					<tr class="Bottom">
					<!--	<form:form action="genwl" method="POST" modelAttribute="loan">
							<form:hidden path="loanId" value="${loan.loanId}" />
							<td style="TEXT-ALIGN: center"><input type="submit"
								class="Button" value="Welcome Letter" name="Welcome Letter"></td>

						</form:form> --> 
<c:choose>
								<c:when test="${loan.userName != 'Akshata'}" >
					 	<form:form action="genwlbl" method="POST" modelAttribute="loan">
							<form:hidden path="loanId" value="${loan.loanId}" />
							<td style="TEXT-ALIGN: center"><input type="submit"
								class="Button" value="Welcome Letter" name="Welcome Letter"></td>

						</form:form></c:when></c:choose>

<c:choose>
								<c:when test="${loan.userName != 'Akshata'}" >
						<form:form action="genrepaySchedule" method="POST"
							modelAttribute="loan">
							<form:hidden path="loanId" value="${loan.loanId}" />
							<td style="TEXT-ALIGN: center"><input type="submit"
								class="Button" value="Repayment Schedule"
								name="Repayment Schedule"></td>

						</form:form>
						</c:when></c:choose>
	<c:choose>
								<c:when test="${loan.userName != 'Akshata'}" >					
						<form:form action="genForeClosure" method="POST"
							modelAttribute="loan">
							<form:hidden path="loanId" value="${loan.loanId}" />
							<td style="TEXT-ALIGN: center"><input type="submit"
								class="Button" value="Foreclosure Document"
								name="Foreclosure Document"></td>

						</form:form>
						</c:when></c:choose>
					<!-- 	<form:form action="genSOA" method="POST" modelAttribute="loan">
							<form:hidden path="loanId" value="${loan.loanId}" />
							<td style="TEXT-ALIGN: center"><input type="submit"
								class="Button" value="SOA" name="SOA"></td>

						</form:form>  -->
	
    	 						
						<td style="TEXT-ALIGN: center"> 
					<c:choose>
								<c:when test="${loan.flagSec == 'N' && loan.fclBlock == '1'}" >
						<a href="fetchSOA?id=${loan.loanId}" class="" target="_blank"><input
										type="submit" class="Button" value="SOADetails" name="SOA"></a>
						</c:when>
						</c:choose> 
							
							
						<!-- <a href="fetchSOA?id=${loan.loanId}" class="" target="_blank"><input
										type="submit" class="Button" value="SOADetails" name="SOA"></a> -->
						
						 
						</td>
				 
						<td style="TEXT-ALIGN: center"> 
					<c:choose>
								<c:when test="${loan.addSOAEnabled == '2'}" >
						<a href="fetchSOA?id=${loan.loanId}" class="" target="_blank"><input
										type="submit" class="Button" value="SOA" name="SOA"></a>
						</c:when>
						</c:choose> 
							
							
						<!-- <a href="fetchSOA?id=${loan.loanId}" class="" target="_blank"><input
										type="submit" class="Button" value="SOADetails" name="SOA"></a> -->
						
						 
						</td> 
						
	 	
	<!--   <td style="TEXT-ALIGN: center">
				<a href="updateColender?id=${loan.loanId}" class="" target="_blank"><input type="submit" class="Button" 
				value="Update Colender" name="UpdateColender"></a></td> 
					</tr>  -->

					<tr class="Bottom">
					<c:choose>
								<c:when test="${loan.userName != 'Akshata'}" >
						<form:form action="mailwl" method="POST" modelAttribute="loan">
							<form:hidden path="loanId" value="${loan.loanId}" />
							<td style="TEXT-ALIGN: center"><input type="submit"
								class="Button" value="Mail Welcome Letter"
								name="Mail Welcome Letter"></td>
						</form:form>

						<form:form action="mailrepaySchedule" method="POST"
							modelAttribute="loan">
							<form:hidden path="loanId" value="${loan.loanId}" />
							<td style="TEXT-ALIGN: center"><input type="submit"
								class="Button" value="Mail Repayment Amort"
								name="Mail Repayment Amort"></td>

						</form:form>


						<form:form action="mailForeClosure" method="POST"
							modelAttribute="loan">
							<form:hidden path="loanId" value="${loan.loanId}" />
							<td style="TEXT-ALIGN: center"><input type="submit"
								class="Button" value="Mail Foreclosure Letter"
								name="Mail Foreclosure Letter"></td>

						</form:form>

						<%-- 	<form:form action="mailSOA" method="POST" modelAttribute="loan">
							<form:hidden path="loanId" value="${loan.loanId}" /> --%>
	
    
		<!--Duplicate Issue 				<td style="TEXT-ALIGN: center"><input type="submit"
							class="Button" value="Mail SOA" name="Mail SOA"></td> --> 
							
						<%-- </form:form>		 --%>
</c:when></c:choose>
					</tr>
				</table>
			</div>
		</td>
	</tr>
</table>
<%-- </form:form>
 --%>
<jsp:include page="tail.jsp" />
