<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@include file="menu.jsp"%>
<!--  <set var="title" value="List"/> -->
<center>
	<h1>Loan Waiver</h1>
</center>

<form:form method="POST" action="waiveNote"   modelAttribute="crdrnote">
<table class="MainTable" border="0" cellspacing="0" cellpadding="0"
	align="center">
	<tr>
		<td valign="top">
			<table class="Header" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="HeaderLeft"><img alt=""
						src="styles/cloudstyle1/Images/Spacer.gif"></td>
					<td class="th"><strong> Loan Waiver </strong></td>
					<td class="HeaderRight"><img alt=""
						src="styles/cloudstyle1/Images/Spacer.gif"></td>
				</tr>

			</table>
			<div id="NoteDetails">
				<table class=Record cellspacing="0" cellpadding="0">
					<tr class="Row">
						<td><strong>Loan Id</strong></td>
						<form:hidden path="loanId" value="${loanid}" />
						<td><input type="text" value="${loanid}" name="${loanid}" readonly></td>
					</tr>					
					<tr class="Row">
						<td><strong>Principal</strong></td>
						<td><form:input path="principalAmount" /></td>
					</tr>
					<tr class="Row">
						<td><strong>Interest</strong></td>
						<td><form:input path="interestAmount" /></td>
					</tr>
					<tr class="Row">
						<td><strong>Charges Due</strong></td>
						<td><form:input path="chargesDueAmt" /></td>
					</tr>
					<tr class="Row">
						<td><strong>Penal Interest</strong></td>
						<td><form:input path="penalIntAmt" /></td>
					</tr>
					<tr class="Row">
						<td><strong>fcl</strong></td>
						<td><form:input path="fclAmt" /></td>
					</tr>
					<tr class="Row">
						<td><strong>brok per interest</strong></td>
						<td><form:input path="brokPerIntAmt" /></td>
					</tr>
					<tr class="Row">
						<td><strong>moratorium</strong></td>
						<td><form:input path="moratoriumAmt" /></td>
					</tr>
					
					
					
					<tr class="Row">
						<td><strong>Description</strong></td>
						<td><form:input path="description" /></td>
					</tr>
					<tr class="Bottom">
						<td colspan=2><input type='submit' value='Add Note' /></td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
</table>
</form:form>


<jsp:include page="tail.jsp" />