<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--  <set var="title" value="List"/> -->
<%@include file="menu.jsp"%>

<center>
	<h1>Welcome to ORFLEND</h1>
</center>
<sql:query dataSource="${acessData}" var="tableNames">
         SHOW TABLES;
      </sql:query>

<select name="tableList" onchange="location = this.value;">
	<c:forEach items="${tableNames.rows}" var="table">
		<option value=greet?tableName=${table.TABLE_NAME}
			${param.tableName == table.TABLE_NAME ? 'selected' : ''}>${table.TABLE_NAME}</option>
	</c:forEach>
</select>

<c:if test="${not empty param.tableName}">
	<%-- 	<c:out value="${param.tableName}"></c:out> --%>
	<sql:query dataSource="${acessData}" var="columnNames">
         select column_name from lms_web_view where table_name = '${param.tableName}';
      </sql:query>
	<c:set var="defColumns" value="" />
	<c:if test="${columnNames.rowCount > 0}">
		<c:forEach var="row" items="${columnNames.rows}" varStatus="loop">
			<c:set var="defColumns" value="${defColumns},${row.column_name}" />
		</c:forEach>
	</c:if>
	<c:set var="actColumns" value="desc ${param.tableName} " />

	<%-- 	 	 	<c:out value="${actColumns}"></c:out> --%>

	<sql:query dataSource="${acessData}" var="actcolumnData">
         ${actColumns};
    </sql:query>

<%-- <c:forEach var="row" items="${actcolumnData.rows}" varStatus="loop">
<c:out value="${row} index ${loop.index }"></c:out>
</c:forEach> --%>
	<c:if test="${actcolumnData.rowCount > 0}">

		<c:set var="insertMetaData"
			value="Insert into lms_web_view (table_name,column_name,disp_name,data_type,is_disp,disp_order,is_search,search_order,ui_type,is_editable,is_primary,create_user) values " />
		<c:forEach var="row" items="${actcolumnData.rows}" varStatus="loop">
			<c:set var="Field" value="${row.COLUMN_NAME}" />
			<c:if test="${!fn:contains(defColumns, Field)}">

				<c:set var="isEditable" value="1" />
				<c:set var="isPrimary" value="0" />

				<c:set var="keyValue" value="${row.COLUMN_KEY}" />
				<c:if test="${keyValue == 'PRI'}">
					<c:set var="isEditable" value="0" />
					<c:set var="isPrimary" value="1" />
				</c:if>

				<c:set var="data_type" value="${row.COLUMN_TYPE} " />
				
				<c:set var = "dtarray" value = "${fn:split(data_type, '(')}" />
				
				 <c:set var = "data_type" value = "${ dtarray[0]}"/>
				<c:set var="ui_type" value="text" />
				<c:choose>
					<c:when test="${data_type == 'varchar'}">
						<c:set var="ui_type" value="text" />
					</c:when>
					<c:when test="${data_type == 'char'}">
						<c:set var="ui_type" value="text" />
					</c:when>
					<c:when test="${data_type == 'int'}">
						<c:set var="ui_type" value="text" />
					</c:when>
					<c:when test="${data_type == 'decimal'}">
						<c:set var="ui_type" value="text" />
					</c:when>
					<c:when test="${data_type == 'date'}">
						<c:set var="ui_type" value="date" />
					</c:when>
					<c:when test="${data_type == 'datetime'}">
						<c:set var="ui_type" value="date" />
					</c:when>
					<c:when test="${data_type == 'tinyint'}">
						<c:set var="ui_type" value="checkbox" />
					</c:when>
				</c:choose>
				<c:choose>
					<c:when test="${loop.last}">
						<c:set var="insertMetaData"
							value="${insertMetaData} ('${param.tableName}','${Field}','${Field}','${data_type}',1,'${loop.index}',1,'${loop.index}','${ui_type}','${isEditable}','${ isPrimary}','WEB_VIEW')" />
					</c:when>
					<c:otherwise>
						<c:set var="insertMetaData"
							value="${insertMetaData} ('${param.tableName}','${Field}','${Field}','${data_type}',1,'${loop.index}',1,'${loop.index}','${ui_type}','${isEditable}','${ isPrimary}','WEB_VIEW')," />
					</c:otherwise>
				</c:choose>
			</c:if>
		</c:forEach>
<%--  		<c:out value="${insertMetaData}"></c:out>  --%>
		<sql:update dataSource="${acessData}" var="metaDataInsert">
         	${insertMetaData} ;
      	</sql:update>
	</c:if>

	<sql:query dataSource="${acessData}" var="webviewdata">
         Select * from lms_web_view where table_name = '${param.tableName}';
      </sql:query>

	<c:set var="dispColumn">id,table_name,column_name,disp_name,is_disp,disp_order,is_search,search_order,ui_type,
      					ui_spec,html_args,default_val,is_editable,is_primary,create_user,create_time</c:set>

	<table class="MainTable" cellspacing="0" cellpadding="0" align="center"
		border="0">
		<tr>
			<td valign="top">
				<table class="Header" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<td class="HeaderLeft"><img alt=""
							src="Styles/cloudstyle1/Images/Spacer.gif"></td>
						<td class="th"><strong>${param.tableName}</strong></td>
						<td class="HeaderRight"><img alt=""
							src="Styles/cloudstyle1/Images/Spacer.gif"></td>
					</tr>
				</table>
				<form name='form_update' method='post' action="greet">
					<input type='hidden' name='updateTable' value='${param.tableName}' />
					<input type='hidden' name='rowCount'
						value='${webviewdata.rowCount}' />
					<table class="Grid" cellspacing="0" cellpadding="0">
						<tr class="Caption">
							<c:forTokens items="${dispColumn}" delims="," var="headerCol">
								<th scope="col">${headerCol}</th>
							</c:forTokens>
						</tr>

						<c:set var="rowclass" value="Row" />
						<c:forEach var="row" items="${webviewdata.rows}" varStatus="loop">
							<tr class='${rowclass}'>
								<c:forTokens items="${dispColumn}" delims="," var="headerCol">
									<c:set var="controlName" value="${headerCol}${loop.index}" />
									<td><input type='text' size='10' name='${controlName}'
										value='${row[headerCol]}' /></td>
								</c:forTokens>
							</tr>
							<c:set var="rowclass"
								value="${rowclass eq 'Row' ? 'AltRow' : 'Row'}" />

						</c:forEach>
						<tr>
							<td><input type='submit' value='UPDATE' /></td>
						</tr>
					</table>
				</form>
			</td>
		</tr>
	</table>
</c:if>

<c:if test="${not empty param.updateTable}">
	<c:set var="updateColumn">disp_name,is_disp,disp_order,is_search,search_order,ui_type,ui_spec,
								html_args,default_val,is_editable,is_primary</c:set>

	<c:forEach begin="0" end="${param.rowCount-1}" varStatus="loop">
		<c:set var="updateQuery" value="UPDATE lms_web_view SET " />
		<c:forTokens items="${updateColumn}" delims="," var="colName">
			<c:set var="paramName" value="${colName}${loop.index}" />
			<c:set var="updateQuery"
				value="${updateQuery}${colName}='${param[paramName]}'," />
		</c:forTokens>
		<c:set var="idparam" value="id${loop.index}" />

		<c:set var="updateQuery"
			value="${updateQuery} modify_user = 'WEB_VIEW' WHERE id ='${param[idparam]}' " />
		<%-- 		<c:out value="${updateQuery}" /> --%>
		<sql:update dataSource="${acessData}" var="webviewupdate">
         	${updateQuery} ;
      	</sql:update>
	</c:forEach>
	<c:redirect url="/greet?tableName=${param.updateTable}" />
</c:if>
<jsp:include page="tail.jsp" />