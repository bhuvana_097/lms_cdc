<head>
<title>ORFLEND </title>
</head>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--  <set var="title" value="List"/> -->
<center>
	<h1>Loan Status</h1>
</center>
<%@include file="menu.jsp"%>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
var Msg ='<%=request.getParameter("waiveAndCloseMessage")%>';
var code ='<%=request.getParameter("code")%>';
if(Msg!=null && Msg!='' && Msg!='null'){
	alert(Msg);
	window.location.href = "/OLMS/getuser?code="+code;
}
function showConfirmDlg(code)
{
	$('dialog-confirm').css('display', 'block');
	 $( "#dialog-confirm" ).dialog({
	      resizable: false,
	      height: "auto",
	      wcodeth: 400,
	      modal: true,
	      buttons: {
	        "Yes": function() {
	        	window.location = "cancelLoan?code="+code;
	        	$( this ).dialog( "close" );
	        },
	        "No": function() {
	          $( this ).dialog( "close" );
	        }
	      }
	    });
}


</script>
<div code="dialog-confirm" title="Cancel Loan" style="display: none">
	<p>
		<span class="ui-icon ui-icon-alert"
			style="float: left; margin: 12px 12px 20px 0;"></span>Loan will be
		cancelled. Are you sure?
	</p>
</div>

<table class="MainTable" border="0" cellspacing="0" cellpadding="0"
	align="center">
	<tr>
		<td valign="top">
			<table class="Header" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="HeaderLeft"><img alt=""
						src="styles/cloudstyle1/Images/Spacer.gif"></td>
					<td class="th"><strong> Loan Details </strong></td>
					<td class="HeaderRight"><img alt=""
						src="styles/cloudstyle1/Images/Spacer.gif"></td>
				</tr>

			</table>
			<div code="UserDetails">
				<table class=Record cellspacing="0" cellpadding="0">
					<tr class="Row">
						<td><strong>User code</strong></td>
						
						<td>${user.code}</td>
						<td></td>
						<td><strong>CKYC Number</strong></td>
						
						<td>${user.name}</td>
						<td></td>
						<td><strong>Loan No</strong></td>
						
						<td>${user.Department}</td>
						<td></td>
						<td><strong>Status</strong></td>
						
						<td>${user.status}</td>
					
					   <td><strong>Loan Amount(Rs)</strong></td>
						
						<td>${user.reason}</td>
						<td></td>
						
					</tr>
								
				</table>
				
			</div>
		
		</td>
	</tr>
</table>
<br></br>
<%-- </form:form>
 --%>
<jsp:include page="tail.jsp" />

