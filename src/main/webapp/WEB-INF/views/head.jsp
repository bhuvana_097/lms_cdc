
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "java.io.*,java.util.*,java.sql.*,org.apache.taglibs.*"%>
<%@ page import = "javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix = "c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix = "sql"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!-- <meta http-equiv="content-type" content="text/html; charset=windows-1252"> -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> ${title} </title>
<link rel="stylesheet" type="text/css" href="${contextPath}/styles/cloudstyle1/Style_doctype.css">
<link rel="stylesheet" type="text/css" href="${contextPath}/chart/css/barchart.css">
<link rel="stylesheet" type="text/css" href="${contextPath}/styles/adx.css">
<link rel="stylesheet" type="text/css" href="${contextPath}/styles/cloudstyle1/Menu.css">
<link rel="stylesheet" type="text/css" href="${contextPath}/styles/cloudstyle1/Style_doctype.css">

<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-D3B5F5BF
</script>
<script language="JavaScript" src="Functions.js" type="text/javascript" charset="utf-8" ccs_src="file=Functions.js"></script>
<script language="JavaScript" type="text/javascript">
//End Include Common JSFunctions

//bind_events @1-7D95CD32
function bind_events() {
    try { Header_bind_events(); } catch(e) {}
    try { Footer_bind_events(); } catch(e) {}
}
//End bind_events

window.onload = bind_events; //Assign bind_events @1-19F7B649

//End CCS script
</script>
</head>
<body>



 <sql:setDataSource var = "acessData" driver = "com.mysql.jdbc.Driver"
         url = "jdbc:mysql://10.10.1.13:3306/orange"
         user = "orangelive"  password = "Orange@2401"/>
              
<script language="JavaScript" type="text/javascript">
//Begin CCS script
//Include Common JSFunctions @1-A2F3288E
</script>
<script language="JavaScript" src="Functions.js" type="text/javascript" charset="utf-8"></script>
<script language="JavaScript" type="text/javascript">
//End Include Common JSFunctions

//Include User Scripts @1-60F64B4A
</script>
<script language="JavaScript" src="js/menu/WCH.js" type="text/javascript"></script>
<script language="JavaScript" src="js/menu/ADxMenu.js" type="text/javascript"></script>

<script src="chart/js/d3.v3.min.js"></script>
<script language="JavaScript" type="text/javascript">
//End Include User Scripts

//bind_events @1-73B672FD
function Header_bind_events() {
    addEventHandler("HeaderMenu1Container", "load", load_ADxMenu);
}
//End bind_events

//End CCS script
</script>

