<head>
<title>ORFLEND USER</title>
</head>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<script type="text/javascript">
	var Msg ='<%=request.getParameter("auth_message")%>';
	if(Msg!=null && Msg!='' && Msg!='null'){
		alert(Msg);
		window.location.href = "/OLMS/list";
	}
	
	//<![CDATA[ 
	 // array of possible countries in the same order as they appear in the country selection list 
	 var countryLists = new Array(4) 
	 countryLists["empty"] = ["Select a Reason"]; 
	 countryLists["DeActive"] = ["Resigned", "Others"]; 
	 countryLists["Active"] = ["Active"];
	 
	 /* CountryChange() is called from the onchange event of a select element. 
	 * param selectObj - the select object which fired the on change event. 
	 */ 
	 function countryChange(selectObj) { 
	 // get the index of the selected option 
	 var idx = selectObj.selectedIndex; 
	 // get the value of the selected option 
	 var which = selectObj.options[idx].value; 
	 // use the selected option value to retrieve the list of items from the countryLists array 
	 cList = countryLists[which]; 
	 // get the country select element via its known id 
	 var cSelect = document.getElementById("reason"); 
	 
	 // remove the current options from the country select 
	 var len=cSelect.options.length; 
	 while (cSelect.options.length > 0) { 
	 cSelect.remove(0); 
	 } 
	 alert("Make Sure Your Activity with Proper Approvals");
	 var newOption; 
	 // create new options 
	 for (var i=0; i<cList.length; i++) { 
	 newOption = document.createElement("option"); 
	 newOption.value = cList[i];  // assumes option string and value are the same 
	 newOption.text=cList[i]; 
	 // add the new option 
	 try { 
	 cSelect.add(newOption);  // this will fail in DOM browsers but is needed for IE 
	 } 
	 catch (e) { 
	 cSelect.appendChild(newOption); 
	 } 
	 }
	 
	 } 
	//]]>
	 
	</script>

<center>
	<h1>Welcome to ORFLEND</h1>
</center>
<%@include file="menu.jsp"%>
<center>
<br/><br/>
<br/><br/>
<h1>UPDATE USER</h1>
<!-- <img width="300" height="55"  src="imgs/bluemoon_logo.png" /> -->

<br/><br/>
<br/><br/>

</center>
<sql:query dataSource="${acessData}" var="userDept">
	select dept_name from lms_user_dept_master where active=true;
</sql:query>
<sql:query dataSource="${acessData}" var="userData">
	select * from lms_user_master where id=<%= request.getParameter("id") %>;
</sql:query>
<sql:query dataSource="${acessData}" var="userBranch">
	select branch_name from  hht_branchmaster;
</sql:query>
<c:forEach items="${userData.rows}" var="columnData" varStatus="loop">
	<c:set var="code" value="${columnData.code}" scope="application"/>
	<c:set var="name" value="${columnData.name}" scope="application"/>
	<c:set var="passwd" value="${columnData.passwd}" scope="application"/>
	<c:set var="dept" value="${columnData.dept}" scope="application"/>
	<c:set var="hier_level" value="${columnData.hier_level}" scope="application"/>
	<c:set var="status" value="${columnData.status}" scope="application"/>
</c:forEach>
<sql:query dataSource="${acessData}" var="userData">
	select * from lms_user_master where id=<%= request.getParameter("code") %>;
</sql:query>
<c:forEach items="${userData.rows}" var="columnData" varStatus="loop">
	<c:set var="code" value="${columnData.code}" scope="application"/>
	<c:set var="name" value="${columnData.name}" scope="application"/>
	<c:set var="passwd" value="${columnData.passwd}" scope="application"/>
	<c:set var="dept" value="${columnData.dept}" scope="application"/>
	<c:set var="hier_level" value="${columnData.hier_level}" scope="application"/>
	<c:set var="status" value="${columnData.status}" scope="application"/>
</c:forEach>
 <form method="POST" action="lmsuserupdate" class="form-signin">
<input type="hidden" id="id" name="id" value="<%= request.getParameter("id") %>">

<table class="MainTable" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td valign="top">
		<div id="searchdiv"> 		 
        	<table class="Record" cellspacing="0" cellpadding="0">
				<tr class="Controls">
					<td class="th"> Code </td>
					<td> <input name="lmsusercode" type="text" class="form-control" placeholder="Code" value="${code}" autofocus="true" tabindex="1" required readonly="readonly"/></td>
				</tr>
				<tr class="Controls">
					<td class="th"> Name </td>
					<td> <input name="lmsusername" type="text" class="form-control" placeholder="Name" value="${name}" tabindex="2" required/></td>
				</tr>
<!-- 			 	<tr class="Controls"> -->
<!-- 					<td class="th"> Password </td> -->
<%-- 					<td> <input name="lmsuserpwd" type="password" class="form-control" placeholder="Password" value="${passwd}" tabindex="3" required/></td> --%>
<!-- 				</tr> -->
				<tr class="Controls">
					<td class="th"> Department </td>
					<td> 
						<select name=dept  tabindex="4" required>
							<option value=''>Select</option>
							<c:forEach var="row" items="${userDept.rows}" varStatus="loop">
						    <option value='${row.dept_name}' ${dept eq row.dept_name ? 'selected' : ''}>${row.dept_name}</option>
						    </c:forEach>
						</select>
					</td>
				</tr>
				<tr class="Controls">
					<td class="th"> Hierarchy level </td>
					<td> 
						<select name=hier_level  tabindex="5" required>
							<option value=''>Select</option>
						    <option value='1' ${hier_level=='1' ? 'selected' : ''}>1</option>
						    <option value='2' ${hier_level=='2' ? 'selected' : ''}>2</option>
						    <option value='3' ${hier_level=='3' ? 'selected' : ''}>3</option>
						    <option value='4' ${hier_level=='4' ? 'selected' : ''}>4</option>
						    <option value='5' ${hier_level=='5' ? 'selected' : ''}>5</option>
						</select>
					</td>
				</tr>
								<tr class="Controls">
					<td class="th"> Branch/Cluster </td>
					<td> 
						<select name=branch  tabindex="5" required>
							<option value=''>Select</option>
							<c:forEach var="row" items="${userBranch.rows}" varStatus="loop">
						    <option value='${row.branch_name}' ${searchlmsuserresult.branch eq row.branch_name ? 'selected' : ''}>${row.branch_name}</option>
		  </c:forEach>
						
<!-- <option value='WARANGAL'>	WARANGAL	</option> -->
<!-- <option value='WARANAGAL'>	WARANAGAL	</option> -->
<!-- <option value='Wanaparthy'>	Wanaparthy	</option> -->
<!-- <option value='Vizianagaram'>	Vizianagaram	</option> -->
<!-- <option value='Visakhapatnam'>	Visakhapatnam	</option> -->
<!-- <option value='Vijayawada'>	Vijayawada	</option> -->
<!-- <option value='Vellore'>	Vellore	</option> -->
<!-- <option value='UDUPI'>	UDUPI	</option> -->
<!-- <option value='Tuticorin'>	Tuticorin	</option> -->
<!-- <option value='TUMKUR'>	TUMKUR	</option> -->
<!-- <option value='Trivandrum'>	Trivandrum	</option> -->
<!-- <option value='Trichy'>	Trichy	</option> -->
<!-- <option value='TIRUVARUR'>	TIRUVARUR	</option> -->
<!-- <option value='TIRUPPUR'>	TIRUPPUR	</option> -->
<!-- <option value='Tirupathi'>	Tirupathi	</option> -->
<!-- <option value='Thrissur'>	Thrissur	</option> -->
<!-- <option value='Thiruvannamalai'>	Thiruvannamalai	</option> -->
<!-- <option value='THIRUNELVELI'>	THIRUNELVELI	</option> -->
<!-- <option value='Theni'>	Theni	</option> -->
<!-- <option value='Tenkasi'>	Tenkasi	</option> -->
<!-- <option value='Tanjore'>	Tanjore	</option> -->
<!-- <option value='Srikakulam'>	Srikakulam	</option> -->
<!-- <option value='Sivakasi'>	Sivakasi	</option> -->
<!-- <option value='SHIMOGA'>	SHIMOGA	</option> -->
<!-- <option value='Salem'>	Salem	</option> -->
<!-- <option value='Ramanad'>	Ramanad	</option> -->
<!-- <option value='Rajahmundry'>	Rajahmundry	</option> -->
<!-- <option value='RAICHUR'>	RAICHUR	</option> -->
<!-- <option value='Pudukkottai'>	Pudukkottai	</option> -->
<!-- <option value='Puducherry(TN Geo)'>	Puducherry(TN Geo)	</option> -->
<!-- <option value='PUDUCHERRY'>	PUDUCHERRY	</option> -->
<!-- <option value='Pudhucherry(TN Geo)'>	Pudhucherry(TN Geo)	</option> -->
<!-- <option value='Pattukottai'>	Pattukottai	</option> -->
<!-- <option value='Palakad'>	Palakad	</option> -->
<!-- <option value='Ongole'>	Ongole	</option> -->
<!-- <option value='Omalur'>	Omalur	</option> -->
<!-- <option value='Nizamabad'>	Nizamabad	</option> -->
<!-- <option value='Nellore'>	Nellore	</option> -->
<!-- <option value='NARASARARAOPET'>	NARASARARAOPET	</option> -->
<!-- <option value='NANDYAL'>	NANDYAL	</option> -->
<!-- <option value='NAMAKKAL'>	NAMAKKAL	</option> -->
<!-- <option value='NALGONDA'>	NALGONDA	</option> -->
<!-- <option value='Nagercoil'>	Nagercoil	</option> -->
<!-- <option value='MYSORE'>	MYSORE	</option> -->
<!-- <option value='MIRYALAGUDA'>	MIRYALAGUDA	</option> -->
<!-- <option value='MANGALORE'>	MANGALORE	</option> -->
<!-- <option value='MANDYA'>	MANDYA	</option> -->
<!-- <option value='Malappuram'>	Malappuram	</option> -->
<!-- <option value='MAHABUBNAGAR'>	MAHABUBNAGAR	</option> -->
<!-- <option value='Madurai'>	Madurai	</option> -->
<!-- <option value='Kurnool'>	Kurnool	</option> -->
<!-- <option value='Kumbakonam'>	Kumbakonam	</option> -->
<!-- <option value='Krishnagiri'>	Krishnagiri	</option> -->
<!-- <option value='Kollam'>	Kollam	</option> -->
<!-- <option value='KOLAR'>	KOLAR	</option> -->
<!-- <option value='Kochi'>	Kochi	</option> -->
<!-- <option value='KIDIRI'>	KIDIRI	</option> -->
<!-- <option value='KHAMMAM'>	KHAMMAM	</option> -->
<!-- <option value='Karur'>	Karur	</option> -->
<!-- <option value='KARIMNAGAR'>	KARIMNAGAR	</option> -->
<!-- <option value='Karaikudi'>	Karaikudi	</option> -->
<!-- <option value='Karaikal (TN Geo)'>	Karaikal (TN Geo)	</option> -->
<!-- <option value='KARAIKAL'>	KARAIKAL	</option> -->
<!-- <option value='Kannur'>	Kannur	</option> -->
<!-- <option value='Kanchipuram'>	Kanchipuram	</option> -->
<!-- <option value='KAKINADA'>	KAKINADA	</option> -->
<!-- <option value='Kadiri'>	Kadiri	</option> -->
<!-- <option value='Kadappa'>	Kadappa	</option> -->
<!-- <option value='HUBLI'>	HUBLI	</option> -->
<!-- <option value='HO'>	HO	</option> -->
<!-- <option value='HINDUPUR'>	HINDUPUR	</option> -->
<!-- <option value='HASSAN'>	HASSAN	</option> -->
<!-- <option value='Guntur'>	Guntur	</option> -->
<!-- <option value='Gudur'>	Gudur	</option> -->
<!-- <option value='Erode'>	Erode	</option> -->
<!-- <option value='Eluru'>	Eluru	</option> -->
<!-- <option value='Dindigul'>	Dindigul	</option> -->
<!-- <option value='Dharmapuri'>	Dharmapuri	</option> -->
<!-- <option value='DAVANAGERE'>	DAVANAGERE	</option> -->
<!-- <option value='Cuddalore'>	Cuddalore	</option> -->
<!-- <option value='Coimbatore'>	Coimbatore	</option> -->
<!-- <option value='chennai'>	chennai	</option> -->
<!-- <option value='CHANNAPATNA'>	CHANNAPATNA	</option> -->
<!-- <option value='Changanassery'>	Changanassery	</option> -->
<!-- <option value='Calicut'>	Calicut	</option> -->
<!-- <option value='BIDAR'>	BIDAR	</option> -->
<!-- <option value='BHIMAVARAM'>	BHIMAVARAM	</option> -->
<!-- <option value='BELLARY'>	BELLARY	</option> -->
<!-- <option value='Attur'>	Attur	</option> -->
<!-- <option value='Ananthapur3'>	Ananthapur3	</option> -->
<!-- <option value='Ananthapur2'>	Ananthapur2	</option> -->
<!-- <option value='Ananthapur1'>	Ananthapur1	</option> -->
<!-- <option value='Ananthapur'>	Ananthapur	</option> -->
<!-- <option value='Anakapalli'>	Anakapalli	</option> -->
<!-- <option value='Alappuzha'>	Alappuzha	</option><option value='97'>AP1</option> -->
<!-- <option value='AP2'>AP2</option> -->
<!-- <option value='KA'>KA</option> -->
<!-- <option value='KL'>KL</option> -->
<!-- <option value='PY'>PY</option> -->
<!-- <option value='TN1'>TN1</option> -->
<!-- <option value='TN2'>TN2</option> -->
<!-- <option value='TN3'>TN3</option> -->
<!-- <option value='TN4'>TN4</option> -->
<!-- <option value='TS'>TS</option> -->

</select>
					</td>
				</tr>
			 	<tr class="Controls">
					<td class="th"> Status </td>
			
 <td> <select name="status" tabindex="14" onchange="countryChange(this);">
    <option value="empty">Select a status</option>
     <option value='Active' ${status=='Active' ? 'selected' : ''}>Active</option>
	 <option value='DeActive' ${status=='DeActive' ? 'selected' : ''}>DeActive</option>
<!--     <option value="DeActive">DeActive</option> -->
<!--     <option value="Active">Active</option> -->
    </select></td>
 
				</tr>
			<tr class="Controls">
					<td class="th">Reason </td>
				<!-- 	<td> 
						<select name=status  tabindex="14" required>
							<option value=''>Select</option>
						    <option value='Active' ${status=='Resigned' ? 'selected' : ''}>Resigned</option>
						    <option value='DeActive' ${status=='Others' ? 'selected' : ''}>Others</option>
						   </select>
					</td> -->
				<td>
  <select name="reason" id="reason"  >
    <option value="0">Select a Reason</option>
    <!-- <option value="Resigned">Resigned</option>
	<option value="Others">Others</option> -->
			</select></td>
				</tr> 
		
						
        		<tr class="Bottom">
        			<td style="TEXT-ALIGN: right" colspan="2">
        			<button class="Button" type="submit">Update</button></td>
        		</tr>
      		</table>
    	</div>
      </td> 
    </tr>
  </table>
 </form>

<br/><br/>
<br/><br/>
<jsp:include page="tail.jsp" />
