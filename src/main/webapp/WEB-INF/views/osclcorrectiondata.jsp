<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<script type="text/javascript">
	var Msg ='<%=request.getParameter("auth_message")%>';
	if(Msg!=null && Msg!='' && Msg!='null'){
		alert(Msg);
		window.location.href = "/OLMS/list";
	}
</script>
<center>
	<h1>Welcome to ORFLEND</h1>
</center>
<%@include file="menu.jsp"%>
<center>
<br/><br/>
<br/><br/>
<h1>OSCL CORRECTION</h1>
<!-- <img width="300" height="55"  src="imgs/bluemoon_logo.png" /> -->

<br/><br/>
<br/><br/>

</center>
<sql:query dataSource="${acessData}" var="userDept">
	select dept_name from lms_user_dept_master where active=true;
</sql:query>
<sql:query dataSource="${acessData}" var="userData">
	select * from lms_user_master where id=<%= request.getParameter("id") %>;
</sql:query>
<c:forEach items="${userData.rows}" var="columnData" varStatus="loop">
	<c:set var="code" value="${columnData.code}" scope="application"/>
	<c:set var="name" value="${columnData.name}" scope="application"/>
	<c:set var="passwd" value="${columnData.passwd}" scope="application"/>
	<c:set var="dept" value="${columnData.dept}" scope="application"/>
	<c:set var="hier_level" value="${columnData.hier_level}" scope="application"/>
	<c:set var="status" value="${columnData.status}" scope="application"/>
</c:forEach>
 <form method="POST" action="lmsuserupdate" class="form-signin">
<input type="hidden" id="id" name="id" value="<%= request.getParameter("id") %>">
<table class="MainTable" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td valign="top">
		<div id="searchdiv"> 		 
        	<table class="Record" cellspacing="0" cellpadding="0">
				<tr class="Controls">
					<td class="th"> Code </td>
					<td> <input name="lmsusercode" type="text" class="form-control" placeholder="Code" value="${code}" autofocus="true" tabindex="1" required readonly="readonly"/></td>
				</tr>
				<tr class="Controls">
					<td class="th"> Name </td>
					<td> <input name="lmsusername" type="text" class="form-control" placeholder="Name" value="${name}" tabindex="2" required/></td>
				</tr>
			<!-- 	<tr class="Controls">
					<td class="th"> Password </td>
					<td> <input name="lmsuserpwd" type="password" class="form-control" placeholder="Password" value="${passwd}" tabindex="3" required/></td>
				</tr> -->
				<tr class="Controls">
					<td class="th"> Department </td>
					<td> 
						<select name=dept  tabindex="4" required>
							<option value=''>Select</option>
							<c:forEach var="row" items="${userDept.rows}" varStatus="loop">
						    <option value='${row.dept_name}' ${dept eq row.dept_name ? 'selected' : ''}>${row.dept_name}</option>
						    </c:forEach>
						</select>
					</td>
				</tr>
				<tr class="Controls">
					<td class="th"> Hierarchy level </td>
					<td> 
						<select name=hier_level  tabindex="5" required>
							<option value=''>Select</option>
						    <option value='1' ${hier_level=='1' ? 'selected' : ''}>1</option>
						    <option value='2' ${hier_level=='2' ? 'selected' : ''}>2</option>
						    <option value='3' ${hier_level=='3' ? 'selected' : ''}>3</option>
						    <option value='4' ${hier_level=='4' ? 'selected' : ''}>4</option>
						    <option value='5' ${hier_level=='5' ? 'selected' : ''}>5</option>
						</select>
					</td>
				</tr>
				<tr class="Controls">
					<td class="th"> Status </td>
					<td> 
						<select name=status  tabindex="14" required>
							<option value=''>Select</option>
						    <option value='Active' ${status=='Active' ? 'selected' : ''}>Active</option>
						    <option value='DeActive' ${status=='DeActive' ? 'selected' : ''}>DeActive</option>
						   </select>
					</td>
				</tr>
			
						
        		<tr class="Bottom">
        			<td style="TEXT-ALIGN: right" colspan="2">
        			<button class="Button" type="submit">Update</button></td>
        		</tr>
      		</table>
    	</div>
      </td> 
    </tr>
  </table>
 </form>

<br/><br/>
<br/><br/>
<jsp:include page="tail.jsp" />
