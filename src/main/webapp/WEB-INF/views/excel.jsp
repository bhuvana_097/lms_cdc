<%@include file="head.jsp"%>
<h1>${param.title}</h1>
<%-- <h2>${download_query}</h2> --%>
<%
	response.setContentType("application/vnd.ms-excel");
	String fileTitle = request.getParameter("title");
	response.setHeader("Content-Disposition", "inline; filename=" + fileTitle+".xls");
%>

<sql:query dataSource="${acessData}" var="tableData">
         ${download_query}
      </sql:query>

<table>
	<thead>
		<tr>
			<c:forEach var="row" items="${tableData.columnNames}"
				varStatus="loop">
				<th>${row}</th>
			</c:forEach>
		</tr>
	</thead>
	<tbody>		
			<c:forEach var="row" items="${tableData.rows}" varStatus="loop">
			<tr>
				<c:forEach var="column" items="${tableData.columnNames}">
					<td>${row[column]}</td>
				</c:forEach>
			</tr>
			</c:forEach>
	</tbody>
</table>
</body>
</html>