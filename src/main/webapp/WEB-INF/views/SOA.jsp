<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--  <set var="title" value="List"/> -->
<center>
	<h1>SOA</h1>
</center>
<%@include file="menu.jsp"%>
<table class="MainTable" border="0" cellspacing="0" cellpadding="0"
	align="center">
	<tr>
		<td valign="top">
			<table class="Header" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="HeaderLeft"><img alt=""
						src="styles/cloudstyle1/Images/Spacer.gif"></td>
					<td class="th"><strong> SOA </strong></td>
					<form:form action="genSOA" method="POST" modelAttribute="loan">
							<form:hidden path="loanId" value="${loan.loanId}" />
							<td style="TEXT-ALIGN: center" class="th"><strong><input type="submit"
								class="Button" value="Download as PDF" name="SOA"></strong></td>

						</form:form>
					<td class="HeaderRight"><img alt=""
						src="styles/cloudstyle1/Images/Spacer.gif"></td>
					
				</tr>

			</table>
<div id="SOA Details">
						<table class=Grid cellspacing="0" cellpadding="0">
							<tr class="Caption">
								<th scope="col">Value Date</th>
								<th scope="col">Particulars</th>
								<th scope="col">Debit</th>
								<th scope="col">Credit</th>
								<th scope="col">Book No</th>
								<th scope="col">Receipt No</th>
							</tr>
							<c:set var="rowclass" value="Row" />
							<c:forEach items="${listSOABean.soaList}" var="SOADetails">
								<tr class='${rowclass}'>
									<td>${SOADetails.valuedate}</td>
									<td>${SOADetails.particulars}</td>
									<td>${SOADetails.debit}</td>
									<td>${SOADetails.credit}</td>
									<td>${SOADetails.bookno}</td>
									<td>${SOADetails.receiptno}</td>
								</tr>
								<c:set var="rowclass"
									value="${rowclass eq 'Row' ? 'AltRow' : 'Row'}" />
							</c:forEach>
	<tr class="AltRow">
		<c:choose>
     <c:when test="${loan.flagSec == 'Y'}" >
							<td ><strong>REACH</strong></td>
					
					<td>${loan.flagSec}</td>
					<td></td>
					</c:when></c:choose>
					
				</tr>  
						</table>
					</div>
		</td>
	</tr>
</table>


<jsp:include page="tail.jsp" />