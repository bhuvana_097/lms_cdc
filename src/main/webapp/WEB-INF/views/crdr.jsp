<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@include file="menu.jsp"%>
<!--  <set var="title" value="List"/> -->
<center>
	<h1>Loan Waiver</h1>
</center>

<form:form method="POST" action="saveCrDrNote" modelAttribute="crdrnote">
<table class="MainTable" border="0" cellspacing="0" cellpadding="0"
	align="center">
	<tr>
		<td valign="top">
			<table class="Header" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="HeaderLeft"><img alt=""
						src="styles/cloudstyle1/Images/Spacer.gif"></td>
					<td class="th"><strong> Loan Waiver </strong></td>
					<td class="HeaderRight"><img alt=""
						src="styles/cloudstyle1/Images/Spacer.gif"></td>
				</tr>

			</table>
			<div id="NoteDetails">
				<table class=Record cellspacing="0" cellpadding="0">
					<tr class="Row">
						<td><strong>Loan Id</strong></td>
						<form:hidden path="loanId" value="${loanid}" />
						<td><input type="text" value="${loanid}" name="${loanid}" readonly></td>
					</tr>					
					<tr class="Row">
						<td><strong>Waiver Type</strong></td>
						<td><form:select path="waiverHead">
								<c:forTokens items="${waiverType}" delims="," var="listVal">
									<option value= "${listVal}">${listVal}</option>
								</c:forTokens>
						</form:select></td>
					</tr>
					
					<tr class="Row">
						<td><strong>Amount in Rupee</strong></td>
						<td><form:input path="amount" /></td>
					</tr>
					<tr class="Row">
						<td><strong>Description</strong></td>
						<td><form:input path="description" /></td>
					</tr>
					<tr class="Bottom">
						<td colspan=2><input type='submit' value='Add Note' /></td>
					</tr>
				</table>
			</div>
		</td>
	</tr>
</table>
</form:form>


<jsp:include page="tail.jsp" />